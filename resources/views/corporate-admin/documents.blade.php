@extends('corporate-admin.layouts.app')
@section('content')
			<div class="users_datatablebox">
	      <div class="signeup_topbox">
	        <div class="signeup_lefttextbox">
	          <p>Documents</p>
	        </div>
	        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Document</a></div>
	      </div>

	      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
	        <thead>
	          <tr>
	            <th>Sr</th>
	            <th>Name</th>
	            <th>Type</th>
	            <th>Disable ?</th>
	            <th>Action</th>
	          </tr>
	        </thead>
	        <tbody>
	        	<?php $i=1; ?>
	          @foreach($document as $doc)
	          <tr>
	            <th><?php echo $i++; ?></th>
	            <th>{{$doc->doument_name}}</th>
	            <th>{{($doc->doument_type == 1) ? 'YES/No/(N/A)' :'Yes/No'}}</th>
	            <th> <a href="javascript:void(0)" class="deletedocument editdeletebtn" id="deletestaff"  data-id="{{$doc->id}}">
	            	@if($doc->is_deleted == 1)
	            	  No
	            	@else
	            	  Yes
	            	@endif
	            </a></th>
	            <th>
	              <a href="#" class="editdeletebtn editdocument" data-toggle="modal" data-target="#editModal" data-id="{{$doc->id}}">
	                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
	              </a> 
	             </th>
	          </tr>
	          	   <!--   edit model -->
	          	   <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
	          	    <div class="modal-dialog">
	          	      <div class="modal-content">
	          	        <div class="modal-body">
	          	          <div class="editsktoboxcover">
	          	           <form method="post" action="{{route('corporate-admin.updatedocument')}}" enctype="multipart/form-data" id="updatedocument">
	          	            @csrf            
	          	            <div class="titlandclobtn2">
	          	              <h3>Edit Document</h3>
	          	              <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
	          	            </div>
	          	            <div class="enterskintonebox">
	          	              <div class="form-group">
	          	                <input type="hidden" name="documentid" id="documentid">
	          	                <input type="text" class="form-control"  name="doument_name" placeholder="Enter Document Name" value="" id="documentname">
	          	              </div>
	          	              <div class="form-group has-feedback">
	          	                <label class="control-label" for="mRadio">Document Type</label>
	          	                <div id="mRadio" class="form-control">
	          	                  <label class="radio-inline">
	          	                    <input type="radio" name="doument_type" value="0" id="0">Yes/No</label>
	          	                  <!-- <label class="radio-inline">
	          	                    <input type="radio" name="doument_type" value="No" id="No">No</label> -->
	          	                  <label class="radio-inline">
	          	                    <input type="radio" name="doument_type" value="1" id="1">Yes/ No/(N/A)</label>
	          	                 </div>
	          	               </div> 
	          	              <input type="button" name="" value="Update" class="btn" id="updatebtn">
	          	            </div>
	          	          </form>
	          	        </div>

	          	      </div>
	          	    </div>
	          	  </div>
	          	</div>
	          @endforeach  
	        </tbody>
	      </table>
	    </div>  
	      <!--     document modal -->
	      <div class="modal fade editskinboxmodal" id="myModal" role="dialog">
	        <div class="modal-dialog">
	          <div class="modal-content">
	            <div class="modal-body">
	              <div class="editsktoboxcover">
	               <form method="post" action="{{route('corporate-admin.adddocument')}}" enctype="multipart/form-data" id="adddocument">
	                @csrf          
	                <div class="titlandclobtn2">
	                  <h3>Add Document</h3>
	                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
	                </div>
	                <div class="enterskintonebox">
	                  <div class="form-group">
	                    <input type="text" class="form-control"  name="doument_name" placeholder="Enter Document Name">
	                  </div>
	                  <div class="form-group has-feedback">
	                    <label class="control-label" for="mRadio">document Type</label>
	                    <div id="mRadio" class="form-control">
	                      <label class="radio-inline">
	                        <input type="radio" name="doument_type" value="0">Yes/No</label>
	                      <!-- <label class="radio-inline">
	                        <input type="radio" name="doument_type" value="No">No</label> -->
	                      <label class="radio-inline">
	                        <input type="radio" name="doument_type" value="1">Yes/No/(N/A)</label>
	                     </div>
	                   </div>     
	                  <input type="button" name="" value="Add" class="btn" id="btnSave">
	                </div>              
	              </form>
	            </div>

	          </div>
	        </div>
	      </div>
	    </div>	
@endsection
@section('scripts')
	<script type="text/javascript">
	  $(document).ready(function(){
	  $('.deletedocument').click(function(){
	              
	              
	                 var id = $(this).data('id');
	                   
	                    $.ajax({
	                             type: "GET",
	                             url: "{{route('corporate-admin.deletedocument')}}",
	                             data: {id:id},
	                             success: function (data) {
	                                    toastr.success(data.message);
	                                    location.reload();    
	                                 }         
	                         }); 
	              
	             });
	  });
	</script>
	<script type="text/javascript">
		 $("#btnSave").click(function(e){
		    e.preventDefault()
		   var $form = $("#adddocument");
		     
		   $.ajax({
		       type: $form.attr('method'),
		       url: $form.attr('action'),
		       data: $form.serialize(),
		       success: function (data, status) {
		            if(data.error){
		               return;
		           }
		           if(data.status==false){
		           		toastr.error(data.message);
		           }
		           else{
		        		toastr.success('Document Added Successfully');
		           		$('#myModal').modal('hide'); 
		           		location.reload();
		       	   }
		       },
		       error: function (result) {

		       }
		   });
		   
		});

		 
		  $('.deletedocument[data-toggle=confirmation]').confirmation({
		 		          rootSelector: '[data-toggle=confirmation]',
		 		          container: 'body',
		 		          onConfirm: function() {
		 		           	var id = $(this).data('id');
		 		           	  
		 		           	   $.ajax({
		 		           	            type: "GET",
		 		           	            url: "{{route('corporate-admin.deletedocument')}}",
		 		           	            data: {id:id},
		 		           	            success: function () {
		 		           	                   toastr.success('Document Deleted Successfully');  
		 		           	                   location.reload();
		 		           	                }         
		 		           	        }); 
		 		          },
		 		        });
		$(document).on('click', '.editdocument', function (e) {
		e.preventDefault();
		var id = $(this).data('id');
		 $.ajax({
		            type: "GET",
		            url: "{{route('corporate-admin.editdocument')}}",
		            data: {id:id},
		            success: function (data) {
		            				console.log(data.doument_name);
		            				$("#documentname").val(data.doument_name);
		            				$("#documentid").val(id);
		                        // $("#tonename").val(data);
		                        // $("#toneid").val(id);
		                         $("#"+data.doument_type).prop("checked", true);
		                }         
		        }); 

		 });
		 $("#updatebtn").click(function(e){
		    e.preventDefault()
		   var $form = $("#updatedocument");
		     
		   $.ajax({
		       type: $form.attr('method'),
		       url: $form.attr('action'),
		       data: $form.serialize(),
		       success: function (data, status) {
		            if(data.error){
		               return;
		           }
		           if(data.status==false){
		           		toastr.error(data.message);
		           }
		           else{
		        toastr.success('Document Updated Successfully');
		           $('#editModal').modal('hide');  
		           location.reload();
		       	}
		       },
		       error: function (result) {

		       }
		   });
		   
		});
	</script>
	<script type="text/javascript">
	  $('#statstable').on('page.dt', function() {
	    setTimeout(
	      function() {
	        $('.deletedocument').click(function(){
	                    
	                    
	                       var id = $(this).data('id');
	                         
	                          $.ajax({
	                                   type: "GET",
	                                   url: "{{route('corporate-admin.deletedocument')}}",
	                                   data: {id:id},
	                                   success: function (data) {
	                                          toastr.success(data.message);
	                                          location.reload();    
	                                       }         
	                               }); 
	                    
	                   });
	      }, 500);
	  });
	 
	</script>
	<script type="text/javascript">
	  $('#statstable').on('order.dt', function() {
	    setTimeout(
	      function() {
	        $('.deletedocument').click(function(){
	                    
	                    
	                       var id = $(this).data('id');
	                         
	                          $.ajax({
	                                   type: "GET",
	                                   url: "{{route('corporate-admin.deletedocument')}}",
	                                   data: {id:id},
	                                   success: function (data) {
	                                          toastr.success(data.message);
	                                          location.reload();    
	                                       }         
	                               }); 
	                    
	                   });
	      }, 500);
	  });
	 
	</script>
@endsection