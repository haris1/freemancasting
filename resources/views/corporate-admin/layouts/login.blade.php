<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content=""/>
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">    
</head>
<body>

 @yield('content')
 <script type="text/javascript" src="{{asset('assets/js/jquery.js') }}"></script>
 <script src="{{asset('assets/js/bootstrap.min.js') }}"></script>
  
</body>
</html>