    <div class="innercategories">
      <ul>
      <!--  <li>
          <a href="index.html">Dashboard</a>
        </li>  -->      
       <!--  <li class="{{Request::is('corporate-admin/form')?'activelist':''}}">
          <a href="{{route('corporate-admin.form')}}">Patients</a>
        </li> -->
        <li class="{{Request::is('corporate-admin/list')?'activelist':''}}">
          <a href="{{route('corporate-admin.list')}}">Internal Staff</a>
        </li>
      
       
        <li>
            <a href="javascript:void(0)" class="plusbttnbox myBtn">Appearance <i class=" {{ (Request::route()->getName() == 'corporate-admin.eyes') || (Request::route()->getName() == 'corporate-admin.ethnicity') || (Request::route()->getName() == 'corporate-admin.hairlength') || (Request::route()->getName() == 'corporate-admin.handedness')  || (Request::route()->getName() == 'corporate-admin.skintone')  || (Request::route()->getName() == 'corporate-admin.nationality')  || (Request::route()->getName() == 'corporate-admin.haircolor') ? 'fa fa-minus' : 'fa fa-plus' }}" aria-hidden="true" ></i></a>
            <div class="dropdown custmenbox">
              <!-- <button id="myBtn" class="dropbtn">Dropdown</button> -->



              <div  class="dropdown-content myDropdown {{ (Request::route()->getName() == 'corporate-admin.eyes') || (Request::route()->getName() == 'corporate-admin.ethnicity') || (Request::route()->getName() == 'corporate-admin.hairlength') || (Request::route()->getName() == 'corporate-admin.handedness')  || (Request::route()->getName() == 'corporate-admin.skintone')  || (Request::route()->getName() == 'corporate-admin.nationality')  ||
              (Request::route()->getName() == 'corporate-admin.hairfacial')  ||
              (Request::route()->getName() == 'corporate-admin.hairstyle')  ||
              (Request::route()->getName() == 'corporate-admin.bodytrait')  ||
               (Request::route()->getName() == 'corporate-admin.haircolor') ? 'show' : '' }}">      

                <a  class="{{Request::is('corporate-admin/eyes')?'activelist':''}}" href="{{route('corporate-admin.eyes')}}">Eyes</a>
                <a class="{{Request::is('corporate-admin/ethnicity')?'activelist':''}}" href="{{route('corporate-admin.ethnicity')}}">Ethnicity</a>

                <a  class="{{Request::is('corporate-admin/hairlength')?'activelist':''}}" href="{{route('corporate-admin.hairlength')}}">Hair Length</a> 
                
                <a  class="{{Request::is('corporate-admin/hairfacial')?'activelist':''}}" href="{{route('corporate-admin.hairfacial')}}">Hair Facial</a>
                
                <a  class="{{Request::is('corporate-admin/hairstyle')?'activelist':''}}" href="{{route('corporate-admin.hairstyle')}}">Hair Style</a>
                
                <a  class="{{Request::is('corporate-admin/bodytrait')?'activelist':''}}" href="{{route('corporate-admin.bodytrait')}}">Body Trait</a>        
                
                <a href="{{route('corporate-admin.handedness')}}"  class="{{Request::is('corporate-admin/handedness')?'activelist':''}}">Handedness</a>
                
                <a href="{{route('corporate-admin.skintone')}}" class="{{Request::is('corporate-admin/skintone')?'activelist':''}}">Skin Tone</a>
                
                <a href="{{route('corporate-admin.nationality')}}" class="{{Request::is('corporate-admin/nationality')?'activelist':''}}">Nationality</a>
                
                <a href="{{route('corporate-admin.haircolor')}}" class="{{Request::is('corporate-admin/haircolor')?'activelist':''}}">Hair Color</a>

              </div>
            </div>
        </li>
         <li class="{{Request::is('corporate-admin/tags')?'activelist':''}}">

          <a href="{{route('corporate-admin.tags')}}">Tags</a>
        </li>
         
        <li class="{{Request::is('corporate-admin/documents')?'activelist':''}}">
          <a href="{{route('corporate-admin.documents')}}">Documents</a>
        </li>
        <li class="{{Request::is('corporate-admin/agent')?'activelist':''}}">
          <a href="{{route('corporate-admin.agent')}}">Agent</a>
        </li>
        <li class="{{Request::is('corporate-admin/union')?'activelist':''}}">
          <a href="{{route('corporate-admin.Union')}}">Union</a>
        </li>
        <li class="{{Request::is('corporate-admin/flag')?'activelist':''}}">
          <a href="{{route('corporate-admin.Flag')}}">Flag</a>
        </li>
        <li class="{{Request::is('corporate-admin/agency')?'activelist':''}}">
          <a href="{{route('corporate-admin.agency')}}">Agency</a>
        </li>
        <li class="hidden {{Request::is('corporate-admin/piercing')?'activelist':''}}">
          <a href="{{route('corporate-admin.piercing')}}">Piercing</a>
        </li>
        <li class="hidden {{Request::is('corporate-admin/tattoo')?'activelist':''}}">
          <a href="{{route('corporate-admin.tattoo')}}">Tattoos</a>
        </li>
        <li class="hidden">
            <a href="javascript:void(0)" class="plusbttnbox myBtn">Vehicles <i class="fa fa-plus" aria-hidden="true"></i></a>
            <div class="dropdown custmenbox">
              <!-- <button id="myBtn" class="dropbtn">Dropdown</button> -->
              <div  class="dropdown-content myDropdown ">      

                <a  class="{{Request::is('corporate-admin/vehicle_type')?'activelist':''}}" href="{{route('corporate-admin.vehicletype')}}">Vehicle Type</a>
                <a  class="{{Request::is('corporate-admin/eyes')?'activelist':''}}" href="{{route('corporate-admin.eyes')}}">Vehicle Year</a>
                <a  class="{{Request::is('corporate-admin/vehicle_make')?'activelist':''}}" href="{{route('corporate-admin.vehiclemake')}}">Vehicle Make</a>
                <a  class="{{Request::is('corporate-admin/eyes')?'activelist':''}}" href="{{route('corporate-admin.eyes')}}">Vehicle Model</a>
                <a  class="{{Request::is('corporate-admin/eyes')?'activelist':''}}" href="{{route('corporate-admin.eyes')}}">Vehicle Condition</a>
                <a  class="{{Request::is('corporate-admin/vehicle_accessories')?'activelist':''}}" href="{{route('corporate-admin.vehicleaccessories')}}">Accessories</a>
              </div>
            </div>
        </li>
        <li class="{{Request::is('corporate-admin/availability')?'activelist':''}}">
          <a href="{{route('corporate-admin.availability')}}">Performer Availability</a>
        </li>
        <li class="{{Request::is('corporate-admin/performerstatus')?'activelist':''}}">
          <a href="{{route('corporate-admin.performerstatus')}}">Performer Status</a>
        </li>
        <li class="{{Request::is('corporate-admin/helpdesk')?'activelist':''}}">
          <a href="{{route('corporate-admin.helpdesk')}}">Help Desk</a>
        </li>
      </ul>
    </div>
