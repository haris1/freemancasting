<!DOCTYPE html>
<html lang="en">
<head>
  <title>admin</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content=""/>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  <link rel="stylesheet" href="{{ asset('assets/corporate-admin/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/corporate-admin/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/corporate-admin/css/styles.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/corporate-admin/css/select2.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/toastr.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">

</head>
<body>
 	@include("corporate-admin.layouts.header") 

	<div class="parettow_coverbox">
		<div class="left_paretboxpart">
      @include("corporate-admin.layouts.sidebar")
    </div>
    <div class="right_paddingboxpart">
      @yield('content')
		</div>	
	</div>
	@include("corporate-admin.layouts.js")
	@yield('scripts')
	 
   <script>
      $(document).on("click",".myBtn",function() {
        $(this).parent('li').find(".myDropdown").toggleClass("show");
        if ($(this).find(".fa-minus").length > 0) {
            $(this).find("i").addClass("fa-plus").removeClass("fa-minus");
        } else {
            $(this).find("i").addClass("fa-minus").removeClass("fa-plus");
        }
      })

   </script>
</body>
</html>