<header class="bgheader_bgcolor">
  <div class="logo_leftbox">
    <div class="imageLogo">
      <img src="{{ asset('assets/images/logo.png') }}" alt="">
    </div>
  </div>

  <div class="logout_rightbox">

      <a href="{{ route('corporate-admin.logout') }}" onclick="event.preventDefault();
        	document.getElementById('logout-form').submit();">
       		<img src="{{ asset('assets/svg/power-off.svg') }}" alt="">
    	</a>
    	<form id="logout-form" action="{{ route('corporate-admin.logout') }}" method="POST" style="display: none;">
        	@csrf
    	</form>
  </div>
</header>

