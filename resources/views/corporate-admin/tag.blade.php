
@extends('corporate-admin.layouts.app')
@section('content')
 
  
		<div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Tags</p>
        </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Tag</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Tags</th>
            <th>Disable ?</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
          @foreach($tags as $tag)
          <tr>
            <th><?php echo $i++; ?></th>
            <th>{{$tag->tag}}</th>
            <th><a href="#" class="deletetag editdeletebtn" id="deletestaff"  data-id="{{$tag->id}}">
              @if($tag->is_deleted == 1)
                No
              @else
                Yes
              @endif
            </a></th>
            <th>
              <a href="#" class="editdeletebtn edittag" data-id="{{$tag->id}}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
              </th>
          </tr>

          @endforeach  
        </tbody>
      </table>
    </div>  
        <!--   edit model -->
       <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
              <div class="editsktoboxcover">
               <form method="post" action="{{route('corporate-admin.updatetag')}}" enctype="multipart/form-data" id="updatetag">
                @csrf            
                <div class="titlandclobtn2">
                  <h3>Edit Tag</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="hidden" name="id" id="tagid">
                    <input type="text" class="form-control"  name="tag" placeholder="Enter Tag" id="tagname">
                  </div>
                  <input type="button" name="" value="Update" class="btn" id="updatebtn">
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>  
    <!--     Eyes modal -->
    <div class="modal fade editskinboxmodal" id="myModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="editsktoboxcover">
             <form method="post" action="{{route('corporate-admin.addtag')}}" enctype="multipart/form-data" id="addtag">
              @csrf          
              <div class="titlandclobtn2">
                <h3>Add Tags</h3>
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
              </div>
              <div class="enterskintonebox">
                <div class="form-group">
                  <input type="text" class="form-control"  name="tag" placeholder="Enter Tag">
                </div>
                <input type="button" name="" value="Add" class="btn" id="btnSave">
              </div>              
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
@endsection
@section('scripts')
<script type="text/javascript">
  $("#btnSave").click(function(e){
     e.preventDefault()
    var $form = $("#addtag");
      
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (data, status) {
             if(data.error){
                return;
            }
            if(data.status==false){
              toastr.error(data.message);
            }
            else{
                  toastr.success('Tag Inserted Successfully');
                  $('#myModal').modal('hide');  
                  location.reload();
                }
        },
        error: function (result) {

        }
    });
    
 });
   
    $("#updatebtn").click(function(e){
     e.preventDefault()
    var $form = $("#updatetag");
      
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (data, status) {
             if(data.error){
                return;
            }
              if(data.status==false){
                toastr.error(data.message);
              }
              else{
                  toastr.success('Tag Updated Successfully');
                  $('#editModal').modal('hide'); 
                  location.reload();
            
        }
      },
        error: function (result) {

        }
    });
    
 });
</script>
<script type="text/javascript">
  $(document).on('click', '.edittag', function (e) {
  e.preventDefault();
  var id = $(this).data('id');
  
   $.ajax({
              type: "GET",
              url: "{{route('corporate-admin.edittag')}}",
              data: {id:id},
              success: function (data) {
                          $("#tagname").val(data);
                          $("#tagid").val(id);
                  }         
          }); 

   });
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.deletetag').click(function(){
              
              
                 var id = $(this).data('id');
                   
                    $.ajax({
                             type: "GET",
                             url: "{{route('corporate-admin.deletetag')}}",
                             data: {id:id},
                             success: function (data) {
                                    toastr.success(data.message);
                                    location.reload();  
                                 }         
                         }); 
              
             });
  });
</script>
<script type="text/javascript">
   $('.deletetag[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                container: 'body',
                onConfirm: function() {
                  var id = $(this).data('id');
                    
                     $.ajax({
                              type: "GET",
                              url: "{{route('corporate-admin.deletetag')}}",
                              data: {id:id},
                              success: function () {
                                     toastr.success('Tag Deleted Successfully');
                                                 location.reload();
                                  }         
                          }); 
                },
              });
  
</script>
<script type="text/javascript">
  $('#statstable').on('page.dt', function() {
    setTimeout(
      function() {
       $('.deletetag').click(function(){
                   
                   
                      var id = $(this).data('id');
                        
                         $.ajax({
                                  type: "GET",
                                  url: "{{route('corporate-admin.deletetag')}}",
                                  data: {id:id},
                                  success: function (data) {
                                         toastr.success(data.message);
                                         location.reload();  
                                      }         
                              }); 
                   
                  });
      }, 500);
  });
 
</script>
<script type="text/javascript">
  $('#statstable').on('order.dt', function() {
    setTimeout(
      function() {
        $('.deletetag').click(function(){
                    
                    
                       var id = $(this).data('id');
                         
                          $.ajax({
                                   type: "GET",
                                   url: "{{route('corporate-admin.deletetag')}}",
                                   data: {id:id},
                                   success: function (data) {
                                          toastr.success(data.message);
                                          location.reload();  
                                       }         
                               }); 
                    
                   });
      }, 500);
  });
 
</script>
@endsection