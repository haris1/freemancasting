@extends('corporate-admin.layouts.app')
@section('content')
 
  
    <div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Tattoos</p>
        </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Tattoo</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Tattoo</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
          @foreach($tattoo as $tat)
          <tr>
            <th><?php echo $i++; ?></th>
            <th>{{$tat->tattoo}}</th>
            
            <th>
              <a href="#" class="editdeletebtn edittattoo"  data-id="{{$tat->id}}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
              <a href="javascript:void(0)" class="deletetattoo editdeletebtn" id="deletestaff"  data-id="{{$tat->id}}" data-toggle="confirmation" data-title="are you sure?"><i class="fa fa-trash" aria-hidden="true"></i></a>
            </th>
          </tr>
       
          @endforeach  
        </tbody>
      </table>
    </div>
       <!--   edit model -->
       <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
              <div class="editsktoboxcover">
               <form method="post" action="{{route('corporate-admin.updatetattoo')}}" enctype="multipart/form-data" id="updatetattoo">
                @csrf            
                <div class="titlandclobtn2">
                  <h3>Edit Tattoo</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="hidden" name="id" id="editid">
                    <input type="text" class="form-control"  name="tattoo" placeholder="Enter Tattoo"  id="edittattooname">
                  </div>
                  <input type="button" name="" value="Update" class="btn" id="updatebtn">
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>    
      <!--     Tattoo modal -->
      <div class="modal fade editskinboxmodal" id="myModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
              <div class="editsktoboxcover">
               <form method="post" action="{{route('corporate-admin.addtattoo')}}" enctype="multipart/form-data" id="addtattoo">
                @csrf          
                <div class="titlandclobtn2">
                  <h3>Add Tattoo</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="text" class="form-control"  name="tattoo" placeholder="Enter Tattoo Name">
                  </div>
                  <input type="button" name="" value="Add" class="btn" id="btnSave">
                </div>              
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>
    @endsection
    @section('scripts')
    <script type="text/javascript">
      $("#btnSave").click(function(e){
         e.preventDefault()
        var $form = $("#addtattoo");
         
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function (data, status) {
                 if(data.error){
                    return;
                }

                if(data.status == false){
                  toastr.error(data.message);
                }
                else{
             toastr.success('Tattoo Inserted Successfully');
             $('#myModal').modal('hide');
             location.reload();  
           }
                
                
            },
            error: function (result) {

            }
        });
        
     });
     
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
       $('.deletetattoo[data-toggle=confirmation]').confirmation({
                    rootSelector: '[data-toggle=confirmation]',
                    container: 'body',
                    onConfirm: function() {
                      var id = $(this).data('id');
                        
                         $.ajax({
                                  type: "GET",
                                  url: "{{route('corporate-admin.deletetattoo')}}",
                                  data: {id:id},
                                  success: function () {
                                         toastr.success('Tattoo Deleted Successfully');
                                         location.reload();    
                                      }         
                              }); 
                    },
                  });
      });
    </script>
    <script type="text/javascript">
      $('#statstable').on('page.dt', function() {
        setTimeout(
          function() {
            $('.deletetattoo[data-toggle=confirmation]').confirmation({
                         rootSelector: '[data-toggle=confirmation]',
                         container: 'body',
                         onConfirm: function() {
                           var id = $(this).data('id');
                             
                              $.ajax({
                                       type: "GET",
                                       url: "{{route('corporate-admin.deletetattoo')}}",
                                       data: {id:id},
                                       success: function () {
                                              toastr.success('Tattoo Deleted Successfully');
                                              location.reload();    
                                           }         
                                   }); 
                         },
                       });
          }, 500);
      });
       
      $('#statstable').on('order.dt', function() {
        setTimeout(
          function() {
            $('.deletetattoo[data-toggle=confirmation]').confirmation({
                         rootSelector: '[data-toggle=confirmation]',
                         container: 'body',
                         onConfirm: function() {
                           var id = $(this).data('id');
                             
                              $.ajax({
                                       type: "GET",
                                       url: "{{route('corporate-admin.deletetattoo')}}",
                                       data: {id:id},
                                       success: function () {
                                              toastr.success('Tattoo Deleted Successfully');
                                              location.reload();    
                                           }         
                                   }); 
                         },
                       });
          }, 500);
      });
    </script>
    <script type="text/javascript">
      $(document).on('click', '.edittattoo', function (e) {
      e.preventDefault();
      var id = $(this).data('id');
      
       $.ajax({
                  type: "GET",
                  url: "{{route('corporate-admin.edittattoo')}}",
                  data: {id:id},
                  success: function (data) {
                              $("#edittattooname").val(data);
                        
                              $("#editid").val(id);
                      }         
              }); 

       });
    </script>
    <script type="text/javascript">
       $("#updatebtn").click(function(e){
          e.preventDefault()
         var form = $("#updatetattoo");
           
         $.ajax({
             type: form.attr('method'),
             url: form.attr('action'),
             data: form.serialize(),
             success: function (data, status) {
                  if(data.error){
                     return;
                 }
                 if(data.status == false){
                   toastr.error(data.message);
                 }
                 else{
                 $('#editModal').modal('hide');  // Your modal Id
                 toastr.success("Tattoo Updated Successfully");
                 location.reload();
               }
             },
             error: function (result) {

             }
         });
         
      });
    </script>
    @endsection