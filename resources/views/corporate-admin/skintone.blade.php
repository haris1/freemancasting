@extends('corporate-admin.layouts.app')
@section('content')
 
  
		<div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">       
          <p>Skin Tone</p>
        </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Skin Tone</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Skin Tone</th>
            <th>Disable ?</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody id="SkintonForm">
          <?php $i=1; ?>
          @foreach($skintone as $tone)
          <tr id="removeSkintone{{$tone->id}}">
            <th><?php echo $i++; ?></th>
            <th>{{$tone->skin_tone}}</th>            
            <th>
              <a href="#" class="editdeletebtn deletetone" id="deletestaff"  data-id="{{$tone->id}}">
                @if($tone->is_deleted == 1)
                  No
                @else
                  Yes
                @endif
              </a>
            </th>            
            <th>
              <a href="#" class="editdeletebtn edittone" data-toggle="modal" data-target="#editModal" data-id="{{$tone->id}}">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
              </a> 
             
            </th>
          </tr>
        
          @endforeach  
        </tbody>
      </table>
    </div>  	
    <!--   edit model -->
    <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-body">
           <div class="editsktoboxcover">
            <form method="post" action="{{route('corporate-admin.updatetone')}}" enctype="multipart/form-data" id="updatetone">
             @csrf            
             <div class="titlandclobtn2">
               <h3>Edit Skin Tone</h3>
               <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
             </div>
             <div class="enterskintonebox">
               <div class="form-group">
                 <input type="hidden" name="toneid" id="toneid">
                 <input type="text" class="form-control"  name="skintone" placeholder="Enter Skin Tone" value="{{$tone->skin_tone}}" id="tonename">
               </div>
               <input type="button" name="" value="Update" class="btn" id="updatebtn">
             </div>
           </form>
         </div>

       </div>
     </div>
   </div>
 </div>   
<!--     Skintone modal -->
<div class="modal fade editskinboxmodal" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="editsktoboxcover">
          <form method="post" action="{{route('corporate-admin.addtone')}}" enctype="multipart/form-data" id="addtone">
            @csrf          
            <div class="titlandclobtn2">
              <h3>Add Skin Tone</h3>
              <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
            </div>
            <div class="enterskintonebox">
              <div class="form-group">
                <input type="text" class="form-control"  name="skintone" placeholder="Enter Skin Tone">
              </div>
              <input type="button" name="" value="Add" class="btn" id="btnSave">
            </div>
          </form>
        </div> 
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
  $("#btnSave").click(function(e){
     e.preventDefault()
    var $form = $("#addtone");
      
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (data, status) {
             if(data.error){
                return;
            }
              if(data.status==false){
                toastr.error(data.message);
              }
              else{
             toastr.success("Skin Tone Added Successfully");
            $('#myModal').modal('hide');  
             location.reload();
           }
        },
        error: function (result) {

        }
    });
    
 });
  $("#updatebtn").click(function(e){
     e.preventDefault()
    var form = $("#updatetone");
      
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function (data, status) {
             if(data.error){
                return;
            }
            if(data.status==false){
              toastr.error(data.message);
            }else{
            $('#editModal').modal('hide');  
            toastr.success("Skin Tone Updated Successfully");
             location.reload();
           }
        },
        error: function (result) {

        }
    });
    
 });
  
    $(document).on('click', '.edittone', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
   
     $.ajax({
                type: "GET",
                url: "{{route('corporate-admin.edittone')}}",
                data: {id:id},
                success: function (data) {
                            $("#tonename").val(data);
                            $("#toneid").val(id);
                    }         
            }); 

     });
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.deletetone').click(function(){
              
              
                 var id = $(this).data('id');
                   
                    $.ajax({
                             type: "GET",
                             url: "{{route('corporate-admin.deletetone')}}",
                             data: {id:id},
                             success: function (data) {
                                    toastr.success(data.message);
                                    location.reload();  
                                 }         
                         }); 
              
             });
  });
</script>
<script type="text/javascript">
   $('.deletetone[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                container: 'body',
                onConfirm: function() {
                  var id = $(this).data('id');
                    
                     $.ajax({
                              type: "GET",
                              url: "{{route('corporate-admin.deletetone')}}",
                              data: {id:id},
                              success: function () {
                                   
                                    toastr.success("Skin Tone Deleted Successfully");
                                    location.reload();
                                  }         
                          }); 
                },
              });
  
</script>
<script type="text/javascript">
  $('#statstable').on('page.dt', function() {
    setTimeout(
      function() {
          $('.deletetone').click(function(){
                      
                      
                         var id = $(this).data('id');
                           
                            $.ajax({
                                     type: "GET",
                                     url: "{{route('corporate-admin.deletetone')}}",
                                     data: {id:id},
                                     success: function (data) {
                                            toastr.success(data.message);
                                            location.reload();  
                                         }         
                                 }); 
                      
                     });
      }, 500);
  });
 
</script>
<script type="text/javascript">
  $('#statstable').on('order.dt', function() {
    setTimeout(
      function() {
          $('.deletetone').click(function(){
                      
                      
                         var id = $(this).data('id');
                           
                            $.ajax({
                                     type: "GET",
                                     url: "{{route('corporate-admin.deletetone')}}",
                                     data: {id:id},
                                     success: function (data) {
                                            toastr.success(data.message);
                                            location.reload();  
                                         }         
                                 }); 
                      
                     });
      }, 500);
  });
 
</script>
@endsection