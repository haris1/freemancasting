@extends('corporate-admin.layouts.app')
@section('content')

	<form id="addhelpdesk" method="post" action="{{route('corporate-admin.addhelpdesk')}}" id="addhelpdesk">
		 @csrf 

		<div class="allhelpdesjkdeta">  
		<div class="signeup_topbox">
			<div class="signeup_lefttextbox">
				<p>Help Desk</p>
			</div>
		</div>        
			<div class="form-group">          
				<label>Portfolio Help Desk</label>
			  	<textarea name="portfolio_help" class="form-control" id="portfolio_help">{{isset($helpdesk->portfolio_help) ? $helpdesk->portfolio_help : '' }}</textarea >
			</div>

			<div class="form-group">          
				<label>Closet Help Desk</label>
			  	<textarea name="closet_help" class="form-control" id="closet_help">{{isset($helpdesk->closet_help) ? $helpdesk->closet_help : '' }}</textarea >
			</div>

			<div class="form-group">          
				<label>Skill Help Desk</label>
			  	<textarea name="skills_help" class="form-control" id="skills_help">{{isset($helpdesk->skills_help) ? $helpdesk->skills_help : '' }}</textarea >
			</div>

			<div class="form-group">          
				<label>Stat Help Desk</label>
			  	<textarea name="stats_help" class="form-control" id="stats_help">{{isset($helpdesk->stats_help) ? $helpdesk->stats_help : '' }}</textarea >
			</div>

			<div class="form-group">          
				<label>Prop Help Desk</label>
			  	<textarea name="props_help" class="form-control" id="props_help">{{isset($helpdesk->props_help) ? $helpdesk->props_help : ''}}</textarea >
			</div>

			<div class="form-group">          
				<label>Resume Help Desk</label>
			  	<textarea name="resume_help" class="form-control" id="resume_help">{{isset($helpdesk->resume_help) ? $helpdesk->resume_help : ''}}</textarea >
			</div>
			<div class="form-group">          
				<label>Document Help Desk</label>
			  	<textarea name="document_help" class="form-control" id="document_help">{{isset($helpdesk->document_help) ? $helpdesk->document_help : '' }}</textarea >
			</div>

			<div class="submitbtnbox">
				<input type="submit" value="Save" class="btn" id="btnSave">
			</div>
		</div>

	</form>
@endsection
@section('scripts')
<script type="text/javascript">
  $("#btnSave").click(function(e){
     e.preventDefault()
    var $form = $("#addhelpdesk");
     
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (data, status) {
             if(data.error){
                return;
            }
       

            if(data.status == false){
              toastr.error(data.message);
            }
            else{
         toastr.success('Help Desk Updated Successfully');
         
         location.reload();  
       }
            
            
        },
        error: function (result) {

        }
    });
    
 });
 
</script>
@endsection