@extends('corporate-admin.layouts.app')
@section('content')
  
    <div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Internal Staff</p>        
        </div>
        <div class="addmemberbtn"><a href="{{route('corporate-admin.form')}}" class="btn">Add Member</a></div>
      </div>
      

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
        <thead>
          <tr>
            <th>Sr</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
          @foreach($internalstaff as $staff)
          <tr id="staff{{$staff->id}}">
            <th><?php echo $i++; ?></th>
            <th>{{$staff->firstname}}</th>
            <th>{{$staff->lastname}}</th>
            <th>{{$staff->email}}</th>
            <th>{{$staff->phonenumber}}</th>
            <th>
              <a href="{{route('corporate-admin.edit',['id'=>$staff->id])}}" class="editdeletebtn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
              <a href="javascript:void(0)" class="editdeletebtn staffdelete" id="deletestaff"  data-id="{{$staff->id}}" data-toggle="confirmation" data-title="are you sure?"><i class="fa fa-trash" aria-hidden="true"></i></a>
              <a href="{{route('corporate-admin.status',['id'=>$staff->id])}}" class="disable_enablebtn">
              @if($staff->status=="enable")  
                  Disable
              @else
                  Enable    
              @endif    

            </a></th>
          </tr>

          @endforeach  
        </tbody>
      </table>
    </div>    
    
@endsection
  @section('scripts')
    <script type="text/javascript">
     
     $('.staffdelete[data-toggle=confirmation]').confirmation({
                  rootSelector: '[data-toggle=confirmation]',
                  container: 'body',
                  onConfirm: function() {
                     var id = $(this).data('id');
                     
                        $.ajax({
                                 type: "GET",
                                 url: "{{route('corporate-admin.delete')}}",
                                 data: {id:id},
                                 success: function () {                    
                                     $("#staff"+id).remove();
                                     toastr.success("Member Deleted Successfully");     
                                     
                                     }         
                             });
                  },
                }); 
    </script>
    <script type="text/javascript">
      $('#statstable').on('page.dt', function() {
        setTimeout(
          function() {
               $('.staffdelete[data-toggle=confirmation]').confirmation({
                            rootSelector: '[data-toggle=confirmation]',
                            container: 'body',
                            onConfirm: function() {
                               var id = $(this).data('id');
                               
                                  $.ajax({
                                           type: "GET",
                                           url: "{{route('corporate-admin.delete')}}",
                                           data: {id:id},
                                           success: function () {                    
                                               $("#staff"+id).remove();
                                               toastr.success("Member Deleted Successfully");     
                                               
                                               }         
                                       });
                            },
                          });
          }, 500);
      });
     
    </script>
    <script type="text/javascript">
      $('#statstable').on('order.dt', function() {
        setTimeout(
          function() {
               $('.staffdelete[data-toggle=confirmation]').confirmation({
                            rootSelector: '[data-toggle=confirmation]',
                            container: 'body',
                            onConfirm: function() {
                               var id = $(this).data('id');
                               
                                  $.ajax({
                                           type: "GET",
                                           url: "{{route('corporate-admin.delete')}}",
                                           data: {id:id},
                                           success: function () {                    
                                               $("#staff"+id).remove();
                                               toastr.success("Member Deleted Successfully");     
                                               
                                               }         
                                       });
                            },
                          });
          }, 500);
      });
     
    </script>
  @endsection
