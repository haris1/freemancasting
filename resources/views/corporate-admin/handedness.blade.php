@extends('corporate-admin.layouts.app')
@section('content')
 
  
		<div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Handedness</p>        
        </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Handedness</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Handedness</th>
            <th>Disable ?</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
          @foreach($handedness as $hand)
          <tr>
            <th><?php echo $i++; ?></th>
            <th>{{$hand->handedness}}</th>
            <th>  <a href="javascript:void(0);" class="deletehandedness editdeletebtn" id="deletestaff"  data-id="{{$hand->id}}">
              @if($hand->is_deleted == 1)
                No
              @else
                Yes 
              @endif
            </a></th>
            <th><a href="#" class="editdeletebtn edithandedness" data-id="{{$hand->id}}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
           </th>
          </tr>

          @endforeach  
        </tbody>
      </table>
    </div>  
       <!--   edit model -->
       <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
              <div class="editsktoboxcover">
               <form method="post" action="{{route('corporate-admin.updateHandedness')}}" enctype="multipart/form-data" id="updateHandedness">
                @csrf            
                <div class="titlandclobtn2">
                  <h3>Edit Handedness</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="hidden" name="id" id="handednessid">
                    <input type="text" class="form-control"  name="handedness" placeholder="Enter Handedness"  id="handednessname">
                  </div>
                  <input type="button" name="" value="Update" class="btn" id="updatebtn">
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>    	
    <!--     handedness modal -->
    <div class="modal fade editskinboxmodal" id="myModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="editsktoboxcover">
              <form method="post" action="{{route('corporate-admin.addhandedness')}}" enctype="multipart/form-data" id="addhandedness">
              @csrf          
                <div class="titlandclobtn2">
                  <h3>Add Handedness</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="text" class="form-control"  name="handedness" placeholder="Enter Handedness">
                  </div>
                  <input type="button" name="" value="Add" class="btn" id="btnSave">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
  $("#btnSave").click(function(e){
     e.preventDefault()
    var $form = $("#addhandedness");
      
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (data, status) {
             if(data.error){
                return;
            }
            if(data.status==false){
              toastr.error(data.message);
            }
            else{
                toastr.success('Handedness Inserted Successfully');
                $('#myModal').modal('hide');  
                location.reload();
                }
        },
        error: function (result) {

        }
    });
    
 });
 
</script>
<script type="text/javascript">
  $(document).on('click', '.edithandedness', function (e) {
  e.preventDefault();
  var id = $(this).data('id');
  
   $.ajax({
              type: "GET",
              url: "{{route('corporate-admin.edithandedness')}}",
              data: {id:id},
              success: function (data) {
                          $("#handednessname").val(data);
                          $("#handednessid").val(id);
                  }         
          }); 

   });
</script>
<script type="text/javascript">
   $("#updatebtn").click(function(e){
      e.preventDefault()
     var form = $("#updateHandedness");
       
     $.ajax({
         type: form.attr('method'),
         url: form.attr('action'),
         data: form.serialize(),
         success: function (data, status) {
              if(data.error){
                 return;
             }
              if(data.status==false){
                toastr.error(data.message);
              }
              else{
             $('#editModal').modal('hide');  
             toastr.success("Handedness Updated Successfully");
             location.reload();
           }
         },
         error: function (result) {

         }
     });
     
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.deletehandedness').click(function(){
              
              
                 var id = $(this).data('id');
                   
                    $.ajax({
                             type: "GET",
                             url: "{{route('corporate-admin.deletehandedness')}}",
                             data: {id:id},
                             success: function (data) {
                                    toastr.success(data.message);
                                    location.reload();  
                                 }         
                         }); 
              
             });
  });
</script>
<script type="text/javascript">
   $('.deletehandedness[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                container: 'body',
                onConfirm: function() {
                  var id = $(this).data('id');
                    
                     $.ajax({
                              type: "GET",
                              url: "{{route('corporate-admin.deletehandedness')}}",
                              data: {id:id},
                              success: function () {
                                     toastr.success('Handedness Deleted Successfully');
                                     location.reload();    
                                  }         
                          }); 
                },
              });
  
</script>
<script type="text/javascript">
  $('#statstable').on('page.dt', function() {
    setTimeout(
      function() {
        $('.deletehandedness').click(function(){
                    
                    
                       var id = $(this).data('id');
                         
                          $.ajax({
                                   type: "GET",
                                   url: "{{route('corporate-admin.deletehandedness')}}",
                                   data: {id:id},
                                   success: function (data) {
                                          toastr.success(data.message);
                                          location.reload();  
                                       }         
                               }); 
                    
                   });
      }, 500);
  });
   
  $('#statstable').on('order.dt', function() {
    setTimeout(
      function() {
        $('.deletehandedness').click(function(){
                    
                    
                       var id = $(this).data('id');
                         
                          $.ajax({
                                   type: "GET",
                                   url: "{{route('corporate-admin.deletehandedness')}}",
                                   data: {id:id},
                                   success: function (data) {
                                          toastr.success(data.message);
                                          location.reload();  
                                       }         
                               }); 
                    
                   });
      }, 500);
  });
</script>
@endsection