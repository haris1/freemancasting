@extends('corporate-admin.layouts.app')
@section('content')
 
  
    <div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Piercing</p>
        </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Piercing</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Piercing</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
          @foreach($piercing as $pier)
          <tr>
            <th><?php echo $i++; ?></th>
            <th>{{$pier->piercing}}</th>
            
            <th>
              <a href="#" class="editdeletebtn editpiercing"  data-id="{{$pier->id}}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
              <a href="javascript:void(0)" class="deletepiercing editdeletebtn" id="deletestaff"  data-id="{{$pier->id}}" data-toggle="confirmation" data-title="are you sure?"><i class="fa fa-trash" aria-hidden="true"></i></a>
            </th>
          </tr>
       
          @endforeach  
        </tbody>
      </table>
    </div>
         <!--   edit model -->
         <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <div class="editsktoboxcover">
                 <form method="post" action="{{route('corporate-admin.updatepiercing')}}" enctype="multipart/form-data" id="updatepiercing">
                  @csrf            
                  <div class="titlandclobtn2">
                    <h3>Edit Piercing</h3>
                    <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                  </div>
                  <div class="enterskintonebox">
                    <div class="form-group">
                      <input type="hidden" name="id" id="editid">
                      <input type="text" class="form-control"  name="piercing" placeholder="Enter Piercing"  id="editpiercingname">
                    </div>
                    <input type="button" name="" value="Update" class="btn" id="updatebtn">
                  </div>
                </form>
              </div>

            </div>
          </div>
        </div>
      </div>    
      <!--     Piercing modal -->
      <div class="modal fade editskinboxmodal" id="myModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
              <div class="editsktoboxcover">
               <form method="post" action="{{route('corporate-admin.addpiercing')}}" enctype="multipart/form-data" id="addpiercing">
                @csrf          
                <div class="titlandclobtn2">
                  <h3>Add Piercing</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="text" class="form-control"  name="piercing" placeholder="Enter Piercing">
                  </div>
                  <input type="button" name="" value="Add" class="btn" id="btnSave">
                </div>              
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>

    @endsection
    @section('scripts')
    <script type="text/javascript">
      $("#btnSave").click(function(e){
         e.preventDefault()
        var $form = $("#addpiercing");
         
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function (data, status) {
                 if(data.error){
                    return;
                }
                if(data.status == false){
                  toastr.error(data.message);
                }
                else{
             toastr.success('Piercing Inserted Successfully');
             $('#myModal').modal('hide');
             location.reload();  
           }
                
                
            },
            error: function (result) {

            }
        });
        
     });
     
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
       $('.deletepiercing[data-toggle=confirmation]').confirmation({
                    rootSelector: '[data-toggle=confirmation]',
                    container: 'body',
                    onConfirm: function() {
                      var id = $(this).data('id');
                        
                         $.ajax({
                                  type: "GET",
                                  url: "{{route('corporate-admin.deletepiercing')}}",
                                  data: {id:id},
                                  success: function () {
                                         toastr.success('Piercing Deleted Successfully');
                                         location.reload();    
                                      }         
                              }); 
                    },
                  });
      });
    </script>
    <script type="text/javascript">
      $('#statstable').on('page.dt', function() {
        setTimeout(
          function() {
            $('.deletepiercing[data-toggle=confirmation]').confirmation({
                         rootSelector: '[data-toggle=confirmation]',
                         container: 'body',
                         onConfirm: function() {
                           var id = $(this).data('id');
                             
                              $.ajax({
                                       type: "GET",
                                       url: "{{route('corporate-admin.deletepiercing')}}",
                                       data: {id:id},
                                       success: function () {
                                              toastr.success('Piercing Deleted Successfully');
                                              location.reload();    
                                           }         
                                   }); 
                         },
                       });
          }, 500);
      });
       
      $('#statstable').on('order.dt', function() {
        setTimeout(
          function() {
            $('.deletepiercing[data-toggle=confirmation]').confirmation({
                         rootSelector: '[data-toggle=confirmation]',
                         container: 'body',
                         onConfirm: function() {
                           var id = $(this).data('id');
                             
                              $.ajax({
                                       type: "GET",
                                       url: "{{route('corporate-admin.deletepiercing')}}",
                                       data: {id:id},
                                       success: function () {
                                              toastr.success('Piercing Deleted Successfully');
                                              location.reload();    
                                           }         
                                   }); 
                         },
                       });
          }, 500);
      });
    </script>
    <script type="text/javascript">
      $(document).on('click', '.editpiercing', function (e) {
      e.preventDefault();
      var id = $(this).data('id');
      
       $.ajax({
                  type: "GET",
                  url: "{{route('corporate-admin.editpiercing')}}",
                  data: {id:id},
                  success: function (data) {
                              $("#editpiercingname").val(data);
                        
                              $("#editid").val(id);
                      }         
              }); 

       });
    </script>
    <script type="text/javascript">
       $("#updatebtn").click(function(e){
          e.preventDefault()
         var form = $("#updatepiercing");
           
         $.ajax({
             type: form.attr('method'),
             url: form.attr('action'),
             data: form.serialize(),
             success: function (data, status) {
                  if(data.error){
                     return;
                 }
                 if(data.status == false){
                   toastr.error(data.message);
                 }
                 else{
                 $('#editModal').modal('hide');  // Your modal Id
                 toastr.success("Piercing Updated Successfully");
                 location.reload();
               }
             },
             error: function (result) {

             }
         });
         
      });
    </script>
    @endsection