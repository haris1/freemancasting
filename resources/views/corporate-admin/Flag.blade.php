@extends('corporate-admin.layouts.app')
@section('content')
 
  
		<div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Flag</p>
        </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Flag</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Flag Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <?php  $i=1; ?>
        <tbody>
        	@foreach($flags as $flag)
        	<tr>
        	  <th><?php echo $i++; ?></th>
        	  <th>{{$flag->flag_name}}</th>
        	  
        	  <th>
        	    <a href="#" class="editdeletebtn editflag" data-toggle="modal" data-target="#editModal" data-id="{{$flag->id}}">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
              </a> 

              @if(isset($flag->flag_count))
              @if($flag->flag_count <= 0)
        	    <a href="#0" class="deleteflag editdeletebtn" id="deletestaff"  data-id="{{$flag->id}}" data-toggle="confirmation" data-title="are you sure?"><i class="fa fa-trash" aria-hidden="true" id="statstable"></i></a>
              @endif
              @endif

            </th>
        	</tr>
        	   <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
        	    <div class="modal-dialog">
        	      <div class="modal-content">
        	        <div class="modal-body">
        	          <div class="editsktoboxcover">
        	           <form method="post" action="{{route('corporate-admin.updateflag')}}" enctype="multipart/form-data" id="updateflag">
        	            @csrf            
        	            <div class="titlandclobtn2">
        	              <h3>Edit Flag</h3>
        	              <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
        	            </div>
        	            <div class="enterskintonebox">
        	              <div class="form-group">
        	                <input type="hidden" name="flagid" id="flagid">
        	                <input type="text" class="form-control"  name="flagname" placeholder="Enter Flag" value="{{$flag->flag_name}}" id="flagname">
        	              </div>
        	              <input type="button" name="" value="Update" class="btn" id="updatebtn">
        	            </div>
        	          </form>
        	        </div>

        	      </div>
        	    </div>
        	  </div>
        	</div>
        	@endforeach  
       </tbody>
      </table>
    </div> 
    <!--     Flag modal -->
    <div class="modal fade editskinboxmodal" id="myModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="editsktoboxcover">
              <form method="post" action="{{route('corporate-admin.addflag')}}" enctype="multipart/form-data" id="addflag">
                @csrf          
                <div class="titlandclobtn2">
                  <h3>Add Flag</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="text" class="form-control"  name="flag_name" placeholder="Enter flag">
                  </div>
                  <input type="button" name="" value="Add" class="btn" id="btnSave">
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>
    </div>	
 @endsection
 @section('scripts')
 <script type="text/javascript">
   $("#btnSave").click(function(e){
      e.preventDefault()
     var form = $("#addflag");
       
     $.ajax({
         type: form.attr('method'),
         url: form.attr('action'),
         data: form.serialize(),
         success: function (data, status) {
              if(data.error){
                 return;
             }
             if(data.status==false)
             {
                toastr.error(data.message);
             }
             else{
          toastr.success('Flag Added Successfully');
             $('#myModal').modal('hide');  
             location.reload();
           }
         },
         error: function (result) {

         }
     });
     
  });
</script>

<script type="text/javascript">
	$(document).on('click', '.editflag', function (e) {
	e.preventDefault();
	var id = $(this).data('id');
	
	 $.ajax({
	            type: "GET",
	            url: "{{route('corporate-admin.editflag')}}",
	            data: {id:id},
	            success: function (data) {
	                        $("#flagname").val(data);
	                        $("#flagid").val(id);
	                }         
	        }); 

	 });
</script>
<script type="text/javascript">
	 $("#updatebtn").click(function(e){
	    e.preventDefault()
	   var form = $("#updateflag");
	     
	   $.ajax({
	       type: form.attr('method'),
	       url: form.attr('action'),
	       data: form.serialize(),
	       success: function (data, status) {
	            if(data.error){
	               return;
	           }
             if(data.status==false)
             {
                toastr.error(data.message);
             }
             else{
             toastr.success('Flag Updated Successfully');
	        // THis is success message
	           $('#editModal').modal('hide');  // Your modal Id
              location.reload();
            }
	       },
	       error: function (result) {

	       }
	   });
	   
	});
</script>
<script type="text/javascript">
   $('.deleteflag[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                container: 'body',
                onConfirm: function() {
                     var id = $(this).data('id');
                       
                        $.ajax({
                                 type: "GET",
                                 url: "{{route('corporate-admin.deleteflags')}}",
                                 data: {id:id},
                                 success: function () {
                                        toastr.success('Flag Deleted Successfully');
                                        location.reload();     
                                     }         
                             }); 
                },
              });
  
</script>
<script type="text/javascript">
  $('#statstable').on('page.dt', function() {
    setTimeout(
      function() {
        $('.deleteflag[data-toggle=confirmation]').confirmation({
                     rootSelector: '[data-toggle=confirmation]',
                     container: 'body',
                     onConfirm: function() {
                          var id = $(this).data('id');
                            
                             $.ajax({
                                      type: "GET",
                                      url: "{{route('corporate-admin.deleteflags')}}",
                                      data: {id:id},
                                      success: function () {
                                             toastr.success('Flag Deleted Successfully');
                                             location.reload();     
                                          }         
                                  }); 
                     },
                   });
      }, 500);
  });
 
</script>
<script type="text/javascript">
  $('#statstable').on('order.dt', function() {
    setTimeout(
      function() {
        $('.deleteflag[data-toggle=confirmation]').confirmation({
                     rootSelector: '[data-toggle=confirmation]',
                     container: 'body',
                     onConfirm: function() {
                          var id = $(this).data('id');
                            
                             $.ajax({
                                      type: "GET",
                                      url: "{{route('corporate-admin.deleteflags')}}",
                                      data: {id:id},
                                      success: function () {
                                             toastr.success('Flag Deleted Successfully');
                                             location.reload();     
                                          }         
                                  }); 
                     },
                   });
      }, 500);
  });
 
</script>
@endsection   