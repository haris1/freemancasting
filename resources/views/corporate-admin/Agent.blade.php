@extends('corporate-admin.layouts.app')
@section('content')
		<div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Agent</p>
        </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Agent</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Agent Name</th>
            <th>Agency Name</th>
            <th>Disable ?</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
        	@foreach($performeragent as $agent)
        	<tr>
        	  <th><?php echo $i++; ?></th>
        	  <th>{{isset($agent->agent_name)?$agent->agent_name:''}}</th>
        	  <th>{{isset($agent->agencies->agencyName)?$agent->agencies->agencyName:''}}</th>
            <th><a href="javascript:void(0)" class="deleteagent editdeletebtn" id="deletestaff"  data-id="{{$agent->id}}">
              @if($agent->is_deleted == 1)
                No
              @else
                Yes
              @endif
            </a></th>
        	  <th>

        	    <a href="#" class="editdeletebtn editagent" data-toggle="modal" data-target="#editModal" data-id="{{$agent->id}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
        	   </th>

        	</tr>
            

        	@endforeach  
        </tbody>
      </table>
    </div> 
       <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
              <div class="editsktoboxcover">
               <form method="post" action="{{route('corporate-admin.updateagent')}}" enctype="multipart/form-data" id="updateagent">
                @csrf            
                <div class="titlandclobtn2">
                  <h3>Edit Agent</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  
                    <input type="hidden" name="agentid" id="agentid">
                    <div class="form-group">
                      <input type="text" class="form-control"  name="agent_name" placeholder="Enter Agent" id="agentname">
                    </div>
                    <div class="selDiv seletcustboxpart">

                      <select required class="selectField" id="agency_id" name="agency_id">
                        <option></option>

                        @foreach ($agency as $val)
                          <option value="{{ $val->id }}">{{ $val->agencyName}}</option>
                        @endforeach
                      </select>
                      
                    </div>
                    <div class="AddeditAgency">
                      <a href="javascript:void(0)" id="openeditmodel" data-toggle="modal" data-target="#AgencyModal">Add Agency</a>
                    </div>
                  
                  <input type="button" name="" value="Update" class="btn" id="updatebtn">
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>	
    <!--     Agent modal -->
    <div class="modal fade editskinboxmodal" id="myModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="editsktoboxcover">
              <form method="post" action="{{route('corporate-admin.addagent')}}" enctype="multipart/form-data" id="addagent">
                @csrf          
                <div class="titlandclobtn2">
                  <h3>Add Agent</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="text" class="form-control"  name="agent_name" placeholder="Enter Agent">
                  </div>
                  <div class="seletcustboxpart">

                    <select required class="selectField" id="addagency_id" name="agency_id">
                      <option></option>

                      @foreach ($agency as $val)
                        <option value="{{ $val->id }}">{{ $val->agencyName}}</option>
                      @endforeach
                    </select>                 
                    
                  </div>
                  <div class="AddeditAgency">
                    <a href="javascript:void(0)" id="openModel" data-toggle="modal" data-target="#AgencyModal">Add Agency</a>
                  </div>
                  <input type="button" name="" value="Add" class="btn" id="btnSave">
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>
    </div>
    <!--     Agency Modal -->
    <div class="modal fade editskinboxmodal" id="AgencyModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="editsktoboxcover">
              <form method="post" action="{{route('corporate-admin.addagent')}}" enctype="multipart/form-data" id="addagent">
                @csrf          
                <div class="titlandclobtn2">
                  <h3>Add Agency</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="text" class="form-control"  name="agencyName" placeholder="Enter Agency" id="addAgencyName">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control"  name="agencyAbbreviation" placeholder="Enter Abbreviation" id="addabbreviation">

                  </div>
                  <input type="button" name="" value="Add Agency" class="btn" id="saveAgency">
                  <input type="button" name="" value="Back" id="cancelAgency" class="btn">
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>
    </div>
@endsection
 @section('scripts')
 <script type="text/javascript">
   $("#btnSave").click(function(e){
      e.preventDefault()
     var form = $("#addagent");
       
     $.ajax({
         type: form.attr('method'),
         url: form.attr('action'),
         data: form.serialize(),
         success: function (data, status) {
              if(data.error){
                 return;
             }
             if(data.status==false){
                toastr.error(data.message);
             }
             else{
                  toastr.success('Agent Added Successfully');
                  $('#myModal').modal('hide');  // Your modal Id
                  location.reload();
                }
         },
         error: function (result) {

         }
     });
     
  });
</script>
<script type="text/javascript">
  $(document).on('click', '.editagent', function (e) {
  e.preventDefault();
  var id = $(this).data('id');
  
   $.ajax({
              type: "GET",
              url: "{{route('corporate-admin.editagent')}}",
              data: {id:id},
              success: function (data) {
                          $("#agentname").val(data.agent_name);
                        //  $('.selDiv option[value="'.data.agency_id.'"]').prop('selected', true);
                        //  $("#agencyid").val(data.agency_id);
                       // $('#agency_id').select2('val',data.agency_id);
                        $("#agency_id").val(data.agency_id).trigger('change');
                          $("#agentid").val(id);
                  }         
          }); 

   });
</script>
<script type="text/javascript">
   $("#updatebtn").click(function(e){
      e.preventDefault()
     var form = $("#updateagent");
       
     $.ajax({
         type: form.attr('method'),
         url: form.attr('action'),
         data: form.serialize(),
         success: function (data, status) {
              if(data.error){
                 return;
             }
             if(data.status==false){
                toastr.error(data.message);
             }
             else{
            toastr.success('Agent Updated Successfully');
             $('#editModal').modal('hide'); 
              location.reload();
            }
         },
         error: function (result) {

         }
     });
     
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.deleteagent').click(function(){
              
              
                 var id = $(this).data('id');
                   
                    $.ajax({
                             type: "GET",
                             url: "{{route('corporate-admin.deleteagent')}}",
                             data: {id:id},
                             success: function (data) {
                                    toastr.success(data.message);
                                    location.reload();    
                                 }         
                         }); 
              
             });
  });
</script>
<script type="text/javascript">

  $(document).on('click', '.deleteagent', function (e) {
  $('.deleteagent[data-toggle=confirmation]').confirmation({
           rootSelector: '[data-toggle=confirmation]',
           container: 'body',
           onConfirm: function() {
              var id = $(this).data('id');
                
                 $.ajax({
                          type: "GET",
                          url: "{{route('corporate-admin.deleteagent')}}",
                          data: {id:id},
                          success: function (data) {
                               
                                toastr.success("Agent Deleted Successfully");
                                location.reload();
                              }         
                      }); 
           },
         });
   });

</script>
<script type="text/javascript">
  $(document).ready(function(){
  jQuery('#addagency_id').select2({
    placeholder:'Select Agency',
  });
  jQuery('#agency_id').select2({
    placeholder:'Select Agency',
  });
  });
</script>
<script type="text/javascript">
  $('#statstable').on('page.dt', function() {
    setTimeout(
      function() {
       $('.deleteagent').click(function(){
                   
                   
                      var id = $(this).data('id');
                        
                         $.ajax({
                                  type: "GET",
                                  url: "{{route('corporate-admin.deleteagent')}}",
                                  data: {id:id},
                                  success: function (data) {
                                         toastr.success(data.message);
                                         location.reload();    
                                      }         
                              }); 
                   
                  });
      }, 500);
  });
 
</script>
<script type="text/javascript">
  $('#statstable').on('order.dt', function() {
    setTimeout(
      function() {
       $('.deleteagent').click(function(){
                   
                   
                      var id = $(this).data('id');
                        
                         $.ajax({
                                  type: "GET",
                                  url: "{{route('corporate-admin.deleteagent')}}",
                                  data: {id:id},
                                  success: function (data) {
                                         toastr.success(data.message);
                                         location.reload();    
                                      }         
                              }); 
                   
                  });
      }, 500);
  });
 
</script>
<script type="text/javascript">
  var i=0;
  $('#openModel').click(function(){
    $('#addAgencyName').val('');
    $('#addabbreviation').val('');
    $('#myModal').modal('hide');
   i=0;  
 
  });
  $('#openeditmodel').click(function(){
    $('#addAgencyName').val('');
    $('#addabbreviation').val('');
    $('#editModal').modal('hide');
    i=1;
     
  });
</script>
<script type="text/javascript">
  $('#saveAgency').click(function(){
    
        $.ajax({

          type: "POST",
          url: "{{route('corporate-admin.addagency')}}",
          headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
          data: {
                  agencyName:$('#addAgencyName').val(),
                  agencyAbbreviation:$('#addabbreviation').val()
                },
          success: function (data) {
                 // console.log(data);
                 if(data.status==false){
                    toastr.error(data.message);
                 }
                 else{
                    toastr.success(data.message);
                    $('#AgencyModal').modal('hide');
                 // $("#myModal").val(null).trigger("change");
                   
                    
                    if(i==0){
                      
                      $('#addagency_id').append('<option value="'+ data.id +'">'+ $('#addAgencyName').val() +'</option>');
                      $("#addagency_id").val(data.id).trigger('change');
                      $('#myModal').modal('show'); 

                    }
                    else{
                                           
                      $('#agency_id').append('<option value="'+ data.id +'">'+ $('#addAgencyName').val() +'</option>');
                      $("#agency_id").val(data.id).trigger('change');
                        $('#editModal').modal('show');
                       
                    }
                 }
                 
                 }         
      });
  });
</script>
<script type="text/javascript">
  $('#cancelAgency').click(function(){
     $('#addAgencyName').val('');
     $('#addabbreviation').val('');
     $('#AgencyModal').modal('hide');

  if(i==0){
     $('#myModal').modal('show');
     
  }
  else{
      $('#editModal').modal('show');
    
  }
  });
</script>
@endsection