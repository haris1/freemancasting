@extends('corporate-admin.layouts.app')
@section('content')
		<div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Union</p>
        </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Union</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Union</th>
            <th>Union Tag</th>
            <th>Disable ?</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
        	@foreach($PerformerUnion as $union)
        	<tr>
        	  <th><?php echo $i++; ?></th>
        	  <th>{{$union->union}}</th>
            <th>{{$union->union_tag}}</th>
        	  <th> <a href="javascript:void(0)" class="deleteunion editdeletebtn" id="deletestaff"  data-id="{{$union->id}}">
              @if($union->is_deleted == 1)
                No
              @else
                Yes
              @endif
            </a></th>
        	  
        	  <th>
        	   	    <a href="#" class="editdeletebtn editunion" data-toggle="modal" data-target="#editModal" data-id="{{$union->id}}">
        	           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        	         </a> 
        	 </th>
        	</tr>
        	   <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
        	    <div class="modal-dialog">
        	      <div class="modal-content">
        	        <div class="modal-body">
        	          <div class="editsktoboxcover">
        	           <form method="post" action="{{route('corporate-admin.updateunion')}}" enctype="multipart/form-data" id="updateunion">
        	            @csrf            
        	            <div class="titlandclobtn2">
        	              <h3>Edit Union</h3>
        	              <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
        	            </div>
        	            <div class="enterskintonebox">
        	              <div class="form-group">
        	                <input type="hidden" name="unionid" id="unionid">
        	                <input type="text" class="form-control"  name="union" placeholder="Enter Union" value="{{$union->union}}" id="unionname">
                        </div>
                        <div class="form-group">
        	                <input type="text" class="form-control"  name="union_tag" placeholder="Enter Tag" value="{{$union->union_tag}}" id="uniontag">
        	              </div>
        	              <input type="button" name="" value="Update" class="btn" id="updatebtn">
        	            </div>
        	          </form>
        	        </div>

        	      </div>
        	    </div>
        	  </div>
        	</div>
        	@endforeach  
        </tbody>
      </table>
    </div> 
    <!--     Union modal -->
    <div class="modal fade editskinboxmodal" id="myModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="editsktoboxcover">
              <form method="post" action="{{route('corporate-admin.addunion')}}" enctype="multipart/form-data" id="addunion">
                @csrf          
                <div class="titlandclobtn2">
                  <h3>Add Union</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="text" class="form-control"  name="union" placeholder="Enter Union" >
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control"  name="union_tag" placeholder="Enter Tag" >
                  </div>
                  <input type="button" name="" value="Add" class="btn" id="btnSave">
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>
    </div>	
@endsection
 @section('scripts')
 <script type="text/javascript">
   $("#btnSave").click(function(e){
      e.preventDefault()
     var form = $("#addunion");
       
     $.ajax({
         type: form.attr('method'),
         url: form.attr('action'),
         data: form.serialize(),
         success: function (data, status) {
              if(data.error){
                 return;
             }
             if(data.status==false){
                toastr.error(data.message);
             }
             else{
          toastr.success('Union Inserted Successfully');
             $('#myModal').modal('hide'); 
             location.reload();
           }
         },
         error: function (result) {

         }
     });
     
  });
</script>
<script type="text/javascript">
	
</script>
<script type="text/javascript">
	$(document).on('click', '.editunion', function (e) {
	e.preventDefault();
	var id = $(this).data('id');
	
	 $.ajax({
	            type: "GET",
	            url: "{{route('corporate-admin.editunion')}}",
	            data: {id:id},
	            success: function (data) {
	            				
	                         $("#unionname").val(data.union);
	                         $("#uniontag").val(data.union_tag);
	                         $("#unionid").val(id);
	                }         
	        }); 

	 });
</script>
<script type="text/javascript">
	 $("#updatebtn").click(function(e){
	    e.preventDefault()
	   var form = $("#updateunion");
	     
	   $.ajax({
	       type: form.attr('method'),
	       url: form.attr('action'),
	       data: form.serialize(),
	       success: function (data, status) {
	            if(data.error){
	               return;
	           }
             if(data.status==false){
                toastr.error(data.message);
             }
             else{
	             toastr.success('Union Updated Successfully');
	           $('#editModal').modal('hide');  
             location.reload();
           }
	       },
	       error: function (result) {

	       }
	   });
	   
	});
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.deleteunion').click(function(){
              
              
                 var id = $(this).data('id');
                   
                    $.ajax({
                             type: "GET",
                             url: "{{route('corporate-admin.deleteunion')}}",
                             data: {id:id},
                             success: function (data) {
                                    toastr.success(data.message);
                                    location.reload();    
                                 }         
                         }); 
              
             });
  });
</script>
<script type="text/javascript">
   $('.deleteunion[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                container: 'body',
                onConfirm: function() {
                  var id = $(this).data('id');
                    
                     $.ajax({
                              type: "GET",
                              url: "{{route('corporate-admin.deleteunion')}}",
                              data: {id:id},
                              success: function () {
                                  toastr.success('Union Deleted Successfully');   location.reload();     
                                  }         
                          }); 
                },
              });
  
</script>
<script type="text/javascript">
  $('#statstable').on('page.dt', function() {
    setTimeout(
      function() {
         $('.deleteunion').click(function(){
                     
                     
                        var id = $(this).data('id');
                          
                           $.ajax({
                                    type: "GET",
                                    url: "{{route('corporate-admin.deleteunion')}}",
                                    data: {id:id},
                                    success: function (data) {
                                           toastr.success(data.message);
                                           location.reload();    
                                        }         
                                }); 
                     
                    });
      }, 500);
  });
 
</script>
<script type="text/javascript">
  $('#statstable').on('order.dt', function() {
    setTimeout(
      function() {
        $('.deleteunion').click(function(){
                    
                    
                       var id = $(this).data('id');
                         
                          $.ajax({
                                   type: "GET",
                                   url: "{{route('corporate-admin.deleteunion')}}",
                                   data: {id:id},
                                   success: function (data) {
                                          toastr.success(data.message);
                                          location.reload();    
                                       }         
                               }); 
                    
                   });
      }, 500);
  });
 
</script>
@endsection   