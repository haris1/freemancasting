@extends('corporate-admin.layouts.app')
@section('content')

		<div class="formboxinform">
      @if($errors->any())

<div class="alert alert-danger" id="danger-alert">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Danger!</strong>{{$errors->first()}}
</div>

@endif
      <form method="post" action="{{route('corporate-admin.addstaff')}}" id="addstaff">
        @csrf
        <div class="form-group">
          <label for="exampleInputName">First Name</label>
          <input type="text" class="form-control" id="exampleInputName" placeholder="First Name" name="firstname">
        </div>
        <div class="form-group">
          <label for="exampleInputName">Last Name</label>
          <input type="text" class="form-control" id="exampleInputName" placeholder="Last Name" name="lastname">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Your Mail Address" name="email">
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
        </div>
        <div class="form-group">
          <label >Phone No:</label>
          <input type="text" class="form-control" placeholder="Enter your Phone Number" name="phonenumber" id="phone">
        </div>
        <div class="form-check custcheckboxmain">
          <label class="checkboxcust" for="exampleCheck1">Check me out
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <span class="checkmark"></span>
          </label>
          <!-- <input type="checkbox" class="form-check-input" id="exampleCheck1">
          <label class="form-check-label" for="exampleCheck1">Check me out</label> -->
        </div>
        <div class="subsavbtnbox">
          <input type="button" class="btn" id="btnSave" value="Submit">
        </div>
      </form>
    </div>
   
@endsection
@section('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>

<script type="text/javascript">

  $(function() {

  $('input[name="datefilter"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
  });
  
  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});
</script>

<script type="text/javascript">
  $(function() {

  $('input[name="datefilter"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
  });

  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});
</script>

<script type="text/javascript">
  $(document).ready(function() {
  

   

  var data = [
      {"sr":1,"date":"21-Nov-2018 @ 5:55 p.m","name":"Abc","page":" Abc","account_type":"Abc","city":"Xyz",'connected':'<img src="{{asset("assets/corporate-admin/images/table-instagram.png")}}"> ' ,'More':''},

      {"sr":2,"date":"22-Nov-2018 @ 5:55 p.m","name":"Abc","page":" Abc","account_type":"Abc","city":"Xyz",'connected':'<img src="{{asset("assets/corporate-admin/images/table-instagram.png")}}"> ' ,'More':''}
    ];
  
  
  function format (data) {
      return '<div class="details-container">'+
          '<table cellpadding="2" cellspacing="0" border="0" class="details-table">'+
              '<tr>'+
                  '<td class="title">Person ID:</td>'+
                  '<td>'+data.sr+'</td>'+
              '</tr>'+
              '<tr>'+
                  '<td class="title">Following:</td>'+
                  '<td>'+data.name+'</td>'+
                  '<td class="title">Followers:</td>'+
                  '<td>'+data.page+'</td>'+
              '</tr>'+
              '<tr>'+
                  '<td class="title">Photos:</td>'+
                  '<td>'+data.name+'</td>'+
                  '<td class="title">Videos:</td>'+
                  '<td>'+data.name+'</td>'+
              '</tr>'+
          '</table>'+
        '</div>';
  };
  
  var table = $('.datatables').DataTable({
    // Column definitions
    columns : [
      {data : 'sr'},
      {data : 'date'},
      {data : 'name'},
      {data : 'page'},
      {data : 'account_type'},
      {data : 'city'},
      {data : 'connected'},
      {
        className      : 'details-control',
        defaultContent : '',
        data           : null,
        orderable      : false
      },
    ],
    
    data : data,
    
    pagingType : 'full_numbers',
    
    // Localization
    language : {
      emptyTable     : 'Virhe! Haku epäonnistui.',
      zeroRecords    : 'Ei hakutuloksia.',
      thousands      : ',',
      processing     : 'Prosessoidaan...',
      loadingRecords : 'Ladataan...',
      infoEmpty      : 'Sivu 0 / 0',
      infoFiltered   : '(rajattu _MAX_ hakutuloksesta)',
      infoPostFix    : '',
      lengthMenu     : 'Rivien _MENU_',
      search         : 'Search:',
      paginate       : {
        // first    : 'Ensimmäinen',
        // last     : 'Viimeinen',
        // next     : 'Seuraava',
        // previous : 'Edellinen'
      },
      aria : {
        sortAscending  : ' aktivoi järjestääksesi sarake nousevasti',
        sortDescending : ' aktivoi järjestääksesi saraka laskevasti'
      }
    }
  });
 
  $('.datatables tbody').on('click', 'td.details-control', function () {
     var tr  = $(this).closest('tr'),
         row = table.row(tr);
    
     if (row.child.isShown()) {
       tr.next('tr').removeClass('details-row');
       row.child.hide();
       tr.removeClass('shown');
     }
     else {
       row.child(format(row.data())).show();
       tr.next('tr').addClass('details-row');
       tr.addClass('shown');
     }
  });
 
});

</script>
<script type="text/javascript">



  $("#btnSave").click(function(){
      
    var $form = $("#addstaff");
      
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (data, status) {
             if(data.error){
                return;
            }
            if(data.status==false){
              toastr.error(data.message);
            }
            else{
               toastr.success(data.message);
               window.location.href = "list";
            }
        },
        error: function (result) {

        }
    });
    
 });
 
</script>
@endsection