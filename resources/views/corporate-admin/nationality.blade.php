@extends('corporate-admin.layouts.app')
@section('content')
 
  
		<div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Nationality</p>
        </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Nationality</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Nationality</th>
            <th>Disable ?</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
          @foreach($nationality as $nation)
          <tr>
            <th><?php echo $i++; ?></th>
            <th>{{$nation->nationality}}</th>
            <th><a href="javascript:void(0);" class="deletenationality editdeletebtn" id="deletenationality"  data-id="{{$nation->id}}">
              @if($nation->is_deleted == 1)
                No
              @else
                Yes
              @endif
            </a></th>
            <th>
              <a href="#" class="editnationality editdeletebtn" data-id="{{$nation->id}}"  
              data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
             </th>
          </tr>
       
          @endforeach  
        </tbody>
      </table>
    </div>
        <!--   edit model -->
        <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
         <div class="modal-dialog">
           <div class="modal-content">
            <div class="modal-body">
              <div class="editsktoboxcover">
                <form method="post" action="{{route('corporate-admin.updatenationality')}}" enctype="multipart/form-data" id="updatenationality">
                 @csrf
                <div class="titlandclobtn2">
                  <h3>Edit Nationality</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>

                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="hidden" name="nationalityid" id="nationalityid">
                    <input type="text" class="form-control"  name="nationality" placeholder="Enter Nationality" value="{{$nation->nationality}}" id="nationalityname">
                  </div>
                  <input type="button" name="" value="Update" class="btn" id="updatebtn">
                </div>

              </form>
            </div>                 
          </div>
        </div>
      </div>
    </div>  	
    <!--     Nationality modal -->
    <div class="modal fade editskinboxmodal" id="myModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="editsktoboxcover">
              <form method="post" action="{{route('corporate-admin.addnationality')}}" enctype="multipart/form-data" id="addnationality">
                @csrf          
                <div class="titlandclobtn2">
                  <h3>Add Nationality</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="text" class="form-control"  name="nationality" placeholder="Enter Nationality">
                  </div>
                  <input type="button" name="" value="Add" class="btn" id="btnSave">
                </div>
              </form>
            </div>      
        </div>
      </div>
    </div>
  </div>
@endsection
@section('scripts')
<script type="text/javascript">
  $("#btnSave").click(function(e){
     e.preventDefault()
    var $form = $("#addnationality");
      
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (data, status) {
             if(data.error){
                return;
            }
            if(data.status==false){
              toastr.error(data.message);
            }
          else{
                toastr.success('Nationality Inserted Successfully');
                $('#myModal').modal('hide');  
                location.reload();
            }
        },
        error: function (result) {

        }
    });
    
 });
    $("#updatebtn").click(function(e){
     e.preventDefault()
    var $form = $("#updatenationality");
      
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (data, status) {
             if(data.error){
                return;
            }
               if(data.error){
                  return;
              }
              if(data.status==false){
                toastr.error(data.message);
              }else{
              toastr.success('Nationality Updated Successfully');
               $('#editModal').modal('hide'); 
              location.reload();
            }
              },
        error: function (result) {

        }
    });
    
 });
  
     $(document).on('click', '.editnationality', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
     $.ajax({
                type: "GET",
                url: "{{route('corporate-admin.editnationality')}}",
                data: {id:id},
                success: function (data) {
                            $("#nationalityname").val(data);
                            $("#nationalityid").val(id);
                    }         
            }); 

     });
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.deletenationality').click(function(){
              
              
                 var id = $(this).data('id');
                   
                    $.ajax({
                             type: "GET",
                             url: "{{route('corporate-admin.deletenationality')}}",
                             data: {id:id},
                             success: function (data) {
                                    toastr.success(data.message);
                                    location.reload();  
                                 }         
                         }); 
              
             });
  });
</script>
<script type="text/javascript">
   $('.deletenationality[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                container: 'body',
                onConfirm: function() {
                  var id = $(this).data('id');
                    
                     $.ajax({
                              type: "GET",
                              url: "{{route('corporate-admin.deletenationality')}}",
                              data: {id:id},
                              success: function () {
                                     toastr.success('Nationality Deleted Successfully');
                                     location.reload();   
                                  }         
                          }); 
                },
              });
  
</script>
<script type="text/javascript">
  $('#statstable').on('page.dt', function() {
    setTimeout(
      function() {
         $('.deletenationality').click(function(){
                     
                     
                        var id = $(this).data('id');
                          
                           $.ajax({
                                    type: "GET",
                                    url: "{{route('corporate-admin.deletenationality')}}",
                                    data: {id:id},
                                    success: function (data) {
                                           toastr.success(data.message);
                                           location.reload();  
                                        }         
                                }); 
                     
                    });
      }, 500);
  });
 
</script>
<script type="text/javascript">
  $('#statstable').on('order.dt', function() {
    setTimeout(
      function() {
          $('.deletenationality').click(function(){
                      
                      
                         var id = $(this).data('id');
                           
                            $.ajax({
                                     type: "GET",
                                     url: "{{route('corporate-admin.deletenationality')}}",
                                     data: {id:id},
                                     success: function (data) {
                                            toastr.success(data.message);
                                            location.reload();  
                                         }         
                                 }); 
                      
                     });
      }, 500);
  });
 
</script>
@endsection