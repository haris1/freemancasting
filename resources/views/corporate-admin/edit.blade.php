@extends('corporate-admin.layouts.app')
@section('content')
   <div class="formboxinform">
		<form method="post" action="{{route('corporate-admin.store')}}" id="editstaff">
      @csrf
      <input type="hidden" name="id" value="{{$internalstaff->id}}">
      <div class="form-group">
        <label for="exampleInputName">First Name</label>
        <input type="text" class="form-control" id="exampleInputName" placeholder="Name" name="firstname" value="{{$internalstaff->firstname}}">
      </div>
      <div class="form-group">
        <label for="exampleInputName">Last Name</label>
        <input type="text" class="form-control" id="exampleInputLastName" placeholder="Name" name="lastname" value="{{$internalstaff->lastname}}">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email" value="{{$internalstaff->email}}">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
      </div>
   
      <div class="form-group">
        <label for="exampleInputName">Phone No:</label>
        <input type="text" class="form-control" placeholder="Enter Your Phone Number" name="phonenumber" id="phone" value="{{$internalstaff->phonenumber}}" data-mask="(000) 000-0000">
      </div>
      <div class="subsavbtnbox">
        <input type="button" class="btn" id="btnSave" value="update">
      </div>
    </form>	
  </div>
   
@endsection
@section('scripts')
<script type="text/javascript">



  $("#btnSave").click(function(){
      
    var $form = $("#editstaff");
      
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (data, status) {
             if(data.error){
                return;
            }
            if(data.status==false){
              toastr.error(data.message);
            }
            else{
               toastr.success(data.message);
               window.location.href = "{{route('corporate-admin.list')}}"
            }
        },
        error: function (result) {

        }
    });
    
 });
 
</script>
@endsection