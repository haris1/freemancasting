@extends('corporate-admin.layouts.app')
@section('content')
 
  
		<div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Performer Availability</p>        
        </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Availability</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Availability</th>
            <th>Disable ?</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
          @foreach($availability as $available)
          <tr>
            <th><?php echo $i++; ?></th>
            <th>{{$available->availability}}</th>
            <th> <a href="javascript:void(0)" class="deleteavailability editdeletebtn" id="deletestaff"  data-id="{{$available->id}}">
              @if($available->is_deleted == 1)
                No
              @else
                Yes
              @endif
            </a>
            </th>
           
            <th><a href="javascript:void(0)" class="editdeletebtn editavailability" data-id="{{$available->id}}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
             </th>
          </tr>
            
          @endforeach  
        </tbody>
      </table>
    </div>   
       <!--   edit model -->
       <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-body">
              <div class="editsktoboxcover">
               <form method="post" action=" {{route('corporate-admin.updateavailability')}} " enctype="multipart/form-data" id="updatehaircolor">
                @csrf            
                <div class="titlandclobtn2">
                  <h3>Edit Performer Availability</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="hidden" name="id" id="editid">
                    <input type="text" class="form-control"  name="availability" placeholder="Enter Performer Availability" id="editname">
                  </div>
                  <input type="button" name="" value="Update" class="btn" id="updatebtn">
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>	
    <!--     Haircolor modal -->
    <div class="modal fade editskinboxmodal" id="myModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="editsktoboxcover">
              <form method="post" action="{{route('corporate-admin.addavailability')}}" enctype="multipart/form-data" id="addavailability">
                @csrf          
                <div class="titlandclobtn2">
                  <h3>Add Availability</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="text" class="form-control"  name="availability" placeholder="Enter Performer Availability">
                  </div>
                  <input type="button" name="" value="Add" class="btn" id="btnSave">
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>

@endsection
@section('scripts')
<script type="text/javascript">
  $("#btnSave").click(function(e){
     e.preventDefault()
    var $form = $("#addavailability");
      
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (data, status) {
             if(data.error){
                return;
            }
            if(data.status==false){
              toastr.error(data.message);
            }
            else{
                toastr.success('Performer Availability Added Successfully');
                $('#myModal').modal('hide');  
                location.reload();
              }
        },
        error: function (result) {

        }
    });
    
 });
   
    $(document).on('click', '.editavailability', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
   
     $.ajax({
                type: "GET",
                url: "{{route('corporate-admin.editavailability')}}",
                data: {id:id},
                success: function (data) {
                            $("#editname").val(data);
                            $("#editid").val(id);
                    }         
            }); 

     });
</script>
<script type="text/javascript">
   $("#updatebtn").click(function(e){
      e.preventDefault()
     var form = $("#updatehaircolor");
       
     $.ajax({
         type: form.attr('method'),
         url: form.attr('action'),
         data: form.serialize(),
         success: function (data, status) {
              if(data.error){
                 return;
             }
             if(data.status==false){
               toastr.error(data.message);
             }
             else{
          // THis is success message
             $('#editModal').modal('hide');  // Your modal Id
             toastr.success("Performer Availability Updated Successfully");
              location.reload();
            }
         },
         error: function (result) {

         }
     });
     
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.deleteavailability').click(function(){
              
              
                 var id = $(this).data('id');
                   
                    $.ajax({
                             type: "GET",
                             url: "{{route('corporate-admin.deleteavailability')}}",
                             data: {id:id},
                             success: function (data) {
                                    toastr.success(data.message);
                                    location.reload();    
                                 }         
                         }); 
              
             });
  });
</script>
<script type="text/javascript">
   $('.deleteavailability[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                container: 'body',
                onConfirm: function() {
                  var id = $(this).data('id');
                    
                     $.ajax({
                              type: "GET",
                              url: "{{route('corporate-admin.deleteavailability')}}",
                              data: {id:id},
                              success: function () {
                                      toastr.success('Availability Deleted Successfully');
                                      location.reload();   
                                  }         
                          }); 
                },
              });
  
</script>
<script type="text/javascript">
  $('#statstable').on('page.dt', function() {
    setTimeout(
      function() {
         $('.deleteavailability').click(function(){
                     
                     
                        var id = $(this).data('id');
                          
                           $.ajax({
                                    type: "GET",
                                    url: "{{route('corporate-admin.deleteavailability')}}",
                                    data: {id:id},
                                    success: function (data) {
                                           toastr.success(data.message);
                                           location.reload();    
                                        }         
                                }); 
                     
                    });
      }, 500);
  });
 
</script>
<script type="text/javascript">
  $('#statstable').on('order.dt', function() {
    setTimeout(
      function() {
         $('.deleteavailability').click(function(){
                     
                     
                        var id = $(this).data('id');
                          
                           $.ajax({
                                    type: "GET",
                                    url: "{{route('corporate-admin.deleteavailability')}}",
                                    data: {id:id},
                                    success: function (data) {
                                           toastr.success(data.message);
                                           location.reload();    
                                        }         
                                }); 
                     
                    });
      }, 500);
  });
 
</script>
@endsection