@extends('corporate-admin.layouts.app')
@section('content')
 
  
		<div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Eyes</p>
        </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Eyes</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Eyes</th>
            <th>Disable ?</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
          @foreach($eyes as $eye)
          <tr>
            <th><?php echo $i++; ?></th>
            <th>{{$eye->eyes}}</th>
            <th><a href="javascript:void(0)" class="deleteeyes editdeletebtn" id="deletestaff"  data-id="{{$eye->id}}">
                @if($eye->is_deleted == 1)
                  No
                @else
                  Yes
                @endif
              </a></th>
            <th>
              <a href="#" class="editdeletebtn editeyes"  data-id="{{$eye->id}}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
             </th>
          </tr>

          @endforeach  
        </tbody>
      </table>
    </div>  
       <!--   edit model -->
       <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
              <div class="editsktoboxcover">
               <form method="post" action="{{route('corporate-admin.updateeyes')}}" enctype="multipart/form-data" id="updateeyes">
                @csrf            
                <div class="titlandclobtn2">
                  <h3>Edit Eyes</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="hidden" name="eyesid" id="eyesid">
                    <input type="text" class="form-control"  name="eyes" placeholder="Enter Eyes"  id="eyesname">
                  </div>
                  <input type="button" name="" value="Update" class="btn" id="updatebtn">
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>   	
    <!--     Eyes modal -->
    <div class="modal fade editskinboxmodal" id="myModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="editsktoboxcover">
             <form method="post" action="{{route('corporate-admin.addeyes')}}" enctype="multipart/form-data" id="addeyes">
              @csrf          
              <div class="titlandclobtn2">
                <h3>Add Eyes</h3>
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
              </div>
              <div class="enterskintonebox">
                <div class="form-group">
                  <input type="text" class="form-control"  name="eyes" placeholder="Enter Eyes">
                </div>
                <input type="button" name="" value="Add" class="btn" id="btnSave">
              </div>              
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
@endsection
@section('scripts')
<script type="text/javascript">
  $("#btnSave").click(function(e){
     e.preventDefault()
    var $form = $("#addeyes");
     
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (data, status) {
             if(data.error){
                return;
            }
            if(data.status == false){
              toastr.error(data.message);
            }
            else{
         toastr.success('Eyes Added Successfully');
         $('#myModal').modal('hide');
         location.reload();  
       }
            
            
        },
        error: function (result) {

        }
    });
    
 });
 
</script>
<script type="text/javascript">
  $(document).on('click', '.editeyes', function (e) {
  e.preventDefault();
  var id = $(this).data('id');
  
   $.ajax({
              type: "GET",
              url: "{{route('corporate-admin.editeyes')}}",
              data: {id:id},
              success: function (data) {
                          $("#eyesname").val(data);
                          $("#eyesid").val(id);
                  }         
          }); 

   });
</script>
<script type="text/javascript">
   $("#updatebtn").click(function(e){
      e.preventDefault()
     var form = $("#updateeyes");
       
     $.ajax({
         type: form.attr('method'),
         url: form.attr('action'),
         data: form.serialize(),
         success: function (data, status) {
              if(data.error){
                 return;
             }
             if(data.status == false){
               toastr.error(data.message);
             }
             else{
             $('#editModal').modal('hide');  // Your modal Id
             toastr.success("Eyes Updated Successfully");
             location.reload();
           }
         },
         error: function (result) {

         }
     });
     
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.deleteeyes').click(function(){
              
              
                 var id = $(this).data('id');
                   
                    $.ajax({
                             type: "GET",
                             url: "{{route('corporate-admin.deleteeyes')}}",
                             data: {id:id},
                             success: function (data) {
                                    toastr.success(data.message);
                                    location.reload();    
                                 }         
                         }); 
              
             });
  });
</script>
<script type="text/javascript">
  $('#statstable').on('page.dt', function() {
    setTimeout(
      function() {
          $('.deleteeyes').click(function(){
                      
                      
                         var id = $(this).data('id');
                           
                            $.ajax({
                                     type: "GET",
                                     url: "{{route('corporate-admin.deleteeyes')}}",
                                     data: {id:id},
                                     success: function (data) {
                                            toastr.success(data.message);
                                            location.reload();    
                                         }         
                                 }); 
                      
                     });
      }, 500);
  });
   
  $('#statstable').on('order.dt', function() {
    setTimeout(
      function() {
          $('.deleteeyes').click(function(){
                      
                      
                         var id = $(this).data('id');
                           
                            $.ajax({
                                     type: "GET",
                                     url: "{{route('corporate-admin.deleteeyes')}}",
                                     data: {id:id},
                                     success: function (data) {
                                            toastr.success(data.message);
                                            location.reload();    
                                         }         
                                 }); 
                      
                     });
      }, 500);
  });
</script>
@endsection