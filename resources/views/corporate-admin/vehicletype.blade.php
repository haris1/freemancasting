@extends('corporate-admin.layouts.app')
@section('content')
 
  
		<div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Vehicle Type</p>
        </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Type</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Vehicle Type</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
          @foreach($vehicletype as $type)
          <tr>
            <th><?php echo $i++; ?></th>
            <th>{{$type->vehicle_type}}</th>
            
            <th>
              <a href="#" class="editdeletebtn editeyes"  data-id="{{$type->id}}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
              <a href="javascript:void(0)" class="deleteeyes editdeletebtn" id="deletestaff"  data-id="{{$type->id}}" data-toggle="confirmation" data-title="are you sure?"><i class="fa fa-trash" aria-hidden="true"></i></a></th>
          </tr>

          @endforeach  
        </tbody>
      </table>
    </div>  
        <!--   edit model -->
        <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
         <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-body">
               <div class="editsktoboxcover">
                <form method="post" action="{{route('corporate-admin.updatevehicletype')}}" enctype="multipart/form-data" id="updatevehicletype">
                 @csrf            
                 <div class="titlandclobtn2">
                   <h3>Edit Vehicle Type</h3>
                   <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                 </div>
                 <div class="enterskintonebox">
                   <div class="form-group">
                     <input type="hidden" name="id" id="editid">
                     <input type="text" class="form-control"  name="vehicle_type" placeholder="Enter Vehicle Type"  id="editname">
                   </div>
                   <input type="button" name="" value="Update" class="btn" id="updatebtn">
                 </div>
               </form>
             </div>

           </div>
         </div>
       </div>
     </div>   
    <!--     Skintone modal -->
    <div class="modal fade editskinboxmodal" id="myModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="editsktoboxcover">
              <form method="post" action="{{route('corporate-admin.addvehicletype')}}" enctype="multipart/form-data" id="addvehicletype">
                @csrf          
                <div class="titlandclobtn2">
                  <h3>Add Vehicle Type</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="text" class="form-control"  name="vehicle_type" placeholder="Enter Vehicle Make">
                  </div>
                  <input type="button" name="" value="Add" class="btn" id="btnSave">
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>
    </div>
    @endsection
    @section('scripts')
    <script type="text/javascript">
      $("#btnSave").click(function(e){
         e.preventDefault();
        var $form = $("#addvehicletype");
          
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function (data, status) {
                 if(data.error){
                    return;
                }
                  if(data.status==false){
                    toastr.error(data.message);
                  }
                  else{
                 toastr.success("Vehicle Make Added Successfully");;
                $('#myModal').modal('hide');  
                 location.reload();
               }
            },
            error: function (result) {

            }
        });
        
     });
      $("#updatebtn").click(function(e){
         e.preventDefault()
        var form = $("#updatevehiclemake");
          
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            success: function (data, status) {
                 if(data.error){
                    return;
                }
                if(data.status==false){
                  toastr.error(data.message);
                }else{
                $('#editModal').modal('hide');  
                toastr.success("Vehicle Make Updated Successfully");
                 location.reload();
               }
            },
            error: function (result) {

            }
        });
        
     });
      
        $(document).on('click', '.editvehiclemake', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
       
         $.ajax({
                    type: "GET",
                    url: "{{route('corporate-admin.editvehiclemake')}}",
                    data: {id:id},
                    success: function (data) {
                                $("#editname").val(data);
                                $("#editid").val(id);
                        }         
                }); 

         });
    </script>
    <script type="text/javascript">
       $('.deletevehiclemake[data-toggle=confirmation]').confirmation({
                    rootSelector: '[data-toggle=confirmation]',
                    container: 'body',
                    onConfirm: function() {
                      var id = $(this).data('id');
                        
                         $.ajax({
                                  type: "GET",
                                  url: "{{route('corporate-admin.deletevehiclemake')}}",
                                  data: {id:id},
                                  success: function () {
                                       
                                        toastr.success("Vehicle Make Deleted Successfully");
                                        location.reload();
                                      }         
                              }); 
                    },
                  });
      
    </script>
    <script type="text/javascript">
      $('#statstable').on('page.dt', function() {
        setTimeout(
          function() {
             $('.deletevehiclemake[data-toggle=confirmation]').confirmation({
                          rootSelector: '[data-toggle=confirmation]',
                          container: 'body',
                          onConfirm: function() {
                            var id = $(this).data('id');
                              
                               $.ajax({
                                        type: "GET",
                                        url: "{{route('corporate-admin.deletevehiclemake')}}",
                                        data: {id:id},
                                        success: function () {
                                             
                                              toastr.success("Vehicle Make Deleted Successfully");
                                              location.reload();
                                            }         
                                    }); 
                          },
                        });
          }, 500);
      });
     
    </script>
    <script type="text/javascript">
      $('#statstable').on('order.dt', function() {
        setTimeout(
          function() {
              $('.deletevehicleaccessories[data-toggle=confirmation]').confirmation({
                           rootSelector: '[data-toggle=confirmation]',
                           container: 'body',
                           onConfirm: function() {
                             var id = $(this).data('id');
                               
                                $.ajax({
                                         type: "GET",
                                         url: "{{route('corporate-admin.deletevehicleaccessories')}}",
                                         data: {id:id},
                                         success: function () {
                                              
                                               toastr.success("Vehicle Make Deleted Successfully");
                                               location.reload();
                                             }         
                                     }); 
                           },
                         });
          }, 500);
      });
     
    </script>
    @endsection      