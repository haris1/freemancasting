@extends('corporate-admin.layouts.app')
@section('content')
 
<div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Hair Style</p>        style   </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Hair Style</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Hair Style</th>
            <th>Disable ?</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
          @foreach($hairstyle as $style)
          <tr>
            <th><?php echo $i++; ?></th>
            <th>{{$style->hair_style}}</th>
            <th><a href="javascript:void(0)" class="deletehairstyle editdeletebtn" id="deletestaff"  data-id="{{$style->id}}" >
                  @if($style->is_deleted == 1)
                    No
                  @else
                    Yes
                  @endif              
            </a></th>
            <th>
              <a href="#" class="editdeletebtn edithairstyle" data-id="{{$style->id}}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
              
            </th>
          </tr>

          @endforeach  
        </tbody>
      </table>
    </div>    

       <!--   edit model -->
       <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
              <div class="editsktoboxcover">
               <form method="post" action="{{route('corporate-admin.updateHairstyle')}}" enctype="multipart/form-data" id="updateHairstyle">
                @csrf            
                <div class="titlandclobtn2">
                  <h3>Edit Hair Style</h3>
                  <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
                </div>
                <div class="enterskintonebox">
                  <div class="form-group">
                    <input type="hidden" name="id" id="editid">
                    <input type="text" class="form-control"  name="hair_style" placeholder="Enter Hair Style"  id="editname">
                  </div>
                     <input type="button" name="" value="Update" class="btn" id="updatebtn">
                   </div>
                 </form>
            </div>

          </div>
        </div>
      </div>
    </div>      
    <!--     hairstyle modal -->
    <div class="modal fade editskinboxmodal" id="myModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="editsktoboxcover">
             <form method="post" action="{{route('corporate-admin.addhairstyle')}}" enctype="multipart/form-data" id="addhairstyle">
              @csrf          
              <div class="titlandclobtn2">
                <h3>Add Hair Style</h3>
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
              </div>
              <div class="enterskintonebox">
                <div class="form-group">
                  <input type="text" class="form-control"  name="hairstyle" placeholder="Enter Hair Style">
                </div>
                <input type="button" name="" value="Add" class="btn" id="btnSave">
              </div>
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
@endsection
@section('scripts')
<script type="text/javascript">
  $("#btnSave").click(function(e){
     e.preventDefault()
    var $form = $("#addhairstyle");
      
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (data, status) {
             if(data.error){
                return;
            }
            if(data.status==false){
              toastr.error(data.message);
            }
            else{
                  toastr.success('Hair Style Inserted Successfully');
                  $('#myModal').modal('hide');  
                  location.reload();
          }
        },
        error: function (result) {

        }
    });
    
 });
  
</script>
<script type="text/javascript">
  $(document).on('click', '.edithairstyle', function (e) {
  e.preventDefault();
  var id = $(this).data('id');
  
   $.ajax({
              type: "GET",
              url: "{{route('corporate-admin.edithairstyle')}}",
              data: {id:id},
              success: function (data) {
                          $("#editname").val(data);
                          $("#editid").val(id);
                  }         
          }); 

   });
</script>
<script type="text/javascript">
   $("#updatebtn").click(function(e){
      e.preventDefault()
     var form = $("#updateHairstyle");
       
     $.ajax({
         type: form.attr('method'),
         url: form.attr('action'),
         data: form.serialize(),
         success: function (data, status) {
              if(data.error){
                 return;
             }
             if(data.status==false){
               toastr.error(data.message);
             }
             else{
          // location.reload();// THis is success message
             $('#editModal').modal('hide');  // Your modal Id
             toastr.success("Hair Style Updated Successfully");
             location.reload();
           }
         },
         error: function (result) {

         }
     });
     
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.deletehairstyle').click(function(){
              
              
                 var id = $(this).data('id');
                   
                    $.ajax({
                             type: "GET",
                             url: "{{route('corporate-admin.deletehairstyle')}}",
                             data: {id:id},
                             success: function (data) {
                                    toastr.success(data.message);
                                    location.reload();  
                                 }         
                         }); 
              
             });
  });
</script>
<script type="text/javascript">
   $('.deletehairstyle[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                container: 'body',
                onConfirm: function() {
                  var id = $(this).data('id');
                    
                     $.ajax({
                              type: "GET",
                              url: "{{route('corporate-admin.deletehairstyle')}}",
                              data: {id:id},
                              success: function () {
                                      toastr.success('Hair Length Deleted Successfully'); 
                                      location.reload();  
                                  }         
                          }); 
                },
              });
  
</script>
<script type="text/javascript">
  $('#statstable').on('page.dt', function() {
    setTimeout(
      function() {
           $('.deletehairstyle').click(function(){
                       
                       
                          var id = $(this).data('id');
                            
                             $.ajax({
                                      type: "GET",
                                      url: "{{route('corporate-admin.deletehairstyle')}}",
                                      data: {id:id},
                                      success: function (data) {
                                             toastr.success(data.message);
                                             location.reload();  
                                          }         
                                  }); 
                       
                      });
      }, 500);
  });
 
</script>
<script type="text/javascript">
  $('#statstable').on('order.dt', function() {
    setTimeout(
      function() {
           $('.deletehairstyle').click(function(){
                       
                       
                          var id = $(this).data('id');
                            
                             $.ajax({
                                      type: "GET",
                                      url: "{{route('corporate-admin.deletehairstyle')}}",
                                      data: {id:id},
                                      success: function (data) {
                                             toastr.success(data.message);
                                             location.reload();  
                                          }         
                                  }); 
                       
                      });
      }, 500);
  });
 
</script>
@endsection