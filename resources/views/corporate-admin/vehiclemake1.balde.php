<?php
echo "HIIIII";
exit();
?>

@extends('corporate-admin.layouts.app')
@section('content')  
		<div class="users_datatablebox">
      <div class="signeup_topbox">
        <div class="signeup_lefttextbox">
          <p>Vehicle Type</p>
        </div>
        <div class="addmemberbtn"><a data-toggle="modal" data-target="#myModal" class="btn">Add Type</a></div>
      </div>

      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Vehicle Make</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
          @foreach($vehiclemake as $make)
          <tr>
            <th><?php echo $i++; ?></th>
            <th>{{$make->vehicle_make}}</th>
            
            <th>
              <a href="#" class="editdeletebtn editeyes"  data-id="{{$make->id}}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
              <a href="javascript:void(0)" class="deleteeyes editdeletebtn" id="deletestaff"  data-id="{{$make->id}}" data-toggle="confirmation" data-title="are you sure?"><i class="fa fa-trash" aria-hidden="true"></i></a></th>
          </tr>

          @endforeach  
        </tbody>
      </table>
    </div>  
@endsection    