@extends('corporate-admin.layouts.app')
@section('content')
			<div class="users_datatablebox">
	      <div class="signeup_topbox">
	        <div class="signeup_lefttextbox">
	          <p>Agency</p>
	        </div>
	        <div class="addmemberbtn"> <a href="javascript:void(0)" id="openModel" data-toggle="modal" data-target="#AgencyModal" class="btn">Add Agency</a></div>
	      </div>

	      <table class="table table-striped table-hover datatables tablestaffint" style="width: 100%;" id="statstable">
	        <thead>
	          <tr>
	            <th>Sr</th>
	            <th>Agency Name</th>
              <th>Agency Abbreviation</th>
	            <th>Disable ?</th>
	            <th>Action</th>
	          </tr>
	        </thead>
	            <tbody>
	              <?php $i=1; ?>
	              @foreach($agency as $agen)
	              <tr>
	                <th><?php echo $i++; ?></th>
	                <th>{{$agen->agencyName}}</th>
                  <th>{{$agen->agencyAbbreviation}}</th>
	                <th><a href="javascript:void(0)" class="deleteagency editdeletebtn" id="deleteagency"  data-id="{{$agen->id}}">
                   @if($agen->is_deleted == 1)
                     No
                   @else
                     Yes
                   @endif 
                  </a></th>
	                <th>
	                	<a href="#" class="editdeletebtn editagency"  data-id="{{$agen->id}}" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
	                </th>
	                </th>
	              </tr>

	              @endforeach  
	            </tbody>
	          </table>
	        </div>  
	        <!--     Agency Modal -->
	        <div class="modal fade editskinboxmodal" id="AgencyModal" role="dialog">
	          <div class="modal-dialog">
	            <div class="modal-content">
	              <div class="modal-body">
	                <div class="editsktoboxcover">
	                  <form method="post" action="{{route('corporate-admin.addagent')}}" enctype="multipart/form-data" id="addagent">
	                    @csrf          
	                    <div class="titlandclobtn2">
	                      <h3>Add Agency</h3>
	                      <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
	                    </div>
	                    <div class="enterskintonebox">
	                      <div class="form-group">
	                        <input type="text" class="form-control"  name="agencyName" placeholder="Enter Agency" id="addAgencyName">
	                      </div>
	                      <div class="form-group">
	                        <input type="text" class="form-control"  name="agencyAbbreviation" placeholder="Enter Abbreviation" id="addabbreviation">

	                      </div>
	                      <input type="button" name="" value="Add Agency" class="btn" id="saveAgency">
	                    
	                    </div>
	                  </form>
	                </div> 
	              </div>
	            </div>
	          </div>
	        </div>
	        <!--    Edit Agency Modal -->
	        <div class="modal fade editskinboxmodal" id="editModal" role="dialog">
	          <div class="modal-dialog">
	            <div class="modal-content">
	              <div class="modal-body">
	                <div class="editsktoboxcover">
	                  <form method="post" action="{{route('corporate-admin.updateagency')}}" enctype="multipart/form-data" id="updateagencyform">
	                    @csrf          
	                    <div class="titlandclobtn2">
	                      <h3>Edit Agency</h3>
	                      <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
	                    </div>
	                    <div class="enterskintonebox">
	                      <div class="form-group">
	                      	<input type="hidden" name="id" id="editagencyid">
	                        <input type="text" class="form-control"  name="agencyName" placeholder="Enter Agency" id="editAgencyName">
	                      </div>
	                      <div class="form-group">
	                        <input type="text" class="form-control"  name="agencyAbbreviation" placeholder="Enter Abbreviation" id="editabbreviation">

	                      </div>
	                      <input type="button" name="" value="Update" class="btn" id="updateagency">
	                    
	                    </div>
	                  </form>
	                </div> 
	              </div>
	            </div>
	          </div>
	        </div>
@endsection
@section('scripts')  
<script type="text/javascript">
  $('#saveAgency').click(function(){
    
        $.ajax({

          type: "POST",
          url: "{{route('corporate-admin.addagency')}}",
          headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
          data: {
                  agencyName:$('#addAgencyName').val(),
                  agencyAbbreviation:$('#addabbreviation').val()
                },
          success: function (data) {
                 // console.log(data);
                 if(data.status==false){
                    toastr.error(data.message);
                 }
                 else{
                    toastr.success(data.message);
                    $('#AgencyModal').modal('hide'); 
                    location.reload();
                 }
                 
                 }         
      });
  });
</script>
<script type="text/javascript">
  $(document).on('click', '.editagency', function (e) {
  e.preventDefault();
  var id = $(this).data('id');
  
   $.ajax({
              type: "GET",
              url: "{{route('corporate-admin.editagency')}}",
              data: {id:id},
              success: function (data) {
                          $("#editAgencyName").val(data.agencyName);
                          $("#editabbreviation").val(data.agencyAbbreviation);
                          $("#editagencyid").val(id);
                  }         
          }); 

   });
</script>
<script type="text/javascript">
   $("#updateagency").click(function(e){
      e.preventDefault()
     var form = $("#updateagencyform");
       
     $.ajax({
         type: form.attr('method'),
         url: form.attr('action'),
         data: form.serialize(),
         success: function (data, status) {
              if(data.error){
                 return;
             }
              if(data.status == false){
                toastr.error(data.message);
              }
              else{
              $('#editModal').modal('hide');  // Your modal Id
              toastr.success(data.message);
              location.reload();
            }
         },
         error: function (result) {

         }
     });
     
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.deleteagency').click(function(){
              
              
                 var id = $(this).data('id');
                   
                    $.ajax({
                             type: "GET",
                             url: "{{route('corporate-admin.deleteagency')}}",
                             data: {id:id},
                             success: function (data) {
                                    toastr.success(data.message);
                                    location.reload();    
                                 }         
                         }); 
              
             });
  });
</script>
<script type="text/javascript">

  
  $('.deleteagency[data-toggle=confirmation]').confirmation({
           rootSelector: '[data-toggle=confirmation]',
           container: 'body',
           onConfirm: function() {
              var id = $(this).data('id');
                
                 $.ajax({
                          type: "GET",
                          url: "{{route('corporate-admin.deleteagency')}}",
                          data: {id:id},
                          success: function (data) {
                               
                                toastr.success("Agency Deleted Successfully");
                                location.reload();
                              }         
                      }); 
           },
         });
 
  

</script>
<script type="text/javascript">
  $('#statstable').on('page.dt', function() {
    setTimeout(
      function() {
       $('.deleteagency').click(function(){
                   
                   
                      var id = $(this).data('id');
                        
                         $.ajax({
                                  type: "GET",
                                  url: "{{route('corporate-admin.deleteagency')}}",
                                  data: {id:id},
                                  success: function (data) {
                                         toastr.success(data.message);
                                         location.reload();    
                                      }         
                              }); 
                   
                  });
      }, 500);
  });
 
</script>
<script type="text/javascript">
  $('#statstable').on('order.dt', function() {
    setTimeout(
      function() {
        $('.deleteagency').click(function(){
                    
                    
                       var id = $(this).data('id');
                         
                          $.ajax({
                                   type: "GET",
                                   url: "{{route('corporate-admin.deleteagency')}}",
                                   data: {id:id},
                                   success: function (data) {
                                          toastr.success(data.message);
                                          location.reload();    
                                       }         
                               }); 
                    
                   });
      }, 500);
  });
 
</script>
@endsection
