
  <?php  $internalStaff  = Route::currentRouteName();?>
   @if(isset($performers_user->applicationfor))
     @if($performers_user->applicationfor == "Group")
     <?php $GroupClass = $performers_user->applicationfor; ?>
     @else
     <?php $GroupClass = ""; ?>
     @endif
     @endif



    <div class="taballdeainerpag   <?php if($GroupClass=='Group'){ echo 'internalStaffTabGroup'; }else{echo 'internalStaffTab';}  ?>">
     <ul>          
       <li class="<?php if($internalStaff=='internal-staff.portfolio'){ echo 'active'; }  ?>"><a href="{{route('internal-staff.portfolio', ['id' => $per_id])}}">Portfolio</a></li>

      <li class=" <?php if($internalStaff=='internal-staff.closet'){ echo 'active'; }  ?>"><a href="{{route('internal-staff.closet', ['id' => $per_id])}}">Closet</a></li>

      <li class="<?php if($internalStaff=='internal-staff.skills'){ echo 'active'; }  ?>"><a href="{{route('internal-staff.skills', ['id' => $per_id])}}">Skills</a></li>

      @if(isset($performers_user->applicationfor))
      @if($performers_user->applicationfor != "Group")
      <li class="<?php if($internalStaff=='internal-staff.appearance'){ echo 'active'; }  ?>"><a href="{{route('internal-staff.appearance', ['id' => $per_id])}}">Stats</a></li>
      @endif
      @endif
      <li class="<?php if($internalStaff=='internal-staff.rental'){ echo 'active'; }  ?>"><a href="{{route('internal-staff.rental', ['id' => $per_id])}}">Props</a></li>

       <li class="<?php if($internalStaff=='internal-staff.resume'){ echo 'active'; }?>"><a href="{{route('internal-staff.resume', ['id' => $per_id])}}">Resume</a></li>

      
      @if(isset($performers_user->applicationfor))
      @if($performers_user->applicationfor != "Group")
      <li class="<?php if($internalStaff=='internal-staff.documents'){ echo 'active'; }  ?>"><a href="{{route('internal-staff.documents', ['id' => $per_id])}}">Documents</a></li>
      @endif
      @endif
      
      @if(isset($performers_user->applicationfor))
      @if($performers_user->applicationfor != "Group")
       <li class="<?php if($internalStaff=='internal-staff.notes'){ echo 'active'; }  ?>"><a href="{{route('internal-staff.note', ['id' => $per_id])}}">Notes</a></li>
      @endif
      @endif
      
    </ul>
    </div>
    