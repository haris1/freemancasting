<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title','freemancasting')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/toastr.min.css') }}">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css"/> -->
    <link href="{{ asset('assets/css/dropzone.css') }}" rel="stylesheet" type="text/css" />
    
    <link href="{{ asset('assets/fontawesome-pro/css/all.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/VueAgile.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/all.min.css') }}">
    
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.imageview.css') }}">

    <link rel="stylesheet"  href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
     <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
    @yield('styles')
</head>

<body>
    @if(Auth::guard('web')->check())
        @include('performers.layouts.header') 
        @yield('content')       
    @elseif(Auth::guard('internal-staff')->check())
        @include('internal-staff.layouts.header') 
    @yield('content')
    @else
        @yield('content')
    @endif
    
      
</body>

<script type="text/javascript" src="{{ asset('assets/js/jquery.js')}}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/js/moment.min.js')}}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->
<script src="{{ asset('assets/js/dropzone.js')}}"></script>
<script src="{{ asset('assets/js/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/fontawesome-pro/js/all.js')}}"></script>
<script src="{{ asset('assets/js/select2.min.js')}}"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ asset('assets/js/dataTable.js')}}"></script>

<script src="{{ asset('assets/js/bootstrap-confirmation.js')}}"></script>
<script src="{{ asset('assets/js/dataTableBoot.js')}}"></script>
<script src="{{ asset('assets/js/masonry.pkgd.min.js')}}"></script>
<!-- <script src="{{ asset('assets/js/vue.min.js')}}"></script> -->
<script src="{{ asset('assets/js/vue.js')}}"></script>

<script src="{{ asset('assets/js/VueAgile.js')}}"></script>
<script src="{{ asset('assets/js/jquery.mask.min.js')}}"></script>
<script src="{{ asset('assets/js/jquery.imageview.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.4.9/jquery.autocomplete.min.js" integrity="sha256-CA1BfwIJ3XnP7Jx7XaRKrrmJILIn+4skJeDB2YVo6nw=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>


<!--     <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
 -->
   <!--  <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script> -->
   
    <script>

/*
      $(document).ready(function () {
    //Disable full page
    
    $("body").on("contextmenu",function(e){
      return false;
    });
    
    //Disable part of page
    $('body').bind('cut copy paste', function (e) {
      e.preventDefault();
    });
    

  });  
  $(document).keydown(function (event) {
    
    if (event.keyCode == 123 || event.keyCode == 44 || event.ctrlKey && 
            (event.keyCode === 67 || 
             event.keyCode === 86 || 
             event.keyCode === 85 || 
             event.keyCode === 117)) { // Prevent F12
      return false;
    } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { 
    // Prevent Ctrl+Shift+I        
      return false;
    }
  });*/
// function copyToClipboard() {
//   // Create a "hidden" input
//   var aux = document.createElement("input");
//   // Assign it the value of the specified element
//   aux.setAttribute("value", "Você não pode mais dar printscreen. Isto faz parte da nova medida de segurança do sistema.");
//   // Append it to the body
//   document.body.appendChild(aux);
//   // Highlight its content
//   aux.select();
//   // Copy the highlighted text
//   document.execCommand("copy");
//   // Remove it from the body
//   document.body.removeChild(aux);
//   //alert("Print screen desabilitado.");
// }

$(window).keyup(function(e){
  if(e.keyCode == 44){
   /// copyToClipboard();
  }
}); 

// $(window).focus(function() {
//   $("body").show();
// }).blur(function() {
//   $("body").hide();
// });
    

           $('#DOB').datepicker("setDate",new Date(1990,0,1));
           $('.DOBdatepicker').datepicker("setDate",new Date(1990,0,1));
       

         $("#updateProfile").click(function(){
        EditformData = $("#performerProfileEdit").serialize();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url:"{{route('performers.updateProfile')}}",
            type:'POST',
            data:EditformData,
            success:function(response)
            {
                if(response.status == true)
                {
                    toastr.success(response.message);
                  
                }
            }
        });

    });
    $("#deleteprofile").click(function(){
       
    });
    // Dropzone.options.AddProfiledropzone =
    // {
    //   maxFilesize: 100,
    //   renameFile: function(file) {
    //     var dt    = new Date();
    //     var time  = dt.getTime();
    //     return time+file.name;
    //   },
    //   type:"post",
    //   headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    //   acceptedFiles: ".jpeg,.jpg,.png,.gif",
    //   addRemoveLinks: true,
    //   timeout: 5000,

    //   success: function(file, response) 
    //   {            
    //     console.log(response);
    //        if(response.status == true){
    //         $('#profilesection').html(response.data);
    //        // location.reload();
           
    //        }        
    //   },
    //   error: function(file, response)
    //   {
    //     console.log(file);
    //     return false;
    //   }
    // };
    </script>

    <script>
      $(document).ready(function(){
        
        $("#ProfilePassword").hide();

        $("#ChangePassword").click(function(){
        $("#ProfilePassword").show();
        $("#ProfileData").hide();
       
    });

    $("#profileformShow").click(function(){
        $("#ProfileData").show();
        $("#ProfilePassword").hide();
    });

      });
    
      $("#PasswordChangeBtn").click(function(){
        ChangePasswordData =$("#ChangePasswordForm").serialize();
        $.ajaxSetup({
         headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
       });
        $.ajax({
            url:"{{route('performers.changepassword')}}",
            type:"post",
            data:ChangePasswordData,
            success:function(response)
            {
              if(response.status == true)
              {
                toastr.success(response.message);
                location.reload();
              }

              if(response.status == false)
              {
                toastr.error(response.message);
              }
            },
            error:function(response)
            {

            }
        });
    });
     $(document).ready(function(){
        src = "{{route('performers.agentlist')}}";

    $( "#modelagent" ).autocomplete({
    source: function(request, response) {
            $.ajaxSetup({
                 headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
               });
            $.ajax({
                
                type: 'POST',
                url: src,
                data: {
                    term : $("#modelagent").val()
                },
                success: function(data) {
                     if (!data.length) {
                    var result = [{ label: "no results", value: response.term }];
                    response(result);
                }
                    response(data);

                },
                
            });
        },
        minLength: 1,
        appendTo: "#categorysomeElem",
    select: function(event, ui) {
        var label = ui.item.label;
                    if (label === "Add Agent") {
                        event.preventDefault();
                        // alert($("#agent").val());
                        $.ajaxSetup({
                             headers: {
                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                             }
                           });
                        $.ajax({
                            
                            type: 'POST',
                            url: "{{route('performers.addinagentlist')}}",
                            data: {
                                 data : $("#modelagent").val()
                            },
                            success: function(data) {
                                alert()
                              $('#modelagent_id').val(data);
                              toastr.success("Agent Added Successfully");

                            },
                            
                        })

                }
       else{         
      $('#modelagent_id').val(ui.item.id);
        }
    },
    change: function (event, ui) {
        if (ui.item == null){ 
         //here is null if entered value is not match in suggestion list
         //  toastr.warning("Select Agent");
            $(this).val((ui.item ? ui.item.id : ""));
        }
    },
    response: function(event, ui) {
        if (!ui.content.length) {

            var noResult = { value:"",label:"No results found" };
          //  var newitem = { value:"test",label:"Add Agent"}
             ui.content.push(noResult);
          //  ui.content.push(newitem);

        }
      }  
     });

        });
    $('.confirmdeleteprofile[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
               $.ajax({
                      type: "GET",
                      url: "{{route('performers.deleteprofile')}}",
                      data:{id:$(this).data('id')},
                     
                      success: function(data){
                        alert("kh");
                        // location.reload();
                        // $("#profiledropzone").removeClass('hidden');  
                        // $("#profileimage").addClass('hidden');  
                      }
                    });
             },
           });

</script>


@if(Auth::guard('web')->check())  
  @include('performers.layouts.footer')
@elseif(Auth::guard('internal-staff')->check())
  @include('internal-staff.layouts.footer')
@endif

  <script type="text/javascript">
    $(document).ready(function(){
       $("#passwordDisplay").hide();

         $("#ChangePassword").click(function(){

          $("#passwordDisplay").show();
          $("#ProfileDisplay").hide();
        });

         $("#profileformShow").click(function(){
          $("#passwordDisplay").hide();
          $("#ProfileDisplay").show();
        });

          $("#updateProfile").click(function(){
          EditformData = $("#performerProfileEdit").serialize();
          $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
          $.ajax({
              url:"{{route('internal-staff.updateProfile')}}",
              type:'POST',
              data:EditformData,
              success:function(response)
              {
                  if(response.status == true)
                  {
                      toastr.success(response.message);
                      location.reload(1500);
                  }
              }
          });

      });
         $("#PasswordChangeBtn").click(function(){
           ChangePasswordData =$("#ChangePasswordForm").serialize();
           $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
           $.ajax({
               url:"{{route('performers.changepassword')}}",
               type:"post",
               data:ChangePasswordData,
               success:function(response)
               {
                 if(response.status == true)
                 {
                   toastr.success(response.message);
                   location.reload();
                 }

                 if(response.status == false)
                 {
                   toastr.error(response.message);
                 }
               },
               error:function(response)
               {
                  
               }
           });
       }); 
    });
    jQuery('#flagSelect').select2({
                  placeholder:'Select Flag'
              });


    function FlagSave(){
          
        var flagformdata = $("#flagSaveForm").serialize();    
           $.ajax({
            url:"{{route('internal-staff.saveFlag')}}",
            type:"POST",
            data:flagformdata,
            success:function(data){
                
             if(data.status == true)
             {
                toastr.success(data.message);
                location.reload();
             }
             if(data.status == false)
             {
                toastr.error(data.message);
             }
            }

        });
    }   


  
  </script>
  
@yield('scripts')
@yield('scripts1')

</html>

