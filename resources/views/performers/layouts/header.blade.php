<header>
  <div class="hedpartlr">
    <div class="lefttitlebox">      
      <div class="imageLogo">
        <a href="javascript:void(0)"><img src="{{ asset('assets/images/logo.png') }}" alt=""></a>
      </div>
  </div> 
  <div class="rightnotusei">
    <div class="inerboxsetpart">
        	<a href="javascript:void(0)" class="belllogbtn"><i class="far fa-bell"></i></a>
        	<!-- <a href="javascript:void(0)" class="active" type="button" data-toggle="modal" data-target="#myModal15"><i class="fas fa-user"></i></a> -->
          <a href="javascript:void(0)" class="active" type="button" data-toggle="modal" data-target="#myModal15">
            <div class="userprofileiconset_inerimg">
              <div class="hexagon_userprofileset_maincl1">
                <div class="hexagon_userprofileset_inercl1">

                  <img src="{{Auth::user()->image_url}}">

                </div>    
              </div>    
            </div>
          </a> 
        	<a href="{{ route('performers.logout') }}" onclick="event.preventDefault();
          	document.getElementById('logout-form').submit();" class="belllogbtn">
         		<i class="fas fa-power-off"></i>
      	 </a>
         
      	<form id="logout-form" action="{{ route('performers.logout') }}" method="POST" style="display: none;">
          	@csrf
      	</form>
    </div> 
  </div> 
</div>
</header>