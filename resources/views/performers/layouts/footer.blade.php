<?php 
  use App\Models\v1\Performer\PerformerUnion;
  use App\Models\v1\Performer\Region;
  use App\Models\v1\PerformerAgent;
  use App\Models\v1\Performer\Appearance;
  use App\Models\v1\Performer\PerformerStatus;
  use App\Models\v1\Performer\PerformerAvailability;
 
   $appearance           = Appearance::where('performer_id',Auth::user()->id)->first();
  $PerformerAvailability = PerformerAvailability::all(); 
  $PerformerUnion = PerformerUnion::all();
  $region = Region::all();
  $agent  = PerformerAgent::all();
  $PerformerStatus = PerformerStatus::all();
?>
<div class="modal fade userinfoboedit" id="myModal15" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">      
      <div class="modal-body">        
        <div class="editboxprofilin">
          <div class="totitleaddris">
            <h4>Edit User Profile</h4>
            <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
          </div>
          
          @if(isset($performers_user))
          <input type="hidden" name="" value="{{$performers_user->id}}" id="performers_user">
          <div class="detailprofcover" id="ProfileData">
           
            <form method="post" id="performerProfileEdit">
            <div class="form-group">
              <label for="usr">First Name</label>
              <input type="text" value="{{$performers_user->firstName}}" name="firstName" class="form-control" >
            </div>
            <div id="profile_image">
           <div id="profilesection" class="dropimgboxsetmod">
            @if(isset($performer->upload_primary_url))   
            <div id="profileshow">         
            <div class="addphotosetbox">
              <img src="{{$performer->image_url}}" alt="">             
              <a href="javascript:void(0)" id="deleteprofile" data-id="{{$performer->id}}" data-toggle="confirmation" data-title="are you sure?" class="confirmdeleteprofile"><i class="fas fa-trash"></i></a>
            </div>
            </div>
            @else
            
            <div class="uploadimgboxprof" id="profiledropzone">
              <div class="dropboxzoniner">
                <div id="Image_upload_dropzone">
                  <div method="post" action="{{ route('performers.storeprofile') }}" enctype="multipart/form-data" 
                  class="dropzone" id="AddProfiledropzone">          
                  </div>
                </div>
              </div>
            </div>
            @endif
          </div>
          </div>

          <div id="dropzoneDisplay" class="hidden">
            <div id="profilesection" class="dropimgboxsetmod">
              <div class="uploadimgboxprof" id="profiledropzone">
              <div class="dropboxzoniner">
                <div id="Image_upload_dropzone">
                  <div method="post" action="{{ route('performers.storeprofile') }}" enctype="multipart/form-data" 
                  class="dropzone" id="AddProfiledropzone">          
                  </div>
                </div>
              </div>
            </div>  
            </div>         
          </div>

        

            <div class="form-group">
              <label for="usr">Middle Name</label>
              <input type="text" value="{{$performers_user->middleName}}" name="middleName" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Last Name</label>
              <input type="text" value="{{$performers_user->lastName}}" name="lastName" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Agent</label>
                <div class="seletcustboxpart2">

                  <select class="form-control validate" id="agent" name="agent_id" data-required="Please enter Agent name">
                    <option></option>
                    @foreach($agent as $agent) 
                    <option value="{{$agent->id}}"  <?php echo (isset($performers_user->agent_id) && $performers_user->agent_id == $agent->id) ? 'selected':''?>>{{$agent->agent_name}}</option>



                    @endforeach
                </select>
              </div>
              <!-- <input type="text" value="{{isset($performers_user->agentDetail->agent_name) ? ($performers_user->agentDetail->agent_name) : '' }}" name="modelagent" class="form-control" id="modelagent">
              <input type="hidden" class="form-control validate" name="agent_id" data-required="Please enter Agent(Company and Contact)" id="modelagent_id"  value="{{isset($performers_user->agent_id) ? ($performers_user->agent_id) : '' }}"> -->
              <div id="categorysomeElem" class="sectionsomeElem" ></div>
            </div>           
            <div class="form-group">
              <label for="usr">Union</label>
              <div class="seletcustboxpart2">

                <select class="form-control validate" id="union" name="union_name" data-required="Please enter union name">
                  <option></option>
                  @foreach($PerformerUnion as $union) 
                  <option value="{{$union->id}}"  <?php echo (isset($performers_user->union_name) && $performers_user->union_name == $union->id) ? 'selected':''?>>{{$union->union}}</option>



                  @endforeach
              </select>
            </div>
          </div>
            <div class="form-group">
              <label for="usr">Union Number</label>
              <input type="text" value="{{$performers_user->union_number}}" name="union_number" class="form-control" >
            </div>
            <div class="form-group date">
                <label for="usr">Date of Birth</label>
                <input type="text"  value="{{$performers_user->date_of_birth}}" class="form-control validate  DOBdatepicker" name="date_of_birth" data-required="Please select the date of birth" autocomplete="off" readonly>
            </div>
             <div class="form-group">
              <label for="usr">Referred by</label>
              <input type="text" value="{{$performers_user->referred_by}}" name="referred_by" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Email</label>
              <input type="text" name="email" value="{{$performers_user->email}}" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Phone</label>
              <input type="text" name="phonenumber" value="{{$performers_user->phonenumber}}" class="form-control" data-mask="(000) 000-0000" id="phonenumber">
            </div>
            <div class="form-group">
              <label for="usr">Address</label>
              <input type="text" name="address" value="{{$performers_user->address}}" class="form-control" id="address">
            </div>
            <div class="form-group">
              <label for="usr">City</label>
              <input type="text" name="city" value="{{$performers_user->city}}" class="form-control" >
            </div>
              <div class="form-group">
                <label for="usr">Region</label>
               <div class="seletcustboxpart2">
                <select class="selectField" id="region" name="region_id">
                  <option></option>
                  @foreach ($region as $region)
                    <option value="{{ $region->id }}" {{isset($appearance)&&($region->id==$appearance->region_id)?'Selected':''}} > {{$region->region}}</option>
                  @endforeach
                </select>
               </div>
            </div>
            <div class="form-group">
              <label for="usr">Province</label>
              <input type="text" name="province" value="{{isset($performers_user->province) ? $performers_user->province : 'BC'}}" class="form-control" id="province">
            </div>
            <div class="form-group">
              <label for="usr">Postal Code</label>
              <input type="text" name="postalCode" value="{{$performers_user->postalCode}}" class="form-control" id="postalCode">
            </div>
            <div class="form-group">
              <label for="usr">Country</label>
              <input type="text" name="country" value="{{isset($performers_user->country) ? $performers_user->country : 'Canada'}}
              " class="form-control" >
            </div>
              <div class="form-group">
                <label for="usr">Performer Availability</label>
               <div class="seletcustboxpart2">
                  <select class="form-control validate" id="availability" name="availability" data-required="Please Select Availability">
                          <option value=""></option>
                         @foreach($PerformerAvailability as $availability) 
                          <option value="{{$availability->id}}" <?php echo (isset($performers_user->availability) && $performers_user->availability == $availability->id) ? 'selected':''?>>{{$availability->availability}}</option>
                           @endforeach
                      </select>
               </div>
            </div>
              <div class="form-group">
                <label for="usr">Performer Status</label>
               <div class="seletcustboxpart2">
                <select class="selectField" id="status" name="status_id">
                  <option></option>
                  @foreach ($PerformerStatus as $status)
                    <option value="{{ $status->id }}" <?php echo (isset($performers_user->status_id) && $performers_user->status_id == $status->id) ? 'selected':''?>> {{$status->performer_status}}</option>
                  @endforeach
                </select>
               </div>
            </div>
            <div class="form-group">
              <label for="usr">Social media(Instagram)</label>
              <div class="inerinputhttps inssocmedlnk1">
                <p>www.instagram.com/</p>
                <input type="text" value="{{$performers_user->instagram}}" name="instagram" class="form-control" >
              </div>              
            </div>
            <div class="form-group">
              <label for="usr">Social media(Twitter)</label>
              <div class="inerinputhttps inssocmedlnk2">
                <p>www.twitter.com/</p>
                <input type="text" value="{{$performers_user->twitter}}" name="twitter" class="form-control" >
              </div>
            </div>
            <div class="form-group">
              <label for="usr">Social media(Facebook)</label>
              <div class="inerinputhttps inssocmedlnk3">
                <p>www.facebook.com/</p>
                <input type="text" value="{{$performers_user->facebook}}" name="facebook" class="form-control" >
              </div>
            </div>
            <div class="form-group">
              <label for="usr">Social media(Linkedin)</label>
              <div class="inerinputhttps inssocmedlnk4">
                <p>www.linkedin.com/</p>
                <input type="text" value="{{$performers_user->linkedin}}" name="linkedin" class="form-control" >
              </div>
            </div>
            <div class="savebtnbox">
              <button type="button" id="updateProfile" class="btn">Save</button>
              <button type="button" id="ChangePassword" class="btn">Change Password</button>
            </div>
            
            <input type="hidden" value="{{$performers_user->id}}" name="id" class="form-control" >
          </form>
          </div>

          <div class="detailprofcover" id="ProfilePassword">
            <form method="post" id="ChangePasswordForm">
            <div class="form-group">
              <label for="usr">Old Password</label>
              <input type="password" value="" name="old_password" class="form-control" >
            </div>

            <div class="form-group">
              <label for="usr">New Password</label>
              <input type="password" value="" name="password" class="form-control" id="usr">
            </div>

            <div class="form-group">
              <label for="usr">Confirm Password</label>
              <input type="password" value="" name="confirm_password" class="form-control" >
            </div>

            <div class="savebtnbox">
              <button type="button" id="PasswordChangeBtn" class="btn">Change Password</button>
              <button type="button" id="profileformShow" class="btn">Profile </button>
            </div>
          </form>
          </div>
          @endif
        </div>
      </div>
    </div>    
  </div>
</div>

<!-- LOGIN -->
 <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVE0QMrl_5EQOVzAB1dvZN5S9wpqODWPk &libraries=places"></script>
        <script>
            var autocomplete = new google.maps.places.Autocomplete($("#address")[0], {});

            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                console.log(place.address_components);
            });
        </script> -->
<script type="text/javascript">
      $("#availability").select2({
    // tags: "true",
    placeholder:  "Select Availability",
    
    allowClear: true,
  });
</script>
<script type="text/javascript">
      $("#agent").select2({
    // tags: "true",
    placeholder:  "Select Agent",
    
    allowClear: true,
  });
</script>
<script type="text/javascript">
      $("#status").select2({
    // tags: "true",
    placeholder:  "Select Status",
    
    allowClear: true,
  });
</script>
<script type="text/javascript">
      $("#region").select2({
    // tags: "true",
    placeholder:  "Select Region",
    
    allowClear: true,
  });
</script>
<script type="text/javascript">
  jQuery('.addFlagModal').addClass('hidden');
  $('.DOBdatepicker').datepicker("setDate",new Date("{{$performers_user->date_of_birth}}"));
    $("#union").select2({
  // tags: "true",
  placeholder:  "Select an Union",
  
  allowClear: true,
});
   $("#updateProfile").click(function(){
        EditformData = $("#performerProfileEdit").serialize();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url:"{{route('performers.updateProfile')}}",
            type:'POST',
            data:EditformData,
            success:function(response)
            {
                if(response.status == true)
                {
                    toastr.success(response.message);
                   location.reload(1500);
                }
                else{
                  toastr.error(response.message);
                }
            }
        });

    });
  $("#province").mask("AA");
  $("#phonenumber").mask("(999) 999-9999");   
  $("#postalCode").mask("AAAAAAAAAAA")
     $(document).ready(function(){
        src = "{{route('performers.agentlist')}}";

    $( "#modelagent" ).autocomplete({
    source: function(request, response) {
            $.ajaxSetup({
                 headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
               });
            $.ajax({
                
                type: 'POST',
                url: src,
                data: {
                    term : $("#modelagent").val()
                },
                success: function(data) {
                     if (!data.length) {
                    var result = [{ label: "no results", value: response.term }];
                    response(result);
                }
                    response(data);

                },
                
            });
        },
        minLength: 1,
        appendTo: "#categorysomeElem",
    select: function(event, ui) {
        var label = ui.item.label;
                    if (label === "Add Agent") {
                        event.preventDefault();
                        // alert($("#agent").val());
                        $.ajaxSetup({
                             headers: {
                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                             }
                           });
                        $.ajax({
                            
                            type: 'POST',
                            url: "{{route('performers.addinagentlist')}}",
                            data: {
                                 data : $("#modelagent").val()
                            },
                            success: function(data) {
                              $('#modelagent_id').val(data);
                              toastr.success("Agent Added Successfully");

                            },
                            
                        })

                }
       else{         
      $('#modelagent_id').val(ui.item.id);
        }
    },
    response: function(event, ui) {
        if (!ui.content.length) {

             var noResult = { value:"",label:"No results found" };
           // var newitem = { value:"test",label:"Add Agent"}
             ui.content.push(noResult);
           // ui.content.push(newitem);

        }
      }  
     });

        });
     jQuery(function($){
       
     });
</script>

<script type="text/javascript">
  
  $(document).ready(function(){    

   $.ajax({
         url:"{{route('performers.portfolio.getSliderImage')}}",
        type:'get',
        data:{id:jQuery('#performers_user').val()},
        success:function(response){
             
          showsliderimg(response.msg);
            
        }

  });
    });
function showsliderimg(img){
  
 setTimeout(function(){ 
  Vue.use(VueAgile);
    app = new Vue({
    el: '#mySidebarImage',
    components: {
    agile: VueAgile },
  data() {
    return {
      asNavFor1: [],
      asNavFor2: [],
      options1: {
        dots: false,
        fade: true,
        navButtons: false },
      options2: {
        autoplay: false,
        centerMode: false,
        dots: false,
        navButtons: false,
         infinite : false,
        slidesToShow: 3,
        responsive: [
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 4 } },

        {
          breakpoint: 1000,
          settings: {
            navButtons: true } }] },


          slides: img
          };


  },
  mounted() {
    this.asNavFor1.push(this.$refs.thumbnails);
    this.asNavFor2.push(this.$refs.main);
  } }); }, 1000);

 }
 $(document).ready(function() {
    $(".set > a").on("click", function() {
      if ($(this).hasClass("active")) {
        $(this).removeClass("active");
        $(this)
        .siblings(".content")
        .slideUp(200);
        $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
      } else {
        $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
        $(this)
        .find("i")
        .removeClass("fa-plus")
        .addClass("fa-minus");
        $(".set > a").removeClass("active");
        $(this).addClass("active");
        $(".content").slideUp(200);
        $(this)
        .siblings(".content")
        .slideDown(200);
      }
    });
  });
 $('#deleteprofile[data-toggle=confirmation]').confirmation({
          rootSelector: '[data-toggle=confirmation]',
          container: 'body',
          onConfirm: function() {

            $("#profile_image").addClass("hidden");
            $("#dropzoneDisplay").removeClass("hidden");
              $.ajax({
                     type: "GET",
                     url: "{{route('performers.deleteprofile')}}",
                     data:{id:$(this).data('id')},
                    
                     success: function(data){
                    
                        $('#profilesection').html(data.response);
                        loadDropzone();
                       

                       // $("#profiledropzone").removeClass('hidden');  
                       // $("#profileimage").addClass('hidden');  
                     }
                   });
          },
        });

 loadDropzone();
function loadDropzone(){
 // Dropzone.autoDiscover = false;
  Dropzone.options.AddProfiledropzone =
    {
      maxFilesize: 100,
      renameFile: function(file) {
        var dt    = new Date();
        var time  = dt.getTime();
        return time+file.name;
      },
      type:"post",
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      acceptedFiles: ".jpeg,.jpg,.png,.gif",
      addRemoveLinks: true,
      timeout: 5000,

      success: function(file, response) 
      {            
        console.log(response);
           if(response.status == true){
            $("#dropzoneDisplay").addClass("hidden");
            $("#profile_image").removeClass("hidden");
            $('#profilesection').html(response.data);
            loadDropzone();
           // location.reload();
           $('#deleteprofile[data-toggle=confirmation]').confirmation({
                    rootSelector: '[data-toggle=confirmation]',
                    container: 'body',
                    onConfirm: function() {
                      
                      $("#profile_image").addClass("hidden");
                      $("#dropzoneDisplay").removeClass("hidden");
                      
                        $.ajax({
                               type: "GET",
                               url: "{{route('performers.deleteprofile')}}",
                               data:{id:$(this).data('id')},
                              
                               success: function(data){

                                 $('#profilesection').html(data.response);                              
                                  

                               }
                             });
                    },

                  });
           }        
      },
      error: function(file, response)
      {
        console.log(file);
        return false;
      }
    }
}

</script>
  
