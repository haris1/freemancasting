<div class="leftprofiledit">
    <div class="topbarprfname">
  <a href="javascript:window.history.back()"> <i class="far fa-chevron-left"></i><h3>{{$performer->full_name}} <span>{{($performer->gender == 'male') ? 'M' : 'F'}}</span></h3></a>
    </div>


    <div class="sidebarsldbox">
      @if(isset($performer->referred_by) && $performer->referred_by !="")
                    <div class="brflimgset">
                      
                        <img src="{{asset('assets/svg/default-icon.svg') }}" alt="">
                    </div>
                    @endif

      <div id="mySidebarImage" class="dtusegslbox">
          <agile class="main" ref="main" :options="options1" :as-nav-for="asNavFor1">
              <div class="slide" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`"><img :src="slide"/></div>
          </agile>
          <agile class="thumbnails" ref="thumbnails" :options="options2" :as-nav-for="asNavFor2">
              <div class="slide slide--thumbniail" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`" @click="$refs.thumbnails.goTo(index)"><img :src="slide"/></div>
              <template slot="prevButton"><i class="fas fa-chevron-left"></i></template>
              <template slot="nextButton"><i class="fas fa-chevron-right"></i></template>
          </agile>
      </div>
    </div>

 
    <div class="leriuserdetabox">
      
      @if(isset($performer->agentDetail->agent_name))
      <div class="userdetabox">
        <div class="userdeta_left">
          <p>Agent</p>
        </div>        
        <div class="userdeta_right">
          <p>{{$performer->agentDetail->agent_name}}</p>
        </div>        
      </div>
      @endif
      @if(isset($performer->union_name))
      <div class="userdetabox">
        <div class="userdeta_left">
          <p>Union</p>
        </div>
        <div class="userdeta_right">
          <p>
            @if(isset($performer->unionDetail->union))
            {{$performer->unionDetail->union}} - {{$performer->union_number}}
            @endif
          </p>
        </div>
      </div>
      @endif
      <div class="userdetabox referredtext">
        <div class="userdeta_left">
          <p>Referred</p>
        </div>
        <div class="userdeta_right">
          <p>{{$performer->referred_by}}</p>
        </div>
      </div>
      @if(isset($performer->date_of_birth))
      <div class="userdetabox ">
        <div class="userdeta_lefticon">
          <i class="far fa-birthday-cake"></i>
        </div>
        <div class="userdeta_right">        
          <p>{{ \Carbon\Carbon::parse($performer->date_of_birth)->format('F d,Y')}}<span>{{ \Carbon\Carbon::parse($performer->date_of_birth)->age}} yrs</span></p>
          <p>{{$age_range}}</p>
        </div>
      </div>
      
      @endif
      @if(isset($performer->email))
      <div class="userdetabox linkboxset">
        <div class="userdeta_lefticon">
          <i class="far fa-envelope"></i>
        </div>
        <div class="userdeta_right">
          <p><a href = "mailto: {{$performer->email}}">{{$performer->email}}</a></p>
        </div>
      </div>
      @endif
      @if(isset($performer->phonenumber))
      <div class="userdetabox">
        <div class="userdeta_lefticon">
          <i class="far fa-phone"></i>
        </div>
        <div class="userdeta_right">
           <p>{{$performer->phonenumber}}</p>
        </div>
      </div>
      @endif
      @if(isset($performer->address))
      <div class="userdetabox">
        <div class="userdeta_lefticon">
          <i class="far fa-map-marked"></i>
        </div>
        <div class="userdeta_right">
          <p>{{$performer->address}}</p>



           <p>{{ $performer->city ?? '' }} , {{ $performer->province ?? '' }} ,{{ $performer->postalCode ?? '' }} </p>
        </div>
      </div>
      @endif
      @if(isset($performer->instagram) && !empty($performer->instagram))      
      <div class="userdetabox linkboxset">
        <div class="userdeta_lefticon">
          <i class="fab fa-instagram"></i>
        </div>
        <div class="userdeta_right">
          <a href="https://www.instagram.com/{{$performer->instagram}}" target="_blank">{{$performer->instagram}}</a>
        </div>
      </div>
      @endif
      @if(isset($performer->twitter)  && !empty($performer->twitter))      
      <div class="userdetabox linkboxset">
        <div class="userdeta_lefticon">
          <i class="fab fa-twitter"></i>
        </div>
        <div class="userdeta_right">
          <a href="https://twitter.com/{{$performer->twitter}}" target="_blank">{{$performer->twitter}}</a>
        </div>
      </div>
      @endif
      
      @if(isset($performer->facebook) && !empty($performer->facebook))      
      <div class="userdetabox linkboxset">
        <div class="userdeta_lefticon">
          <i class="fab fa-facebook"></i>
        </div>
        <div class="userdeta_right">
          <a href="https://www.facebook.com/{{$performer->facebook}}" target="_blank">{{$performer->facebook}}</a>
        </div>
      </div>
      @endif
      @if(isset($performer->linkedin) && !empty($performer->linkedin))      
      <div class="userdetabox linkboxset">
        <div class="userdeta_lefticon">
          <i class="fab fa-linkedin"></i>
        </div>
        <div class="userdeta_right">
          <a href="https://www.linkedin.com/{{$performer->linkedin}}" target="_blank">{{$performer->linkedin}}</a>
        </div>
      </div>
      @endif

      
    </div>
  </div>
 