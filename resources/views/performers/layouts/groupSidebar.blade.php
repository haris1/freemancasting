<div class="leftprofiledit">
    <div class="topbarprfname">
      <i class="far fa-chevron-left"></i><h3>
        @if($performer->groupdetail)
         {{$performer->groupdetail->group_name}}
        @endif

        <span>Group</span></h3>
    </div>
    <div class="sidebarsldbox">
      @if(isset($performer->referred_by) && $performer->referred_by !="")
                    <div class="brflimgset">
                      
                        <img src="{{asset('assets/svg/default-icon.svg') }}" alt="">
                    </div>
                    @endif
                    <div id="mySidebarImage" class="dtusegslbox">
                      <agile class="main" ref="main" :options="options1" :as-nav-for="asNavFor1">
                        <div class="slide" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`"><img :src="slide"/></div>
                      </agile>
                      <agile class="thumbnails" ref="thumbnails" :options="options2" :as-nav-for="asNavFor2">
                        <div class="slide slide--thumbniail" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`" @click="$refs.thumbnails.goTo(index)"><img :src="slide"/></div>
                        <template slot="prevButton"><i class="fas fa-chevron-left"></i></template>
                        <template slot="nextButton"><i class="fas fa-chevron-right"></i></template>
                      </agile>
                    </div>
    
<!--     @if(isset($performer->upload_primary_url))
    <div class="profile_photo" id="profileimage">

      <img src="{{$performer->image_url}}" alt="">
     
      <a href="javascript:void(0)" id="deleteprofile" data-id="{{$performer->id}}"><i class="fas fa-trash"></i></a>
    </div>
    @else
    <div class="uploadimgboxprof" id="profiledropzone">
      <div class="dropboxzoniner">
        <div id="Image_upload_dropzone">
          <div method="post" action="{{ route('performers.storeprofile') }}" enctype="multipart/form-data" 
          class="dropzone" id="AddProfiledropzone">          
          </div>
        </div>
      </div>
    </div>
    @endif -->
    <!-- <div class="userallphotosbox">
      <ul>
        @if(count($sidebar_image))
        @foreach($sidebar_image as $imagesList)        
        <li><img src="{{ $imagesList->image_url }}" alt=""></li>        
        @endforeach
        @else
        <li><img src="{{asset('assets/images/demoimg.png') }}" alt=""></li>
        <li><img src="{{asset('assets/images/demoimg.png') }}" alt=""></li>
        <li><img src="{{asset('assets/images/demoimg.png') }}" alt=""></li>
        <li><img src="{{asset('assets/images/demoimg.png') }}" alt=""></li>        
       @endif   
      </ul>
    </div> -->
    <div class="leriuserdetabox">
      
      @if(isset($performer->agentDetail->agent_name))
      <div class="userdetabox">
        <div class="userdeta_left">
          <p>Agent</p>
        </div>        
        <div class="userdeta_right">
          <p>{{$performer->agentDetail->agent_name}}</p>
        </div>        
      </div>
      @endif
      @if(isset($performer->union_name))
      <div class="userdetabox">
        <div class="userdeta_left">
          <p>Union</p>
        </div>
        <div class="userdeta_right">
          <p>{{$performer->unionDetail->union}} -  {{$performer->union_number}}</p>
        </div>
      </div>
      @endif
      <div class="userdetabox referredtext">
        <div class="userdeta_left">
          <p>Referred</p>
        </div>
        <div class="userdeta_right">
          <p>{{$performer->referred_by}}</p>
        </div>
      </div>
      @if(isset($groupUser->groupPerformers))
      <div class="userdetaaddbox">        
          <a class="hiddenforInternalStaff" href="javascript:void(0)" data-toggle="modal" data-target="#addGroupMember">
          <i class="fal fa-plus"></i> Members({{count($groupUser->groupPerformers) + count($performer->GroupMembers)}})              
          </a> 

          <a class="hidden" id="showForIntenalStaff">
           Members({{count($groupUser->groupPerformers) + count($performer->GroupMembers)}})              
          </a>        
      </div>
      @endif
    </div>
    <div class="allbxousercover">
      @if(isset($groupUser->groupPerformers))
      @foreach($groupUser->groupPerformers as $key=> $groupUserVal)
    <div class="group_profileboxs1"> 
      @if(isset($groupUserVal->full_name))
      <div class="userdetabox primaryContact">       
        <div class="userdeta_right">        
          <p>{{$groupUserVal->full_name}}
          @if($key == 0)          
          <span>Primary Contact</span>
          @endif
          </p>
        </div>
      </div>
      @endif
      @if(isset($groupUserVal->age))
      <div class="userdetabox ">
        <div class="userdeta_lefticon">
          <i class="far fa-birthday-cake"></i>
        </div>
        <div class="userdeta_right">        
          <p>{{$groupUserVal->age}} yrs</p>          
        </div>
      </div>
      @endif
      @if(isset($groupUserVal->email))
      <div class="userdetabox linkboxset">
        <div class="userdeta_lefticon">
          <i class="far fa-envelope"></i>
        </div>
        <div class="userdeta_right">
           <p><a href = "mailto: {{$groupUserVal->email}}">{{$groupUserVal->email}}</a></p>
        </div>
      </div>
      @endif
       @if(isset($groupUserVal->phonenumber))
      <div class="userdetabox">
        <div class="userdeta_lefticon">
          <i class="far fa-phone"></i>
        </div>
        <div class="userdeta_right">
           <p>{{$groupUserVal->phonenumber}}</p>
        </div>
      </div>
      @endif
      @if(isset($groupUserVal->address))
      <div class="userdetabox">
        <div class="userdeta_lefticon">
          <i class="far fa-map-marked"></i>
        </div>
        <div class="userdeta_right">
          <p>{{$groupUserVal->address}}</p>
           <p>{{ $groupUserVal->city ?? '' }} , {{ $groupUserVal->province ?? '' }} ,{{ $groupUserVal->postalCode ?? '' }} </p>
        </div>
      </div>
      @endif 
      @if($key == 0)
    @if(isset($groupUserVal->instagram) && !empty($groupUserVal->instagram))      
      <div class="userdetabox linkboxset">
        <div class="userdeta_lefticon">
          <i class="fab fa-instagram"></i>
        </div>
        <div class="userdeta_right">
          <a href="https://www.instagram.com/{{$groupUserVal->instagram}}" target="_blank">https://www.instagram.com/{{$groupUserVal->instagram}}</a>
        </div>
      </div>
      @endif
      @if(isset($groupUserVal->twitter)  && !empty($groupUserVal->twitter))      
      <div class="userdetabox linkboxset">
        <div class="userdeta_lefticon">
          <i class="fab fa-twitter"></i>
        </div>
        <div class="userdeta_right">
          <a href="https://twitter.com/{{$groupUserVal->twitter}}" target="_blank">https://twitter.com/{{$groupUserVal->twitter}}</a>
        </div>
      </div>
      @endif
      @endif      
    </div>
    
    @endforeach
    @endif   

    
    <div class="memnerList group_profileboxs1 userliscoverset">
    @if(isset($performer->GroupMembers))
    @foreach($performer->GroupMembers as $key=> $val)
    <div id="memberList{{$val->id}}" class="userlisboxset">
        @if(isset($val->full_name))
        <div class="userdetabox">
          <div class="userdeta_lefticon">
            <i class="far fa-user"></i>
          </div>
          <div class="userdeta_right">
           <p>{{$val->full_name}}</p>
         </div>
       </div>    
       @endif
       @if(isset($val->member_email))
        <div class="userdetabox">
          <div class="userdeta_lefticon">
            <i class="far fa-envelope"></i>
          </div>
          <div class="userdeta_right">
           <p>{{$val->member_email}}</p>
         </div>
       </div>    
       @endif
       @if(isset($val->member_phone))
        <div class="userdetabox">
          <div class="userdeta_lefticon">
            <i class="far fa-phone"></i>
          </div>
          <div class="userdeta_right">
           <p>{{$val->member_phone}}</p>
         </div>
       </div>  
       @endif
       <div class="memberaction">

        <a href="javascript:void(0)" class="groupMemberEdit" data-editid="{{$val->id}}"><i class="fas fa-pencil"></i>
        </a>

        <a href="javascript:void(0)" data-toggle="confirmation" data-title="are you sure?" data-deleteid="{{$val->id}}"  class="deletememberconfirm"><i class="fas fa-trash"></i>
        </a> 

       </div> 
     </div>
    @endforeach
    @endif
    </div>
</div>
    </div>
  </div>
 
  <!-- Add Group Member -->
  <div class="modal fade newgallerymod noteseditmodal adduserboxpart" id="addGroupMember" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
       <!--  <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Group Member</h4>
        </div> -->
        <div class="modal-body">
          <div class="editsktoboxcover">
            <div class="titlandclobtn2">
              <h3>Add Member</h3>
                <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
              </div>
            <form id="addGroupMemberForm">
              <div class="enterskintonebox">                
                <div class="form-group">
                  <label for="usr">Member First Name</label>
                  <input type="text"  name="member_first_name" class="form-control" >
                </div>
                <div class="form-group">
                  <label for="usr">Member Last Name</label>
                  <input type="text"  name="member_last_name" class="form-control" >
                </div>
                <div class="form-group">
                  <label for="usr">Member Email</label>
                  <input type="email"  name="member_email" class="form-control" >
                </div>
                <div class="form-group">
                  <label for="usr">Member Phone</label>
                  <input type="text"  data-mask="(000) 000-0000" name="member_phone" class="form-control" >
                </div>
              </div>
            </form>
            <div class="modalfooter">
              <button type="button" id="groupMemberStore" class="btn btn-info" >Add Member</button>
            </div>
          </div>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" id="groupMemberStore" class="btn btn-info" >Add Member</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
    </div>
  </div>

  <!-- Edit Group Member -->
  <div class="modal fade newgallerymod noteseditmodal adduserboxpart" id="UpdateGroupMember" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Group Member</h4>
        </div> -->
        <div class="modal-body">
          <div class="editsktoboxcover">
            <div class="titlandclobtn2">
              <h3>Edit Member</h3>
              <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <form id="updateGroupMemberForm">
              <div class="enterskintonebox">
                <div class="form-group">
                  <input type="hidden" id="member_id"  name="id" class="form-control" >
                  <label for="usr">Member First Name</label>
                  <input type="text" id="member_first_name"  name="member_first_name" class="form-control" >
                </div>
                <div class="form-group">
                  <label for="usr">Member Last Name</label>
                  <input type="text"  id="member_last_name" name="member_last_name" class="form-control" >
                </div>
                <div class="form-group">
                  <label for="usr">Member Email</label>
                  <input type="email" id="member_email"  name="member_email" class="form-control" >
                </div>
                <div class="form-group">
                  <label for="usr">Member Phone</label>
                  <input type="text"  id="member_phone" name="member_phone" data-mask="(000) 000-0000" class="form-control" >
                </div>
              </div>
            </form>
            <div class="modalfooter">
              <button type="button" id="groupMemberUpdate" class="btn" >Update Member</button>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>






<script type="text/javascript">
  
  $("#groupMemberStore").click(function(){
    var FormData = $("#addGroupMemberForm").serialize();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
          url:"{{route('performers.addGroupMember')}}",
          type:"POST",
          data:FormData,
          success:function(data){
             if(data.status == true)
             {  
                $("#addGroupMember").modal("hide");
                 toastr.success(data.message);
                 setTimeout(function(){location.reload();}, 1500);
             }
          }
        });

  });

$('.deletememberconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
                DeleteMember($(this).data('deleteid'));
             },
           });

function DeleteMember(id)
{
      $.ajax({
          url:"{{route('performers.removeGroupMember')}}",
          type:"GET",
          data:{id:id},
          success:function(data){
            if(data.status == true)
            {
                $("#memberList"+id).remove();
                toastr.success(data.message);
                setTimeout(function(){location.reload();}, 1500);
            }
          }
      });
}

$(".groupMemberEdit").click(function(){
    id = $(this).data('editid'); 

    $.ajax({
        url:"{{route('performers.editGroupMember')}}",
        type:"GET",
        data:{id:id},
        success:function(data){
          if(data.status == true)
          {

            $("#member_id").val(data.member.id);
            $("#member_first_name").val(data.member.member_first_name);
            $("#member_last_name").val(data.member.member_last_name);
            $("#member_email").val(data.member.member_email);
            $("#member_phone").val(data.member.member_phone);
            $("#UpdateGroupMember").modal("show");
            
          }
        }
    });

});

$("#groupMemberUpdate").click(function(){

      var UpdateFormData =  $("#updateGroupMemberForm").serialize();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url:"{{route('performers.updateGroupMember')}}",
        type:"POST",
        data:UpdateFormData,
        success:function(data){
          if(data.status == true){
            
            toastr.success(data.message);

            setTimeout(function(){location.reload();}, 1500);

          }
        }
      });


});
</script>
