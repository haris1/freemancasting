
  <div class="titleboxeditbox">
    <h3>{{$Workperformer->resume_title}}</h3>
    <!-- <a href="javascript:void(0)"><i class="far fa-plus"></i></a> -->
  </div>
  <div class="pjtimrostendbox">
    <div class="pjtimroendbox1">
      <div class="inputbox1">
        <p>{{ $ProjectPerformer->project_title }}</p>
      </div>
      <div class="inputbox2">
        <p><a href="{{ $ProjectPerformer->IMDC_link }}" target="_blank">IMDC link</a></p>
      </div>
    </div>
      @foreach($ProjectRole as $role)
    <div class="pjtimroendbox2" id="removeProjectmodal{{$role->id}}">
      <div class="inputbox3">
        <p>{{$role->role_title}}</p>
      </div>
      <div class="inputbox4">
          <p>{{ date('d M Y',strtotime($role->start_date))}}</p>
      </div>
      <div class="inputbox5">
          <p>{{ date('d M Y',strtotime($role->end_date))}}</p>
      </div>

@if(Auth::guard('web')->check())  
      <div class="inputbox7">
      <a href="javascript:void(0)" data-toggle="confirmation" data-title="are you sure?" data-resumeid="{{$role->id}}"  class="deleteresumeconfirm"><i class="fas fa-trash"></i></a>
    </div>
    @endif
    </div>
    
    @endforeach
   
  </div>

