@extends('performers.layouts.app')
@section('content')
<div class="wctleftrightbox">
  @if($performer->applicationfor == "Group")
  @include('performers.layouts.groupSidebar')  
  @else
  @include('performers.layouts.sidebar')  
  @endif

  @if($authtype=='performer')     
  <input type="hidden" value="{{request()->route('id')}}" id="get_id" name="">
  @else
  <input type="hidden" value="{{request()->route('id')}}" id="get_id" name="">  
  @endif  
  
  <div class="rightallditinwct">
   <!--  <div class="searchbarboxfilt">
      <div class="srchbox">        
        <div class="form-group">
          <i class="far fa-search"></i>
          <input type="text" class="form-control" id="closetSearch" placeholder="Search">
        </div>
      </div>

      <div class="addandtagbox">
        <div class="addatribut">
          <a href="javascript:void(0)" type="button" data-toggle="modal" data-target="#myClosetModal"><i class="far fa-plus"></i> New Closet Item</a>
        </div>

        <div class="tagboxright">
          <a href="javascript:void(0)"><i class="far fa-tag"></i></a>
        </div>
      </div>
    </div> -->
    <div class="addflagshidbox">
      <div class="accordion-container">
        <div class="set hedtobox">
          <div class="srchbox">        
            <div class="form-group hidesearch">
              <i class="far fa-search"></i>
             <input type="text" class="form-control" id="closetSearch" placeholder="Search">
            </div>
          </div>
      @if($authtype=='performer')     
     @else
     @include('performers.flag')
     @endif      
        </div>      
      </div>
    </div>
   <div class="topbarbotdivcover">
     @if($authtype=='performer')
     @include('performers.layouts.tabbar')
     @else
     @include('performers.layouts.internal-tabbar')
     @endif
  @if(count($closet_list))
  <div id="defaultDisplayDiv" class="portfolboxcover">
    @foreach($closet_list as $closetValue)
    @if($closetValue->is_video == "no")
    <div class="hedfullmotobox" id="galleryDelete{{$closetValue->id}}">
      <div class="allboxtitlesem">
        <div class="titleboxallcover">
          <div class="titledivallbox"><h3><a href="javascript:void(0)"  onclick="viewGallery('{{ $closetValue['id'] }}','{{ $closetValue['closet_title'] }}')" >{{$closetValue['closet_title'] }}</a></h3></div>
          <div class="helpboxdcl">
            <span class="tool" data-tip="{{isset($helpdesk->closet_help) ? $helpdesk->closet_help : '' }}" tabindex="1"><i class="fal fa-question-circle"></i></span>
            
          </div>
          <div class="morbtnmnubox">
            <div class="morebtneddil">                
              <div class="MenuSceinain">
                <div class="menuheaderinmore">
                  <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
                  <div class="mainineropenbox_1">
                    <div class="mainineropenbox_2">
                      <div class="opensubmenua_1">
                        <a href="javascript:void(0)" id="{{$closetValue->id}}"  class="imageAddTogallery" onclick="imageAddTogallery('{{$closetValue->id}}')">Add</a>
                      </div> 
                       <div class="opensubmenua_1">
                         <a href="javascript:void(0)"  onclick="viewGallery('{{ $closetValue['id'] }}','{{ $closetValue['closet_title'] }}')" >Edit</a>
                       </div>
                       @if($closetValue['closet_title'] != "Winter" && $closetValue['closet_title'] != "Summer" && $closetValue['closet_title'] != "Business" && $closetValue['closet_title'] != "Spring" && $closetValue['closet_title'] != "Fall")
                      
                      <div class="opensubmenua_1">
                        <a href="javascript:void(0)" id="{{$closetValue->id}}"  class="gallaryconfirm"  data-toggle="confirmation" data-title="are you sure?" data-imagetagid="{{$closetValue->id}}" >Delete</a>
                      </div>
                      @endif 
                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>
          
            <div id="closettag{{$closetValue->id}}">
              @if(count($closetValue->closettag))
              @foreach($closetValue->closettag as $tagVal)
              <span class="btnremadd" id="TagRemove{{$tagVal->id}}">
                {{$tagVal->tag}} 
                <a href="javascript:void(0)" class="DeleteTag{{$tagVal->id }} imagetagdeleteconfirm"  data-toggle="confirmation" data-title="are you sure?" data-imagetagid="{{$tagVal->id }}" > <i class="fal fa-times"></i></a>
              </span>
              @endforeach
              @endif
            </div>
            <span class="AddTag" onclick="addTag({{$closetValue->id}})" id="{{$closetValue->id}}">Add Tag <i class="fal fa-plus"></i></span> 
            <span id="tagText_{{$closetValue->id}}" class="hidden tagtextltbox">
              <input type="" data-attid="{{$closetValue->id}}"  name="tag" id="tagval_{{$closetValue->id}}" class="tagstore{{$closetValue->id}} tagSave">
            </span>
         
        </div>
      </div>  
      <div class="gelimgleftbox">        
        <ul id="gelimgleftbox{{$closetValue->id}}" class="gelimgleftboxGallery">
        @if(count($closetValue['performermedia']))       
          @foreach($closetValue['performermedia'] as $key=>$mediaValue)         
          @if($key <=6)
          @if(strstr($mediaValue->media_type,"image"))
          <li id="closetMedia{{$mediaValue->id}}">
            <span  data-toggle="confirmation" data-title="are you sure?" data-imageid="{{$mediaValue->id}}"  class="deleteimageconfirm deleteimgbox"><i class="fas fa-trash"></i></span>
           <a href="{{$mediaValue->image_url}}"> <img src="{{$mediaValue->image_url}}" alt=""></a>
          </li>
          @else
           <li  id="closetMedia{{$mediaValue->id}}">
          <span data-toggle="confirmation" data-title="are you sure?" data-imageid="{{$mediaValue->id}}"  class="deleteimageconfirm deleteimgbox"><i class="fas fa-trash"></i></span>
          
          <img src="{{asset('assets/images/video.png')}}" alt="">
        
        </li> 
          @endif

           
          @endif 
          @endforeach
          @else
          <div class="gelimgleftbox">        
            
            <div class="noprojAvailable">
             <p>No Image</p>
            </div>
          </div>
          @endif
           </ul>
          @if(count($closetValue['performermedia']) > 7)    
      <div class="viewgallbox">
         @if($authtype=='performer')
        <a href="{{route('performers.view.closetgallery',['id'=>$closetValue->id])}}">View<br>Gallery<br>
          <span>
          {{count($closetValue['performermedia'])}} Images </span>
        </a>   
        @else
        <a href="{{route('internal-staff.view.closetgallery',['id'=>$closetValue->id,'pid'=>$per_id])}}">View<br>Gallery<br>
          <span>{{count($closetValue['performermedia'])}} Images </span>
        </a>   
        @endif
      </div>
      @endif
       
      </div>   
        
    </div>

    @endif

    @endforeach
    <!-- //video listing -->
    <!-- @foreach($closet_list as $closetValue)
    @if($closetValue->is_video == "yes")
    <div class="hedfullmotobox">
      <div class="allboxtitlesem">
        <div class="titleboxallcover">
          <h3>{{$closetValue['  closet_title'] }}</h3>
          <div class="morebtneddil">                
            <div class="MenuSceinain">
              <div class="menuheaderinmore">
                <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
                <div class="mainineropenbox_1">
                  <div class="mainineropenbox_2">
                    <div class="opensubmenua_1">
                      <a href="javascript:void(0)" id="{{$closetValue->id}}" class="videoAddTogallery" onclick="videoAddTogallery('{{$closetValue->id}}')" >Add </a>
                    </div> 

                  </div> 
                </div>
              </div>
            </div>
          </div>
          <div class="imgvidaodbox">
            <div id="closetMedia{{$closetValue->id}}">
              @if(count($closetValue->closettag))
              @foreach($closetValue->closettag as $tagVal)
              <span class="btnremadd" id="TagRemove{{$tagVal->id}}">
                {{$tagVal->tag}} 
                <a href="javascript:void(0)" onclick="tagDelete('{{$tagVal->id }}')"> <i class="fal fa-times"></i></a>
              </span>
              @endforeach
              @endif
            </div>
            <span class="AddTag" onclick="addTag({{$closetValue->id}})" id="{{$closetValue->id}}">Add Tag <i class="fal fa-plus"></i></span> 
            <span id="tagText_{{$closetValue->id}}" class="hidden tagtextltbox">
              <input type="" data-attid="{{$closetValue->id}}"  name="tag" id="tagval_{{$closetValue->id}}" class="tagstore{{$closetValue->id}} tagSave" >
            </span>
          </div>
      </div> 
      <div class="audiorealboxp"> 

        <div class="videoplaybtnbox"> 
          @if(count($closetValue['performermedia']))        
          @foreach($closetValue['performermedia'] as $mediaValue)         
          <div class="btnplaybox" id="closetMedia{{$mediaValue->id}}">
            <a href="javascript:void(0)" data-key="{{$mediaValue->id}}" onclick="showClosetVideo('{{$mediaValue->id}}')" class="showClosetVideo">
              <div class="playiconbtn"><i class="fas fa-play"></i></div> 
              <span class="medtitleleft">{{$mediaValue->media_title}}</span> 
              <span class="dateformright">{{$closetValue->created_at}}</span>              
            </a>
            <div class="rigtmorebtnboxmid">          
              <div class="morebtneddil">                
                <div class="MenuSceinain">
                  <div class="menuheaderinmore">
                    <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
                    <div class="mainineropenbox_1">
                      <div class="mainineropenbox_2">
                       <div class="opensubmenua_1">
                        <a href="javascript:void(0)" data-toggle="confirmation" data-title="are you sure?" data-videoid="{{$mediaValue->id}}"  class="deletevideoconfirm">Delete</a>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        @endforeach
        @else
        <div class="gelimgleftbox">        
          
          <div class="noprojAvailable">
           <p>No Audio</p>
          </div>
        </div>
        @endif
      </div>
    </div>   
  </div>
</div>
@endif
@endforeach -->
<div class="addbtnbotbox">
    <button data-toggle="modal" data-target="#myClosetModal"><i class="far fa-plus"></i> New Closet Item</button>
  </div>
</div>
@else
<div class="hedfullmotobox">
  <div class="gelimgleftbox">        
    <p>No Closet available</p>
  </div>
</div>
@endif
</div>



<!--upload image closet  model -->
<div class="modal fade newgallerymod" id="myClosetModal" role="dialog">
 <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-body">
       <div class="GalleryBoxinerdeta">
        
        <form method="post" action="" enctype="multipart/form-data" id="closetGallery"> 
             @csrf    
         <div class="titlandclobtn">
           <h3>New Closet Gallery</h3>
           <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
         </div>
         <!--  <select id="select_upload" name="is_video">
          <option value="ImageUpload">Image Upload</option>
          <option value="VideoUpload">Video Upload</option>
        </select> -->
         <div class="form-group inputbixin">
           <input type="text" class="form-control   closet_title" id="closet_title" name="closet_title" placeholder="Title Gallery">
         </div>
         <div class="form-group textarbox">
           <textarea class="form-control" id="" name="description" placeholder="Add a description"></textarea>
         </div>
         <div class="dropboxzoniner">        

          <div id="Image_upload_dropzone">
            <!-- image or video both upload dropzone --> 
            <div method="post" action="{{route('performers.store.closet.images')}}" enctype="multipart/form-data" 
            class="dropzone" id="dropzone">          
          </div>
        </div>

        <!-- <div id="video_upload_dropzone">
          
          <div method="post" action="{{route('performers.store.closet.video')}}" enctype="multipart/form-data" 
          class="dropzone" id="Videodropzone">          
        </div>
      </div> -->
    </div>
         <div class="form-group imgtitleboz">
           <div class="tagandimgtitle">
        <div class="leftimgtitlebox">
          <div class="form-group">
              <input type="text" class="form-control" id="media_title" name="media_title" placeholder="Image Title">
          </div>
        </div>
        <div class="rightaddtagboxset">
            <div class="boxTags">
                <div class="taglist">                  
                </div>
                <span class="addTagCreate">Add Tag <i class="fal fa-plus"></i></span>
            </div>            
        </div>
    </div>
         </div>
       </div>
       <div class="imgaddedboxi">

       </div>
       <div class="submitbtnbox">
       <input type="button" id="addcloset_image" name="" value="submit" class="btn btn-primary">
       <!-- <input type="button" id="addcloset_video" name="" value="submit" class="btn btn-primary"> -->
     </div>
     </form>
     </div>
   </div>
 </div>
</div>

<!-- video show model -->
<div class="modal fade newgalrymodvideo" id="videoDispalyModal" role="dialog">
 <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-body">
       <div class="Galleryvideoboxinr">        

        <div class="titlandclobtn">
         <h3>video</h3>
         <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>         
       </div>      
       <div class="video_dispay" >
        
         
        </div>    
      </div>
    </div>
  </div>
</div>
</div>

<!-- add image modal -->
 <div class="modal fade newgallerymod" id="addImage" role="dialog">
 <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-body">
       <div class="GalleryBoxinerdeta">        
        <form method="post" action="" enctype="multipart/form-data" id="closetaddImage"> 
         @csrf    
        
        <div class="titlandclobtn">
         <h3>Add Closet Media </h3>
         <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
       </div>      
       <div class="form-group inputbixin">
         <input type="hidden" class="form-control" id="addImage_closet_id" name="closet_id">
       </div>

       <div class="dropboxzoniner">        

        <div id="Image_upload_dropzone">
          <!-- image or video both upload dropzone new update  --> 
          <div method="post" action="{{route('performers.store.closet.images')}}" enctype="multipart/form-data" 
          class="dropzone" id="AddImagedropzone1">          
        </div>
      </div>
    </div>
 <div class="tagandimgtitle">
        <div class="leftimgtitlebox">
          <div class="form-group">
              <input type="text" class="form-control" id="media_title" name="media_title" placeholder="Image Title">
          </div>
        </div>
        <div class="rightaddtagboxset">
            <div class="boxTags">
                <div class="taglist">                  
                </div>
                <span class="addTagCreate">Add Tag <i class="fal fa-plus"></i></span>
            </div>            
        </div>
    </div>
</div>
<div class="imgaddedboxi">

</div>
<div class="submitbtnbox">
<input type="button" id="closet_image_add" name="" value="submit" class="btn btn-primary">
</div>
</form>
</div>
</div>
</div>
</div>

<!-- Add video modal -->
 <div class="modal fade newgallerymod" id="addVideo" role="dialog">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-body">
         <div class="GalleryBoxinerdeta">        
          <form method="post" action="" enctype="multipart/form-data" id="closetaddVideo"> 
           @csrf 
           <div class="titlandclobtn">
             <h3>Add Video</h3>
             <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
           </div>      
           <div class="form-group">
             <input type="hidden" class="form-control" id="video_closet_id" name="" >
           </div>

           <div class="dropboxzoniner">        

            <div id="Video_upload_dropzone">
              <!-- image upload dropzone --> 
              <div method="post" action="{{route('performers.store.closet.video')}}" enctype="multipart/form-data" 
              class="dropzone" id="AddVideodropzone">          
            </div>
          </div>
        </div>
        <div class="form-group imgtitleboz">
         <input type="text" class="form-control" id="media_title2" name="media_title" placeholder="Video Title">
       </div>
     </div>
     <div class="imgaddedboxi">

     </div>
     <div class="submitbtnbox">
      <input type="button" id="closet_video_add" name="" value="submit" class="btn btn-primary">
    </div>
  </form>
</div>
</div>
</div>
</div>
@endsection
@include('performers.commonCloset')
@section('scripts')
<script type="text/javascript">
  $( document ).ready(function() {
    /** ADD TAG IN CREATE PROFILO **/
    $('.addTagCreate').on('click',function(){
        var tag = $('.inputTag').val();
        if (tag != "") {
            $(this).after('<span class="boxInputTag"><input type="text" data-status="tag" id="tag0" data-id="0" class="inputTag"></span>');

            $(".inputTag").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "{{route('performers.portfolio.getTagsList')}}",
                        type:'GET',
                        data: {
                            tags: request.term,
                            //id:$(this.element.get(0)).attr('data-attid')
                        },
                        success: function( data ) {
                            if(data.status == true){
                                if(data.html.length > 0){
                                    response(data.html);
                                    response($.map(data.html, function (el) {

                                    return {
                                        label: el.tag,
                                        id: el.id,
                                        //protfolio_id: data.protfolio_id
                                     };
                                   }));
                                } else {
                                    $('.inputTag').val('');
                                }

                            }
                        }
                    });
                },select: function( event, ui ) {
                    event.preventDefault();
                    $(".taglist").append('<span class="tagRemove" >'+ui.item.label+'<a href="javascript:void(0)"  class="thistagDelete"><i class="fal fa-times"></i><input type="hidden" value="'+ui.item.label+'" name="tags[]"></a></span>');
                    $('.boxInputTag').remove();
                }
            });
        }  
    })

   $('.imagetagdeleteconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
                tagDelete($(this).data('imagetagid'));
             },
           });
   
   $('.gallaryconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
                DeleteGallery($(this).data('imagetagid'));
             },
           });
    /** END TAG IN CREATE PROFILO **/

    
    $("#video_upload_dropzone").hide();
    $("#addcloset_video").hide();
   $('select').on('change', function() {
       if(this.value == "VideoUpload")       {
        $("#video_upload_dropzone").show();
        $("#Image_upload_dropzone").hide();
        $("#addcloset_video").show();
        $("#addcloset_image").hide();
        $("#media_title").attr("placeholder", "video title");
       }else       {
          $("#video_upload_dropzone").hide();
          $("#Image_upload_dropzone").show();
          $("#addcloset_video").hide();
          $("#addcloset_image").show();
          $("#media_title").attr("placeholder", "Image title");
       }
    });
});

</script>
<script type="text/javascript">
      var uploadedDocumentMap = {};
      Dropzone.options.dropzone =
         {
            maxFilesize: 12,
            renameFile: function(file) {
                var dt    = new Date();
                var time  = dt.getTime();
                return time+file.name;
            },
            type:"post",
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            acceptedFiles: ".jpeg,.jpg,.png,.gif,video/*",
            addRemoveLinks: true,
            timeout: 5000,

            success: function(file, response) 
            {            
              console.log(response);
            $('#closetGallery').append('<input type="hidden" name="images[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
            uploadedDocumentMap[file.name] = response;              
            },
            error: function(file, response)
            {
              console.log(response);
               return false;
            }
};

$('#addcloset_image').click(function(){

    formdata = $('#closetGallery').serialize();
   var closetTitle = $("#closetGallery #closet_title").val();

    if(closetTitle == "")
    {
      toastr.error('Enter Closet Title');
      return false;
    }
      
      //$(this).prop('disabled', true);
      $.ajaxSetup({
       headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
     });
      $.ajax({
          url:"{{route('performers.store.closet.media')}}",
          type:"POST",
          data:formdata,
          success: function(response)
          {
             if(response.status == true)
               {
                location.reload();
                toastr.success(response.message);
               }else{
                toastr.error(response.message);
               }
          }
      });
});
</script>

<!-- video upload dropzone -->
<script type="text/javascript">
$(document).ready(function(){
  $("#select_upload").select2();
});

  var uploadedDocumentMap = {};
  Dropzone.options.Videodropzone =
  {
    maxFilesize: 100,
    renameFile: function(file) {
      var dt    = new Date();
      var time  = dt.getTime();
      return time+file.name;
    },
    type:"post",
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    acceptedFiles: ".mp4,.wmv,.flv,.avi",
    addRemoveLinks: true,
    timeout: 5000,

    success: function(file, response) 
    {            
      console.log(response);
      $('#closetGallery').append('<input type="hidden" name="images[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
      uploadedDocumentMap[file.name] = response;              
    },
    error: function(file, response)
    {
      console.log(file);
      return false;
    }
  };

  $('#addcloset_video').click(function(){

    formdata = $('#closetGallery').serialize();

    var closetTitle = $("#closet_title").val();

    if(closetTitle == "")
    {
      toastr.error('Enter Closet Title');
      return false;
    }
      
    $(this).prop('disabled', true);
     $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
    $.ajax({
      url:"{{route('performers.store.closet.media')}}",
      type:"POST",
      data:formdata,
      success: function(response)
      {
       if(response.status == true)
       {        
         location.reload();
         toastr.success(response.message);
      }else{
        console.log(response);
        toastr.error(response.message);
      }
    }
  });
  });


  // $(".showClosetVideo").click(function(){
    function showClosetVideo(id)
    {
  id = id;

    $.ajax({
      url:"{{route('performers.closet.video.show')}}",
      type:"GET",
      data:{id:id},
      success:function(response){
        if(response.status==true){

          $(".video_dispay").html("");
                  
          $("#videoDispalyModal").modal('show');
          $(".video_dispay").html('<video width="600" height="300" controls class=""><source  src="'+response.data.image_url+'" type="video/mp4">Your browser does not support the video tag.</video>');   
          
        }
        

      },
    error: function(file, response)
    {
     
    }

    });
}
// });


  //image add to gallery modal
function imageAddTogallery(id)
{ 
   // id =$(this).attr('id');
   $("#addImage_closet_id").val(id);
   $("#addImage").modal('show');


  
}



   var uploadedDocumentMap = {};
  Dropzone.options.AddImagedropzone1 =
  {
    maxFilesize: 100,
    renameFile: function(file) {
      var dt    = new Date();
      var time  = dt.getTime();
      return time+file.name;
    },
    type:"post",
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    acceptedFiles: ".jpeg,.jpg,.png,.gif,video/*",
    addRemoveLinks: true,
    timeout: 5000,

    success: function(file, response) 
    {            
      console.log(response);
      $('#closetaddImage').append('<input type="hidden" name="images[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
      uploadedDocumentMap[file.name] = response;              
    },
    error: function(file, response)
    {
      console.log(file);
      return false;
    }
  };
 $('#closet_image_add').click(function(){

    formdata = $('#closetaddImage').serialize();   
      
    $(this).prop('disabled', true);

     $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
    $.ajax({
      url:"{{route('performers.add.closet.images')}}",
      type:"POST",
      data:formdata,
      success: function(response)
      {
       if(response.status == true)
       {
        location.reload();
        toastr.success(response.message);
      }else{
        toastr.error(response.message);
      }
    }
  });
  });
  $('.tagSave').focusout(function(){
    if(jQuery(this).val()==''){
    jQuery(this).parent('.tagtextltbox').addClass('hidden');
    }
  });
//add video dropzone code
  var uploadedDocumentMap = {};
  Dropzone.options.AddVideodropzone =
  {
    maxFilesize: 100,
    renameFile: function(file) {
      var dt    = new Date();
      var time  = dt.getTime();
      return time+file.name;
    },
    type:"post",
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    acceptedFiles: ".mp4,.wmv,.flv,.avi",
    addRemoveLinks: true,
    timeout: 5000,

    success: function(file, response) 
    {            
      console.log(response);
      $('#closetaddVideo').append('<input type="hidden" name="videos[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
      uploadedDocumentMap[file.name] = response;              
    },
    error: function(file, response)
    {
      console.log(file);
      return false;
    }
  };


//video add
 function videoAddTogallery(id)
 {
   id =$(this).attr('id');
   $("#video_closet_id").val(id);
   $("#addVideo").modal('show');


   //video add btn
   $("#closet_video_add").click(function(){
   
    formdata = $('#closetaddVideo').serialize(); 
    $(this).prop('disabled', true);  
    $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
    $.ajax({
      url:"{{route('performers.add.closet.video')}}",
      type:"POST",
      data:formdata,
      success: function(response)
      {
       if(response.status == true)
       {
        location.reload();
        toastr.success(response.message);
      }else{
        toastr.error(response.message);
      }
    }
  });
  });   
     
}


function addTag(c_id)
 {
  
  closet_id =c_id;
  tag ='#tagval_'+c_id;
  id = "#tagText_"+c_id;
  tag = ".tagstore"+c_id;  
  $(id).removeClass('hidden');     
 }
 
//  $(".AddTag").click(function(){
//   closet_id =$(this).attr('id');
//   tag ='#tagval_'+$(this).attr('id');
//   id = "#tagText_"+$(this).attr('id');
//   tag = ".tagstore"+$(this).attr('id');
  
//   $(id).removeClass('hidden');  
// tag_val =$(this).val();
  
// });


//$(document).on('focusout', '.tagSave', function() {
function storeTags(id,tag){
// $('.tagSave').focusout(function(){
  var tag_val = tag;

       if(tag_val == ""){
        $(id).addClass('hidden');  
        return false;
      }
     
     var closet_id=id;      
      
          $.ajaxSetup({
               headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
             });
            $.ajax({
                url:"{{route('performers.closet.addtag')}}",
                type:"post",
                data:{closet_id:closet_id,tag:tag_val},
                success:function(response){
                 if(response.status == true)
                 {
                   // location.reload(2000);
                   $("#closettag"+closet_id).append('<span class="btnremadd imagetagdeleteconfirm" id="TagRemove'+response.data.id+'">'+response.data.tag+'<a href="javascript:void(0)" class="tagDelete DeleteTag'+response.data.id+' imagetagdeleteconfirm" data-toggle="confirmation" data-title="are you sure?" data-imagetagid='+response.data.id+'> <i class="fal fa-times"></i></a></span>');
                    toastr.success(response.message);
                    $(tag).val("");
                   $(id).addClass('hidden');
                    $('.tagSave').val('');
                    $('.tagtextltbox').addClass('hidden');
                    $('.imagetagdeleteconfirm[data-toggle=confirmation]').confirmation({
                              rootSelector: '[data-toggle=confirmation]',
                              container: 'body',
                              onConfirm: function() {
                                 tagDelete($(this).data('imagetagid'));
                              },
                            });
                 }},
                  error: function(file, response)
                  {
                    console.log(file);
                    return false;
                  }
                
            });
          
  }
//  $(".tagDelete").click(function(){
//   tag_id = $(this).data('id');
//   $(this).parent().remove();

//     $.ajax({
//       url:"{{route('performers.closet.removetag')}}",
//       type:'get',
//       data:{id:tag_id},
//       success:function(data){        
//         if(data.status == true)
//         {   
//           toastr.success(data.message);     
//         }      
//       }
//     });
// });

 $(document).ready(function(){
  $('.gelimgleftboxGallery').imageview();
    $("#closetSearch").keyup(function(){
    search = $(this).val();
    get_id = $("#get_id").val();   
     $.ajax({
        url:"{{route('performers.closet.search')}}",
        type:'GET',
        data:{search:search,get_id:get_id},
        success:function(data)
        {
          if(data.status == true){        
            $("#defaultDisplayDiv").html(data.html);
            $('.gelimgleftboxGallery').imageview();
            $('.deleteimageconfirm[data-toggle=confirmation]').confirmation({
                     rootSelector: '[data-toggle=confirmation]',
                     container: 'body',
                     onConfirm: function() {
                        closetMediaDelete($(this).data('imageid'));
                     },
                   });
            $('.imagetagdeleteconfirm[data-toggle=confirmation]').confirmation({
                      rootSelector: '[data-toggle=confirmation]',
                      container: 'body',
                      onConfirm: function() {
                         tagDelete($(this).data('imagetagid'));
                      },
                    });
            $('.tagSave').focusout(function(){
              if(jQuery(this).val()==''){
              jQuery(this).parent('.tagtextltbox').addClass('hidden');
              }
            });
            $(".tagSave").autocomplete({
                //source: availableTags
                source: function(request, response) {
                    $.ajax({
                        url: "{{route('performers.closet.getTags')}}",
                        type:'GET',
                        data: {
                            tags: request.term,
                            id:$(this.element.get(0)).attr('data-attid')
                        },
                        success: function( data ) {
                            if(data.status == true){
                                if(data.html.length > 0){
                                    //response(data.html);
                                    response($.map(data.html, function (el) {

                                     return {
                                       label: el.tag,
                                       id: el.id,
                                       closet_id: data.closet_id
                                     };
                                   }));
                                } else {
                                   // $('.tagSave').val('');
                                       var result = [{ label: "no results", value: response.term }];
                                   response(result);
                                }

                            }
                        }
                    });
                },
            change: function (event, ui) {
                if (ui.item == null){ 
                 //here is null if entered value is not match in suggestion list
                   //toastr.warning("click on add Agent if you want add new Value");
                    $(this).val((ui.item ? ui.item.id : ""));
                }
            },select: function( event, ui ) {
                    event.preventDefault();
                   // console.log();
                   // var id = $(this.element.get(0)).attr('data-attid');
                   var label = ui.item.label;
                               if (label === "no results") {

                               }
                               else{
                    $('.tagSave').val(ui.item.label);
                    var id = ui.item.closet_id;
                    var tag = ui.item.label;
                    storeTags(id,tag);
                  }
                }
            });
            }
        }
      });

  });
});


function tagDelete(id)
{ 
  $("#TagRemove"+id).remove();
    $.ajax({
      url:"{{route('performers.closet.removetag')}}",
      type:'get',
      data:{id:id},
      success:function(data){        
        if(data.status == true)
        {   
          toastr.success(data.message);     

        }      
      }
    });
}

function closetMediaDelete(id)
{  
  $("#closetMedia"+id).remove();
  $.ajax({
         url:"{{route('performers.closet.removeMedia')}}",
        type:'get',
        data:{id:id},
        success:function(response){
            if(response.status == true)
            {
              toastr.success(response.message);                   
            }
        }

  });
  
}


$("#showGalleryModal").on("hidden.bs.modal", function () {
    location.reload();
});
    /** auto complated tags ajax base **/
    $(".tagSave").autocomplete({
        //source: availableTags
        source: function(request, response) {
            $.ajax({
                url: "{{route('performers.closet.getTags')}}",
                type:'GET',
                data: {
                    tags: request.term,
                    id:$(this.element.get(0)).attr('data-attid')
                },
                success: function( data ) {
                    if(data.status == true){
                        if(data.html.length > 0){
                            //response(data.html);
                            response($.map(data.html, function (el) {

                             return {
                               label: el.tag,
                               id: el.id,
                               closet_id: data.closet_id
                             };
                           }));
                        } else {
                           // $('.tagSave').val('');
                               var result = [{ label: "no results", value: response.term }];
                           response(result);
                        }

                    }
                }
            });
        },
    change: function (event, ui) {
        if (ui.item == null){ 
         //here is null if entered value is not match in suggestion list
           //toastr.warning("click on add Agent if you want add new Value");
            $(this).val((ui.item ? ui.item.id : ""));
        }
    },select: function( event, ui ) {
            event.preventDefault();
           // console.log();
           // var id = $(this.element.get(0)).attr('data-attid');
           var label = ui.item.label;
                       if (label === "no results") {

                       }
                       else{
            $('.tagSave').val(ui.item.label);
            var id = ui.item.closet_id;
            var tag = ui.item.label;
            storeTags(id,tag);
          }
        }
    });
    $('.deleteimageconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
                closetMediaDelete($(this).data('imageid'));
             },
           });
    $('.deletevideoconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
              closetMediaDelete($(this).data('videoid'));
             },
           });


    //Delete Gallery
function DeleteGallery(id)
{
   $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
  $.ajax({
       url:"{{route('performers.closetGalleryRemove')}}",
       type:"post",
       data:{id:id},
       success:function(data)
       {
         if(data.status == true)
         {
            $("#galleryDelete"+id).remove();
            toastr.success(data.message);

         }
       }

  });

}

 $(document).on("click", ".thistagDelete", function() { 
      $(this).parent().remove(); 
 });


</script>

@endsection