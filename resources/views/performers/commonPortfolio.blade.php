<?php
    $tag ='';
    if(isset($gallery->performerMedia)){

      foreach ($gallery->performerMedia as $key => $value) {       
    if(!empty($value->tag)){
    
      $tag = $tag .','.$value->tag;
    }
    }
}else{
}
    $tags = explode(',', $tag);
   $tags= array_unique(array_filter($tags));
?>
<div class="hidden" id="appDtataAppend">
    <agile class="main" ref="main" :options="options1" :as-nav-for="asNavFor1">
                <div class="slide" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`"><img :src="slide"/></div>
              </agile>
              <div class="lftinttileditbox">            
                <div class="form-group imgtitleboz">            
                  <input type="text" class="form-control" id="" placeholder="Image Tile">
                </div>
                <div class="rightedittagbox">

                    @if(isset($gallery->portfolioTag))
                    @foreach($gallery->portfolioTag as $tagVal)
                    <!-- <span class="btnremadd" id="TagRemoveModel{{$tagVal->id}}">
                     
                      <a href="javascript:void(0)" class="DeleteTag{{$tagVal->id }} imagetagdeleteconfirm"  data-toggle="confirmation" data-title="are you sure?" data-imagetagid="{{$tagVal->id }}"> <i class="fal fa-times"></i></a>
                  </span> -->
                    @endforeach
                    @endif
                    <div class="tagDisplay">
                        
                   </div>
                   <div class="tagbox" id="AddlistingTag">
                       
                   </div>

                  <div class="tagbox">                                               
                    @foreach($tags as $val)
                    <span class="btnremadd" id="TagRemoveModel">   
                        {{$val}}                       
                    </span>
                    @endforeach                        
                    <a href="javascript:void(0)">Tag</a>
                  </div>


                 <!--  <div class="editbox">
                   <a href="javascript:void(0)"><i class="far fa-edit"></i></a>
                 </div> -->
               </div>
             </div>
             <div class="leftrightaggthumbox">
                <agile class="thumbnails" ref="thumbnails" :options="options2" :as-nav-for="asNavFor2">
                    <div class="slide slide--thumbniail" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`" @click="$refs.thumbnails.goTo(index)"><img :src="slide"/></div>
                    <template slot="prevButton"><i class="fas fa-chevron-left"></i></template>
                    <template slot="nextButton"><i class="fas fa-chevron-right"></i></template>
                </agile>
              <div class="drprightboxiner">
                <div class="droplisingimg">
                    <!-- <div id="">
                        <div class="dropzone needsclick btnAddPortfolioGallery">
                            <div class="dz-message needsclick">
                                <div class="lefimgadd">
                                    <i class="far fa-plus"></i> <p>image</p>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <a href="javascript:void(0)" class="btnAddPortfolioGallery" >
                        <i class="far fa-plus"></i> <p>image</p>
                    </a>
                  </div>
              </div>
            </div>
            
</div>
<div class="modal fade newgallerymod" id="showGalleryModal" role="dialog">
  <div class="modal-dialog">    
    <!-- Modal content-->
    <div class="modal-content">      
      <div class="modal-body">
        <div class="Gallerydetaaddbox">
          <div class="titlandclobtn">
            <h4></h4>
            <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
          </div>
          
          <div class="sliderpartinerset">  

            <div id="app" class="gallryappbox">
              <agile class="main" ref="main" :options="options1" :as-nav-for="asNavFor1">
                <div class="slide" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`"><img :src="slide"/></div>
              </agile>
              <div class="lftinttileditbox">            
                <div class="form-group imgtitleboz">            
                  <input type="text" class="form-control" id="" placeholder="Image Tile">
                </div>
                <div class="rightedittagbox">
                  <div class="tagbox">

                    <a href="javascript:void(0)">Tag</a>
                  </div>
                 <!--  <div class="editbox">
                   <a href="javascript:void(0)"><i class="far fa-edit"></i></a>
                 </div> -->
               </div>
             </div>
             <div class="leftrightaggthumbox">
                <agile @afterChange="showCurrentSlide()" class="thumbnails" ref="thumbnails" :options="options2" :as-nav-for="asNavFor2">
                    <div class="slide slide--thumbniail" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`" @click="$refs.thumbnails.goTo(index)"><img :src="slide"/></div>
                    <template slot="prevButton"><i class="fas fa-chevron-left"></i></template>
                    <template slot="nextButton"><i class="fas fa-chevron-right"></i></template>
                </agile>
              <div class="drprightboxiner">
                <div class="droplisingimg">
                    <a href="javascript:void(0)" class="btnAddPortfolioGallery" >
                        <i class="far fa-plus"></i> <p>image</p>
                    </a>
                </div>
              </div>
            </div>
            
          </div>
        </div>                   
      </div>
    </div>
  </div>      
</div>
</div>



<div class="modal fade newgallerymod" id="addPortfolioGallery" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="GalleryBoxinerdeta">
                    <!-- {{route('performers.store.portfolio.media')}} -->
                    <form method="post" enctype="multipart/form-data" id="updatePortfolioGallery"> 
                        @csrf 
                        <div class="titlandclobtn">
                            <h3>Edit Gallery</h3>
                            <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
                        </div>
                        
                        <div class="form-group inputbixin">
                            <input type="text" class="form-control pt_hidden" id="portfolio_title" name="portfolio_title" placeholder="Title Gallery" value="{{ (isset($gallery['portfolio_title'])?$gallery['portfolio_title']:'') }}">
                        </div>
                        <div class="dropboxzoniner">
                            <div id="Image_upload_dropzone">
                                <!-- image upload dropzone --> 
                                <div method="post" action="{{ route('performers.store.portfolio.images') }}" enctype="multipart/form-data" 
                                class="dropzone" id="AddImagedropzonePlus">          
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="gallery_id" id="gallery_id" value="{{ (isset($gallery['id'])?$gallery['id']:'') }}">
                        <div class="form-group imgtitleboz">
                            <div class="tagandimgtitle">
        <div class="leftimgtitlebox">
          <div class="form-group">
              <input type="text" class="form-control" id="media_title" name="media_title" placeholder="Image Title">
          </div>
        </div>
        <div class="rightaddtagboxset">
            <div class="boxTags">
                <div class="taglist">                  
                </div>
                <span class="addTagCreate">Add Tag <i class="fal fa-plus"></i></span>
            </div>            
        </div>
    </div>
                        </div>
                        <div class="submitbtnbox">
                        <input type="submit" id="addPortfolio" name="" value="submit" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

      //DROPZONE
        // $("#dropzone").dropzone({ dictDefaultMessage: "Drop your primary image to upload" });
        var uploadedDocumentMap = {};
        Dropzone.options.AddImagedropzonePlus =
        {
            maxFilesize: 100,
            renameFile: function(file) {
                var dt    = new Date();
                var time  = dt.getTime();
                return time+file.name;
            },
            type:"POST",
            //headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            timeout: 5000,

            success: function(file, response) 
            {            
                console.log(response);
                $('#updatePortfolioGallery').append('<input type="hidden" name="images[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');

                //$('input[name="images[]"]').array_push(response.data.name);
                uploadedDocumentMap[file.name] = response;              
            },
            error: function(file, response)
            {
                console.log(file);
                return false;
            }
        };

    $(document).on("click",".btnAddPortfolioGallery",function(e) {
        myarray = ["Headshots","Full Body Shots","Tattos","Piercings"]; 
            var pt_title = $(".pt_hidden").val();
            if(jQuery.inArray(pt_title, myarray) != -1) {
               $('.pt_hidden').attr('readonly', true);
            } 

        $('#addPortfolioGallery').modal('show');
        $('#showGalleryModal').modal('hide');    
         setTimeout(function(){
            $('body').addClass('modal-open');
            
        },1000)
    });

    $('#grid2').addClass('effect-2');

    $(window).load(function(){
        //slides_image = $('#hidden_slide').val();
        // var $container = $('.grid');
        // // initialize
        // $container.masonry({
        // //columnWidth: 200,
        // itemSelector: '.item'
        // })

      //  $('.item > a').removeAttr('href')
    
        //$('#portfolioGallery').on('submit',(function(e) {
        $(document).on('submit','#updatePortfolioGallery',(function(e) {
          
            e.preventDefault();
            var formData = new FormData(this);
            var portfolioTitle = $("#updatePortfolioGallery #portfolio_title").val();

            if(portfolioTitle == "")
            {
              toastr.error('Enter Portfolio Title');
              return false;
            }
            $.ajax({
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:'{{ route("performers.portfolioGalleryUpdate") }}',
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                beforeSend:function(){},
                success:function(response) {
                    if (response.status == true) {
                        toastr.success(response.message);
                        $('#addPortfolioGallery').modal('hide');
                          location.reload();
                        //$('#showGalleryModal').modal('show');
                        viewGallery($('#gallery_id').val());
                        
                        setTimeout(function(){
                            $('body').addClass('modal-open');
                            
                        },1000)
                    } else {
                        toastr.error(response.message);
                    }
                },
                complete:function(){},
                error:function(){},
            
            });
        }));
    });


function viewGallery(id,title){

    $('#gallery_id').val(id);    
    $('input[name="portfolio_title"]').val(title);

    var slideImage='';
    
    $.ajax({
        url:"{{route('performers.view.gallery.images')}}",
        type:"GET",
        data:{id:id},
        success: function(response)
        {
            if(response.status == true)
            {

                slideImage = response.slide;
                jQuery('#showGalleryModal').modal('show');
                jQuery('#app').html(jQuery('#appDtataAppend').html());    
            }
            

         },error: function(response)
        {
            
            console.log(response);
            return false;
        }

    });  
    jQuery('#showGalleryModal').modal('show');
    setTimeout(function(){         
         Vue.use(VueAgile);
        app = new Vue({
            el: '#app',
            components: {
                agile: VueAgile
            },
            data() {
                return {
                    asNavFor1: [],
                    asNavFor2: [],
                    options1: {
                        dots: false,
                        fade: true,
                        navButtons: false },
                        options2: {
                            autoplay: false,
                            centerMode: false,
                            dots: false,
                            infinite : false,
                            navButtons: false,
                            slidesToShow: 5,
                            responsive: [
                            {
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 5
                                }
                            },
                            {
                                breakpoint: 1000,
                                settings: {
                                    navButtons: true
                                }
                            }]
                        },

                        slides: slideImage
                    };
                },
               
                mounted() {
                        this.asNavFor1.push(this.$refs.thumbnails);
                        this.asNavFor2.push(this.$refs.main);
                    }
                });
   
        $('.imagetagdeleteconfirm[data-toggle=confirmation]').confirmation({
           rootSelector: '[data-toggle=confirmation]',
           container: 'body',
           onConfirm: function() {
            $("#TagRemoveModel"+ $(this).data('imagetagid')).remove();
            tagDelete($(this).data('imagetagid'));
        },
    });
   function tagDelete(id)
        {   
           $("#TagRemoveModel"+id).remove();
           
            $.ajax({
              url:"{{route('performers.portfolio.removetag')}}",
              type:'get',
              data:{id:id},
              success:function(data){

                if(data.status == true)
                {   

                  toastr.success(data.message);     

              }      
          }
      });
        }

    }, 1000);
        

   
}
function showCurrentSlide(){
    
    //alert(1);
    // Shows for example: { currentSlide: 1 }
}


//credate tag
 $('.addTagCreate').on('click',function(){
        var tag = $('.inputTag').val();
        if (tag != "") {
            $(this).after('<span class="boxInputTag"><input type="text" data-status="tag" id="tag0" data-id="0" class="inputTag"></span>');

            $(".inputTag").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "{{route('performers.portfolio.getTagsList')}}",
                        type:'GET',
                        data: {
                            tags: request.term,
                            //id:$(this.element.get(0)).attr('data-attid')
                        },
                        success: function( data ) {
                            if(data.status == true){
                                if(data.html.length > 0){
                                    response(data.html);
                                    response($.map(data.html, function (el) {

                                    return {
                                        label: el.tag,
                                        id: el.id,
                                        //protfolio_id: data.protfolio_id
                                     };
                                   }));
                                } else {
                                    $('.inputTag').val('');
                                }

                            }
                        }
                    });
                },select: function( event, ui ) {
                    event.preventDefault();
                
                    $(".taglist").append('<span class="tagRemove" >'+ui.item.label+'<a href="javascript:void(0)" class="thistagDelete"><i class="fal fa-times"></i><input type="hidden" value="'+ui.item.label+'" name="tags[]"></a></span>');
                    $('.boxInputTag').remove();
                }
            });
        }  
    })

function ImageTagRemove(id)
{
   $.ajax({
            url:"{{route('performers.portfolio.portfolioImageTagRemove')}}",
            type:"GET",
            data:{id:id},
            success:function(data){
                if(data.status == true){
                    toastr.success("Tag remove successfully..");
                }
            }
       });
}

 $(document).on("click", ".thistagDelete", function() { 
      $(this).parent().remove(); 
 });
</script>