<?php $i=1; ?>
@foreach($records as $usernote)
  <tr>
      <td><?php echo $i; ?></td>
      <td>{{$usernote->username}}</td>
      <td>{{$usernote->notes}}</td>
      <td>{{$usernote->created_at->setTimezone(Auth::guard('internal-staff')->user()->timezone)->format('m/d/Y H:i:s')}}</td>
      <td><a href=""
        data-id="{{$usernote->id}}" class="editnote"
        data-toggle="modal" data-target="#myModal"><i class="far fa-edit"></i></a>
        <a data-id="{{$usernote->id}}" class="deletenote" href="javascript:void(0)" data-toggle="confirmation" data-title="are you sure?"><i class="far fa-trash-alt"></i></a></td>

  </tr>
<?php  $i++; ?>
 @endforeach 