@extends('performers.layouts.app')
@section('content')
<div class="wctleftrightbox">
 @if($performer->applicationfor == "Group")
  @include('performers.layouts.groupSidebar')  
  @else
  @include('performers.layouts.sidebar')  
  @endif
  <div class="rightallditinwct">
   <!--  <div class="searchbarboxfilt">
      <div class="srchbox">        
        <div class="form-group">
          <i class="far fa-search"></i>
          <input type="text" class="form-control" id="" placeholder="Search">
        </div>
      </div>
      <div class="addandtagbox">
        <div class="addatribut">
           <a href="javascript:void(0)" type="button" data-toggle="modal" data-target="#myClosetModal"><i class="far fa-plus"></i> New Closet Item</a> 
        </div>
        <div class="tagboxright">
          <a href="javascript:void(0)"><i class="far fa-tag"></i></a>
        </div>
      </div>
    </div> -->
    <div class="addflagshidbox">
      <div class="accordion-container">
        <div class="set hedtobox">
          <div class="srchbox hidden">        
            <div class="form-group">
              <i class="far fa-search"></i>
             <input type="text" class="form-control" id="" placeholder="Search">
            </div>
          </div>
      @if($authtype=='performer')     
     @else
     @include('performers.flag')
     @endif       
        </div>      
      </div>
    </div>
    <div class="topbarbotdivcover">
 @if($authtype=='performer')
     @include('performers.layouts.tabbar')
     @else
     @include('performers.layouts.internal-tabbar')
     @endif
    <div class="notealldetacoverbox">
    <form action="{{route('internal-staff.savenotes')}}" method="get"  id="addnotes">
      @csrf
      <!--  <textarea name="notes" ></textarea> -->
      <div class="notesboxusrna">
        <div class="form-group noteboxset">
          <label for="exampleInputPassword1">Note</label>
          <input type="hidden" name="staff_id" value="{{Auth::user()->id}}">
          <input type="hidden" name="performer_id" value="{{$performer->id}}">
          <input type="text" name="user" value="{{Auth::user()->full_name}}" class="form-control" readonly="">
        </div>
        <div class="form-group">          
          <textarea name="note" class="form-control" id="note"></textarea >
        </div>
        
        <!-- Notes:
        <textarea name="note"></textarea>
        Username:
        <input type="text" name="user"> -->
        <div class="submitbtnbox">
          <input type="submit" value="save" id="btnSave" class="btn">
        </div>
      </div>
    </form>
  <table class="table notestblbox" id="UserNote" >
      <thead>
          <tr>
              <th class="text-center">ID</th>
              <th class="text-center">UserName</th>
              <th class="text-center">Note</th>
              <th class="text-center">Date</th>
              <th class="text-center actionNote">Action</th>
          </tr>
      </thead>
      <tbody id="updatetablebody">
        <?php $i=1; ?>
        @foreach($note as $usernote)
          <tr>
              <td><?php echo $i; ?></td>
              <td>{{(isset($usernote->staffmember) ? $usernote->staffmember->firstname : '')}}</td>
              <td>{{$usernote->notes}}</td>
              <td>{{$usernote->created_at->setTimezone(Auth::user()->timezone)->format('m/d/Y H:i:s')}}</td>
              <td><a href=""
                data-id="{{$usernote->id}}" class="editnote"
                data-toggle="modal" data-target="#myModal"><i class="far fa-edit"></i></a>
                <a data-id="{{$usernote->id}}" class="deletenote" href="javascript:void(0)" data-toggle="confirmation" data-title="are you sure?"><i class="far fa-trash-alt"></i></a></td>

          </tr>
        <?php  $i++; ?>
         @endforeach 
      </tbody>
  </table>


  </div>
</div>
      <!--     Eyes modal -->
<div class="modal fade newgallerymod noteseditmodal" id="myModal" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="GalleryBoxinerdeta">
           <form method="post" action="{{route('internal-staff.updatenote')}}" enctype="multipart/form-data" id="updatenotemodel">
            @csrf          
            <div class="titlandclobtn">
              <h3>Edit Note</h3>
              <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <div class="noteditditbox">              
              <input type="hidden" name="noteid" id="editnoteid">              
              <div class="form-group">
                <label for="exampleInputPassword1">Notes</label>
                <textarea name="note" class="form-control" id="editmodelnote"></textarea>                
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Username</label>
                <input type="text" class="form-control"  name="username" placeholder="" id="editusername" readonly>
              </div>
            </div>
          </div>
          <div class="updatedbtnbox"><input type="button" name="" value="Update" class="btn" id="updatenotebtn"></div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')

<script type="text/javascript">
$('#UserNote').DataTable({
    "order": [[ 3, "desc" ]]
  });
       // CKEDITOR.replace( 'notes' );
       $(document).on('click', '#btnSave', function(e){          
           e.preventDefault();
           
            toastr.options = {
            "preventDuplicates": true,
            "preventOpenDuplicates": true
            };
     //      for (instance in CKEDITOR.instances) 
     //      {
     //          CKEDITOR.instances[instance].updateElement();
     //      }

     if($("#note").val()==""){
      toastr.error("Please Enter Note");
     }else{
       
      
    var $form = $("#addnotes");
      
    $.ajax({

        type: 'GET',
        url: "{{route('internal-staff.savenotes')}}",

        data: $form.serialize(),
        success: function (data) {
            if(data)
            {
               toastr.options = {
            "preventDuplicates": true,
            "preventOpenDuplicates": true
            };

          toastr.success('Note Updated Successfully');
            
          $('#updatetablebody').html(data.html); 
            $('#addnotes')[0].reset();
          $('.deletenote[data-toggle=confirmation]').confirmation({
                   rootSelector: '[data-toggle=confirmation]',
                   container: 'body',
                   onConfirm: function() {
                     $.ajax({
                              type: "GET",
                              url: "{{route('internal-staff.deletenote')}}",
                              data: {id: $(this).data('id')},
                              success: function (data) {
                                      toastr.success("Note Deleted Successfully");   
                                      location.reload();  
                                  }         
                          }); 
                   },
                 });
            }
           
        },
        error: function (result) {

        }
    });
    }
 });

        $(document).on('click', '.deletenote', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
      
     
    });
        $(document).on('click', '.editnote', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
   
     $.ajax({
                type: "GET",
                headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                url: "{{route('internal-staff.editnote')}}",
                data: {id:id},
                success: function (data) {
                  
                        // /*    $("#tonename").val(data);
                        //     $("#toneid").val(id); */
                            $('#editmodelnote').html(data.notes);
                            $('#editusername').val(data.username);
                            $("#editnoteid").val(id);
                    }         
            }); 

     });  
           $("#updatenotebtn").click(function(e){
             e.preventDefault();
         
           var $form = $("#updatenotemodel");
             if($("#editmodelnote").val()==""){
              toastr.error("Enter Note");
             }else{
           $.ajax({
               type: "GET",
               url: $form.attr('action'),
               data: $form.serialize(),
               success: function (data, status) {
                    if(data.error){

                       return;
                   }

                  toastr.success("Note Updated Successfully");// THis is success message

                    $('#myModal').modal('hide'); 
                 //    // Your modal Id
                    $('#updatetablebody').html(data.html); 
                    $('#updatenotemodel')[0].reset();

                    $('.deletenote[data-toggle=confirmation]').confirmation({
                             rootSelector: '[data-toggle=confirmation]',
                             container: 'body',
                             onConfirm: function() {
                               $.ajax({
                                        type: "GET",
                                        url: "{{route('internal-staff.deletenote')}}",
                                        data: {id: $(this).data('id')},
                                        success: function (data) {
                                                toastr.success("Note Deleted");   
                                                location.reload();  
                                            }         
                                    }); 
                             },
                           });
               },
               error: function (result) {

               }
           });
           }
        });
           $('.deletenote[data-toggle=confirmation]').confirmation({
                    rootSelector: '[data-toggle=confirmation]',
                    container: 'body',
                    onConfirm: function() {
                      $.ajax({
                               type: "GET",
                               url: "{{route('internal-staff.deletenote')}}",
                               data: {id: $(this).data('id')},
                               success: function (data) {
                                       toastr.success("Note Deleted Successfully");   
                                       location.reload();  
                                   }         
                           }); 
                    },
                  });
      </script>
      <script type="text/javascript">
        $('#UserNote').on('page.dt', function() {
          setTimeout(
            function() {
              $('.deletenote[data-toggle=confirmation]').confirmation({
                       rootSelector: '[data-toggle=confirmation]',
                       container: 'body',
                       onConfirm: function() {
                         $.ajax({
                                  type: "GET",
                                  url: "{{route('internal-staff.deletenote')}}",
                                  data: {id: $(this).data('id')},
                                  success: function (data) {
                                          toastr.success("Note Deleted Successfully");   
                                          location.reload();  
                                      }         
                              }); 
                       },
                     });
            }, 500);
        });
       
      </script>
      <script type="text/javascript">
        $('#UserNote').on('order.dt', function() {
          setTimeout(
            function() {
              $('.deletenote[data-toggle=confirmation]').confirmation({
                       rootSelector: '[data-toggle=confirmation]',
                       container: 'body',
                       onConfirm: function() {
                         $.ajax({
                                  type: "GET",
                                  url: "{{route('internal-staff.deletenote')}}",
                                  data: {id: $(this).data('id')},
                                  success: function (data) {
                                          toastr.success("Note Deleted Successfully");   
                                          location.reload();  
                                      }         
                              }); 
                       },
                     });
            }, 500);
        });
       
      </script>
      @endsection
