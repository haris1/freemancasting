@extends('performers.layouts.app')
@section('content')
<div class="wctleftrightbox">
  @include('performers.layouts.sidebar') 
     <div class="rightallditinwct">
    <div class="searchbarboxfilt">
      <div class="srchbox">        
        <div class="form-group">
          <i class="far fa-search"></i>
          <input type="text" class="form-control" id="skillSearch" placeholder="Search">
        </div>
      </div>
      <div class="addandtagbox">
        <div class="addatribut">
          <a href="javascript:void(0)" type="button" data-toggle="modal" data-target="#mySkillModal"><i class="far fa-plus"></i>New Skill</a>
        </div>
        <div class="tagboxright">
          <a href="javascript:void(0)"><i class="far fa-tag"></i></a>
        </div>
      </div>
    </div> 
  @include('performers.layouts.tabbar')
 
  @if(count($skill_list))
  <div id="defaultDisplayDiv">
  @foreach($skill_list as $skillValue)
  @if($skillValue->is_video == "no")
  <div class="hedfullmotobox">
    <div class="allboxtitlesem">
      <div class="titleboxallcover">
        <h3>{{$skillValue->skill_title}}</h3>
        <div class="imgvidaodbox">
          <div id="skilltag{{$skillValue->id}}">
          @if(count($skillValue->skilltag))
          @foreach($skillValue->skilltag as $tagVal)
          <span class="btnremadd" id="TagRemove{{$tagVal->id}}">
          {{$tagVal->tag}} 
          <a href="javascript:void(0)" class="DeleteTag{{$tagVal->id }}"  onclick="tagDelete({{$tagVal->id }})"> <i class="fal fa-times"></i></a>
          </span>
          @endforeach
          @endif
        </div>
          <span class="AddTag" onclick="addTag({{$skillValue->id}})" id="{{$skillValue->id}}">Add Tag <i class="fal fa-plus"></i></span> 
          <span id="tagText_{{$skillValue->id}}" class="hidden tagtextltbox">
            <input type="" data-attid="{{$skillValue->id}}"  name="tag" id="tagval_{{$skillValue->id}}" class="tagstore{{$skillValue->id}} tagSave" >
          </span>
        </div>
        

        <div class="editboxinerpart">
          <a href="javascript:void(0)" id="{{$skillValue->id}}" class="imageAddTogallery"><i class="far fa-plus"></i></a>

          <a href="javascript:void(0)" onclick="viewGallery('{{ $skillValue['id'] }}','{{ $skillValue['skill_title'] }}')"><i class="far fa-edit"></i></a>
        </div>
      </div>
    </div>  
    <div class="gelimgleftbox">        
      <ul>
        @if(count($skillValue['performermedia']))        
        @foreach($skillValue['performermedia'] as $key=>$mediaValue)         
        @if($key <=6)
        <li id="skillMedia{{$mediaValue->id}}">
          <a href="javascript:void(0)" onclick="skillMediaDelete('{{$mediaValue->id}}')"><i class="fas fa-trash"></i></a>
          <img src="{{$mediaValue->image_url}}" alt="">
        </li> 
        @endif
        @endforeach
        @else
        <div class="gelimgleftbox">           
          <p>No Image</p>
        </div>
        @endif
      </ul>
    </div>   
    <div class="viewgallbox">
      <a href="{{route('performers.view.skillgallery',['id'=>$skillValue->id])}}">View Gallery<br>
      {{count($skillValue['performermedia'])}} Images</a>   
    </div>
  </div>
@endif
@endforeach
<!-- //video listing -->
@foreach($skill_list as $skillValue)
  @if($skillValue->is_video == "yes")
<div class="hedfullmotobox">
    <div class="allboxtitlesem">
      <div class="titleboxallcover">
    <h3>{{$skillValue['skill_title']}}</h3>
    <div class="imgvidaodbox">
          <div id="skilltag{{$skillValue->id}}">
            @if(count($skillValue->skilltag))
            @foreach($skillValue->skilltag as $tagVal)
            <span class="btnremadd" id="TagRemove{{$tagVal->id}}">
              {{$tagVal->tag}} 
              <a href="javascript:void(0)" onclick="tagDelete({{$tagVal->id }})"> <i class="fal fa-times"></i></a>
            </span>
            @endforeach
            @endif
          </div>
          <span class="AddTag" onclick="addTag({{$skillValue->id}})" id="{{$skillValue->id}}">Add Tag <i class="fal fa-plus"></i></span> 
          <span id="tagText_{{$skillValue->id}}" class="hidden tagtextltbox">
            <input type="" data-attid="{{$skillValue->id}}"  name="tag" id="tagval_{{$skillValue->id}}" class="tagstore{{$skillValue->id}} tagSave" >
          </span>
        </div>
        <div class="editboxinerpart">
          <a href="javascript:void(0)" id="{{$skillValue->id}}" class="videoAddTogallery"><i class="far fa-plus"></i></a>

          <a href="javascript:void(0)"><i class="far fa-edit"></i></a>
        </div>
  </div>     
    <div class="gelimgleftbox">        
      <div class="videoplaybtnbox">
        @if(count($skillValue['performermedia']))        
        @foreach($skillValue['performermedia'] as $mediaValue)         
         <div class="btnplaybox" id="skillMedia{{$mediaValue->id}}">
          <a href="javascript:void(0)" data-key="{{$mediaValue->id}}" class="showVideo">
          <i class="fal fa-play-circle"></i> <span>{{$mediaValue->media_title}}</span> <span>{{$skillValue->created_at->format('d M Y')}}</span></a>
          <a href="javascript:void(0)" class="ditbtnbox" onclick="skillMediaDelete('{{$mediaValue->id}}')">
                <i class="fas fa-trash"></i>
        </div>
        @endforeach
        @else
        <div class="gelimgleftbox">        
          <p>Not Video</p>
        </div>
        @endif        
       
      </div>
    </div>   
  </div>
</div>
@endif
@endforeach

@else
<div class="hedfullmotobox">
  <div class="gelimgleftbox">        
    <p>No skill available</p>
  </div>
</div>
@endif
</div>
</div>
</div>
<!-- image upload model -->
<div class="modal fade newgallerymod" id="mySkillModal" role="dialog">
 <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-body">
       <div class="GalleryBoxinerdeta">        
        <form method="post" action="" enctype="multipart/form-data" id="skillGallery"> 
         @csrf    
        
        <div class="titlandclobtn">
         <h3>New Gallery</h3>
         <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
       </div>  
       <div class="imguplodvideo">
         <div class="seletcustboxpart2">
           <select id="select_upload" name="is_video">
            <option value="ImageUpload">Image Upload</option>
            <option value="VideoUpload">Video Upload</option>
          </select>
         </div>
       </div>
           
       <div class="form-group inputbixin">
         <input type="text" class="form-control" id="skill_title" name="skill_title" placeholder="Title Gallery">
       </div>
       <div class="form-group textarbox">
         <textarea class="form-control" id="" name="description" placeholder="Add a description"></textarea>
       </div>
       <div class="dropboxzoniner">        

        <div id="Image_upload_dropzone">
          <!-- image upload dropzone --> 
          <div method="post" action="{{route('performers.store.skill.images')}}" enctype="multipart/form-data" 
          class="dropzone" id="dropzone">          
        </div>
      </div>

      <div id="video_upload_dropzone">
        <!-- video upload dropzone -->
        <div method="post" action="{{route('performers.store.skill.video')}}" enctype="multipart/form-data" 
        class="dropzone" id="Videodropzone">          
      </div>
    </div>
  </div>
  <div class="form-group imgtitleboz">
   <input type="text" class="form-control" id="media_title" name="media_title" placeholder="Image Title">
 </div>
</div>
<div class="imgaddedboxi">

</div>
<div class="submitbtnbox">
  <input type="button" id="addSkill_image" name="" value="Submit" class="btn">
  <input type="button" id="addSkill_video" name="" value="Submit" class="btn">
</div>

</form>
</div>
</div>
</div>
</div>

<!-- video show model -->
<div class="modal fade newgalrymodvideo" id="videoDispalyModal" role="dialog">
 <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-body">
       <div class="Galleryvideoboxinr">        

        <div class="titlandclobtn">
         <h3>video</h3>
         <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>         
       </div>      
       <div class="video_dispay" >
        
         
        </div>    
      </div>
    </div>
  </div>
</div>
</div>

 <!-- add image modal -->
 <div class="modal fade newgallerymod" id="addImage" role="dialog">
 <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-body">
       <div class="GalleryBoxinerdeta">        
        <form method="post" action="" enctype="multipart/form-data" id="skilladdImage"> 
         @csrf    
        
        <div class="titlandclobtn">
         <h3>Add Image</h3>
         <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
       </div>      
       <div class="form-group inputbixin">
         <input type="hidden" class="form-control" id="skill_id" name="skill_id" >
       </div>

       <div class="dropboxzoniner">        

        <div id="Image_upload_dropzone">
          <!-- image upload dropzone --> 
          <div method="post" action="{{route('performers.store.skill.images')}}" enctype="multipart/form-data" 
          class="dropzone" id="AddImagedropzone">          
        </div>
      </div>
    </div>
 <div class="form-group imgtitleboz">
   <input type="text" class="form-control" id="media_title" name="media_title" placeholder="Image Title">
 </div>
</div>
<div class="imgaddedboxi">

</div>
<div class="submitbtnbox">
<input type="button" id="skill_image_add" name="" value="submit" class="btn btn-primary">
</div>
</form>
</div>
</div>
</div>
</div>

<!-- Add video modal -->
 <div class="modal fade newgallerymod" id="addVideo" role="dialog">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-body">
         <div class="GalleryBoxinerdeta">        
          <form method="post" action="" enctype="multipart/form-data" id="skilladdVideo"> 
           @csrf 
           <div class="titlandclobtn">
             <h3>Add Video</h3>
             <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
           </div>      
           <div class="form-group">
             <input type="hidden" class="form-control" id="video_skill_id" name="skill_id" >
           </div>

           <div class="dropboxzoniner">        

            <div id="Video_upload_dropzone">
              <!-- image upload dropzone --> 
              <div method="post" action="{{route('performers.store.skill.video')}}" enctype="multipart/form-data" 
              class="dropzone" id="AddVideodropzone">          
            </div>
          </div>
        </div>
        <div class="form-group imgtitleboz">
         <input type="text" class="form-control" id="media_title" name="media_title" placeholder="Video Title">
       </div>
     </div>
     <div class="imgaddedboxi">

     </div>
     <div class="submitbtnbox">
      <input type="button" id="skill_video_add" name="" value="submit" class="btn btn-primary">
    </div>
  </form>
</div>
</div>
</div>
</div>
@endsection
@section('scripts')
<!-- image upload dropzone  -->
<script type="text/javascript">
  var uploadedDocumentMap = {};
  Dropzone.options.dropzone =
  {
    maxFilesize: 100,
    renameFile: function(file) {
      var dt    = new Date();
      var time  = dt.getTime();
      return time+file.name;
    },
    type:"post",
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    addRemoveLinks: true,
    timeout: 5000,

    success: function(file, response) 
    {            
      console.log(response);
      $('#skillGallery').append('<input type="hidden" name="images[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
      uploadedDocumentMap[file.name] = response;              
    },
    error: function(file, response)
    {
      console.log(file);
      return false;
    }
  };

  $('#addSkill_image').click(function(){

    formdata = $('#skillGallery').serialize();
    
    
     $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
    $.ajax({
      url:"{{route('performers.store.skill.media')}}",
      type:"POST",
      data:formdata,
      success: function(response)
      {
       if(response.status == true)
       {
        location.reload();
        toastr.success(response.message);
      }else{
        toastr.error(response.message);
      }
    }
  });
  });
</script>

<!-- video upload dropzone -->
<script type="text/javascript">
  var uploadedDocumentMap = {};
  Dropzone.options.Videodropzone =
  {
    maxFilesize: 100,
    renameFile: function(file) {
      var dt    = new Date();
      var time  = dt.getTime();
      return time+file.name;
    },
    type:"post",
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    acceptedFiles: ".mp4,.wmv,.flv,.avi",
    addRemoveLinks: true,
    timeout: 5000,

    success: function(file, response) 
    {            
      console.log(response);
      $('#skillGallery').append('<input type="hidden" name="images[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
      uploadedDocumentMap[file.name] = response;              
    },
    error: function(file, response)
    {
      console.log(file);
      return false;
    }
  };

  $('#addSkill_video').click(function(){

    formdata = $('#skillGallery').serialize();
    
    
     $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
    $.ajax({
      url:"{{route('performers.store.skill.media')}}",
      type:"POST",
      data:formdata,
      success: function(response)
      {
       if(response.status == true)
       {
        
         location.reload();
         toastr.success(response.message);
      }else{
        console.log(response);
        toastr.error(response.message);
      }
    }
  });
  });
</script>




<script type="text/javascript">
  $( document ).ready(function() {
    $("#select_upload").select2();
    $("#video_upload_dropzone").hide();
    $("#addSkill_video").hide();
   $('select').on('change', function() {
       if(this.value == "VideoUpload")       {
        $("#video_upload_dropzone").show();
        $("#Image_upload_dropzone").hide();
        $("#addSkill_video").show();
        $("#addSkill_image").hide();
        $("#media_title").attr("placeholder", "video title");
       }else       {
          $("#video_upload_dropzone").hide();
          $("#Image_upload_dropzone").show();
          $("#addSkill_video").hide();
          $("#addSkill_image").show();
          $("#media_title").attr("placeholder", "Image title");
       }
    });
});


$(".showVideo").click(function(){
  id = $(this).data('key');

    $.ajax({
      url:"{{route('performers.skill.video.show')}}",
      type:"GET",
      data:{id:id},
      success:function(response){
        if(response.status==true){
          $(".video_dispay").html("");
          $("#videoDispalyModal").modal('show');
          $(".video_dispay").html('<video controls class=""><source  src="'+response.data.image_url+'" type="video/mp4">Your browser does not support the video tag.</video>'); 
          }
      },
    error: function(file, response)
    {     
    }
    });
});

</script>

<!-- + Add Image dropzone -->
<script type="text/javascript">
  var uploadedDocumentMap = {};
  Dropzone.options.AddImagedropzone =
  {
    maxFilesize: 100,
    renameFile: function(file) {
      var dt    = new Date();
      var time  = dt.getTime();
      return time+file.name;
    },
    type:"post",
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    addRemoveLinks: true,
    timeout: 5000,

    success: function(file, response) 
    {            
      console.log(response);
      $('#skilladdImage').append('<input type="hidden" name="images[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
      uploadedDocumentMap[file.name] = response;              
    },
    error: function(file, response)
    {
      console.log(file);
      return false;
    }
  };

  $('#skill_image_add').click(function(){
    formdata = $('#skilladdImage').serialize();   
    
     $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
    $.ajax({
      url:"{{route('performers.add.skill.images')}}",
      type:"POST",
      data:formdata,
      success: function(response)
      {
       if(response.status == true)
       {
        location.reload();
        toastr.success(response.message);
      }else{
        toastr.error(response.message);
      }
    }
  });
  });


  //add video dropzone code
  var uploadedDocumentMap = {};
  Dropzone.options.AddVideodropzone =
  {
    maxFilesize: 100,
    renameFile: function(file) {
      var dt    = new Date();
      var time  = dt.getTime();
      return time+file.name;
    },
    type:"post",
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    acceptedFiles: ".mp4,.wmv,.flv,.avi",
    addRemoveLinks: true,
    timeout: 5000,

    success: function(file, response) 
    {            
      console.log(response);
      $('#skilladdVideo').append('<input type="hidden" name="videos[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
      uploadedDocumentMap[file.name] = response;              
    },
    error: function(file, response)
    {
      console.log(file);
      return false;
    }
  };
</script>
<!--end  Add Image dropzone -->

<script type="text/javascript">  
//image add to gallery modal
$(".imageAddTogallery").click(function(){
   id =$(this).attr('id');
   $("#skill_id").val(id);
   $("#addImage").modal('show');
   
});


 //video add
 $(".videoAddTogallery").click(function(){
   id =$(this).attr('id');
   $("#video_skill_id").val(id);
   $("#addVideo").modal('show');


   //video add btn
   $("#skill_video_add").click(function(){
   
    formdata = $('#skilladdVideo').serialize();   
    $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
    $.ajax({
      url:"{{route('performers.add.skills.video')}}",
      type:"POST",
      data:formdata,
      success: function(response)
      {
       if(response.status == true)
       {
        location.reload();
        toastr.success(response.message);
      }else{
        toastr.error(response.message);
      }
    }
  });
  });   
     
});



function addTag(s_id)
 {
  
  skill_id =s_id;
  tag ='#tagval_'+s_id;
  id = "#tagText_"+s_id;
  tag = ".tagstore"+s_id;  
  $(id).removeClass('hidden');     
 }


// $(".AddTag").click(function(){
//   skill_id =$(this).attr('id');
//   tag ='#tagval_'+$(this).attr('id');
//   id = "#tagText_"+$(this).attr('id');
//   tag = ".tagstore"+$(this).attr('id');
  
//   $(id).removeClass('hidden');  
//   tag_val =$(this).val();

  
  

// });

// $(document).on('focusout', '.tagSave', function() {
    function storeTags(id,tag){
  var tag_val=tag;

  if(tag_val == ""){
    $(id).addClass('hidden');  
    return false;
  }

  var skill_id =id;      

  $.ajaxSetup({
   headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }
 });
  $.ajax({
    url:"{{route('performers.skill.addtag')}}",
    type:"post",
    data:{skill_id:skill_id,tag:tag_val},
    success:function(response){
     if(response.status == true)
     {
    
                   // location.reload(2000);
                   $("#skilltag"+skill_id).append('<span class="btnremadd" id="TagRemove'+response.data.id+'">'+response.data.tag+'<a href="javascript:void(0)" class="tagDelete" onclick="tagDelete('+response.data.id+')" > <i class="fal fa-times"></i></a></span>');
                   toastr.success(response.message);
                   $(tag).val("");
                   $(id).addClass('hidden');
                   $('.tagSave').val('');
                   $('.tagtextltbox').addClass('hidden');
                 }},
                 error: function(file, response)
                 {
                  console.log(file);
                  return false;
                }
                
              });

}
function tagDelete(id)
{ 
 
 
  $("#TagRemove"+id).remove();
    $.ajax({
      url:"{{route('performers.skill.removetag')}}",
      type:'get',
      data:{id:id},
      success:function(data){        
        if(data.status == true)
        {   
          toastr.success(data.message);     

        }      
      }
    });
}

function skillMediaDelete(id)
{  
  $("#skillMedia"+id).remove();
  $.ajax({
         url:"{{route('performers.skills.removeMedia')}}",
        type:'get',
        data:{id:id},
        success:function(response){
            if(response.status == true)
            {
              toastr.success(response.message);                   
            }
        }

  });
  
}

//search 
 $(document).ready(function(){
    $("#skillSearch").keyup(function(){
    search = $(this).val();

      $.ajax({
        url:"{{route('performers.skills.search')}}",
        type:'GET',
        data:{search:search},
        success:function(data)
        {
          if(data.status == true){        
            $("#defaultDisplayDiv").html(data.html);
            }
        }
      });     
  });
});
 /** auto complated tags ajax base **/
    $(".tagSave").autocomplete({
        //source: availableTags
        source: function(request, response) {
            $.ajax({
                url: "{{route('performers.skills.getTags')}}",
                type:'GET',
                data: {
                    tags: request.term,
                    id:$(this.element.get(0)).attr('data-attid')
                },
                success: function( data ) {
                    if(data.status == true){
                        if(data.html.length > 0){
                            //response(data.html);
                            response($.map(data.html, function (el) {

                             return {
                               label: el.tag,
                               id: el.id,
                               skill_id: data.skill_id
                             };
                           }));
                        } else {
                            $('.tagSave').val('');
                        }

                    }
                }
            });
        },select: function( event, ui ) {
            event.preventDefault();
           // console.log();
           // var id = $(this.element.get(0)).attr('data-attid');
            $('.tagSave').val(ui.item.label);
            var id = ui.item.skill_id;
            var tag = ui.item.label;
            storeTags(id,tag);
        }
    });
</script>

@include('performers.commonSkill')
@endsection