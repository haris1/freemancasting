
<div class="hidden" id="appDtataAppend">
    <agile class="main" ref="main" :options="options1" :as-nav-for="asNavFor1">
        <div class="slide" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`"><img :src="slide"/></div>
    </agile>
    <div class="lftinttileditbox">            
        <div class="form-group imgtitleboz">            
            <input type="text" class="form-control" id="" placeholder="Image Tile">
        </div>
        <div class="rightedittagbox">
            <div class="tagbox">
                <a href="javascript:void(0)">Tag</a>
            </div>
            <div class="editbox">
                <a href="javascript:void(0)"><i class="far fa-edit"></i></a>
            </div>
        </div>
    </div>
    <div class="leftrightaggthumbox">
        <agile class="thumbnails" ref="thumbnails" :options="options2" :as-nav-for="asNavFor2">
            <div class="slide slide--thumbniail" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`" @click="$refs.thumbnails.goTo(index)"><img :src="slide"/></div>
            <template slot="prevButton"><i class="fas fa-chevron-left"></i></template>
            <template slot="nextButton"><i class="fas fa-chevron-right"></i></template>
        </agile>
        <div class="drprightboxiner">
            <div class="droplisingimg">
                <a href="javascript:void(0)" class="btnAddSkillGallery" >
                    <i class="far fa-plus"></i> <p>image</p>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade newgallerymod" id="showGalleryModal" role="dialog">
  <div class="modal-dialog">    
    <!-- Modal content-->
    <div class="modal-content">      
      <div class="modal-body">
        <div class="Gallerydetaaddbox">
          <div class="titlandclobtn">
            <h4>{{(isset($gallery['skill_title'])?$gallery['skill_title']:'')}}</h4>
            <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
          </div>
          
          <div class="sliderpartinerset">  

            <div id="app" class="gallryappbox">
              <agile class="main" ref="main" :options="options1" :as-nav-for="asNavFor1">
                <div class="slide" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`"><img :src="slide"/></div>
              </agile>
              <div class="lftinttileditbox">            
                <div class="form-group imgtitleboz">            
                  <input type="text" class="form-control" id="" placeholder="Image Tile">
                </div>
                <div class="rightedittagbox">
                  <div class="tagbox">
                    <a href="javascript:void(0)">Tag</a>
                  </div>
                  <div class="editbox">
                   <a href="javascript:void(0)"><i class="far fa-edit"></i></a>
                 </div>
               </div>
             </div>
             <div class="leftrightaggthumbox">
              <agile class="thumbnails" ref="thumbnails" :options="options2" :as-nav-for="asNavFor2">
                <div class="slide slide--thumbniail" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`" @click="$refs.thumbnails.goTo(index)"><img :src="slide"/></div>
                <template slot="prevButton"><i class="fas fa-chevron-left"></i></template>
                <template slot="nextButton"><i class="fas fa-chevron-right"></i></template>
              </agile>
              <div class="drprightboxiner">
                <div class="droplisingimg">
                  <a href="javascript:void(0)" class="btnAddSkillGallery" >
                    <i class="far fa-plus"></i> <p>image</p>
                  </a>
                </div>
                <!-- <div class="droplisingimg">
                    <div class="dropzone needsclick" id="demo-upload" action="/upload">
                      <div class="dz-message needsclick">
                        <div class="lefimgadd">
                          <i class="far fa-plus"></i> <p>image</p>
                        </div>                 
                      </div>
                    </div> 
                </div>-->
              </div>
            </div>
            
          </div>
        </div>                   
      </div>
    </div>
  </div>      
</div>
</div>
<div class="modal fade newgallerymod" id="addSkillGallery" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="GalleryBoxinerdeta">
                    <!-- {{route('performers.store.portfolio.media')}} -->
                    <form method="post" enctype="multipart/form-data" id="updateSkillGallery"> 
                        @csrf    
                        <div class="titlandclobtn">
                            <h3>Edit Gallery</h3>
                            <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
                        </div>
                        <div class="form-group inputbixin">
                            <input type="text" class="form-control" id="skill_title" name="skill_title" placeholder="Title Gallery" value="{{ (isset($gallery['skill_title'])?$gallery['skill_title']:'') }}">
                        </div>
                        <div class="dropboxzoniner">
                            <div id="Image_upload_dropzone">
                                <!-- image upload dropzone --> 
                                <div method="post" action="{{ route('performers.store.skill.images') }}" enctype="multipart/form-data" 
                                class="dropzone" id="updateImagedropzone">          
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="gallery_id" id="gallery_id" value="{{ (isset($gallery['id'])?$gallery['id']:'') }}">
                        <div class="form-group imgtitleboz">
                            <input type="text" class="form-control" id="" name="media_title" placeholder="Image Tile" value="">
                        </div>
                        <div class="submitbtnbox">
                            <input type="submit" value="submit" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> 


<script type="text/javascript">
  $('#grid2').addClass('effect-2');   
    var uploadedDocumentMap = {};
    Dropzone.options.updateImagedropzone =
    {
        maxFilesize: 100,
        renameFile: function(file) {
            var dt    = new Date();
            var time  = dt.getTime();
            return time+file.name;
        },
        type:"POST",
        //headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        addRemoveLinks: true,
        timeout: 5000,

        success: function(file, response) 
        {            
            console.log(response);
            $('#updateSkillGallery').append('<input type="hidden" name="images[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
            uploadedDocumentMap[file.name] = response;              
        },
        error: function(file, response)
        {
            console.log(file);
            return false;
        }
    };

    $(document).on("click",".btnAddSkillGallery",function(e) {
        $('#addSkillGallery').modal('show');
        $('#showGalleryModal').modal('hide');
        setTimeout(function(){
            $('body').addClass('modal-open');
        },1000)
    });

    $('#updateSkillGallery').on('submit',(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:'{{ route("performers.skillGalleryUpdate") }}',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            beforeSend:function(){},
            success:function(response) {
                if (response.status == true) {
                    toastr.success(response.message);
                    $('#addSkillGallery').modal('hide');
                        //$('#showGalleryModal').modal('show');
                        viewGallery($('#gallery_id').val());
                        setTimeout(function(){
                            $('body').addClass('modal-open');
                            
                        },1000)
                } else {
                    toastr.warning(response.message);
                }
            },
            complete:function(){},
            error:function(){},
        });
    }));

  $(window).load(function(){
     //slides_image = $('#hidden_slide').val();
    var $container = $('.grid');
  // initialize
  $container.masonry({
    //columnWidth: 200,
    itemSelector: '.item'
  })
  $('.item > a').removeAttr('href')
});


  function viewGallery(id,title){
    id = id;
    $('#gallery_id').val(id);
    $('input[name="skill_title"]').val(title);
    var slideImage;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
          url:"{{route('performers.skillslidergallery.images')}}",
          type:"GET",
          data:{id:id},
          success: function(response)
          {
            slideImage = response;
            jQuery('#showGalleryModal').modal('show');
            jQuery('#app').html(jQuery('#appDtataAppend').html());
            
             console.log(response); 
          },error: function(response)
            {
              console.log(response);
               return false;
            }

    });  

    jQuery('#showGalleryModal').modal('show');
    setTimeout(function(){ 

      Vue.use(VueAgile);
      app = new Vue({
        el: '#app',
        components: {
          agile: VueAgile },
          data() {
            return {
              asNavFor1: [],
              asNavFor2: [],
              options1: {
                dots: false,
                fade: true,
                navButtons: false },
                options2: {
                    autoplay: false,
                    centerMode: false,
                    dots: false,
                    navButtons: false,
                    slidesToShow: 3,
                    infinite : false,
                  responsive: [
                  {
                    breakpoint: 600,
                    settings: {
                      slidesToShow: 5 } },

                      {
                        breakpoint: 1000,
                        settings: {
                          navButtons: true } }] },

                          slides: slideImage};


                        },
                        mounted() {
                          this.asNavFor1.push(this.$refs.thumbnails);
                          this.asNavFor2.push(this.$refs.main);
                        } }); }, 1000);
  }
</script>