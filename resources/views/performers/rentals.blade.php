@extends('performers.layouts.app')
@section('content')
<div class="wctleftrightbox">
 @if($performer->applicationfor == "Group")
  @include('performers.layouts.groupSidebar')  
  @else
  @include('performers.layouts.sidebar')  
  @endif

   @if($authtype=='performer')     
  <input type="hidden" value="{{request()->route('id')}}" id="get_id" name="">
  @else
  <input type="hidden" value="{{request()->route('id')}}" id="get_id" name="">  
  @endif
  <div class="rightallditinwct">
   <!--  <div class="searchbarboxfilt">
      <div class="srchbox">        
        <div class="form-group">
          <i class="far fa-search"></i>
          <input type="text" class="form-control" id="rentalSearch" placeholder="Search">
        </div>
      </div>
      <div class="addandtagbox">
        <div class="addatribut">
          <a href="" type="button" data-toggle="modal" data-target="#rentalModal"><i class="far fa-plus"></i> New Props Item</a>
        </div>
        <div class="tagboxright">
          <a href="javascript:void(0)"><i class="far fa-tag"></i></a>
        </div>
      </div>
    </div> -->
    <div class="addflagshidbox">
      <div class="accordion-container">
        <div class="set hedtobox">
          <div class="srchbox">        
            <div class="form-group hidesearch">
              <i class="far fa-search"></i>
             <input type="text" class="form-control" id="rentalSearch" placeholder="Search">
            </div>
          </div>
      @if($authtype=='performer')     
     @else
     @include('performers.flag')
     @endif       
        </div>      
      </div>
    </div>
    <div class="topbarbotdivcover">
@if($authtype=='performer')
     @include('performers.layouts.tabbar')
     @else
     @include('performers.layouts.internal-tabbar')
     @endif
  @if(count($rental_list))
  <div id="defaultDisplayDiv" class="portfolboxcover">
    @foreach($rental_list as $rentalValue)
    @if($rentalValue->is_video == "no")
    <div class="hedfullmotobox" id="galleryDelete{{$rentalValue->id}}">
      <div class="allboxtitlesem">
        <div class="titleboxallcover">
          <div class="titledivallbox"><h3><a href="javascript:void(0)"  onclick="viewGallery('{{ $rentalValue['id'] }}','{{ $rentalValue['rental_title'] }}')" >{{$rentalValue['rental_title'] }}</a></h3></div>
          <div class="helpboxdcl">
            <span class="tool" data-tip="{{isset($helpdesk->props_help) ? $helpdesk->props_help : '' }}" tabindex="1"><i class="fal fa-question-circle"></i></span>
          </div>
          <div class="morbtnmnubox">
            <div class="morebtneddil">                
              <div class="MenuSceinain">
                <div class="menuheaderinmore">
                  <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
                  <div class="mainineropenbox_1">
                    <div class="mainineropenbox_2">
                      <div class="opensubmenua_1">
                        <a href="javascript:void(0)" id="{{$rentalValue->id}}"  class="imageAddTogallery" onclick="imageAddTogallery('{{$rentalValue->id}}')">Add</a>
                      </div> 
                      
                      <div class="opensubmenua_1">
                        <a href="javascript:void(0)"  onclick="viewGallery('{{ $rentalValue['id'] }}','{{ $rentalValue['rental_title'] }}')" >Edit</a>
                      </div>
                      @if($rentalValue['rental_title'] != "My Pet" && $rentalValue['rental_title'] != "My Vehicle" && $rentalValue['rental_title'] != "My Sports" &&
                      $rentalValue['rental_title'] != "My Musical Instruments" &&
                       $rentalValue['rental_title'] != "My Props"
                      )
                      
                      <div class="opensubmenua_1">
                        <a href="javascript:void(0)" id="{{$rentalValue->id}}"  class="gallaryconfirm"  data-toggle="confirmation" data-title="are you sure?" data-imagetagid="{{$rentalValue->id }}" >Delete</a>
                      </div>
                      @endif
                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>
          
            <div id="rentaltag{{$rentalValue->id}}">
              @if(count($rentalValue->rentaltag))
              @foreach($rentalValue->rentaltag as $tagVal)
              <span class="btnremadd" id="TagRemove{{$tagVal->id}}">
                {{$tagVal->tag}} 
                <a href="javascript:void(0)" class="DeleteTag{{$tagVal->id }} imagetagdeleteconfirm"  data-toggle="confirmation" data-title="are you sure?" data-imagetagid="{{$tagVal->id }}"> <i class="fal fa-times"></i></a>
              </span>
              @endforeach
              @endif
            </div>
            <span class="AddTag" onclick="addTag('{{$rentalValue->id}}')" id="{{$rentalValue->id}}">Add Tag <i class="fal fa-plus"></i></span> 
            <span id="tagText_{{$rentalValue->id}}" class="hidden tagtextltbox">
              <input type="" data-attid="{{$rentalValue->id}}"  name="tag" id="tagval_{{$rentalValue->id}}" class="tagstore{{$rentalValue->id}} tagSave">
            </span>
          
        </div>
      </div>  
      <div class="gelimgleftbox">        
        <ul  id="gelimgleftbox{{$rentalValue->id}}" class="gelimgleftboxGallery">
          @if(count($rentalValue['rentalmedia']))       
          @foreach($rentalValue['rentalmedia'] as $key=>$mediaValue)         
          @if($key <=6)
          <li id="rental{{$mediaValue->id}}">
            <span  data-toggle="confirmation" data-title="are you sure?" data-imageid="{{$mediaValue->id}}"  class="deleteimageconfirm deleteimgbox"><i class="fas fa-trash"></i></span>

            <a href="{{$mediaValue->image_url}}"><img src="{{$mediaValue->image_url}}" alt=""></a>
          </li> 
          
          @endif
          @endforeach
          @else
          <div class="gelimgleftbox">        
            <div class="noprojAvailable">
             <p>No Image</p>
            </div>
          </div>
          @endif
           </ul>
          @if(count($rentalValue['rentalMedia']) > 7)    
          <div class="viewgallbox">
             @if($authtype=='performer')
            <a href="{{route('performers.view.rentalgallery',['id'=>$rentalValue->id])}}">View<br>Gallery<br>
              <span>{{count($rentalValue['rentalMedia'])}} Images </span></a> 
              @else
              <a href="{{route('internal-staff.view.rentalgallery',['id'=>$rentalValue->id,'pid'=>$per_id])}}">View<br>Gallery<br>
                <span>{{count($rentalValue['rentalMedia'])}} Images </span>
              </a>   
              @endif  
            </div>
            @endif
       
      </div>   
         
    </div>
    @endif
    @endforeach    
    <div class="addbtnbotbox">
    <button  data-toggle="modal" data-target="#rentalModal"><i class="far fa-plus"></i> New Props Item</button>
  </div>
  </div>
  @else
  <div class="hedfullmotobox">
    <div class="gelimgleftbox">        
      <p>No Rental available</p>
    </div>
  </div>
  @endif
</div>
<!-- Rental image upload model -->
<div class="modal fade newgallerymod" id="rentalModal" role="dialog">
 <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-body">
       <div class="GalleryBoxinerdeta">        
        <form method="post" action="" enctype="multipart/form-data" id="rentalImageUpload"> 
         @csrf    
        
        <div class="titlandclobtn">
         <h3>New Props Gallery</h3>
         <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
       </div> 
       
       <div class="form-group inputbixin">
         <input type="text" class="form-control" id="rental_title" name="rental_title" placeholder="Title Gallery">
       </div>
       <div class="form-group textarbox">
         <textarea class="form-control" id="" name="description" placeholder="Add a description"></textarea>
       </div>
       <div class="dropboxzoniner">        

        <div id="Image_upload_dropzone">
          <!-- image upload dropzone --> 
          <div method="post" action="{{route('performers.store.rental.images')}}" enctype="multipart/form-data" 
          class="dropzone" id="dropzone">          
        </div>
      </div>

  
  </div>
    
        <div class="tagandimgtitle">
        <div class="leftimgtitlebox">
          <div class="form-group">
              <input type="text" class="form-control" id="media_title" name="media_title" placeholder="Image Title">
          </div>
        </div>
        <div class="rightaddtagboxset">
            <div class="boxTags">
                <div class="taglist">                  
                </div>
                <span class="addTagCreate">Add Tag <i class="fal fa-plus"></i></span>
            </div>            
        </div>
    </div>
    
</div>
<div class="imgaddedboxi">

</div>
<div class="submitbtnbox">
<input type="button" id="upload_rental_image" name="" value="submit" class="btn btn-primary">
</div>
</form>
</div>
</div>
</div>
</div>  

<!-- // add image rental -->

 <div class="modal fade newgallerymod" id="addImage" role="dialog">
 <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-body">
       <div class="GalleryBoxinerdeta">        
        <form method="post" action="" enctype="multipart/form-data" id="rentaladdImage"> 
         @csrf    
        
        <div class="titlandclobtn">
         <h3>Add Image</h3>
         <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
       </div>      
       <div class="form-group">
         <input type="hidden" class="form-control" id="rental_id" name="rental_id" >
       </div>

       <div class="dropboxzoniner">        

        <div id="Image_upload_dropzone">
          <!-- image upload dropzone --> 
          <div method="post" action="{{route('performers.store.rental.images')}}" enctype="multipart/form-data" 
          class="dropzone" id="AddImagedropzone">          
        </div>
      </div>
    </div>
 <div class="tagandimgtitle">
        <div class="leftimgtitlebox">
          <div class="form-group">
              <input type="text" class="form-control" id="media_title" name="media_title" placeholder="Image Title">
          </div>
        </div>
        <div class="rightaddtagboxset">
            <div class="boxTags">
                <div class="taglist">                  
                </div>
                <span class="addTagCreate">Add Tag <i class="fal fa-plus"></i></span>
            </div>            
        </div>
    </div>
</div>
<div class="imgaddedboxi">

</div>
<div class="submitbtnbox">
<input type="button" id="rental_image_add" name="" value="submit" class="btn btn-primary">
</div>
</form>
</div>
</div>
</div>
</div>

@endsection

@section('scripts')
<script>
     /** ADD TAG IN CREATE PROFILO **/
    $(document).ready(function() {
      jQuery('.gelimgleftboxGallery').imageview();   
        $('.addTagCreate').on('click',function(){
            var tag = $('.inputTag').val();
            if (tag != "") {
                $(this).after('<span class="boxInputTag"><input type="text" data-status="tag" id="tag0" data-id="0" class="inputTag"></span>');

                $(".inputTag").autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "{{route('performers.portfolio.getTagsList')}}",
                            type:'GET',
                            data: {
                                tags: request.term,
                                //id:$(this.element.get(0)).attr('data-attid')
                            },
                            success: function( data ) {
                                if(data.status == true){
                                    if(data.html.length > 0){
                                        response(data.html);
                                        response($.map(data.html, function (el) {

                                        return {
                                            label: el.tag,
                                            id: el.id,
                                            //protfolio_id: data.protfolio_id
                                         };
                                       }));
                                    } else {
                                        $('.inputTag').val('');
                                    }

                                }
                            }
                        });
                    }
                    ,
                    select: function( event, ui ) {
                        event.preventDefault();
                        $(".taglist").append('<span class="tagRemove" >'+ui.item.label+'<a href="javascript:void(0)" class="tagDelete"><i class="fal fa-times"></i><input type="hidden" value="'+ui.item.label+'" name="tags[]"></a></span>');
                        $('.boxInputTag').remove();
                    }
                });
            }  
        })

        $(document).on('click','.tagRemove',function(){
            $(this).remove();
        })
    });
    /** END TAG IN CREATE PROFILO **/

    
  var uploadedDocumentMap = {};
  Dropzone.options.dropzone =
  {
    maxFilesize: 100,
    renameFile: function(file) {
      var dt    = new Date();
      var time  = dt.getTime();
      return time+file.name;
    },
    type:"post",
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    addRemoveLinks: true,
    timeout: 5000,

    success: function(file, response) 
    {            
      console.log(response);
      $('#rentalImageUpload').append('<input type="hidden" name="images[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
      uploadedDocumentMap[file.name] = response;              
    },
    error: function(file, response)
    {
      console.log(file);
      return false;
    }
  };

  $('#upload_rental_image').click(function(){

    formdata = $('#rentalImageUpload').serialize();
    
    var rentalTitle = $("#rental_title").val();

    if(rentalTitle == "")
    {
      toastr.error('Enter Rental Title');
      return false;
    }
      
     // $(this).prop('disabled', true);
    
     $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
    $.ajax({
      url:"{{route('performers.store.rental.media')}}",
      type:"POST",
      data:formdata,
      success: function(response)
      {
       if(response.status == true)
       {
        location.reload();
        toastr.success(response.message);
      }else{
        toastr.error(response.message);
      }
    }
  });
  });

function addTag(r_id)
 {
  
  rental_id =r_id;
  tag ='#tagval_'+r_id;
  id = "#tagText_"+r_id;
  tag = ".tagstore"+r_id;  
  $(id).removeClass('hidden');     
 }

//   $(".AddTag").click(function(){

//   rental_id =$(this).attr('id');
//   tag ='#tagval_'+$(this).attr('id');
//   id = "#tagText_"+$(this).attr('id');
//   tag = ".tagstore"+$(this).attr('id');
  
//   $(id).removeClass('hidden');  

// });

$('.tagSave').focusout(function(){
  if(jQuery(this).val()==''){
  jQuery(this).parent('.tagtextltbox').addClass('hidden');
  }
});

function storeTags(id,tag){
  var tag_val = tag;

  if(tag_val == ""){
    $(id).addClass('hidden');  
    return false;
  }
  
  var rental_id = id;  

  $.ajaxSetup({
   headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }
 });
  $.ajax({
    url:"{{route('performers.rental.addtag')}}",
    type:"post",
    data:{rental_id:rental_id,tag:tag_val},
    success:function(response){
     if(response.status == true)
     {
                   // location.reload(2000);
                   $("#rentaltag"+rental_id).append('<span class="btnremadd" id="TagRemove'+response.data.id+'">'+response.data.tag+'<a href="javascript:void(0)" class="tagDelete DeleteTag'+response.data.id+' imagetagdeleteconfirm" data-toggle="confirmation" data-title="are you sure?" data-imagetagid='+response.data.id+'> <i class="fal fa-times"></i></a></span>');
                   toastr.success(response.message);
                   $(tag).val("");
                   $(id).addClass('hidden');
                   $('.tagSave').val('');
                    $('.tagtextltbox').addClass('hidden');
                    $('.imagetagdeleteconfirm[data-toggle=confirmation]').confirmation({
                              rootSelector: '[data-toggle=confirmation]',
                              container: 'body',
                              onConfirm: function() {
                                 tagDelete($(this).data('imagetagid'));
                              },
                            });
                 }},
                 error: function(file, response)
                 {
                  
                  return false;
                }
                
              });
  
}


function rentalMediaDelete(id)
{  
  $("#rental"+id).remove();
  $.ajax({
         url:"{{route('performers.rental.removeMedia')}}",
        type:'get',
        data:{id:id},
        success:function(response){
          toastr.options = {
                 "preventDuplicates": true,
                 "preventOpenDuplicates": true
                 };
            if(response.status == true)
            {
              toastr.success(response.message);                   
            }
        }

  });
  
}


  function imageAddTogallery(id)
  {
    id =id;
   $("#rental_id").val(id);
   $("#addImage").modal('show'); 
  }
   
   

var uploadedDocumentMap = {};
  Dropzone.options.AddImagedropzone =
  {
    maxFilesize: 100,
    renameFile: function(file) {
      var dt    = new Date();
      var time  = dt.getTime();
      return time+file.name;
    },
    type:"post",
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    addRemoveLinks: true,
    timeout: 5000,

    success: function(file, response) 
    {            
      console.log(response);
      $('#rentaladdImage').append('<input type="hidden" name="images[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
      uploadedDocumentMap[file.name] = response;              
    },
    error: function(file, response)
    {
      console.log(file);
      return false;
    }
  };


  $("#rental_image_add").click(function(){
   
    formdata = $('#rentaladdImage').serialize();   
    $(this).prop('disabled', true);
    $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
    $.ajax({
      url:"{{route('performers.add.rental.images')}}",
      type:"POST",
      data:formdata,
      success: function(response)
      {
       if(response.status == true)
       {
        location.reload();
        toastr.success(response.message);
      }else{
        toastr.error(response.message);
      }
    }
  });
  });  


  $(document).ready(function(){
    $("#rentalSearch").keyup(function(){
    search = $(this).val();
    get_id = $("#get_id").val();
      $.ajax({
        url:"{{route('performers.rental.search')}}",
        type:'GET',
        data:{search:search,get_id:get_id},
        success:function(data)
        {
          if(data.status == true){        
            $("#defaultDisplayDiv").html(data.html);
             $('.gelimgleftboxGallery').imageview();
             $('.deleteimageconfirm[data-toggle=confirmation]').confirmation({
                         rootSelector: '[data-toggle=confirmation]',
                         container: 'body',
                         onConfirm: function() {
                            rentalMediaDelete($(this).data('imageid'));
                         },
                       });
             $('.tagSave').focusout(function(){
               if(jQuery(this).val()==''){
               jQuery(this).parent('.tagtextltbox').addClass('hidden');
               }
             });
             $('.imagetagdeleteconfirm[data-toggle=confirmation]').confirmation({
                        rootSelector: '[data-toggle=confirmation]',
                        container: 'body',
                        onConfirm: function() {
                           tagDelete($(this).data('imagetagid'));
                        },
                      });
             $(".tagSave").autocomplete({
                 //source: availableTags
                 source: function(request, response) {
                     $.ajax({
                         url: "{{route('performers.rental.getTags')}}",
                         type:'GET',
                         data: {
                             tags: request.term,
                             id:$(this.element.get(0)).data('attid')
                         },
                         success: function( data ) {
                             if(data.status == true){
                                 if(data.html.length > 0){
                                     //response(data.html);
                                     response($.map(data.html, function (el) {

                                      return {
                                        label: el.tag,
                                        id: el.id,
                                        rental_id: data.rental_id
                                      };
                                    }));
                                 } else {
                                    // $('.tagSave').val('');
                                     var result = [{ label: "no results", value: response.term }];
                                 response(result);
                                 }

                             }
                         }
                     });
                 },
                                   change: function (event, ui) {
                                         if (ui.item == null){ 
                                          //here is null if entered value is not match in suggestion list
                                            //toastr.warning("click on add Agent if you want add new Value");
                                             $(this).val((ui.item ? ui.item.id : ""));
                                         }
                                     },select: function( event, ui ) {

                     event.preventDefault();
                     var label = ui.item.label;
                                 if (label === "no results") {

                                 }
                                 else{

                    // console.log();
                    // var id = $(this.element.get(0)).attr('data-attid');
                     $('.tagSave').val(ui.item.label);
                     var id = ui.item.rental_id;
                     var tag = ui.item.label;

                     console.log(ui.item)
                     storeTags(id,tag);

                   }
                 }
             });

            }
        }
      });
         
  });
});

  function tagDelete(id)
{ 
  $("#TagRemove"+id).remove();
    $.ajax({
      url:"{{route('performers.rental.removetag')}}",
      type:'get',
      data:{id:id},
      success:function(data){        
        if(data.status == true)
        {   
          toastr.success(data.message);     

        }      
      }
    });
}

/** auto complated tags ajax base **/
$(".tagSave").autocomplete({
    //source: availableTags
    source: function(request, response) {
        $.ajax({
            url: "{{route('performers.rental.getTags')}}",
            type:'GET',
            data: {
                tags: request.term,
                id:$(this.element.get(0)).data('attid')
            },
            success: function( data ) {
                if(data.status == true){
                    if(data.html.length > 0){
                        //response(data.html);
                        response($.map(data.html, function (el) {

                         return {
                           label: el.tag,
                           id: el.id,
                           rental_id: data.rental_id
                         };
                       }));
                    } else {
                       // $('.tagSave').val('');
                        var result = [{ label: "no results", value: response.term }];
                    response(result);
                    }

                }
            }
        });
    },
                      change: function (event, ui) {
                            if (ui.item == null){ 
                             //here is null if entered value is not match in suggestion list
                               //toastr.warning("click on add Agent if you want add new Value");
                                $(this).val((ui.item ? ui.item.id : ""));
                            }
                        },select: function( event, ui ) {

        event.preventDefault();
        var label = ui.item.label;
                    if (label === "no results") {

                    }
                    else{

       // console.log();
       // var id = $(this.element.get(0)).attr('data-attid');
        $('.tagSave').val(ui.item.label);
        var id = ui.item.rental_id;
        var tag = ui.item.label;

        console.log(ui.item)
        storeTags(id,tag);

      }
    }
});
 $('.deleteimageconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
                rentalMediaDelete($(this).data('imageid'));
             },
           });
  $('.imagetagdeleteconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
                tagDelete($(this).data('imagetagid'));
             },
           });
$('.gallaryconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
                DeleteGallery($(this).data('imagetagid'));
             },
           });
//Delete Gallery
function DeleteGallery(id)
{
   $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
  $.ajax({
       url:"{{route('performers.rentalGalleryRemove')}}",
       type:"post",
       data:{id:id},
       success:function(data)
       {
         if(data.status == true)
         {
            $("#galleryDelete"+id).remove();
            toastr.success(data.message);

         }
       }

  });

}
</script>
@include('performers.commonRental')
@endsection