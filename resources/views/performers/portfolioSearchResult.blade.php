@if(count($search_result))
@foreach($search_result as $portfolioValue)
  @if($portfolioValue->is_video == "no")
  <div class="hedfullmotobox">
    <div class="allboxtitlesem">
      <div class="titleboxallcover">
      <div class="titledivallbox"><h3> <a href="javascript:void(0)"  onclick="viewGallery('{{ $portfolioValue['id'] }}','{{ $portfolioValue['portfolio_title'] }}')" >{{$portfolioValue['portfolio_title'] }}</a></h3></div>
      <div class="helpboxdcl">
        <span class="tool" data-tip="{{isset($helpdesk->portfolio_help)? $helpdesk->portfolio_help :'' }}" tabindex="1"><i class="fal fa-question-circle"></i></span>
        
      </div>
      <div class="morbtnmnubox">
        <div class="morebtneddil">                
          <div class="MenuSceinain">
            <div class="menuheaderinmore">
               @if(Auth::guard('web')->check())
              <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
              <div class="mainineropenbox_1">                
                <div class="mainineropenbox_2">
                  <div class="opensubmenua_1">
                    <a href="javascript:void(0)" id="{{$portfolioValue->id}}"  class="imageAddTogallery" onclick="imageAddTogallery('{{$portfolioValue->id}}')">Add</a>
                  </div> 
                  <div class="opensubmenua_1">
                    <a href="javascript:void(0)"  onclick="viewGallery('{{ $portfolioValue['id'] }}','{{ $portfolioValue['portfolio_title'] }}')" >Edit</a>
                  </div>
                </div> 
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>

    <div class="imgvidaodbox">
      <div id="portfolio{{$portfolioValue->id}}">
        @if(count($portfolioValue->portfoliotag))
        @foreach($portfolioValue->portfoliotag as $tagVal)
            <span class="btnremadd" id="TagRemove{{$tagVal->id}}">
              {{$tagVal->tag}} 
              <a href="javascript:void(0)" class="DeleteTag{{$tagVal->id }} imagetagdeleteconfirm"  data-toggle="confirmation" data-title="are you sure?" data-imagetagid="{{$tagVal->id }}"> <i class="fal fa-times"></i></a>
            </span>
        @endforeach
        @endif
      </div>
      <span class="AddTag" onclick="addTag({{$portfolioValue->id}})" id="{{$portfolioValue->id}}">Add Tag<i class="fal fa-plus"></i></span> 
      <span id="tagText_{{$portfolioValue->id}}" class="hidden tagtextltbox">
        <input type="" data-attid="{{$portfolioValue->id}}"  name="tag" id="tagval_{{$portfolioValue->id}}" class="tagstore{{$portfolioValue->id}} tagSave">
      </span>
    </div>
      </div>
    </div>  
    <div class="gelimgleftbox">        
      <ul id="gelimgleftbox{{$portfolioValue->id}}" class="gelimgleftboxGallery">
        @if(count($portfolioValue['performermedia']))       
        @foreach($portfolioValue['performermedia'] as $key=>$mediaValue)         
        @if($key <=6)

        <li id="portfolioMedia{{$mediaValue->id}}">
           @if(Auth::guard('web')->check()) 
          <span href="javascript:void(0)"  class="deleteimageconfirm deleteimgbox" data-toggle="confirmation" data-title="are you sure?" data-imageid="{{$mediaValue->id}}"><i class="fas fa-trash" ></i></span>
          @endif
           <a href="{{$mediaValue->image_url}}"> <img src="{{$mediaValue->image_url}}" alt=""></a>
        </li> 
        @endif
        @endforeach
        @else
        <div class="gelimgleftbox">        
          <p>No Image</p>
        </div>
        @endif
      </ul>
    @if(count($portfolioValue['performermedia']) > 7)    
    <div class="viewgallbox">
      <a href="{{route('performers.view.gallery',['id'=>$portfolioValue->id])}}">View<br>Gallery<br>
      <span>{{count($portfolioValue['performermedia'])}} Images </span></a>   
    </div>
    @endif
   
    </div>   
  </div>

@endif
@endforeach
<!-- //video listing -->
@foreach($search_result as $portfolioValue)
  @if($portfolioValue->is_video == "yes")
  <div class="hedfullmotobox">
    <div class="allboxtitlesem">
      <div class="titleboxallcover">
        <h3>{{$portfolioValue['portfolio_title'] }}</h3>
        <div class="morebtneddil">                
                <div class="MenuSceinain">
                  <div class="menuheaderinmore">
                    <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
                    <div class="mainineropenbox_1">
                      <div class="mainineropenbox_2">
                        <div class="opensubmenua_1">
                          <a href="javascript:void(0)" id="{{$portfolioValue->id}}" class="videoAddTogallery" onclick="videoAddTogallery('{{$portfolioValue->id}}')" >Add</a>
                        </div> 
                      
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
        <div class="imgvidaodbox">
          <div id="portfolio{{$portfolioValue->id}}">
            @if(count($portfolioValue->portfoliotag))
            @foreach($portfolioValue->portfoliotag as $tagVal)
            <span class="btnremadd" id="TagRemove{{$tagVal->id}}">
              {{$tagVal->tag}} 
              <a href="javascript:void(0)" onclick="tagDelete('{{$tagVal->id }}')"> <i class="fal fa-times"></i></a>
            </span>
            @endforeach
            @endif
          </div>
          <span class="AddTag" onclick="addTag({{$portfolioValue->id}})" id="{{$portfolioValue->id}}">Add Tag<i class="fal fa-plus"></i></span> 
          <span id="tagText_{{$portfolioValue->id}}" class="hidden tagtextltbox">
            <input type="" data-attid="{{$portfolioValue->id}}"  name="tag" id="tagval_{{$portfolioValue->id}}" class="tagstore{{$portfolioValue->id}} tagSave" >
          </span>
        </div>
      <!--   <div class="editboxinerpart">
          <a href="javascript:void(0)" id="{{$portfolioValue->id}}" class="videoAddTogallery"><i class="far fa-plus"></i></a>

          <a href="javascript:void(0)"><i class="far fa-edit"></i></a>
        </div> -->
      </div> 
      <div class="audiorealboxp"> 

        <div class="videoplaybtnbox"> 
          @if(count($portfolioValue['performermedia']))        
          @foreach($portfolioValue['performermedia'] as $mediaValue)         
          <!-- <div class="btnplaybox" id="portfolioMedia{{$mediaValue->id}}">
            <a href="javascript:void(0)" data-key="{{$mediaValue->id}}" onclick="portfolioshowVideo('{{$mediaValue->id}}')" class="portfolioshowVideo">
              <div class="playiconbtn"><i class="fas fa-play"></i></div> 
              <span class="medtitleleft">{{$mediaValue->media_title}}</span> 
              <span class="dateformright">{{$portfolioValue->created_at}}</span>              
            </a>
            <div class="rigtmorebtnboxmid">              
              
              <div class="morebtneddil">                
                <div class="MenuSceinain">
                  <div class="menuheaderinmore">
                    <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
                    <div class="mainineropenbox_1">
                      <div class="mainineropenbox_2">
                         <div class="opensubmenua_1">
                          <a href="javascript:void(0)"  onclick="portfolioMediaDelete('{{$mediaValue->id}}')">Delete</a>
                        </div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
            </div>           
          </div> -->

           <div class="audioboxplbox" id="audiodelete{{$mediaValue->id}}">
            <div class="audioboxpcoverbox">
              <audio id="music" preload="true">
                <source src="{{$mediaValue->image_url}}">                
                </audio>
                <div id="audioplayer" class="audioplayerbox">
                  <button id="pButton" class="play"></button>
                  <div id="timeline" class="timelinebox">    
                    <div id="playhead"></div>
                  </div>
                </div>
                <a href="javascript:void(0)"  class="audioDeleteconfirm"  data-toggle="confirmation" data-title="are you sure?" data-audioid="{{$mediaValue->id}}">
                  <i class="fal fa-trash"></i>
                </a> 
              </div>                        
            </div>


            @endforeach
            @else

            <div class="gelimgleftbox">        
              <p>No Audio</p>
            </div>
            @endif
          </div>
        </div>   
      </div>
    </div>
@endif
@endforeach
@endif
