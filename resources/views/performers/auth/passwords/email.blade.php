@extends('performers.layouts.app')
@section('content')
<div class="container">
    <div class="logonamecover">
        
        <img src="{{ asset('assets/images/logo.png') }}" alt="">
        
        <h3>For Background Casting Performers</h3>
    </div>
</div>
<section class="formdisinercover">
         <form method="POST" action="{{ url('performers/password/email') }}" class="mainformcl">
         @csrf         
        <div class="container">            
            <div class="alldabtitlcover">
                <div class="signupboxdis">
                    <h2>{{ __('Reset Password') }}</h2>
                </div>
                <div class="tab current tabdisinerbox">                 
                    <div class="detainerformall">
                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                          @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>                                
                    </div>
                </div>
                <div class="btn-control nextpribtnbox">
                    <button style="width:60%;" type="submit" class="cadastrobtn visible"> {{ __('Send Password Reset Link') }}</button>
                    <br>                   
                </div>
                 <a class="btn btn-link signpboxlink" href="{{ route('performers.login') }}">Login</a>
            </div> 
        </div> 
    </form>
</section>
@endsection
