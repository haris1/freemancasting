@extends('performers.layouts.app')
@section('title','Freemancasting | Login')
@section('content')
<div class="container">
    <div class="logonamecover">
        
        <img src="{{ asset('assets/images/logo.png') }}" alt="">
        
        <h3>Welcome Performer</h3>
    </div>
</div>
<section class="formdisinercover">
    <form id="regForm" class="mainformcl" method="POST" action="{{ route('performers.login') }}">
        <div class="container">
            
            <div class="alldabtitlcover">
                <div class="signupboxdis">
                    <h2>Login</h2>
                </div>
                @csrf
                <div class="tab current tabdisinerbox">                 
                    <div class="detainerformall">
                        <div class="form-group">
                            <label for="usr">Username</label>
                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="emailaddress" required autocomplete="email" value="{{ old('email') }}" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="usr">Password</label>
                            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="" required autocomplete="current-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                         <div class="form-group hidden" id="firstNameOption" >
                            <label for="usr">Child Name</label>
                            <div class="seletcustboxpart2">
                               <select class="form-control" name="firstName" id="firstName">
                                   <option></option>
                               </select>
                           </div>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>                     
                    </div>
                </div>
                <div class="btn-control nextpribtnbox">
                    <button type="submit" class="cadastrobtn visible">Submit</button>
                    <br>

                    
                    <a class="btn btn-link forgotpasslink" href="{{ route('performers.password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                   
                </div> 
                    <a class="btn btn-link signpboxlink" href="{{ route('performers.register') }}">Sign Up</a>
                
            </div> 
        </div> 
    </form>
</section>
@endsection
@section('scripts')
<script type="text/javascript">

    $('#firstName').select2({
      placeholder: "Select"
    });
    

    $("#emailaddress").focusout(function(){     
        $.ajax({
            type:'get',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:'{{ route("performers.checkUser") }}',
            data: {email: $("#emailaddress").val()},
            
            beforeSend:function(){},
            success:function(response) {
                if (response.count == 1) {
                 jQuery('#firstName').html(response.html);
                 jQuery('#firstNameOption').removeClass('hidden');
                }else{
                 jQuery('#firstNameOption').addClass('hidden');
                }
            },
            complete:function(){},
            error:function(){},

        });
    });
</script>
@endsection