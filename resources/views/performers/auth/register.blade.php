@extends('performers.layouts.app')
@section('title','Freemancasting | Sign In')
@section('content')



<div class="container">
    <div class="logonamecover">
        
        <img src="{{ asset('assets/images/logo.png') }}" alt="">
        
        <h3>For Background Casting Performers</h3>
    </div>
</div>
<section class="formdisinercover">
    <form id="regForm" method="POST" class="mainformcl" action="{{ route('performers.signup') }}" enctype="multipart/form-data">
        <div class="container">
            
            <div class="alldabtitlcover">
                <div class="signupboxdis">
                    <h2>Sign Up</h2>
                </div>
                <div class="tab current tabdisinerbox tabbox1">

                    <div class="detainerformall">                   
                        <div class="textdidisclaimer">
                            
                            <label class="ckeck-container">
                                <input type="checkbox" checked="checked" id="iAgree">
                                <!-- <span class="checkmark"></span> -->
                             <h2>TERMS OF USE</h2>
<p>&nbsp;</p>
<p>PLEASE READ THESE TERMS OF USE CAREFULLY. IT SETS FORTH THE LEGALLY BINDING TERMS AND CONDITIONS FOR YOUR ACCESS TO AND USE OF THE WEBSITE AND SERVICE (DEFINED BELOW).</p>
<p>The Freeman Casting website located at www.freemancasting.com (the “<strong>Website</strong>”) is licensed, operated and owned and made available to you by Sandra-Ken Freeman Productions Ltd., a Canadian corporation doing business as Freeman Casting (hereinafter referred to as “<strong>Company</strong>”, “<strong>our</strong>”, “<strong>us</strong>” or “<strong>we</strong>”) and is being made available to you subject to the following terms of use (“<strong>Terms of Use</strong>”). By accessing and using the Website and services provided by the Company in connection therewith (the “<strong>Service</strong>”), you (and your parent/legal guardian on your behalf, if you are under the age of majority in your jurisdiction of residence) (hereinafter referred to as “<strong>you</strong>”, “<strong>your</strong>”, or “<strong>user</strong>”) signify that you have read, fully understand and agree to be bound by these Terms of Use and by the Privacy Policy which is available at: www.freemancasting.com/privacy-policy. You represent and warrant that you have the legal right and capacity to enter into these Terms of Use in your jurisdiction. If you do not agree with these Terms of Use or the Privacy Policy, your sole recourse is to leave the Website and to stop using the Service immediately.</p>
<p>We reserve the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Website (or any part thereof) and Service (or any part thereof) with or without notice and without liability to you. From time to time, we may also amend these Terms of Use by posting revisions on the Website with no additional notice to you. The amended Terms of Use will be effective as of the time of posting, or such later date as may be specified in the amended Terms of Use. Please check back frequently to see the Terms of Use then in effect. Your continued access to and use of the Website and Service will constitute acceptance of the amended Terms of Use. <strong>If you do not agree with these Terms of Use, or any future amendments, your sole recourse is to cease use of the Website and Service.</strong></p>
<p>If you have any questions or concerns about these Terms of Use or the Privacy Policy please email us at: info@freemancasting.com.</p>
<p>&nbsp;</p>
<p><strong>I. Service Description</strong></p>
<p>The Website and Service serves the user, background performer casting personnel, producers, Service providers, and other employers by allowing the user to submit a general application to the Company containing relevant personal information including, but not limited to, photographs, personal characteristics, employment eligibility, contact information, and such other information as may be required or submitted by individuals in order to be considered for engagement as a background performer (collectively, “<strong>Information</strong>”). The purpose of the collection of this Information enables background performer casting personnel, producers, Service providers, and other employers to search for, select and cast users as background performers in film, television or other productions. Upon the selection of a user as a prospective background performer in a film, television or other production, the Company will retain said user’s Information and may contact selected users for potential casting in a current or future film, television or other production. <strong>You acknowledge and agree that the Company does not guarantee that you or any other applicant will be selected or engaged for use as a background performer in any film, television and/or other production.</strong> Users are further provided with access to information about the Company, as well as a host of related, interactive and other online experiences. We may augment, change, suspend, modify or discontinue any aspect of the Website or Service at any time without notice to you. We are not a talent agency and do not collect fees of any kind from the user.</p>
<p>&nbsp;</p>
<p><strong>II. Consent to the Collection, Use and Disclosure of Data</strong></p>
<p>In order to use the Services, you must submit a general application to the Company and provide certain Information about yourself. The Company will collect and retain, use and disclose the Information and any other information you choose to provide for the purposes of (i) verifying your identity and age, (ii) managing your application, (iii) providing the Services, (iv) searching for, selecting and casting users as background performers in film, television and/or other productions, and (v) any other purpose indicated in the Company’s Privacy Policy. You represent and warrant that: (i) you own and/or are authorized to provide all of the Information in your submitted application to the Company; (ii) the submission and use of your Information through the Service does not violate, misrepresent, misappropriate or infringe on the rights of any third party, including, without limitation, privacy rights, publicity rights, copyrights, trademark and/or other intellectual property rights; and (iii) the Information you provide is complete, accurate, current, and truthful information. You further agree to contact the Company to update your Information as necessary to maintain its truth and accuracy. The Company does not claim ownership of any Information that you submit in your application through the Service. Instead, you hereby grant to the Company a non-exclusive, fully paid and royalty-free, transferable, sub-licensable, worldwide license to use the Information that you submit in your application through the Service, subject to the Company’s Privacy Policy.</p>
<p>We do not knowingly collect Information from individuals under the age of majority in the jurisdiction of their residence (“<strong>Minor</strong>”) without the consent of their parent or legal guardian. Accordingly, all Services that require the submission of Information to the Company are restricted to persons who are the age of majority in the jurisdiction of their residence. Notwithstanding the foregoing, if you are a Minor and have the consent of your parent or legal guardian to submit your Information to the Company, you may submit Information to the Company only with the involvement of a parent of legal guardian who agrees to these Terms of Use and to be responsible for your use of the Service. IF YOU ARE A MINOR, DO NOT SUBMIT AN APPLICATION TO THE COMPANY UNLESS YOU HAVE THE CONSENT AND INVOLVEMENT OF YOUR PARENT OR LEGAL GUARDIAN. Until you are the age of majority in your jurisdiction of residence, your parents or legal guardians may ask us to modify, deny access to, or delete your application and we may do so at their request at any time, for any reason, without notice or liability. To submit Information to the Company and to use the Service, the Company may require that the parents or legal guardians of Minors enter into a separate agreement to evidence the consent of the parent or legal guardian. If we become aware that we have collected Information from Minors without verification of parental consent, we will discard and delete said Information.</p>
<p>Except as otherwise described in the Company’s Privacy Policy, as between you and the Company, any Information submitted by you to the Company will be non-confidential and non-proprietary and the Company will not be liable for any use or disclosure of the Information, subject to the Company’s Privacy Policy. You acknowledge and agree that your relationship with the Company is not a confidential, fiduciary, or other type of special relationship, and that your decision to submit any Information does not place the Company in a position that is any different from the position held by members of the general public, including with regard to your Information, subject to the Company’s Privacy Policy.</p>
<p>&nbsp;</p>
<p><strong>III. Privacy Policy</strong></p>
<p>In order to use the Website and the Services, you may be asked to provide certain personal Information. The collection, use and disclosure of this personal Information is governed by the Company’s Privacy Policy, which is available at www.freemancasting.com/privacy-policy. By using the Website and the Services, you agree to the Company’s Privacy Policy.</p>
<p>&nbsp;</p>
<p><strong>IV. Use of Website, Service and User Conduct</strong></p>
<ol>
<li>If you are a Minor, by using this Website and Service, including the submission of Information to the Company, you acknowledge that your parents or legal guardians have consented to your use and submission and to these Terms of Use on your behalf, and you acknowledge and agree that your submission of Information to the Company and use of the Website and Service is at their discretion.</li>
<li>You may not submit in your application violent, nude, partially nude, discriminatory, unlawful, infringing, hateful, pornographic or sexually suggestive photos or other content.</li>
<li>With the exception of people or businesses that are expressly authorized to submit applications to the Company on behalf of their employers or clients, the Company prohibits the submission of and you agree that you will not submit an application for anyone other than yourself and that you will not submit more than one application for yourself. If it is discovered that you have submitted more than one application the Company has the right, but not the obligation to remove all of your applications and to remove you from consideration for potential casting as a background performer in film, television and/or other productions.</li>
<li>You agree that you will not solicit, collect or use the Information of other users of the Company’s Service.</li>
<li>You agree that in the event that you discover any breach of security, you will promptly notify us at info@freemancasting.com. You agree the Company is not responsible for your failure to comply with this clause, or for any delay in shutting down your access to the Website or Service after you have reported a breach of security to the Company.</li>
<li>You may not use the Service for any illegal or unauthorized purpose. You agree to comply with all laws, rules and regulations applicable to your use of the Service and your Information, including, without limitation, copyright laws.</li>
<li>You are solely responsible for your conduct and any data, text, files, information, usernames, images, graphics, photos, profiles, audio and video clips, sounds, musical works, works of authorship, applications, links and other content or materials that you submit to the Company.</li>
<li>You must not change, modify, adapt or alter the Service or change, modify or alter another website so as to falsely imply that it is associated with the Service or the Company.</li>
<li>You may not inject content or code or otherwise alter or interfere with the way any Website page is rendered or displayed in a user&#8217;s browser or device.</li>
<li>You agree not to attempt to damage, deny service to, hack, crack, reverse-engineer, or otherwise disrupt or interfere with the Website, Service or servers or networks connected to the Website of Service in any manner, including, without limitation, by transmitting any worms, viruses, spyware, malware or any other code of a destructive or disruptive nature (collectively, “<strong>Interfere</strong>”). If you in any way Interfere with the Website, you agree to pay all damages incurred by the Company, including any consequential damages, and agree that the measure of harm to evaluate such damages will be the highest estimate of damages as provided for by the Company. Your Interference with the Website relieves the Company of any of its contractual and any other legal obligations to you. The Company will cooperate with the authorities in prosecuting any user who Interferes with the Website, attempts to defraud the Company through user’s use of the Website or Services.</li>
<li>You must not submit applications to the Company through unauthorized means, including, but not limited to, by using an automated device, script, bot, spider, crawler or scraper, or any other similar device now known or hereafter devised.</li>
<li>You must not attempt to restrict another user from using or enjoying the Website or Service and you must not encourage or facilitate violations of these Terms of Use or any other terms or policy.</li>
<li>You acknowledge and agree that the Company is not a backup service and you will not rely on the Company for the purposes of backup or storage of your Information. The Company will not be liable to you for any modification, suspension, or discontinuation of the Services, or the loss of any of your Information. You also acknowledge that the Internet may be subject to breaches of security and that the submission of Information may not be secure.</li>
<li>We reserve the right to refuse access to the Website or Service to anyone for any reason at any time.</li>
</ol>
<p>&nbsp;</p>
<p><strong>V. Intellectual Property</strong></p>
<p>Unless otherwise indicated, all text, data, graphics, photographs, images, audio, video, trademarks, service marks, trade names and other information, visual or other digital material, software (including source and object codes) and all other content of any description available on the Website or Service or available via a link from the Website to a page created by us on another website (collectively, the “<strong>Content</strong>”), are the sole property of the Company and/or its licensors. All Content is protected by copyright, trade-mark, service marks, patents, trade secrets and other proprietary rights and laws. Systematic retrieval of data or other content from the Website to create or compile, directly or indirectly, a collection, compilation, database or directory is prohibited. In addition, use of the Content for any purpose not expressly permitted in these Terms of Use is prohibited. You may not copy, reproduce, perform, sell or create derivative works of the Content.</p>
<p>&nbsp;</p>
<p><strong>VI. Links to Third-Party Websites</strong></p>
<p>We may offer hyperlinks on the Website to the websites of third parties. Such links are provided only as a convenience. We do not review the content of such websites, and neither endorse, nor are responsible for, any content, advertising, products, services or other materials on or available from such third party websites. You assume full responsibility for your use of third party websites. Such websites may be governed by terms and conditions different from those applicable to this Website, and we encourage you to review the terms and privacy policies of those third parties before using their websites.</p>
<p>You acknowledge and agree that your dealings with any third parties, including any merchants or advertisers linked to the Website, and all other terms, conditions, representations and warranties related to such dealings, are solely between you and such third parties. You agree that the Company will not be responsible or liable in any way for any loss or damage of any kind incurred as a result of, or in connection with, any such dealings or transactions.</p>
<p>We may also offer links to Content created by us and available on other websites. If you link to that Content you are responsible for ensuring that you comply with the terms of use applicable to those websites while you are visiting them.</p>
<p>&nbsp;</p>
<p><strong>VII. Indemnification</strong></p>
<p>By using the Website and/or Service, you agree to defend, indemnify and hold harmless the Company, and our licensors and partners, and their respective employees, officers, directors, or agents (each, a “<strong>Released Party</strong>”, collectively, the “<strong>Released Parties</strong>”) from any and all costs, claims, fines, penalties, demands, investigations, liabilities, losses, damages, judgments, settlements and expenses, including, without limitation, attorneys’ fees and other legal expenses, whether in tort, contract or otherwise, relating to or arising out of your (or anyone acting under your password or username) use or direct activities on the Service, breach of these Terms of Use, infringement of the rights of any third party, violation of any laws, rules, regulations, or orders of any governmental and quasi-governmental authorities, including, without limitation, all regulatory, administrative and legislative authorities, or any misrepresentation made by you.</p>
<p>&nbsp;</p>
<p><strong>VIII. No Warranties</strong></p>
<p>YOUR USE OF OUR WEBSITE AND SERVICE IS AT YOUR SOLE RISK. ALL CONTENT AND USER CONTENT INCLUDED ON OR AVAILABLE THROUGH THE WEBSITE IS PROVIDED TO YOU ON AN “AS IS”, “AS AVAILABLE” AND “WITH ALL FAULTS” BASIS, WITHOUT ANY REPRESENTATIONS OR WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED. NEITHER THE COMPANY NOR ANY OF ITS EMPLOYEES, MANAGERS, OFFICERS OR AGENTS MAKE ANY REPRESENTATION OR WARRANTY OF ANY KIND WHATSOEVER THAT THE CONTENT IS ACCURATE, RELIABLE, FACTUAL OR CORRECT; THAT THE WEBSITE AND SERVICE WILL BE AVAILABLE AT ANY PARTICULAR TIME OR LOCATION; THAT YOUR ACCESS TO THE WEBSITE OR SERVICE WILL BE UNINTERRUPTED; THAT ANY DEFECTS OR ERRORS WILL BE CORRECTED; THAT THE CONTENT IS FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS; THAT THE CONTENT IS TIMELY, SECURE OR ERROR-FREE; OR REGARDING THE SECURITY ASSOCIATED WITH THE TRANSMISSION OF INFORMATION TO THE COMPANY VIA THE SERVICE OR OTHERWISE.</p>
<p>Neither reference to a product or service by trade name, trademark, manufacturer, supplier or otherwise, nor a review of such product or service, constitutes or implies endorsement, sponsorship or recommendation thereof, or any affiliation therewith, by the Company. We do not sell, resell, or license any of the products or the services that we review, list, or advertise on the Website, and we disclaim any responsibility for or liability related to them.</p>
<p>TO THE MAXIMUM EXTENT PERMITTED BY LAW, WE DISCLAIM ALL WARRANTIES, INCLUDING, WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, TITLE, CUSTOM, TRADE, QUIET ENJOYMENT, SYSTEM INTEGRATION AND FREEDOM FROM COMPUTER VIRUS. BECAUSE SOME JURISDICTIONS DO NOT PERMIT THE EXCLUSION OF CERTAIN WARRANTIES, THESE EXCLUSIONS MAY NOT APPLY TO YOU.</p>
<p>&nbsp;</p>
<p><strong>IX. Disclaimer / Limitation of Liability</strong></p>
<p>Although we strive to update and keep accurate as much as possible the SERVICE OR content contained on the Website, errors and/or omissions may occur. BY ACCESSING THE WEBSITE AND SERVICE, YOU UNDERSTAND YOU MAY BE WAIVING RIGHTS WITH RESPECT TO CLAIMS THAT ARE AT THIS TIME UNKNOWN OR UNSUSPECTED.</p>
<p>UNDER NO CIRCUMSTANCES WILL WE, OR ANY RELEASED PARTY, BE LIABLE FOR ANY LOSS, INJURY, CLAIM, LIABILITY OR DAMAGE OF ANY KIND RESULTING FROM YOUR USE OF OUR WEBSITE OR THE SERVICES. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, EACH OF THE COMPANY AND THE RELEASED PARTIES DISCLAIM ALL RESPONSIBILITY FOR ANY LOSS, INJURY, CLAIM, LIABILITY OR DAMAGE OF ANY KIND, INCLUDING DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR PUNITIVE DAMAGES OF ANY KIND (INCLUDING, WITHOUT LIMITATION, ATTORNEYS’ FEES AND OTHER LEGAL EXPENSES) RESULTING FROM, ARISING OUT OF OR IN ANY WAY RELATED TO YOUR USE OF OUR WEBSITE, SERVICES, OR THE CONTENT, INCLUDING, WITHOUT LIMITATION, (A) ANY ERRORS IN, OR OMISSIONS FROM, OUR WEBSITE, SERVICES AND THE CONTENT; (B) ANY THIRD-PARTY WEBSITES, CONTENT AND/OR SERVICES DIRECTLY OR INDIRECTLY ACCESSED THROUGH LINKS FROM OUR WEBSITE; (C) THE UNAVAILABILITY OF OUR WEBSITE, SERVICES OR ANY PORTION THEREOF; (D) YOUR USE OF, OR INABILITY TO USE, OUR WEBSITE OR SERVICES; (E) ANY ERRORS IN, OMISSIONS FROM, OR IDEAS, INFORMATION AND/OR OPINIONS CONTAINED IN USER CONTENT ON THE WEBSITE; (F) YOUR USE OF ANY EQUIPMENT OR SOFTWARE IN CONNECTION WITH OUR WEBSITE; OR (G) ANY DAMAGE TO ANY USER’S COMPUTER, MOBILE DEVICE, OR OTHER EQUIPMENT OR TECHNOLOGY INCLUDING, WITHOUT LIMITATION, DAMAGE FROM ANY SECURITY BREACH OR COMPUTER VIRUSES, WORMS OR TROJAN HORSES, ANY OTHER TYPE OF DESTRUCTIVE OR MALICIOUS COMPUTER CODE (BY WHATEVER NAME IT IS CALLED), OR ANY UNAUTHORIZED COMPUTER CODE THAT IS ATTACHED TO, OR MADE A PART OF, OR UNAUTHORIZED ACCESS TO, OUR WEBSITE BY ANY PERSON, GROUP OR ORGANIZATION. Some jurisdictions do not allow exclusion or limitation of CERTAIN damages. ACCORDINGLY, the EXCLUSIONS AND/OR limitations above may not apply to you.</p>
<p>Each Released Party is hereby constituted a third party beneficiary of the provisions of these Terms of Use.</p>
<p>APPLICABLE LAW MAY NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY NOT APPLY TO YOU. IN NO CIRCUMSTANCE WILL LIABILITY EXCEED THE AMOUNT PAID FOR A GIVEN SERVICE.</p>
<p>&nbsp;</p>
<p><strong>X. Notices</strong></p>
<p>Except as explicitly stated otherwise or as required by law, you will provide any notices to the Company by contacting us in writing at the following address:</p>
<p>4021 Hastings Street<br />
Vancouver, BC<br />
V5C 2J1</p>
<p>&nbsp;</p>
<p><strong>XI. Governing Law and Forum</strong></p>
<p>These Terms of Use are governed by and will be interpreted in accordance with the laws of the Province of British Columbia, and the federal laws of Canada applicable therein, without regard to any principles of conflicts of law. You agree that any action to enforce these Terms of Use may be brought in the courts located in the Province of British Columbia. You further agree to submit to the personal jurisdiction of these courts for the purpose of any proceeding arising out of these Terms of Use and waive any objections and defenses inconsistent with such venue. By using the Website and Service, you represent and warrant that your use complies with applicable law in your jurisdiction of residence.</p>
<p>&nbsp;</p>
<p><strong>XII. Entire Agreement and Assignment</strong></p>
<p>These Terms of Use, the Privacy Policy, and any other terms and conditions posted on the Website from time to time, constitute the entire agreement between you and the Company with respect to the use of the Website and the Service, superseding any prior agreements between you and the Company. You will not assign these Terms of Use or assign any rights or delegate any obligations hereunder, in whole or in part, whether voluntarily or by operation of law, without the prior written consent of the Company. The Company may assign these Terms of Use or any rights hereunder without your consent.</p>
<p>&nbsp;</p>
<p><strong>XIII. General</strong></p>
<p>Headings of the sections are for reference purposes only and will not be used to interpret or construe these Terms of Use. If any provision of these Terms of Use is held to be invalid, void or unenforceable, such provision will be stricken and the remaining provisions enforced. Notwithstanding any other provisions of these Terms of Use, any provision of these Terms of Use that imposes or contemplates continuing rights or obligations on you or us will survive the expiration or termination of these Terms of Use, including, without limitation, the indemnification and limitation of liability provisions. The Company’s failure to enforce your strict compliance with these Terms of Use does not constitute a waiver of any of its rights. Any waiver of any provision of these Terms of Use will be effective only if in writing and signed by the Company.</p>
<p style="text-align: right;"><em>Last Updated: January 25, 2018</em></p>                           
                            </label>
                            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
                        </div>
                    </div>
                </div>
                <div class="tab tabdisinerbox">
                    <div class="selectprofbox">                        
                        <h3>Select Profile</h3>

                        <div class="child_adultbox"> 
                            <label class="radiocust">Adult
                                <input type="radio" checked class="profile" name="applicationfor" id="adult" value="Adult">
                                <span class="radiomark"></span>
                            </label>
                            <label class="radiocust">Child
                                <input type="radio" class="profile" name="applicationfor" id="child"  value="Child">
                                <span class="radiomark"></span>
                            </label> 
                            <label class="radiocust">Group
                                <input type="radio" class="profile" name="applicationfor" id="group"  value="Group">
                                <span class="radiomark"></span>
                            </label>                            
                        </div>
                        <!-- <div class="child_adultbox">                        
                            <span>Adult</span>
                            <input type="radio" class="profile" name="profile" id="adult" value="adult">
                            <span>Child</span> 
                            <input type="radio" class="profile" name="profile" id="child"  value="child">
                        </div> -->
                        <!-- <input type="text" class="form-control validate" name="firstName" value="{{ old('firstName') }}" data-required="Please enter the firstname"> -->
                    
                        <div id="dounloadPDF" class="hidden">
                            <div class="pdfdowbox">
                                <p>Each Children/Youth Application must be accompanied by the following form signed by a Parent and/or Legal Guardian.</p>
                                <a href="{{ asset('assets/parental_guardian_consent_agreement.pdf') }}"  class="dwlBtnCss" name="" download>Download</a>
                                <p>Email this to info@freemancasting.com</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab tabdisinerbox">
                    <div class="detainerformall">
                        
                        <div id="groupOption" class="form-group hidden">
                            <label for="usr">Group Name</label>
                            <input type="text" class="form-control validate" name="" value="{{ old('group_name') }}"  id="GroupFocus"  data-required="Please enter the group">                            
                            <input type="hidden" class="form-control" name="group_id" value=""  id="GroupID" >                            
                        </div>

                        <div class="form-group">
                            <label for="usr">First Name</label>
                            <input type="text" class="form-control validate" name="firstName" value="{{ old('firstName') }}" data-required="Please enter the firstname">                            
                        </div>
                        <div class="form-group">
                            <label for="usr">Middle Name</label>
                            <input type="text" class="form-control " name="middleName" value="{{ old('middleName') }}" data-required="Please enter the middle name">                            
                        </div>
                        <div class="form-group">
                            <label for="usr">Last Name</label>
                            <input type="text" class="form-control validate" name="lastName" value="{{ old('lastName') }}" data-required="Please enter the last name">                            
                        </div>

                        <div class="form-group">
                            <label for="usr">Email</label>
                            <input type="text" class="form-control validate" name="email" id="email" data-required="Please enter the email" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="usr">Phone</label>
                            <input type="text" class="form-control validate" name="phonenumber" data-required="Please enter the phone number" data-mask="(000) 000-0000">
                        </div>
                        <div class="form-group">
                            <label >Password</label>
                            <input type="password" class="form-control validate" id="password" name="password" data-required="Please enter the password">
                        </div>
                        <div class="form-group">
                            <label >Confirm password</label>
                            <input type="password" class="form-control validate" id="confirm_password" name="confirm_password" data-required="Please enter the confirm password">
                        </div>                     
                    </div>
                </div>
                <div class="tab tabdisinerbox">
                    <div class="detainerformall">
                        <div class="form-group">
                            <label for="usr">Address</label>
                            <!-- <textarea  class="form-control validate" name="address" data-required="Please enter the address"></textarea> -->
                            <input type="text" class="form-control validate" name="address" data-required="Please enter the address">
                        </div> 
                        <div class="form-group">
                            <label for="usr">City</label>
                            <input type="text" class="form-control validate" name="city" value="{{ old('city') }}" data-required="Please enter the city">                            
                        </div>
                        <div class="form-group">
                            <label for="usr">Province</label>
                            <input type="text" class="form-control validate" name="province" value="{{ (old('province'))?old('province'): 'BC' }}" data-required="Please enter the province" data-mask="SS" >                            
                        </div>
                        <div class="form-group displayHideGroup">
                          <label for="usr">SIN</label>
                          <input type="text" name="sin" value="" data-required="Please enter the sin"  class="form-control validate" >
                        </div>
                        <div class="form-group">
                            <label for="usr">Postal Code</label>
                            <input type="text" class="form-control validate" name="postalCode" value="{{ old('postalCode') }}" data-required="Please enter the postalCode" data-mask="AAAAAAAAAAA">                            
                        </div>
                        <div class="form-group">
                            <label for="usr">Country</label>
                            <input type="text" class="form-control validate" name="country" value="{{ (old('country'))?old('country') : 'Canada'  }}" data-required="Please enter the country">                            
                        </div> 
                        <div class="form-group date">
                            <label for="usr">Date of Birth</label>
                            <input type="text" class="form-control validate DOBdatepicker" name="date_of_birth" data-required="Please select the date of birth"  autocomplete="off"  value="Saturday 02 August 2014">
                        </div>
                        <div class="form-group date hidden" id="parentGuardian">
                            <label for="usr">Parental Guardian Consent Agreement</label>
                            <input type="file"  accept="application/pdf" class="form-control validate " name="parent_aggrement" data-required="Please Upload Parental Guardian Consent Agreement" autocomplete="off">
                        </div>
                        <div class="form-group displayHideGroup">
                            <label>Gender</label>
                            <div class="seletcustboxpart2">
                                <select class="form-control validate" id="gendersignup" name="gender" data-required="Please select the gender">
                                    <option value=""></option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="transgender">Transgender</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Social media(Instagram)</label>
                            <div class="httpsboxinput itfllinkbox1">
                                <p>https://www.instagram.com/</p>
                                <input type="text" class="form-control " name="instagram" data-required="Please enter the instagram">
                            </div>
                        </div>
                        <div class="form-group itfllinkbox2">
                            <label>Social media(Twitter)</label>
                            <div class="httpsboxinput">
                                <p>https://twitter.com/</p>
                                <input type="text" class="form-control " name="twitter" data-required="Please enter the twitter">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Social media(Facebook)</label>
                            <div class="httpsboxinput itfllinkbox3">
                                <p>https://www.facebook.com/</p>
                                <input type="text" class="form-control " name="facebook" data-required="Please enter the facebook">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Social media(Linkedin)</label>
                            <div class="httpsboxinput itfllinkbox4">
                                <p>https://www.linkedin.com/</p>
                                <input type="text" class="form-control " name="linkedin" data-required="Please enter the linkedin">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>IMDB URL</label>
                            <div class="httpsboxinput">
                                <p>https://</p>
                                <input type="text" class="form-control " name="imdb_url" data-required="Please enter the IMDB URL">
                            </div>
                        </div>
                        <div id="memberOfgroup" class="form-group">
                            <label>Are you a member of a Group?</label>
                            <div class="seletcustboxpart2">
                            <select name="member_of_group" id="member_of_group">
                                <option></option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>                        
                            <input type="text" id="memberInput" class="form-control" name="" data-required="Please enter the group name">
                            <!-- <input type="hidden" id="memberGroupId" class="form-control" name="nameofgroup" data-required="Please enter the group name"> -->
                        </div>
                        
                    </div>
                </div>
                <div class="tab tabdisinerbox">
                    <div class="detainerformall">
                        <div class="form-group">
                            <label for="usr">Agent(Company and Contact)</label>
                              <div class="seletcustboxpart2">

                                <select class="form-control validate" id="agentselect" name="agent_id" data-required="Please enter Agent name">
                                  <option></option>
                                  @foreach($agent as $agent) 
                                  <option value="{{$agent->id}}"  >{{$agent->agent_name}}</option>



                                  @endforeach
                              </select>
                            </div>
                            <!-- <input type="text" class="form-control validate" name="agent" data-required="Please enter Agent(Company and Contact)" id="agent">
                            <input type="hidden" class="form-control validate" name="agent_id" data-required="Please enter Agent(Company and Contact)" id="agent_id"> -->
                        </div>
                        <div class="form-group" >
                            <label for="usr">Union Name</label>
                           
                            <div class="seletcustboxpart2">

                              <select class="form-control validate" id="union_name" name="union_name" data-required="Please enter union name">
                                <option></option>
                                @foreach($PerformerUnion as $union) 
                                <option value="{{$union->id}}">{{$union->union}}</option>
                                @endforeach
                            </select>
                        </div>
                        </div>
                        <div class="form-group" id="hideUnionNumber">
                            <label for="usr">Union Number</label>
                            <input type="text" class="form-control validate" name="union_number" id="union_number" data-required="Please enter union number">
                        </div>
                        <div class="form-group">
                            <label for="usr">Availability</label>
                            <div class="seletcustboxpart2">
                            <select class="form-control validate" id="availability" name="availability" data-required="Please Select Availability">
                                    <option value=""></option>
                                   @foreach($PerformerAvailability as $availability) 
                                    <option value="{{$availability->id}}">{{$availability->availability}}</option>
                                     @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab tabdisinerbox droboxdisset">
                    <div class="detainerformall">
                        <!-- <input type="file" class="form-control" name="upload_primary_url"> -->
                        <div method="post" action="{{route('performers.imageUpload')}}" enctype="multipart/form-data" class="dropzone" id="dropzone"> 
                                 
                            </div>
                        <input type="hidden" name="upload_primary_url" id="upload_primary_url">
                    </div>
                </div>
                <input type="hidden" id="step_form">
                <div class="btn-control nextpribtnbox">
                    <!-- <div class="btn-control-internal"></div> -->
                    <button type="button" class="prevbtn">Previous</button>
                    <button type="button" class="nextbtn visible">Next</button>
                    <button type="submit" class="cadastrobtn">Submit</button>                   
                </div>
                <a class="btn btn-link signpboxlink" href="{{ route('performers.login') }}">Sign In</a>

            </div> 
        </div> 
    </form>
</section>

<div class="modal fade dreatgroupbox" id="AddingGroup">
    <div class="modal-dialog">
        <div class="modal-content">  
            <h4>This group does not exist</h4>              
            <h4>Are you  want to create a group profile ?</h4>
            <button type="button"  id="isGroupYes" class="btn btn-info">Yes</button>
            <button type="button" id="isGroupNo" class="btn btn-danger">No</button>            
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>

   $(document).ready(function () {
 
    $(".profile").click(function(){ 

        if($(this).val() == "Child")
        {
            $("#dounloadPDF").removeClass('hidden');
            $("#parentGuardian").removeClass('hidden');
            $("#groupOption").addClass('hidden');
            $("#memberOfgroup").removeClass('hidden');
            $(".displayHideGroup").show();
        }
        if($(this).val() == "Adult")
        {
           $("#dounloadPDF").addClass('hidden');
           $("#parentGuardian").addClass('hidden');
           $("#groupOption").addClass('hidden');
           $("#memberOfgroup").removeClass('hidden');
           $(".displayHideGroup").show();
       }
       if($(this).val() == "Group")
       {
           $("#dounloadPDF").addClass('hidden');
           $("#parentGuardian").addClass('hidden');
           $("#groupOption").removeClass('hidden');
           $("#memberOfgroup").addClass('hidden');
           $(".displayHideGroup").hide();


       }


    });



       

    });
    // member or not
        $('select').on('change', function (e) {
                var optionSelected = $("option:selected", this);
                var valueSelected = this.value;
                

                if(valueSelected == "Yes")
                {   
                    $("#memberInput").removeClass("hidden");
                }
                if(valueSelected == "No")
                {   
                    $("#memberInput").addClass("hidden");
                }
    
        });

         $('#union_name').on('change', function (e) {
                var optionSelected = $("option:selected", this);
                var SelectedUnion = this.value;

                if(optionSelected.html() === "NON-UNION"){
                    $("#hideUnionNumber").hide();
                }else{
                    $("#hideUnionNumber").show();
                }
      }); 
        
</script>

<script>
    //CHECK BOX
    $('#iAgree').click(function(){
         if($(this).prop("checked") == true){
            $('.nextbtn').attr("disabled", false);
        } else if($(this).prop("checked") == false){
            $('.nextbtn').attr("disabled", true);
        }
    });


    //GENDER SELECT2
    $('#gender').select2({
      minimumResultsForSearch: -1
    });
    $('#union_name').select2({
      placeholder: "Please Select union"
    });
    $('#agentselect').select2({
       placeholder: "Please Select Agent" 
    })

    //EMAIL CHECKED
    // $("#email").focusout(function() {
    //     var email = $(this).val();
    //     $.ajax({
    //         type: "GET",
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         url: '{{ route("performers.checkedEmail") }}',
    //         data: {email : email},
    //         success: function(response)
    //         {
    //             if (response.status == true) {
    //                 //toastr.success(response.message);
    //                 $('.nextbtn').attr("disabled", false);
    //             } else {
    //                 $('.nextbtn').attr("disabled", true);
    //                 toastr.warning(response.message);
    //             }
    //         }
    //     });
    // });

    //UNION NUMBER CHECKED
    $("#union_number").focusout(function() {
        var union_number = $(this).val();
        $.ajax({
            type: "GET",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ route("performers.checkedUnionNumber") }}',
            data: {union_number : union_number},
            success: function(response)
            {
                if (response.status == true) {
                    //toastr.success(response.message);
                    $('.nextbtn').attr("disabled", false);
                } else {
                    toastr.warning(response.message);
                    $('.nextbtn').attr("disabled", true);
                }
            }
        });
    });


    //Register
    $("#regForm").on('submit',function(e){
        e.preventDefault();
        jQuery('.cadastrobtn').attr('disabled',true);
        var formData = new FormData(this);
        $.ajax({
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:'{{ route("performers.signup") }}',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            beforeSend:function(){},
            success:function(response) {
                if (response.status == true) {
                    toastr.success(response.message);
                    window.location.href = '{{ route("performers.portfolio")}}';
                } else {
                    toastr.warning(response.message);
                }
            },
            complete:function(){
$('.cadastrobtn').attr("disabled", false);    
$('.cadastrobtn').removeAttr("disabled");
            },
            error:function(){

$('.cadastrobtn').attr("disabled", false);    
$('.cadastrobtn').removeAttr("disabled");

            },
        
        });
    });



    var currentTab = 0;

    var tab = document.querySelectorAll(".tab");
    var step = document.querySelectorAll(".step");

    var nextbtn = document.querySelector(".nextbtn");
    var prevbtn = document.querySelector(".prevbtn");
    var cadastrobtn = document.querySelector(".cadastrobtn");

    function next() {
        nextbtn.addEventListener("click", function() {
            var total_error = 0 ;

            if($('#regForm').find('.validate:visible').length>0){
                $('#regForm .error').remove();
                $('#regForm').find('.validate:visible').each(function(){

                    if($(this).is('[data-required]') && $(this).attr('data-required')!="" && $(this).val()==""){
                        total_error++;
                        $(this).after('<span class="error">'+$(this).data('required')+'</span>');
                        
                    } else {
                        $(this).removeClass('error');
                    }
                    

                })
                var password = $('#password').val();
                var confirm_password = $('#confirm_password').val();
                if(password!="" && confirm_password!="" && password!=confirm_password){
                    $('#confirm_password').after('<span class="error">Password and confirm password does not match</span>');
                    total_error++;
                }
                if (total_error == 0) {
                    nextSuccess();
                }
            } else {
                nextSuccess();
                
            }
        });
    }

    function nextSuccess(){
        if (currentTab <= 5) {
            currentTab = currentTab + 1;

            $(".tab").each(function() {
                if ($(this).hasClass("current")) {
                    $(this).removeClass("current");
                }
                tab[currentTab].classList.add("current");
            });

            $(".step").each(function() {
                if ($(this).hasClass("active")) {
                    $(this).removeClass("active");
                }
                step[currentTab].classList.add("active");
            });

            $(".input").each(function() {
                if ($(this).hasClass("input-current")) {
                    $(this).removeClass("input-current");
                }
                tab[currentTab].classList.add("current");
            });
            verifyPosition();
        }
    }

  function prev() {
      prevbtn.addEventListener("click", function() {
        if (currentTab > 0) {
          currentTab = currentTab - 1;
          $(".tab").each(function() {
            if ($(this).hasClass("current")) {
              $(this).removeClass("current");
          }
          tab[currentTab].classList.add("current");
      });

          $(".step").each(function() {
            if ($(this).hasClass("active")) {
              $(this).removeClass("active");
          }
          step[currentTab].classList.add("active");
      });
          verifyPosition();
      }
  });
  }

  function verifyPosition() {
      console.log(currentTab);
      if (currentTab > 0) {
        prevbtn.classList.add("visible");
    } else {
        prevbtn.classList.remove("visible");
    }

    if (currentTab == 5) {
        nextbtn.classList.remove("visible");
        cadastrobtn.classList.add("visible");
    } else {
        nextbtn.classList.add("visible");
        cadastrobtn.classList.remove("visible");
    }
}
if($('#step_form').val() != '2'){
    next();
}
prev();

</script>
<script>
    // Dropzone.autoDiscover = false;
    // var myDropzone = new Dropzone("#dropzone", { url: "/file/post",dictDefaultMessage: "Drop your primary image to upload"});
</script>

<script>
     $(document).ready(function(){
        src = "{{route('performers.agentlist')}}";
    $( "#agent" ).autocomplete({
    source: function(request, response) {
            $.ajaxSetup({
                 headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
               });
            $.ajax({                
                type: 'POST',
                url: src,
                data: {
                    term : $("#agent").val()
                },
                success: function(data) {
                     if (!data.length) {
                    var result = [{ label: "no results", value: response.term }];
                    response(result);
                }
                    response(data);

                },
                
            });
        },
        minLength: 1,
        select: function(event, ui) {
        var label = ui.item.label;
                    if (label === "Add Agent") {
                        event.preventDefault();
                        // alert($("#agent").val());
                        $.ajaxSetup({
                             headers: {
                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                             }
                           });
                        $.ajax({
                            
                            type: 'POST',
                            url: "{{route('performers.addinagentlist')}}",
                            data: {
                                 data : $("#agent").val()
                            },
                            success: function(data) {

                              $('#agent_id').val(data);
                              toastr.success("Agent Added Successfully");

                            },
                            
                        })

                }
       else{         
      $('#agent_id').val(ui.item.id);
        }
    },
    change: function (event, ui) {
        if (ui.item == null){ 
         //here is null if entered value is not match in suggestion list
         //  toastr.warning("Select Agent");
            $(this).val((ui.item ? ui.item.id : ""));
        }
    },
    response: function(event, ui) {
        if (!ui.content.length) {

             var noResult = { value:"",label:"No results found" };
         //   var newitem = { value:"test",label:"Add Agent"}
             ui.content.push(noResult);
            //ui.content.push(newitem);

        }
      }  
     });

        });


$(document).ready(function(){
//Grouplist and select

     
    $( "#memberInput" ).autocomplete({
    source: function(request, response) {
            $.ajaxSetup({
                 headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
               });
            $.ajax({                
                type: 'POST',
                url: "{{route('performers.groupList')}}",
                data: {
                    term : $("#memberInput").val()
                },
                success: function(data) {
                     if (!data.length) {
                    var result = [{ label: "no results", value: response.term }];
                    response(result);
                }
                    response(data);

                },
                
            });
        },
        minLength: 1,
        select: function(event, ui) {
        var label = ui.item.label;                   
                    $('#GroupID').val(ui.item.id);
       
        if(label === "This group does not exist"){
            $("#ModalGroupName").val($("#memberInput").val())
            $("#AddingGroup").modal("show");
        }
    },
    response: function(event, ui) {
        if (!ui.content.length) {

             $("#AddingGroup").modal("show");
             var noResult = { value:"",label:"This group does not exist" };         
             ui.content.push(noResult);
          
        }
      }  
     });


});


$("#isGroupYes").click(function(){  
    location.reload();
});
    
$("#isGroupNo").click(function(){
    $("#AddingGroup").modal("hide");
    $("#memberInput").val("");
    $("#memberInput").hide();

    $("#member_of_group option[value='no']").prop('selected',true);
});




    var uploadedDocumentMap = {};
    Dropzone.options.dropzone =
    {
        maxFiles: 1,
        accept: function(file, done) {
            console.log("uploaded");
            done();
        },
        init: function() {
            this.hiddenFileInput.removeAttribute('multiple');

            this.on("maxfilesexceeded", function(file){
                toastr.error("Only a single photo allowed");
            });


        },  
        renameFile: function(file) {
            var dt    = new Date();
            var time  = dt.getTime();
            return time+file.name;
        },
        type:"post",
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        addRemoveLinks: true,
        timeout: 5000,
        dictDefaultMessage: "Drop profile picture to upload",
        success: function(file , response) 
        {            
            if (response.status == true) {
                $('#upload_primary_url').val(response.image);
            }           
        },
        error: function(response)
        {
            console.log(response);
            return false;
        }
    };

 jQuery('#gendersignup').select2({
   placeholder:'Select Gender',
 });
 jQuery('#availability').select2({
   placeholder:'Select Availability',
 });
jQuery('#member_of_group').select2({
  placeholder:'want group members',
 });
$("#member_of_group").change(function(){
     member=$(this).val();
    if(member=="yes"){
        $("#memberInput").show();
    }
    else{
        $("#memberInput").hide();
    }
});



$("#GroupFocus").focusout(function(){
    

    var group_name =$(this).val();
    
    if(group_name == "")
    {
        toastr.error("Enter Group Name");
        return false;
    }
    
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({

        url:"{{route('performers.addGroup')}}",
        type:"POST",
        data:{group_name:group_name},
        success:function(data){
            if(data.status == true)
            {     
                $("#GroupID").val(data.data);           
                toastr.success(data.message);
            }if(data.status == false)
            {
                $("#GroupID").val('');
                $("#GroupFocus").val('');
                toastr.error(data.message);
            }
        }
    });
    
})
</script>
@endsection
