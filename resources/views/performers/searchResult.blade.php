@if(count($search_result))
 @foreach($search_result as $rentalValue)
    @if($rentalValue->is_video == "no")
    <div class="hedfullmotobox">
      <div class="allboxtitlesem">
        <div class="titleboxallcover">
          <div class="titledivallbox"><h3> <a href="javascript:void(0)"  onclick="viewGallery('{{ $rentalValue['id'] }}','{{ $rentalValue['rental_title'] }}')" >{{$rentalValue['rental_title'] }}</a></h3></div>
          <div class="helpboxdcl">
            <span class="tool" data-tip="{{isset($helpdesk->props_help) ? $helpdesk->props_help : ''}}" tabindex="1"><i class="fal fa-question-circle"></i></span>
          </div>
          <div class="morbtnmnubox">
            <div class="morebtneddil">                
              <div class="MenuSceinain">
                <div class="menuheaderinmore">
                  @if(Auth::guard('web')->check())
                  <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                    
                  <div class="mainineropenbox_1">
                    <div class="mainineropenbox_2">
                      <div class="opensubmenua_1">
                        <a href="javascript:void(0)" id="{{$rentalValue->id}}"  class=""  onclick="imageAddTogallery('{{$rentalValue->id}}')">Add</a>
                      </div> 
                      <div class="opensubmenua_1">
                        <a href="javascript:void(0)"  onclick="viewGallery('{{ $rentalValue['id'] }}','{{ $rentalValue['rental_title'] }}')" >Edit</a>
                      </div>
                    </div> 
                  </div>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="imgvidaodbox">
            <div id="rentaltag{{$rentalValue->id}}">
              @if(count($rentalValue->rentaltag))
              @foreach($rentalValue->rentaltag as $tagVal)
              <span class="btnremadd" id="TagRemove{{$tagVal->id}}">
                {{$tagVal->tag}} 
               <a href="javascript:void(0)" class="DeleteTag{{$tagVal->id }} imagetagdeleteconfirm"  data-toggle="confirmation" data-title="are you sure?" data-imagetagid="{{$tagVal->id }}"> <i class="fal fa-times"></i></a>
              </span>
              @endforeach
              @endif
            </div>
            <span class="AddTag" onclick="addTag('{{$rentalValue->id}}')" id="{{$rentalValue->id}}">Add Tag <i class="fal fa-plus"></i></span> 
            <span id="tagText_{{$rentalValue->id}}" class="hidden tagtextltbox">
              <input type="" data-attid="{{$rentalValue->id}}"  name="tag" id="tagval_{{$rentalValue->id}}" class="tagstore{{$rentalValue->id}} tagSave">
            </span>
          </div>
        </div>
      </div>  
      <div class="gelimgleftbox">        
        <ul class="gelimgleftboxGallery">
          @if(count($rentalValue['rentalmedia']))       
          @foreach($rentalValue['rentalmedia'] as $key=>$mediaValue)         
          @if($key <=6)
          <li id="rental{{$mediaValue->id}}">
            @if(Auth::guard('web')->check())
            <span class="deleteimageconfirm deleteimgbox" data-toggle="confirmation" data-title="are you sure?" data-imageid="{{$mediaValue->id}}"><i class="fas fa-trash"></i></span>
            @endif
            <a href="{{$mediaValue->image_url}}"> <img src="{{$mediaValue->image_url}}" alt=""></a>
          </li> 
          @endif
          @endforeach
          @else
          <div class="gelimgleftbox">        
            <p>No Image</p>
          </div>
          @endif
            </ul>
      @if(count($rentalValue['rentalmedia']) > 7)    
      <div class="viewgallbox">
        <a href="{{route('performers.view.gallery',['id'=>$rentalValue->id])}}">View<br>Gallery<br>
        <span>{{count($rentalValue['rentalmedia'])}} Images</span></a>   
      </div>
      @endif
     
      </div>
    </div>
    @endif
    @endforeach       
@endif
