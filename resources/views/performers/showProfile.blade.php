@if(isset($performer->upload_primary_url))            
<div class="addphotosetbox">
 <img src="{{$performer->image_url}}" alt="">             
 <a href="javascript:void(0)" id="deleteprofile" data-id="{{$performer->id}}" data-toggle="confirmation" data-title="are you sure?" class="confirmdeleteprofile"><i class="fas fa-trash"></i></a>
</div>
@else
<div class="uploadimgboxprof" id="profiledropzone" title="Dropzone">
 <div class="dropboxzoniner">
   <div id="Image_upload_dropzone">
     <div method="post" action="{{ route('performers.storeprofile') }}" enctype="multipart/form-data" 
     class="dropzone" id="AddProfiledropzone">          
   </div>
 </div>
</div>
</div>
@endif