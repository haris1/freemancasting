@extends('performers.layouts.app')
@section('content')

<div class="wctleftrightbox">
 @if($performer->applicationfor == "Group")
  @include('performers.layouts.groupSidebar')  
  @else
  @include('performers.layouts.sidebar')  
  @endif

   @if($authtype=='performer')     
  <input type="hidden" value="{{request()->route('id')}}" id="get_id" name="">
  @else
  <input type="hidden" value="{{request()->route('id')}}" id="get_id" name="">  
  @endif
  <div class="rightallditinwct">
  <!--   <div class="searchbarboxfilt">
      <div class="srchbox">        
        <div class="form-group">
          <i class="far fa-search"></i>
          <input type="text" class="form-control" id="" placeholder="Search">
        </div>
      </div>
      <div class="addandtagbox">
        <div class="addatribut">
          <a href="#" type="button" data-toggle="modal" data-target="#myModal"><i class="far fa-plus"></i> Add New Resume</a>
        </div>
        <div class="tagboxright">
          <a href="javascript:void(0)"><i class="far fa-tag"></i></a>
        </div>
      </div>
    </div> -->
    <div class="addflagshidbox">
      <div class="accordion-container">
        <div class="set hedtobox">
          <div class="srchbox">        
            <div class="form-group skill_resume_Search_hide">
              <i class="far fa-search"></i>
             <input type="text" class="form-control" id="resumeSearchBox" placeholder="Search">
            </div>
          </div>
      @if($authtype=='performer')     
     @else
     @include('performers.flag')
     @endif      
        </div>      
      </div>
    </div>
    <div class="topbarbotdivcover">
@if($authtype=='performer')
     @include('performers.layouts.tabbar')
     @else
     @include('performers.layouts.internal-tabbar')
     @endif
    <div class="resumeboxnewdesbox" id="ResumeResult">
    @forelse($Workperformer as $work)    
      <div class="acopartboxne1" id="{{$work->id}}">
        <div class="accordion-container">
          <div class="set hedtobox">
            <div class="acodhedpart{{(count($work->project_performer) ?' active' :'')}}">
                <div class="titledivallbox">
                  <h3>{{$work->resume_title}}</h3>
                  <div class="helpboxdcl">
                    <span class="tool" data-tip="{{$helpdesk->resume_help}}" tabindex="1"><i class="fal fa-question-circle"></i></span>
                  </div>
                  <div class="morbtnmnubox">
                    <div class="morebtneddil">                
                      <div class="MenuSceinain">
                        <div class="menuheaderinmore">
                          <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
                          <div class="mainineropenbox_1">
                            <div class="mainineropenbox_2">
                              <div class="opensubmenua_1">
                                <a href="javascript:void(0)" data-toggle="modal" data-id="{{ $work->id }}" class="editModel">Edit</a>
                              </div>

                              @if (!in_array($work->resume_title, array('Background Work','Speaking Roles','Commercial Roles'))) 
                              <div class="opensubmenua_1">
                                <a href="javascript:void(0)"  data-id="{{ $work->id }}" data-toggle="confirmation" data-title="Are you sure?" class="deleteresume">Delete</a>
                              </div>
                              @endif
                            </div> 
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>              

              <a href="javascript:void(0)">
                <span class="moretext">Show section</span>
                <span class="lesstext">Hide section</span>                 
                <i class="far fa-angle-down"></i>
              </a>
            </div>

            <div class="content" style="display{{(count($work->project_performer) ? ':block;' : '' )}}">
              <?php $i=1;?>
                 @forelse($work->project_performer as $project)
                @if($i < 6)          
              <div class="inprojanmembox" id="removeProject{{$project->id}}">
                <div class="inprojanmembox_iner1">
                  <p><a href="javascript:void(0)" type="button"  id="{{$project->id}}" class="projectdata"> {{ $project->project_title }}</a></p>
                </div>
                <div class="inprojanmembox_iner2">
                  <p> <a href="javascript:void(0)" type="button" id="{{$project->id}}" class="projectdata"> {{ $project->project_role->count() }} Roles</a></p>
                </div>
                <div class="inprojanmembox_iner3">
                  <?php
                  $startdate=array();
                  $enddate=array();
                  foreach($project->project_role as $role){
                    $startdate[] = $role->start_date;
                    $enddate[]   = $role->end_date;
                  } ?>
                  @if(!empty($startdate))
                  <p>{{date('d M Y',strtotime(min($startdate))) }} - {{date('d M Y',strtotime(max($enddate))) }}</p>
                  @endif
                </div>
                <div class="inprojanmembox_iner4">
                  <a href="{{ $project->IMDC_link }}" target="_blank">IMDB <i class="far fa-long-arrow-alt-right"></i></a>
                </div>

                <div class="inprojanmembox_iner5">
                <a href="javascript:void(0)" data-toggle="confirmation" data-title="are you sure?" data-resumeid="{{$project->id}}"  class="deleteresumeconfirm"><i class="fas fa-trash"></i></a>
              </div>

              </div>             
              <?php $i++; ?>
              @else
              <div class="showMoreResume{{$work->id}}" style="display:none">
                <div class="inprojanmembox" id="removeProject{{$project->id}}">
                <div class="inprojanmembox_iner1">
                  <h3>{{ $project->project_title }}</h3>
                </div>
                <div class="inprojanmembox_iner2">
                  <p> <a href="javascript:void(0)" type="button"  id="{{$project->id}}" class="projectdata"> {{ $project->project_role->count() }} Roles</a></p>
                </div>
                <div class="inprojanmembox_iner3">
                  <?php
                  $startdate=array();
                  $enddate=array();
                  foreach($project->project_role as $role){
                    $startdate[] = $role->start_date;
                    $enddate[]   = $role->end_date;
                  } ?>
                  @if(!empty($startdate))
                  <p>{{date('d M Y',strtotime(min($startdate))) }} - {{date('d M Y',strtotime(max($enddate))) }}</p>
                  @endif
                </div>
                <div class="inprojanmembox_iner4">
                  <a href="{{ $project->IMDC_link }}" target="_blank">IMDB <i class="far fa-long-arrow-alt-right"></i></a>
                </div>
                <div class="inprojanmembox_iner5">
                <a href="javascript:void(0)" data-toggle="confirmation" data-title="are you sure?" data-resumeid="{{$project->id}}"  class="deleteresumeconfirm"><i class="fas fa-trash"></i></a>
              </div>
              </div>             
              </div>
             @endif
              @empty
              <!-- <div class="">No Project Available</div> -->             
              <div class="noprojAvailable">
               <p>No Project Available</p>
              </div>
              
              @endforelse

              @if($work->project_performer->count()>5 || $i==5)
              <div class="showmorebtnbox">
                <a href="javascript:void(0)" onclick="showMore('{{$work->id}}')"><span id="changtxt{{$work->id}}">Show more</span>
                  <i id="changarrDoun{{$work->id}}" class="far fa-angle-down"></i>
                  <i id="changarrUp{{$work->id}}" class="far fa-angle-up hidden"></i>
                </a>
              </div>
              @endif

            </div>
          </div>
        </div>

      </div>
    
      @empty
      <div class="hedfullmotobox">
        <div class="noprojAvailable">     
          <p>No Resume available</p>
        </div>
      </div>
      @endforelse
     
      </div>
      <div class="addbtnbotbox">
    <button  data-toggle="modal" data-target="#myModal"><i class="far fa-plus"></i> Add New Resume</button>
  </div>
  </div>
  
  </div>
  <div class="modal fade editditboxadd" id="myModal" role="dialog">
    <div class="modal-dialog">    
      <div class="modal-content">        
        <div class="modal-body">
          <div class="editboxinerditsbox">

            <input type="hidden" name="projectindex" id="projectIndex" value="0">
            <form id="resumeform">
                <div class="totitleaddris">
              
              <h4>Add Resume</h4>
              <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <input type="hidden" id="workid" class="workid">
            <div class="allditboxeditcover" >
             
              <div class="titleboxeditbox">
                <div class="form-group">
                  <input type="text" class="form-control ofo worktitle" id="worktitle" name="worktitle" placeholder="Work Title" data-workid='0'>
                </div>
                <a href="javascript:void(0)" class="projectplus"><i class="far fa-plus"></i></a>
              </div>
              <div class="project_box">
                  
              </div>
              <div class="pjtimrostendbox" id="projectsection">
                <input type="hidden" name="roleprojectIndex" id="roleprojectIndex0" value="1">              
                <input type="hidden" id="projectid0" class="projectid">

                <div class="pjtimroendbox1">
                  <div class="form-group inputbox1">

                    <input type="text" class="form-control ofo projecttitle" id="projecttitle0" placeholder="Project Title" name="projecttitle" data-projectid='0'>
                  </div>
                  <div class="form-group inputbox2">
                    <div class="inerinputhttps">
                      <p>https://</p>
                      <input type="text" class="form-control ofo imdclink" id="imdclink0" placeholder="IMDC link" name="imdclink" data-projectid="0" >
                    </div>
                    
                  </div>
                </div>
                <div class="pjtimroendbox2" id="rolesection0">
                  <div class="form-group inputbox3">
                    <input type="text" class="form-control roletitle" id="roletitle01" placeholder="Role Title" name="roletitle0">
                  </div>
                  <div class="form-group date inputbox4">
                    <input type="text" class="form-control validate datepicker startdate" name="startdate0" id="startdate01"data-required="Please select the date of birth" autocomplete="off" placeholder="Start Date" data-id="01" readonly>
                  </div>
                  <div class="form-group date inputbox5">
                    <input type="text" class="form-control validate datepicker enddate" id="enddate01" name="enddate01" data-required="Please select the date of birth" autocomplete="off" placeholder="End Date" readonly>
                  </div>
                  <div class="inputbox6">
                    <a class="save" id="0" onclick="saverole(this,'0','1');"><i class="far fa-save"></i></a>
                    <a class="roleplus" id="0"  onclick="addrolesection(this,'0');"><i class="far fa-plus" ></i></a>

                  </div>
                </div>
              </div>

            </div>
            </form>
          </div>
        </div>
      </div>      
    </div>

  </div>

    <!-- EDIT MODEL -->
    <div class="modal fade editditboxadd" id="editModel" role="dialog">
        <div class="modal-dialog">    
            <div class="modal-content">        
                <div class="modal-body">
                    <div class="editboxinerditsbox">
                       <form id="editResumeform">
                            
                        </form> 
                    </div>
                </div>
            </div>      
        </div>
    </div>

    <div class="modal fade editditboxadd" id="myModal3" role="dialog">
        <div class="modal-dialog">    
            <div class="modal-content">        
                <div class="modal-body">
                    <div class="editboxinerditsbox">
                        <div class="totitleaddris">
                            <h4>Add Resume</h4>
                            <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times" onClick="window.location.reload();"></i></button>
                        </div>
                        <div class="allditboxeditcover" id="resumedata">
                        </div>  
                    </div>
                </div>
            </div>      
        </div>
    </div>


@endsection

@section('scripts')

<script>
    $(document).ready(function() {
  $(".set > .acodhedpart ").on("click", function() {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this)
        .siblings(".content")
        .slideUp(200);
      $(".set > .acodhedpart  i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
    } else {
      $(".set > .acodhedpart  i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
      $(this)
        .find("i")
        .removeClass("fa-plus")
        .addClass("fa-minus");
      $(".set > .acodhedpart ").removeClass("active");
      $(this).addClass("active");
      $(".content").slideUp(200);
      $(this)
        .siblings(".content")
        .slideDown(200);
    }
  });
});
</script>
<script type="text/javascript">
var end="";
  //   $(".datepicker").datepicker({
  //       dateFormat: "dd-mm-yy",
  //       autoclose: true,
  //   }).on("change", function(date) {
  //   var start=$(this).val();
  //    end="enddate"+$(this).data('id');
  //    end="#"+end;
  //    $(end).datepicker({
  //     minDate:start,
  //    });
  
  // });

 datepicker('.startdate','.enddate');

    
function datepicker(startdate,enddate){

 $(startdate).datepicker({
  format: 'dd-mm-yyyy',
  autoclose: true,
}).on('changeDate', function (selected) {
    var startDate = new Date(selected.date.valueOf());
    $('.enddate').datepicker('setStartDate', startDate);
}).on('clearDate', function (selected) {
    $('.enddate').datepicker('setStartDate', null);
});

$(enddate).datepicker({
   format: 'dd-mm-yyyy',
   autoclose: true,
}).on('changeDate', function (selected) {
   var endDate = new Date(selected.date.valueOf());
   $('.startdate').datepicker('setEndDate', endDate);
}).on('clearDate', function (selected) {
   $('.startdate').datepicker('setEndDate', null);
});
}


    
    function saverole(element,projectIndex,RoleIndex){ 
      
        var project_id = $('#projectid'+projectIndex).val();
      
        //var project_id = $(this).parents('.pjtimrostendbox').find('.projectid').val();
        var roletitle = $('#roletitle'+projectIndex+''+RoleIndex).val();
        // var startdate = $(this).parents('.pjtimroendbox2').find('.startdate').val();
        // var enddate = $(this).parents('.pjtimroendbox2').find('.enddate').val();
         var startdate=$("#startdate"+projectIndex+""+RoleIndex).val();
         var enddate=$("#enddate"+projectIndex+""+RoleIndex).val();
        console.log(project_id);
        $.ajax({
            type: "GET",
            url: "{{route('performers.addrole')}}",
            data:{
                role_title:roletitle,
                start_date:startdate,
                end_date:enddate,      
                project_id:project_id,
            },
            success: function(data){
                if (data.status == true) {
                    toastr.success(data.message);
                } else {
                    toastr.warning(data.message);
                }
            }
        }); 
  }


    $('body').delegate(".roleSaveEdit",'click',function(){

        var project_id = $(this).parents('.pjtimrostendbox').find('.projectid').val();

        var roletitle = $(this).parents('.pjtimroendbox2').find('.roletitle').val();
        var roleid = $(this).parents('.pjtimroendbox2').find('.roletitle').data('roleid');
        var delectclass = jQuery(this).data('delectclass');

        var id = $('#'+roleid).val();
        var startdate = $(this).parents('.pjtimroendbox2').find('.startdate').val();
        var enddate = $(this).parents('.pjtimroendbox2').find('.enddate').val();
        var roleId = id;
        
        $.ajax({
            type: "GET",
            url: "{{route('performers.updateRole')}}",
            data:{
                role_title:roletitle,
                project_id:project_id,
                start_date:startdate,
                end_date:enddate,      
                roleId:roleId        
            },
            success: function(data){
                if (data.status == true) {
                
                  $('#'+roleid).val(data.id);
                   $('#'+delectclass).attr('data-id',data.id);
                   $('#'+delectclass).addClass('roleDelete');
                   $('#'+delectclass).removeClass('roleminus');
                    toastr.success(data.message);
                } else {
                    toastr.warning(data.message);
                }
            }
        });
    })

  function addrolesection(element,ids){
    var indexRoleId = parseInt(jQuery('#roleprojectIndex'+ids).val()) + parseInt(1);
    var i=ids+""+indexRoleId;
    jQuery('#roleprojectIndex'+ids).val(indexRoleId);
     $('#rolesection'+ids).prepend('<div class="pjtimroendbox2 rolesection"><div class="form-group inputbox3 rolesection" ><input type="text" class="form-control roletitle" id="roletitle'+i+'" placeholder="Role Title" name="roletitle'+i+'"></div><div class="form-group date inputbox4"><input type="text" class="form-control validate datepicker startdate" name="startdate'+i+'" id="startdate'+i+'"data-required="Please select the date of birth" autocomplete="off" placeholder="Start Date" readonly></div><div class="form-group date inputbox5"><input type="text" class="form-control validate datepicker enddate" name="enddate'+i+'" id="enddate'+i+'" data-required="Please select the date of birth" autocomplete="off" placeholder="End Date" readonly></div><div class="inputbox6"><a data-id="'+i+'" onclick="saverole(this,'+ids+','+indexRoleId+');"><i class="far fa-save"></i></a><a class="roleminus"><i class="far fa-minus"></i></a></div></div>');
      setTimeout(function(){
        datepicker('#startdate'+i,'#enddate'+i);//$('.datepicker').datepicker();
   },100);
 
  }

 
    $('body').delegate(".roleplusedit",'click',function(){
    var ids= $(this).data('id');     
   // alert(ids);
     //$(this).attr('data-id',parseInt(ids)+parseInt(1));     
    var indexRoleId = parseInt(jQuery('#roleprojectIndexEdit'+ids).val()) + parseInt(1);
    console.log(indexRoleId);
    var i=ids+""+indexRoleId;

    jQuery('#roleprojectIndexEdit'+ids).val(indexRoleId);
     $(this).parents('.pjtimroendbox2').before('<div class="pjtimroendbox2 rolesectionEdit" id="rolesectionEdit'+i+'"><div class="form-group inputbox3 rolesectionEdit" > <input type="hidden" name="roleidGet" id="roleidGetAp'+i+'" value=""><input type="text" class="form-control roletitle" id="roletitle'+i+'"   data-roleid="roleidGetAp'+i+'" placeholder="Role Title" name="roletitle'+i+'"></div><div class="form-group date inputbox4"><input type="text" class="form-control validate datepicker startdate" name="startdate'+i+'" id="startdate'+i+'"data-required="Please select the date of birth" autocomplete="off" placeholder="Start Date" readonly></div><div class="form-group date inputbox5"><input type="text" class="form-control validate datepicker enddate" name="enddate'+i+'" id="enddate'+i+'" data-required="Please select the date of birth" autocomplete="off" placeholder="End Date" readonly></div><div class="inputbox6"><a data-id="'+i+'" data-delectclass="adddeleteId'+i+'" class="save roleSaveEdit" data-roleid="0"><i class="far fa-save"></i></a><a class="roleminus" id="adddeleteId'+i+'" ><i class="far fa-minus"></i></a></div></div>');
      setTimeout(function(){
        datepicker('#startdate'+i,'#enddate'+i);
   },100);
 
  })
 // $('body').on( "click", ".save", function() {
 //          alert($(this).attr("id"));
 //      });
 

 $(".projectplus").click(function(){
  var projindex = jQuery('#projectIndex').val();
  
  var k = parseInt(projindex) + parseInt(1);
  jQuery('#projectIndex').val(k);
  var i=k+'1'; 
  console.log(k)
  console.log(i);
  $(".project_box").prepend('<div class="projectsection"><div class="titleboxeditbox"><a href="javascript:void(0)" class="projectminus" data-removeid="removeProject'+k+'" data-key="'+k+'"><i class="fa fa-minus"></i></a></div><div class="pjtimrostendbox"><div class="pjtimroendbox'+k+'"><input type="hidden" class="projectid" id="projectid'+k+'"><input type="hidden" name="roleprojectIndex" id="roleprojectIndex'+k+'" value="1"> <div class="form-group inputbox1"><input type="text" class="form-control ofo projecttitle"  placeholder="Project Title" data-projectid="'+k+'" name="projecttitle" id="projecttitle'+k+'"></div><div class="form-group inputbox2"> <div class="inerinputhttps"><p>https://</p><input type="text" class="form-control ofo imdclink"  placeholder="IMDC link" name="imdclink" data-projectid="'+k+'"  id="imdclink'+k+'"></div></div><div class="pjtimroendbox2"  id=rolesection'+k+'><div class="form-group inputbox3"><input type="text" class="form-control roletitle" id="roletitle'+i+'" placeholder="Role Title" name="roletitle'+i+'"></div><div class="form-group date inputbox4"><input type="text" class="form-control validate datepicker startdate" name="startdate'+i+'" id="startdate'+i+'"data-required="Please select the date of birth" autocomplete="off" placeholder="Start Date" readonly></div><div class="form-group date inputbox5"><input type="text" class="form-control validate datepicker enddate" id="enddate'+i+'" name="enddate'+i+'" data-required="Please select the date of birth" autocomplete="off" placeholder="End Date" readonly></div><div class="inputbox6"><a class="save" id='+i+' href="javascript:void(0)" onclick="saverole(this,'+k+',1);"><i class="far fa-save"></i></a><a href="javascript:void(0)" id='+i+' class="roleplus" onclick="addrolesection(this,'+k+');"><i class="far fa-plus"></i></a></div></div></div> <input type="hidden" value="" id="setBysave'+k+'"> </div>');

       setTimeout(function(){
        datepicker('#startdate'+i,'#enddate'+i);
       },100)
 });


 $('body').delegate(".projectplusEdit",'click',function(){
  var projindex = jQuery('#projectIndexEdit').val();
  console.log(projindex);
  var k = parseInt(projindex) + parseInt(1);
  console.log(k);

  jQuery('#projectIndexEdit').val(k);
  var i=k+'1'; 

  $(".project_box_edit").prepend('<div class="projectsection" id="removeProject'+k+'"><div class="titleboxeditbox"><a href="javascript:void(0)" data-removeid="removeProject'+k+'" class="projectminus"><i class="fa fa-minus"></i></a></div><div class="pjtimrostendbox"><div class="pjtimroendbox1"><input type="hidden" class="projectid" id="projectid'+k+'"><input type="hidden" name="roleprojectIndex" id="roleprojectIndex'+k+'" value="1"> <div class="form-group inputbox1"><input type="text" class="form-control ofo projecttitle"  placeholder="Project Title" data-projectid="'+k+'" name="projecttitle" id="projecttitle'+k+'"></div><div class="form-group inputbox2"> <div class="inerinputhttps"><p>https://</p><input type="text" class="form-control ofo imdclink"  placeholder="IMDC link" name="imdclink" data-projectid="'+k+'"  id="imdclink'+k+'"></div></div><div class="pjtimroendbox2"  id=rolesection'+k+'><div class="form-group inputbox3"><input type="hidden" name="roleidGet" id="roleidGetAp'+i+'" value=""><input type="text" class="form-control roletitle" data-roleid="roleidGetAp'+i+'" id="roletitle'+i+'" placeholder="Role Title" name="roletitle'+i+'"></div><div class="form-group date inputbox4"><input type="text" class="form-control validate datepicker startdate" name="startdate'+i+'" id="startdate'+i+'"data-required="Please select the date of birth" autocomplete="off" placeholder="Start Date" readonly></div><div class="form-group date inputbox5"><input type="text" class="form-control validate datepicker enddate" id="enddate'+i+'" name="enddate'+i+'" data-required="Please select the date of birth" autocomplete="off" placeholder="End Date" readonly></div><div class="inputbox6"><a data-id="'+i+'" class="save roleSaveEdit" data-roleid="0"><i class="far fa-save"></i></a><a class="roleplusedit" href="javascript:void(0)" data-id="'+i+'"><i class="far fa-plus" ></i></a></div></div></div></div>');

       setTimeout(function(){
        datepicker('#startdate'+i,'#enddate'+i);
       },100)
 });

  $(document).ready(function () {
    $(".projectdata").click(function(e){
          e.preventDefault();
       $.ajax({
              type: "GET",
              url: "{{route('performers.projectdata')}}",
              data:{data:$(this).attr('id')},
             
              success: function(data){
                 jQuery('#myModal3').modal('show');
                $("#resumedata").html(data.html);     
              }, complete: function(data) {

                  $('.deleteresumeconfirm[data-toggle=confirmation]').confirmation({
                   rootSelector: '[data-toggle=confirmation]',
                   container: 'body',
                   onConfirm: function() {
                    $("#removeProjectmodal"+$(this).data('resumeid')).remove();
                    roleDelete($(this).data('resumeid'));
                    
                  },
                });
              }
            });
    });

    $('#myModal3').on('hidden.bs.modal', function () {  
location.reload();
});

  $('body').on( "focusout", ".worktitle", function() {

    
  var workid = $(this).data('workid');
    if($(this).val()){
            var workId = $(this).data('workid');
            $.ajax({
              type: "GET",
              url: "{{route('performers.addworktitle')}}",
              data:{data:$(this).val(),work_id:workId},
             
              success: function(data){
                 toastr.success("Work Title Added Successfully");
                 $("#workid").val(data);
                     
              }
            });
    }
    else{
      
    } 
  });
    $(document).on( "focusout", ".projecttitle", function() {

     
      
      

    var ids= $(this).data('projectid');
    var worktitle = $(this).parents('.allditboxeditcover').find('.worktitle').val();
    var workId = $('.workid:last').val();
    var project_id = $(this).parents('.pjtimrostendbox').find('.projectid').val();
    console.log(project_id);
    if($(this).val() && worktitle!=""){
        //console.log($(this).parents('.pjtimrostendbox').find('.projectid').val());
        $.ajax({
            type: "GET",
            url: "{{route('performers.addprojecttitle')}}",
            data:{data:$(this).val(),rid:workId,project_id:project_id},
            success: function(data){
                toastr.success("Project Title Added Successfully");
                $("#projectid"+ids).val(data);

                $("#setBysave"+ids).val(data);
                


            }
        }); 
    } else if($(this).val() && !$('.worktitle').val()){
      toastr.error("Please Enter Worktitle");
  }
    else{
     
    }  
  });


    $('#resumeform').on( "click", ".projectminus", function() {

      key = $(this).data('key');    
      id = $("#setBysave"+key).val();
      

      if(id != ""){

      $.ajax({
        url:"{{route('performers.removeProjectRole')}}",
        type:"GET",
        data:{id:id},
        success:function(data){

          if(data.status == true)
          {
            $(this).parents('.projectsection').remove();
            toastr.success(data.message);             
          }

        },complete: function(data) {
          $(this).parents('.projectsection').remove();
        } 
    });        
           
      }else{
        
        $(this).parents('.projectsection').remove();
      }


    });





  $('body').on( "focusout", ".imdclink", function() {
    var ids =$(this).data('projectid');
    var worktitle = $(this).parents('.allditboxeditcover').find('.worktitle').val();
    var projecttitle = $(this).parents('.allditboxeditcover').find('.projecttitle').val();
    var project_id = $(this).parents('.pjtimrostendbox').find('.projectid').val();
    var workId = $('.workid:last').val();

    console.log(ids);
    console.log($("#projectid"+ids).val());
    if($(this).val() && projecttitle && worktitle!=""){
        $.ajax({
            type: "GET",
            url: "{{route('performers.addimdc')}}",
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            data:{data:$(this).val(),
                pid:$("#projectid"+ids).val(),
                project_id:project_id,
                workId:workId
            },

            success: function(data){
                toastr.success("IMDC Link Added Successfully");
            }
        });
    }
      else if($(this).val() && !$("#projecttitle"+ids).val()){
      toastr.error("Please Enter Project Title");
  }
          else if($(this).val() && $("#projecttitle"+ids).val()){
          toastr.error("Please Enter Work Title");
      }
      else{
   
    } 
  });
    var i=0;
 //    $('body').on( "click", ".roleplus", function() {
 //  // $('.roleplus').click(function(){
 //   i++;
 //    // $('#rolesection'+i).prepend('<div class="pjtimroendbox2 rolesection"><div class="form-group inputbox3 rolesection" ><input type="text" class="form-control" id="roletitle'+i+'" placeholder="Role Title" name="roletitle'+i+'"></div><div class="form-group date inputbox4"><input type="text" class="form-control validate datepicker" name="startdate'+i+'" id="startdate'+i+'"data-required="Please select the date of birth" autocomplete="off" placeholder="Start Date"></div><div class="form-group date inputbox5"><input type="text" class="form-control validate datepicker" name="enddate'+i+'" id="enddate'+i+'" data-required="Please select the date of birth" autocomplete="off" placeholder="End Date"></div><div class="inputbox6"><a id="'+i+'" onclick="test(this);"><i class="far fa-save"></i></a><a href="javascript:void(0)" class="roleminus"><i class="far fa-minus"></i></a></div></div>');
 // });
 

    $('body').delegate(".roleminus","click", function() {
        console.log($(this).parents('.rolesection').remove());
        console.log($(this).parents('.rolesectionEdit').remove());
    });
    $('body').on( "click", ".projectminus", function() {
      $(this).parents('.projectsection').remove();
      
      project_id = $(this).parents('.projectsection').find('.projectid').val();
        
      var removeId =jQuery(this).data('removeid');
      jQuery('#'+removeId).remove();
        if (project_id!="") {
            $.ajax({
                type: "GET",
                url: "{{route('performers.deleteProject')}}",
                headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                data:{project_id:project_id},

                success: function(data){
                    if (data.status == true) {
                        toastr.success(data.message);
                    } else {
                        toastr.warning(data.message);
                    }
                }
            });
        }
    });

    // $('body').on( "click", ".projectminus", function() {
    //   project_id = $(this).parents('.projectsection').find('.projectid').val();
    //     if (project_id!="") {
    //         $.ajax({
    //             type: "GET",
    //             url: "{{route('performers.deleteProject')}}",
    //             headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    //             data:{project_id:project_id},

    //             success: function(data){
    //                 if (data.status == true) {
    //                     toastr.success(data.message);
    //                 } else {
    //                     toastr.warning(data.message);
    //                 }
    //             }
    //         });
    //     }
    // });

    
    $('body').on( "click", ".editModel", function() {




        var work_id = $(this).data('id');
        $.ajax({
            type: "GET",
            url: "{{ route('performers.editResume') }}",
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            data:{ work_id:work_id },
            success: function(data){
                if (data.status == true) {
                    $('#editResumeform').html(data.data);
                    $('#editModel').modal('show');
                     myarray = ["Background Work","Speaking Roles","Commercial Roles"]; 
           
             var rt_title = $(".rt_hidden").val();   
            
            if(jQuery.inArray(rt_title, myarray) != -1) {
               $('.rt_hidden').attr('disabled', true);
            }

                    setTimeout(function(){
                        datepicker('.startdate','.enddate');
                        
                   },100);
                   // toastr.success(data.message);
                } else {
                    toastr.warning(data.message);
                }
            }
        });
    });  


    /** ROLE DELETE **/
    $(document).on( "click", ".roleDelete", function() {
        var role_id = $(this).data('id');
        $.ajax({
            type: "GET",
            url: "{{ route('performers.roleDelete') }}",
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            data:{ role_id:role_id },
            success: function(data){
                if (data.status == true) {
                    toastr.success(data.message);
                } else {
                    toastr.warning(data.message);
                }
            }
        });
        $(this).parents('.pjtimroendbox2').remove();
    });  
    

    $("#myModal , #editModel").on("hidden.bs.modal", function () {
        location.reload(true);
    });
});

function showMore(id)
{

 $('.showMoreResume'+id).slideToggle();

 var txt = $("#changtxt"+id).text(); 
 if(txt === "Show more")
 {
  $("#changtxt"+id).text("Hide"); 
  $("#changarrUp"+id).removeClass("hidden"); 
  $("#changarrDoun"+id).addClass("hidden"); 
    
 }
 else
  {
   $("#changtxt"+id).text("Show more");
   $("#changarrDoun"+id).removeClass("hidden"); 
  $("#changarrUp"+id).addClass("hidden"); 
 }
 
  
}
</script>
<script type="text/javascript">
  $('.deleteresume[data-toggle=confirmation]').confirmation({
           rootSelector: '[data-toggle=confirmation]',
           container: 'body',
           onConfirm: function() {
                id = $(this).data('id');
               $.ajax({

                 type: "POST",
                 url: "{{route('performers.deleteresume')}}",
                 headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                 data: {
                         id:id,
                       },
                 success: function (data) {
                        // console.log(data);
                        toastr.success('Resume Removed Successfully');
                         $("#"+data).remove();
                       }         
             });
           },
         });   

  $('.deleteresumeconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
                resumeDelete($(this).data('resumeid'));
             },
           });
  function resumeDelete(id)
  {
    
    $.ajax({
        url:"{{route('performers.removeProject')}}",
        type:"GET",
        data:{id:id},
        success:function(data){

          if(data.status == true)
          {
              $("#removeProject"+id).remove();
              toastr.success(data.message);
          }

        }
    });

  }

  function roleDelete(id)
  {
    
    $.ajax({
        url:"{{route('performers.removeProjectRole')}}",
        type:"GET",
        data:{id:id},
        success:function(data){

          if(data.status == true)
          {
              $("#removeProject"+id).remove();
              toastr.success(data.message);
          }

        }
    });

  }

  





    $("#resumeSearchBox").keyup(function(){
    search = $(this).val();
    get_id = $("#get_id").val();   
     $.ajax({
        url:"{{route('performers.resumeSearch')}}",
        type:'GET',
        data:{search:search,get_id:get_id},
        success:function(data)
        {
          if(data.status == true){        
            $("#ResumeResult").html(data.html);
            
            $(".projectdata").click(function(e){
          e.preventDefault();
       $.ajax({
              type: "GET",
              url: "{{route('performers.projectdata')}}",
              data:{data:$(this).attr('id')},
             
              success: function(data){
                 jQuery('#myModal3').modal('show');
                $("#resumedata").html(data.html);     
              }, complete: function(data) {

                  $('.deleteresumeconfirm[data-toggle=confirmation]').confirmation({
                   rootSelector: '[data-toggle=confirmation]',
                   container: 'body',
                   onConfirm: function() {
                    $("#removeProjectmodal"+$(this).data('resumeid')).remove();
                    roleDelete($(this).data('resumeid'));
                    
                  },
                });
              }
            });
    });
            } 
        }
      });

  });
</script>



@endsection


