@if(count($search_result))
 @forelse($search_result as $work)    
      <div class="acopartboxne1" id="{{$work->id}}">
        <div class="accordion-container">
          <div class="set hedtobox">
            <div class="acodhedpart{{(count($work->project_performer) ?' active' :'')}}">
                <div class="titledivallbox">
                  <h3>{{$work->resume_title}}</h3>
                  <div class="helpboxdcl">
                    <span class="tool" data-tip="{{$helpdesk->resume_help}}" tabindex="1"><i class="fal fa-question-circle"></i></span>
                  </div>
                  <div class="morbtnmnubox">
                    <div class="morebtneddil">                
                      <div class="MenuSceinain">
                        <div class="menuheaderinmore">
                          <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
                          <div class="mainineropenbox_1">
                            <div class="mainineropenbox_2">
                              <div class="opensubmenua_1">
                                <a href="javascript:void(0)" data-toggle="modal" data-id="{{ $work->id }}" class="editModel">Edit</a>
                              </div>

                              @if (!in_array($work->resume_title, array('Background Work','Speaking Roles','Commercial Roles'))) 
                              <div class="opensubmenua_1">
                                <a href="javascript:void(0)"  data-id="{{ $work->id }}" data-toggle="confirmation" data-title="Are you sure?" class="deleteresume">Delete</a>
                              </div>
                              @endif
                            </div> 
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>              

              <a href="javascript:void(0)">
                <span class="moretext">Show section</span>
                <span class="lesstext">Hide section</span>                 
                <i class="far fa-angle-down"></i>
              </a>
            </div>

            <div class="content" style="display{{(count($work->project_performer) ? ':block;' : '' )}}">
              <?php $i=1;?>
                 @forelse($work->project_performer as $project)
                @if($i < 6)          
              <div class="inprojanmembox" id="removeProject{{$project->id}}">
                <div class="inprojanmembox_iner1">
                  <p><a href="javascript:void(0)" type="button"  id="{{$project->id}}" class="projectdata"> {{ $project->project_title }}</a></p>
                </div>
                <div class="inprojanmembox_iner2">
                  <p> <a href="javascript:void(0)" type="button" id="{{$project->id}}" class="projectdata"> {{ $project->project_role->count() }} Roles</a></p>
                </div>
                <div class="inprojanmembox_iner3">
                  <?php
                  $startdate=array();
                  $enddate=array();
                  foreach($project->project_role as $role){
                    $startdate[] = $role->start_date;
                    $enddate[]   = $role->end_date;
                  } ?>
                  @if(!empty($startdate))
                  <p>{{date('d M Y',strtotime(min($startdate))) }} - {{date('d M Y',strtotime(max($enddate))) }}</p>
                  @endif
                </div>
                <div class="inprojanmembox_iner4">
                  <a href="{{ $project->IMDC_link }}" target="_blank">IMDB <i class="far fa-long-arrow-alt-right"></i></a>
                </div>

                <div class="inprojanmembox_iner5">
                <a href="javascript:void(0)" data-toggle="confirmation" data-title="are you sure?" data-resumeid="{{$project->id}}"  class="deleteresumeconfirm"><i class="fas fa-trash"></i></a>
              </div>

              </div>             
              <?php $i++; ?>
              @else
              <div class="showMoreResume{{$work->id}}" style="display:none">
                <div class="inprojanmembox" id="removeProject{{$project->id}}">
                <div class="inprojanmembox_iner1">
                  <h3>{{ $project->project_title }}</h3>
                </div>
                <div class="inprojanmembox_iner2">
                  <p> <a href="javascript:void(0)" type="button"  id="{{$project->id}}" class="projectdata"> {{ $project->project_role->count() }} Roles</a></p>
                </div>
                <div class="inprojanmembox_iner3">
                  <?php
                  $startdate=array();
                  $enddate=array();
                  foreach($project->project_role as $role){
                    $startdate[] = $role->start_date;
                    $enddate[]   = $role->end_date;
                  } ?>
                  @if(!empty($startdate))
                  <p>{{date('d M Y',strtotime(min($startdate))) }} - {{date('d M Y',strtotime(max($enddate))) }}</p>
                  @endif
                </div>
                <div class="inprojanmembox_iner4">
                  <a href="{{ $project->IMDC_link }}" target="_blank">IMDB <i class="far fa-long-arrow-alt-right"></i></a>
                </div>
                <div class="inprojanmembox_iner5">
                <a href="javascript:void(0)" data-toggle="confirmation" data-title="are you sure?" data-resumeid="{{$project->id}}"  class="deleteresumeconfirm"><i class="fas fa-trash"></i></a>
              </div>
              </div>             
              </div>
             @endif
             @empty              
              <div class="noprojAvailable">
               <p>No Project Available</p>
              </div>              
              @endforelse
              @if($work->project_performer->count()>5 || $i==5)
              <div class="showmorebtnbox">
                <a href="javascript:void(0)" onclick="showMore('{{$work->id}}')"><span id="changtxt{{$work->id}}">Show more</span>
                  <i id="changarrDoun{{$work->id}}" class="far fa-angle-down"></i>
                  <i id="changarrUp{{$work->id}}" class="far fa-angle-up hidden"></i>
                </a>
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>    
      @empty
      <div class="hedfullmotobox">
        <div class="noprojAvailable">     
          <p>No Resume available</p>
        </div>
      </div>
      @endforelse
@endif
