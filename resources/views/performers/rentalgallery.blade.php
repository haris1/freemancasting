@extends('performers.layouts.app')
@section('content')
<div class="gallerypotosall">
  <div class="leftrighticontitl">
    <div class="topbarprfname">

     <a href="javascript:window.history.back()"> <i class="far fa-chevron-left"></i></a> <h3>{{$gallery['rental_title']}}</h3>
    </div>
    <div class="righteditiocn">
      <a href="javascript:void(0)" onclick="viewGallery('{{ $gallery['id'] }}','{{ $gallery['rental_title'] }}')"><i class="far fa-edit"></i></a>
    </div>
  </div>
  
  <div class="addvewimgbox">
    <div class="discriptionimg">
      <h3>{{ $gallery['description'] }}</h3>
      <a href="javascript:void(0)"  type="button" class="btnAddPortfolioGallery"><i class="far fa-plus"></i> image1</a>      
    </div>
    <div class="gridimgphotobox">

      <div class="grid" id="grid">
       @if($gallery['rentalmedia'])
       @foreach($gallery['rentalmedia'] as $mediaValue)        
       <div class="item" id="portfolioMedia{{$mediaValue->id}}">
        <span href="javascript:void(0)"  data-toggle="confirmation" data-title="are you sure?" data-imageid="{{$mediaValue->id}}"  class="deleteimageconfirm deleteimgbox"><i class="fas fa-trash"></i></span>
        <a href="{{$mediaValue->image_url}}">
        <img src="{{$mediaValue->image_url}}" alt="">
      </a>
    </div>
       @endforeach
       @endif
     </div>
   </div>
 </div>
</div>



@endsection
@section('scripts')

<script type="text/javascript">
    jQuery('#grid').imageview();   
  </script>
<script type="text/javascript">

function portfolioMediaDelete(id)
{  

  $("#portfolioMedia"+id).remove();
  $.ajax({
         url:"{{route('performers.rental.removeMedia')}}",
        type:'get',
        data:{id:id},
        success:function(response){
            if(response.status == true)
            {
              toastr.success(response.message); 
              location.reload();                   
            }
        }

  });
  
}
$('.deleteimageconfirm[data-toggle=confirmation]').confirmation({
         rootSelector: '[data-toggle=confirmation]',
         container: 'body',
         onConfirm: function() {
            portfolioMediaDelete($(this).data('imageid'));
         },
       });
</script>
@include('performers.commonRental')
@endsection