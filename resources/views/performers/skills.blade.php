  @extends('performers.layouts.app')
@section('content')
<div class="wctleftrightbox">
 @if($performer->applicationfor == "Group")
  @include('performers.layouts.groupSidebar')  
  @else
  @include('performers.layouts.sidebar')  
  @endif

  @if($authtype=='performer')     
  <input type="hidden" value="{{request()->route('id')}}" id="get_id" name="">
  @else
  <input type="hidden" value="{{request()->route('id')}}" id="get_id" name="">  
  @endif
   <div class="rightallditinwct">
    <!-- <div class="searchbarboxfilt">
      <div class="srchbox">        
        <div class="form-group">
          <i class="far fa-search"></i>
          <input type="text" class="form-control" id="skillSearch" placeholder="Search">
        </div>
      </div>
      <div class="addandtagbox">
        <div class="addatribut">
          <a href="javascript:void(0)" type="button" data-toggle="modal" data-target="#mySkillModal"><i class="far fa-plus"></i>New Skill</a>
        </div>
        <div class="tagboxright">
          <a href="javascript:void(0)"><i class="far fa-tag"></i></a>
        </div>
      </div>
    </div> -->
    <div class="addflagshidbox">
      <div class="accordion-container">
        <div class="set hedtobox">
          <div class="srchbox">        
            <div class="form-group skill_resume_Search_hide">
              <i class="far fa-search"></i>
             <input type="text" class="form-control" id="skillSearch" placeholder="Search">
            </div>
          </div>
      @if($authtype=='performer')     
     @else
     @include('performers.flag')
     @endif 
        </div>      
      </div>
    </div>

  <div class="topbarbotdivcover">
    @if($authtype=='performer')
    @include('performers.layouts.tabbar')
    @else
    @include('performers.layouts.internal-tabbar')
    @endif
    <div class="skillsalldetainer" id="skillcontainer">
  @foreach($skill_list as $skill) 
      <div>    
      <div class="qsmsodetailbox" id="skillPart{{$skill->id}}">
              <div class="qsmso_titlebox">
                <h3 id="skillouterpart{{$skill->id}}">{{$skill->skill_title}}</h3>
                <div class="helpboxdcl">
                  <span class="tool" data-tip="{{$helpdesk->skills_help}}" tabindex="1"><i class="fal fa-question-circle"></i></span>
                  
                </div>
                <div class="morebtneddil">            
                  <div class="MoreMenuScenes">
                    <div class="MenuSceinain">
                      <div class="menuheaderinmore">
                        <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>
                        <!-- <img src="svg/more.svg" class="MoreImgSet"> -->
                        <div class="mainineropenbox_1">
                          <div class="mainineropenbox_2">
                            <div class="opensubmenua_1">
                              <a href="javascript:void(0)" id="{{$skill->id}}" onclick="fieldId('{{$skill->id}}');">Add</a>
                            </div> 
                           
                            @if (!in_array($skill->skill_title, array('Qualifications','Specialty Driver','Sports','Music','Others','Community Servants','Circus/Magic','Culinary','Dance','Military','Bands','Speciality Driver'))) 
                            <div class="opensubmenua_1">
                              <a href="javascript:void(0)" onclick="editModel('{{$skill->id}}')">Edit</a>
                            </div>
                            <div class="opensubmenua_1">
                              <a href="javascript:void(0)"  data-toggle="confirmation" data-title="Are you sure?" data-titleid="{{$skill->id}}" href="javascript:void(0)" class="deleteskilltitle">Delete</a>
                            </div> 
                            @endif
                          </div> 
                        </div>
                      </div>
                    </div>
                  </div>            
                </div>
              </div>
              <div id="skillContent{{$skill->id}}" class="deletefield">
           
              @foreach($skill->skillFields as $section)
                <div class="inerdetaqsmobox">
                  <div class="inerdetaqsmo_left">
                    <input type="hidden" id="editskill_id{{$section->id}}" value="{{$section->skill_id}}">
                    <p class="pText{{$section->id}}">{{$section->field_name}}</p>
                    <input type="text" class="hidden SkillText{{$section->id}}"  value="{{$section->field_name}}" name="field_name" id="editfield_name{{$section->id}}">
                  </div>
                  <div class="inerdetaqsmo_right">
                    <p class="pText{{$section->id}}">{{$section->field_value}}</p>
                    <input type="text"  class="hidden SkillText{{$section->id}}" value="{{$section->field_value}}" name="field_value" id="editfield_value{{$section->id}}">

                  <a  class="edithidebox edithide{{$section->id}}"  data-toggle="confirmation" data-title="are you sure?" data-sectionid="{{$section->id}}" href="javascript:void(0)"><i class="far fa-trash"></i></a>
                  <a onclick="editSkill('{{$section->id}}')" class="edithidebox edithide{{$section->id}}" href="javascript:void(0)"><i class="far fa-edit"></i></a>
                  <a onclick="updateSkill('{{$section->id}}')" class="edithidebox SkillText{{$section->id}} hidden" href="javascript:void(0)"><i class="far fa-save"></i></a>
                  </div>
                </div>
              
            @endforeach   
            </div>  
                <div class="inerdetaqsmobox fieldForm" id="fieldForm{{$skill->id}}">
                  <div class="inerdetaqsmo_left">
                   <!--  <input type="hidden" name="skill_id" value="{{$skill->id}}" class="{{$skill->id}}"> -->
                    <p><input type="text" name="field_name" placeholder="Enter skill name" id="field_name{{$skill->id}}" class="field_name" ></p>
                  </div>
                  <div class="inerdetaqsmo_right">
                    <input type="text" name="field_value" placeholder="Enter skill value"  id="field_value{{$skill->id}}" class="field_value"> <a href="javascript:void(0)" class="{{$skill->id}} edithidebox"  onclick="saveSkillSection(this,'{{$skill->id}}');"><i class="far fa-save"></i></a>
                    
                  </div>

                </div>
              </div>
            </div>
  @endforeach  
    </div>
    <div class="addbtnbotbox">
      <button id="addskill" data-toggle="modal" data-target="#SkillModal"><i class="far fa-plus"></i>Add Skill</button>
    </div>
  </div>

</div>
</div>
</div>
<!-- skill model -->
  <div class="modal fade newgallerymod noteseditmodal" id="SkillModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="editsktoboxcover">
           <form method="post" action="{{route('performers.addskill')}}"  id="skillform">
            @csrf          
            <div class="titlandclobtn2">
              <h3>Skill Title</h3>
              <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <div class="enterskintonebox">
              <div class="form-group">
                <input type="text" class="form-control"  name="skill" placeholder="Skill Title" id="addtitlemodal">
              </div>
              
              <input type="button"  value="Add" class="btn" id="skillSubmit">
            </div>              
          </form>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- end skill model -->
<!--edit skill model -->
  <div class="modal fade newgallerymod noteseditmodal" id="EditSkillModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="editsktoboxcover">
           <form method="post" action="{{route('performers.updateskilltitle')}}"  id="editskillform">
            @csrf          
            <div class="titlandclobtn2">
              <h3>Edit Title</h3>
              <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <div class="enterskintonebox">
              <div class="form-group">
                <input type="hidden" name="id" id="editskillid">
                <input type="text" class="form-control"  name="skill_title" placeholder="Skill Title" id="editskilltitle">
          
             </div>
              
              <input type="button"  value="UPDATE" class="btn" id="updateskillbutton">
            </div>              
          </form>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- end edit skill model -->
@endsection
@section('scripts')

<script type="text/javascript">
   $("#updateskillbutton").click(function(e){
      e.preventDefault()
     var form = $("#editskillform");
        
     $.ajax({
         type: form.attr('method'),
         url: form.attr('action'),
         data: form.serialize(),
         success: function (data) {
              if(data.error){
                 return;
             }
             
             if(data.status==false){
                toastr.error(data.message);
             }
             else{
         $('#skillouterpart'+$('#editskillid').val()).html($('#editskilltitle').val());
           $('#EditSkillModal').modal('hide');  // Your modal Id

           toastr.success(data.message);// THis is success message
          }
         },
         error: function (result) {

         }
     });
     
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $(".fieldForm").hide();
  });
</script>
<script type="text/javascript">
   $("#skillSubmit").click(function(e){
      e.preventDefault();
        skilltitle=$('#addtitlemodal').val();
        if(skilltitle==""){
          toastr.error("Enter Skill Title");
          return false;
        }
         var form = $("#skillform");
         $.ajax({
         type : form.attr('method'),
         url  : form.attr('action'),
         data : form.serialize(),
         success : function (data, status) {
              if(data.error){
                 return;
              }
              if(data.status==false){
                toastr.error(data.message);
              }
              else{
              toastr.success("Skill Title Added Successfully");
              location.reload();
            }
          },
         error: function (result) {

         }
     }); 
     
  });
</script>
<script type="text/javascript">
  function saveSkillSection(element,skill_id) {
    
  //var sectionclass       = element.getAttribute('class');
  var field_nameelement  = $("#field_name"+skill_id);
  var field_valueelement = $("#field_value"+skill_id);   
  var field_name         = field_nameelement.val();
  var field_value        = field_valueelement.val();
  
   $.ajax({

      type: "POST",
      url: "{{route('performers.addskillsection')}}",
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data: {
              skill_id:skill_id,
              field_name:field_name,
              field_value:field_value
            },
      success: function (data) {
             // console.log(data);
             if(data.status==false){
              toastr.error(data.message);
             }
             else{
             toastr.success("Skill Added Successfully");
             jQuery('#skillContent'+skill_id).html(data.response);
             $("#field_name"+skill_id).val('');
             $("#field_value"+skill_id).val('');

              $("#fieldForm"+skill_id).hide();
             //location.reload(); 
             $('.deletefield [data-toggle=confirmation]').confirmation({
                       rootSelector: '[data-toggle=confirmation]',
                       container: 'body',
                       onConfirm: function() {
                         deleteSkillField($(this).data('sectionid'));
                      },
                    });
            }
            }         
  });
  }
</script>
<script type="text/javascript">
  function fieldId(Id){
   
    $("#fieldForm"+Id).show();
    $("#field_name"+Id).focus();

  }
  function editModel(id){
      
        $.ajax({

          type: "POST",
          url: "{{route('performers.getSkillTitle')}}",
          headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
          data: {
                  id:id,
                },
          success: function (data) {
                 // console.log(data);
                  $("#editskilltitle").val(data);
                  $("#editskillid").val(id);
                  jQuery('#EditSkillModal').modal('show');
                 }         
      });
  }
  
  function skillDelete(id){
    
      $.ajax({

        type: "POST",
        url: "{{route('performers.deleteskill')}}",
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
        data: {
                id:id,
              },
        success: function (data) {
               // console.log(data);
                toastr.success(data.message);
                $('#skillPart'+id).remove();
              }         
    });
  }

  function editSkill(id)
  {
     $(".SkillText"+id).removeClass('hidden');
     $(".pText"+id).addClass('hidden');
     $(".edithide"+id).hide();
  }
  function updateSkill(id){    
    $.ajax({

      type: "POST",
      url: "{{route('performers.editskillsection')}}",
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data: { 
              id    : id, 
              skill_id:$('#editskill_id'+id).val(),
              field_name:$('#editfield_name'+id).val(),
              field_value:$('#editfield_value'+id).val(),
            },
      success: function (data) {
             // console.log(data);
              if(data.status==false){
                toastr.error(data.message);
              }

else{
              toastr.success("Skill Updated Successfully");
              jQuery('#skillContent'+$('#editskill_id'+id).val()).html(data.response);
              $('.deletefield [data-toggle=confirmation]').confirmation({
                        rootSelector: '[data-toggle=confirmation]',
                        container: 'body',
                        onConfirm: function() {
                          deleteSkillField($(this).data('sectionid'));
                       },
                     });
            }
              
            }         
  });
  }
  function deleteSkillField(id){
        $.ajax({

          type: "POST",
          url: "{{route('performers.deletefieldsection')}}",
          headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
          data: {
                  id:id,
                  skill_id:$('#editskill_id'+id).val()
                },
          success: function (data) {
                 // console.log(data);
                  toastr.success(data.message);
                  jQuery('#skillContent'+$('#editskill_id'+id).val()).html(data.response);
                   $('.deletefield [data-toggle=confirmation]').confirmation({
                       rootSelector: '[data-toggle=confirmation]',
                       container: 'body',
                       onConfirm: function() {
                         deleteSkillField($(this).data('sectionid'));
                      },
                    });
                }         
      });
  }
  $('.deletefield [data-toggle=confirmation]').confirmation({
           rootSelector: '[data-toggle=confirmation]',
           container: 'body',
           onConfirm: function() {
             deleteSkillField($(this).data('sectionid'));
           },
         });
  $('.deleteskilltitle[data-toggle=confirmation]').confirmation({
           rootSelector: '[data-toggle=confirmation]',
           container: 'body',
           onConfirm: function() {
             skillDelete($(this).data('titleid'));
           },
         });   

  $(document).ready(function(){
  
    $("#skillSearch").keyup(function(){
    search = $(this).val();
    get_id = $("#get_id").val();   
     $.ajax({
        url:"{{route('performers.skills.search')}}",
        type:'GET',
        data:{search:search,get_id:get_id},       
        success:function(data)
        {
          if(data.status == true){        
            $(".skillsalldetainer").html(data.html);
             $(".fieldForm").hide();
              $('.deletefield [data-toggle=confirmation]').confirmation({
                       rootSelector: '[data-toggle=confirmation]',
                       container: 'body',
                       onConfirm: function() {
                         deleteSkillField($(this).data('sectionid'));
                       },
                     });
              $('.deleteskilltitle[data-toggle=confirmation]').confirmation({
                       rootSelector: '[data-toggle=confirmation]',
                       container: 'body',
                       onConfirm: function() {
                         skillDelete($(this).data('titleid'));
                       },
                     });   


            }
        }
      });

  });
});
</script>
@include('performers.commonSkill')
@endsection

