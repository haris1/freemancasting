@extends('performers.layouts.app')
@section('content')
<div class="wctleftrightbox">
  @include('performers.layouts.sidebar')
  <div class="rightallditinwct">
    <div class="searchbarboxfilt">
      <div class="srchbox">        
        <div class="form-group">
          <i class="far fa-search"></i>
          <input type="text" class="form-control" id="" placeholder="Search">
        </div>
      </div>
      <div class="addandtagbox">
        
        <div class="tagboxright">
          <a href="javascript:void(0)"><i class="far fa-tag"></i></a>
        </div>
      </div>
    </div>
    @include('performers.layouts.tabbar')
    
@endsection
@section('scripts')
 
@endsection