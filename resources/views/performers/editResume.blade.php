<div class="totitleaddris">
    <input type="hidden" name="projectindex" id="projectIndexEdit" value="0">
    <h4>Edit Resume</h4>
    <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
</div>
<input type="hidden" id="workid" class="workid" value="{{ $workperformer->id }}">

<div class="allditboxeditcover" >
    
    <div class="titleboxeditbox">
        <div class="form-group">
            <input type="text" class="form-control ofo worktitle rt_hidden" name="worktitle" placeholder="Work Title" data-workid='{{ $workperformer->id }}' value="{{ $workperformer->resume_title }}" id="editResumeTitle">
        </div>
        <a href="javascript:void(0)" class="projectplusEdit"><i class="far fa-plus"></i></a>
    </div>
        <div class="project_box_edit">
              
        </div>
        @forelse($workperformer->project_performer as $key => $project_performer)
            <div class="pjtimrostendbox">                
            <input type="hidden" id="rolecount"  value="{{isset($project_performer->role_count) ? $project_performer->role_count : 0 }}">

            <input type="hidden" name="roleprojectIndexEdit{{$key}}" id="roleprojectIndexEdit{{$key}}" value="1">              
            <input type="hidden" class="projectid" value="{{ $project_performer->id }}" id="projectid{{ $key }}">

            <div class="pjtimroendbox1">
                <div class="form-group inputbox1">
                    <input type="text" class="form-control ofo projecttitle" id="projecttitle{{ $key }}" placeholder="Project Title" name="projecttitle" value="{{ $project_performer->project_title }}" data-projectid="{{ $key }}">
                </div>
                <div class="form-group inputbox2">
                    <div class="inerinputhttps">
                           <p>https://</p>
                    <input type="text" class="form-control ofo imdclink" id="imdclink0" placeholder="IMDC link" name="imdclink" value="{{ $project_performer->IMDC_link }}" data-projectid="{{ $key }}">
                </div>
                </div>
            </div>
            @forelse($project_performer->project_role as $index => $project_role)
                <div class="pjtimroendbox2" id="rolesectionEdit{{ $index }}">
                    <input type="hidden" name="roleidGet" id="roleidGet{{ $index }}" value="{{ $project_role->id }}">
                    <div class="form-group inputbox3">
                        <input type="text" class="form-control roletitle" placeholder="Role Title" name="roletitle"  data-roleid="roleidGet{{ $index }}" value="{{ $project_role->role_title }}">
                    </div>
                    <div class="form-group date inputbox4">
                        <input type="text" class="form-control validate datepicker startdate" name="startdate" id="startdate01"data-required="Please select the date of birth" autocomplete="off" placeholder="Start Date" value="{{ date('d-m-Y', strtotime($project_role->start_date)) }}" readonly>
                    </div>
                    <div class="form-group date inputbox5">
                        <input type="text" class="form-control validate datepicker enddate"  name="enddate" data-required="Please select the date of birth" autocomplete="off" placeholder="End Date" value="{{ date('d-m-Y', strtotime($project_role->end_date)) }}" readonly>
                    </div>
                    <div class="inputbox6">
                            <a href="javascript:void(0)" class="save roleSaveEdit" data-roleid="{{ $project_role->id }}"><i class="far fa-save"></i></a>
                            <?php
                                $projectRoleTotal =  count($project_performer->project_role)-1;
                            ?>
                            @if($projectRoleTotal == $index)
                                <a class="roleplusedit" href="javascript:void(0)" data-id="{{$index}}"><i class="far fa-plus" ></i></a>
                            @else
                                <a class="roleDelete" href="javascript:void(0)" data-id="{{ $project_role->id }}"><i class="far fa-minus"></i></a>
                            @endif

                    </div>
                </div>
            @empty
                <div class="pjtimroendbox2" id="rolesectionEdit0">
                    <div class="form-group inputbox3">
                        <input type="text" class="form-control roletitle" id="roletitle01" placeholder="Role Title" name="roletitle">
                    </div>
                    <div class="form-group date inputbox4">
                        <input type="text" class="form-control validate datepicker startdate" name="startdate0" id="startdate01"data-required="Please select the date of birth" autocomplete="off" placeholder="Start Date" readonly>
                    </div>
                    <div class="form-group date inputbox5">
                        <input type="text" class="form-control validate datepicker enddate" id="enddate01" name="enddate01" data-required="Please select the date of birth" autocomplete="off" placeholder="End Date" readonly>
                    </div>
                    <div class="inputbox6">
                        <a href="javascript:void(0)" id="0" class="save roleSaveEdit" data-roleid="0"><i class="far fa-save"></i></a>
                        <a class="roleplusedit" href="javascript:void(0)" data-id="0"><i class="far fa-plus" ></i></a>

                    </div>
                </div>
            @endforelse
            </div>
        @empty
            <div class="pjtimrostendbox" >

                <div class="pjtimroendbox1">
                    <div class="form-group inputbox1">
                          <input type="hidden" id="projectid1" class="projectid">

                        <input type="text" class="form-control ofo projecttitle" id="projecttitle1" placeholder="Project Title" name="projecttitle" data-projectid='1'>
                    </div>
                    <div class="form-group inputbox2">
                        <div class="inerinputhttps">
                           <p>https://</p>
                           <input type="text" class="form-control ofo imdclink" id="imdclink1" placeholder="IMDC link" name="imdclink" data-projectid="1" >
                       </div>
                    </div>
                </div>
                <div class="pjtimroendbox2" id="rolesectionEdit0">
                    <div class="form-group inputbox3">
                        <input type="text" class="form-control" id="roletitle11" placeholder="Role Title" name="roletitle0">
                    </div>
                    <div class="form-group date inputbox4">
                        <input type="text" class="form-control validate datepicker startdate" name="startdate0" id="startdate11"data-required="Please select the date of birth" autocomplete="off" placeholder="Start Date" readonly>
                    </div>
                    <div class="form-group date inputbox5">
                        <input type="text" class="form-control validate datepicker enddate" id="enddate11" name="enddate01" data-required="Please select the date of birth" autocomplete="off" placeholder="End Date" readonly>
                    </div>
                    <div class="inputbox6">
                        <a class="save" href="javascript:void(0)" id="0" onclick="saverole(this,'1','1');"><i class="far fa-save"></i></a>
                        <a class="roleplusedit" href="javascript:void(0)" id="0"  onclick="addrolesectionEdit(this,'0');" data-id="0"><i class="far fa-plus" ></i></a>
                    </div>
                </div>
            </div>
        @endforelse

</div>