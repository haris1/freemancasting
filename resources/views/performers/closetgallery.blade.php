@extends('performers.layouts.app')
@section('content')
<div class="gallerypotosall">
  <div class="leftrighticontitl">
    <div class="topbarprfname">

     <a href="javascript:window.history.back()" > <i class="far fa-chevron-left"></i></a> <h3>{{$gallery['closet_title']}}</h3>
    </div>
    <div class="righteditiocn">
      <a href="javascript:void(0)" onclick="viewGallery('{{ $gallery['id'] }}','{{ $gallery['closet_title'] }}')"><i class="far fa-edit"></i></a>
    </div>
  </div>
  
  <div class="addvewimgbox">
    <div class="discriptionimg">
      <h3>{{$gallery['descripation']}}</h3>
      <a href="javascript:void(0)" class="btnAddClosetGallery"><i class="far fa-plus"></i> image</a>      
    </div>
    <div class="gridimgphotobox">

      <div class="grid" id="grid">
       @if($gallery['performermedia'])
       @foreach($gallery['performermedia'] as $mediaValue)        
        <div class="item" id="closetMedia{{$mediaValue->id}}">
        <span  data-toggle="confirmation" data-title="are you sure?" data-imageid="{{$mediaValue->id}}"  class="deleteimageconfirm deleteimgbox"><i class="fas fa-trash"></i></span>
          <a href="{{$mediaValue->image_url}}">
        <img src="{{$mediaValue->image_url}}" alt="">
      </a></div>
       @endforeach
       @endif
     </div>
   </div>
 </div>
</div>




@endsection

@section('scripts')

<script type="text/javascript">
    jQuery('#grid').imageview();   
  </script>
<script type="text/javascript">
    
  function closetMediaDelete(id)
{  
  $("#closetMedia"+id).remove();
  $.ajax({
         url:"{{route('performers.closet.removeMedia')}}",
        type:'get',
        data:{id:id},
        success:function(response){
            if(response.status == true)
            {
              toastr.success(response.message);  
              location.reload();                 
            }
        }

  });
  
}

$("#showGalleryModal").on("hidden.bs.modal", function () {
    location.reload();
});
$('.deleteimageconfirm[data-toggle=confirmation]').confirmation({
         rootSelector: '[data-toggle=confirmation]',
         container: 'body',
         onConfirm: function() {
            closetMediaDelete($(this).data('imageid'));
         },
       });
</script>

@include('performers.commonCloset')

@endsection