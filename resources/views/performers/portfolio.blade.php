@extends('performers.layouts.app')
<!-- <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-tagsinput.css') }}"> -->
@section('content')
<div class="wctleftrightbox">
  @if($performer->applicationfor == "Group")
  @include('performers.layouts.groupSidebar')  
  @else
  @include('performers.layouts.sidebar')  
  @endif

   @if($authtype=='performer')     
  <input type="hidden" value="{{request()->route('id')}}" id="get_id" name="">
  @else
  <input type="hidden" value="{{request()->route('id')}}" id="get_id" name="">  
  @endif  
 <div class="rightallditinwct">
  <div class="searchbarboxfilt">
    
    <!-- <div class="addandtagbox">
      <div class="addatribut">
        <a href="javascript:void(0)" type="button" data-toggle="modal" data-target="#myModal"><i class="far fa-plus"></i>New Media Gallery</a>
      </div> 
      <div class="tagboxright">
        <a href="javascript:void(0)"><i class="far fa-tag"></i></a>
      </div>
    </div> -->
  </div>
  
  <div class="addflagshidbox">
    <div class="accordion-container">
      <div class="set hedtobox">
        <div class="srchbox">        
          <div class="form-group hidesearch">
            <i class="far fa-search"></i>
            <input type="text" class="form-control" id="portfolioSearch" placeholder="Search">
          </div>
        </div>
     
     @if($authtype=='performer')     
     @else
     @include('performers.flag')
     @endif
         
      </div>      
    </div>
  </div>


  <div class="topbarbotdivcover">
   
  @if($authtype=='performer')
     @include('performers.layouts.tabbar')
     @else
     @include('performers.layouts.internal-tabbar')
     @endif
  @if(count($portfolio_list))
  <div id="defaultDisplayDiv" class="portfolboxcover">
  @foreach($portfolio_list as $portfolioValue)
  @if($portfolioValue->is_video == "no")
  <div class="hedfullmotobox" id="galleryDelete{{$portfolioValue->id}}">
    <div class="allboxtitlesem">
      <div class="titleboxallcover">
      <div class="titledivallbox">
        <h3> <a href="javascript:void(0)"  onclick="viewGallery('{{ $portfolioValue['id'] }}','{{ $portfolioValue['portfolio_title'] }}')" >{{$portfolioValue['portfolio_title'] }}</a></h3>
      </div>
      <div class="helpboxdcl">
        <span class="tool" data-tip="{{isset($helpdesk->portfolio_help)? $helpdesk->portfolio_help :'' }}" tabindex="1"><i class="fal fa-question-circle"></i></span>
        
      </div>
      <div class="morbtnmnubox">
        <div class="morebtneddil">                
          <div class="MenuSceinain">
            <div class="menuheaderinmore">
              <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
              <div class="mainineropenbox_1">
                <div class="mainineropenbox_2">
                  <div class="opensubmenua_1">
                    <a href="javascript:void(0)" id="{{$portfolioValue->id}}"  class="imageAddTogallery" onclick="imageAddTogallery('{{$portfolioValue->id}}')">Add</a>
                  </div> 
                 
                  <div class="opensubmenua_1">
                    <a href="javascript:void(0)"  onclick="viewGallery('{{ $portfolioValue['id'] }}','{{ $portfolioValue['portfolio_title'] }}')" >Edit</a>
                  </div>
                  @if($portfolioValue['portfolio_title'] != "Headshots" && $portfolioValue['portfolio_title'] != "Full Body Shots" && $portfolioValue['portfolio_title'] != "Tattoos" &&
                  $portfolioValue['portfolio_title'] != "Piercings")                  
                  <div class="opensubmenua_1">
                    <a href="javascript:void(0)" id="{{$portfolioValue->id}}" class="gallaryconfirm"  data-toggle="confirmation" data-title="are you sure?" data-imagetagid="{{$portfolioValue->id }}">Delete</a>
                  </div> 
                  @endif
                </div> 
              </div>
            </div>
          </div>
        </div>
      </div>

   
      <div id="portfolio{{$portfolioValue->id}}">
        @if(count($portfolioValue->portfoliotag))
        @foreach($portfolioValue->portfoliotag as $tagVal)
            <span class="btnremadd" id="TagRemove{{$tagVal->id}}">
              {{$tagVal->tag}} 
              <a href="javascript:void(0)" class="DeleteTag{{$tagVal->id}} imagetagdeleteconfirm"  data-toggle="confirmation" data-title="are you sure?" data-imagetagid="{{$tagVal->id }}"> <i class="fal fa-times"></i></a>
            </span>
        @endforeach
        @endif
      </div>
      <span class="AddTag" onclick="addTag('{{$portfolioValue->id}}')" id="{{$portfolioValue->id}}">Add Tag <i class="fal fa-plus"></i></span> 
      <span id="tagText_{{$portfolioValue->id}}" class="hidden tagtextltbox">
        <input type="" data-attid="{{$portfolioValue->id}}"  name="tag" id="tagval_{{$portfolioValue->id}}" class="tagstore{{$portfolioValue->id}} tagSave">
      </span>

      </div>
    </div>  
    <div class="gelimgleftbox">       

      <ul id="gelimgleftbox{{$portfolioValue->id}}" class="gelimgleftboxGallery">
        @if(count($portfolioValue['performermedia'])>0)       
        @foreach($portfolioValue['performermedia'] as $key=>$mediaValue)         
        @if($key <=6)
      

         @if($mediaValue->media_type == 'video') 
          <li class="videocpbox"  id="portfolioMedia{{$mediaValue->id}}">
          <span  data-toggle="confirmation" data-title="are you sure?" data-imageid="{{$mediaValue->id}}"  class="deleteimageconfirm deleteimgbox"><i class="fas fa-trash"></i></span>
          <span onclick="portfolioshowVideo('{{$mediaValue->id}}')" >
          <img src="{{asset('assets/images/video.png')}}" alt="">
          </span>
        </li> 
        @else
          <li id="portfolioMedia{{$mediaValue->id}}">
            <span data-toggle="confirmation" data-title="are you sure?" data-imageid="{{$mediaValue->id}}"  class="deleteimageconfirm deleteimgbox"><i class="fas fa-trash"></i></span>
            <a href="{{$mediaValue->image_url}}">
              <img src="{{$mediaValue->image_url}}" alt="">
            </a>
          </li> 
        @endif
        
        @endif
        @endforeach
        @else
        <div class="gelimgleftbox">        
          <div class="noprojAvailable">
           <p>No Image</p>
          </div>
        </div>
        @endif
         </ul>
         @if(count($portfolioValue['performermedia']) > 7) 
          <div class="viewgallbox">
           @if($authtype=='performer')
           <a href="{{route('performers.view.gallery',['id'=>$portfolioValue->id])}}">View<br>Gallery<br>
            <span>{{count($portfolioValue['performermedia'])}} Images </span>
          </a>   
          @else
          <a href="{{route('internal-staff.view.gallery',['id'=>$portfolioValue->id,'pid'=>$per_id])}}">View<br>Gallery<br>
            <span>{{count($portfolioValue['performermedia'])}} Images </span>
          </a>   
          @endif
            </div>         
         @endif
     
    </div>   
   

  
  </div>

@endif
@endforeach
<!-- //video listing -->
@foreach($portfolio_list as $portfolioValue)
  @if($portfolioValue->is_video == "yes")

  <div class="hedfullmotobox">
    <div class="allboxtitlesem">
      <div class="titleboxallcover">
        <h3>{{$portfolioValue['portfolio_title'] }}</h3>
        <div class="morebtneddil">                
                <div class="MenuSceinain">
                  <div class="menuheaderinmore">
                    <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
                    <div class="mainineropenbox_1">
                      <div class="mainineropenbox_2">
                        <div class="opensubmenua_1">
                          <a href="javascript:void(0)" id="{{$portfolioValue->id}}" class="videoAddTogallery " onclick="videoAddTogallery('{{$portfolioValue->id}}')">Add </a>
                        </div> 
                      
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
        <div class="imgvidaodbox">
          <div id="portfolio{{$portfolioValue->id}}">
            @if(count($portfolioValue->portfoliotag))
            @foreach($portfolioValue->portfoliotag as $tagVal)
            <span class="btnremadd" id="TagRemove{{$tagVal->id}}">
              {{$tagVal->tag}} 
              <a href="javascript:void(0)" onclick="tagDelete('{{$tagVal->id }}')"> <i class="fal fa-times"></i></a>
            </span>
            @endforeach
            @endif
          </div>
          <span class="AddTag" onclick="addTag({{$portfolioValue->id}})" id="{{$portfolioValue->id}}">Add Tag <i class="fal fa-plus"></i></span> 
          <span id="tagText_{{$portfolioValue->id}}" class="hidden tagtextltbox">
            <input type="" data-attid="{{$portfolioValue->id}}"  name="tag" id="tagval_{{$portfolioValue->id}}" class="tagstore{{$portfolioValue->id}} tagSave">
          </span>
        </div>
      <!--   <div class="editboxinerpart">
          <a href="javascript:void(0)" id="{{$portfolioValue->id}}" class="videoAddTogallery"><i class="far fa-plus"></i></a>

          <a href="javascript:void(0)"><i class="far fa-edit"></i></a>
        </div> -->
      </div> 
      <div class="audiorealboxp"> 
        <div class="videoplaybtnbox"> 
          @if(count($portfolioValue['performermedia']))        
          @foreach($portfolioValue['performermedia'] as $mediaValue)         
          <!-- <div class="btnplaybox" id="portfolioMedia{{$mediaValue->id}}">
            <a href="javascript:void(0)"  >

              <div class="playiconbtn"><i class="fas fa-play"></i></div> 
              <span class="medtitleleft">{{$mediaValue->media_title}}</span> 
              <span class="dateformright">{{$portfolioValue->created_at}}</span>              
            </a>
            <div class="rigtmorebtnboxmid">           
              <div class="morebtneddil">                
                <div class="MenuSceinain">
                  <div class="menuheaderinmore">
                    <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
                    <div class="mainineropenbox_1">
                      <div class="mainineropenbox_2">
                         <div class="opensubmenua_1">
                          <a href="javascript:void(0)"  data-toggle="confirmation" data-title="are you sure?" data-videoid="{{$mediaValue->id}}"  class="deletevideoconfirm">Delete</a>
                        </div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
           
          </div> -->
          <!-- audio listing -->
          <div class="audioboxplbox" id="audiodelete{{$mediaValue->id}}">
            <div class="audioboxpcoverbox">
              <audio id="music" preload="true">
                <source src="{{$mediaValue->image_url}}">                
                </audio>
                <div id="audioplayer" class="audioplayerbox">
                  <button id="pButton" class="play"></button>
                  <div id="timeline" class="timelinebox">    
                    <div id="playhead"></div>
                  </div>
                </div>
                <a href="javascript:void(0)"  class="audioDeleteconfirm"  data-toggle="confirmation" data-title="are you sure?" data-audioid="{{$mediaValue->id}}">
                  <i class="fal fa-trash"></i>
                </a> 
              </div>
            </div>
     @endforeach
            @else
            <div class="gelimgleftbox">  
              <div class="noprojAvailable">
               <p>No Audio</p>
              </div>
            </div>
            @endif
          </div>

        </div>   
      </div>
    </div>
@endif
@endforeach
<div class="addbtnbotbox internalStaffAction" >
    <button  data-toggle="modal" data-target="#myModal"><i class="far fa-plus"></i>
      New Media Gallery
    </button>
  </div>
</div>
@else
<div class="hedfullmotobox">
  <div class="gelimgleftbox">        
    <p>No Portfolio available</p>
  </div>
</div>
@endif

</div>

<!-- image upload model -->
<div class="modal fade newgallerymod" id="myModal" role="dialog">
 <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-body">
       <div class="GalleryBoxinerdeta">        
        <form method="post" action="" enctype="multipart/form-data" id="portfolioGallery"> 
         @csrf    
        
        <div class="titlandclobtn">
         <h3>New Gallery</h3>
         <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
       </div> 
        <!-- <div class="imguplodvideo"> 
          <div class="seletcustboxpart2"> 
            <select id="select_upload" name="is_video">
              <option value="ImageUpload">Image Upload</option>
              <option value="VideoUpload">Video Upload</option>
            </select>
          </div>
        </div> -->
       <div class="form-group inputbixin">
         <input type="text" class="form-control" id="portfolio_titlemodel" name="portfolio_title" placeholder="Title Gallery">
       </div>
       <div class="form-group textarbox">
         <textarea class="form-control" id="" name="description" placeholder="Add a description"></textarea>
       </div>
       <div class="dropboxzoniner">        

        <div id="Image_upload_dropzone">
          <!-- image or video both upload dropzone --> 
          <div method="post" action="{{route('performers.store.portfolio.images')}}" enctype="multipart/form-data" 
          class="dropzone" id="dropzone">          
        </div>
      </div>

      <!-- <div id="video_upload_dropzone">        
        <div method="post" action="{{route('performers.store.portfolio.video')}}" enctype="multipart/form-data"
        class="dropzone" id="Videodropzone">          
      </div>
    </div> -->
  </div>
    <div class="tagandimgtitle">
        <div class="leftimgtitlebox">
          <div class="form-group">
              <input type="text" class="form-control" id="media_title" name="media_title" placeholder="Image Title">
          </div>
        </div>
        <div class="rightaddtagboxset">
            <div class="boxTags">
                <div class="taglist">                  
                </div>
                <span class="addTagCreate">Add Tag <i class="fal fa-plus"></i></span>
            </div>            
        </div>
    </div>
</div>
<div class="imgaddedboxi">

</div>
<div class="submitbtnbox">
<input type="button" id="addportfolio_image" name="" value="submit" class="btn btn-primary">
<!-- <input type="button" id="addportfolio_video" name="" value="submit" class="btn btn-primary"> -->
</div>
</form>
</div>
</div>
</div>
</div>

<!-- video show model -->
<div class="modal fade newgalrymodvideo " id="videoDispalyModal" role="dialog">
 <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-body">
       <div class="Galleryvideoboxinr">        

        <div class="titlandclobtn">
         <h3>video</h3>
         <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>         
       </div>      
       <div class="video_dispay" >
        
         
        </div>    
      </div>
    </div>
  </div>
</div>
</div>
<!-- add image modal -->
 <div class="modal fade newgallerymod" id="addImage" role="dialog">
 <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-body">
       <div class="GalleryBoxinerdeta">        
        <form method="post" action="" enctype="multipart/form-data" id="portfolioaddImage"> 
         @csrf    
        
        <div class="titlandclobtn">
         <h3>Add Media</h3>
         <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
       </div>      
       <div class="form-group">
         <input type="hidden" class="form-control" id="portfolio_id" name="portfolio_id" >
       </div>

       <div class="dropboxzoniner">        

        <div id="Image_upload_dropzone">
          <!-- image or video both upload dropzone --> 
          <div method="post" action="{{route('performers.store.portfolio.images')}}" enctype="multipart/form-data" 
          class="dropzone" id="AddImagedropzone">          
        </div>
      </div>
    </div>
 <div class="tagandimgtitle">
        <div class="leftimgtitlebox">
          <div class="form-group">
              <input type="text" class="form-control" id="media_title" name="media_title" placeholder="Image Title">
          </div>
        </div>
        <div class="rightaddtagboxset">
            <div class="boxTags">
                <div class="taglist">                  
                </div>
                <span class="addTagCreate">Add Tag <i class="fal fa-plus"></i></span>
            </div>            
        </div>
    </div>
</div>
<div class="imgaddedboxi">

</div>
<div class="submitbtnbox">
<input type="button" id="portfolio_image_add" name="" value="submit" class="btn btn-primary">
</div>
</form>
</div>
</div>
</div>
</div>


<!-- Add video modal -->
 <div class="modal fade newgallerymod" id="addVideo" role="dialog">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-body">
         <div class="GalleryBoxinerdeta">        
          <form method="post" action="" enctype="multipart/form-data" id="portfolioaddVideo"> 
           @csrf 
           <div class="titlandclobtn">
             <h3>Add Audio</h3>
             <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
           </div>      
           <div class="form-group">
             <input type="hidden" class="form-control" id="video_portfolio_id" name="portfolio_id" >
           </div>

           <div class="dropboxzoniner">        

            <div id="Video_upload_dropzone">
              <!-- image upload dropzone --> 
              <div method="post" action="{{route('performers.store.portfolio.video')}}" enctype="multipart/form-data" 
              class="dropzone" id="AddVideodropzone">          
            </div>
          </div>
        </div>
        <div class="form-group imgtitleboz">
         <input type="text" class="form-control" id="media_title" name="media_title" placeholder="Audio Title">
       </div>
     </div>
     <div class="imgaddedboxi">

     </div>
     <div class="submitbtnbox">
      <input type="button" id="portfolio_video_add" name="" value="submit" class="btn btn-primary">
    </div>
  </form>
</div>
</div>
</div>
</div>


<!-- dummy audio div  -->
<div class="audioboxplbox hidden" >
  <div class="audioboxpcoverbox">
    <audio id="music" preload="true">
      <source >                
      </audio>
      <div id="audioplayer" class="audioplayerbox">
        <button id="pButton" class="play"></button>
        <div id="timeline" class="timelinebox">    
          <div id="playhead"></div>
        </div>
      </div>
      <a href="javascript:void(0)"  class="audioDeleteconfirm"  data-toggle="confirmation" data-title="are you sure?" >
        <i class="fal fa-trash"></i>
      </a> 
    </div>
  </div>
 @endsection 
@section('scripts')
<script type="text/javascript">

  $(document).ready(function(){
    /** ADD TAG IN CREATE PROFILO **/
    $('.addTagCreate').on('click',function(){
        var tag = $('.inputTag').val();
        if (tag != "") {
            $(this).after('<span class="boxInputTag"><input type="text" data-status="tag" id="tag0" data-id="0" class="inputTag"></span>');

            $(".inputTag").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "{{route('performers.portfolio.getTagsList')}}",
                        type:'GET',
                        data: {
                            tags: request.term,
                            //id:$(this.element.get(0)).attr('data-attid')
                        },
                        success: function( data ) {
                            if(data.status == true){
                                if(data.html.length > 0){
                                    response(data.html);
                                    response($.map(data.html, function (el) {

                                    return {
                                        label: el.tag,
                                        id: el.id,
                                        //protfolio_id: data.protfolio_id
                                     };
                                   }));
                                } else {
                                    $('.inputTag').val('');
                                }

                            }
                        }
                    });
                },select: function( event, ui ) {
                    event.preventDefault();
                    $(".taglist").append('<span class="tagRemove" >'+ui.item.label+'<a href="javascript:void(0)" class="thistagDelete"><i class="fal fa-times"></i><input type="hidden" value="'+ui.item.label+'" name="tags[]"></a></span>');
                    $('.boxInputTag').remove();
                }
            });
        }  
    })

    // $(document).on('click','.tagRemove',function(){
    //     $(this).remove();
    // })
    /** END TAG IN CREATE PROFILO **/

    $("#select_upload").select2();
     
     $("#video_upload_dropzone").hide();
    
   $('select').on('change', function() {
       if(this.value == "VideoUpload")       {
        $("#video_upload_dropzone").show();
        $("#Image_upload_dropzone").hide();
        
        $("#media_title").attr("placeholder", "video title");
       }else       {
          $("#video_upload_dropzone").hide();
          $("#Image_upload_dropzone").show();
          $("#addportfolio_image").show();
          $("#media_title").attr("placeholder", "Image title");
       }
    });
  });
  
  // update code video /image both upload
  var uploadedDocumentMap = {};
  Dropzone.options.dropzone =
  {
    maxFilesize: 12,
    renameFile: function(file) {
      var dt    = new Date();
      var time  = dt.getTime();
      return time+file.name;
    },
    type:"post",
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    acceptedFiles: ".jpeg,.jpg,.png,.gif ,video/*",
    addRemoveLinks: true,
    timeout: 5000,

    success: function(file, response) 
    {            
      console.log(response);
      $('#portfolioGallery').append('<input type="hidden" name="images[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
      uploadedDocumentMap[file.name] = response;              
    },
    error: function(file, response)
    {
      console.log(response);
      return false;
    }
  };


   //video upload dropzone
  var uploadedDocumentMap = {};
  Dropzone.options.Videodropzone =
  {
    maxFilesize: 100,
    renameFile: function(file) {
      var dt    = new Date();
      var time  = dt.getTime();
      return time+file.name;
    },
    type:"post",
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    acceptedFiles: "audio/*",
    addRemoveLinks: true,
    timeout: 5000,

    success: function(file, response) 
    {            
      console.log(response);
      $('#portfolioGallery').append('<input type="hidden" name="images[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
      uploadedDocumentMap[file.name] = response;              
    },
    error: function(file, response)
    {
      console.log(file);
      return false;
    }
  };


 $('#addportfolio_image').click(function(){
    
    var portfolioTitle = $("#portfolio_titlemodel").val();
    if(portfolioTitle == "")
    {
      toastr.error('Enter Portfolio Title');
      return false;
    }

    formdata = $('#portfolioGallery').serialize();
   // $(this).prop('disabled', true);    
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url:"{{route('performers.store.portfolio.media')}}",
      type:"POST",
      data:formdata,
      success: function(response)
      {
       if(response.status == true)
       {
        location.reload();
        toastr.success(response.message);
      }else{
        toastr.error(response.message);
        $(this).prop('disabled', false);

        
      }
    }
  });
  });


  $('#addPortfolio').click(function(){

    formdata = $('#portfolioGallery').serialize();
    
    $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
    $.ajax({
      url:'{{route('performers.store.portfolio.media')}}',
      type:"POST",
      data:formdata,
      success: function(response)
      {
       if(response.status == true)
       {
        location.reload();
        toastr.success(response.message);
      }else{
        toastr.error(response.message);
      }
    }
  });
  });


  //show video
  // $(".portfolioshowVideo").click(function(){

    function portfolioshowVideo(id)
    {
  id =id;

    $.ajax({
      url:"{{route('performers.portfolio.video.show')}}",
      type:"GET",
      data:{id:id},
      success:function(response){
        if(response.status==true){
          $(".video_dispay").html("");
          $("#videoDispalyModal").modal('show');
          $(".video_dispay").html('<video width="600" height="300" controls class=""><source  src="'+response.data.image_url+'" type="video/mp4">Your browser does not support the video tag.</video>'); 
          }
      },
    error: function(file, response)
    {     
    }
    });
  }
// });


    function imageAddTogallery(id)
    {
           
   id =id;
   $("#portfolio_id").val(id);
   $("#addImage").modal('show');
   }


 $("#portfolio_image_add").click(function(){
   
    formdata = $('#portfolioaddImage').serialize();   
    $(this).prop('disabled', true);
    $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   }); 
    $.ajax({
      url:"{{route('performers.add.portfolio.images')}}",
      type:"POST",
      data:formdata,
      success: function(response)
      {
       if(response.status == true)
       {
        location.reload();
        toastr.success(response.message);
      }else{
        toastr.error(response.message);
      }
    }
  });
  });  

 //video add
 // $(".videoAddTogallery").click(function(){

  function videoAddTogallery(id)
  {
   id =id;
   $("#video_portfolio_id").val(id);
   $("#addVideo").modal('show');


   //video add btn
   $("#portfolio_video_add").click(function(){
   
    formdata = $('#portfolioaddVideo').serialize(); 
    $(this).prop('disabled', true);  
    $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
    $.ajax({
      url:"{{route('performers.add.portfolio.video')}}",
      type:"POST",
      data:formdata,
      success: function(response)
      {
       if(response.status == true)
       {
        location.reload();
        toastr.success(response.message);
      }else{
        toastr.error(response.message);
      }
    }
  });
  });   
     }


  toastr.options = {
         "preventDuplicates": true,
         "preventOpenDuplicates": true
         };      
var uploadedDocumentMap = {};
  Dropzone.options.AddImagedropzone =
  {
    maxFilesize: 100,
    renameFile: function(file) {
      var dt    = new Date();
      var time  = dt.getTime();
      return time+file.name;
    },
    type:"post",
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    acceptedFiles: ".jpeg,.jpg,.png,.gif,video/*",
    addRemoveLinks: true,
    timeout: 5000,

    success: function(file, response) 
    {            
      console.log(response);
      $('#portfolioaddImage').append('<input type="hidden" name="images[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
      uploadedDocumentMap[file.name] = response;              
    },
    error: function(file, response)
    {
      console.log(file);
      return false;
    }
  };



  //add video dropzone code
  var uploadedDocumentMap = {};
  Dropzone.options.AddVideodropzone =
  {
    maxFilesize: 100,
    renameFile: function(file) {
      var dt    = new Date();
      var time  = dt.getTime();
      return time+file.name;
    },
    type:"post",
    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    acceptedFiles: "audio/*",
    addRemoveLinks: true,
    timeout: 5000,

    success: function(file, response) 
    {            
      console.log(response);
      $('#portfolioaddVideo').append('<input type="hidden" name="videos[]" value="' + response.data.name + '"> <br> <input type="hidden" name="type[]" value="' + response.data.type + '">  ');
      uploadedDocumentMap[file.name] = response;              
    },
    error: function(file, response)
    {
      console.log(file);
      return false;
    }
  };

 
 function addTag(p_id)
 {
  
  portfolio_id =p_id;
  tag ='#tagval_'+p_id;
  id = "#tagText_"+p_id;
  tag = ".tagstore"+p_id;  
  $(id).removeClass('hidden');    
 }

// $(".AddTag").click(function(){

//   portfolio_id =$(this).attr('id');
//   tag ='#tagval_'+$(this).attr('id');
//   id = "#tagText_"+$(this).attr('id');
//   tag = ".tagstore"+$(this).attr('id');
  
//   $(id).removeClass('hidden');  

 

// });
// $(document).ready(function(){
// $('body').on('focusout','.tagsave',function(){

//   alert("DSD");
// });
// });
// $(document).on("focusout","td.edit input",function(){
//    alert("finally bye");
// });


//$(document).on('focusout','.tagSave',function(){

//$('body .tagSave').focusout(function(){
    function storeTags(id,tag){
        var tag_val=tag;

        if(tag_val == ""){
            $(id).addClass('hidden');  
            return false;
        }

        var portfolio_id =id;      

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url:"{{route('performers.portfolio.addtag')}}",
            type:"post",
            data:{portfolio_id:portfolio_id,tag:tag_val},
            success:function(response){
                if(response.status == true)
                {
                    $("#portfolio"+portfolio_id).append('<span class="btnremadd" id="TagRemove'+response.data.id+'">'+response.data.tag+'<a href="javascript:void(0)" class="tagDelete DeleteTag'+response.data.id+' imagetagdeleteconfirm" data-toggle="confirmation" data-title="are you sure?" data-imagetagid='+response.data.id+'> <i class="fal fa-times"></i></a></span>');
                    toastr.success(response.message);
                    $(tag).val("");
                    $(id).addClass('hidden');
                    $('.tagSave').val('');
                    $('.tagtextltbox').addClass('hidden');
                    $('.imagetagdeleteconfirm[data-toggle=confirmation]').confirmation({
                            rootSelector: '[data-toggle=confirmation]',
                            container: 'body',
                            onConfirm: function() {
                               tagDelete($(this).data('imagetagid'));
                            },
                          });
                }
            },
            error: function(file, response){
                console.log(file);
                return false;
            }
        });
    }


$(document).ready(function(){
    $("#portfolioSearch").keyup(function(){
    search = $(this).val();
    get_id = $("#get_id").val();
        $.ajax({
        url:"{{route('performers.portfolio.search')}}",
        type:'GET',
        data:{search:search,get_id:get_id},
        success:function(data)
        {
          if(data.status == true){        
            $("#defaultDisplayDiv").html(data.html);
             $('.gelimgleftboxGallery').imageview();
             $(".tagSave").autocomplete({
                 //source: availableTags
                 source: function(request, response) {
                     $.ajax({
                         url: "{{route('performers.portfolio.getTags')}}",
                         type:'GET',
                         data: {
                             tags: request.term,
                             id:$(this.element.get(0)).attr('data-attid')
                         },
                         success: function( data ) {
                             if(data.status == true){
                                 if(data.html.length > 0){
                                     //response(data.html);
                                     response($.map(data.html, function (el) {

                                      return {
                                        label: el.tag,
                                        id: el.id,
                                        protfolio_id: data.protfolio_id
                                      };
                                    }));
                                 } else {
                                   //  $('.tagSave').val('');
                                       var result = [{ label: "no results", value: response.term }];
                                   response(result);
                                 }

                             }
                         }
                     });
                 },
             change: function (event, ui) {
                 if (ui.item == null){ 
                  //here is null if entered value is not match in suggestion list
                    //toastr.warning("click on add Agent if you want add new Value");
                     $(this).val((ui.item ? ui.item.id : ""));
                 }
             },
                 select: function( event, ui ) {
                     event.preventDefault();
                    // console.log();
                    // var id = $(this.element.get(0)).attr('data-attid');
                    var label = ui.item.label;
                                if (label === "no results") {

                                }
                                else{
                     $('.tagSave').val(ui.item.label);
                     var id = ui.item.protfolio_id;
                     var tag = ui.item.label;
                     console.log(ui.item)
                     storeTags(id,tag);
                   }
                 }
             });
             $('.deleteimageconfirm[data-toggle=confirmation]').confirmation({
                      rootSelector: '[data-toggle=confirmation]',
                      container: 'body',
                      onConfirm: function() {

                         portfolioMediaDelete($(this).data('imageid'));
                         
                      },
                    });
             $('.tagSave').focusout(function(){
               if(jQuery(this).val()==''){
               jQuery(this).parent('.tagtextltbox').addClass('hidden');
               }
             });
             $('.imagetagdeleteconfirm[data-toggle=confirmation]').confirmation({
                     rootSelector: '[data-toggle=confirmation]',
                     container: 'body',
                     onConfirm: function() {
                        tagDelete($(this).data('imagetagid'));
                     },
                   });
            }
        }
      });
          
  });
});

$('.tagSave').focusout(function(){
  if(jQuery(this).val()==''){
  jQuery(this).parent('.tagtextltbox').addClass('hidden');
  }
});


function tagDelete(id)
{ 
  
  $("#TagRemove"+id).remove();
    $.ajax({
      url:"{{route('performers.portfolio.removetag')}}",
      type:'get',
      data:{id:id},
      success:function(data){

        if(data.status == true)
        {   
          toastr.success(data.message);     

        }      
      }
    });
}


function portfolioMediaDelete(id)
{  
  $("#portfolioMedia"+id).remove();
  $.ajax({
         url:"{{route('performers.portfolio.removeMedia')}}",
        type:'get',
        data:{id:id},
        success:function(response){
            if(response.status == true)
            {
              toastr.success(response.message);                   
            }
        }

  });
  
}

//Delete Gallery
function DeleteGallery(id)
{
   $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
  $.ajax({
       url:"{{route('performers.portfolioGalleryRemove')}}",
       type:"post",
       data:{id:id},
       success:function(data)
       {
         if(data.status == true)
         {
            $("#galleryDelete"+id).remove();
            toastr.success(data.message);

         }
       }

  });

}




  $("#showGalleryModal").on("hidden.bs.modal", function () {
    location.reload();
  });

    /** auto complated tags ajax base **/
    $(".tagSave").autocomplete({
        //source: availableTags
        source: function(request, response) {
            $.ajax({
                url: "{{route('performers.portfolio.getTags')}}",
                type:'GET',
                data: {
                    tags: request.term,
                    id:$(this.element.get(0)).attr('data-attid')
                },
                success: function( data ) {
                    if(data.status == true){
                        if(data.html.length > 0){
                            //response(data.html);
                            response($.map(data.html, function (el) {

                             return {
                               label: el.tag,
                               id: el.id,
                               protfolio_id: data.protfolio_id
                             };
                           }));
                        } else {
                          //  $('.tagSave').val('');
                              var result = [{ label: "no results", value: response.term }];
                          response(result);
                        }

                    }
                }
            });
        },
    change: function (event, ui) {
        if (ui.item == null){ 
         //here is null if entered value is not match in suggestion list
           //toastr.warning("click on add Agent if you want add new Value");
            $(this).val((ui.item ? ui.item.id : ""));
        }
    },
        select: function( event, ui ) {
            event.preventDefault();
           // console.log();
           // var id = $(this.element.get(0)).attr('data-attid');
           var label = ui.item.label;
                       if (label === "no results") {

                       }
                       else{
            $('.tagSave').val(ui.item.label);
            var id = ui.item.protfolio_id;
            var tag = ui.item.label;
            console.log(ui.item)
            storeTags(id,tag);
          }
        }
    });
    $('.deleteimageconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {

                portfolioMediaDelete($(this).data('imageid'));
             },
           });
    $('.deletevideoconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
              portfolioMediaDelete($(this).data('videoid'));
             },
           });
     $('.imagetagdeleteconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
                tagDelete($(this).data('imagetagid'));
             },
           });
     $('.gallaryconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
                DeleteGallery($(this).data('imagetagid'));
             },
           });
     $('.audioDeleteconfirm[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
                portfolioMediaDelete($(this).data('audioid'));
                $("#audiodelete"+$(this).data('audioid')).remove();
             },
           });


    
  var music = document.getElementById('music'); // id for audio element

var duration = '0' // Duration of audio clip, calculated here for embedding purposes
var pButton = document.getElementById('pButton'); // play button
var playhead = document.getElementById('playhead'); // playhead
var timeline = document.getElementById('timeline'); // timeline

// timeline width adjusted for playhead
var timelineWidth = timeline.offsetWidth - playhead.offsetWidth;

// play button event listenter
pButton.addEventListener("click", play);

// timeupdate event listener
music.addEventListener("timeupdate", timeUpdate, false);

// makes timeline clickable
timeline.addEventListener("click", function(event) {
    moveplayhead(event);
    music.currentTime = duration * clickPercent(event);
}, false);

// returns click as decimal (.77) of the total timelineWidth
function clickPercent(event) {
    return (event.clientX - getPosition(timeline)) / timelineWidth;
}

// makes playhead draggable
playhead.addEventListener('mousedown', mouseDown, false);
window.addEventListener('mouseup', mouseUp, false);

// Boolean value so that audio position is updated only when the playhead is released
var onplayhead = false;

// mouseDown EventListener
function mouseDown() {
    onplayhead = true;
    window.addEventListener('mousemove', moveplayhead, true);
    music.removeEventListener('timeupdate', timeUpdate, false);
}

// mouseUp EventListener
// getting input from all mouse clicks
function mouseUp(event) {
    if (onplayhead == true) {
        moveplayhead(event);
        window.removeEventListener('mousemove', moveplayhead, true);
        // change current time
        music.currentTime = duration * clickPercent(event);
        music.addEventListener('timeupdate', timeUpdate, false);
    }
    onplayhead = false;
}
// mousemove EventListener
// Moves playhead as user drags
function moveplayhead(event) {
    var newMargLeft = event.clientX - getPosition(timeline);

    if (newMargLeft >= 0 && newMargLeft <= timelineWidth) {
        playhead.style.marginLeft = newMargLeft + "px";
    }
    if (newMargLeft < 0) {
        playhead.style.marginLeft = "0px";
    }
    if (newMargLeft > timelineWidth) {
        playhead.style.marginLeft = timelineWidth + "px";
    }
}

// timeUpdate
// Synchronizes playhead position with current point in audio
function timeUpdate() {
    var playPercent = timelineWidth * (music.currentTime / duration);
    playhead.style.marginLeft = playPercent + "px";
    if (music.currentTime == duration) {
        pButton.className = "";
        pButton.className = "play";
    }
}

//Play and Pause
function play() {
    // start music
    if (music.paused) {
        music.play();
        // remove play, add pause
        pButton.className = "";
        pButton.className = "pause";
    } else { // pause music
        music.pause();
        // remove pause, add play
        pButton.className = "";
        pButton.className = "play";
    }
}

// Gets audio file duration
music.addEventListener("canplaythrough", function() {
    duration = music.duration;
}, false);

// getPosition
// Returns elements left position relative to top-left of viewport
function getPosition(el) {
    return el.getBoundingClientRect().left;
}
$('.gelimgleftboxGallery').imageview(); 
</script>
<!-- <script src="{{ asset('assets/js/bootstrap-tagsinput.js')}}"></script> -->

@include('performers.commonPortfolio') 



@endsection