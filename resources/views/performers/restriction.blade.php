@extends('performers.layouts.app')
@section('content')
<div class="wctleftrightbox">
  @include('performers.layouts.sidebar')
  <div class="rightallditinwct">
    <div class="searchbarboxfilt">
      <div class="srchbox">        
        <div class="form-group">
          <i class="far fa-search"></i>
          <input type="text" class="form-control" id="" placeholder="Search">
        </div>
      </div>
      <div class="addandtagbox">
        <div class="addatribut">
         <!--  <a href="javascript:void(0)" type="button" data-toggle="modal" data-target="#myClosetModal"><i class="far fa-plus"></i> New Closet Item</a> -->
        </div>
        <div class="tagboxright">
          <a href="javascript:void(0)"><i class="far fa-tag"></i></a>
        </div>
      </div>
    </div>
    @include('performers.layouts.tabbar')
    <div class="topbartitlebox">
      <h3 class="titleboxiner">Restrictions</h3>  
    </div>
    <?php $i =0;
    $res= round(count($restriction)/2); 
    ?>
    

          @foreach($restriction as $restrict)
              <?php if($i==0 || $i == $res ){ ?>
              <div class="hacinuboxcheck">
                <?php } ?>
                      
              
                        <div class="cutmbocheck">
                          <label class="checkbox-container">{{$restrict->restriction_name}}
                            <input type="checkbox" name="{{$restrict->restriction_name}}" value="{{$restrict->id}}" class="restriction"
                                @if(isset($restrictionperformer))
                              @foreach($restrictionperformer as $restrictperformer) 
                            {{($restrict->id==$restrictperformer->restriction_id)?'checked':''}}
                                @endforeach
                                  @endif
                            >
                            <span class="checkmark"></span>
                          </label>
                        </div>

                        <?php if($res-1 == $i || $i == count($restriction)-1){ ?>
                          </div>
                           
                          <?php } $i++; ?>
                        @endforeach

                      <div class="hacinuboxcheck">
                        <h3 class="titleboxiner">Nudity</h3>
                         @foreach($nudity as $nud)
                        <div class="cutmbocheck">
                          <label class="checkbox-container"> {{$nud->nudity_name}}
                            <input type="checkbox" name="{{$nud->nudity_name}}" value="{{$nud->id}}" class="nudity"
                                @if(isset($nudityperformer))
                              @foreach($nudityperformer as $nudperformer) 
                            {{($nud->id==$nudperformer->nudity_id)?'checked':''}}
                                @endforeach
                                  @endif
                            >
                            <span class="checkmark"></span>
                          </label>
                        </div>
                        @endforeach
                      </div>
@endsection
@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
     toastr.options = {
            "preventDuplicates": true,
            "preventOpenDuplicates": true
            };
    //set initial state.
    $('.nudity').change(function() {
            id=$(this).val();
            
             $.ajax({
                type: "GET",
                url: "{{url('performers/addnudity')}}",
                data: {id:id},
                success: function (data) {
                        if(data.result=="deleted")
                     {
                        toastr.error("Nudity Removed Successfully");
                     } 
                     else{
                        toastr.success("Nudity Added Successfully");
                     }
                    }         
            });
        
        // $('#textbox1').val(this.checked);        
    });

    $('.restriction').change(function() {
            id=$(this).val();
             $.ajax({
                type: "GET",
                url: "{{url('performers/addrestriction')}}",
                data: {id:id},
                success: function (data) {
                     if(data.result=="deleted")
                     {
                        toastr.error("Restrictions Removed Successfully");
                     } 
                     else{
                        toastr.success("Restrictions Added Successfully");
                     }
                    }         
            });
        
        // $('#textbox1').val(this.checked);        
    });
});
</script>
@endsection