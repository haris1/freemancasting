 <a href="javascript:void(0)" class="redbox">
  <i class="far fa-angle-down"></i>
  <span class="moretext">
    @if($performerFlag)
    {{count($performerFlag)}}
    @endif
  Flags</span>
  <span class="lesstext">Hide</span>              
</a>
<div class="content">
  <div class="flagsaddboxin">
    <div class="flagsaddin_box1">
      <p><i class="far fa-flag"></i> Flags</p>
      <a href="javascript:void(0)" data-toggle="modal" class="addFlagModal" data-target="#addFlagModal"><i class="fal fa-plus" ></i> Add Flag</a>
    </div>
    @if($performer->performerFlag)
    @foreach($performerFlag as $flagval)
    
    <div class="flagsaddin_box2">
      <div class="latdanstubox1">
        <p>{{isset($flagval->flag->flag_name) ? $flagval->flag->flag_name : '-'}}</p>
      </div>
      <div class="latdanstubox2">
        <p>{{isset($flagval->created_at) ? $flagval->created_at : '-'}}</p>
      </div>
      <div class="latdanstubox3">
        <p>{{isset($flagval->note) ? $flagval->note : ''}}</p>
      </div>
      <div class="latdanstubox4">
        <a href="javascript:void(0)" class="deleteflag" data-toggle="confirmation" data-title="are you sure?" data-id="{{$flagval->id}}"><i class="fa fa-trash" aria-hidden="true"></i></a>
      </div>
    </div>
    @endforeach
    @endif
  </div>
</div>


<!-- Add Flag -->
<div class="modal fade editskinboxmodal" class="addFlagModal" id="addFlagModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="editsktoboxcover">
             <form method="post" enctype="multipart/form-data" id="flagSaveForm">
              @csrf          
              <div class="titlandclobtn2">
                <h3>Add Flag</h3>
                <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
              </div>

              <div class="enterskintonebox">
                 <div class="form-group">
                <div class="seletcustboxpart">
                        <select id="flagSelect" name="flag_id" class="selectField flagSelect">
                            <option></option>
                            @if($flag)
                            @foreach($flag as $val)
                            <option value="{{$val->id}}">{{$val->flag_name}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
              
              </div>
                
                <div class="form-group">
                  <input type="text" class="form-control"  name="note" placeholder="Note">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" value="{{$performer->full_name}}"  name="" readonly="">
                </div>
                <div class="form-group">
                  <input type="hidden" class="form-control" value="{{$performer->id}}"  name="performer_id" readonly="">
                  
                </div>
                <input type="button" name="" value="Add Flag" class="btn" onclick="FlagSave()">
              </div>              
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
   function flagId(Id){
      
    }
  </script>
  @if(Auth::guard('web')->check())  
  @section('scripts')
  @else
  @section('scripts1')
  @endif
  <script type="text/javascript">
   
    $('.deleteflag[data-toggle=confirmation]').confirmation({
             rootSelector: '[data-toggle=confirmation]',
             container: 'body',
             onConfirm: function() {
               
               var id = $(this).data('id');
               $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });
                   $.ajax({                         
                   url: "{{route('internal-staff.removeflag')}}", type: "POST",
                     data: {
                             id:id,
                           },
                     success: function (data) {
                             toastr.success(data.message);
                             location.reload();
                            }         
                 });
             },
           }); 
  </script>
  @endsection