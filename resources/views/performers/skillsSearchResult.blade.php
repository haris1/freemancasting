@if(count($search_result))
 @foreach($search_result   as $skill) 
      <div>    
      <div class="qsmsodetailbox" id="skillPart{{$skill->id}}">
              <div class="qsmso_titlebox">
                <h3 id="skillouterpart{{$skill->id}}">{{$skill->skill_title}}</h3>
                <div class="helpboxdcl">
                  <span class="tool" data-tip="{{isset($helpdesk->skills_help) ? $helpdesk->skills_help : '' }}" tabindex="1"><i class="fal fa-question-circle"></i></span>
                  
                </div>
                <div class="morebtneddil">            
                  <div class="MoreMenuScenes">
                    <div class="MenuSceinain">
                      <div class="menuheaderinmore">
                        @if(Auth::guard('web')->check())
                        <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>
                        <div class="mainineropenbox_1">
                          <div class="mainineropenbox_2">
                            <div class="opensubmenua_1">
                              <a href="javascript:void(0)" id="{{$skill->id}}" onclick="fieldId('{{$skill->id}}');">Add</a>
                            </div> 
                           
                            @if (!in_array($skill->skill_title, array('Qualifications','Sports','Music','Specialty Driver','Others'))) 
                            <div class="opensubmenua_1">
                              <a href="javascript:void(0)" onclick="editModel('{{$skill->id}}')">Edit</a>
                            </div>
                            <div class="opensubmenua_1">
                              <a href="javascript:void(0)"  data-toggle="confirmation" data-title="Are you sure?" data-titleid="{{$skill->id}}" href="javascript:void(0)" class="deleteskilltitle">Delete</a>
                            </div> 
                            @endif
                          </div> 
                        </div>
                        @endif
                      </div>
                    </div>
                  </div>            
                </div>
              </div>
              <div id="skillContent{{$skill->id}}" class="deletefield">
           
              @foreach($skill->skillFields as $section)
                <div class="inerdetaqsmobox">
                  <div class="inerdetaqsmo_left">
                    <input type="hidden" id="editskill_id{{$section->id}}" value="{{$section->skill_id}}">
                    <p class="pText{{$section->id}}">{{$section->field_name}}</p>
                    <input type="text" class="hidden SkillText{{$section->id}}"  value="{{$section->field_name}}" name="field_name" id="editfield_name{{$section->id}}">
                  </div>
                  <div class="inerdetaqsmo_right">
                    <p class="pText{{$section->id}}">{{$section->field_value}}</p>
                    <input type="text"  class="hidden SkillText{{$section->id}}" value="{{$section->field_value}}" name="field_value" id="editfield_value{{$section->id}}">

                    @if(Auth::guard('web')->check())
                  <a  class="edithidebox edithide{{$section->id}}"  data-toggle="confirmation" data-title="are you sure?" data-sectionid="{{$section->id}}" href="javascript:void(0)"><i class="far fa-trash"></i></a>
                  <a onclick="editSkill('{{$section->id}}')" class="edithidebox edithide{{$section->id}}" href="javascript:void(0)"><i class="far fa-edit"></i></a>
                  <a onclick="updateSkill('{{$section->id}}')" class="edithidebox SkillText{{$section->id}} hidden" href="javascript:void(0)"><i class="far fa-save"></i></a>
                  @endif
                  </div>
                </div>
              
            @endforeach   
            </div>  
                <div class="inerdetaqsmobox fieldForm" id="fieldForm{{$skill->id}}">
                  <div class="inerdetaqsmo_left">
                   
                    <p><input type="text" name="field_name" placeholder="Enter skill name" id="field_name{{$skill->id}}" class="field_name" ></p>
                  </div>
                  <div class="inerdetaqsmo_right">
                    <input type="text" name="field_value" placeholder="Enter skill value"  id="field_value{{$skill->id}}" class="field_value"> <a href="javascript:void(0)" class="{{$skill->id}} edithidebox"  onclick="saveSkillSection(this,'{{$skill->id}}');"><i class="far fa-save"></i></a>
                    
                  </div>

                </div>
              </div>
            </div>
  @endforeach  
@endif