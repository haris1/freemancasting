@extends('performers.layouts.app')
@section('content')
<div class="gallerypotosall">
  <div class="leftrighticontitl">
    <div class="topbarprfname">

     <a href="{{route('performers.skills')}}"> <i class="far fa-chevron-left"></i></a> <h3>{{$gallery['skill_title']}}</h3>
    </div>
    <div class="righteditiocn">
      <a href="javascript:void(0)" onclick="viewGallery('{{ $gallery['id'] }}','{{ $gallery['skill_title'] }}')"><i class="far fa-edit"></i></a>
    </div>
  </div>
  
  <div class="addvewimgbox">
    <div class="discriptionimg">
      <h3>{{ $gallery['description'] }}</h3>
      <a href="javascript:void(0)" class="btnAddSkillGallery"><i class="far fa-plus"></i> image</a>      
    </div>
    <div class="gridimgphotobox">

      <div class="grid" id="grid">
       @if($gallery['performermedia'])
       @foreach($gallery['performermedia'] as $mediaValue)        
       <div class="item" id="skillMedia{{$mediaValue->id}}">
        <a href="javascript:void(0)" onclick="skillMediaDelete('{{$mediaValue->id}}')"><i class="fas fa-trash"></i></a>
        <img src="{{$mediaValue->image_url}}" alt=""></div>
       @endforeach
       @endif
     </div>
   </div>
 </div>
</div>


@endsection
@section('scripts')
<script type="text/javascript">
    
  function skillMediaDelete(id)
{  
  $("#skillMedia"+id).remove();
  $.ajax({
         url:"{{route('performers.skills.removeMedia')}}",
        type:'get',
        data:{id:id},
        success:function(response){
            if(response.status == true)
            {
              toastr.success(response.message);                   
            }
        }

  });
  
}
</script>
@include('performers.commonSkill')

@endsection