@if(count($search_result))
@foreach($search_result as $closetValue)
    @if($closetValue->is_video == "no")
    <div class="hedfullmotobox">
      <div class="allboxtitlesem">
        <div class="titleboxallcover">
          <div class="titledivallbox"><h3> <a href="javascript:void(0)"  onclick="viewGallery('{{ $closetValue['id'] }}','{{ $closetValue['closet_title'] }}')" >{{$closetValue['closet_title'] }}</a></h3></div>
          <div class="helpboxdcl">
            <span class="tool" data-tip="{{isset($helpdesk->closet_help) ? $helpdesk->closet_help : '' }}" tabindex="1"><i class="fal fa-question-circle"></i></span>
            
          </div>
          <div class="morbtnmnubox">
            <div class="morebtneddil">                
              <div class="MenuSceinain">
                <div class="menuheaderinmore">
                  @if(Auth::guard('web')->check())
                  <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
                  <div class="mainineropenbox_1">
                    <div class="mainineropenbox_2">
                      <div class="opensubmenua_1">
                        <a href="javascript:void(0)" id="{{$closetValue->id}}"  class="imageAddTogallery" onclick="imageAddTogallery('{{$closetValue->id}}')">Add</a>
                      </div> 
                      <div class="opensubmenua_1">
                        <a href="javascript:void(0)"  onclick="viewGallery('{{ $closetValue['id'] }}','{{ $closetValue['closet_title'] }}')">Edit</a>
                      </div>
                    </div> 
                  </div>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="imgvidaodbox">
            <div id="closettag{{$closetValue->id}}">
              @if(count($closetValue->closettag))
              @foreach($closetValue->closettag as $tagVal)
              <span class="btnremadd" id="TagRemove{{$tagVal->id}}">
                {{$tagVal->tag}} 
                <a href="javascript:void(0)" class="DeleteTag{{$tagVal->id }} imagetagdeleteconfirm"  data-toggle="confirmation" data-title="are you sure?" data-imagetagid="{{$tagVal->id }}" > <i class="fal fa-times"></i></a>
              </span>
              @endforeach
              @endif
            </div>
            <span class="AddTag" onclick="addTag({{$closetValue->id}})" id="{{$closetValue->id}}">Add Tag <i class="fal fa-plus"></i></span> 
            <span id="tagText_{{$closetValue->id}}" class="hidden tagtextltbox">
              <input type="" data-attid="{{$closetValue->id}}"  name="tag" id="tagval_{{$closetValue->id}}" class="tagstore{{$closetValue->id}} tagSave">
            </span>
          </div>
        </div>
      </div>  
      <div class="gelimgleftbox">        
        <ul class="gelimgleftboxGallery">
          @if(count($closetValue['performermedia']))       
          @foreach($closetValue['performermedia'] as $key=>$mediaValue)         
          @if($key <=6)
          <li id="closetMedia{{$mediaValue->id}}">
            @if(Auth::guard('web')->check())
            <span class="deleteimageconfirm deleteimgbox" data-imageid="{{$mediaValue->id}}"  data-toggle="confirmation" data-title="are you sure?" ><i class="fas fa-trash"></i></span>
            @endif
            <a href="{{$mediaValue->image_url}}"><img src="{{$mediaValue->image_url}}" alt=""></a>
          </li> 
          @endif
          @endforeach
          @else
          <div class="gelimgleftbox">        
            <p>No Image</p>
          </div>
          @endif
          </ul>
      @if(count($closetValue['performermedia']) > 7)    
      <div class="viewgallbox">
        <a href="{{route('performers.view.closetgallery',['id'=>$closetValue->id])}}">View<br>Gallery<br>
      <span>
        {{count($closetValue['performermedia'])}} Images </span></a>   
      </div>
      @endif
    </div>
    @endif
     
      </div>
    @endforeach
    <!-- //video listing -->
    @foreach($search_result as $closetValue)
    @if($closetValue->is_video == "yes")
    <div class="hedfullmotobox">
      <div class="allboxtitlesem">
        <div class="titleboxallcover">
          <h3>{{$closetValue['closet_title'] }}</h3>
          <div class="morebtneddil">                
            <div class="MenuSceinain">
              <div class="menuheaderinmore">
                <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
                <div class="mainineropenbox_1">
                  <div class="mainineropenbox_2">
                    <div class="opensubmenua_1">
                      <a href="javascript:void(0)" id="{{$closetValue->id}}" class="videoAddTogallery" onclick="videoAddTogallery('{{$closetValue->id}}')" >Add </a>
                    </div> 

                  </div> 
                </div>
              </div>
            </div>
          </div>
          <div class="imgvidaodbox">
            <div id="closetMedia{{$closetValue->id}}">
              @if(count($closetValue->closettag))
              @foreach($closetValue->closettag as $tagVal)
              <span class="btnremadd" id="TagRemove{{$tagVal->id}}">
                {{$tagVal->tag}} 
                <a href="javascript:void(0)" onclick="tagDelete('{{$tagVal->id }}')"> <i class="fal fa-times"></i></a>
              </span>
              @endforeach
              @endif
            </div>
            <span class="AddTag" onclick="addTag({{$closetValue->id}})" id="{{$closetValue->id}}">Add Tag <i class="fal fa-plus"></i></span> 
            <span id="tagText_{{$closetValue->id}}" class="hidden tagtextltbox">
              <input type="" data-attid="{{$closetValue->id}}"  name="tag" id="tagval_{{$closetValue->id}}" class="tagstore{{$closetValue->id}} tagSave" >
            </span>
          </div>
      </div> 
      <div class="audiorealboxp"> 

        <div class="videoplaybtnbox"> 
          @if(count($closetValue['performermedia']))        
          @foreach($closetValue['performermedia'] as $mediaValue)         
          <div class="btnplaybox" id="closetMedia{{$mediaValue->id}}">
            <a href="javascript:void(0)" data-key="{{$mediaValue->id}}" onclick="showClosetVideo('{{$mediaValue->id}}')" class="showClosetVideo">
              <div class="playiconbtn"><i class="fas fa-play"></i></div> 
              <span class="medtitleleft">{{$mediaValue->media_title}}</span> 
              <span class="dateformright">{{$closetValue->created_at}}</span>              
            </a>
            <div class="rigtmorebtnboxmid">          
              <div class="morebtneddil">                
                <div class="MenuSceinain">
                  <div class="menuheaderinmore">
                    <div class="MoreImgSet"><i class="far fa-ellipsis-v"></i></div>                     
                    <div class="mainineropenbox_1">
                      <div class="mainineropenbox_2">
                       <div class="opensubmenua_1">
                        <a href="javascript:void(0)"  onclick="closetMediaDelete('{{$mediaValue->id}}')">Delete</a>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        @endforeach
        @else
        <div class="gelimgleftbox">        
          <p>Not Video</p>
        </div>
        @endif
      </div>
    </div>   
  </div>
</div>
@endif
@endforeach
@endif