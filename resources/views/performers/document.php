@extends('performers.layouts.app')
@section('content')
<div class="wctleftrightbox">
  @if($performer->applicationfor == "Group")
  @include('performers.layouts.groupSidebar')  
  @else
  @include('performers.layouts.sidebar')  
  @endif
     <div class="rightallditinwct">
   <!--  <div class="searchbarboxfilt">
      <div class="srchbox">        
        <div class="form-group">
          <i class="far fa-search"></i>
          <input type="text" class="form-control" id="skillSearch" placeholder="Search">
        </div>
      </div>
      <div class="addandtagbox">
        <div class="addatribut">
          <a href="javascript:void(0)" type="button" data-toggle="modal" data-target="#myDocumentsModal"><i class="far fa-plus"></i>New Document</a>
        </div>
        <div class="tagboxright">
          <a href="javascript:void(0)"><i class="far fa-tag"></i></a>
        </div>
      </div>
    </div> -->
    <div class="addflagshidbox">
      <div class="accordion-container">
        <div class="set hedtobox">
          <div class="srchbox hidden">        
            <div class="form-group">
              <i class="far fa-search"></i>
             <input type="text" class="form-control" id="" placeholder="Search">
            </div>
          </div>
      @if($authtype=='performer')     
     @else
     @include('performers.flag')
     @endif       
        </div>      
      </div>
    </div>
  <div class="topbarbotdivcover">
    @if($authtype=='performer')
    @include('performers.layouts.tabbar')
    @else
    @include('performers.layouts.internal-tabbar')
    @endif
  
    <div class="doctslistdit">
      <div class="doctslistdit_title">

        <h3>Documents List</h3>
      </div>
      <div class="errbrtext">
        <i class="far fa-info-circle"></i>
        <p>Please be sure to store these documents in an easy to find locations so that you can bring them to set. You will not be able to work without these documents. Do not upload them to this system in any capacity.</p>
      </div>
      <div class="dpnpiwsbox">
        <div class="dpnpiwsbox_partdiv">
       @foreach($document as $doc)
        
          <div class="dpnpiwsbox_inerfullw">
            <div class="lefttextdpmpiws">
              <p>{{ $doc->doument_name }}</p>
            </div>
     <!--        <div class="righttextdpmpiws">  
              <div class="custheckedbox Checkedgrn">
                <input class="styled-checkbox documentcheckbox" id="{{$doc->id}}" type="checkbox" value="{{$doc->id}}" checked>
                <label for="{{$doc->id}}">Checked</label>
              </div>
              <div class="custheckedbox Checkedred">
                <input class="styled-checkbox documentcheckbox" id="{{$doc->id}}" type="checkbox" value="{{$doc->id}}">
                <label for="{{$doc->id}}">Not Checked</label>
              </div>
            </div> -->
          </div>  
          
           
          @endforeach
         
        </div>
        <div class="dpnpiwsbox_partdiv">
          @foreach($documentna as $nadoc)
          <div class="dpnpiwsbox_inerfullw">
            <div class="lefttextdpmpiws">
              <p>{{ $nadoc->doument_name }}</p>
            </div>
            <div class="righttextdpmpiws">
              <div class="notaplibox"><i class="far fa-minus-square"></i> <p> Not applicable</p></div>
            </div>
          </div>
          @endforeach          
        
        </div>



      </div>
      

  </div>
  
</div>

  
</div>
</div>
</div>



@endsection

@section('scripts')

<script type="text/javascript">
    $('.documentcheckbox').on('change', function(){ 
        id=$(this).attr('id');
        if (this.checked) {
             $.ajax({

                type: "POST",
                url: "{{route('performers.adddocument')}}",
                headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                data: {
                        id:id
                      },
                success: function (data) {
                       // console.log(data);
                       toastr.success(data.message);
                      }         
            });
        }
        else{
           $.ajax({

              type: "POST",
              url: "{{route('performers.removedocument')}}",
              headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
              data: {
                      id:id
                    },
              success: function (data) {
                       toastr.success(data.message); 
                    }         
          });         
        }
    });
</script>
@include('performers.commonSkill')
@endsection