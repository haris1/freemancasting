@extends('performers.layouts.app')
@section('content')
<div class="wctleftrightbox">
 @if($performer->applicationfor == "Group")
  @include('performers.layouts.groupSidebar')  
  @else
  @include('performers.layouts.sidebar')  
  @endif
     <div class="rightallditinwct">
  <!--   <div class="searchbarboxfilt">
      <div class="srchbox">        
        <div class="form-group">
          <i class="far fa-search"></i>
          <input type="text" class="form-control" id="" placeholder="Search">
        </div>
      </div>
      <div class="addandtagbox">
         <div class="addatribut">
          <a href="javascript:void(0)"><i class="far fa-plus"></i>Add Attribute</a>
        </div> 
        <div class="tagboxright">
          <a href="javascript:void(0)"><i class="far fa-tag"></i></a>
     
        </div>
      </div>
    </div> -->
    <div class="addflagshidbox">
      <div class="accordion-container">
        <div class="set hedtobox">
          <div class="srchbox hidden">        
            <div class="form-group">
              <i class="far fa-search"></i>
             <input type="text" class="form-control" id="" placeholder="Search">
            </div>
          </div>
      @if($authtype=='performer')     
     @else
     @include('performers.flag')
     @endif      
        </div>      
      </div>
    </div>
  <div class="topbarbotdivcover">
  @if($authtype=='performer')
  @include('performers.layouts.tabbar')
  @else
  @include('performers.layouts.internal-tabbar')
  @endif
  <div class="Appeaceboxalldeta">
      
      <form id="searchForm" method="post">
      

      <div class="measumtbox">
        <div class="topbartitlebox">
          <h3>Measurements</h3>      
             <a href="javascript:void(0)" id="editAppearance" onclick="editAppearance()"><i class="far fa-edit"></i></a>
             @if($authtype=='performer')
             <a href="javascript:void(0)" id="updateAppearance" class="hidden" onclick="updateAppearance()"><i class="far fa-save"></i></a>    
             @endif
        </div>
        <!-- <div class="bodyditbox_left">

          <img src="{{asset('assets/images/userimg1.png') }}" alt="">
          <img src="{{asset('assets/images/userimg2.png') }}" alt="">
        </div> -->      
        <div class="bodyditbox_right">
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Height <span> (cm)</span></p>
            </div>
            <div class="egchhhsboxlr_right">
              <p  class="displayField">{{isset($appearance->height) ? $appearance->height : '-'}}

              </p>
              <div class="appearanceFormat">                
                <input type="text" name="height" class="editField hidden" value="{{isset($appearance->height)?$appearance->height:''}}"  onkeypress="return IsNumeric(event,this.value);"/>
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p >Weight <span> (lbs)</span></p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->weight) ? str_replace("lbs","",$appearance->weight) : '-'}} </p>
              <div class="appearanceFormat">                
                <input type="text" name="weight" class="editField hidden" value="{{isset($appearance->weight)?$appearance->weight:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>chest</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->chest) ? $appearance->chest : '-'}}</p>
              <div class="appearanceFormat">                
                <input type="text" name="chest" class="editField hidden" value="{{isset($appearance->chest)?$appearance->chest:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Waist</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->waist) ? $appearance->waist : '-'}}</p>
              <div class="appearanceFormat">                
                <input type="text" name="waist" class="editField hidden" value="{{isset($appearance->waist)?$appearance->waist:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Hips</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p  class="displayField">{{isset($appearance->hips) ? $appearance->hips : '-'}}</p>
              <div class="appearanceFormat">                
                <input type="text" name="hips" class="editField hidden" value="{{isset($appearance->hips)?$appearance->hips:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Dress</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->dress) ? $appearance->dress : '-'}}</p>
              <div class="appearanceFormat">                
                <input type="text" name="dress" class="editField hidden" value="{{isset($appearance->dress)?$appearance->dress:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Collar</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->collar) ? $appearance->collar : '-'}}</p>
              <div class="appearanceFormat">                
                <input type="text" name="collar" class="editField hidden" value="{{isset($appearance->collar) ? $appearance->collar: ''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Sleeve</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->sleeve) ? $appearance->sleeve : '-'}}</p>
              <div class="appearanceFormat">                
                <input type="text" name="sleeve" class="editField hidden" value="{{isset($appearance->sleeve) ? $appearance->sleeve : ''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Pant</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->pant) ? $appearance->pant : '-'}}</p>
              <div class="appearanceFormat">                
                <input type="text" name="pant" class="editField hidden" value="{{isset($appearance->pant)?$appearance->pant:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Jeans size</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->jeans) ? $appearance->jeans : '-'}}</p>
              <div class="appearanceFormat">                
                <input type="text" name="jeans" class="editField hidden" value="{{isset($appearance->jeans)?$appearance->jeans:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
        </div>
        <div class="bodyditbox_right">
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Inseam</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p  class="displayField">{{isset($appearance->inseam) ? $appearance->inseam : '-'}}</p>
              <div class="appearanceFormat">                
                 <input type="text" name="inseam" class="editField hidden" value="{{isset($appearance->inseam)?$appearance->inseam:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Hat</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->hat) ? $appearance->hat : '-'}}</p>
              <div class="appearanceFormat">                
                <input type="text" name="hat" class="editField hidden" value="{{isset($appearance->hat)?$appearance->hat:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Ring</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->ring) ? $appearance->ring : '-'}}</p>
              <div class="appearanceFormat">                
                <input type="text" name="ring" class="editField hidden" value="{{isset($appearance->ring)?$appearance->ring:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Bust</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p  class="displayField">{{isset($appearance->bust) ? $appearance->bust : '-'}}</p>
              <div class="appearanceFormat">                
               <input type="text" name="bust" class="editField hidden" value="{{isset($appearance->bust)?$appearance->bust:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Shoes</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->shoes) ? $appearance->shoes : '-'}}</p>
              <div class="appearanceFormat">                
                <input type="text" name="shoes" class="editField hidden" value="{{isset($appearance->shoes)?$appearance->shoes:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Jacket</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->jacket) ? $appearance->jacket : '-'}}</p>
              <div class="appearanceFormat">                
                <input type="text" name="jacket" class="editField hidden" value="{{isset($appearance->jacket)?$appearance->jacket:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
         <div class="teodetacoverbox">
           <div class="egchhhsboxlr_left">
             <p>Gloves Size</p>
           </div>
           <div class="egchhhsboxlr_right">
             <p class="displayField">{{isset($appearance->gloves) ? $appearance->gloves : '-'}}</p>
             <div class="appearanceFormat editField hidden">
               <div class="seletcustboxpart2">
                
                 <select class="selectField" data-placeholder="Select gloves" name="gloves" id="gloves">
                   <option></option>
                   <option value="XXS" {{isset($appearance)&&($appearance->gloves=="XXS")?'Selected':''}}>XXS</option>
                   <option value="XS" {{isset($appearance)&&($appearance->gloves=="XS")?'Selected':''}}>XS</option>
                   <option value="S" {{isset($appearance)&&($appearance->gloves=="S")?'Selected':''}}>S</option>
                   <option value="M" {{isset($appearance)&&($appearance->gloves=="M")?'Selected':''}}>M</option>
                   <option value="L" {{isset($appearance)&&($appearance->gloves=="L")?'Selected':''}}>L</option>
                   <option value="XL" {{isset($appearance)&&($appearance->gloves=="XL")?'Selected':''}}>XL</option>
                 </select>
                 </div>
             </div>
           </div>
         </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Child Size</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p  class="displayField">{{isset($appearance->child_size) ? $appearance->child_size : '-'}}</p>
              <div class="appearanceFormat">                
                <input type="text" name="child_size" class="editField hidden" value="{{isset($appearance->child_size)?$appearance->child_size:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Infant Size</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p  class="displayField">{{isset($appearance->infant_size) ? $appearance->infant_size : '-'}}</p>
              <div class="appearanceFormat">                
               <input type="text" name="infant_size" class="editField hidden" value="{{isset($appearance->infant_size)?$appearance->infant_size:''}}" onkeypress="return IsNumeric(event,this.value);">
              </div>
            </div>
          </div>
        </div> 
      </div>
      <div class="userapperboxiner">
        <div class="topbartitlebox">
          <h3>Appearance</h3>
          
       
        </div>
        <div class="egchhhsboxlr">
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Eyes</p>
              
            </div>
          
            <div class="egchhhsboxlr_right">
              <p  class="displayField">{{isset($appearance->eyes->eyes) ? $appearance->eyes->eyes : '-'}}</p>
              <div class="appearanceFormat editField hidden">
              <div class="seletcustboxpart2">
                <select required class="selectField" id="Eyes" name="eyes_id">
                  <option></option>
                  @foreach ($Eyes as $Eye)
                    <option value="{{ $Eye->id }}" {{isset($appearance)&&($Eye->id==$appearance->eyes_id)?'Selected':''}}>{{ $Eye->eyes }}</option>
                  @endforeach
                </select>
              </div>
             </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Glasses</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->glasses) ? $appearance->glasses : '-'}}</p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                  <select class="selectField" id="Glasses" name="glasses">
                    <option></option>
                    <option value="Yes" {{isset($appearance)&&($appearance->glasses=="Yes")?'Selected':''}}>Yes</option>
                    <option value="No" {{isset($appearance)&&($appearance->glasses=="No")?'Selected':''}}>No</option>
                  </select>
                </div>  
              </div>  
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Contacts</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->contacts) ? $appearance->contacts : '-'}}</p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                  <select class="selectField" id="Contacts" name="contacts">
                     <option></option>
                     <option value="Yes" {{isset($appearance)&&($appearance->contacts=="Yes")?'Selected':''}}>Yes</option>
                     <option value="No" {{isset($appearance)&&($appearance->contacts=="No")?'Selected':''}}>No</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Hair Length</p>
            </div>           
            <div class="egchhhsboxlr_right">
              <p class="displayField">
                {{isset($appearance->hairlength->hair_length) ? $appearance->hairlength->hair_length : '-'}}
              </p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                <select required class="selectField" id="HairLength" name="hair_length_id">
                  <option></option>
                  @foreach ($HairLength as $HairLength)
                    <option value="{{ $HairLength->id }}" {{isset($appearance)&&($HairLength->id==$appearance->hair_length_id)?'Selected':''}}>{{ $HairLength->hair_length }}</option>
                  @endforeach
                </select>
                </div>
              </div>
            </div>
          </div>



          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Hair Colour</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->haircolor->hair_color) ? $appearance->haircolor->hair_color : '-'}}</p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                  <select class="selectField" id="HairColor" name="hair_color_id">
                    <option></option>
                    @foreach ($HairColor as $HairColor)

                      <option value="{{ $HairColor->id }}" 
                        {{isset($appearance)&&($HairColor->id==$appearance->hair_color_id)?'Selected':''}}
                        >{{$HairColor->hair_color}}</option>
                    }
                    }
                    @endforeach  
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Body Traits</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->bodytrait->body_trait) ? $appearance->bodytrait->body_trait : '-'}}</p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                  <select class="custselectbox1" id="bodytrait" name="body_trait_id">
                     <option value=""></option>
                     @foreach ($bodytrait as $val) 
                     <option value="{{$val->id}}" {{isset($appearance)&&($val->id==$appearance->body_trait_id)?'Selected':''}}>{{$val->body_trait}}</option>
                     @endforeach
                 </select>
                  <!-- <input type="text" name="tattoos" class="editField hidden" required/>
                   -->
                </div>
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Hair Style</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->hairstyle->hair_style) ? $appearance->hairstyle->hair_style : '-'}}</p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                   <select class="custselectbox1" id="hairstyle" name="hair_style_id">
                     <option value=""></option>
                     @foreach ($hairstyle as $val)
                     <option value="{{$val->id}}" {{isset($appearance)&&($val->id==$appearance->hair_style_id)?'Selected':''}}
                        >{{$val->hair_style}}</option>
                     @endforeach
                 </select>
                  <!-- <input type="text" name="tattoos" class="editField hidden" required/>
                   -->
                </div>
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Skin Tone</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->skintone->skin_tone) ? $appearance->skintone->skin_tone : '-'}}</p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                  <select class="selectField" id="skinTone" name="skin_tone_id">
                    <option></option>
                    @foreach ($Skintone as $Skintone)
                      <option value="{{ $Skintone->id }}" {{isset($appearance)&&($Skintone->id==$appearance->skin_tone_id)?'Selected':''}}>{{$Skintone->skin_tone}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="egchhhsboxlr">
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Nationality</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->nationality->nationality) ? $appearance->nationality->nationality : '-'}}</p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                  <select class="selectField" id="Nationality" name="nationality_id">
                    <option></option>
                    @foreach ($Nationality as $Nationality)
                      <option value="{{ $Nationality->id }}" {{isset($appearance)&&($Nationality->id==$appearance->nationality_id)?'Selected':''}}>{{$Nationality->nationality}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>

          
      



          
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Gender</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->gender) ? $appearance->gender : '-'}}</p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                  <select class="selectField" id="Gender" name="gender">
                    <option></option>
                    <option value="Male" {{isset($appearance)&&($appearance->gender=="Male")?'Selected':''}}>Male</option>
                    <option value="Female" {{isset($appearance)&&($appearance->gender=="Female")?'Selected':''}}>Female</option>
                    <option value="Transgender" {{isset($appearance)&&($appearance->gender=="Transgender")?'Selected':''}}>Transgender</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Ethnicity</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->ethnicity->ethnicity) ? $appearance->ethnicity->ethnicity : '-'}}</p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                  <select class="selectField" id="Ethnicity" name="ethnicity_id">
                    <option></option>
                    @foreach ($Ethnicity as $Ethnicity)
                      <option value="{{ $Ethnicity->id }}" {{isset($appearance)&&($Ethnicity->id==$appearance->ethnicity_id)?'Selected':''}}>{{$Ethnicity->ethnicity}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Handedness</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->handedness->handedness) ? $appearance->handedness->handedness : '-'}}</p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                  <select class="selectField" data-palceholder="Select Handedness" id="Handedness" name="handedness_id">
                    <option></option>
                    @foreach ($Handedness as $Handedness)
                      <option value="{{ $Handedness->id }}" {{isset($appearance)&&($Handedness->id==$appearance->handedness_id)?'Selected':''}}>{{$Handedness->handedness}}</option>
                    @endforeach 
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Tattoos</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->tattoos) ? $appearance->tattoos : '-'}}</p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                  <select class="selectField" id="tattoos" name="tattoos">
                    <option></option>
                    <option value="Yes" {{isset($appearance)&&($appearance->tattoos=="Yes")?'Selected':''}}>Yes</option>
                    <option value="No" {{isset($appearance)&&($appearance->tattoos=="No")?'Selected':''}}>No</option>
                  </select>
                  <!-- <input type="text" name="tattoos" class="editField hidden" required/>
                   -->
                </div>
              </div>
            </div>
          </div>
          <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Facial Hair</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p class="displayField">{{isset($appearance->hairfacial->hair_facial) ? $appearance->hairfacial->hair_facial : '-'}}</p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                     <select class="custselectbox1" id="hairfacial" name="facial_hair_id">
                      <option value=""></option>
                      @foreach ($hairfacial as $val)
                      <option value="{{$val->id}}" {{isset($appearance)&&($val->id==$appearance->facial_hair_id)?'Selected':''}}>{{$val->hair_facial}}</option>
                      @endforeach
                  </select>
                  <!-- <input type="text" name="tattoos" class="editField hidden" required/>
                   -->
                </div>
              </div>
            </div>
          </div>
         
           <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Piercings</p>
              
                 @if(isset($appearance->piercing))
                  <?php  
                    $p = explode(",",$appearance->piercing);                              
                  ?>
                  @else
                  <?php
                      $p= ["None"];
                    ?>                    
                @endif
            </div>
            <div class="egchhhsboxlr_right">
               <p class="displayField">
                @if(isset($appearance->piercing))
                  {{$appearance->piercing}}
                @endif
              </p>
              <div class="appearanceFormat editField hidden">
                <div class="seletcustboxpart2">
                  <select class="selectField" multiple data-palceholder="Select piercing" id="piercing" name="piercing[]">                   
                    @foreach ($piercing as $val)
                    <option value="{{ $val->piercing }}" <?php if(in_array($val->piercing,$p)){ echo 'Selected';} ?>>{{$val->piercing}}</option>
                    @endforeach 
                  </select>
                </div>
              </div>
            </div>
          </div>

            <div class="teodetacoverbox">
            <div class="egchhhsboxlr_left">
              <p>Doubles for</p>
            </div>
            <div class="egchhhsboxlr_right">
              <p  class="displayField">{{isset($appearance->double_for) ? $appearance->double_for : '-'}}</p>
              <div class="appearanceFormat">                
               <input type="text" name="double_for" class="editField hidden" value="{{isset($appearance->double_for)?$appearance->double_for:''}}">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="restriboxalldeta hidden" id="restrictionedit">
        <div class="topbartitlebox">
          <h3>Restrictions</h3>        
        </div>
        
        <div class="gsin_boxdivd">
          <h4>General</h4>

            @foreach($restriction as $restrict)
            @if($restrict->restrictions_type=='General')
          <div class="cutmbocheck">
            <label class="checkbox-container">{{$restrict->restriction_name}}
              <input type="checkbox" name="{{$restrict->restriction_name}}" class="restriction" value="{{$restrict->id}}"   @if(isset($restrictionperformer))
                              @foreach($restrictionperformer as $restrictperformer) 
                            {{($restrict->id==$restrictperformer->restriction_id)?'checked':''}}
                                @endforeach
                                  @endif>
              <span class="checkmark"></span>
            </label>
          </div>

          @endif
          @endforeach
        </div>
        
        <div class="gsin_boxdivd">          
          <h4>Smoking</h4>
            @foreach($restriction as $restrict)
            @if($restrict->restrictions_type=='Smoking')
          <div class="cutmbocheck">
            <label class="checkbox-container">{{$restrict->restriction_name}}
              <input type="checkbox" class="restriction" name="{{$restrict->restriction_name}}" value="{{$restrict->id}}" @if(isset($restrictionperformer))
                              @foreach($restrictionperformer as $restrictperformer) 
                            {{($restrict->id==$restrictperformer->restriction_id)?'checked':''}}
                                @endforeach
                                  @endif>
              <span class="checkmark"></span>
            </label>
          </div>
          
          @endif
          @endforeach
        </div>
        <div class="gsin_boxdivd">
          <h4>Interactions</h4>
            @foreach($restriction as $restrict)
            @if($restrict->restrictions_type=='Interactions')
          <div class="cutmbocheck">
            <label class="checkbox-container">{{$restrict->restriction_name}}
              <input type="checkbox" name="{{$restrict->restriction_name}}" class="restriction" value="{{$restrict->id}}" @if(isset($restrictionperformer))
                              @foreach($restrictionperformer as $restrictperformer) 
                            {{($restrict->id==$restrictperformer->restriction_id)?'checked':''}}
                                @endforeach
                                  @endif>
              <span class="checkmark"></span>
            </label>
          </div>
          
          @endif
          @endforeach
        </div>
        <div class="gsin_boxdivd">
          <h4>Nudity</h4>
           @foreach($nudity as $nud)
          <div class="cutmbocheck">
            <label class="checkbox-container">{{$nud->nudity_name}}
              <input type="checkbox" id="{{$nud->id}}" value="{{$nud->id}}" class="nudity" name="{{$nud->nudity_name}}" @if(isset($nudityperformer))
                              @foreach($nudityperformer as $nudperformer) 
                            {{($nud->id==$nudperformer->nudity_id)?'checked':''}}
                                @endforeach
                                  @endif >
              <span class="checkmark"></span>
            </label>
          </div>
          @endforeach
     
        </div>
      </div>
      
       <div class="restriboxalldeta removeCheckbox" id="restrictionview">
        <div class="topbartitlebox">
          <h3>Restrictions</h3>        
        </div>

        <div class="gsin_boxdivd">
          <h4>General</h4>
          


            @foreach($restrictionperformergeneral as $restrict)
           <div class="cutmbocheck">
            <label class="checkbox-container">{{$restrict->restrictionName->restriction_name}}
             
             <!--  <span class="checkmark"></span> -->
            </label>
          </div>

  
          @endforeach
        </div>
        
        <div class="gsin_boxdivd">          
          <h4>Smoking</h4>
            @foreach($restrictionperformersmoking as $restrict)
           <div class="cutmbocheck">
            <label class="checkbox-container">{{$restrict->restrictionName->restriction_name}}
            
            </label>
          </div> 
             @endforeach
        </div>
        <div class="gsin_boxdivd">
          <h4>Interactions</h4>
            @foreach($restrictionperformerInteractions as $restrict)
       
          <div class="cutmbocheck">
            <label class="checkbox-container">{{$restrict->restrictionName->restriction_name}}
        
            </label>
          </div>
          
        
          @endforeach
        </div>
        <div class="gsin_boxdivd">
          <h4>Nudity</h4>
           @foreach($nudityperformer as $nud)
          <div class="cutmbocheck">
            <label class="checkbox-container">{{$nud->nudityName->nudity_name}}
            
            </label>
          </div>
          @endforeach
     
        </div>
      </div>

    </div>  
 </form>
</div>
</div>

</div>
<script type="text/javascript">

  

  function editAppearance(argument) {
    jQuery('.displayField').addClass('hidden');
    jQuery('.editField').removeClass('hidden');
    jQuery('#editAppearance').addClass('hidden');
    jQuery('#updateAppearance').removeClass('hidden');

    jQuery('#restrictionedit').removeClass('hidden');
    jQuery('#restrictionview').addClass('hidden');
    jQuery('#skinTone').select2({
      placeholder:'select skinTone',

    });
    jQuery('#Eyes').select2({
      placeholder:'Select Eyes',
    });
    jQuery('#HairColor').select2({
      placeholder:'Select HairColor',
    });
    jQuery('#Ethnicity').select2({
      placeholder:'Select Ethnicity',
    });
    jQuery('#Handedness').select2({
      placeholder:'Select Handedness',
    });
    jQuery('#Nationality').select2({
      placeholder:'Select Nationality',
    });
    jQuery('#region_select').select2({
      placeholder:'Select Region',
    });
    jQuery('#Glasses').select2({
      placeholder:'Select Glasses',
    });
    jQuery('#Contacts').select2({
      placeholder:'Select Contacts',
    });
    jQuery('#Gender').select2({
      placeholder:'Select Gender',
    });
    jQuery('#Tatoos').select2({
      placeholder:'Select Tatoos',
    });
    jQuery('#HairLength').select2({
      placeholder:'Select HairLength',
    });
    jQuery('#tattoos').select2({
      placeholder:'Select Tattoos',
    });
    jQuery('#gloves').select2({
      placeholder:'Select Gloves',
    });
    jQuery('#bodytrait').select2({
      placeholder:'Select Body Trait',
    });
    jQuery('#hairstyle').select2({
      placeholder:'Select Hair Style',
    });
    jQuery('#hairfacial').select2({
      placeholder:'Select Hair Facial',
    });
    jQuery('#piercing').select2({
      placeholder:'Select Piercing',
      multiple: true,
      tags:true,
      allowClear: true,
    });
  }
  function updateAppearance(argument) {
    
    jQuery('.displayField').removeClass('hidden');
    jQuery('.editField').addClass('hidden');
    jQuery('#editAppearance').removeClass('hidden');
    jQuery('#updateAppearance').addClass('hidden');
    jQuery('#restrictionedit').removeClass('hidden');
    jQuery('#restrictionview').addClass('hidden');
    // jQuery(document).ready(function () {
    // jQuery( "#updateAppearance").click(function(e) {
     // alert('hii');
        // e.preventDefault();
       var form =  jQuery( "#searchForm" ).serialize();
       // alert(form); 
      
       $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
       $.ajax({
                      url: '{{route("performers.appearance")}}',
                      type: 'POST',
                      data: form,
                      success: function(response)
                      {
                       toastr.success('Stats Updated Successfully');
                       location.reload();
                      }
                  });
       // form.action=$(this).attr('href');
       // console.log(form.action);
        //form.submit();
        return false;
     // });
  // });

  }
  //$(function(){
  //     $("#request").on("submit", function(e){
  //         $.ajax({
  //           url: 'match_api.php',
  //           type: 'post',
  //           data: $('#request').serialize(),
  //           dataType: 'json',
  //           success: function(data) {
  //               alert($('#request').serialize());
  //          });
  //          e.preventDefault();
  //     });
 //  });
 
</script>

@endsection
@section('scripts')



<script type="text/javascript">

  
  


        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        function IsNumeric(e,valueKey) {
          var keyCode = e.which ? e.which : e.keyCode 
          if(keyCode==34 || keyCode==39 || keyCode== 44 || keyCode== 46 ){
              return ret;
            }
          var ret = ((keyCode >= 48 && keyCode <= 57 )  ||  specialKeys.indexOf(keyCode) != -1); 
                       return ret;
          
          
        }
        
    </script>
<script type="text/javascript">
    $(document).ready(function() {
       toastr.options = {
              "preventDuplicates": true,
              "preventOpenDuplicates": true
              };
      //set initial state.
      $('.nudity').change(function() {
              id=$(this).val();
              
               $.ajax({
                  type: "GET",
                  url: "{{route('performers.addnudity')}}",
                  data: {id:id},
                  success: function (data) {
                          if(data.result=="deleted")
                       {
                          toastr.error("Nudity Removed Successfully");
                       } 
                       else{
                          toastr.success("Nudity Added Successfully");
                       }
                      }         
              });
          
          // $('#textbox1').val(this.checked);        
      });

      $('.restriction').change(function() {
              id=$(this).val();
               $.ajax({
                  type: "GET",
                  url: "{{route('performers.addrestriction')}}",
                  data: {id:id},
                  success: function (data) {
                       if(data.result=="deleted")
                       {
                          toastr.error("Restrictions Removed Successfully");
                       } 
                       else{
                          toastr.success("Restrictions Added Successfully");
                       }
                      }         
              });
          
          // $('#textbox1').val(this.checked);        
      });
  });
</script> 
@endsection