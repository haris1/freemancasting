@extends('performers.layouts.app')
@section('content')
<div class="wctleftrightbox">
  @if($performer->applicationfor == "Group")
  @include('performers.layouts.groupSidebar')  
  @else
  @include('performers.layouts.sidebar')  
  @endif
     <div class="rightallditinwct">
    <!-- <div class="searchbarboxfilt">
      <div class="srchbox">        
        <div class="form-group">
          <i class="far fa-search"></i>
          <input type="text" class="form-control" id="skillSearch" placeholder="Search">
        </div>
      </div>
      <div class="addandtagbox">
        <div class="addatribut">
          <a href="javascript:void(0)" type="button" data-toggle="modal" data-target="#myDocumentsModal"><i class="far fa-plus"></i>New Document</a>
        </div>
        <div class="tagboxright">
          <a href="javascript:void(0)"><i class="far fa-tag"></i></a>
        </div>
      </div>
    </div> -->
       <div class="addflagshidbox">
    <div class="accordion-container">
      <div class="set hedtobox">
        <div class="srchbox hidden">        
          <div class="form-group">
            <i class="far fa-search"></i>
            <input type="text" class="form-control" id="" placeholder="Search">
          </div>
        </div>
        @if($authtype=='performer')     
        @else
        @include('performers.flag')
        @endif       
      </div>      
    </div>
  </div>
  <div class="topbarbotdivcover">
  @if($authtype=='performer')

    @include('performers.layouts.tabbar')
    @else
    @include('performers.layouts.internal-tabbar')
    @endif 

         <div class="doctslistdit">
      <div class="doctslistdit_title">
          <h3>Documents List</h3>
          <div class="docliteddibox doctslistdit_title_hide">
            <a href="javascript:void(0)" id="editDocumentbtn"  onclick="editDocument()"><i class="far fa-edit"></i></a>
            <a href="javascript:void(0)" id="updateDocumentbtn" class="hidden"  onclick="updateDocument()"><i class="far fa-save"></i></a>
          </div>          
      </div>
      <div class="errbrtext">
        <i class="far fa-info-circle"></i>
        <p>{{$helpdesk->document_help}}</p>
      </div>
      <div class="dpnpiwsbox">
   

        <div class="dpnpiwsbox_partdiv">
          @foreach($document as $doc)
          <?php if(!in_array($doc->id,$docStatus)){ ?>
            @if(isset($performersStatus[$doc->id]))
            @if($performersStatus[$doc->id]->document_status=='Yes')
            <div class="dpnpiwsbox_inerfullw documentSelectedList">
              <div class="lefttextdpmpiws">
                <p>{{ $doc->doument_name }}</p>
              </div>
              <div class="righttextdpmpiws">
                <div class="custheckedbox Checkedgrn">
                  <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1" checked>
                  <label for="styled-checkbox-1">Checked</label>
                </div>
              </div>
            </div>
          @elseif($performersStatus[$doc->id]->document_status=='No')
          <div class="dpnpiwsbox_inerfullw documentSelectedList">
           <div class="lefttextdpmpiws">
            <p>{{ $doc->doument_name }}</p>
          </div>
          <div class="righttextdpmpiws">
            <div id="red{{$doc->id}}" class="custheckedbox Checkedred">
              <input class="styled-checkbox documentcheckbox" id="{{$doc->id}}" type="checkbox" value="{{$doc->id}}">
              <label id="lblred{{$doc->id}}" for="{{$doc->id}}">Not Checked</label>              
            </div>
          </div>
        </div>        
        @endif
          @endif
          <?php } ?>


          
          
           <div class="documentSelectOption hidden">
            @if($doc->doument_type == 1)
            <div class="dpnpiwsbox_inerfullw">
              <div class="lefttextdpmpiws">
                <p>{{$doc->doument_name}}</p>
              </div>
              <div class="righttextdpmpiws">
                <div class="customradiocover">
                  <div class="radiobtnboxset">
                    <label class="customradiobtn">
                      <input  class="documentStatus" type="radio" value="Yes" name="document_status{{$doc->id}}" onclick="documentStatus('{{$doc->id}}','Yes')" <?php if(isset($performersStatus[$doc->id]->document_status)){if($performersStatus[$doc->id]->document_status =='Yes'){echo "checked";}}?> >Yes
                        <span class="checkmarkbox"></span>
                    </label>
                  </div>              
                  <div class="radiobtnboxset">
                    <label class="customradiobtn">
                      <input class="documentStatus" type="radio" value="No" name="document_status{{$doc->id}}" onclick="documentStatus('{{$doc->id}}','No')" <?php if(isset($performersStatus[$doc->id]->document_status)){if($performersStatus[$doc->id]->document_status =='No'){echo "checked";}}?>> No
                      <span class="checkmarkbox"></span>
                    </label> 
                  </div>
                  <div class="radiobtnboxset">
                    <label class="customradiobtn">
                      <input  class="documentStatus" type="radio" value="N/A" name="document_status{{$doc->id}}" onclick="documentStatus('{{$doc->id}}','NA')" <?php if(isset($performersStatus[$doc->id]->document_status)){if($performersStatus[$doc->id]->document_status =='NA'){echo "checked";}}?>> N/A
                      <span class="checkmarkbox"></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
            @else
            <div class="dpnpiwsbox_inerfullw">
              <div class="lefttextdpmpiws">
                <p>{{$doc->doument_name}}</p>
              </div>
              <div class="righttextdpmpiws">
                <div class="customradiocover">
                  <div class="radiobtnboxset">
                    <label class="customradiobtn">
                      <input  class="documentStatus" type="radio" value="Yes" name="document_status{{$doc->id}}" onclick="documentStatus('{{$doc->id}}','Yes')" <?php if(isset($performersStatus[$doc->id]->document_status)){if($performersStatus[$doc->id]->document_status =='Yes'){echo "checked";}}?>> Yes
                        <span class="checkmarkbox"></span>
                    </label>
                  </div>
                  <div class="radiobtnboxset">
                    <label class="customradiobtn">
                      <input class="documentStatus" type="radio" value="No" name="document_status{{$doc->id}}" onclick="documentStatus('{{$doc->id}}','No')" 
                      <?php if(isset($performersStatus[$doc->id]->document_status)){if($performersStatus[$doc->id]->document_status =='No'){echo "checked";}}?>> No
                      <span class="checkmarkbox"></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
            @endif
          </div>
          @endforeach
          @if(count($performersStatus) <= 0)
          @foreach($document as $doc)
            <div class="dpnpiwsbox_inerfullw documentSelectedList">
             <div class="lefttextdpmpiws">
              <p>{{ $doc->doument_name }}</p>
            </div>
            <div class="righttextdpmpiws">
              <div id="red{{$doc->id}}" class="custheckedbox Checkedred">
                <input class="styled-checkbox documentcheckbox" id="{{$doc->id}}" type="checkbox" value="{{$doc->id}}">
                <label id="lblred{{$doc->id}}" for="{{$doc->id}}">Not Checked</label>              
              </div>
            </div>
          </div>        
          @endforeach    
          @endif
        </div>
        <div class="dpnpiwsbox_partdiv">
            @foreach($document as $doc)
             <?php if(in_array($doc->id,$docStatus)){ ?>
          <div class="dpnpiwsbox_inerfullw documentSelectedList">
            <div class="lefttextdpmpiws">
              <p>{{$doc->doument_name}}</p>
            </div>
            <div class="righttextdpmpiws">
              <div class="notaplibox"><i class="far fa-minus-square"></i> <p> Not applicable</p></div>
            </div>
          </div>
        <?php } ?>
         @endforeach
        </div>
      </div>
    </div>
  </div>  
</div>  
</div>
</div>
</div>


@endsection
@section('scripts')

<script type="text/javascript">


    $('.documentcheckbox').on('change', function(){ 

        id=$(this).attr('id');      
        if (this.checked) {
             $.ajax({

                type: "POST",
                url: "{{route('performers.adddocument')}}",
                headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                data: {
                        id:id
                      },
                success: function (data) {
                       // console.log(data);
                      //  alert(data.message);
                    
                      $("#red"+id).removeClass('Checkedred');
                      $("#red"+id).addClass('Checkedgrn');
                      $("#lblred"+id).html('Checked');
                      if($("#lblgrn"+id).html() == 'Not Checked'){
                       $("#green"+id).addClass('Checkedgrn');
                       $("#green"+id).removeClass('Checkedred');
                       $("#lblgrn"+id).html('Checked'); 
                      }
                       toastr.success(data.message);
                      }
                              
            });
        }else{
           $.ajax({

              type: "POST",
              url: "{{route('performers.removedocument')}}",
              headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
              data: {
                      id:id
                    },
              success: function (data) {

                      $("#green"+id).removeClass('Checkedgrn');
                      $("#green"+id).addClass('Checkedred');
                      $("#lblgrn"+id).html('Not Checked');
                      if($("#lblred"+id).html() == 'Checked'){
                       $("#red"+id).addClass('Checkedred');
                       $("#red"+id).removeClass('Checkedgrn');
                       $("#lblred"+id).html('Not Checked'); 
                      }
                       toastr.success(data.message); 

                    }         
          });         
        }
    });

    function editDocument()
    {

      $("#editDocumentbtn").addClass("hidden");
      $("#updateDocumentbtn").removeClass("hidden");
      
      $(".documentSelectedList").addClass("hidden");
      $(".documentSelectOption").removeClass("hidden");
    }

    function updateDocument()
    {

      $("#editDocumentbtn").removeClass("hidden");
      $("#updateDocumentbtn").addClass("hidden");
      
      $(".documentSelectedList").removeClass("hidden");
      $(".documentSelectOption").addClass("hidden");
      location.reload(true);
    }

    function documentStatus(id,value)
    {
        $.ajax({

          url:"{{route('performers.editDocument')}}",
          type:"GET",
          data:{document_id:id,document_status:value},
          success:function(data){
            toastr.success(data.message);
          }
      });     
    }
</script>
@include('performers.commonSkill')
@endsection