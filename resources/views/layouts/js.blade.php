<script type="text/javascript" src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script src="https://raw.githubusercontent.com/igorescobar/jQuery-Mask-Plugin/master/src/jquery.mask.js"></script>
<script type="text/javascript">
	$(function() {
     $("#phone").mask("(99) 9999-9999"); 
  $('input[name="datefilter"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
  });

  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});
</script>

<script type="text/javascript">
	$(function() {

  $('input[name="datefilter"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
  });

  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});
</script>

<script type="text/javascript">
	$(document).ready(function() {
  
	var data = [
	    {"sr":1,"date":"21-Nov-2018 @ 5:55 p.m","name":"Abc","page":" Abc","account_type":"Abc","city":"Xyz",'connected':'<img src="images/table-instagram.png"> ' ,'More':''},

	    {"sr":2,"date":"22-Nov-2018 @ 5:55 p.m","name":"Abc","page":" Abc","account_type":"Abc","city":"Xyz",'connected':'<img src="images/table-instagram.png"> ' ,'More':''}
  	];
  
  
  function format (data) {
      return '<div class="details-container">'+
          '<table cellpadding="2" cellspacing="0" border="0" class="details-table">'+
              '<tr>'+
                  '<td class="title">Person ID:</td>'+
                  '<td>'+data.sr+'</td>'+
              '</tr>'+
              '<tr>'+
                  '<td class="title">Following:</td>'+
                  '<td>'+data.name+'</td>'+
                  '<td class="title">Followers:</td>'+
                  '<td>'+data.page+'</td>'+
              '</tr>'+
              '<tr>'+
                  '<td class="title">Photos:</td>'+
                  '<td>'+data.name+'</td>'+
                  '<td class="title">Videos:</td>'+
                  '<td>'+data.name+'</td>'+
              '</tr>'+
          '</table>'+
        '</div>';
  };
  
  var table = $('.datatables').DataTable({
    // Column definitions
    columns : [
      {data : 'sr'},
      {data : 'date'},
      {data : 'name'},
      {data : 'page'},
      {data : 'account_type'},
      {data : 'city'},
      {data : 'connected'},
      {
        className      : 'details-control',
        defaultContent : '',
        data           : null,
        orderable      : false
      },
    ],
    
    data : data,
    
    pagingType : 'full_numbers',
    
    // Localization
    language : {
      emptyTable     : 'Virhe! Haku epäonnistui.',
      zeroRecords    : 'Ei hakutuloksia.',
      thousands      : ',',
      processing     : 'Prosessoidaan...',
      loadingRecords : 'Ladataan...',
      infoEmpty      : 'Sivu 0 / 0',
      infoFiltered   : '(rajattu _MAX_ hakutuloksesta)',
      infoPostFix    : '',
      lengthMenu     : 'Rivien _MENU_',
      search         : 'Search:',
      paginate       : {
        // first    : 'Ensimmäinen',
        // last     : 'Viimeinen',
        // next     : 'Seuraava',
        // previous : 'Edellinen'
      },
      aria : {
        sortAscending  : ' aktivoi järjestääksesi sarake nousevasti',
        sortDescending : ' aktivoi järjestääksesi saraka laskevasti'
      }
    }
  });
 
  $('.datatables tbody').on('click', 'td.details-control', function () {
     var tr  = $(this).closest('tr'),
         row = table.row(tr);
    
     if (row.child.isShown()) {
       tr.next('tr').removeClass('details-row');
       row.child.hide();
       tr.removeClass('shown');
     }
     else {
       row.child(format(row.data())).show();
       tr.next('tr').addClass('details-row');
       tr.addClass('shown');
     }
  });
 
});
</script>