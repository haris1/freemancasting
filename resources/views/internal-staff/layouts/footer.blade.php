<?php $internal_staff =Auth::user();?>
<div class="modal fade userinfoboedit" id="myModal15" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">      
      <div class="modal-body">        
        <div class="editboxprofilin">
          <div class="totitleaddris">
            <h4>Edit User Profile</h4>
            <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
          </div>

          <input type="hidden" name="performers_user" id="performers_user" value="{{isset($per_id) ? $per_id : '' }}">

          @if(isset($internal_staff))
          <!-- ProfileData --> 
          <div class="detailprofcover" id="ProfileDisplay">
            <form method="post" id="staffProfileEdit">
            <div class="form-group">
              <label for="usr">First Name</label>
              <input type="text" value="{{$internal_staff->firstname}}" name="firstname" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Last Name</label>
              <input type="text" value="{{$internal_staff->lastname}}" name="lastname" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Email</label>
              <input type="text" name="email" value="{{$internal_staff->email}}" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Phone</label>
              <input type="text" name="phonenumber" value="{{$internal_staff->phonenumber}}" class="form-control"   data-mask="(000) 000-0000">
            </div>
            <div class="savebtnbox">
              <button type="button" id="staffupdateProfile" class="btn">Save</button>

              <button type="button" id="ChangePassword" class="btn">Change Password</button>
            </div>
            <input type="hidden" value="{{$internal_staff->id}}" name="id" class="form-control" >
          </form>
          </div>
          
          <!-- ProfilePassword -->
          <div class="detailprofcover" id="passwordDisplay">
            <form method="post" id="ChangePasswordForm">
            <div class="form-group">
              <label for="usr">Old Password</label>
              <input type="password" value="" name="old_password" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">New Password</label>
              <input type="password" value="" name="password" class="form-control" id="usr">
            </div>
            <div class="form-group">
              <label for="usr">Confirm Password</label>
              <input type="password" value="" name="confirm_password" class="form-control" >
            </div>
            <div class="savebtnbox">
              <button type="button" id="PasswordChangeBtn" class="btn">Change Password</button>
              <button type="button" id="profileformShow" class="btn">Profile </button>
            </div>
          </form>
          </div>
          @endif
          
        </div>
      </div>
    </div>    
  </div>
</div>

@if(!isset($is_show))
<script type="text/javascript">
 jQuery(document).ready(function(){
 
  jQuery('.internalStaffAction,.deleteresumeconfirm,.deletefield a,.morebtneddil,.AddTag,.btnremadd a,.gelimgleftbox li .deleteimgbox,#editAppearance,.addbtnbotbox,.btnAddPortfolioGallery,.deleteimageconfirm,.doctslistdit_title_hide,.audioDeleteconfirm,.skill_resume_Search_hide,.hidesearch,.btnAddRentalGallery,.btnAddClosetGallery,.hiddenforInternalStaff').addClass('hidden');
    

jQuery('#showForIntenalStaff').removeClass("hidden");

    $(".fieldForm").hide();
       $("#staffupdateProfile").click(function(){
      EditformData = $("#staffProfileEdit").serialize();

      $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
      $.ajax({
          url:"{{route('internal-staff.updateProfile')}}",
          type:'POST',
          data:EditformData,
          success:function(response)
          {
           
              if(response.status == true)
              {
                  toastr.success(response.message);
                  location.reload(1500);
              }
              else{
                toastr.error(response.message);
              }
          }
      });

  });
 });
</script>

<script type="text/javascript">
  
  $(document).ready(function(){    

   $.ajax({
        url:"{{route('internal-staff.portfolio.getSliderImage')}}",
        type:'get',
       
        data:{id:$.trim(jQuery('#performers_user').val())},
        success:function(response){
           showsliderimg(response.msg);
            
        }

  });
    });
function showsliderimg(img){
  
 setTimeout(function(){ 
  Vue.use(VueAgile);
    app = new Vue({
    el: '#mySidebarImage',
    components: {
    agile: VueAgile },
  data() {
    return {
      asNavFor1: [],
      asNavFor2: [],
      options1: {
        dots: false,
        fade: true,
        navButtons: false },
      options2: {
        autoplay: false,
        centerMode: false,
        dots: false,
        navButtons: false,
        infinite : false,
        slidesToShow: 3,
        responsive: [
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 4 } },

        {
          breakpoint: 1000,
          settings: {
            navButtons: true } }] },


          slides: img
          };


  },
  mounted() {
    this.asNavFor1.push(this.$refs.thumbnails);
    this.asNavFor2.push(this.$refs.main);
  } }); }, 1000);

 }
 
 $(document).ready(function() {
    $(".set > a").on("click", function() {
      if ($(this).hasClass("active")) {
        $(this).removeClass("active");
        $(this)
        .siblings(".content")
        .slideUp(200);
        $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
      } else {
        $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
        $(this)
        .find("i")
        .removeClass("fa-plus")
        .addClass("fa-minus");
        $(".set > a").removeClass("active");
        $(this).addClass("active");
        $(".content").slideUp(200);
        $(this)
        .siblings(".content")
        .slideDown(200);
      }
    });
  });
</script>
@endif
