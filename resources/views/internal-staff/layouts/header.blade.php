<header class="clainternal_staffhed">
  <div class="hedpartlr">
    <div class="lefttitlebox">
      <div class="imageLogo">
        <a href="{{ route('internal-staff.performers.list') }}">
        <img src="{{ asset('assets/images/logo.png') }}" alt="">
      </a>
      </div>
    </div> 
    <div class="rightnotusei">
      <div class="inerboxsetpart">
        	<a href="javascript:void(0)" class="belllogbtn"><i class="far fa-bell"></i></a>
        	<a href="javascript:void(0)" class="active" type="button" data-toggle="modal" data-target="#myModal15"><i class="fas fa-user"></i></a>
          <!-- <a href="javascript:void(0)" class="active" type="button" data-toggle="modal" data-target="#myModal15">
            <div class="userprofileiconset_inerimg">
              <div class="hexagon_userprofileset_maincl1">
                <div class="hexagon_userprofileset_inercl1">
                  <img src="http://192.168.1.18/freemancasting/public/storage/performers/1/1563363531Chrysanthemum.jpg">
                </div>    
              </div>    
            </div>
          </a>  -->
        	<a href="{{ route('internal-staff.logout') }}" onclick="event.preventDefault();
          	document.getElementById('logout-form').submit();" class="belllogbtn">
         		<i class="fas fa-power-off"></i>
      	</a>
      	<form id="logout-form" action="{{ route('internal-staff.logout') }}" method="POST" style="display: none;">
          	@csrf
      	</form>
      </div> 
    </div> 
  </div>
 
</header>
  <div class="tabdataperfbox internalStaffAction">
    <div class="leftfreeperbtn">
      <a href="{{ route('internal-staff.performers.list') }}" class="activebt">Freeman Casting Database</a>
      <!-- <a href="{{ route('internal-staff.myperformers') }}" >My Performers</a> -->
    </div>
    <div class="rightretperfbtn">
      <!-- <a href="javascript:void(0)"><i class="fal fa-plus"></i>Create Performer</a> -->
      <a href="javascript:void(0)" class="active" type="button" data-toggle="modal" data-target="#PerformerModel"><i class="fal fa-plus"></i> Create Performer</a>
    </div>
  </div>