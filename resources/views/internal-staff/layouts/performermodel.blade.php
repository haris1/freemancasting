<div class="modal fade userinfoboedit" id="PerformerModel" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">      
      <div class="modal-body">        
        <div class="editboxprofilin">
          <div class="totitleaddris">
            <h4>Add Performer</h4>
            <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
          </div>
          
          <div class="detailprofcover" id="ProfileData">
             <form method="POST" action="{{route('internal-staff.addperformer')}}" enctype="multipart/form-data" id="addperformer">
                @csrf          
            <div class="form-group">
              <label for="usr">First Name</label>
              <input type="text" value="" name="firstName" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Middle Name</label>
              <input type="text" value="" name="middleName" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Last Name</label>
              <input type="text" value="" name="lastName" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Agent</label>
              <!-- <input type="text" class="form-control validate" name="agent" data-required="Please enter Agent(Company and Contact)" id="agent">
              <input type="hidden" class="form-control validate" name="agent_id" data-required="Please enter Agent(Company and Contact)" id="agent_id"> -->

<!--              L\ <div id="categorysomeElem" class="sectionsomeElem" ></div> -->
              <div class="seletcustboxpart2">

                <select class="form-control validate" id="agentselect" name="agent_id" data-required="Please enter Agent name">
                  <option></option>
                  @foreach($agent as $agent) 
                  <option value="{{$agent->id}}">{{$agent->agent_name}}</option>



                  @endforeach
              </select>
            </div>
            </div>

         <!--    <div class="semselectdis">
                <p>Immigration Status</p>
                <div class="seletcustboxpart">
                    <select  name="" id="immigrationmodel">
                        <option value=""></option>
                        @foreach ($perfomerstatus as $value)
                        <option value="{{ $value->id }}">{{$value->performer_status}}</option>
                        @endforeach 
                    </select>
                </div>
            </div> -->
            <div class="form-group date">
                <label for="usr">Date of Birth</label>
                <input type="text" value="" class="form-control validate  DOBdatepicker" name="date_of_birth" data-required="Please select the date of birth" autocomplete="off" id="DOB" readonly>
            </div>
            <div class="form-group">
              <label for="usr">Email</label>
              <input type="text" name="email" value="" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Phone</label>
              <input type="text" name="phonenumber" value="" class="form-control"  id="phonenumber">
            </div>
            <div class="form-group">
              <label for="usr">Address</label>
              <input type="text" name="address" value="" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">City</label>
              <input type="text" name="city" value="" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Province</label>
              <input type="text" name="province" value="{{isset($performers_user->province) ? $performers_user->province : 'BC'}}" class="form-control" id="province">
            </div>

            <div class="form-group">
              <label for="usr">Postal Code</label>
              <input type="text" name="postalCode" value="" class="form-control" id="postalcode" data-mask="AAAAAAAAAAA">
            </div>
            <div class="form-group">
              <label for="usr">Country</label>
              <input type="text" name="country" value="{{isset($performers_user->country) ? $performers_user->country : 'Canada'}}" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">SIN</label>
              <input type="text" name="sin" value="" class="form-control" >
            </div>
            <div class="form-group">
                
                    <label>Union</label>
                <div class="seletcustboxpart">
                    <select class="selectField selectSearchField" id="unionmodel"  data-reid="unionSelect" name="union">
                        <option value=""></option>
                        <option>Union</option>
                        @foreach ($performerunion as $value)
                        <option value="{{ $value->union }}">{{$value->union}}</option>
                        @endforeach
                    </select>
                </div>
             
            </div>
            <div class="form-group">
              <label for="usr">Union Number</label>
              <input type="text" name="union_number" value="" class="form-control"  id="">
            </div>
            <div class="form-group">
              <label>Gender</label>
              <div class="seletcustboxpart">
                <select class="selectField selectSearchField"  data-reid="genderSelect" id="gendermodel" name="gender">
                    <option value=""></option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                    <option value="Transgender">Transgender</option>
                </select>
              </div>
            </div>
               
    <!--         <div class="form-group">
              <label for="usr">Social media(Instagram)</label>
              <input type="text" value="" name="instagram" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Social media(Twitter)</label>
              <input type="text" value="" name="twitter" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Social media(Facebook)</label>
              <input type="text" value="" name="facebook" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Social media(Linkedin)</label>
              <input type="text" value="" name="linkedin" class="form-control" >
            </div>
            <div class="form-group">
              <label for="usr">Password</label>
              <input type="password" value="" name="password" class="form-control" >
            </div> -->
            <div class="savebtnbox">
              <button type="button" id="submitPerformerModel" class="btn">Save</button>

              <!-- <button type="button" id="ChangePassword" class="btn">Can</button> -->
            </div>
            
            <!-- <input type="hidden" value="" name="id" class="form-control" > -->
          </form>
          </div>

         <!--  <div class="detailprofcover" id="ProfilePassword">
            <form method="post" id="ChangePasswordForm">
            <div class="form-group">
              <label for="usr">Old Password</label>
              <input type="password" value="" name="old_password" class="form-control" >
            </div>

            <div class="form-group">
              <label for="usr">New Password</label>
              <input type="password" value="" name="password" class="form-control" id="usr">
            </div>

            <div class="form-group">
              <label for="usr">Confirm Password</label>
              <input type="password" value="" name="confirm_password" class="form-control" >
            </div>

            <div class="savebtnbox">
              <button type="button" id="PasswordChangeBtn" class="btn">Change Password</button>
              <button type="button" id="profileformShow" class="btn">Profile </button>
            </div>
          </form>
          </div> -->
          
        </div>
      </div>
    </div>    
  </div>
</div>


  

