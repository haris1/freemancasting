<!-- user detail modal -->
<?php 
//echo "<pre>";
//print_r($performer);
//print_r($restriction);
?> 
<div class="modal-body">
    <div class="filhivdatauserbox">
        
        <div class="filhivdatauserbox_left">            
            <div id="app" class="dtusegslbox">
                @if(isset($performer->referred_by) && $performer->referred_by !="" )
                <div class="brflimgset">
                    <img src="{{asset('assets/svg/default-icon.svg') }}" alt="">
                </div>
                @endif
                <!-- <agile class="main" ref="main" :options="options1" :as-nav-for="asNavFor1">
                    <div class="slide" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`"><img id="userImage" src="{{$performer->image_url}}"/></div>
                </agile>
                <agile class="thumbnails" ref="thumbnails" :options="options2" :as-nav-for="asNavFor2">
                    <div class="slide slide--thumbniail" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`" @click="$refs.thumbnails.goTo(index)">
                        
                        <img :src="slide"/>
                    </div>
                    <template slot="prevButton"><i class="fas fa-chevron-left"></i></template>
                    <template slot="nextButton"><i class="fas fa-chevron-right"></i></template>
                </agile> -->


                <agile class="main" ref="main" :options="options1" :as-nav-for="asNavFor1">
                    <div class="slide" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`"><img :src="slide"/></div>
                </agile>
                <agile class="thumbnails" ref="thumbnails" :options="options2" :as-nav-for="asNavFor2">
                    <div class="slide slide--thumbniail" v-for="(slide, index) in slides" :key="index" :class="`slide--${index}`" @click="$refs.thumbnails.goTo(index)"><img :src="slide"/></div>
                    <template slot="prevButton"><i class="fas fa-chevron-left"></i></template>
                    <template slot="nextButton"><i class="fas fa-chevron-right"></i></template>
                </agile>
            </div>
            <div class="hevglbsbox">
                <div class="hevlbsbox_inerbox">
                    <p>{{$performer->age}}{{($performer->gender=="male"?'M':'F')}}</p>
                </div>
                <div class="hevlbsbox_inerbox">
                    <p>@if(isset($performer->appearance[0]->height))
                        <?php $height = preg_replace(array('/\bfeet\b/','/\bft\b/','/\bf\b/','/\binches\b/','/\bin\b/'),array("'","'","'",'"','"','"'), $performer->appearance[0]->height);

                           str_replace('ft',"'",$height);
                        ?>
                        
                            {{(strlen($height) < 7 ? $height : "")}}
                                              
                        @else-
                        @endif</p>
                </div> 
                <div class="hevlbsbox_inerbox">
                    <p>@if(isset($performer->appearance[0]->weight)) 
                        {{str_replace("lbs","",$performer->appearance[0]->weight)}} lbs
                     @else -
                    @endif</p>
                </div>
            </div>
            <div class="viewprofboxbtno hidden">
                <div class="threebtnboxset">
                    <a href="javascript:void(0)"><i class="far fa-star"></i></a>
                    <a href="javascript:void(0)"><i class="far fa-user-friends"></i></a>
                    <a href="javascript:void(0)"><i class="far fa-film"></i></a>
                </div>
                <!-- <div class="provewbtn"><a href="javascript:void(0)">View Profile <i class="far fa-long-arrow-right"></i></a></div> -->
            </div>
            
        </div>
        <div class="filhivdatauserbox_right">
            <div class="detatilbox">
                <h3 ></h3>
                <p id="userName">{{isset($performer->full_name) ? $performer->full_name : '-'}}</p>
                
                <button type="button" class="close" data-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <div class="aufddockbox">
                <div class="aufddockbox_left">
                    <p>Agent</p>
                </div>

                <div class="aufddockbox_right">
                    <p id="agentName">{{isset($performer->agentDetail->agent_name) ? $performer->agentDetail->agent_name : '-'}}</p><i class="far fa-comment"></i>
                    <!-- <p id="agentName">{{isset($performer->age) ? $performer->age : '-'}}</p> -->
                </div>
            </div>
            <div class="aufddockbox">
                <div class="aufddockbox_left">
                    <p>Union</p>
                </div>
                <div class="aufddockbox_right">
                    <p id="unionDetail">
                        @if(isset($performer->unionDetail->union))
                        {{$performer->unionDetail->union}}
                        @else
                        -
                        @endif
                        
                        <span id="unionNumber">{{isset($performer->union_number) ? $performer->union_number : '-'}}</span>
                    </p>
                      
                </div>
            </div>
             <div class="aufddockbox referredtextcl">
                <div class="aufddockbox_left">
                    <p>Referred</p>
                </div>
                <div class="aufddockbox_right">
                    <p>{{isset($performer->referred_by) ? $performer->referred_by : '-'}}</p>
                </div>
            </div>
            <div class="aufddockbox flagicontext">
                <div class="aufddockbox_left">
                    <p>Flags</p>
                </div>
                <div class="aufddockbox_right">
                    <p>
                        @if($performer->performerFlag)
                        @if(count($performer->performerFlag) >= 1)                                
                        <p id="flagcount{{$performer->id}}">{{count($performer->performerFlag)}}</p>
                        <i class="far fa-flag-alt"></i>                           
                        @else
                        <p id="flagcount{{$performer->id}}">0</p>
                        <i class="far fa-flag-alt"></i>                                
                        @endif                                  

                        @endif 
                    </p>
                    <a href="javascript:void(0)" onclick="ShowAddFlagForm('{{$performer->id}}')"><i class="fal fa-plus-square"></i> Add flag</a>
                </div>
            </div>
            <div id="flagFormShow{{$performer->id}}" class="addflaginselbox hidden">
                <form id="flagSaveForm">
                    @csrf 
                <input type="hidden" value="{{$performer->id}}" name="performer_id">                    
                <div class="addflaginsetbox1">
                    <div class="seletcustboxpart">
                        <select id="flagSelect" name="flag_id" class="selectField flagSelect">
                            <option></option>
                            @if($flag)
                            @foreach($flag as $val)
                            <option value="{{$val->id}}">{{$val->flag_name}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="addflaginsetbox1">
                    <div class="form-group">   
                      <input name="note" type="text" class="form-control" placeholder="Note">                
                        
                    </div>
                </div>
                <div class="addflaginsetbox1">
                    <div class="form-group">                   
                      <input value="{{$performer->full_name}}" readonly="" type="text" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="addflgsave">
                    <a href="javascript:void(0)" onclick="FlagSave('{{$performer->id}}')"><i class="far fa-save"></i></a>
                </div>
            </form>
            </div>
            <div class="aufddockbox">
                <div class="aufddockbox_left">
                    <p>Documents</p>
                </div>
                <div class="aufddockbox_right textredcol">
                    <p>Incomplete</p>
                </div>
            </div>
            <div class="carpawboxset">
                <div class="leftcarandpatcin">
                     @if($performer->performerrental)
                    @foreach($performer->performerrental as $val)
                   
                        @if($val->rental_title == "My Vehicle")
                         @if(count($val->rentalMedia)) 
                        <a href="javascript:void(0)"><i class="fas fa-car-side"></i></a>
                        @endif
                        @endif
                        @if($val->rental_title == "My Pet")
                         @if(count($val->rentalMedia)) 
                        <a href="javascript:void(0)"><i class="fas fa-paw"></i></a>
                        @endif
                        @endif
                    @endforeach
                @endif 
                </div>
                <div class="rightviewprobtn">
                    <a href="{{route('internal-staff.portfolio', ['id' => $performer->id])}}">View Profile <i class="far fa-long-arrow-alt-right"></i></a>
                </div>
            </div>
        </div>
        <div class="acoddetabox acodboxcustset amralldetainrside">
            <div id="accordion" class="panel-group">

                <div class="panel">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h3><i class="far fa-user"></i> Appearance</h3>
                            <a href="#panelBody1" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">
                                <span class="showtextset">Show</span> 
                                <span class="hidetextset">Hide</span> 
                                <i class="far fa-angle-down"></i>
                            </a>
                        </div>
                    </div>
                    <div id="panelBody1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="measutmtboxdit">

                                <div class="hafboxmestmain">
                                    <div class="hafboxmestiner">
                                        <div class="hafboxmest_left">
                                            <p>Eyes</p>
                                        </div>
                                        <div class="hafboxmest_right">
                                            <p id="eyesDetail">
                                                @if(isset($performer->appearance[0]['eyes']->eyes))
                                                {{$performer->appearance[0]['eyes']->eyes}}
                                                @else
                                                -
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="hafboxmestiner">
                                        <div class="hafboxmest_left">
                                            <p>Glasses</p>
                                        </div>
                                        <div class="hafboxmest_right">
                                            <p id="glassesDetail">
                                               @if(isset($performer->appearance[0]->glasses))
                                               {{$performer->appearance[0]->glasses}}
                                               @else
                                                -
                                               @endif
                                           </p>
                                       </div>
                                   </div>
                                   <div class="hafboxmestiner">
                                    <div class="hafboxmest_left">
                                        <p>Contacts</p>
                                    </div>
                                    <div class="hafboxmest_right">
                                        <p id="ContactsDetail">
                                            @if(isset($performer->appearance[0]->contacts))
                                            {{$performer->appearance[0]->contacts}}
                                            @else
                                                -
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="hafboxmestiner">
                                    <div class="hafboxmest_left">
                                        <p>Hair Length</p>
                                    </div>
                                    <div class="hafboxmest_right">
                                        <p id="HairLengthDetail">

                                          @if(isset($performer->appearance[0]['hairlength']->hair_length))
                                          {{$performer->appearance[0]['hairlength']->hair_length}}
                                          @else
                                                -
                                          @endif
                                      </p>
                                  </div>
                              </div>
                              <div class="hafboxmestiner">
                                <div class="hafboxmest_left">
                                    <p>Hair Colour</p>
                                </div>
                                <div class="hafboxmest_right">
                                    <p id="HairColorName">
                                       @if(isset($performer->appearance[0]['haircolor']->hair_color))
                                       {{$performer->appearance[0]['haircolor']->hair_color}}
                                       @else
                                                -
                                       @endif                
                                   </p>
                               </div>
                           </div>
                           <div class="hafboxmestiner">
                            <div class="hafboxmest_left">
                                <p>Skin Tone</p>
                            </div>
                            <div class="hafboxmest_right">
                                <p id="skintoneName">
                                    @if(isset($performer->appearance[0]['skintone']->skin_tone))
                                    {{$performer->appearance[0]['skintone']->skin_tone}}
                                    @else
                                                -
                                    @endif                  
                                </p>
                            </div>
                        </div>                       
                    </div>
                    <div class="hafboxmestmain">                        
                        <div class="hafboxmestiner">
                            <div class="hafboxmest_left">
                                <p>Nationality</p>
                            </div>
                            <div class="hafboxmest_right">
                                <p id="nationalityName">
                                    @if(isset($performer->appearance[0]['nationality']->nationality))
                                    {{$performer->appearance[0]['nationality']->nationality}}
                                    @else
                                                -
                                    @endif
                                </p>
                            </div>
                        </div>
                         <div class="hafboxmestiner">
                            <div class="hafboxmest_left">
                                <p>Region</p>
                            </div>
                            <div class="hafboxmest_right">
                                <p id="nationalityName">
                                    @if(isset($performer->appearance[0]['region']->region))
                                    {{$performer->appearance[0]['region']->region}}
                                    @else
                                                -
                                    @endif
                                </p>
                            </div>
                        </div>

                        <div class="hafboxmestiner">
                            <div class="hafboxmest_left">
                                <p>Gender</p>
                            </div>
                            <div class="hafboxmest_right">
                                <p id="genderValue">
                                 @if(isset($performer->gender))
                                 {{$performer->gender}}
                                 @endif
                             </p>
                         </div>
                     </div>
                     <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Ethnicity</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="ethnicityValue">
                                @if(isset($performer->appearance[0]['ethnicity']->ethnicity))
                                {{$performer->appearance[0]['ethnicity']->ethnicity}}
                                @else
                                                -
                                @endif 
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Handedness</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="handednessValue">
                                @if(isset($performer->appearance[0]['handedness']->handedness))
                                {{$performer->appearance[0]['handedness']->handedness}}
                                @else
                                                -
                                @endif 
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Gloves</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="handednessValue">
                                @if(isset($performer->appearance[0]['gloves']))
                                {{$performer->appearance[0]['gloves']}}
                                @else
                                                -
                                @endif 
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Tattos</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">
                                
                             @if(isset($performer->appearance[0]->tattoos))
                                {{$performer->appearance[0]->tattoos}}
                                @else
                                                -
                                @endif
                            </p>
                        </div>
                    </div>

                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Doubles For</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">
                                
                             @if(isset($performer->appearance[0]->double_for))
                                {{$performer->appearance[0]->double_for}}
                                @else
                                                -
                                @endif
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">
        <div class="panel-title">
            <h3><i class="far fa-ruler"></i> Measurements</h3>
            <a href="#panelBody2" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">
                <span class="showtextset">Show</span> 
                <span class="hidetextset">Hide</span> 
                <i class="far fa-angle-down"></i>
            </a>
        </div>
    </div>
    <div id="panelBody2" class="panel-collapse collapse">
        <div class="panel-body">
            <div class="measutmtboxdit">
                <div class="hafboxmestmain">
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Height</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="heightValue">
                                 @if(isset($performer->appearance[0]->height))
                                {{$performer->appearance[0]->height}}
                                 @else
                                    -
                                @endif 
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Weight</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="weightValue">
                                 @if(isset($performer->appearance[0]->weight))
                                {{$performer->appearance[0]->weight}}
                                 @else
                                    -
                                @endif 
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Bust</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="bustValue">
                                @if(isset($performer->appearance[0]->bust))
                                {{$performer->appearance[0]->bust}}
                                 @else
                                                -
                                @endif 
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Waist</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="waistValue">
                                @if(isset($performer->appearance[0]->waist))
                                {{$performer->appearance[0]->waist}}
                                 @else
                                    -
                                @endif 
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Hips</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="hipValue">
                                @if(isset($performer->appearance[0]->hips))
                                {{$performer->appearance[0]->hips}}
                                @else
                                    -
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Jacket</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->jacket))
                                {{$performer->appearance[0]->jacket}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                     <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Chest</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->chest))
                                {{$performer->appearance[0]->chest}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Dress</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->dress))
                                {{$performer->appearance[0]->dress}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Collar</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->collar))
                                {{$performer->appearance[0]->collar}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Sleeve</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->sleeve))
                                {{$performer->appearance[0]->sleeve}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="hafboxmestmain">
                    
                   
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Pant</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->pant))
                                {{$performer->appearance[0]->pant}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Jeans size</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->jeans))
                                {{$performer->appearance[0]->jeans}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Inseam</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->inseam))
                                {{$performer->appearance[0]->inseam}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Hat</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->hat))
                                {{$performer->appearance[0]->hat}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Hat</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->hat))
                                {{$performer->appearance[0]->hat}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Ring</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->ring))
                                {{$performer->appearance[0]->ring}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Shoes</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->shoes))
                                {{$performer->appearance[0]->shoes}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Gloves Size</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->gloves))
                                {{$performer->appearance[0]->gloves}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Child Size</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->child_size))
                                {{$performer->appearance[0]->child_size}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="hafboxmestiner">
                        <div class="hafboxmest_left">
                            <p>Infant Size</p>
                        </div>
                        <div class="hafboxmest_right">
                            <p id="tattoosValue">                                
                             @if(isset($performer->appearance[0]->infant_size))
                                {{$performer->appearance[0]->infant_size}}
                                @else
                                 -
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">
        <div class="panel-title">
            <h3><i class="far fa-times-octagon"></i> Restrictions</h3>          
            <a href="#panelBody3" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">
                <span class="showtextset">Show</span> 
                <span class="hidetextset">Hide</span> 
                <i class="far fa-angle-down"></i>
            </a>
        </div>
    </div>
    <div id="panelBody3" class="panel-collapse collapse">
        <div class="panel-body">
            <div class="gsinboxmaincl">
                <div class="gsinboxlr">
                    <div class="inergsinboxlr">
                        @if(isset($performer->restriction))
                        <h4>General</h4>
                        @foreach($performer->restriction as $value)
                        @if($value->restrictionName->restrictions_type == "General")
                            <p>{{$value->restrictionName->restriction_name}}</p>
                        @endif
                        @endforeach
                        @endif
                    </div>
                </div>
                <div class="gsinboxlr">
                    <div class="inergsinboxlr">
                        @if(isset($performer->restriction))
                        <h4>Smoking</h4>
                        @foreach($performer->restriction as $value)
                        @if($value->restrictionName->restrictions_type == "Smoking")
                            <p>{{$value->restrictionName->restriction_name}}</p>
                        @endif
                        @endforeach
                        @endif
                    </div>
                    <div class="inergsinboxlr">
                        @if(isset($performer->restriction))
                        <h4>Interactions</h4>
                        @foreach($performer->restriction as $value)
                        @if($value->restrictionName->restrictions_type == "Interactions")
                            <p>{{$value->restrictionName->restriction_name}}</p>
                        @endif
                        @endforeach
                        @endif
                    </div>
                    <div class="inergsinboxlr">
                       @if(isset($performer->nudity))
                       <h4>Nudity</h4>
                       @foreach($performer->nudity as $value)                        
                       <p>{{$value->nudityName->nudity_name}}</p>                       
                       @endforeach
                       @endif
                    </div>
                </div>
            </div>

            <div class="wardrobeboxche hidden">
                <?php $i =0;
                $res= round(count($restriction)/2); 
                ?>


                @foreach($restriction as $restrict)
                <?php if($i==0 || $i == $res ){ ?>
                    <div class="hacinuboxcheck">
                    <?php } ?>


                    <div class="cutmbocheck">
                        <label class="checkbox-container">{{$restrict->restriction_name}}
                            <input type="checkbox" name="restriction_id[]" value="{{$restrict->id}}" class="restriction" 
                              @if(isset($performer->restriction))
                              @foreach($performer->restriction as $restrictperformer) 
                            {{($restrict->id==$restrictperformer->restriction_id)?'checked':''}}
                                @endforeach
                                  @endif
                            >
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <?php if($res-1 == $i || $i == count($restriction)-1){ ?>
                    </div>

                <?php } $i++; ?>
                @endforeach

                 <div class="hacinuboxcheck">
                        <h3 class="titleboxiner">Nudity</h3>
                         @foreach($nudity as $nud)
                        <div class="cutmbocheck">
                          <label class="checkbox-container"> {{$nud->nudity_name}}
                            <input type="checkbox" name="{{$nud->nudity_name}}" value="{{$nud->id}}" class="nudity"
                                @if(isset($nudityperformer))
                              @foreach($nudityperformer as $nudperformer) 
                            {{($nud->id==$nudperformer->nudity_id)?'checked':''}}
                                @endforeach
                                  @endif
                            >
                            <span class="checkmark"></span>
                          </label>
                        </div>
                        @endforeach
                      </div>

            </div>
        </div>
    </div>
</div>



</div>
</div>
</div>
</div>      
