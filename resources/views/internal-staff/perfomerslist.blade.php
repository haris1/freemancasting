@extends('internal-staff.layouts.app')
@section('content')
@if(isset($search_parameter))    
<input type="hidden" name="" id="submitForm" value="1">
@else
<input type="hidden" name="" id="submitForm" value="0">
@endif

<div class="databaseboxalldeta">
    <div class="databasesearch">
        <div class="searchbox1">
            <div class="form-group">
                <i class="far fa-search"></i>
                <input type="text" class="form-control" id="search" name="search" placeholder="Search by Name or Group,Tags and Attributes">        
            </div>
            <div class="clearbtnboxright">
                <a href="" onClick="window.location.reload();"><i class="far fa-trash"></i><span>Clear all</span></a>
            </div>
            <!-- <a href="javascript:void(0)" class="active" type="button" data-toggle="modal" data-target="#PerformerModel">Create Performer</a> -->
        </div>
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">

               <div class="panel-heading">


                <div class="searchbox2">
                    <div class="inpuandsecleboxwi">
                        <div class="seltinpiut">
                            <div class="semselectdis">
                                <p>Gender</p>

                                <div class="seletcustboxpart">
                                    <select class="selectField selectSearchField"  data-reid="genderSelect" id="gender">
                                        <option value="" <?php echo (isset($search_parameter['gender'])&&$search_parameter['gender'] == '')?'selected':''?>></option>
                                        <option value="Male"  <?php echo (isset($search_parameter['gender'])&&$search_parameter['gender'] == 'Male')?'selected':''?>>Male</option>
                                        <option value="Female" <?php echo (isset($search_parameter['gender'])&&$search_parameter['gender'] == 'Female')?'selected':''?>>Female</option>
                                        <option value="Transgender" <?php echo (isset($search_parameter['gender'])&&$search_parameter['gender'] == 'Transgender')?'selected':''?>>Transgender</option>
                                    </select>
                                </div>
                                <div class="cleardetabox hidden" id="genderSelect">
                                  <a href="javascript:void(0)" class="removeOption" data-selectid='gender'><i class="fal fa-times"></i></a>
                              </div>
                          </div>          
                      </div>
                      <div class="seltinpiut">
                        <div class="semselectdis">
                            <p>Ethinicity</p>

                            <div class="seletcustboxpart">
                                <select class="selectField selectSearchField" data-reid="ethnicitySelect" id="ethnicity">

                                    <option></option>
                                    @foreach ($Ethnicity as $value)
                                    <option value="{{ $value->id }}" <?php echo (isset($search_parameter['ethnicity']) && $search_parameter['ethnicity'] == $value->id) ? 'selected':''?>>{{$value->ethnicity}}
                                    </option>
                                    @endforeach                   
                                </select>
                            </div>
                            <div class="cleardetabox hidden" id="ethnicitySelect">
                                <a href="javascript:void(0)"  class="removeOption" data-selectid='ethnicity'><i class="fal fa-times"></i></a>
                            </div>
                        </div>  
                    </div>  
               <!--  <div class="seltinpiut">
                    <div class="semselectdis">
                        <p>Age</p>
                        <div class="seletcustboxpart">
                            <select class="selectField selectSearchField" data-reid="ageSelect" id="age">
                                <option value=""></option>
                                <option>Age</option>
                            </select>
                        </div>
                        <div class="cleardetabox hidden" id="ageSelect">
                            <a  class="removeOption" data-selectid='age' href="javascript:void(0)"><i class="fal fa-times"></i></a>
                        </div>
                    </div>  
                </div>  -->
                <div class="inerfiltbox ageinputtextset">
                    <div class="semselectdis">
                        <p>Age</p>
                        <div class="inputiner2">
                            <div class="form-group">
                                <input type="number" value="{{ isset($search_parameter['age']['age_minval']) ? $search_parameter['age']['age_minval'] : ''}}" class="form-control ageclr" id="age_minval" name="age_min" placeholder="Min" min="1" oninput="validity.valid||(value='');">
                            </div>
                        </div>
                        <div class="inputiner2">
                            <div class="form-group">
                                <input type="number" class="form-control ageclr age_maxFocus" id="age_maxval" value="{{ isset($search_parameter['age']['age_maxval']) ? $search_parameter['age']['age_maxval'] : ''}}"  name="age_max"  placeholder="Max" min="1" oninput="validity.valid||(value='');">
                            </div>
                        </div>
                    </div>
                </div>   
                <div class="seltinpiut">
                    <div class="semselectdis">
                        <p>Union</p>
                        <div class="seletcustboxpart">
                            <select class="selectField selectSearchField" id="union" name="union" data-reid="unionSelect">
                                <option></option>                                
                                @foreach ($performerunion as $value)
                                <option value="{{$value->id}}">{{$value->union}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="cleardetabox hidden" id="unionSelect">
                            <a class="removeOption" data-selectid='union' href="javascript:void(0)"><i class="fal fa-times"></i></a>
                        </div>
                    </div>  
                </div>    

                 <div class="seltinpiut">

                    <div class="semselectdis">                                      
                        <p>Availability</p>
                        <div class="seletcustboxpart"> 
                            <select class="selectField selectSearchField" id="availability" data-reid="availabilitySelect">
                                <option value=""></option>
                                @foreach ($performeravailability as $value)
                                <option <?php echo (isset($search_parameter['availability']) && $search_parameter['availability'] == $value->id) ? 'selected':''?> value="{{ $value->id }}">{{$value->availability}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="cleardetabox hidden" id="availabilitySelect">
                            <a class="removeOption" data-selectid='availability' href="javascript:void(0)"><i class="fal fa-times"></i></a>
                        </div>
                    </div>  

                </div>   
                <div class="seltinpiut">
                    <div class="semselectdis">
                        <p>Region</p>
                        <div class="seletcustboxpart">
                            <select class="selectField selectSearchField" data-reid="regionSelect" id="region">
                                <option value=""></option>
                                @foreach ($Region as $val)
                                <option <?php echo (isset($search_parameter['region']) && $search_parameter['region'] == $val->id) ? 'selected':''?> value="{{$val->id}}">{{$val->region}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="cleardetabox hidden" id="regionSelect">
                            <a class="removeOption" data-selectid='region' href="javascript:void(0)"><i class="fal fa-times"></i></a>
                        </div>
                    </div>  
                </div>
                
            </div>
            <div class="advanced_searchbox">
                <!-- <a href="javascript:void(0)" type="button" data-toggle="modal" data-target="#myModal2">Advanced search</a> -->
                <a data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse1">
                    <i class="far fa-angle-down"></i><span>Advanced Filters</span>
                </a>
            </div>
            <!-- <div class="savrandsrcrbtn"> 
                @if(isset($search_parameter)) 
                <button onclick="window.location='{{ URL::route('internal-staff.performers.list') }}'"
                 >Clear All</button>
                @else
                 <button  onclick="location.reload();">Clear All</button>
                @endif                  
                
                <button data-toggle="modal" data-target="#serchresultModal">Save Search Result</button>
            </div> -->
        </div>
    </div>
    <div class="botadssearchbox">
        <div id="collapse1" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="SugestBodymainQutbox">
                    <div class="inercolselfiltrt">
                        <div class="accordion">
                            <div class="acoinerboxcoverbox">                                            
                                <div class="accordion-content" style="display: block;">
                                    <div class="alboxcovrfull">
                                        <div class="vmgeaminmaxclmo_tbl_iner1">
                                            <div class="lictebox_setin">
                                                <i class="far fa-briefcase"></i>
                                                <p>Administrative</p>
                                            </div>
                                        </div>
                                        <form id="advanced_search_form" method="post">
                                            <input type="hidden" id="advanceViewformat" value="grid" name="viewformat">
                                            <div class="peunagpeallselbox1">
                                                <div class="semselectdis">
                                                    <p>Performance Category</p>
                                                    <div class="seletcustboxpart">
                                                        <select id="Performancecategory" name="applicationfor" class="custselectbox1">
                                                            <option></option>
                                                            <option value="Child">Child</option>
                                                            <option value="Adult">Adult</option>
                                                            <option value="Group">Group</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="peunagpeallselbox1">
                                                <div class="semselectdis">
                                                    <p>Union</p>
                                                    <div class="seletcustboxpart">
                                                        <select class="custselectbox1" name="union" id="union2">

                                                            <option></option>
                                                            @foreach ($performerunion as $value)
                                                            <option value="{{$value->id}}">{{$value->union}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="peunagpeallselbox1">
                                                <div class="semselectdis">
                                                    <p>Agency</p>
                                                    <div class="seletcustboxpart">
                                                        <select name="agency_id" class="custselectbox1" id="agency">
                                                            <option></option>
                                                            @foreach ($agency as $value)
                                                            <option value="{{$value->id}}">{{$value->agencyName}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="peunagpeallselbox1">
                                                <div class="semselectdis">
                                                    <p>Performer Status</p>
                                                    <div class="seletcustboxpart">
                                                        <select name="status_id" class="custselectbox1" id="performancestatus">
                                                            <option value=""></option>
                                                            @foreach ($perfomerstatus as $status)
                                                            <option value="{{$status->id}}">{{$status->performer_status}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>  

                                <div class="acoinerboxcoverbox">
                                            <!-- <div class="accordion-header">
                                                <div class="alliocnandtextboxset">
                                                    <i class="far fa-user"></i>
                                                    <p>Appearance</p>
                                                </div>
                                            </div> -->
                                            <div class="accordion-content" style="display: block;">
                                                <div class="alboxcovrfull">
                                                    <div class="vmgeaminmaxclmo_tbl_iner1">
                                                        <div class="lictebox_setin">
                                                            <i class="far fa-ruler"></i>
                                                            <p>Measurements</p> 
                                                        </div>
                                                    </div>
                                                    <div class="peunagpeallselbox2">
                                                        <div class="form-group">
                                                            <label for="">Height (in)</label>
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control heightclr" id="" name="height_min" placeholder="Min"
                                                                    value="{{ isset($search_parameter['height_min']) ? $search_parameter['height_min'] : ''}}"
                                                                    >
                                                                </div>
                                                            </div>
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control heightclr"  name="height_max" placeholder="Max"
                                                                    value="{{ isset($search_parameter['height_max']) ? $search_parameter['height_max'] : ''}}"
                                                                    >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="peunagpeallselbox2">
                                                        <div class="form-group">
                                                            <label for="">Weight (lbs)</label>
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control weightclr" id="" name="weight_min" placeholder="Min" 
                                                                    value="{{ isset($search_parameter['weight_min']) ? $search_parameter['weight_min'] : ''}}" 
                                                                    >
                                                                </div>
                                                            </div>
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control weightclr"  name="weight_max" placeholder="Max"

                                                                    value="{{ isset($search_parameter['weight_max']) ? $search_parameter['weight_max'] : ''}}"
                                                                    >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="peunagpeallselbox2">
                                                        <div class="form-group">
                                                            <label for="">Chest</label>
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control chestclr" id="" name="chest_min"  placeholder="Min"
                                                                    value="{{ isset($search_parameter['chest_min']) ? $search_parameter['chest_min'] : ''}}"
                                                                    >
                                                                </div>
                                                            </div>
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control chestclr"  name="chest_max" placeholder="Max"
                                                                    value="{{ isset($search_parameter['chest_max']) ? $search_parameter['chest_max'] : ''}}"
                                                                    >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="peunagpeallselbox2">
                                                        <div class="form-group">
                                                            <label for="">Waist</label>
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control waistclr" id="" name="waist_min" placeholder="Min" 
                                                                    value="{{ isset($search_parameter['waist_min']) ? $search_parameter['waist_min'] : ''}}"
                                                                    >
                                                                </div>
                                                            </div>
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control waistclr"  name="waist_max" placeholder="Max"
                                                                    value="{{ isset($search_parameter['waist_max']) ? $search_parameter['waist_max'] : ''}}"
                                                                    >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="peunagpeallselbox2">
                                                        <div class="form-group">
                                                            <label for="">Hip</label>
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control hipsclr" id="" name="hip_min" placeholder="Min"
                                                                    value="{{ isset($search_parameter['hip_min']) ? $search_parameter['hip_min'] : ''}}"
                                                                    >
                                                                </div>
                                                            </div>
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control hipsclr"  name="hip_max" placeholder="Max"
                                                                    value="{{ isset($search_parameter['hip_max']) ? $search_parameter['hip_max'] : ''}}"
                                                                    >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="peunagpeallselbox2">
                                                        <div class="semselectdis">
                                                            <p>Dress</p>
                                                            
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control dressclr" id="" name="dress_min" placeholder="Min"
                                                                    value="{{ isset($search_parameter['dress_min']) ? $search_parameter['dress_min'] : ''}}"
                                                                    >
                                                                </div>
                                                            </div>
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control dressclr"  name="dress_max" placeholder="Max"
                                                                    value="{{ isset($search_parameter['dress_max']) ? $search_parameter['dress_max'] : ''}}"
                                                                    >
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="peunagpeallselbox2">
                                                        <div class="semselectdis">
                                                            <p>Collar</p>
                                                            
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control collarclr" id="" name="collar_min" placeholder="Min"

                                                                    value="{{ isset($search_parameter['collar_min']) ? $search_parameter['collar_min'] : ''}}"
                                                                    >
                                                                </div>
                                                            </div>
                                                            <div class="inputiner2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control collarclr"  name="collar_max" placeholder="Max"
                                                                    value="{{ isset($search_parameter['collar_max']) ? $search_parameter['collar_max'] : ''}}"
                                                                    >
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>                                                    
                                                </div>

                                                <div class="ineracodbox2">
                                                    <div class="blnkdivbox">

                                                    </div>
                                                    <div class="accordion-container">
                                                        <div class="set hedtobox">

                                                          <!--   <a href="javascript:void(0)">
                                                                <span class="moretext">More appearance</span>
                                                                <span class="lesstext">Less measurements</span>
                                                                <i class="far fa-angle-down"></i>
                                                            </a> -->
                                                            
                                                            <div class="content" style="display: inline-block !important; ">
                                                                <div class="ineracod_appearance">
                                                                    <div class="peunagpeallselbox3">
                                                                        <div class="semselectdis">
                                                                            <p>Sleeve</p>
                                                                            
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control sleeveclr" id="" name="sleeve_min" placeholder="Min"

                                                                                    value="{{ isset($search_parameter['sleeve_min']) ? $search_parameter['sleeve_min'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control sleeveclr"  name="sleeve_max" placeholder="Max"

                                                                                    value="{{ isset($search_parameter['sleeve_max']) ? $search_parameter['sleeve_max'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox3">
                                                                        <div class="semselectdis">
                                                                            <p>Pant</p>
                                                                            
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control pantclr" id="" name="pant_min" placeholder="Min"

                                                                                    value="{{ isset($search_parameter['pant_min']) ? $search_parameter['pant_min'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control pantclr"  name="pant_max" placeholder="Max"

                                                                                    value="{{ isset($search_parameter['pant_max']) ? $search_parameter['pant_max'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox3">
                                                                        <div class="semselectdis">
                                                                            <p>Jean Size</p>
                                                                            
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control jeansclr" id="" name="jeansize_min" placeholder="Min"
                                                                                    value="{{ isset($search_parameter['jeansize_min']) ? $search_parameter['jeansize_min'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control jeansclr"  name="jeansize_max" placeholder="Max"
                                                                                    value="{{ isset($search_parameter['jeansize_max']) ? $search_parameter['jeansize_max'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox3">
                                                                        <div class="semselectdis">
                                                                            <p>Inseam</p>
                                                                            
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control inseamclr" id="" name="inseam_min" placeholder="Min"

                                                                                    value="{{ isset($search_parameter['inseam_min']) ? $search_parameter['inseam_min'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control inseamclr"  name="inseam_max" placeholder="Max"
                                                                                    value="{{ isset($search_parameter['inseam_max']) ? $search_parameter['inseam_max'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox3">
                                                                        <div class="semselectdis">
                                                                            <p>Hat</p>
                                                                            
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control hatclr" id="" name="hat_min" placeholder="Min"
                                                                                    value="{{ isset($search_parameter['hat_min']) ? $search_parameter['hat_min'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control hatclr"  name="hat_max" placeholder="Max"
                                                                                    value="{{ isset($search_parameter['hat_max']) ? $search_parameter['hat_max'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox3">
                                                                        <div class="semselectdis">
                                                                            <p>Ring</p>
                                                                            
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control ringclr" id="" name="ring_min" placeholder="Min"
                                                                                    value="{{ isset($search_parameter['ring_min']) ? $search_parameter['ring_min'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control ringclr"  name="ring_max" placeholder="Max"
                                                                                    value="{{ isset($search_parameter['ring_max']) ? $search_parameter['ring_max'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox3">
                                                                        <div class="semselectdis">
                                                                            <p>Bust</p>
                                                                            
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control bustclr" id="" name="bust_min" placeholder="Min"

                                                                                    value="{{ isset($search_parameter['bust_min']) ? $search_parameter['bust_min'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control bustclr"  name="bust_max" placeholder="Max"

                                                                                    value="{{ isset($search_parameter['bust_max']) ? $search_parameter['bust_max'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="ineracod_appearance">
                                                                    <div class="peunagpeallselbox3">
                                                                        <div class="semselectdis">
                                                                            <p>Shoes</p>

                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control shoesclr" id="" name="shoes_min" placeholder="Min"

                                                                                    value="{{ isset($search_parameter['shoes_min']) ? $search_parameter['shoes_min'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>
                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control shoesclr"  name="shoes_max" placeholder="Max"

                                                                                    value="{{ isset($search_parameter['shoes_max']) ? $search_parameter['shoes_max'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox3">
                                                                        <div class="semselectdis">
                                                                            <p>Jacket</p>

                                                                            <div class="inputiner2">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control jacketclr" id="" name="jacket_min" placeholder="Min"

                                                                                    value="{{ isset($search_parameter['jacket_min']) ? $search_parameter['jacket_min'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>
                                                                            <div class="inputiner2">
                                                                                <div class="form-group ">
                                                                                    <input type="text" class="form-control jacketclr"  name="jacket_max" placeholder="Max"

                                                                                    value="{{ isset($search_parameter['jacket_max']) ? $search_parameter['jacket_max'] : ''}}"
                                                                                    >
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                        <div class="peunagpeallselbox3">
    <div class="semselectdis">
        <p>Child size</p>

        <div class="inputiner2">
            <div class="form-group">
                <input type="text" class="form-control childsizeclr" id="" name="child_size_min" placeholder="Min"
                value="{{ isset($search_parameter['child_size_min']) ? $search_parameter['child_size_min'] : ''}}"
                >
            </div>
        </div>
        <div class="inputiner2">
            <div class="form-group ">
                <input type="text" class="form-control childsizeclr"  name="child_size_max" placeholder="Max"

                value="{{ isset($search_parameter['child_size_max']) ? $search_parameter['child_size_max'] : ''}}"
                >
            </div>
        </div>

    </div>
</div>

<div class="peunagpeallselbox3">
    <div class="semselectdis">
        <p>Infant size</p>

        <div class="inputiner2">
            <div class="form-group">
                <input type="text" class="form-control infantsizeclr" id="" name="infant_size_min" placeholder="Min"
                value="{{ isset($search_parameter['infant_size_min']) ? $search_parameter['infant_size_min'] : ''}}"
                >
            </div>
        </div>
        <div class="inputiner2">
            <div class="form-group ">
                <input type="text" class="form-control infantsizeclr"  name="infant_size_max" placeholder="Max"

                value="{{ isset($search_parameter['infant_size_max']) ? $search_parameter['infant_size_max'] : ''}}"
                >
            </div>
        </div>

    </div>
</div>




                                                                    <div class="peunagpeallselbox3">
                                                                        <div class="semselectdis">
                                                                            <p>Glove Size</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="selectField custselectbox1" data-placeholder="Select gloves" name="gloves" id="gloves">
                                                                                 <option></option>
                                                                                 <option value="XXS" {{isset($appearance)&&($appearance->gloves=="XXS")?'Selected':''}}>XXS</option>
                                                                                 <option value="XS" {{isset($appearance)&&($appearance->gloves=="XS")?'Selected':''}}>XS</option>
                                                                                 <option value="S" {{isset($appearance)&&($appearance->gloves=="S")?'Selected':''}}>S</option>
                                                                                 <option value="M" {{isset($appearance)&&($appearance->gloves=="M")?'Selected':''}}>M</option>
                                                                                 <option value="L" {{isset($appearance)&&($appearance->gloves=="L")?'Selected':''}}>L</option>
                                                                                 <option value="XL" {{isset($appearance)&&($appearance->gloves=="XL")?'Selected':''}}>XL</option>
                                                                             </select>
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                






                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="acoinerboxcoverbox">
                                        <div class="accordion-header">
                                            <div class="alliocnandtextboxset">
                                                    <!-- <i class="far fa-ruler"></i>
                                                        <p>Measurements</p> -->
                                                    </div>
                                                </div>
                                                <div class="accordion-content" style="display: block !important;">
                                                    <div class="alboxcovrfull">
                                                        <div class="vmgeaminmaxclmo_tbl_iner1">
                                                            <div class="lictebox_setin">
                                                                <i class="far fa-user"></i>
                                                                <p>Appearance</p>
                                                            </div>
                                                        </div>                                                    
                                                        <div class="peunagpeallselbox1">
                                                            <div class="semselectdis">
                                                                <p>Hair Colour</p>
                                                                <div class="seletcustboxpart">
                                                                    <select class="custselectbox1" id="haircolor" name="haircolor">
                                                                        <option value=""></option>
                                                                        @foreach ($haircolor as $val)
                                                                        <option <?php echo (isset($search_parameter['haircolor']) && $search_parameter['haircolor'] == $val->id) ? 'selected':''?> value="{{$val->id}}">{{$val->hair_color}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="peunagpeallselbox1">
                                                            <div class="semselectdis">
                                                                <p>Skin Tone</p>
                                                                <div class="seletcustboxpart">
                                                                    <select class="custselectbox1" id="skintone" name="skintone">
                                                                        <option value=""></option>
                                                                        @foreach ($skintone as $val)
                                                                        <option <?php echo (isset($search_parameter['skintone']) && $search_parameter['skintone'] == $val->id) ? 'selected':''?> value="{{$val->id}}">{{$val->skin_tone}}</option>
                                                                        @endforeach

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="peunagpeallselbox1">
                                                            <div class="semselectdis">
                                                                <p>Facial Hair</p>
                                                                <div class="seletcustboxpart">
                                                                   <select class="custselectbox1" id="hairfacial" name="hairfacial">
                                                                    <option value=""></option>
                                                                    @foreach ($hairfacial as $val)
                                                                    <option <?php echo (isset($search_parameter['hairfacial']) && $search_parameter['hairfacial'] == $val->id) ? 'selected':''?> value="{{$val->id}}">{{$val->hair_facial}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="peunagpeallselbox1">
                                                        <div class="semselectdis">
                                                            <p>Hair Length</p>
                                                            <div class="seletcustboxpart">
                                                                <select id="hairlength" name="hairlength" class="custselectbox1">
                                                                    <option value=""></option>
                                                                    @foreach ($hairLength as $val)
                                                                    <option <?php echo (isset($search_parameter['hairLength']) && $search_parameter['hairLength'] == $val->id) ? 'selected':''?> value="{{$val->id}}">{{$val->hair_length}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="peunagpeallselbox1">
                                                        <div class="semselectdis">
                                                            <p>Body Traits</p>
                                                            <div class="seletcustboxpart">
                                                             <select class="custselectbox1" id="bodytrait" name="bodytrait">
                                                                <option value=""></option>
                                                                @foreach ($bodytrait as $val) 
                                                                <option <?php echo (isset($search_parameter['bodytrait']) && $search_parameter['bodytrait'] == $val->id) ? 'selected':''?> value="{{$val->id}}">{{$val->body_trait}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>                                                    
                                            </div>

                                            <div class="ineracodbox2">
                                                <div class="blnkdivbox">

                                                </div>
                                                <div class="accordion-container">
                                                    <div class="set hedtobox">

                                                          <!--   <a href="javascript:void(0)">
                                                                <span class="moretext">More appearance</span>
                                                                <span class="lesstext">Less measurements</span>
                                                                <i class="far fa-angle-down"></i>
                                                            </a> -->
                                                            
                                                            <div class="content" style="display: block !important;">
                                                                <div class="ineracod_appearance">
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Hair Style</p>
                                                                            <div class="seletcustboxpart">
                                                                              <select class="custselectbox1" id="hairstyle" name="hairstyle">
                                                                                <option value=""></option>
                                                                                @foreach ($hairstyle as $val)
                                                                                <option <?php echo (isset($search_parameter['hairstyle']) && $search_parameter['hairstyle'] == $val->id) ? 'selected':''?> value="{{$val->id}}">{{$val->hair_style}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="peunagpeallselbox4">
                                                                    <div class="semselectdis">
                                                                        <p>Eye Colour</p>
                                                                        <div class="seletcustboxpart">
                                                                         <select class="custselectbox1" id="eyes" name="eyes">
                                                                            <option value=""></option>
                                                                            @foreach ($eyes as $val)
                                                                            <option <?php echo (isset($search_parameter['eyes']) && $search_parameter['eyes'] == $val->id) ? 'selected':''?> value="{{$val->id}}">{{$val->eyes}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="peunagpeallselbox4">
                                                                <div class="semselectdis">
                                                                    <p>Nationality</p>
                                                                    <div class="seletcustboxpart">
                                                                        <select class="custselectbox1" id="nationality" name="nationality">
                                                                            <option value=""></option>
                                                                            @foreach ($nationality as $val)
                                                                            <option <?php echo (isset($search_parameter['nationality']) && $search_parameter['nationality'] == $val->id) ? 'selected':''?> value="{{$val->id}}">{{$val->nationality}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="peunagpeallselbox4">
                                                                <div class="semselectdis">
                                                                    <p>Handedness</p>
                                                                    <div class="seletcustboxpart">
                                                                     <select class="custselectbox1" id="handedness" name="handedness"> 
                                                                        <option value=""></option>
                                                                        @foreach ($handedness as $val)
                                                                        <option <?php echo (isset($search_parameter['handedness']) && $search_parameter['handedness'] == $val->id) ? 'selected':''?> value="{{$val->id}}">{{$val->handedness}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="peunagpeallselbox4">
                                                            <div class="semselectdis">
                                                                <p>Tattos</p>
                                                                <div class="seletcustboxpart">
                                                                    <select class="custselectbox1" id="tattoos" name="tattoos">
                                                                        <option <?php echo (isset($search_parameter['tattoos']) && $search_parameter['tattoos'] == "") ? 'selected':''?> value=""></option>

                                                                        <option <?php echo (isset($search_parameter['tattoos']) && $search_parameter['tattoos'] == "Yes") ? 'selected':''?> value="Yes">Yes</option>

                                                                        <option <?php echo (isset($search_parameter['tattoos']) && $search_parameter['tattoos'] == "No") ? 'selected':''?> value="No">No</option>

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="peunagpeallselbox4">
                                                            <div class="semselectdis">
                                                                <p>Piercing</p>
                                                                <div class="seletcustboxpart">
                                                                    <select class="custselectbox1" id="piercing" name="piercing">
                                                                       <option value=""></option>
                                                                       @foreach ($piercing as $pier)
                                                                       <option <?php echo (isset($search_parameter['piercing']) && $search_parameter['piercing'] == $pier->id) ? 'selected':''?> value="{{$pier->piercing}}">{{$pier->piercing}}</option>
                                                                       @endforeach

                                                                   </select>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>

                               </div>
                           </div>  

                           <div class="acoinerboxcoverbox">                                           
                            <div class="accordion-content hidden">

                                <div class="ineracodbox3">                     
                                    <div class="accordion-container">
                                        <div class="set hedtobox">
                                         <div class="vmgeaminmaxclmo_tbl_iner1">
                                            <div class="lictebox_setin">
                                                <i class="far fa-lightbulb"></i>
                                                <p>Special Skills</p>
                                            </div>
                                        </div>
                                                            <!-- <a href="javascript:void(0)">
                                                                <span class="moretext">Show</span>
                                                                <span class="lesstext">Hide special skills</span>
                                                                <i class="far fa-angle-down"></i>
                                                            </a> -->
                                                            
                                                            <div class="content" style="display: block !important;">
                                                                <div class="ineracod_appearance">
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Qualifications</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Community Servants</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Sports</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Circus / Magic</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Culinary</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="ineracod_appearance">
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Dance</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Military</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Music</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Bands</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Specialty Driver</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="acoinerboxcoverbox">                                           
                                            <div class="accordion-content">

                                                <div class="ineracodbox3">                     
                                                    <div class="accordion-container">
                                                        <div class="set hedtobox">
                                                         <div class="vmgeaminmaxclmo_tbl_iner1">
                                                            <div class="lictebox_setin">
                                                                <i class="far fa-tshirt"></i>
                                                                <p>Wardrobe</p>
                                                            </div>
                                                        </div>
                                                            <!-- <a href="javascript:void(0)">
                                                                <span class="moretext">Show</span>
                                                                <span class="lesstext">Hide wardrobe</span>
                                                                <i class="far fa-angle-down"></i>
                                                            </a>  --> 
                                                            <div class="content">
                                                                <div class="seaaccwarpiebox">
                                                                    <div class="seaaccwarpieiner1">
                                                                        <h4>Season</h4>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input  value="Winter" type="checkbox" name='closetGallery[]' class="closetGallery" id="test2">
                                                                            <label for="test2" id="test2">Winter</label>
                                                                        </div>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input value="Spring" type="checkbox" name='closetGallery[]' class="closetGallery" id="test3">
                                                                            <label for="test3" id="test3">Spring</label>
                                                                        </div>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input value="Summer" type="checkbox" name='closetGallery[]' class="closetGallery" id="test4">
                                                                            <label for="test4" id="test4">Summer</label>
                                                                        </div>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input value="Fall" type="checkbox" name='closetGallery[]' class="closetGallery" id="test5">
                                                                            <label for="test5" id="test5">Fall</label>
                                                                        </div>
                                                                    </div>
                                                                   <!--  <div class="seaaccwarpieiner1">
                                                                        <h4>Accessories</h4>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input type="checkbox" name='test6' id="test6">
                                                                            <label for="test6" id="test6">Hat</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="seaaccwarpieiner1">
                                                                        <h4>Wardrobe Type</h4>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input type="checkbox" name='test7' id="test7">
                                                                            <label for="test7" id="test7">Item</label>
                                                                        </div>
                                                                    </div> -->
                                                                  <!--   <div class="seaaccwarpieiner1">
                                                                        <h4>Piercings</h4>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input type="checkbox" name='test8' id="test8">
                                                                            <label for="test8" id="test8">Item</label>
                                                                        </div>
                                                                    </div> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="acoinerboxcoverbox">                                           
                                            <div class="accordion-content">

                                                <div class="ineracodbox3">                     
                                                    <div class="accordion-container">
                                                        <div class="set hedtobox">
                                                            <div class="vmgeaminmaxclmo_tbl_iner1">
                                                                <div class="lictebox_setin">
                                                                    <i class="far fa-times-octagon"></i>
                                                                    <p>Restrictions</p>
                                                                </div>
                                                            </div>
                                                            <!-- <a href="javascript:void(0)">
                                                                <span class="moretext">Show</span>
                                                                <span class="lesstext">Hide Restrictions</span>
                                                                <i class="far fa-angle-down"></i>
                                                            </a>  --> 
                                                            <!-- <div class="content">
                                                                <div class="seaaccwarpiebox">
                                                                    <div class="seaaccwarpieiner1">
                                                                        <h4>General</h4>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input type="checkbox" name='test2' id="test2">
                                                                            <label for="test2" id="test2">Winter</label>
                                                                        </div>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input type="checkbox" name='test3' id="test3">
                                                                            <label for="test3" id="test3">Spring</label>
                                                                        </div>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input type="checkbox" name='test4' id="test4">
                                                                            <label for="test4" id="test4">Summer</label>
                                                                        </div>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input type="checkbox" name='test5' id="test5">
                                                                            <label for="test5" id="test5">Fall</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="seaaccwarpieiner1">
                                                                        <h4>Smoking</h4>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input type="checkbox" name='test6' id="test6">
                                                                            <label for="test6" id="test6">Hat</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="seaaccwarpieiner1">
                                                                        <h4>Interactions</h4>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input type="checkbox" name='test7' id="test7">
                                                                            <label for="test7" id="test7">Item</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="seaaccwarpieiner1">
                                                                        <h4>Nudity</h4>
                                                                        <div class="mb-2 custcolorbox">
                                                                            <input type="checkbox" name='test8' id="test8">
                                                                            <label for="test8" id="test8">Item</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>  -->
                                                            <div class="content">
                                                                <div class="seaaccwarpiebox">

                                                                    <div class="seaaccwarpieiner1">
                                                                        <h4>General</h4>
                                                                        @foreach($restriction as $restrict)
                                                                        @if($restrict->restrictions_type == 'General')
                                                                        <div class="mb-2 custcolorbox">
                                                                            <div class="cutmbocheck">  
                                                                                <input type="checkbox" name="restriction_id[]" value="{{$restrict->id}}" class="restriction" id="generalCk{{$restrict->id}}"
                                                                                >
                                                                                <label for="generalCk{{$restrict->id}}"> {{$restrict->restriction_name}}</label>
                                                                            </div>                                                    
                                                                        </div>
                                                                        @endif                                                    
                                                                        @endforeach       
                                                                    </div>                 
                                                                    <div class="seaaccwarpieiner1">
                                                                        <h4>Interactions</h4>
                                                                        @foreach($restriction as $restrict)

                                                                        @if($restrict->restrictions_type == 'Interactions')
                                                                        <div class="mb-2 custcolorbox">
                                                                            <div class="cutmbocheck">  
                                                                                <input type="checkbox" name="restriction_id[]" value="{{$restrict->id}}" class="restriction" id="interactions{{$restrict->id}}"
                                                                                >
                                                                                <label for="interactions{{$restrict->id}}"> {{$restrict->restriction_name}}</label>
                                                                            </div>                                                    
                                                                        </div>
                                                                        @endif                                                    
                                                                        @endforeach       
                                                                    </div>               
                                                                    <div class="seaaccwarpieiner1">
                                                                        <h4>Smoking</h4>
                                                                        @foreach($restriction as $restrict)

                                                                        @if($restrict->restrictions_type == 'Smoking')
                                                                        <div class="mb-2 custcolorbox">
                                                                            <div class="cutmbocheck">  
                                                                                <input type="checkbox" name="restriction_id[]" value="{{$restrict->id}}" class="restriction" id="smoking{{$restrict->id}}"
                                                                                >
                                                                                <label for="smoking{{$restrict->id}}"> {{$restrict->restriction_name}}</label>
                                                                            </div>                                                    
                                                                        </div>
                                                                        @endif                                                    
                                                                        @endforeach       
                                                                    </div>
                                                                    <div class="seaaccwarpieiner1">
                                                                        <h4>Nudity</h4>
                                                                        @foreach($nudity as $nud)
                                                                        <div class="mb-2 custcolorbox">
                                                                         <input type="checkbox" name='nudity_id[]' value="{{$nud->id}}" id="nudity{{$nud->id}}">
                                                                         <label for="nudity{{$nud->id}}">{{$nud->nudity_name}}</label>
                                                                     </div>
                                                                     @endforeach


                                                                 </div>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>

                                     <div class="acoinerboxcoverbox">
                                      <div class="accordion-content hidden">

                                        <div class="ineracodbox3">                     
                                            <div class="accordion-container">
                                                <div class="set hedtobox">
                                                    <div class="vmgeaminmaxclmo_tbl_iner1">
                                                        <div class="lictebox_setin">
                                                            <i class="far fa-paw"></i>
                                                            <p>Props</p>
                                                        </div>
                                                    </div> 
                                                           <!--  <a href="javascript:void(0)">
                                                                <span class="moretext">Show</span>
                                                                <span class="lesstext">Hide props</span>
                                                                <i class="far fa-angle-down"></i>
                                                            </a> -->
                                                            
                                                            <div class="content">
                                                                <div class="ineracod_appearance">
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Prop</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Pets</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Pet Type</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Musical Instrument</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                        
                                        <div class="acoinerboxcoverbox">                                           
                                            <div class="accordion-content" style="border-bottom: 0;">

                                                <div class="ineracodbox3">                     
                                                    <div class="accordion-container hidden">
                                                        <div class="set hedtobox">
                                                            <div class="vmgeaminmaxclmo_tbl_iner1">
                                                                <div class="lictebox_setin">
                                                                    <i class="far fa-car-side"></i>
                                                                    <p>Transportation</p>
                                                                </div>
                                                            </div> 
                                                            <!-- <a href="javascript:void(0)">
                                                                <span class="moretext">Show</span>
                                                                <span class="lesstext">Hide transportation</span>
                                                                <i class="far fa-angle-down"></i>
                                                            </a> -->
                                                            
                                                            <div class="content">
                                                                <div class="ineracod_appearance">
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Color</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Year</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Model</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Condition</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Vehicle Type</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="ineracod_appearance">
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Accessory</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="peunagpeallselbox4">
                                                                        <div class="semselectdis">
                                                                            <p>Make</p>
                                                                            <div class="seletcustboxpart">
                                                                                <select class="custselectbox1">
                                                                                    <option value=""></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="searchboxbotset">

                                            <a id="btn_search" href="javascript:void(0)"  onclick="AdvanceFormSubmit()" class="AdvancedSearch_btn">Search</a>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="tagListDiv" class="srcfitrembox hidden">
       <!--  <a href="javascript:void(0)"><span>Has vehicle</span> <i class="fal fa-times"></i></a>
        <a href="javascript:void(0)"><span>Cigarette</span> <i class="fal fa-times"></i></a>
        <a href="javascript:void(0)"><span>Nudity - Full Frontal</span> <i class="fal fa-times"></i></a>
        <a href="javascript:void(0)"><span>Earrings</span> <i class="fal fa-times"></i></a>
        <a href="javascript:void(0)"><span>Hat</span> <i class="fal fa-times"></i></a> -->
    </div>
</div>

<div class="srcresltsbox" id="allRecord">
    <div id="wait" class="loadingpage" style="">
        <div class="loadinginerpage">        
            <img src='{{ asset("assets/images/loading.gif") }}' width="64" height="64" /><br><p>Loading...</p>
        </div>
    </div>
    <div class="serboxtitlebox">
        <div class="serboxtitle_boxleft">
            <h3>Search Results</h3>
        </div>
        <div class="serboxtitle_boxright">
            <div class="showboxart">
                <a href="javascript:void(0)">Show: <span>{{count($performer)}}</span> <i class="far fa-sort"></i></a>
            </div>
            <input type="hidden" id="viewformat" value="grid" name="">
            <div class="layoutsbtncng">
                <p>Layout:</p>
                <a href="javascript:void(0)"  onclick="listView('grid','gridActive')" id="gridActive" class="active"><i class="far fa-grip-horizontal"></i></a>
                <a href="javascript:void(0)" onclick="listView('list','listActive')" id="listActive"><i class="far fa-list"></i></a>
            </div>
        </div>
    </div>
    <ul id="permorer_list">
        @foreach($performer as $person)
        <li>
            <a class="showPerformersDetail" href="javascript:void(0)"  id="{{$person->id}}">

                @if(isset($person->referred_by) && $person->referred_by !="" )
                <div class="brflimgset2">
                    <img src="{{asset('assets/svg/default-icon.svg') }}" alt="">
                </div>
                @endif
                <div class="fltsr_userimg">
                    <img src="{{ $person->image_url }}" alt="">
                </div>
                <div class="fltsr_userdits2">
                    <?php
                    if(isset($person->gender)){
                     $gender_ch = ($person->gender=="male") ? 'M' : (($person->gender=="female") ? 'F' : 'T');}else{ $gender_ch ="-";}

                     ?>
                     <h4>{{substr($person->full_name,0,15) }}</h4>
                     <div class="hwvbox"><p> <span>{{$person->age}}{{$gender_ch}}</span>
                        <span>                            
                            @if(isset($person->appearance[0]->height))
                            <?php $height = preg_replace(array('/\bfeet\b/','/\bft\b/','/\bf\b/','/\binches\b/','/\bin\b/'),array("'","'","'",'"','"','"'), $person->appearance[0]->height);
                            str_replace('ft',"'",$height);
                            ?>
                            
                            <?php
                            if(is_numeric($height))
                            {
                                $height =  round(floatval($height / 30.48),2);
                            }else{
                                $height = $height;
                            }                           

                            ?>
                            {{(strlen($height) < 7 ? $height : "-")}}                   
                            @else-
                            @endif
                        </span>
                        <span>@if(isset($person->appearance[0]->weight))
                         {{str_replace("lbs","",$person->appearance[0]->weight)}} lbs
                         @else -
                     @endif</span> </p></div>

                     <div class="flggrnbox">
                        <div class="flggrnbox_left">                              
                            @if($person->performerFlag)
                            @if(count($person->performerFlag) >= 1)                                
                            <p id="performerListflagcount{{$person->id}}"> {{count($person->performerFlag)}}</p>
                            <i class="far fa-flag-alt"></i>
                            @else
                            <p id="performerListflagcount{{$person->id}}">0</p>
                            <i class="far fa-flag-alt"></i>
                            @endif                                  

                            @endif                             

                        </div>
                        <div class="flggrnbox_right">
                            <p>
                                @if(isset($person->unionDetail->union_tag))
                                {{$person->unionDetail->union_tag}}
                                @endif
                                -{{$person->union_number}}</p>
                                <p>
                                    @if(isset($person->agentDetail->agencies->agencyAbbreviation))
                                    {{$person->agentDetail->agencies->agencyAbbreviation}}
                                    @endif

                                </p>
                            </div>
                        </div>
                        
                    </div>
                </a>
            </li>
           <!--  <li>
                <a class="showPerformersDetail" href="javascript:void(0)"  id="{{$person->id}}" >
                    <div class="fltsr_userimg">
                        <img src="{{ $person->image_url }}" alt="">
                    </div>
                    <div class="fltsr_userdits">
                        <h4><span class="leftnamebox">{{$person->firstName}}</span> <span class="rigfmbox">{{($person->gender=="male"?'M':'F')}}</span></h4>
                        <h5>Available - up to date</h5>
                        <p>UBCP Extra</p>
                    </div>
                </a>
            </li> -->
            @endforeach
        </ul>

        <div class="litboxldetabox" id="listView">


        </div>

    </div>


    <div class="srcresltsbox hidden" id="serchResult">

    </div>
</div>


<div class="modal fade useralldetainer" id="myModalUserdetail" role="dialog">
    <div class="modal-dialog"> 
        <div class="modal-content" id="ViewModalInfo">

        </div>
    </div>
</div>

@include('internal-staff.layouts.performermodel') 



<!-- // modal for serch result -->
<div class="modal fade editskinboxmodal" id="serchresultModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="editsktoboxcover">
         <form method="post"  enctype="multipart/form-data" id="searchDataForm">
          @csrf          
          <div class="titlandclobtn2">
            <h3>Save Search Result</h3>
            <button type="button" class="close" data-dismiss="modal"><img src="{{url('public/assets/corporate-admin/svg/close-icon.svg')}}" alt=""></button>
        </div>
        <div class="enterskintonebox">
            <div class="form-group">
              <input type="text" class="form-control"  name="my_performer" placeholder="Enter Title">
          </div>
          <input type="button" name="" value="Save" class="btn" id="saveResult">
      </div>              
  </form>
</div>

</div>
</div>
</div>
</div>


@endsection
@section('scripts')
<script>
    $(document).ready(function() {
      $(".set > a").on("click", function() {
        if ($(this).hasClass("active")) {
          $(this).removeClass("active");
          $(this)
          .siblings(".content")
          .slideUp(200);
          $(".set > a i")
          .removeClass("fa-minus")
          .addClass("fa-plus");
      } else {
          $(".set > a i")
          .removeClass("fa-minus")
          .addClass("fa-plus");
          $(this)
          .find("i")
          .removeClass("fa-plus")
          .addClass("fa-minus");
          $(".set > a").removeClass("active");
          $(this).addClass("active");
          $(".content").slideUp(200);
          $(this)
          .siblings(".content")
          .slideDown(200);
      }
  });
  });

</script>
<script type="text/javascript">
    //SCROLL
    var processing;
    $(document).ready(function(){
        var fs = $('#submitForm').val();
        if(fs === "1")
        { 
            // $("#wait").css("display", "block");
            // jQuery('body').addClass('loaderDisplay');
            AdvanceFormSubmit();   
        }

        $(document).scroll(function(e){
            if (processing)
                return false;

            if ($(window).scrollTop() >= $(document).height() - $(window).height() - 700){
                processing = true;

                $("#wait").css("display", "block");
                jQuery('body').addClass('loaderDisplay');
                var viewformat =  $('#viewformat').val();
                var firstId     = $('.showPerformersDetail:first').attr('id');
                var lastId      = $('.showPerformersDetail:last').attr('id');
                var search      = $('#search').val();
                var gender      =jQuery('#gender').val();
                var availability =jQuery('#availability').val();
                var union       =jQuery('#union').val();
                var ethnicity   =jQuery('#ethnicity').val();
                var region      =jQuery('#region').val();
                var min_age      =jQuery('#age_minval').val();
                var max_age         =jQuery('#age_maxval').val();
                var search_value    = $("#search").val();



                formData = $("#advanced_search_form").serializeArray();
                formData.push({name: 'gender', value: gender});
                formData.push({name: 'availability', value: availability});
                formData.push({name: 'union', value: union});
                formData.push({name: 'ethnicity', value: ethnicity});
                formData.push({name: 'region', value: region});
                formData.push({name: 'min_age', value: min_age});
                formData.push({name: 'max_age', value: max_age});
                formData.push({name: 'search', value: search_value});

                formData.push({name: 'firstId', value: firstId});
                formData.push({name: 'lastId', value: lastId});



                $.ajax({
                    url:"{{route('internal-staff.advanceSearch')}}",
                    type:'get',
                    data:formData,
                    success:function(response){

                        if($('#viewformat').val() == "list")
                        {
                            if(response.message=='error'){
                                $('#listView').html(response.data);
                            }else{
                                $('#listView').append(response.data);
                            }

                        }else{
                            if(response.message=='error'){
                               $('#permorer_list').html(response.data); 
                           }else{
                            $('#permorer_list').append(response.data);
                        }

                    }

                    processing = false;
                },complete:function(){
                  $("#wait").css("display", "none");
                  jQuery('body').removeClass('loaderDisplay');
              }

          });

                    /*$.post('/echo/html/', 'html=<div class="loadedcontent">new div</div>', function(data){
                        //$('#container').append(data);
                        processing = false;
                    }
                    );*/
                }
            });
    });
    /*END SROLL*/


    // $(document).ready(function(){
    //     var processing_searchbar;
    //     $(document).scroll(function(e){
    //         if (processing_searchbar)
    //             return false;

    //         if ($(window).scrollTop() >= $(document).height() - $(window).height() - 700){
    //             processing_searchbar = true;

    //             var firstId = $('.showPerformersDetail:first').attr('id');
    //             var lastId = $('.showPerformersDetail:last').attr('id');

    //             alert(lastId);
    //             $.ajax({
    //                 url:"{{route('internal-staff.performersScrollSearch')}}",
    //                 type:'get',
    //                 data:{firstId:firstId,lastId:lastId},
    //                 success:function(response){
    //                     $('#serchResult').append(response)
    //                     processing_searchbar = false;
    //                 }

    //             });
    //         }
    //     });
    // });

    $(document).on('click','#saveResult',function(){

        var gender                  = $('#gender').find(":selected").text();
        var ethnicity               = $('#ethnicity').find(":selected").val();
        var age_minval              = $('#age_minval').val();
        var age_maxval              = $('#age_maxval').val();
        var union                   = $('#union').find(":selected").val();
        var availability            = $('#availability').find(":selected").val();
        var region                  = $('#region').find(":selected").val();


        AdvanceformData = $("#advanced_search_form").serialize();
        formData = $('#searchDataForm').serialize();
        $.ajaxSetup({
           headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
     });
        $.ajax({
            url:"{{route('internal-staff.search.result')}}",
            type:"POST",
            data:{formData,AdvanceformData,gender,ethnicity,age_minval,age_maxval,union,availability,region},
            success:function(response)
            {
                if(response.status == true)
                {
                    location.reload();
                }
                if(response.status == false)
                {
                    toastr.error(response.message);
                }
            }

        });
    });




    jQuery(document).ready(function(){
     jQuery('#ethnicity').select2({
        placeholder:'Select Ethnicity',
    }); 
     jQuery('#ethnicitytwo').select2({
        placeholder:'Select Ethnicity',
    }); 
     jQuery('#gender').select2({
        placeholder:'Select Gender',
    });
     jQuery('#age').select2({
        placeholder:'Select Age',
    }); 
     jQuery('#union').select2({
        placeholder:'Select Union',
    }); 
     jQuery('#availability').select2({
        placeholder:'Select availability',
    }); 
     jQuery('#region').select2({
        placeholder:'Select region',
    }); 
     jQuery('#gender2').select2({
        placeholder:'Select Gender'
    });
     jQuery('#region2').select2({
      placeholder:'Select Region'
  });
     jQuery('#skintone').select2({
      placeholder:'Select SkinTone'
  });
     jQuery('#haircolor').select2({
      placeholder:'Select HairColor'
  });
     jQuery('#hairfacial').select2({
      placeholder:'Select HairFacial'
  });
     jQuery('#bodytrait').select2({
      placeholder:'Select BodyTrait'
  });
     jQuery('#hairstyle').select2({
      placeholder:'Select HairStyle'
  });
     jQuery('#eyes').select2({
      placeholder:'Select Eyes'
  });
     jQuery('#nationality').select2({
      placeholder:'Select Nationality'
  });
     jQuery('#handedness').select2({
      placeholder:'Select Handedness'
  });
     jQuery('#tattoos').select2({
      placeholder:'Select Tattoos'
  });
     jQuery('#availability2').select2({
      placeholder:'Select Availability'
  });
     jQuery('#Performancecategory').select2({
      placeholder:'Select Category',
      // allowClear:true,
  });
     jQuery('#performancestatus').select2({
      placeholder:'Select Status'
  });
     jQuery('#bust').select2({
      placeholder:'Select Bust'
  });
     jQuery('#infantsize').select2({
      placeholder:'Select Size'
  });
     jQuery('#Jacket').select2({
      placeholder:'Select Jacket'
  });
     jQuery('#shoes').select2({
      placeholder:'Select Shoes'
  });
     jQuery('#vehicletype').select2({
      placeholder:'Select Type'
  });
     jQuery('#year').select2({
      placeholder:'Select year'
  });
     jQuery('#model').select2({
      placeholder:'Select Model'
  });
     jQuery('#condition').select2({
      placeholder:'Select Condition'
  });
     jQuery('#color').select2({
      placeholder:'Select Colour'
  });
     jQuery('#accessory').select2({
      placeholder:'Select Accessory'
  });
     jQuery('#make').select2({
      placeholder:'Select Make'
  });
     jQuery('#tattoos').select2({
      placeholder:'Select Tattoos'
  });
     jQuery('#prop').select2({
      placeholder:'Select Prop'
  });
     jQuery('#pets').select2({
      placeholder:'Select Pet'
  });
     jQuery('#pettype').select2({
      placeholder:'Select Type'
  });
     jQuery('#musicalinstrument').select2({
      placeholder:'Select Instrument'
  });
     jQuery('#qualifications').select2({
      placeholder:'Select Qualification'
  });
     jQuery('#communityservents').select2({
      placeholder:'Select Servant'
  });
     jQuery('#sports').select2({
      placeholder:'Select Sport'
  });
     jQuery('#magic').select2({
      placeholder:'Select magic'
  });
     jQuery('#culinary').select2({
      placeholder:'Select Culinary'
  });
     jQuery('#dance').select2({
      placeholder:'Select Dance'
  });
     jQuery('#military').select2({
      placeholder:'Select Military'
  });
     jQuery('#music').select2({
      placeholder:'Select Music'
  });
     jQuery('#bands').select2({
      placeholder:'Select Band'
  });
     jQuery('#specialtydriver').select2({
      placeholder:'Select SpecialtyDriver'
  });
     jQuery('.custselectbox1').select2({
      placeholder:'All'
  });
     jQuery('#agentselect').select2({
      placeholder:'Select Agent'
  });
     jQuery('#agency').select2({
      placeholder:'Select All'
  });

 });
</script>

<script>


    $(document).on('click',".showPerformersDetail",function(){
      id =$(this).attr('id');

      $.ajax({
          url:"{{route('internal-staff.performers.detail')}}",
          type:'get',
          data:{id:id},
          success:function(response){
            if(response.status == true)
            {
                jQuery('#ViewModalInfo').html(response.html);
                setTimeout( function(){
                    jQuery('#myModalUserdetail').modal('show');
                    jQuery('#flagSelect').select2({
                      placeholder:'All'
                  });
                },1000);

                showPerformersDetail(response.data);           
            }

        }
    });

  });


    function showPerformersDetail(img){               

        jQuery('#myModalUserdetail').modal('show');    
        setTimeout(function(){ 

          Vue.use(VueAgile);
          app = new Vue({
            el: '#app',
            components: {
                agile: VueAgile },
                data() {
                    return {
                      asNavFor1: [],
                      asNavFor2: [],
                      options1: {
                        dots: false,
                        fade: true,
                        navButtons: false },
                        options2: {
                            autoplay: false,
                            centerMode: false,
                            dots: false,
                            navButtons: false,
                             infinite : false,
                            slidesToShow: 3,
                            responsive: [
                            {
                              breakpoint: 600,
                              settings: {
                                slidesToShow: 4 } },

                                {
                                  breakpoint: 1000,
                                  settings: {
                                    navButtons: true } }] },


                                    slides: img
                                };


                            },
                            mounted() {
                                this.asNavFor1.push(this.$refs.thumbnails);
                                this.asNavFor2.push(this.$refs.main);
                            } }); }, 1000);
    }

//search
// var request = null;
// $('#search').on('keyup',function(){
//     if(request != null) {
//        request.abort();
//    } 

//    value=$(this).val();
//    $("#wait").css("display", "block");
//    var viewformat =  $('#viewformat').val();
//    jQuery('body').addClass('loaderDisplay');

//    var gender =jQuery('#gender').val();
//    var availability =jQuery('#availability').val();
//    var union =jQuery('#union').val();
//    var ethnicity =jQuery('#ethnicity').val();
//    var region  =jQuery('#region').val();
//    var min_age  =jQuery('#age_minval').val();
//    var max_age  =jQuery('#age_maxval').val();
//    var search_value = $("#search").val();
//    var viewformat =  $('#viewformat').val();
//  formData = $("#advanced_search_form").serializeArray();
//    formData.push({name: 'gender', value: gender});
//    formData.push({name: 'availability', value: availability});
//    formData.push({name: 'union', value: union});
//    formData.push({name: 'ethnicity', value: ethnicity});
//    formData.push({name: 'region', value: region});
//    formData.push({name: 'min_age', value: min_age});
//    formData.push({name: 'max_age', value: max_age});
//    formData.push({name: 'search', value: search_value});
//    formData.push({name: 'viewformat', value: viewformat});

//  request =   $.ajax({
//         type : 'get',
//         url : "{{route('internal-staff.advanceSearch')}}",
//         data:formData,
//         success:function(response){

  
//    $("#collapse1").removeClass("in");

//     if(viewformat == 'list'){
//         $('#permorer_list').addClass('hidden');
//         $('#listView').removeClass('hidden');
//         $('#listView').html(response.data);
//     }else{
//         $('#permorer_list').removeClass('hidden');
//         $('#listView').addClass('hidden');
//         $('#permorer_list').html(response.data);
//     }
//         //$("#serchResult").removeClass("hidden");
//         //$("#allRecord").addClass("hidden");
//        // alert('hello');

//    },complete:function(){

//     request =null;
//     $("#wait").css("display", "none");
//     jQuery('body').removeClass('loaderDisplay');

// }

// });

// })



//setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 1000;  //time in ms, 5 second for example
var $input = $('#search');

//on keyup, start the countdown
$input.on('keyup', function () {
  clearTimeout(typingTimer);

  typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

//on keydown, clear the countdown 
$input.on('keydown', function () {
  clearTimeout(typingTimer);
});

//user is "finished typing," do something
var request = null;
function doneTyping () {

    if(request != null) {
       request.abort();
    } 
   value=$input.val();
   $("#wait").css("display", "block");
   var viewformat =  $('#viewformat').val();
   jQuery('body').addClass('loaderDisplay');

   var gender =jQuery('#gender').val();
   var availability =jQuery('#availability').val();
   var union =jQuery('#union').val();
   var ethnicity =jQuery('#ethnicity').val();
   var region  =jQuery('#region').val();
   var min_age  =jQuery('#age_minval').val();
   var max_age  =jQuery('#age_maxval').val();
   var search_value = $("#search").val();
   var viewformat =  $('#viewformat').val();
 formData = $("#advanced_search_form").serializeArray();
   formData.push({name: 'gender', value: gender});
   formData.push({name: 'availability', value: availability});
   formData.push({name: 'union', value: union});
   formData.push({name: 'ethnicity', value: ethnicity});
   formData.push({name: 'region', value: region});
   formData.push({name: 'min_age', value: min_age});
   formData.push({name: 'max_age', value: max_age});
   formData.push({name: 'search', value: search_value});
   formData.push({name: 'viewformat', value: viewformat});

 request =   $.ajax({
        type : 'get',
        url : "{{route('internal-staff.advanceSearch')}}",
        data:formData,
        success:function(response){

  
   $("#collapse1").removeClass("in");

    if(viewformat == 'list'){
        $('#permorer_list').addClass('hidden');
        $('#listView').removeClass('hidden');
        $('#listView').html(response.data);
    }else{
        $('#permorer_list').removeClass('hidden');
        $('#listView').addClass('hidden');
        $('#permorer_list').html(response.data);
    }
        //$("#serchResult").removeClass("hidden");
        //$("#allRecord").addClass("hidden");
       // alert('hello');

   },complete:function(){

    $("#wait").css("display", "none");
    jQuery('body').removeClass('loaderDisplay');
    request =null;

}

});

  
}


//selected option search
//  gender="";
// $('#gender').on('change', function() {     
//   gender =(this.value);
//   searchData();
// });
// ethnicity="";
// $('#ethnicity').on('change', function() {  
//   ethnicity= (this.value);

//   searchData();
// });
// region="";
$('.selectSearchField').on('change', function() {  
//  region= (this.value);
searchData();
});
$('.selectField').on('select2:select', function (e) {
 jQuery('#'+jQuery(this).data('reid')).removeClass('hidden');
});
$('.removeOption').on('click', function() { 
    jQuery(this).parent('.cleardetabox').addClass('hidden'); 
    $('#'+jQuery(this).data('selectid')).select2('destroy').val('').select2({placeholder:'Select '+jQuery(this).data('selectid')});


 //jQuery('#'+jQuery(this).data('reid')).addClass('hidden');
 searchData();
});

$(document).on("focusout",".age_maxFocus",function(){
   min_age = $('#age_minval').val();
   max_age = $(this).val();
   if(min_age == ""){ min_age =0}
       if(max_age == ""){ max_age =0}

        if(parseInt(max_age) < parseInt(min_age)){
            toastr.error('Please enter max age greater than min age');
            $('#age_maxval').val(min_age);
            
        }  

        searchData();

    });

function searchData()
{
   $("#wait").css("display", "block");
   jQuery('body').addClass('loaderDisplay');
   var gender =jQuery('#gender').val();
   var availability =jQuery('#availability').val();
   var union =jQuery('#union').val();
   var ethnicity =jQuery('#ethnicity').val();
   var region  =jQuery('#region').val();
   var min_age  =jQuery('#age_minval').val();
   var max_age  =jQuery('#age_maxval').val();
   var search_value = $("#search").val();
   var viewformat =  $('#viewformat').val();


   formData = $("#advanced_search_form").serializeArray();
   formData.push({name: 'gender', value: gender});
   formData.push({name: 'availability', value: availability});
   formData.push({name: 'union', value: union});
   formData.push({name: 'ethnicity', value: ethnicity});
   formData.push({name: 'region', value: region});
   formData.push({name: 'min_age', value: min_age});
   formData.push({name: 'max_age', value: max_age});
   formData.push({name: 'search', value: search_value});

   $.ajax({
      url:"{{route('internal-staff.advanceSearch')}}",
      type:"get",
      data:formData,
      success:function(response){

        // $("#serchResult").removeClass("hidden");
        // $("#allRecord").addClass("hidden");
        $("#collapse1").removeClass("in");

        if(viewformat == 'list'){
            $('#permorer_list').addClass('hidden');
            $('#listView').removeClass('hidden');

            $('#listView').html(response.data);
        }else{
            $('#permorer_list').removeClass('hidden');
            $('#listView').addClass('hidden');
            $('#permorer_list').html(response.data);
        }
       // $('#permorer_list').html(response);
   },complete:function(){
    $("#wait").css("display", "none");
    jQuery('body').removeClass('loaderDisplay');
}
});

}
</script>

<script type="text/javascript">





  function AdvanceFormSubmit()
  {

    var defaultClosetGalleryTitle = []; 
    $.each($("input[name='closetGallery[]']:checked"), function(){            
        defaultClosetGalleryTitle.push($(this).val());
    });

    $("#wait").css("display", "block");
    jQuery('body').addClass('loaderDisplay');
    var viewformat =  $('#viewformat').val(); 
    var gender =jQuery('#gender').val();
    var availability =jQuery('#availability').val();
    var union =jQuery('#union').val();
    var ethnicity =jQuery('#ethnicity').val();
    var region  =jQuery('#region').val();
    var min_age  =jQuery('#age_minval').val();
    var max_age  =jQuery('#age_maxval').val();
    var search_value = $("#search").val();
    var viewformat =  $('#viewformat').val();

    var firstId     = $('.showPerformersDetail:first').attr('id');
    var lastId      = $('.showPerformersDetail:last').attr('id');

    formData = $("#advanced_search_form").serializeArray();
    formData.push({name: 'gender', value: gender});
    formData.push({name: 'availability', value: availability});
    formData.push({name: 'union', value: union});
    formData.push({name: 'ethnicity', value: ethnicity});
    formData.push({name: 'region', value: region});
    formData.push({name: 'min_age', value: min_age});
    formData.push({name: 'max_age', value: max_age});
    formData.push({name: 'search', value: search_value});
    formData.push({name: 'closetTitles', value: defaultClosetGalleryTitle});

    formData.push({name: 'firstId', value: firstId});
    formData.push({name: 'lastId', value: lastId});

    $.ajax({
      url:"{{route('internal-staff.advanceSearch')}}",
      type:"get",
      data:formData,
      success:function(response){

        if(response.status == true)
        {  
            // $("#serchResult").removeClass("hidden");
            //$("#allRecord").addClass("hidden");
           //$("#myModal2").modal('hide');

                // $('html, body').animate({scrollTop:0}, '100');

                if(viewformat == 'list'){
                    $('#listView').removeClass('hidden');
                    $('#listView').html(response.data);
                    $('#permorer_list').addClass('hidden');
                }else{
                    $('#permorer_list').removeClass('hidden');
                    $('#permorer_list').html(response.data);
                    $('#listView').addClass('hidden');
                }
                $("#collapse1").removeClass("in");
                if(response.tag)
                {
                 $("#tagListDiv").removeClass("hidden");
                 $(".srcfitrembox").html('');
                 $.each(response.tag, function(key, value) {
                    id= "'"+key+"'";
                    $(".srcfitrembox").append('<a  href="javascript:void(0)"><span>'+value+'</span> <div id="'+key+'" onclick="RemoveOptionAdvance('+id+')" class="removeoptionadvance"> <i class="fal fa-times" ></i></div></a>')  
                })
             }

         }

     },complete:function(){
        $("#wait").css("display", "none");
        jQuery('body').removeClass('loaderDisplay');
    }
});    
}


function RemoveOptionAdvance(id)
{   
    res = id.charAt(0);
    if(res === "#")
    {    
       $(id).select2('destroy').val('').select2({placeholder:'All'});        
   }else {
    $(id).val('');    
}

AdvanceFormSubmit();

}


$(document).ready(function(){
 $("#submitPerformerModel").click(function(){        

   var form = $("#addperformer");          
   $.ajax({
       type: 'POST',
       url: '{{route("internal-staff.addperformer")}}',
       data: form.serialize(),
       success: function (data, status) {
        if(data.error){
           return;
       }

         if(data.status==false){
            toastr.error(data.message);
        } 
        else{
                     toastr.success("Performer Added Successfully");// THis is success message

                   $('#PerformerModel').modal('hide');  // Your modal Id
               }
           },
           error: function (result) {

           }
       });   

});
});







//hide div
$("#Transportation").hide();
$("#Wardrobe").hide();
$("#Prop").hide();
$("#Qualifications").hide();
function listView(view){
    $('#viewformat').val(view);
    if(view=='grid'){
        $('#gridActive').addClass('active');
        $('#listActive').removeClass('active');
        $('#advanceViewformat').val("grid");
        searchData();
    }else{
        $('#gridActive').removeClass('active');
        $('#listActive').addClass('active');
        $('#advanceViewformat').val("list");
        searchData();
    }
    

}


</script>



<script type="text/javascript">
    // $(".restriction").attr('disabled', 'disabled');
    $("#province").mask("SS");
    $("#phonenumber").mask("(999) 999-9999");
    $("#postalcode").mask("999 999");
</script>
<script>

    function FlagSave(id){

        $("#flagcount"+id).html("");  
        $("#performerListflagcount"+id).html("");
        var flagformdata = $("#flagSaveForm").serialize();    
        $.ajax({
            url:"{{route('internal-staff.saveFlag')}}",
            type:"POST",
            data:flagformdata,
            success:function(data){

             if(data.status == true)
             { 

                console.log(data.flagCount);
                toastr.success(data.message);                
                setTimeout(function(){
                  $("#flagcount"+id).html(data.flagCount);
                  $("#performerListflagcount"+id).html(data.flagCount); 
                  $('#flagSaveForm')[0].reset();
                  jQuery('#flagSelect').select2({
                      placeholder:'All'
                  });
              }, 100);

                
            }
            if(data.status == false)
            {
                toastr.error(data.message);
            }
        }

    });
    }       
    
    function ShowAddFlagForm(id)
    {
       $("#flagFormShow"+id).removeClass("hidden");
   }
</script>
@endsection