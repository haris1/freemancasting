@extends('internal-staff.layouts.app')
@section('content')

<div class="listviewpagedeta">
	<div class="serboxtitlebox">
	    <div class="serboxtitle_boxleft">
	        <h3>Freeman Casting</h3>
	    </div>
	    <div class="serboxtitle_boxright">
	        <div class="showboxart">
	            <a href="javascript:void(0)">Show: <span>30</span> <i class="far fa-sort"></i></a>
	        </div>
	        <div class="layoutsbtncng">
	            <p>Layout:</p>
	            <a href="javascript:void(0)" class="active"><i class="far fa-grip-horizontal"></i></a>
	            <a href="javascript:void(0)"><i class="far fa-list"></i></a>
	        </div>
	    </div>
	</div>

	<div class="litboxldetabox">
		<div class="fremanctlistbox">
			<div class="fremanctlistiner1">
				<img src="http://192.168.1.18/freemancasting/public/storage/performers/1/1563363531Chrysanthemum.jpg" alt="">
			</div>
			<div class="fremanctlistiner2">
				<h4>Pan Hyuk</h4>
				<p><span class="fagesp">33F</span> <span class="fagesp">5'8"</span> <span class="fagesp">99.8lbs</span></p>
			</div>
			<div class="fremanctlistiner3">
				<h5 class="blokedcol">Booked</h5>
				<p>UBCP Extra</p>
			</div>
			<div class="fremanctlistiner4">
				<h6>Profile last updated</h6>
				<p>15 Jan 2019</p>
			</div>
			<div class="fremanctlistiner5">				
				<div class="flggrnbox_left1">
	                <p>1</p>
	                <i class="far fa-flag-alt"></i>
	            </div>
	            <div class="flggrnbox_right1">
	                <p>U-123</p> <p>GRE</p>
	            </div>
			</div>
			<div class="fremanctlistiner6">
				<a href="javascript:void(0)">View Profile</a>
			</div>
		</div>
		<div class="fremanctlistbox">
			<div class="fremanctlistiner1">
				<img src="http://192.168.1.18/freemancasting/public/storage/performers/1/1563363531Chrysanthemum.jpg" alt="">
			</div>
			<div class="fremanctlistiner2">
				<h4>Pan Hyuk</h4>
				<p><span class="fagesp">33F</span> <span class="fagesp">5'8"</span> <span class="fagesp">99.8lbs</span></p>
			</div>
			<div class="fremanctlistiner3">
				<h5 class="onholdcol">1st Hold</h5>
				<p>UBCP Extra</p>
			</div>
			<div class="fremanctlistiner4">
				<h6>Profile last updated</h6>
				<p>15 Jan 2019</p>
			</div>
			<div class="fremanctlistiner5">				
				<div class="flggrnbox_left1">
	                <p>1</p>
	                <i class="far fa-flag-alt"></i>
	            </div>
	            <div class="flggrnbox_right1">
	                <p>U-123</p> <p>GRE</p>
	            </div>
			</div>
			<div class="fremanctlistiner6">
				<a href="javascript:void(0)">View Profile</a>
			</div>
		</div>
		<div class="fremanctlistbox">
			<div class="fremanctlistiner1">
				<img src="http://192.168.1.18/freemancasting/public/storage/performers/1/1563363531Chrysanthemum.jpg" alt="">
			</div>
			<div class="fremanctlistiner2">
				<h4>Pan Hyuk</h4>
				<p><span class="fagesp">33F</span> <span class="fagesp">5'8"</span> <span class="fagesp">99.8lbs</span></p>
			</div>
			<div class="fremanctlistiner3">
				<h5 class="notavailablecol">Not Available</h5>
				<p>UBCP Extra</p>
			</div>
			<div class="fremanctlistiner4">
				<h6>Profile last updated</h6>
				<p>15 Jan 2019</p>
			</div>
			<div class="fremanctlistiner5">				
				<div class="flggrnbox_left1">
	                <p>1</p>
	                <i class="far fa-flag-alt"></i>
	            </div>
	            <div class="flggrnbox_right1">
	                <p>U-123</p> <p>GRE</p>
	            </div>
			</div>
			<div class="fremanctlistiner6">
				<a href="javascript:void(0)">View Profile</a>
			</div>
		</div>

	</div>
</div>

@endsection