@extends('internal-staff.layouts.app')
@section('content')
<div class="databaseboxalldeta">

  <div class="perfotblboxdis">
    <h3>My Performers</h3>
    <table class="table table-bordered notestblbox" id="saveperformers" >
      <thead>
        <tr>
          <th class="text-center">ID</th>
          <th class="text-center">Title</th>
          <th class="text-center">Date</th>
          <th class="text-center">Action</th>
        </tr>
      </thead>
      <tbody>
        @if(count($searchResult))
        @foreach($searchResult as $key=>$value)
        <tr>
          <td>{{$key+=1}}</td>
          <td>{{$value->my_performer}}</td>
          <td>{{$value->created_at->setTimezone(Auth::user()->timezone)->format('m/d/Y H:i:s')}}</td>
          <td><a href="{{route('internal-staff.search.result.display',['id'=>$value->id])}}"><i class="far fa-eye"></i></a></td>
        </tr>
        @endforeach
        @endif

      </tbody>
    </table>
  </div>

</div>
@endsection
@section('scripts')
<script type="text/javascript">
  
$('#saveperformers').DataTable({
    "order": [[ 3, "desc" ]]
  });
</script>

@endsection