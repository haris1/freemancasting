<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""/>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">
    
</head>
<body>
 @yield('content')



<script type="text/javascript" src="{{asset('assets/js/jquery.js') }}"></script>
<script src="{{asset('assets/js/bootstrap.min.js') }}"></script>


<!-- <script>
    var currentTab = 0;

var tab = document.querySelectorAll(".tab");
var step = document.querySelectorAll(".step");

var nextbtn = document.querySelector(".nextbtn");
var prevbtn = document.querySelector(".prevbtn");
var cadastrobtn = document.querySelector(".cadastrobtn");

function next() {
  nextbtn.addEventListener("click", function() {
    if (currentTab <= 0) {
      currentTab = currentTab + 1;

      $(".tab").each(function() {
        if ($(this).hasClass("current")) {
          $(this).removeClass("current");
        }
        tab[currentTab].classList.add("current");
      });

      $(".step").each(function() {
        if ($(this).hasClass("active")) {
          $(this).removeClass("active");
        }
        step[currentTab].classList.add("active");
      });

      $(".input").each(function() {
        if ($(this).hasClass("input-current")) {
          $(this).removeClass("input-current");
        }
        tab[currentTab].classList.add("current");
      });
      verifyPosition();
    }
  });
}

function prev() {
  prevbtn.addEventListener("click", function() {
    if (currentTab > 0) {
      currentTab = currentTab - 1;
      $(".tab").each(function() {
        if ($(this).hasClass("current")) {
          $(this).removeClass("current");
        }
        tab[currentTab].classList.add("current");
      });

      $(".step").each(function() {
        if ($(this).hasClass("active")) {
          $(this).removeClass("active");
        }
        step[currentTab].classList.add("active");
      });
      verifyPosition();
    }
  });
}

function verifyPosition() {
  console.log(currentTab);
  if (currentTab > 0) {
    prevbtn.classList.add("visible");
  } else {
    prevbtn.classList.remove("visible");
  }

  if (currentTab == 0) {
    nextbtn.classList.remove("visible");
    cadastrobtn.classList.add("visible");
  } else {
    nextbtn.classList.add("visible");
    cadastrobtn.classList.remove("visible");
  }
}

next();
prev();

</script> -->


</body>
</html>