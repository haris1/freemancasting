@extends('internal-staff.auth.layouts.app')
@section('content')
<div class="container">
     <div class="logonamecover">
        
        <img src="{{ asset('assets/images/logo.png') }}" alt="">
      
        <h3>For Background Casting Internal staff</h3>
    </div>
</div>
<section class="formdisinercover">
    
         <form method="POST" action="{{ route('internal-staff.login') }}" class="mainformcl">
             @csrf
        <div class="container">
            
            <div class="alldabtitlcover">
                <div class="signupboxdis">
                    <h2>Login</h2>
                </div>
                <div class="tab current tabdisinerbox">        @error('status')
                            {{ $message}}
                        @enderror         
                    <div class="detainerformall">
                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>                      
                        <div class="form-group">
                            <label for="password">{{ __('Password') }}</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>                     
                    </div>
                </div>
                <div class="btn-control nextpribtnbox">
                    <button type="submit" class="cadastrobtn visible"> {{ __('Login') }}</button>
                    <br>
                     @if (Route::has('internal-staff.password.request'))
                    <a class="btn btn-link signpboxlink" href="{{ route('internal-staff.password.request') }}">
 
                        {{ __('Forgot Your Password?') }}
                    </a>
                   @endif
                </div>
            </div> 
        </div> 
    </form>
</section>
@endsection
