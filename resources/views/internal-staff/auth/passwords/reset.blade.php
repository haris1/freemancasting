@extends('internal-staff.layouts.app')
@section('content')
<div class="container">
     <div class="logonamecover">
        
        <img src="{{ asset('assets/images/logo.png') }}" alt="">
        
        <h3>For Background Casting Internal staff</h3>
    </div>
</div>
<section class="formdisinercover">
    <form method="POST" action="{{route('internal-staff.password.update') }}" class="mainformcl">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">

        <div class="container">            
            <div class="alldabtitlcover">
                <div class="signupboxdis">
                    <h2>{{ __('Reset Password') }}</h2>
                </div>
                @csrf
                <div class="tab current tabdisinerbox">                 
                    <div class="detainerformall">
                        <div class="form-group">
                            <label for="usr">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="usr">Password</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>  

                        <div class="form-group">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>
                            
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                           
                            
                        </div>                     
                    </div>
                </div>
                <div class="btn-control nextpribtnbox">
                    <button style="width:50%;" type="submit" class="cadastrobtn visible"> {{ __('Reset Password') }}</button>
                    <br>
                    
                </div> 
                  <a class="btn btn-link signpboxlink" href="{{ route('internal-staff.login') }}">Login</a>
                
            </div> 
        </div> 
    </form>
</section>
@endsection
@section('scripts')
@endsection