<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformerRentalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performer_rentals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rental_title');
            $table->text('description')->nullable();
            $table->integer('performer_id')->unsigned();
            $table->foreign('performer_id')->references('id')->on('performers');
            $table->enum('is_video',['yes','no'])->default('no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performer_rentals');
    }
}
