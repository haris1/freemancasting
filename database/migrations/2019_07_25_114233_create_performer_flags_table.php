<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformerFlagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performer_flags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('performer_id')->nullable()->unsigned();
            $table->foreign('performer_id')->references('id')->on('performers');
            $table->integer('internal_staff_id')->nullable()->unsigned();
            $table->foreign('internal_staff_id')->references('id')->on('internal_staffs');
            $table->integer('flag_id')->nullable()->unsigned();
            $table->foreign('flag_id')->references('id')->on('flags');
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performer_flags');
    }
}
