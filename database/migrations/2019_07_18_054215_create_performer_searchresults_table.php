<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformerSearchresultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performer_searchresults', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('my_performer');
            $table->text('search_parameter');
            $table->integer('internalstaff_id')->unsigned();
            $table->foreign('internalstaff_id')->references('id')->on('internal_staffs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performer_searchresults');
    }
}
