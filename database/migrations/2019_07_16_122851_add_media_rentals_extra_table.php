<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMediaRentalsExtraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media_rentals', function (Blueprint $table) {
            $table->string('media_url');
            $table->integer('rental_id')->unsigned();
            $table->foreign('rental_id')->references('id')->on('performer_rentals');
            $table->integer('performers_id')->nullable();
            $table->string('media_title')->nullable();
            $table->string('media_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media_rentals', function (Blueprint $table) {
           $table->dropColumn('media_url');
            $table->dropColumn('rental_id');
            $table->dropColumn('rental_id');
            $table->dropColumn('performers_id');
            $table->dropColumn('media_title');
            $table->dropColumn('media_type');
        });
    }
}
