<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpDeskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_desk', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('portfolio_help');
            $table->text('closet_help');
            $table->text('skills_help');
            $table->text('stats_help');
            $table->text('props_help');
            $table->text('resume_help');
            $table->text('document_help');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('help_desk');
    }
}
