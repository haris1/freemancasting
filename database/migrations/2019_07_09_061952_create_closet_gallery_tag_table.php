<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClosetGalleryTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('closet_gallery_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tag');
            $table->integer('closet_id')->unsigned();
            $table->foreign('closet_id')->references('id')->on('closets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('closet_gallery_tags');
    }
}
