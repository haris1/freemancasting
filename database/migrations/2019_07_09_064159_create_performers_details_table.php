<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performers_details', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('hairStyle')->nullable();
            $table->string('hairFacial')->nullable();
            $table->string('bgPerformerDetails')->nullable();
            $table->string('unionAffiliations')->nullable();
            $table->string('unionNum')->nullable();
            $table->string('visa_student')->nullable();
            $table->string('visa_international')->nullable();
            $table->string('visa_open')->nullable();
            $table->string('visa_film')->nullable();
            $table->string('visa_film_other')->nullable();
            $table->string('shirt')->nullable();
            $table->string('agentDetails')->nullable();
            $table->string('castingTitle')->nullable();            
            $table->string('job')->nullable(); 


            $table->string('cdnCitizenship')->nullable();
            $table->string('visa')->nullable();
            $table->string('dayPhone')->nullable();
            $table->string('eveningPhone')->nullable();
            $table->string('emailSecondary')->nullable();
            $table->string('applicationFor')->nullable();
            $table->string('referral1_name')->nullable();
            $table->string('referral1_phone')->nullable();
            $table->string('referral2_name')->nullable();
            $table->string('referral2_phone')->nullable();
            $table->string('emergencyContact1_name')->nullable();
            $table->string('emergencyContact1_phone')->nullable();
            $table->string('emergencyContact1_email')->nullable();            
            $table->string('emergencyContact2_name')->nullable(); 

            $table->string('emergencyContact2_phone')->nullable();
            $table->string('emergencyContact2_email')->nullable();
            $table->string('driversLicense')->nullable();
            $table->string('leftHandDrive')->nullable();
            $table->string('neck')->nullable();
            $table->string('chest')->nullable();
            $table->string('insea')->nullable();
            $table->string('resume')->nullable();
            $table->string('bgPerformer')->nullable();
            $table->string('availability')->nullable();
            $table->string('cdnCitizen')->nullable();
            $table->string('bcResident')->nullable();
            $table->string('taxes')->nullable();            
            $table->string('sin')->nullable(); 
            $table->string('languageFirst')->nullable();            
            $table->string('languageSecond')->nullable(); 


            $table->integer('performer_id')->nullable()->unsigned();
            $table->foreign('performer_id')->references('id')->on('Performers');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performers_details');
    }
}
