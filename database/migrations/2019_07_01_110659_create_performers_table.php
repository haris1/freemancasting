<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group_name',100)->nullable();
            $table->string('firstName',100)->nullable();
            $table->string('middleName',100)->nullable();
            $table->string('lastName',100)->nullable();
            $table->string('email')->nullable();
            $table->string('password');
            $table->string('phonenumber',50)->nullable();
            $table->text('address')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('postalCode')->nullable();
            $table->string('country')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->enum('gender',['male','female','transgender'])->default('male');
            $table->enum('applicationfor',['Adult','Child','Teen','Infant','Group'])->default('Adult');
            $table->text('parent_aggrement')->nullable();
            $table->text('instagram')->nullable();
            $table->text('twitter')->nullable();
            $table->text('linkedin')->nullable();
            $table->text('facebook')->nullable();
            $table->text('imdb_url')->nullable();
            $table->string('agent_name')->nullable();
            $table->string('referred_by')->nullable();
            $table->string('union_name')->nullable();
            $table->string('union_number')->nullable();
            $table->string('availability')->nullable();
            $table->enum('member_of_group',['yes','no'])->default('no');
            $table->string('nameofgroup',100)->nullable();
            $table->string('agent')->nullable();
            $table->string('union_det')->unique();
            $table->text('upload_primary_url')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performers');
    }
}
