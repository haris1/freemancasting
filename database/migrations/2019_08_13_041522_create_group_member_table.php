<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('member_first_name')->nullable();
            $table->string('member_last_name')->nullable();
            $table->string('member_email')->nullable();
            $table->string('member_phone')->nullable();
            $table->integer('performer_id')->nullable()->unsigned();
            $table->foreign('performer_id')->references('id')->on('performers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_members');
    }
}
