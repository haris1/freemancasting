<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNudityPerfomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nudity_perfomers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('performer_id')->nullable()->unsigned();
            $table->foreign('performer_id')->references('id')->on('performers');
            $table->integer('nudity_id')->nullable()->unsigned();
            // $table->foreign('nudity_id')->references('id')->on('nudity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nudity_perfomers');
    }
}
