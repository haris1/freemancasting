<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAgencyIdColumsToPerformerAgents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('performer_agents', function (Blueprint $table) {
            //
           $table->integer('agency_id')->nullable()->unsigned();
            $table->foreign('agency_id')->references('id')->on('agency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('performer_agents', function (Blueprint $table) {
            //
        });
    }
}
