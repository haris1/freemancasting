<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaClosetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_closets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('media_url');
            $table->integer('closet_id')->unsigned();
            $table->foreign('closet_id')->references('id')->on('closets');
            $table->string('media_title')->nullable();
            $table->string('media_type')->nullable();
            $table->integer('performers_id')->nullable();
            $table->text('tag')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_closets');
    }
}
