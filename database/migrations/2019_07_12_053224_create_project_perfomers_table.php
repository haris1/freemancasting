<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectPerfomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_perfomers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('project_title');
            $table->string('IMDC_link')->nullable();
            $table->bigInteger('resume_id')->nullable()->unsigned();
            $table->foreign('resume_id')->references('id')->on('work_perfomers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_perfomers');
    }
}
