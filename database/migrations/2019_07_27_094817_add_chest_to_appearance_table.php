<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChestToAppearanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appearance', function (Blueprint $table) {
           $table->text('chest')->nullable();
           $table->text('infant_size')->nullable();
           $table->text('child_size')->nullable();
           $table->text('inseam')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appearance', function (Blueprint $table) {
            //
        });
    }
}
