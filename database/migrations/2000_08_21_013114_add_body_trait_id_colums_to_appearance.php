<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBodyTraitIdColumsToAppearance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appearance', function (Blueprint $table) {
             $table->integer('body_trait_id')->nullable()->unsigned();
              $table->integer('hair_style_id')->nullable()->unsigned();
              $table->integer('facial_hair_id')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appearance', function (Blueprint $table) {
            //
        });
    }
}
