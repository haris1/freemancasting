<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformerDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performer_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('document_id')->nullable()->unsigned();
            $table->foreign('document_id')->references('id')->on('documents');
            $table->integer('performer_id')->nullable()->unsigned();
            $table->foreign('performer_id')->references('id')->on('performers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performer_documents');
    }
}
