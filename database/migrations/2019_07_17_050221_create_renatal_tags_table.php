<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRenatalTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renatal_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tag');
            $table->integer('rental_id')->unsigned();
            $table->foreign('rental_id')->references('id')->on('performer_rentals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renatal_tags');
    }
}
