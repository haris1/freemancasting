<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPerformerGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('performers', function (Blueprint $table) {
           $table->int('group_id',100)->nullable();
           $table->int('age',100)->nullable();
           $table->enum('member_of_group',['yes','no'])->default('no');
           $table->string('nameofgroup',100)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('performers', function (Blueprint $table) {
          $table->dropColumn('group_name');
          $table->dropColumn('member_of_group');
          $table->dropColumn('nameofgroup');
        });
    }
}
