<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPerformerRentalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('performer_rentals', function (Blueprint $table) {
            $table->string('rental_title');
            $table->text('description')->nullable();
            $table->integer('performer_id')->unsigned();
            $table->foreign('performer_id')->references('id')->on('performers');
            $table->enum('is_video',['yes','no'])->default('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('performer_rentals', function (Blueprint $table) {
             $table->dropColumn('rental_title');
            $table->dropColumn('description');
            $table->dropColumn('performer_id');
            $table->dropColumn('performer_id');
            $table->dropColumn('is_video');
          
        });
    }
}
