<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaRentalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_rentals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('media_url');
            $table->integer('rental_id')->unsigned();
            $table->foreign('rental_id')->references('id')->on('performer_rentals');
            $table->integer('performers_id')->nullable();
            $table->string('media_title')->nullable();
            $table->string('media_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_rentals');
    }
}
