<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApperanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appearance', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ethnicity_id')->nullable()->unsigned();
            $table->foreign('ethnicity_id')->references('id')->on('ethnicities');
            
            $table->integer('performer_id')->nullable()->unsigned();
            $table->foreign('performer_id')->references('id')->on('Performers');

            $table->bigInteger('skin_tone_id')->nullable()->unsigned();
            $table->foreign('skin_tone_id')->references('id')->on('skin_tones');
            $table->bigInteger('hair_color_id')->nullable()->unsigned();
            $table->foreign('hair_color_id')->references('id')->on('hair_colors');
            
            $table->bigInteger('eyes_id')->nullable()->unsigned();
            $table->foreign('eyes_id')->references('id')->on('eyes');
            $table->bigInteger('handedness_id')->nullable()->unsigned();
            $table->foreign('handedness_id')->references('id')->on('handednesses');
            
            $table->bigInteger('hair_length_id')->nullable()->unsigned();
            $table->foreign('hair_length_id')->references('id')->on('hair_lengths');
            

            $table->integer('nationality_id')->nullable()->unsigned();
            $table->foreign('nationality_id')->references('id')->on('nationalities');
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->string('bust')->nullable();
            $table->string('waist')->nullable();
            $table->string('hips')->nullable();
            $table->string('dress')->nullable();
            $table->string('jeans')->nullable();
            $table->string('shoes')->nullable();
            $table->string('contacts')->nullable();
            $table->string('hair_length')->nullable();
            $table->string('gloves')->nullable();
            $table->string('glasses')->nullable();
            $table->string('gender')->nullable();            
            $table->string('tattoos')->nullable();    

            $table->string('collar')->nullable();
            $table->string('jacket')->nullable();
            $table->string('sleeve')->nullable();
            $table->string('pant')->nullable();
            $table->string('hat')->nullable();
            $table->string('ring')->nullable();
            $table->string('size')->nullable();
            $table->string('vehicle')->nullable();            
            $table->string('pets')->nullable();     

 


            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appearance');
    }
}
