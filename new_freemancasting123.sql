-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2019 at 01:40 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `freemancasting_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `appearance`
--

CREATE TABLE `appearance` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ethnicity_id` bigint(20) UNSIGNED DEFAULT NULL,
  `performer_id` int(10) UNSIGNED DEFAULT NULL,
  `skin_tone_id` bigint(20) UNSIGNED DEFAULT NULL,
  `hair_color_id` bigint(20) UNSIGNED DEFAULT NULL,
  `eyes_id` bigint(20) UNSIGNED DEFAULT NULL,
  `handedness_id` bigint(20) UNSIGNED DEFAULT NULL,
  `hair_length_id` bigint(20) UNSIGNED DEFAULT NULL,
  `nationality_id` int(10) UNSIGNED DEFAULT NULL,
  `height` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bust` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waist` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hips` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dress` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jeans` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shoes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacts` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hair_length` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gloves` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glasses` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tattoos` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jacket` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sleeve` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ring` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pets` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `appearance`
--

INSERT INTO `appearance` (`id`, `ethnicity_id`, `performer_id`, `skin_tone_id`, `hair_color_id`, `eyes_id`, `handedness_id`, `hair_length_id`, `nationality_id`, `height`, `weight`, `bust`, `waist`, `hips`, `dress`, `jeans`, `shoes`, `contacts`, `hair_length`, `gloves`, `glasses`, `gender`, `tattoos`, `collar`, `jacket`, `sleeve`, `pant`, `hat`, `ring`, `size`, `vehicle`, `pets`, `created_at`, `updated_at`) VALUES
(1, 17, NULL, 26, 19, 4, 3, 8, 29, '5 feet 3 inches ', '155', '38 HH', '30', '34', '12', '12', '8', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, '2014-12-01 00:00:00', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(2, 5, NULL, 27, 7, 11, 3, 2, 422, '5\'10\"', '162', NULL, '31', NULL, NULL, '31', '11', 'No', NULL, 'M', 'No', 'Male', 'Yes', '16', '42r', '35.5', NULL, '7.25', '10', NULL, 'Yes', 'No', NULL, NULL),
(3, 5, NULL, NULL, 5, 12, 3, NULL, 29, '5\'2', '110', NULL, NULL, NULL, NULL, NULL, '7.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, '2019-06-05 00:00:00', NULL, 'No', 'Yes', NULL, NULL),
(4, 5, NULL, NULL, 5, 12, 3, NULL, 29, '5\'2', '110', NULL, NULL, NULL, NULL, NULL, '7.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, '2019-06-05 00:00:00', NULL, 'No', 'Yes', NULL, NULL),
(5, 18, NULL, 6, 7, 4, 2, 8, 423, '2019-05-04 00:00:00', '74', NULL, NULL, NULL, NULL, NULL, '2', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, '2019-12-10 00:00:00', NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(6, 6, NULL, NULL, 4, 13, 3, NULL, 29, '4ft4 inches', '90', NULL, NULL, NULL, NULL, NULL, '3', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(7, 5, NULL, 26, 24, 2, 3, 9, 424, '5â€™9â€', '190', NULL, NULL, NULL, NULL, '36', '10', 'No', NULL, 'M', 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(8, 19, NULL, NULL, 4, 1, 3, NULL, 425, '59 in', '103lbs', NULL, NULL, NULL, NULL, NULL, '24 cm', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(9, 5, NULL, NULL, 7, 7, 3, NULL, 29, '6\'', '175', NULL, NULL, NULL, NULL, NULL, '10.5', 'No', NULL, NULL, 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(10, 20, NULL, 26, 25, 14, 3, 2, 29, '2019-01-06 00:00:00', '183', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, 'L', 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(11, 5, NULL, NULL, 5, 2, 3, NULL, 426, '5\'6', '130', NULL, NULL, NULL, NULL, NULL, '7.5', 'No', NULL, 'S', 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(12, 21, NULL, 28, 4, 4, 3, 10, 73, '5 ft 3 inches', '172', '38', '41', '46', 'Size 14', 'Size  14', '6.5', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, 'Large', '22 inches', NULL, 'Medium', '6.5', NULL, 'Yes', 'No', NULL, NULL),
(13, 5, NULL, NULL, 7, 8, 3, 11, 29, '5â€™5', '115', NULL, NULL, NULL, 'Adult S', NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, 'Adult S', NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(14, 5, NULL, 29, 7, 8, 3, 12, 29, '5â€™6', '115', NULL, NULL, NULL, 'Adult S', NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(15, 22, NULL, 1, 4, 4, 3, 2, 427, '5\'. 6\'\'', '137', NULL, '32', NULL, NULL, '32', '11.5', 'No', NULL, 'M', 'No', 'Male', 'No', '15.5', '37', '23', NULL, 'small', '8 to 9', NULL, 'Yes', 'No', NULL, NULL),
(16, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Transgender', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(17, 5, NULL, NULL, 7, 4, 3, 13, 29, '75 in', '190 lbs', NULL, NULL, NULL, NULL, NULL, '10.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(18, 5, NULL, 30, 5, 15, 3, 8, 29, '5â€™9', '165', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(19, 5, NULL, NULL, 7, 8, 3, 2, 428, '6\' 0\"', '202', NULL, NULL, NULL, NULL, '34\"', '10.5', 'No', NULL, 'L', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(20, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(21, 23, NULL, 31, 7, 4, 3, 2, 429, '6', '190', NULL, '32-33', NULL, NULL, '32-33', '9.5-10', 'No', NULL, 'M', 'No', 'Male', 'No', 'M-L', 'M-L(38R-40R)', '34', NULL, '7 3/4, m', '2019-10-09 00:00:00', NULL, 'Yes', 'No', NULL, NULL),
(22, 24, NULL, NULL, 4, 4, 3, 14, 29, '2019-09-05 00:00:00', '175', NULL, NULL, NULL, NULL, NULL, '8.5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(23, 5, NULL, 26, 7, 4, 3, 15, 430, '2000-06-01 00:00:00', '215', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(24, 5, NULL, NULL, 7, 4, 3, NULL, 29, '5â€™10â€', '160', NULL, NULL, NULL, NULL, NULL, '9.5', 'No', NULL, 'M', 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(25, 4, NULL, NULL, 4, 4, 3, 16, 72, '2019-03-06 00:00:00', '195', NULL, NULL, NULL, NULL, NULL, '12', 'No', NULL, 'XL', 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(26, 5, NULL, NULL, 5, 4, 3, NULL, 29, '2019-08-04 00:00:00', '62', NULL, NULL, NULL, NULL, NULL, '4', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(27, 25, NULL, 32, 25, 4, 3, 8, 29, '4\'5\"', '75', NULL, NULL, NULL, 'Xs', NULL, '5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'Youth 12', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(28, 26, NULL, 33, 25, 4, 3, NULL, 29, '5\'6', '185', NULL, NULL, NULL, NULL, NULL, '10W/11', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(29, 20, NULL, NULL, 26, 4, 3, NULL, 29, '2019-05-05 00:00:00', '142', NULL, NULL, NULL, NULL, NULL, '8', 'No', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(30, 5, NULL, NULL, 27, 2, 3, NULL, 29, '6\'2\"', '185', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(31, 27, NULL, 34, 8, 16, 3, 17, 431, '2019-07-05 00:00:00', '135', '32-DD', NULL, NULL, '7', '1932-07-01 00:00:00', '7.5', 'No', NULL, 'M', 'Yes', 'Female', 'No', NULL, '8', NULL, NULL, NULL, '7', NULL, 'Yes', 'No', NULL, NULL),
(32, 28, NULL, 35, 28, 8, 2, 18, 50, '5\'4', '165', NULL, '38', NULL, NULL, NULL, '39', 'No', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(33, 29, NULL, NULL, 29, 4, 3, 12, 21, '5â€2', '140', NULL, NULL, NULL, NULL, NULL, '7', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(34, 8, NULL, 36, 7, 4, 3, 8, 432, '4\'11 ', '100', '32c ', '26', '32', 'Small ', '2', '4', 'No', NULL, 'XXS', 'No', 'Female', 'No', NULL, 'Small ', NULL, NULL, 'Universal ', '6', NULL, 'Yes', 'No', NULL, NULL),
(35, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(36, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(37, 30, NULL, 6, 5, 2, 3, 19, 29, '53\"', '110lbs', NULL, NULL, NULL, NULL, NULL, '5 3E', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(38, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(39, 30, NULL, 5, 5, 8, 3, 20, 29, '53\"', '110', NULL, NULL, NULL, NULL, NULL, '5 1/2 3E', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(40, 5, NULL, NULL, 5, 2, 3, NULL, 29, '4ft 10in', '76lbs', NULL, NULL, NULL, NULL, NULL, '4', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(41, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(42, 31, NULL, NULL, 4, 4, 3, NULL, 29, '7\'74', '77', NULL, NULL, NULL, NULL, NULL, '23.5cm', 'No', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(43, 1, NULL, 28, 4, 13, 3, 13, 73, '5.92', '136.1', NULL, NULL, NULL, NULL, NULL, '8.5', 'No', NULL, 'M', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(44, 32, NULL, NULL, 4, 4, 3, NULL, 29, '5\'7', '128', NULL, '28', NULL, NULL, NULL, '9.5', 'No', NULL, 'S', 'No', 'Male', 'No', NULL, '36R', '28', NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(45, 5, NULL, 37, 30, 2, 4, NULL, 29, '6â€™0â€', '178', NULL, '32', NULL, NULL, '32x32', '11', 'No', NULL, 'L', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(46, 20, NULL, 26, 5, 2, 4, 2, 29, '5â€™11â€', '183', NULL, '33', NULL, NULL, '32/34', '10', 'Yes', NULL, 'L', 'Yes', 'Male', 'No', NULL, 'Large', NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(47, 20, NULL, 5, 27, 2, 2, 21, 433, '5 5 ', '185lbs', NULL, NULL, NULL, 'Xl', '32', '8.5', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, 'Xl', NULL, NULL, NULL, '8', NULL, 'Yes', 'Yes', NULL, NULL),
(48, 5, NULL, 38, 7, 4, 3, 12, 434, '60 inches', '160lbs', '36C', NULL, NULL, '2019-12-10 00:00:00', '2019-12-10 00:00:00', '8/8.5', 'No', NULL, 'S', 'Yes', 'Female', 'No', NULL, 'M/L', NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(49, 1, NULL, NULL, 4, 4, 3, NULL, 136, '5\'10', '130', NULL, NULL, NULL, NULL, NULL, '9.5', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(50, 33, NULL, 39, 31, 13, 3, 2, 435, '6\'2\"', '200', NULL, NULL, NULL, NULL, NULL, '10.5', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(51, 5, NULL, 26, 32, 2, 3, 8, 436, '50â€', '56', NULL, NULL, NULL, '8', NULL, '1', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, '2019-08-07 00:00:00', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(52, 8, NULL, NULL, 33, 4, 3, NULL, 29, '5\'5', '160', NULL, NULL, NULL, NULL, NULL, '9', 'Yes', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(53, 5, NULL, NULL, 7, 4, 3, NULL, 29, '5â€™6', '102', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(54, 30, NULL, 40, 34, 7, 3, 8, 437, '5\'2\"', '155', NULL, NULL, NULL, NULL, NULL, '8', 'Yes', NULL, NULL, 'Yes', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(55, 34, NULL, 8, 5, 4, 3, 12, 29, '5\'5\"', '110', '32', '24', '34', '0-2', NULL, '8.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(56, 5, NULL, 5, 7, 2, 2, 8, 29, '5\' 4\"', '110', NULL, NULL, NULL, NULL, NULL, '8', 'Yes', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(57, 5, NULL, 26, 24, 2, 3, 12, 29, '5\'8 1/2\"', '122lbs', NULL, '29', NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(58, 35, NULL, NULL, 35, 4, 3, 2, 29, '2019-08-05 00:00:00', '165', NULL, '33', NULL, NULL, NULL, '8.5', 'No', NULL, 'M', 'No', 'Male', 'No', NULL, '40R', '32', NULL, NULL, '2019-09-08 00:00:00', NULL, 'Yes', 'No', NULL, NULL),
(59, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(60, 36, NULL, NULL, 4, 8, 3, NULL, 29, '6ft', '210lbs', NULL, NULL, NULL, NULL, NULL, '12', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(61, 5, NULL, 5, 36, 2, 3, 8, 29, '4â€6', '55', NULL, NULL, NULL, '2019-08-07 00:00:00', NULL, '2', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, '2019-08-07 00:00:00', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(62, 37, NULL, 6, 15, 7, 3, 8, 29, '5', '100', NULL, NULL, NULL, 'Girls 12', NULL, '9 womens', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'Girls 12-14', NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(63, 5, NULL, NULL, 37, 4, 3, NULL, 29, '5ft 4in', '125 lbs', NULL, NULL, NULL, NULL, NULL, '6.5 mens', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(64, 5, NULL, NULL, 24, 2, 3, 8, 29, '4ft/4in', '55lbs', NULL, NULL, NULL, NULL, NULL, 'Kids 2', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'Girls 7x', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(65, 38, NULL, 8, 20, 8, 3, 4, 29, '5â€™3', '130', NULL, NULL, NULL, NULL, '7', '8', 'Yes', NULL, NULL, 'Yes', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, '6.5', NULL, 'Yes', 'Yes', NULL, NULL),
(66, 5, NULL, 26, 7, 2, 3, 8, 29, '5,3â€', '130', NULL, NULL, NULL, NULL, NULL, '6.5', 'No', NULL, NULL, 'No', 'Transgender', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(67, 5, NULL, NULL, 7, 8, 3, NULL, 29, '4\'10\"', '115 lbs', NULL, NULL, NULL, NULL, NULL, '10.5 (kids)', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(68, 39, NULL, NULL, 25, 17, 3, 22, 29, '5â€™â€™2', '160', NULL, NULL, NULL, NULL, NULL, '8.5/9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(69, 8, NULL, 5, 4, 4, 3, 2, 96, '6\'1', '215', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, 'L', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(70, 5, NULL, 5, 7, 4, 3, 4, 29, '5ft 2in', '88', NULL, NULL, NULL, 'XS', '24', '5', 'Yes', NULL, 'XS', 'Yes', 'Female', 'No', NULL, 'XS', NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(71, 5, NULL, 41, 27, 18, 3, 23, 29, '5\'6  (5\'9 in lifts (heels)', '130', '36B', '28', '36', '8', '28', '8.5', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, '8', NULL, NULL, NULL, '7', NULL, 'Yes', 'No', NULL, NULL),
(72, 5, NULL, 41, 38, 18, 3, 23, 29, '5\'7  (- 5\'10 in lifts (heels)', '125', '36B', '28', '36', '8', '28', '8', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, '8', NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(73, 5, NULL, 41, 27, 18, 3, 23, 29, '5\'7  (- 5\'10 in lifts (heels)', '125', '36B', '28', '36', '8', '28', '8', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, '8', NULL, NULL, NULL, '7', NULL, 'Yes', 'No', NULL, NULL),
(74, 40, NULL, NULL, 7, 2, 3, NULL, 438, '2019-03-05 00:00:00', '124', '34b', '25in', '35in', '2', NULL, '7', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(75, 20, NULL, NULL, 24, 8, 3, 12, 29, '67', '170', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(76, 1, NULL, NULL, 7, 4, 3, NULL, 29, '5ft 3in', '118', '32A', '27', '31.5', '2', NULL, '6.5', 'Yes', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, '31.5', NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(77, 5, NULL, 5, 39, 4, 3, 24, 29, '4\' 9 1/2\"', '91 lbs', NULL, NULL, NULL, '10', NULL, '3', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, '10', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(78, 41, NULL, 42, 4, 4, 3, NULL, 29, '77.5 inches', '175lbs', NULL, NULL, NULL, NULL, NULL, '12', 'No', NULL, 'XL', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(79, 42, NULL, NULL, 7, 4, 2, NULL, 29, '5\'4', '110', '34A', '28', '34', '2', NULL, '7', 'Yes', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(80, 5, NULL, 5, 25, 8, 2, 8, 29, '2019-06-05 00:00:00', '120', NULL, '27', NULL, 'Small', '26', '9', 'No', NULL, 'XS', 'No', 'Female', 'Yes', NULL, 'Small', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(81, 43, NULL, 9, 7, 4, 3, 25, 439, '5 ft', '100', 'd', '1', NULL, NULL, NULL, '6', 'No', NULL, NULL, 'Yes', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(82, 20, NULL, NULL, 5, 7, 3, 8, 29, '5â€™5', '135', '32', NULL, NULL, NULL, '26', '8.5', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(83, 7, NULL, NULL, 7, 4, 3, NULL, 29, '5\'5\"', '158', NULL, NULL, NULL, NULL, NULL, '7.5', 'Yes', NULL, 'M', 'Yes', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(84, 44, NULL, 43, 7, 4, 3, 26, 29, '5â€™4â€', '120', '36', '32', '39', NULL, '8', '6.5', 'No', NULL, 'S', 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(85, 42, NULL, NULL, 7, 4, 2, NULL, 29, '5\'4', '110', NULL, NULL, NULL, NULL, NULL, '7', 'Yes', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(86, 5, NULL, 5, 37, 7, 3, NULL, 29, '5\'5', '120', NULL, NULL, NULL, NULL, NULL, '8', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(87, 5, NULL, NULL, 39, 19, 3, 12, 440, '5.6', '148', 'DD', '29', '39', '8', NULL, '7.5', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(88, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(89, 45, NULL, 2, 4, 4, 4, 12, 65, '6 feet 4 in', '209', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(90, 46, NULL, 44, 7, 4, 3, NULL, 29, '5 feet 7inchs', '170', NULL, NULL, NULL, NULL, NULL, '9', 'Yes', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(91, 5, NULL, 5, 40, 4, 3, 27, 29, '5\'9\"', '130', '34', '27', '36', '4', '9', '8.5', 'No', NULL, 'S', 'No', 'Female', 'No', NULL, 'M', NULL, NULL, NULL, '6', NULL, 'Yes', 'Yes', NULL, NULL),
(92, 30, NULL, 45, 19, 20, 3, 4, 29, '5,4', '260', NULL, NULL, NULL, '24', '18', '10', 'No', NULL, 'M', 'Yes', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, '9', NULL, 'Yes', 'No', NULL, NULL),
(93, 5, NULL, 5, 2, 2, 3, 28, 29, '2019-07-05 00:00:00', '150', '40', '32', '40', '8', NULL, '7.5', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, '24', NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(94, 5, NULL, 26, 7, 7, 3, 2, 29, '6ft 4in', '175 lbs', NULL, '32 in', NULL, NULL, '32', '45', 'No', NULL, 'L', 'No', 'Male', 'Yes', NULL, '40 tall', NULL, NULL, '2004-01-07 00:00:00', NULL, NULL, 'No', 'No', NULL, NULL),
(95, 47, NULL, 26, 25, 2, 3, NULL, 29, '5.5', '113 pounds', NULL, NULL, NULL, NULL, NULL, '8.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(96, 5, NULL, 5, 5, 2, 3, 8, 441, '4ft 9 inch', '72lbs', NULL, NULL, NULL, 'size 10 child', NULL, 'size 4 ladies', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'child size 10', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(97, 19, NULL, 7, 4, 13, 3, 29, 29, '5\' 2\"', '115', '32\"', '26\"', '37\"', '2', '26', '6', 'Yes', NULL, 'S', 'Yes', 'Female', 'No', NULL, '2', '20 1/2\"', NULL, NULL, '7', NULL, 'No', 'Yes', NULL, NULL),
(98, 48, NULL, 3, 41, 4, 3, 30, 442, '5/6.5', '125', '34', '26', '36', '2', '2004-02-26 00:00:00', '7.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, '2019-07-05 00:00:00', NULL, 'No', 'No', NULL, NULL),
(99, 49, NULL, NULL, 24, 7, 4, NULL, 96, '6\'4', '124', NULL, NULL, NULL, NULL, NULL, '8', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(100, 8, NULL, 26, 41, 8, 3, 12, 29, '5\'25', '160', '38C', NULL, NULL, 'L or 12', '30/31 or 10/12', '7.5', 'Yes', NULL, NULL, 'Yes', 'Female', 'No', NULL, 'L', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(101, 50, NULL, 46, 7, 4, 3, 31, 29, '5\'7\"', '125', NULL, NULL, NULL, NULL, '27', '8', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(102, 20, NULL, 5, 5, 7, 3, NULL, 443, '2019-06-05 00:00:00', '125', NULL, NULL, NULL, NULL, NULL, '8.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(103, 20, NULL, 5, 5, 2, 2, 8, 29, '2019-01-05 00:00:00', '120', NULL, NULL, NULL, NULL, NULL, '7', 'No', NULL, NULL, 'Yes', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(104, 28, NULL, 44, 42, 21, 2, 30, 29, '2019-07-05 00:00:00', '225', NULL, NULL, NULL, '1X', '18', '11', 'Yes', NULL, NULL, 'Yes', 'Female', 'Yes', NULL, '1X', NULL, NULL, NULL, '19', NULL, 'No', 'Yes', NULL, NULL),
(105, 51, NULL, NULL, 7, 8, 4, 32, 124, '6â€™1â€', '160', NULL, '32', NULL, NULL, '32', '9.5', 'Yes', NULL, 'M', 'Yes', 'Male', 'No', '16', '38', '33', NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(106, 51, NULL, NULL, 7, 8, 4, 32, 124, '6â€™1â€', '160', NULL, '32', NULL, NULL, '32', '9.5', 'Yes', NULL, 'M', 'Yes', 'Male', 'No', '16', '38', '33', NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(107, 51, NULL, NULL, 7, 8, 4, 32, 124, '6â€™1â€', '160', NULL, '32', NULL, NULL, '32', '9.5', 'Yes', NULL, 'M', 'Yes', 'Male', 'No', '16', '38', '33', NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(108, 51, NULL, NULL, 7, 8, 4, 32, 124, '6â€™1â€', '160', NULL, '32', NULL, NULL, '32', '9.5', 'Yes', NULL, 'M', 'Yes', 'Male', 'No', '16', '38', '33', NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(109, 51, NULL, NULL, 7, 8, 4, 32, 124, '6â€™1â€', '160', NULL, '32', NULL, NULL, '32', '9.5', 'Yes', NULL, 'M', 'Yes', 'Male', 'No', '16', '38', '33', NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(110, 5, NULL, 8, 7, 8, 3, 2, 444, '5â€™11â€', '160', NULL, '30', NULL, NULL, '30', '10', 'No', NULL, 'M', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(111, 5, NULL, NULL, 7, 2, 3, NULL, 29, '6\"0', '195', NULL, '33', NULL, NULL, '32/33', '10.5', 'No', NULL, 'M', 'No', 'Male', 'No', NULL, '40', NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(112, 52, NULL, NULL, 7, 2, 3, 33, 29, '5\'11', '173', NULL, '32', NULL, NULL, '32', '10.5', 'Yes', NULL, NULL, 'No', 'Male', 'No', NULL, 'M', NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(113, 20, NULL, NULL, 7, 8, 4, NULL, 22, '5ft 8inches', '165lbs', NULL, NULL, NULL, NULL, NULL, '11', 'Yes', NULL, 'M', 'Yes', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(114, 1, NULL, NULL, 4, 4, 3, NULL, 32, '6ft', '162', NULL, NULL, NULL, NULL, NULL, '11', 'Yes', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(115, 1, NULL, NULL, 4, 4, 3, NULL, 32, '6ft', '162', NULL, NULL, NULL, NULL, NULL, '11', 'Yes', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(116, 53, NULL, 5, 43, 7, 3, 2, 29, '5\'10\"', '223', NULL, '36', NULL, NULL, NULL, '11', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(117, 20, NULL, 26, 5, 2, 3, 34, 445, '6.1â€', '220', NULL, '36â€', NULL, NULL, '36-32 ', '12', 'No', NULL, 'L', 'No', 'Male', 'No', NULL, 'Large ', '37â€', NULL, '8', NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(118, 20, NULL, NULL, 27, 2, 3, 35, 54, '6,1', '225', NULL, '36', NULL, NULL, '36', '12', 'No', NULL, 'L', 'No', 'Male', 'No', NULL, '44r', '37', NULL, 'Large', NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(119, 54, NULL, 9, 4, 4, 3, 2, 29, '5 feet 11 inches', '170 lbs', NULL, '31', NULL, NULL, '31', 'ten and a half ', 'No', NULL, 'M', 'No', 'Male', 'Yes', 'n/a', 'n/a', 'n/a', NULL, 'n/a', 'n/a', NULL, 'No', 'No', NULL, NULL),
(120, 1, NULL, NULL, 7, 4, 3, NULL, 29, '5â€™ 8â€', '145lbs', NULL, NULL, NULL, NULL, NULL, '7', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(121, 55, NULL, 47, 4, 13, 3, 8, 29, '5\'3\"', '140', NULL, NULL, NULL, NULL, '30-32', '6.5', 'Yes', NULL, 'XXS', 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(122, 56, NULL, 48, 5, 7, 3, 8, 29, '5\'6.5 inches', '132lbs', '34C', '27', '36', '10', '30', '7', 'Yes', NULL, 'S', 'Yes', 'Female', 'No', NULL, '10', NULL, NULL, 'medium', NULL, NULL, 'No', 'No', NULL, NULL),
(123, 5, NULL, NULL, 7, 4, 2, NULL, 29, '5\'4\"', '135', NULL, NULL, NULL, NULL, NULL, '7', 'Yes', NULL, NULL, 'Yes', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(124, 5, NULL, NULL, 7, 4, 2, NULL, 29, '5\'4\"', '135', NULL, NULL, NULL, NULL, NULL, '7', 'Yes', NULL, NULL, 'Yes', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(125, 5, NULL, NULL, 7, 4, 2, NULL, 29, '5\'4\"', '135', NULL, NULL, NULL, NULL, NULL, '7', 'Yes', NULL, NULL, 'Yes', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(126, 5, NULL, NULL, 7, 4, 2, NULL, 29, '5\'4\"', '135', NULL, NULL, NULL, NULL, NULL, '7', 'Yes', NULL, NULL, 'Yes', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(127, 20, NULL, NULL, 7, 8, 3, NULL, 116, '5,8', '135', '35', '27', '38', NULL, NULL, '8', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(128, 5, NULL, 5, 27, 7, 3, 33, 446, '5,8 ', '140', '34DDD', '29', '38', '2019-08-06 00:00:00', '27', '9,5', 'No', NULL, 'S', 'No', 'Female', 'No', NULL, 'M', '22', NULL, 'Super small', '2019-09-07 00:00:00', NULL, 'Yes', 'Yes', NULL, NULL),
(129, 5, NULL, 49, 5, 2, 3, 8, 447, '5\'4', '127', '34\'', '30', '34', '2019-06-05 00:00:00', NULL, '8', 'Yes', NULL, NULL, 'Yes', 'Female', 'Yes', NULL, 'M', NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(130, 5, NULL, 49, 5, 2, 4, 8, 448, '5\'4', '127', '34\'', '30', '34', '8', '2019-06-05 00:00:00', '8', 'Yes', NULL, 'S', 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, '21', '7', NULL, 'No', 'No', NULL, NULL),
(131, 7, NULL, 5, 44, 4, 3, NULL, 29, '5\'10', '231.4', NULL, NULL, NULL, NULL, NULL, 'Size 9 mens', 'No', NULL, NULL, 'Yes', 'Transgender', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(132, 57, NULL, 5, 4, 4, 3, 35, 29, '5\"5', '110', '34b', '25', NULL, '0-2 (small)', '25', '6', 'No', NULL, 'S', 'No', 'Female', 'No', NULL, 'Small', NULL, NULL, 'Small ', '7', NULL, 'Yes', 'No', NULL, NULL),
(133, 5, NULL, 6, 5, 4, 3, 36, 449, '5â€™0â€', '132', NULL, NULL, NULL, NULL, 'Size 8', '5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, 'Medium ', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(134, 58, NULL, 5, 4, 1, 3, NULL, 29, '60', '147', NULL, NULL, NULL, NULL, NULL, '7', 'No', NULL, 'XS', 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(135, 5, NULL, NULL, 25, 8, 3, NULL, 29, '5\'4\"', '135', NULL, NULL, NULL, NULL, NULL, '7.5', 'No', NULL, NULL, 'Yes', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(136, 32, NULL, 3, 4, 1, 3, 17, 29, '5\'2\"', '105 lbs', '34\"', '25.5\"', '34\"', '0', '0', '7.5', 'No', NULL, 'S', 'No', 'Female', 'No', NULL, '0', NULL, NULL, 'small', '7.5', NULL, 'Yes', 'Yes', NULL, NULL),
(137, 5, NULL, 8, 45, 8, 3, 8, 450, '5\'6\"', '200', '45\"', '37\"', '50\"', '16-18', '16-18', '9-9.5', 'No', NULL, 'M', 'No', 'Female', 'Yes', NULL, 'Large', '30\"', NULL, 'Head Circ: 21.5\"', '7', NULL, 'Yes', 'Yes', NULL, NULL),
(138, 59, NULL, 8, 7, 4, 2, 37, 29, '5 ft', '105', NULL, NULL, NULL, NULL, NULL, '6.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(139, 5, NULL, 8, 7, 4, 3, 35, 29, '5.1', '162', NULL, '36', '41', '2019-12-10 00:00:00', '2019-12-10 00:00:00', '8', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, NULL, '24', NULL, NULL, '8', NULL, 'Yes', 'No', NULL, NULL),
(140, 19, NULL, NULL, 4, 4, 3, 36, 29, '2019-07-05 00:00:00', '110', '32', '28', '34', '4', '4', '7.5', 'No', NULL, 'S', 'No', 'Female', 'No', NULL, 'small', '21', NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(141, 60, NULL, NULL, 7, 2, 3, 8, 29, '5\'2\"', '120', NULL, NULL, NULL, NULL, NULL, '8', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(142, 61, NULL, NULL, 46, 22, 2, NULL, 29, '5ft 5in', '265lbs', NULL, NULL, NULL, NULL, NULL, '9 Dwidth', 'No', NULL, NULL, 'Yes', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(143, 62, NULL, 26, 47, 4, 3, 12, 4, '56 inches', '66.6', NULL, NULL, NULL, NULL, NULL, '4 youth', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, '12 slim', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(144, 63, NULL, 3, 5, 2, 3, 8, 451, '2019-02-05 00:00:00', '126', '35', '29', '34', '5', '26', '6', 'Yes', NULL, 'S', 'No', 'Female', 'Yes', NULL, 'Small/medium', '16', NULL, 'Medium', '7', NULL, 'Yes', 'Yes', NULL, NULL),
(145, 5, NULL, 45, 7, 7, 3, 38, 29, '5\'5\'\'', '125', '34B', '127', '34', 'S (2)', '25-26', '7', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, 'S', NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(146, 5, NULL, 26, 2, 7, 3, NULL, 29, '5\'8\'\'', '152', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(147, 5, NULL, NULL, 7, 2, 3, NULL, 29, '4ft 7in', '82lbs', NULL, NULL, NULL, NULL, NULL, '6', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(148, 64, NULL, 50, 25, 23, 3, 12, 452, '6\"1', '200', NULL, '33', NULL, NULL, NULL, '12', 'No', NULL, NULL, 'No', 'Male', 'Yes', NULL, 'Lrg', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(149, 65, NULL, 32, 25, 21, 3, 39, 29, '6â€™4â€', '185 lbs', NULL, '32', NULL, NULL, '32', '12', 'No', NULL, 'L', 'Yes', 'Male', 'Yes', NULL, '40R', NULL, NULL, '7 3/8 or 7 1/2 (23 1/2) ', '8', NULL, 'No', 'No', NULL, NULL),
(150, 54, NULL, NULL, 4, 4, 3, 12, 29, '5\'7', '134', NULL, '30', NULL, NULL, '30x30', '9.5', 'No', NULL, 'M', 'No', 'Male', 'No', NULL, '36R', '22', NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(151, 32, NULL, NULL, 48, 4, 3, NULL, 29, '4 feet 8', '80', NULL, NULL, NULL, NULL, NULL, '6', 'No', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(152, 66, NULL, 51, 4, 4, 3, 8, 453, '2019-10-05 00:00:00', '130', '30D', '26', '36', '2', '26', '10', 'Yes', NULL, 'M', 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(153, 7, NULL, 52, 25, 4, 3, 17, 454, '5\'4', '127', NULL, '24', '27', NULL, 'Size 7', '6.5', 'Yes', NULL, NULL, 'Yes', 'Female', 'Yes', NULL, 'Medium ', NULL, NULL, NULL, '6', NULL, 'No', 'Yes', NULL, NULL),
(154, 67, NULL, NULL, 7, 8, 3, 8, 29, '69', '145', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(155, 67, NULL, NULL, 7, 8, 3, 8, 29, '69', '145', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(156, 68, NULL, 8, 49, 24, 3, 30, 29, '5\'2\"', '132', '36\"', '27\"', '100\"', 'M/6', '28', '37', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(157, 11, NULL, 8, 4, 4, 3, 2, 67, '5\'10', '180LBS', NULL, '36', NULL, NULL, NULL, '10.5', 'No', NULL, 'L', 'No', 'Male', 'Yes', NULL, '42R', '33', NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(158, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(159, 69, NULL, 7, 7, 4, 3, 2, 29, '4ft\\10\'', '115 lbs', NULL, NULL, NULL, NULL, NULL, '6', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, 'xlarge  boys 14', NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(160, 19, NULL, NULL, 4, 25, 3, 8, 73, '64inch', '94 lbs', NULL, NULL, NULL, NULL, NULL, '6', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(161, 5, NULL, 45, 50, 26, 4, 40, 29, '6\'2', '190', NULL, '32', NULL, NULL, NULL, '11.5', 'No', NULL, 'L', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(162, 5, NULL, 53, 7, 2, 3, 30, 50, '5\' 8\"', '174', NULL, '36', NULL, NULL, NULL, '9', 'Yes', NULL, NULL, 'Yes', 'Male', 'No', '16', '42R', '35', NULL, '2008-01-07 00:00:00', NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(163, 52, NULL, 7, 51, 2, 3, 2, 29, '5\' 9\"', '175', NULL, '34', NULL, NULL, NULL, '10', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, '42', NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(164, 42, NULL, 54, 25, 4, 3, 2, 29, '5', '80', NULL, NULL, NULL, 'Na', NULL, 'Youth 5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'Gap 12-14', NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(165, 70, NULL, 26, 38, 27, 3, 41, 455, '181', '167', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, 'M', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, 'M/L', '2013-11-01 00:00:00', NULL, 'No', 'No', NULL, NULL),
(166, 71, NULL, 55, 39, 14, 3, 12, 116, '181', '170', NULL, '32', NULL, NULL, '32/34', '11', 'No', NULL, 'M', 'No', 'Male', 'Yes', '15', '38R', '23', NULL, 'M/L', '11', NULL, 'No', 'No', NULL, NULL),
(167, 72, NULL, NULL, 20, 28, 3, NULL, 456, '123', '23', NULL, NULL, NULL, NULL, NULL, '12', 'No', NULL, 'XS', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(168, 73, NULL, NULL, 52, 29, 3, NULL, 457, 'asdsad', 'asdsadas', NULL, NULL, NULL, NULL, NULL, 'asdsa', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(169, 30, NULL, 5, 5, 30, 3, NULL, 458, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(170, 30, NULL, 5, 5, 30, 3, NULL, 459, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(171, 30, NULL, 5, 5, 30, 3, NULL, 460, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(172, 30, NULL, 5, 5, 30, 3, NULL, 461, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(173, 30, NULL, 5, 5, 30, 3, NULL, 462, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(174, 30, NULL, 5, 5, 30, 3, NULL, 463, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(175, 30, NULL, 5, 5, 30, 3, NULL, 464, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(176, 30, NULL, 5, 5, 30, 3, NULL, 465, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(177, 5, NULL, NULL, 5, 7, 3, NULL, 29, '5ft 3in', '120', NULL, NULL, NULL, NULL, NULL, '6.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(178, 30, NULL, NULL, 7, 2, 3, NULL, 29, '5\'4\" ', '115', NULL, NULL, NULL, NULL, NULL, '6', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(179, 74, NULL, 8, 7, 2, 3, 12, 29, '5\'3', '160', NULL, NULL, NULL, NULL, NULL, '8', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(180, 5, NULL, 5, 53, 4, 3, 42, 29, '5\'7\"', '125', NULL, NULL, NULL, NULL, NULL, '8', 'Yes', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(181, 5, NULL, 5, 54, 31, 3, 12, 29, '2019-04-05 00:00:00', '110', NULL, NULL, NULL, NULL, NULL, '7', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(182, 75, NULL, 56, 7, 4, 3, 43, 29, '5', '80', NULL, NULL, NULL, 'Youth med', NULL, '5.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'Youth 6 waist youth 10 length ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(183, 5, NULL, 57, 5, 2, 3, 12, 466, '5â€™7â€', '140lbs', '38â€', '26â€', '38â€', '6', NULL, '7.5â€', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(184, 5, NULL, 5, 25, 4, 3, 8, 467, '5\'7\"', '118', '32\"', '24\"', '33\"', '2', '26', '9', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, 'S', NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(185, 5, NULL, NULL, 7, 4, 3, 8, 468, '5\'7\"', '115', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(186, 1, NULL, NULL, 4, 32, 3, NULL, 73, '5.1', '100', NULL, NULL, NULL, NULL, NULL, '7', 'No', NULL, 'S', 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(187, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(188, 5, NULL, 6, 25, 4, 3, 2, 469, '46 in', '41 lbs', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, 'Small', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(189, 5, NULL, NULL, 25, 4, 3, NULL, 29, '3\'7', '41', NULL, NULL, NULL, NULL, NULL, 'Youth 11', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, '2019-06-05 00:00:00', NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(190, 19, NULL, 6, 4, 4, 3, 2, 73, '5\'8', '145 lbs', NULL, '33.25', NULL, NULL, '31/32', '9', 'No', NULL, 'M', 'No', 'Male', 'No', NULL, '36R - 38R', '32.5', NULL, '7.125', NULL, NULL, 'Yes', 'No', NULL, NULL),
(191, 5, NULL, NULL, 20, 2, 3, NULL, 29, '5\'5\"', '146', NULL, NULL, NULL, NULL, NULL, '8', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(192, 5, NULL, 26, 5, 7, 3, 8, 29, '4\'3\"', '55', NULL, NULL, NULL, '10', NULL, '13.5 - 1', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, '10', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(193, 20, NULL, NULL, 2, 8, 4, NULL, 470, '5â€™8â€', '135', '35', '28', '40', '4', '4', '10', 'No', NULL, NULL, 'No', 'Transgender', 'No', NULL, 'S', NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(194, 76, NULL, 9, 25, 8, 3, 36, 471, '5\'9', '140', '34 DD', '26', '37', '2019-08-06 00:00:00', '6', '9.5', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, '7', NULL, 'No', 'No', NULL, NULL),
(195, 5, NULL, NULL, 32, 30, 3, 2, 29, '57â€', '75', NULL, NULL, NULL, NULL, NULL, '6', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, '11', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(196, 20, NULL, 26, 39, 7, 3, 2, 29, '58â€', '79lbs', NULL, NULL, NULL, NULL, NULL, '6.5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, 'Size 12', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(197, 5, NULL, 32, 5, 2, 4, 44, 29, '40', '35', NULL, NULL, NULL, '4T', NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(198, 5, NULL, 58, 6, 2, 3, NULL, 472, '67', '190', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(199, 20, NULL, 26, 5, 33, 2, 17, 29, '3f6inch', '40 lbs', NULL, NULL, NULL, 'Size 6', NULL, 'Size 11', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'Size 6', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(200, 5, NULL, 5, 32, 8, 3, 45, 473, '2019-09-04 00:00:00', '99', NULL, NULL, NULL, 'youth large', NULL, '4', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'youth large', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(201, 77, NULL, NULL, 7, 4, 2, NULL, 29, '2019-07-05 00:00:00', '125', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(202, 5, NULL, 5, 5, 2, 3, 46, 29, '4 ft 10', '106 lbs', NULL, NULL, NULL, 'Youth Size 12  - Adult Size  Extra Small', NULL, '5.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'Youth Size 12  - Adult Size Extra Small', NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(203, 5, NULL, 41, 55, 8, 3, 2, 474, '5\'10', '200', NULL, '36', NULL, NULL, '36', '11', 'No', NULL, 'L', 'No', 'Male', 'No', NULL, '44', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(204, 5, NULL, 8, 7, 8, 3, 47, 29, '5\'3\"', '95lbs', '31 1/2\"', '24\"', '31 1/2\"', '0', '24', '6.5', 'No', NULL, 'S', 'No', 'Female', 'No', NULL, '2', '20\"', NULL, NULL, '4', NULL, 'No', 'No', NULL, NULL),
(205, 5, NULL, 5, 56, 2, 2, 12, 29, '5\'9\"', '137lbs', NULL, NULL, NULL, NULL, NULL, '9/10 womens', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(206, 19, NULL, 59, 4, 1, 3, 2, 29, '5\'4', '110', '32B', '26 inches', '36 inches', '2019-04-02 00:00:00', '26', '7', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(207, 5, NULL, 8, 57, 4, 3, 11, 29, '4â€™3â€', '70', NULL, NULL, NULL, '8 or small', NULL, '1', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, '8 or small', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(208, 78, NULL, 60, 38, 7, 3, 48, 29, '2019-05-05 00:00:00', '165', NULL, '40 inches ', NULL, NULL, '34', '9 men', 'No', NULL, 'L', 'No', 'Male', 'No', '17 inches', 'Lg', '22inches', NULL, '22 int', '9', NULL, 'No', 'Yes', NULL, NULL),
(209, 79, NULL, 61, 25, 4, 3, 49, 475, '5\'4\"', '130', NULL, '27', NULL, '2019-06-04 00:00:00', '27', '7.5', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(210, 5, NULL, 5, 7, 8, 3, 50, 29, '4 foot 7 inches ', '71 lbs ', NULL, NULL, NULL, '2019-12-10 00:00:00', NULL, '6', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, '2019-12-10 00:00:00', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(211, 19, NULL, NULL, 4, 4, 3, NULL, 73, '58inc', '85lb', NULL, NULL, NULL, NULL, NULL, '5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(212, 80, NULL, NULL, 4, 4, 3, NULL, 476, '58INC', '85LB', NULL, NULL, NULL, NULL, NULL, '5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(213, 1, NULL, NULL, 7, 21, 3, NULL, 73, '5.4', '129', NULL, NULL, NULL, NULL, NULL, '7', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(214, 4, NULL, 1, 4, 4, 3, NULL, 477, '5â€™10 (70 inches)', '190', NULL, '33', NULL, NULL, '33', '9/9.5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(215, 4, NULL, 1, 4, 4, 3, NULL, 478, '5â€™10 (70 inches)', '190', NULL, '33', NULL, NULL, '33', '9/9.5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(216, 81, NULL, 8, 7, 4, 3, 30, 67, '5â€™5 ', '128', '36 C ', '26', '36', '6', '26', '8-8.5', 'No', NULL, 'S', 'No', 'Female', 'No', NULL, 'Medium ', '32.5', NULL, 'S', '7', NULL, 'Yes', 'No', NULL, NULL),
(217, 82, NULL, 26, 4, 4, 3, 51, 67, '2019-06-05 00:00:00', '165', NULL, NULL, NULL, NULL, NULL, '8.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(218, 82, NULL, NULL, 4, 4, 3, NULL, 479, '5.6', '75 kg', NULL, NULL, NULL, NULL, NULL, '39', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(219, 5, NULL, 5, 7, 4, 3, 8, 29, '5\'6\"', '150', NULL, NULL, NULL, NULL, NULL, '7.5', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(220, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(221, 22, NULL, 1, 4, 4, 3, NULL, 143, '5.7', '177', NULL, NULL, NULL, NULL, '33 32', '9.5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(222, 5, NULL, 8, 7, 4, 3, 35, 29, '5.1', '162', NULL, '36', '41', '2019-12-10 00:00:00', '2019-12-10 00:00:00', '8', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, NULL, '24', NULL, NULL, '8', NULL, 'Yes', 'No', NULL, NULL),
(223, 19, NULL, NULL, 4, 4, 3, 36, 29, '2019-07-05 00:00:00', '110', '32', '28', '34', '4', '4', '7.5', 'No', NULL, 'S', 'No', 'Female', 'No', NULL, 'small', '21', NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(224, 5, NULL, 5, 58, 8, 3, 52, 29, '5\' 10 1/2\"', '130', '33\"', '27\"', '37\"', '2019-06-04 00:00:00', '29', '8.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, 'small/medium', '23\"', NULL, '22\"', NULL, NULL, 'Yes', 'No', NULL, NULL),
(225, 60, NULL, NULL, 7, 2, 3, 8, 29, '5\'2\"', '120', NULL, NULL, NULL, NULL, NULL, '8', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(226, 20, NULL, NULL, 7, 7, 3, NULL, 480, '5â€™3â€', '180 lbs', NULL, NULL, NULL, NULL, NULL, '8', 'Yes', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(227, 61, NULL, NULL, 46, 22, 2, NULL, 29, '5ft 5in', '265lbs', NULL, NULL, NULL, NULL, NULL, '9 Dwidth', 'No', NULL, NULL, 'Yes', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(228, 62, NULL, 26, 47, 4, 3, 12, 4, '56 inches', '66.6', NULL, NULL, NULL, NULL, NULL, '4 youth', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, '12 slim', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(229, 25, NULL, 5, 25, 8, 3, 2, 29, '4\'2\"', '65', NULL, NULL, NULL, NULL, NULL, 'Youth 3', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, 'Youth 10-12', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(230, 63, NULL, 3, 5, 2, 3, 8, 481, '2019-02-05 00:00:00', '126', '35', '29', '34', '5', '26', '6', 'Yes', NULL, 'S', 'No', 'Female', 'Yes', NULL, 'Small/medium', '16', NULL, 'Medium', '7', NULL, 'Yes', 'Yes', NULL, NULL),
(231, 83, NULL, NULL, 5, 7, 3, NULL, 29, '5â€7', '140', NULL, NULL, NULL, NULL, NULL, '10', 'Yes', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(232, 5, NULL, 45, 7, 7, 3, 38, 29, '5\'5\'\'', '125', '34B', '127', '34', 'S (2)', '25-26', '7', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, 'S', NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(233, 84, NULL, 32, 59, 7, 3, 53, 29, '5\"7', '125 lbs', NULL, '28', NULL, '5', '28', '6.5 Womens', 'No', NULL, 'XS', 'No', 'Female', 'Yes', NULL, 'Small', NULL, NULL, 'XS', NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(234, 5, NULL, 26, 2, 7, 3, NULL, 29, '5\'8\'\'', '152', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(235, 5, NULL, NULL, 7, 2, 3, NULL, 29, '4ft 7in', '82lbs', NULL, NULL, NULL, NULL, NULL, '6', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(236, 64, NULL, 50, 25, 23, 3, 12, 482, '6\"1', '200', NULL, '33', NULL, NULL, NULL, '12', 'No', NULL, NULL, 'No', 'Male', 'Yes', NULL, 'Lrg', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(237, 5, NULL, 5, 4, 8, 4, 30, 29, '5\'9\"', '115 lbs', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(238, 5, NULL, 26, 7, 2, 3, 2, 29, '5\'11', '145', NULL, '32', NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(239, 65, NULL, 32, 25, 21, 3, 39, 29, '6â€™4â€', '185 lbs', NULL, '32', NULL, NULL, '32', '12', 'No', NULL, 'L', 'Yes', 'Male', 'Yes', NULL, '40R', NULL, NULL, '7 3/8 or 7 1/2 (23 1/2) ', '8', NULL, 'No', 'No', NULL, NULL),
(240, 5, NULL, 26, 7, 2, 3, 2, 29, '5\'11', '145', NULL, '32', NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL);
INSERT INTO `appearance` (`id`, `ethnicity_id`, `performer_id`, `skin_tone_id`, `hair_color_id`, `eyes_id`, `handedness_id`, `hair_length_id`, `nationality_id`, `height`, `weight`, `bust`, `waist`, `hips`, `dress`, `jeans`, `shoes`, `contacts`, `hair_length`, `gloves`, `glasses`, `gender`, `tattoos`, `collar`, `jacket`, `sleeve`, `pant`, `hat`, `ring`, `size`, `vehicle`, `pets`, `created_at`, `updated_at`) VALUES
(241, 1, NULL, 2, 4, 13, 2, NULL, 48, '62.4', '130', NULL, NULL, NULL, NULL, NULL, '7', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, '5', NULL, 'Yes', 'No', NULL, NULL),
(242, 54, NULL, NULL, 4, 4, 3, 12, 29, '5\'7', '134', NULL, '30', NULL, NULL, '30x30', '9.5', 'No', NULL, 'M', 'No', 'Male', 'No', NULL, '36R', '22', NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(243, 85, NULL, 2, 4, 1, 3, NULL, 48, '5 feet 4 inches ', '96 pounds ', NULL, NULL, NULL, NULL, NULL, '8', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(244, 32, NULL, NULL, 48, 4, 3, NULL, 29, '4 feet 8', '80', NULL, NULL, NULL, NULL, NULL, '6', 'No', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(245, 66, NULL, 51, 4, 4, 3, 8, 483, '2019-10-05 00:00:00', '130', '30D', '26', '36', '2', '26', '10', 'Yes', NULL, 'M', 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(246, 7, NULL, 52, 25, 4, 3, 17, 484, '5\'4', '127', NULL, '24', '27', NULL, 'Size 7', '6.5', 'Yes', NULL, NULL, 'Yes', 'Female', 'Yes', NULL, 'Medium ', NULL, NULL, NULL, '6', NULL, 'No', 'Yes', NULL, NULL),
(247, 86, NULL, 26, 7, 2, 3, NULL, 50, '6â€™3â€™â€™', '180', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(248, 67, NULL, NULL, 7, 8, 3, 8, 29, '69', '145', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(249, 67, NULL, NULL, 7, 8, 3, 8, 29, '69', '145', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(250, 68, NULL, 8, 49, 24, 3, 30, 29, '5\'2\"', '132', '36\"', '27\"', '100\"', 'M/6', '28', '37', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(251, 11, NULL, 8, 4, 4, 3, 2, 67, '5\'10', '180LBS', NULL, '36', NULL, NULL, NULL, '10.5', 'No', NULL, 'L', 'No', 'Male', 'Yes', NULL, '42R', '33', NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(252, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(253, 69, NULL, 7, 7, 4, 3, 2, 29, '4ft\\10\'', '115 lbs', NULL, NULL, NULL, NULL, NULL, '6', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, 'xlarge  boys 14', NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(254, 19, NULL, NULL, 4, 25, 3, 8, 73, '64inch', '94 lbs', NULL, NULL, NULL, NULL, NULL, '6', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(255, 5, NULL, 45, 50, 26, 4, 40, 29, '6\'2', '190', NULL, '32', NULL, NULL, NULL, '11.5', 'No', NULL, 'L', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(256, 5, NULL, 53, 7, 2, 3, 30, 50, '5\' 8\"', '174', NULL, '36', NULL, NULL, NULL, '9', 'Yes', NULL, NULL, 'Yes', 'Male', 'No', '16', '42R', '35', NULL, '2008-01-07 00:00:00', NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(257, 52, NULL, 7, 51, 2, 3, 2, 29, '5\' 9\"', '175', NULL, '34', NULL, NULL, NULL, '10', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, '42', NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(258, 42, NULL, 54, 25, 4, 3, 2, 29, '5', '80', NULL, NULL, NULL, 'Na', NULL, 'Youth 5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'Gap 12-14', NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(259, 70, NULL, 26, 38, 27, 3, 41, 485, '181', '167', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, 'M', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, 'M/L', '2013-11-01 00:00:00', NULL, 'No', 'No', NULL, NULL),
(260, 71, NULL, 55, 39, 14, 3, 12, 116, '181', '170', NULL, '32', NULL, NULL, '32/34', '11', 'No', NULL, 'M', 'No', 'Male', 'Yes', '15', '38R', '23', NULL, 'M/L', '11', NULL, 'No', 'No', NULL, NULL),
(261, 72, NULL, NULL, 20, 28, 3, NULL, 486, '123', '23', NULL, NULL, NULL, NULL, NULL, '12', 'No', NULL, 'XS', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(262, 73, NULL, NULL, 52, 29, 3, NULL, 487, 'asdsad', 'asdsadas', NULL, NULL, NULL, NULL, NULL, 'asdsa', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(263, 30, NULL, 5, 5, 30, 3, NULL, 488, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(264, 30, NULL, 5, 5, 30, 3, NULL, 489, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(265, 30, NULL, 5, 5, 30, 3, NULL, 490, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(266, 30, NULL, 5, 5, 30, 3, NULL, 491, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(267, 30, NULL, 5, 5, 30, 3, NULL, 492, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(268, 30, NULL, 5, 5, 30, 3, NULL, 493, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(269, 30, NULL, 5, 5, 30, 3, NULL, 494, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(270, 30, NULL, 5, 5, 30, 3, NULL, 495, '4 ft 0 in', '50 lbs approx', NULL, NULL, NULL, NULL, NULL, 'size 1-2', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, 'size 7 boys ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(271, 5, NULL, NULL, 5, 7, 3, NULL, 29, '5ft 3in', '120', NULL, NULL, NULL, NULL, NULL, '6.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(272, 30, NULL, NULL, 7, 2, 3, NULL, 29, '5\'4\" ', '115', NULL, NULL, NULL, NULL, NULL, '6', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(273, 74, NULL, 8, 7, 2, 3, 12, 29, '5\'3', '160', NULL, NULL, NULL, NULL, NULL, '8', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(274, 5, NULL, 5, 53, 4, 3, 42, 29, '5\'7\"', '125', NULL, NULL, NULL, NULL, NULL, '8', 'Yes', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(275, 5, NULL, 5, 54, 31, 3, 12, 29, '2019-04-05 00:00:00', '110', NULL, NULL, NULL, NULL, NULL, '7', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(276, 75, NULL, 56, 7, 4, 3, 43, 29, '5', '80', NULL, NULL, NULL, 'Youth med', NULL, '5.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'Youth 6 waist youth 10 length ', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(277, 5, NULL, 57, 5, 2, 3, 12, 496, '5â€™7â€', '140lbs', '38â€', '26â€', '38â€', '6', NULL, '7.5â€', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(278, 5, NULL, 5, 25, 4, 3, 8, 497, '5\'7\"', '118', '32\"', '24\"', '33\"', '2', '26', '9', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, 'S', NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(279, 5, NULL, NULL, 7, 4, 3, 8, 498, '5\'7\"', '115', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(280, 1, NULL, NULL, 4, 32, 3, NULL, 73, '5.1', '100', NULL, NULL, NULL, NULL, NULL, '7', 'No', NULL, 'S', 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(281, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(282, 5, NULL, 6, 25, 4, 3, 2, 499, '46 in', '41 lbs', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, 'Small', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(283, 5, NULL, NULL, 25, 4, 3, NULL, 29, '3\'7', '41', NULL, NULL, NULL, NULL, NULL, 'Youth 11', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, '2019-06-05 00:00:00', NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(284, 19, NULL, 6, 4, 4, 3, 2, 73, '5\'8', '145 lbs', NULL, '33.25', NULL, NULL, '31/32', '9', 'No', NULL, 'M', 'No', 'Male', 'No', NULL, '36R - 38R', '32.5', NULL, '7.125', NULL, NULL, 'Yes', 'No', NULL, NULL),
(285, 5, NULL, NULL, 20, 2, 3, NULL, 29, '5\'5\"', '146', NULL, NULL, NULL, NULL, NULL, '8', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(286, 5, NULL, 26, 5, 7, 3, 8, 29, '4\'3\"', '55', NULL, NULL, NULL, '10', NULL, '13.5 - 1', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, '10', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(287, 20, NULL, NULL, 2, 8, 4, NULL, 500, '5â€™8â€', '135', '35', '28', '40', '4', '4', '10', 'No', NULL, NULL, 'No', 'Transgender', 'No', NULL, 'S', NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(288, 76, NULL, 9, 25, 8, 3, 36, 501, '5\'9', '140', '34 DD', '26', '37', '2019-08-06 00:00:00', '6', '9.5', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, '7', NULL, 'No', 'No', NULL, NULL),
(289, 5, NULL, NULL, 32, 30, 3, 2, 29, '57â€', '75', NULL, NULL, NULL, NULL, NULL, '6', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, '11', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(290, 20, NULL, 26, 39, 7, 3, 2, 29, '58â€', '79lbs', NULL, NULL, NULL, NULL, NULL, '6.5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, 'Size 12', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(291, 5, NULL, 32, 5, 2, 4, 44, 29, '40', '35', NULL, NULL, NULL, '4T', NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(292, 5, NULL, 58, 6, 2, 3, NULL, 502, '67', '190', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(293, 20, NULL, 26, 5, 33, 2, 17, 29, '3f6inch', '40 lbs', NULL, NULL, NULL, 'Size 6', NULL, 'Size 11', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'Size 6', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(294, 5, NULL, 5, 32, 8, 3, 45, 503, '2019-09-04 00:00:00', '99', NULL, NULL, NULL, 'youth large', NULL, '4', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'youth large', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(295, 77, NULL, NULL, 7, 4, 2, NULL, 29, '2019-07-05 00:00:00', '125', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(296, 5, NULL, 5, 5, 2, 3, 46, 29, '4 ft 10', '106 lbs', NULL, NULL, NULL, 'Youth Size 12  - Adult Size  Extra Small', NULL, '5.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'Youth Size 12  - Adult Size Extra Small', NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(297, 5, NULL, 41, 55, 8, 3, 2, 504, '5\'10', '200', NULL, '36', NULL, NULL, '36', '11', 'No', NULL, 'L', 'No', 'Male', 'No', NULL, '44', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(298, 5, NULL, 8, 7, 8, 3, 47, 29, '5\'3\"', '95lbs', '31 1/2\"', '24\"', '31 1/2\"', '0', '24', '6.5', 'No', NULL, 'S', 'No', 'Female', 'No', NULL, '2', '20\"', NULL, NULL, '4', NULL, 'No', 'No', NULL, NULL),
(299, 5, NULL, 5, 56, 2, 2, 12, 29, '5\'9\"', '137lbs', NULL, NULL, NULL, NULL, NULL, '9/10 womens', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(300, 19, NULL, 59, 4, 1, 3, 2, 29, '5\'4', '110', '32B', '26 inches', '36 inches', '2019-04-02 00:00:00', '26', '7', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(301, 5, NULL, 8, 57, 4, 3, 11, 29, '4â€™3â€', '70', NULL, NULL, NULL, '8 or small', NULL, '1', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, '8 or small', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(302, 78, NULL, 60, 38, 7, 3, 48, 29, '2019-05-05 00:00:00', '165', NULL, '40 inches ', NULL, NULL, '34', '9 men', 'No', NULL, 'L', 'No', 'Male', 'No', '17 inches', 'Lg', '22inches', NULL, '22 int', '9', NULL, 'No', 'Yes', NULL, NULL),
(303, 79, NULL, 61, 25, 4, 3, 49, 505, '5\'4\"', '130', NULL, '27', NULL, '2019-06-04 00:00:00', '27', '7.5', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(304, 5, NULL, 5, 7, 8, 3, 50, 29, '4 foot 7 inches ', '71 lbs ', NULL, NULL, NULL, '2019-12-10 00:00:00', NULL, '6', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, '2019-12-10 00:00:00', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(305, 19, NULL, NULL, 4, 4, 3, NULL, 73, '58inc', '85lb', NULL, NULL, NULL, NULL, NULL, '5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(306, 80, NULL, NULL, 4, 4, 3, NULL, 506, '58INC', '85LB', NULL, NULL, NULL, NULL, NULL, '5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(307, 1, NULL, NULL, 7, 21, 3, NULL, 73, '5.4', '129', NULL, NULL, NULL, NULL, NULL, '7', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(308, 4, NULL, 1, 4, 4, 3, NULL, 507, '5â€™10 (70 inches)', '190', NULL, '33', NULL, NULL, '33', '9/9.5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(309, 4, NULL, 1, 4, 4, 3, NULL, 508, '5â€™10 (70 inches)', '190', NULL, '33', NULL, NULL, '33', '9/9.5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(310, 81, NULL, 8, 7, 4, 3, 30, 67, '5â€™5 ', '128', '36 C ', '26', '36', '6', '26', '8-8.5', 'No', NULL, 'S', 'No', 'Female', 'No', NULL, 'Medium ', '32.5', NULL, 'S', '7', NULL, 'Yes', 'No', NULL, NULL),
(311, 82, NULL, 26, 4, 4, 3, 51, 67, '2019-06-05 00:00:00', '165', NULL, NULL, NULL, NULL, NULL, '8.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(312, 82, NULL, NULL, 4, 4, 3, NULL, 509, '5.6', '75 kg', NULL, NULL, NULL, NULL, NULL, '39', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(313, 5, NULL, 5, 7, 4, 3, 8, 29, '5\'6\"', '150', NULL, NULL, NULL, NULL, NULL, '7.5', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(314, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(315, 22, NULL, 1, 4, 4, 3, NULL, 143, '5.7', '177', NULL, NULL, NULL, NULL, '33 32', '9.5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(316, 5, NULL, 5, 58, 8, 3, 52, 29, '5\' 10 1/2\"', '130', '33\"', '27\"', '37\"', '2019-06-04 00:00:00', '29', '8.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, 'small/medium', '23\"', NULL, '22\"', NULL, NULL, 'Yes', 'No', NULL, NULL),
(317, 20, NULL, NULL, 7, 7, 3, NULL, 510, '5â€™3â€', '180 lbs', NULL, NULL, NULL, NULL, NULL, '8', 'Yes', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(318, 25, NULL, 5, 25, 8, 3, 2, 29, '4\'2\"', '65', NULL, NULL, NULL, NULL, NULL, 'Youth 3', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, 'Youth 10-12', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(319, 83, NULL, NULL, 5, 7, 3, NULL, 29, '5â€7', '140', NULL, NULL, NULL, NULL, NULL, '10', 'Yes', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(320, 84, NULL, 32, 59, 7, 3, 53, 29, '5\"7', '125 lbs', NULL, '28', NULL, '5', '28', '6.5 Womens', 'No', NULL, 'XS', 'No', 'Female', 'Yes', NULL, 'Small', NULL, NULL, 'XS', NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(321, 5, NULL, 5, 4, 8, 4, 30, 29, '5\'9\"', '115 lbs', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(322, 5, NULL, 26, 7, 2, 3, 2, 29, '5\'11', '145', NULL, '32', NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(323, 5, NULL, 26, 7, 2, 3, 2, 29, '5\'11', '145', NULL, '32', NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(324, 1, NULL, 2, 4, 13, 2, NULL, 48, '62.4', '130', NULL, NULL, NULL, NULL, NULL, '7', 'No', NULL, NULL, 'No', 'Female', 'Yes', NULL, NULL, NULL, NULL, NULL, '5', NULL, 'Yes', 'No', NULL, NULL),
(325, 85, NULL, 2, 4, 1, 3, NULL, 48, '5 feet 4 inches ', '96 pounds ', NULL, NULL, NULL, NULL, NULL, '8', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(326, 86, NULL, 26, 7, 2, 3, NULL, 50, '6â€™3â€™â€™', '180', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(327, 1, NULL, NULL, 4, 4, 3, NULL, 66, '5\'2\"', '128', NULL, NULL, NULL, NULL, NULL, '8.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(328, 87, NULL, 62, 4, 13, 3, 54, 29, '5ft', '95', '33 inches around', '27 inches around', '32 inches around', NULL, NULL, '36 in EU size', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(329, 9, NULL, NULL, 4, 4, 4, NULL, 65, '72', '165', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'Yes', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(330, 30, NULL, 5, 3, 8, 3, NULL, 29, '6\'2\"', '195', NULL, '34', NULL, NULL, '34', '13', 'No', NULL, 'XL', 'No', 'Male', 'No', NULL, '44T', '35/36', NULL, '7.375', NULL, NULL, 'Yes', 'No', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `body_traits`
--

CREATE TABLE `body_traits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `body_trait` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `body_traits`
--

INSERT INTO `body_traits` (`id`, `body_trait`, `created_at`, `updated_at`) VALUES
(1, 'Albinism', NULL, NULL),
(2, 'Amputation', NULL, NULL),
(3, 'Body Enhancement', NULL, NULL),
(4, 'Body Modification', NULL, NULL),
(5, 'Braces', NULL, NULL),
(6, 'Burn Survivor', NULL, NULL),
(7, 'Extreme Piercing', NULL, NULL),
(8, 'Extreme Tattoo', NULL, NULL),
(9, 'Glass Eye(s)', NULL, NULL),
(10, 'Little Person', NULL, NULL),
(11, 'Piercing', NULL, NULL),
(12, 'Prosthetic', NULL, NULL),
(13, 'Scar', NULL, NULL),
(14, 'Tattoo', NULL, NULL),
(15, 'Teeth (Gold)', NULL, NULL),
(16, 'Teeth (Missing)', NULL, NULL),
(17, 'Vitiligo', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `closets`
--

CREATE TABLE `closets` (
  `id` int(10) UNSIGNED NOT NULL,
  `closet_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripation` text COLLATE utf8mb4_unicode_ci,
  `performer_id` int(10) UNSIGNED NOT NULL,
  `is_video` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `closet_gallery_tags`
--

CREATE TABLE `closet_gallery_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `closet_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `corporate_admins`
--

CREATE TABLE `corporate_admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `corporate_admins`
--

INSERT INTO `corporate_admins` (`id`, `name`, `email`, `password`, `phonenumber`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'dev', 'developer.cor@gmail.com', '$2y$10$eSqN73T.iQDrB3IGrNjVqOUIAkLvmmmgBg/bq6SShxOK3ch3ijQVu', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ethnicities`
--

CREATE TABLE `ethnicities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ethnicity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ethnicities`
--

INSERT INTO `ethnicities` (`id`, `ethnicity`, `created_at`, `updated_at`) VALUES
(1, 'Asian', NULL, NULL),
(2, 'Belizean', NULL, NULL),
(3, 'Bi Racial', NULL, NULL),
(4, 'Black', NULL, NULL),
(5, 'Caucasian', NULL, NULL),
(6, 'East Indian', NULL, NULL),
(7, 'First Nations', NULL, NULL),
(8, 'Hispanic', NULL, NULL),
(9, 'Indian', NULL, NULL),
(10, 'Inuit', NULL, NULL),
(11, 'Middle Eastern', NULL, NULL),
(12, 'New Zealander', NULL, NULL),
(13, 'Other', NULL, NULL),
(14, 'Pacific Islander', NULL, NULL),
(15, 'Polynesian', NULL, NULL),
(16, 'South Asian', NULL, NULL),
(17, ' Caucasian', NULL, NULL),
(18, 'Cacasian', NULL, NULL),
(19, 'Japanese', NULL, NULL),
(20, 'White', NULL, NULL),
(21, 'Canada', NULL, NULL),
(22, 'African', NULL, NULL),
(23, 'North African, Arab', NULL, NULL),
(24, 'Mixed - asian/caucasian', NULL, NULL),
(25, 'Mix ', NULL, NULL),
(26, 'Black - Nigerian', NULL, NULL),
(27, 'Brazilian/Italian', NULL, NULL),
(28, 'Mixed Race (black and white)', NULL, NULL),
(29, 'Mixed ', NULL, NULL),
(30, 'Caucasion', NULL, NULL),
(31, 'Asian Japanese', NULL, NULL),
(32, 'Chinese', NULL, NULL),
(33, 'Mixed European and Indigenous Heritage', NULL, NULL),
(34, 'Caucasian/indian/mexican', NULL, NULL),
(35, 'Chinese Malaysion', NULL, NULL),
(36, 'Russian, Yugoslavian & Maltese', NULL, NULL),
(37, 'Native and Caucasian ', NULL, NULL),
(38, 'First Nations/irish', NULL, NULL),
(39, 'Jamaican/European ', NULL, NULL),
(40, 'Caucasian/Hispanic', NULL, NULL),
(41, 'African/Black', NULL, NULL),
(42, 'Chinese/Caucasian', NULL, NULL),
(43, 'Aboriginal French ', NULL, NULL),
(44, 'First Nations/ polish', NULL, NULL),
(45, 'sikh', NULL, NULL),
(46, 'German/British', NULL, NULL),
(47, 'Caucasian with ancestry from South Africa, Ireland, Wales, Belgium, UK, France, Spain, and Malaysia.', NULL, NULL),
(48, 'Afro Caribbean/Black', NULL, NULL),
(49, 'Latina', NULL, NULL),
(50, 'Half Caucasian (Irish, British, French-Canadian) quarter Filipino, quarter Chinese', NULL, NULL),
(51, 'caucasian / central europe', NULL, NULL),
(52, 'European', NULL, NULL),
(53, 'Anglo Saxon', NULL, NULL),
(54, 'Vietnamese', NULL, NULL),
(55, 'Italian', NULL, NULL),
(56, 'Causasian', NULL, NULL),
(57, 'Korean', NULL, NULL),
(58, 'Filipino', NULL, NULL),
(59, 'Canadian / Portuguese', NULL, NULL),
(60, 'First Nation & Cacausian', NULL, NULL),
(61, 'Cocausian', NULL, NULL),
(62, 'Swiss-Latin', NULL, NULL),
(63, 'Finnish', NULL, NULL),
(64, 'Caucasian/Mediterranean', NULL, NULL),
(65, 'Panamanian', NULL, NULL),
(66, 'Kenyan Guyanese ', NULL, NULL),
(67, 'Canadian', NULL, NULL),
(68, 'middle eastern/ambiguous', NULL, NULL),
(69, 'Caucasian/Chinese', NULL, NULL),
(70, 'Russian/German/Sweedish ', NULL, NULL),
(71, 'Eastern European/Icelandic', NULL, NULL),
(72, 'asdasd', NULL, NULL),
(73, 'asdsa', NULL, NULL),
(74, 'Caucasian/latina', NULL, NULL),
(75, 'Chinese Canadian ', NULL, NULL),
(76, 'Mixed (Mulatto)', NULL, NULL),
(77, 'Canadian/Italian/irish', NULL, NULL),
(78, 'English ', NULL, NULL),
(79, 'Japanese/British (racially ambiguous look)', NULL, NULL),
(80, 'North Vancouver', NULL, NULL),
(81, 'Persian ', NULL, NULL),
(82, 'Iranian ', NULL, NULL),
(83, ' White', NULL, NULL),
(84, 'German / Ukrainian ', NULL, NULL),
(85, 'Southeast Asian', NULL, NULL),
(86, 'Caucasien ', NULL, NULL),
(87, 'Taiwanese', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `eyes`
--

CREATE TABLE `eyes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `eyes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `eyes`
--

INSERT INTO `eyes` (`id`, `eyes`, `created_at`, `updated_at`) VALUES
(1, 'Black', NULL, NULL),
(2, 'Blue', NULL, NULL),
(3, 'Blue-Green', NULL, NULL),
(4, 'Brown', NULL, NULL),
(5, 'Gold', NULL, NULL),
(6, 'Gray', NULL, NULL),
(7, 'Green', NULL, NULL),
(8, 'Hazel', NULL, NULL),
(9, 'Heterochromia', NULL, NULL),
(10, 'Other', NULL, NULL),
(11, ' Brown/ hazel', NULL, NULL),
(12, 'green/blue', NULL, NULL),
(13, 'Dark brown', NULL, NULL),
(14, 'Blue/green', NULL, NULL),
(15, 'Hazle', NULL, NULL),
(16, 'Hazel green', NULL, NULL),
(17, 'Dark briwn', NULL, NULL),
(18, 'Blue- Green Hazel', NULL, NULL),
(19, 'green / brown (hazel?)', NULL, NULL),
(20, 'Baby blue', NULL, NULL),
(21, 'light brown', NULL, NULL),
(22, 'Gray/green', NULL, NULL),
(23, 'Green blue', NULL, NULL),
(24, 'brown/hazel', NULL, NULL),
(25, 'Broan', NULL, NULL),
(26, 'BRN', NULL, NULL),
(27, 'Green-Blue', NULL, NULL),
(28, 'red', NULL, NULL),
(29, 'asdsadsa', NULL, NULL),
(30, 'blue green', NULL, NULL),
(31, 'Blue/grey ', NULL, NULL),
(32, 'Blown', NULL, NULL),
(33, 'Grey', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hair_colors`
--

CREATE TABLE `hair_colors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hair_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hair_colors`
--

INSERT INTO `hair_colors` (`id`, `hair_color`, `created_at`, `updated_at`) VALUES
(1, 'Ash', NULL, NULL),
(2, 'Auburn', NULL, NULL),
(3, 'Bald', NULL, NULL),
(4, 'Black', NULL, NULL),
(5, 'Blonde', NULL, NULL),
(6, 'Blue ', NULL, NULL),
(7, 'Brown', NULL, NULL),
(8, 'Chestnut', NULL, NULL),
(9, 'Copper', NULL, NULL),
(10, 'Dark', NULL, NULL),
(11, 'Dirty', NULL, NULL),
(12, 'Gray', NULL, NULL),
(13, 'Green', NULL, NULL),
(14, 'Honey', NULL, NULL),
(15, 'Light', NULL, NULL),
(16, 'n/a', NULL, NULL),
(17, 'Pink', NULL, NULL),
(18, 'Platinum', NULL, NULL),
(19, 'Purple ', NULL, NULL),
(20, 'Red', NULL, NULL),
(21, 'Salt & Pepper', NULL, NULL),
(22, 'Strawberry', NULL, NULL),
(23, 'White', NULL, NULL),
(24, 'Light brown', NULL, NULL),
(25, 'Dark brown', NULL, NULL),
(26, 'Brown/red', NULL, NULL),
(27, 'Blond', NULL, NULL),
(28, 'dark roots and lighter/blonder tips', NULL, NULL),
(29, 'Black with highlights ', NULL, NULL),
(30, 'Silver', NULL, NULL),
(31, 'Pepper, grey and white', NULL, NULL),
(32, 'Dirty blonde', NULL, NULL),
(33, 'black brown', NULL, NULL),
(34, 'Brown/Grey', NULL, NULL),
(35, 'Black with white', NULL, NULL),
(36, 'Red/copper/strawberry blonde', NULL, NULL),
(37, 'Blonde/brown ', NULL, NULL),
(38, 'Dirty Blond', NULL, NULL),
(39, 'dark blonde', NULL, NULL),
(40, 'Strawberry Blonde', NULL, NULL),
(41, 'Dark brown/black', NULL, NULL),
(42, 'Dark brown with blonde ends', NULL, NULL),
(43, 'Blonde/gray', NULL, NULL),
(44, 'Multi coloured /black/purple ', NULL, NULL),
(45, 'Hazel', NULL, NULL),
(46, 'silver/white', NULL, NULL),
(47, 'Auburn hair', NULL, NULL),
(48, 'MEDIUM BROWN', NULL, NULL),
(49, 'black/brown', NULL, NULL),
(50, 'BRN', NULL, NULL),
(51, 'Grey', NULL, NULL),
(52, 'asdsadsa', NULL, NULL),
(53, 'Brown / black', NULL, NULL),
(54, 'Light brown (red/blond tones) ', NULL, NULL),
(55, 'D. Brown', NULL, NULL),
(56, 'Brown/Blonde', NULL, NULL),
(57, 'Golden', NULL, NULL),
(58, 'salt and pepper', NULL, NULL),
(59, 'sun bleached blonde', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hair_facials`
--

CREATE TABLE `hair_facials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hair_facial` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hair_facials`
--

INSERT INTO `hair_facials` (`id`, `hair_facial`, `created_at`, `updated_at`) VALUES
(1, 'Beard (Long)', NULL, NULL),
(2, 'Beard (Short)', NULL, NULL),
(3, 'Beard (Styled)', NULL, NULL),
(4, 'Clean-shaven', NULL, NULL),
(5, 'Goatee', NULL, NULL),
(6, 'Moustache', NULL, NULL),
(7, 'Mutton Chops', NULL, NULL),
(8, 'n/a', NULL, NULL),
(9, 'Side Burns', NULL, NULL),
(10, 'Stubble', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hair_lengths`
--

CREATE TABLE `hair_lengths` (
  `id` int(11) NOT NULL,
  `hair_length` varchar(191) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory of common hair lengths for performers';

--
-- Dumping data for table `hair_lengths`
--

INSERT INTO `hair_lengths` (`id`, `hair_length`, `created_at`, `updated_at`) VALUES
(1, 'Bald', '2019-07-05 09:14:11', NULL),
(2, 'Short', '2019-07-05 09:14:11', NULL),
(3, 'Chin', '2019-07-05 09:14:11', NULL),
(4, 'Shoulder', '2019-07-05 09:14:11', NULL),
(5, 'Middle-back', '2019-07-05 09:14:11', NULL),
(6, 'Extremely Long', '2019-07-05 09:14:11', NULL),
(7, 'Shaved Head', '2019-07-05 09:14:11', NULL),
(8, 'Long', '2019-07-05 09:14:11', NULL),
(9, '.5mm-4â€inch', NULL, NULL),
(10, 'Chin length', NULL, NULL),
(11, 'Below shoulders', NULL, NULL),
(12, 'Medium ', NULL, NULL),
(13, 'Very Short', NULL, NULL),
(14, 'Short hair', NULL, NULL),
(15, '3 inches ', NULL, NULL),
(16, 'Jawline', NULL, NULL),
(17, 'Mid back ', NULL, NULL),
(18, '2 inches below shoulder length ', NULL, NULL),
(19, 'Various, due to movie/TV roles', NULL, NULL),
(20, 'Various', NULL, NULL),
(21, 'Med long ', NULL, NULL),
(22, 'Just above shoulders', NULL, NULL),
(23, 'Medium to Long', NULL, NULL),
(24, 'mid arm length', NULL, NULL),
(25, '32 inch', NULL, NULL),
(26, 'Passed shoulders', NULL, NULL),
(27, 'Very long', NULL, NULL),
(28, 'short-medium bob cut', NULL, NULL),
(29, 'Collarbone', NULL, NULL),
(30, 'Shoulder length', NULL, NULL),
(31, 'mid-low back', NULL, NULL),
(32, 'medium-short', NULL, NULL),
(33, 'Mid', NULL, NULL),
(34, 'Shortish ', NULL, NULL),
(35, 'Med ', NULL, NULL),
(36, 'Medium length', NULL, NULL),
(37, 'Long to lower back', NULL, NULL),
(38, 'shoulder-length', NULL, NULL),
(39, '10 inches', NULL, NULL),
(40, '10cm', NULL, NULL),
(41, 'Varies', NULL, NULL),
(42, ' 26\" ', NULL, NULL),
(43, '24â€', NULL, NULL),
(44, 'Chest', NULL, NULL),
(45, 'just below shoulders', NULL, NULL),
(46, 'Just below shoulder', NULL, NULL),
(47, 'Bust length', NULL, NULL),
(48, 'Shorr', NULL, NULL),
(49, 'Medium / shoulder', NULL, NULL),
(50, 'Medium shoulder ', NULL, NULL),
(51, 'Sholder', NULL, NULL),
(52, 'a few inches past the shoulder', NULL, NULL),
(53, 'chest length', NULL, NULL),
(54, '17 inches', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hair_styles`
--

CREATE TABLE `hair_styles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hair_style` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hair_styles`
--

INSERT INTO `hair_styles` (`id`, `hair_style`, `created_at`, `updated_at`) VALUES
(1, 'Afro', NULL, NULL),
(2, 'Bald', NULL, NULL),
(3, 'Bob', NULL, NULL),
(4, 'Braids', NULL, NULL),
(5, 'Buzz Cut', NULL, NULL),
(6, 'Classic', NULL, NULL),
(7, 'Comb Over', NULL, NULL),
(8, 'Cornrows', NULL, NULL),
(9, 'Curly', NULL, NULL),
(10, 'Dreads', NULL, NULL),
(11, 'Fauxhawk', NULL, NULL),
(12, 'Jerry Curl', NULL, NULL),
(13, 'Man Bun', NULL, NULL),
(14, 'Mohawk', NULL, NULL),
(15, 'Mullett', NULL, NULL),
(16, 'n/a', NULL, NULL),
(17, 'Perm', NULL, NULL),
(18, 'Punk / Spiked', NULL, NULL),
(19, 'Solid Color', NULL, NULL),
(20, 'Straight', NULL, NULL),
(21, 'Streaks', NULL, NULL),
(22, 'Wavy', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `handednesses`
--

CREATE TABLE `handednesses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `handedness` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `handednesses`
--

INSERT INTO `handednesses` (`id`, `handedness`, `created_at`, `updated_at`) VALUES
(1, 'Both (Ambdextrous)', NULL, NULL),
(2, 'Left', NULL, NULL),
(3, 'Right', NULL, NULL),
(4, 'Ambidextrous', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `internal_staffs`
--

CREATE TABLE `internal_staffs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('enable','disable') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'enable',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `internal_staffs`
--

INSERT INTO `internal_staffs` (`id`, `name`, `email`, `password`, `phonenumber`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(20, 'ankit', 'internal@domain.com', '$2y$10$hTzdn3oCW6R3Vj10zoav6.soMAw32Nidvvwbs0KBQ.HmnM3XyBkCa', '(11) 1111-1111', NULL, 'disable', '2019-07-09 02:25:33', '2019-07-09 06:37:42');

-- --------------------------------------------------------

--
-- Table structure for table `media_closets`
--

CREATE TABLE `media_closets` (
  `id` int(10) UNSIGNED NOT NULL,
  `media_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `closet_id` int(10) UNSIGNED NOT NULL,
  `performers_id` int(11) NOT NULL,
  `media_title` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_type` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media_portfolios`
--

CREATE TABLE `media_portfolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `media_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_id` int(10) UNSIGNED NOT NULL,
  `performers_id` int(11) DEFAULT NULL,
  `media_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media_portfolios`
--

INSERT INTO `media_portfolios` (`id`, `media_url`, `portfolio_id`, `performers_id`, `media_title`, `media_type`, `created_at`, `updated_at`) VALUES
(1, 'C958AE17-40EE-474E-8CE3-165F95D962BB.jpeg', 1, 2, NULL, 'jpeg', '2019-07-13 00:59:58', '2019-07-13 00:59:58'),
(2, 'BC4126F4-98EA-4B60-8F7E-ECE29EF3C52B.jpeg', 1, 2, NULL, 'jpeg', '2019-07-13 01:00:06', '2019-07-13 01:00:06'),
(3, 'FFAE8C3A-5771-482E-85EF-0232E9C569B6.jpeg', 1, 2, NULL, 'jpeg', '2019-07-13 01:00:14', '2019-07-13 01:00:14'),
(4, '20190317_142429.jpg', 2, 4, NULL, 'jpg', '2019-07-13 01:00:29', '2019-07-13 01:00:29'),
(5, '20190501_122547.jpg', 2, 4, NULL, 'jpg', '2019-07-13 01:00:36', '2019-07-13 01:00:36'),
(6, '20181110_125554.jpg', 2, 4, NULL, 'jpg', '2019-07-13 01:00:37', '2019-07-13 01:00:37'),
(7, 'Snapchat-954081419.jpg', 2, 4, NULL, 'jpg', '2019-07-13 01:00:39', '2019-07-13 01:00:39'),
(8, '20190317_142429-1.jpg', 3, 5, NULL, 'jpg', '2019-07-13 01:01:03', '2019-07-13 01:01:03'),
(9, '20190501_122547-1.jpg', 3, 5, NULL, 'jpg', '2019-07-13 01:01:10', '2019-07-13 01:01:10'),
(10, '20181110_125554-1.jpg', 3, 5, NULL, 'jpg', '2019-07-13 01:01:11', '2019-07-13 01:01:11'),
(11, 'Snapchat-954081419-1.jpg', 3, 5, NULL, 'jpg', '2019-07-13 01:01:13', '2019-07-13 01:01:13'),
(12, '20190320_221748.jpg', 4, 6, NULL, 'jpg', '2019-07-13 01:02:04', '2019-07-13 01:02:04'),
(13, '41688071_163478654562474_2516879315384139776_o_163478651229141.jpg', 4, 6, NULL, 'jpg', '2019-07-13 01:02:07', '2019-07-13 01:02:07'),
(14, '49465009_227652364811769_2181024928810139648_n_227652361478436.jpg', 4, 6, NULL, 'jpg', '2019-07-13 01:02:08', '2019-07-13 01:02:08'),
(15, '36428344_118074982436175_1360562484510982144_o_118074979102842.jpg', 4, 6, NULL, 'jpg', '2019-07-13 01:02:11', '2019-07-13 01:02:11'),
(16, '216CD3BA-76F0-4820-B4A4-C45DCCB33FAC.jpeg', 5, 7, NULL, 'jpeg', '2019-07-13 01:02:27', '2019-07-13 01:02:27'),
(17, 'EC008E23-6454-4E2C-960D-AD7504EDCCEE.jpeg', 5, 7, NULL, 'jpeg', '2019-07-13 01:02:40', '2019-07-13 01:02:40'),
(18, '478D6D9B-9A9A-4C14-95F5-621DAF5A4EDB.jpeg', 6, 8, NULL, 'jpeg', '2019-07-13 01:02:52', '2019-07-13 01:02:52'),
(19, 'EB555D1E-DEF0-40F3-A599-901BDCED5C90.jpeg', 6, 8, NULL, 'jpeg', '2019-07-13 01:02:55', '2019-07-13 01:02:55'),
(20, '4CCA7244-F56B-4678-AB4D-F1FC4DF2A1B1.jpeg', 6, 8, NULL, 'jpeg', '2019-07-13 01:02:58', '2019-07-13 01:02:58'),
(21, '91360083-00DD-4A31-8970-0E9F2B79AB9B.jpeg', 6, 8, NULL, 'jpeg', '2019-07-13 01:03:01', '2019-07-13 01:03:01'),
(22, '0041DD6F-71DC-4A14-9FE4-143776C2E3E0.jpeg', 7, 9, NULL, 'jpeg', '2019-07-13 01:03:36', '2019-07-13 01:03:36'),
(23, 'CFE44351-90D3-4ABE-A26C-4268291BB861.jpeg', 7, 9, NULL, 'jpeg', '2019-07-13 01:03:47', '2019-07-13 01:03:47'),
(24, 'me-1.jpg', 8, 10, NULL, 'jpg', '2019-07-13 01:03:53', '2019-07-13 01:03:53'),
(25, 'me-3.jpg', 8, 10, NULL, 'jpg', '2019-07-13 01:03:56', '2019-07-13 01:03:56'),
(26, 'me-2.jpg', 8, 10, NULL, 'jpg', '2019-07-13 01:03:59', '2019-07-13 01:03:59'),
(27, '6021E389-55A6-4DB6-9A6B-FA403E417983.jpeg', 9, 11, NULL, 'jpeg', '2019-07-13 01:04:08', '2019-07-13 01:04:08'),
(28, '618BF653-23AD-4326-86B4-19C870D3BF6E.jpeg', 9, 11, NULL, 'jpeg', '2019-07-13 01:04:18', '2019-07-13 01:04:18'),
(29, '20181014_133246.jpg', 10, 12, NULL, 'jpg', '2019-07-13 01:04:51', '2019-07-13 01:04:51'),
(30, '20180926_141944.jpg', 10, 12, NULL, 'jpg', '2019-07-13 01:05:05', '2019-07-13 01:05:05'),
(31, '88D5C84D-7D2B-4F25-8346-2D20CDA1C0C2.jpeg', 11, 13, NULL, 'jpeg', '2019-07-13 01:05:21', '2019-07-13 01:05:21'),
(32, '6ED446AD-D450-44F1-A487-F1EFC3C5D0DA.jpeg', 11, 13, NULL, 'jpeg', '2019-07-13 01:05:30', '2019-07-13 01:05:30'),
(33, '13E99E77-08D9-4A90-B009-059167A73B41.jpeg', 11, 13, NULL, 'jpeg', '2019-07-13 01:05:39', '2019-07-13 01:05:39'),
(34, '27FEA538-6E14-4191-97D5-8C1647CD06CC.jpeg', 11, 13, NULL, 'jpeg', '2019-07-13 01:05:44', '2019-07-13 01:05:44'),
(35, '4360FB71-084C-4655-B995-7F4C1D170FD8.jpeg', 12, 14, NULL, 'jpeg', '2019-07-13 01:05:53', '2019-07-13 01:05:53'),
(36, '968CB810-D084-4EB3-8A17-42E037E4D63A.png', 12, 14, NULL, 'png', '2019-07-13 01:06:21', '2019-07-13 01:06:21'),
(37, 'E82E15C4-0F54-4339-B4BE-C61F2062D5C6.jpeg', 12, 14, NULL, 'jpeg', '2019-07-13 01:06:27', '2019-07-13 01:06:27'),
(38, '61C32C48-1279-4205-B4BF-274FC7113605.png', 12, 14, NULL, 'png', '2019-07-13 01:06:43', '2019-07-13 01:06:43'),
(39, 'C7C1790F-8D1E-4B2A-AA5E-C7B5C4D8BF6E.jpeg', 13, 15, NULL, 'jpeg', '2019-07-13 01:06:52', '2019-07-13 01:06:52'),
(40, 'E78D2CC4-27B5-4FB2-B06D-D5A27B409642.png', 13, 15, NULL, 'png', '2019-07-13 01:07:03', '2019-07-13 01:07:03'),
(41, 'EAF1395B-E41E-4C85-B902-660DF915786C.jpeg', 13, 15, NULL, 'jpeg', '2019-07-13 01:07:08', '2019-07-13 01:07:08'),
(42, 'AB-full-detective.jpg', 14, 16, NULL, 'jpg', '2019-07-13 01:07:14', '2019-07-13 01:07:14'),
(43, '20181211_142304-2.jpeg', 14, 16, NULL, 'jpeg', '2019-07-13 01:07:17', '2019-07-13 01:07:17'),
(44, 'AB-Military.jpg', 14, 16, NULL, 'jpg', '2019-07-13 01:07:19', '2019-07-13 01:07:19'),
(45, 'AB-Sport.jpg', 14, 16, NULL, 'jpg', '2019-07-13 01:07:20', '2019-07-13 01:07:20'),
(46, 'AB-police.jpg', 14, 16, NULL, 'jpg', '2019-07-13 01:07:21', '2019-07-13 01:07:21'),
(47, 'AB-Military-Full.jpg', 14, 16, NULL, 'jpg', '2019-07-13 01:07:29', '2019-07-13 01:07:29'),
(48, 'AB-Military-2.jpg', 14, 16, NULL, 'jpg', '2019-07-13 01:07:30', '2019-07-13 01:07:30'),
(49, 'AB-FUll-Miltary.jpg', 14, 16, NULL, 'jpg', '2019-07-13 01:07:33', '2019-07-13 01:07:33'),
(50, 'AB-Beach.jpg', 14, 16, NULL, 'jpg', '2019-07-13 01:07:34', '2019-07-13 01:07:34'),
(51, 'Resume-.docx', 14, 16, NULL, 'docx', '2019-07-13 01:07:35', '2019-07-13 01:07:35'),
(52, 'Adam-Fedyk-Full-Body.jpg', 15, 18, NULL, 'jpg', '2019-07-13 01:07:42', '2019-07-13 01:07:42'),
(53, 'Adam-Fedyk-Waist-Up.jpg', 15, 18, NULL, 'jpg', '2019-07-13 01:07:48', '2019-07-13 01:07:48'),
(54, 'Adam-Fedyk-Headshot-Beard.jpg', 15, 18, NULL, 'jpg', '2019-07-13 01:07:53', '2019-07-13 01:07:53'),
(55, 'Adam-Fedyk-Headshot.jpg', 15, 18, NULL, 'jpg', '2019-07-13 01:07:57', '2019-07-13 01:07:57'),
(56, 'Adam-Fedyk-Resume.pdf', 15, 18, NULL, 'pdf', '2019-07-13 01:08:00', '2019-07-13 01:08:00'),
(57, '76E1EB44-5BE1-4C4E-9805-901AF5CB7647.png', 16, 19, NULL, 'png', '2019-07-13 01:08:16', '2019-07-13 01:08:16'),
(58, 'E49F6A90-EE9D-4956-ACB2-C6086EBAF8AB.jpeg', 16, 19, NULL, 'jpeg', '2019-07-13 01:08:19', '2019-07-13 01:08:19'),
(59, '1097657_10153115317280154_987207846_o.jpg', 17, 20, NULL, 'jpg', '2019-07-13 01:08:24', '2019-07-13 01:08:24'),
(60, '10398982_101857671685_5610527_n.jpg', 17, 20, NULL, 'jpg', '2019-07-13 01:08:26', '2019-07-13 01:08:26'),
(61, '20150825_091422.jpg', 17, 20, NULL, 'jpg', '2019-07-13 01:08:29', '2019-07-13 01:08:29'),
(62, '10398982_101857801685_2365372_n.jpg', 17, 20, NULL, 'jpg', '2019-07-13 01:08:30', '2019-07-13 01:08:30'),
(63, 'o.jpg', 17, 20, NULL, 'jpg', '2019-07-13 01:08:32', '2019-07-13 01:08:32'),
(64, 'IMG_9381.jpg', 17, 20, NULL, 'jpg', '2019-07-13 01:08:37', '2019-07-13 01:08:37'),
(65, '2014-09-05-17.10.03.png', 18, 22, NULL, 'png', '2019-07-13 01:08:52', '2019-07-13 01:08:52'),
(66, 'DSC00465.jpg', 18, 22, NULL, 'jpg', '2019-07-13 01:08:59', '2019-07-13 01:08:59'),
(67, 'DSC00547.jpg', 18, 22, NULL, 'jpg', '2019-07-13 01:09:05', '2019-07-13 01:09:05'),
(68, 'full-body.jpg', 19, 23, NULL, 'jpg', '2019-07-13 01:09:49', '2019-07-13 01:09:49'),
(69, 'half-body.jpg', 19, 23, NULL, 'jpg', '2019-07-13 01:09:51', '2019-07-13 01:09:51'),
(70, 'Adam-Nishi-resume-Feb-15-2019.pdf', 19, 23, NULL, 'pdf', '2019-07-13 01:09:53', '2019-07-13 01:09:53'),
(71, 'received_1674103425974775.jpeg', 20, 24, NULL, 'jpeg', '2019-07-13 01:09:59', '2019-07-13 01:09:59'),
(72, 'received_1660350860683365.jpeg', 20, 24, NULL, 'jpeg', '2019-07-13 01:10:04', '2019-07-13 01:10:04'),
(73, 'Headshot1-2.jpg', 20, 24, NULL, 'jpg', '2019-07-13 01:10:07', '2019-07-13 01:10:07'),
(74, 'AirBrush_20180224124540.jpg', 20, 24, NULL, 'jpg', '2019-07-13 01:10:17', '2019-07-13 01:10:17'),
(75, '88D38097-4A99-4B0A-9B4D-54F44C428779.jpeg', 21, 25, NULL, 'jpeg', '2019-07-13 01:10:23', '2019-07-13 01:10:23'),
(76, '933058F8-5EFF-4BE8-9264-3A830195D46D.jpeg', 21, 25, NULL, 'jpeg', '2019-07-13 01:10:32', '2019-07-13 01:10:32'),
(77, 'IMG_20190214_195401.jpg', 22, 26, NULL, 'jpg', '2019-07-13 01:10:36', '2019-07-13 01:10:36'),
(78, 'IMG_3600.jpg', 22, 26, NULL, 'jpg', '2019-07-13 01:10:38', '2019-07-13 01:10:38'),
(79, 'image000000_19.jpg', 23, 27, NULL, 'jpg', '2019-07-13 01:10:44', '2019-07-13 01:10:44'),
(80, 'image000000_18.jpg', 23, 27, NULL, 'jpg', '2019-07-13 01:10:48', '2019-07-13 01:10:48'),
(81, 'IMG_20190318_1500332.jpg', 24, 28, NULL, 'jpg', '2019-07-13 01:11:06', '2019-07-13 01:11:06'),
(82, 'IMG_20190321_1224242.jpg', 24, 28, NULL, 'jpg', '2019-07-13 01:11:11', '2019-07-13 01:11:11'),
(83, 'MVIMG_20190325_180820.jpg', 24, 28, NULL, 'jpg', '2019-07-13 01:11:31', '2019-07-13 01:11:31'),
(84, 'IMG_20190413_140221.jpg', 24, 28, NULL, 'jpg', '2019-07-13 01:11:38', '2019-07-13 01:11:38'),
(85, '18740256_10156212709377166_150344782738445483_n-1-1.jpg', 25, 29, NULL, 'jpg', '2019-07-13 01:11:44', '2019-07-13 01:11:44'),
(86, 'IMG_7469-2.jpg', 25, 29, NULL, 'jpg', '2019-07-13 01:11:48', '2019-07-13 01:11:48'),
(87, 'IMG_7467-2.jpg', 25, 29, NULL, 'jpg', '2019-07-13 01:11:51', '2019-07-13 01:11:51'),
(88, 'IMG_7467-2-1.jpg', 25, 29, NULL, 'jpg', '2019-07-13 01:11:55', '2019-07-13 01:11:55'),
(89, 'IMG_20190315_110828.jpg', 26, 30, NULL, 'jpg', '2019-07-13 01:12:06', '2019-07-13 01:12:06'),
(90, 'IMG_20190315_110339.jpg', 26, 30, NULL, 'jpg', '2019-07-13 01:12:11', '2019-07-13 01:12:11'),
(91, 'IMG_20190104_122330.jpg', 26, 30, NULL, 'jpg', '2019-07-13 01:12:17', '2019-07-13 01:12:17'),
(92, 'Screen-Shot-2018-04-23-at-10.26.54-AM.png', 27, 31, NULL, 'png', '2019-07-13 01:12:23', '2019-07-13 01:12:23'),
(93, '30171923_10214039934527447_1894365168078978442_o.jpg', 27, 31, NULL, 'jpg', '2019-07-13 01:12:26', '2019-07-13 01:12:26'),
(94, 'IMG_1356.png', 28, 32, NULL, 'png', '2019-07-13 01:12:35', '2019-07-13 01:12:35'),
(95, 'IMG_1358.png', 28, 32, NULL, 'png', '2019-07-13 01:12:40', '2019-07-13 01:12:40'),
(96, 'IMG_1361.png', 28, 32, NULL, 'png', '2019-07-13 01:12:44', '2019-07-13 01:12:44'),
(97, 'IMG_1351.png', 28, 32, NULL, 'png', '2019-07-13 01:12:48', '2019-07-13 01:12:48'),
(98, 'IMG_1350.png', 28, 32, NULL, 'png', '2019-07-13 01:12:52', '2019-07-13 01:12:52'),
(99, 'IMG_1347.png', 28, 32, NULL, 'png', '2019-07-13 01:12:55', '2019-07-13 01:12:55'),
(100, 'IMG_1341.png', 28, 32, NULL, 'png', '2019-07-13 01:13:02', '2019-07-13 01:13:02'),
(101, 'IMG_1345.png', 28, 32, NULL, 'png', '2019-07-13 01:13:08', '2019-07-13 01:13:08'),
(102, 'IMG_1357.png', 28, 32, NULL, 'png', '2019-07-13 01:13:15', '2019-07-13 01:13:15'),
(103, 'IMG_8536.jpg', 29, 33, NULL, 'jpg', '2019-07-13 01:13:29', '2019-07-13 01:13:29'),
(104, 'IMG_8385.jpg', 29, 33, NULL, 'jpg', '2019-07-13 01:13:34', '2019-07-13 01:13:34'),
(105, 'IMG_8576.jpg', 29, 33, NULL, 'jpg', '2019-07-13 01:13:36', '2019-07-13 01:13:36'),
(106, 'revised-resume-1-1.pdf', 29, 33, NULL, 'pdf', '2019-07-13 01:13:38', '2019-07-13 01:13:38'),
(107, 'B5DA99A7-BE28-42C1-A348-B8D6BCFCFEEC.jpeg', 30, 34, NULL, 'jpeg', '2019-07-13 01:13:46', '2019-07-13 01:13:46'),
(108, 'C6A940BB-A295-400A-8455-2A6E212C5201.jpeg', 30, 34, NULL, 'jpeg', '2019-07-13 01:13:54', '2019-07-13 01:13:54'),
(109, '20180402_154056.jpg', 31, 35, NULL, 'jpg', '2019-07-13 01:14:12', '2019-07-13 01:14:12'),
(110, 'adriana_caldwell-104.jpg', 31, 35, NULL, 'jpg', '2019-07-13 01:14:25', '2019-07-13 01:14:25'),
(111, 'stand-in-resume-adri.docx', 31, 35, NULL, 'docx', '2019-07-13 01:14:26', '2019-07-13 01:14:26'),
(112, 'Aidan-2017-1.jpeg', 32, 38, NULL, 'jpeg', '2019-07-13 01:14:33', '2019-07-13 01:14:33'),
(113, 'Aidan-pillar-1.jpeg', 32, 38, NULL, 'jpeg', '2019-07-13 01:14:36', '2019-07-13 01:14:36'),
(114, 'DSC_0058.jpeg', 32, 38, NULL, 'jpeg', '2019-07-13 01:14:39', '2019-07-13 01:14:39'),
(115, 'fullsizeoutput_415f.jpeg', 32, 38, NULL, 'jpeg', '2019-07-13 01:14:45', '2019-07-13 01:14:45'),
(116, 'Aidan-2017-1-1.jpeg', 33, 40, NULL, 'jpeg', '2019-07-13 01:14:50', '2019-07-13 01:14:50'),
(117, 'Aidan-pillar-1-1.jpeg', 33, 40, NULL, 'jpeg', '2019-07-13 01:14:53', '2019-07-13 01:14:53'),
(118, 'fullsizeoutput_415f-1.jpeg', 33, 40, NULL, 'jpeg', '2019-07-13 01:14:58', '2019-07-13 01:14:58'),
(119, 'fullsizeoutput_710.jpeg', 33, 40, NULL, 'jpeg', '2019-07-13 01:15:05', '2019-07-13 01:15:05'),
(120, 'fullsizeoutput_f7.jpeg', 33, 40, NULL, 'jpeg', '2019-07-13 01:15:11', '2019-07-13 01:15:11'),
(121, 'IMG_20190430_222016.jpg', 34, 41, NULL, 'jpg', '2019-07-13 01:15:33', '2019-07-13 01:15:33'),
(122, 'IMG_20190430_222731.jpg', 34, 41, NULL, 'jpg', '2019-07-13 01:15:38', '2019-07-13 01:15:38'),
(123, 'IMG_20190430_222614.jpg', 34, 41, NULL, 'jpg', '2019-07-13 01:15:42', '2019-07-13 01:15:42'),
(124, 'IMG_20190430_222108.jpg', 34, 41, NULL, 'jpg', '2019-07-13 01:15:49', '2019-07-13 01:15:49'),
(125, 'IMG_20190430_222837.jpg', 34, 41, NULL, 'jpg', '2019-07-13 01:15:52', '2019-07-13 01:15:52'),
(126, 'Resized_20190415_193959_1442.jpeg', 34, 41, NULL, 'jpeg', '2019-07-13 01:15:55', '2019-07-13 01:15:55'),
(127, 'IMG_9288.jpg', 35, 43, NULL, 'jpg', '2019-07-13 01:16:24', '2019-07-13 01:16:24'),
(128, 'IMG_9289-1.jpg', 35, 43, NULL, 'jpg', '2019-07-13 01:16:32', '2019-07-13 01:16:32'),
(129, 'IMG_8757.jpg', 35, 43, NULL, 'jpg', '2019-07-13 01:16:35', '2019-07-13 01:16:35'),
(130, 'IMG_7079.jpg', 35, 43, NULL, 'jpg', '2019-07-13 01:16:43', '2019-07-13 01:16:43'),
(131, 'image-10.jpg', 36, 44, NULL, 'jpg', '2019-07-13 01:16:53', '2019-07-13 01:16:53'),
(132, 'image-11.jpg', 36, 44, NULL, 'jpg', '2019-07-13 01:16:58', '2019-07-13 01:16:58'),
(133, '82656417-376F-4B3B-8190-FCDD27E4B073.jpeg', 36, 44, NULL, 'jpeg', '2019-07-13 01:17:01', '2019-07-13 01:17:01'),
(134, 'D07C5F5B-B3C0-427D-99EA-AA77FE2A6E8E.jpeg', 36, 44, NULL, 'jpeg', '2019-07-13 01:17:05', '2019-07-13 01:17:05'),
(135, '1.jpg', 37, 46, NULL, 'jpg', '2019-07-13 01:23:11', '2019-07-13 01:23:11'),
(136, 'headshot5.jpg', 37, 46, NULL, 'jpg', '2019-07-13 01:23:14', '2019-07-13 01:23:14'),
(137, 'alanyu1.jpg', 37, 46, NULL, 'jpg', '2019-07-13 01:23:17', '2019-07-13 01:23:17'),
(138, 'alanyu2.jpg', 37, 46, NULL, 'jpg', '2019-07-13 01:23:19', '2019-07-13 01:23:19'),
(139, 'alanyu3.jpg', 37, 46, NULL, 'jpg', '2019-07-13 01:23:21', '2019-07-13 01:23:21'),
(140, 'actres2018.pdf', 37, 46, NULL, 'pdf', '2019-07-13 01:23:23', '2019-07-13 01:23:23'),
(141, '799BABBF-5D5D-424C-B663-8817D1197896.jpeg', 38, 47, NULL, 'jpeg', '2019-07-13 01:23:32', '2019-07-13 01:23:32'),
(142, 'A5453344-54CF-4ABC-B0EB-522AB9DFE37E.jpeg', 38, 47, NULL, 'jpeg', '2019-07-13 01:23:35', '2019-07-13 01:23:35'),
(143, '3B150D59-25D1-4573-8F47-467BE35ED6DF.jpeg', 38, 47, NULL, 'jpeg', '2019-07-13 01:23:38', '2019-07-13 01:23:38'),
(144, '589F34F4-C4CF-4FD4-B1C7-7792C714FFC3.jpeg', 38, 47, NULL, 'jpeg', '2019-07-13 01:23:41', '2019-07-13 01:23:41'),
(145, '14F4353A-20CA-4ABD-9D10-9F8AD31BDA05.jpeg', 39, 48, NULL, 'jpeg', '2019-07-13 01:23:56', '2019-07-13 01:23:56'),
(146, 'F166A739-1E0F-405E-AA97-8DD99E67348C.jpeg', 39, 48, NULL, 'jpeg', '2019-07-13 01:24:01', '2019-07-13 01:24:01'),
(147, '10EFBC9A-E50E-49EC-AD6C-A31A07EB514E.jpeg', 39, 48, NULL, 'jpeg', '2019-07-13 01:24:04', '2019-07-13 01:24:04'),
(148, '0BFBAE2F-64B1-4FEC-904E-8849D208295B.jpeg', 39, 48, NULL, 'jpeg', '2019-07-13 01:24:07', '2019-07-13 01:24:07'),
(149, 'CEA80821-B883-47F4-AA12-E3A5FD3074CF.jpeg', 39, 48, NULL, 'jpeg', '2019-07-13 01:24:16', '2019-07-13 01:24:16'),
(150, '2B039E73-DD6F-47EE-8B23-57E2B9F32998.jpeg', 39, 48, NULL, 'jpeg', '2019-07-13 01:24:28', '2019-07-13 01:24:28'),
(151, '8FC8C148-D5BE-449A-A99E-5D9B55D2D74E.jpeg', 39, 48, NULL, 'jpeg', '2019-07-13 01:24:36', '2019-07-13 01:24:36'),
(152, '8C0F7E23-93CC-43D9-B9D2-34AB73FC7ABD.png', 39, 48, NULL, 'png', '2019-07-13 01:25:08', '2019-07-13 01:25:08'),
(153, '1B99FB5B-1A65-4BEF-A752-2DC307CB7CA7.jpeg', 39, 48, NULL, 'jpeg', '2019-07-13 01:25:10', '2019-07-13 01:25:10'),
(154, '20180901_133313.jpg', 40, 49, NULL, 'jpg', '2019-07-13 01:25:38', '2019-07-13 01:25:38'),
(155, '20180612_174921.jpg', 40, 49, NULL, 'jpg', '2019-07-13 01:25:45', '2019-07-13 01:25:45'),
(156, '20180901_110945.jpg', 40, 49, NULL, 'jpg', '2019-07-13 01:25:55', '2019-07-13 01:25:55'),
(157, '20180922_114449.jpg', 40, 49, NULL, 'jpg', '2019-07-13 01:26:07', '2019-07-13 01:26:07'),
(158, 'Alana-Richardson-Full-Body.png', 41, 50, NULL, 'png', '2019-07-13 01:26:19', '2019-07-13 01:26:19'),
(159, 'AlanaScarlett-Richardson-Waist-Up.jpg', 41, 50, NULL, 'jpg', '2019-07-13 01:26:22', '2019-07-13 01:26:22'),
(160, 'AlanaScarlett-Richardson-Additional-Picture.jpg', 41, 50, NULL, 'jpg', '2019-07-13 01:26:24', '2019-07-13 01:26:24'),
(161, 'Alana-Richardson-Additional-Picture-2.png', 41, 50, NULL, 'png', '2019-07-13 01:26:25', '2019-07-13 01:26:25'),
(162, 'Albert_wholebody.jpg', 42, 51, NULL, 'jpg', '2019-07-13 01:26:37', '2019-07-13 01:26:37'),
(163, 'Albert_waistup.jpg', 42, 51, NULL, 'jpg', '2019-07-13 01:26:39', '2019-07-13 01:26:39'),
(164, 'Mayfield-1.jpg', 43, 52, NULL, 'jpg', '2019-07-13 01:26:44', '2019-07-13 01:26:44'),
(165, 'Mayfield-2.jpg', 43, 52, NULL, 'jpg', '2019-07-13 01:26:46', '2019-07-13 01:26:46'),
(166, 'AC-REZUMAY.docx', 43, 52, NULL, 'docx', '2019-07-13 01:26:47', '2019-07-13 01:26:47'),
(167, 'EB60F8FF-826A-4B94-91AD-3B07DF002C26.jpeg', 44, 53, NULL, 'jpeg', '2019-07-13 01:26:56', '2019-07-13 01:26:56'),
(168, '3386F1E8-86E9-4B8B-814B-91C101C5710F.jpeg', 44, 53, NULL, 'jpeg', '2019-07-13 01:27:01', '2019-07-13 01:27:01'),
(169, 'C87B679C-8D1A-4C98-AB43-3111A4CD046D.jpeg', 44, 53, NULL, 'jpeg', '2019-07-13 01:27:06', '2019-07-13 01:27:06'),
(170, '8B8A813A-A9BF-4FC9-817C-0574F3C9C2D2.jpeg', 44, 53, NULL, 'jpeg', '2019-07-13 01:27:09', '2019-07-13 01:27:09'),
(171, 'Screen-Shot-2018-08-03-at-10.15.55-PM.png', 45, 54, NULL, 'png', '2019-07-13 01:27:17', '2019-07-13 01:27:17'),
(172, '38537216_1207067079436251_5277950130253725696_n.jpg', 45, 54, NULL, 'jpg', '2019-07-13 01:27:19', '2019-07-13 01:27:19'),
(173, '44516541_358702498204999_6351741926198214656_n.jpg', 45, 54, NULL, 'jpg', '2019-07-13 01:27:21', '2019-07-13 01:27:21'),
(174, '38540465_1645503492238789_6168170479891775488_n.jpg', 45, 54, NULL, 'jpg', '2019-07-13 01:27:23', '2019-07-13 01:27:23'),
(175, '44502931_340291336739646_2954278505635905536_n.jpg', 45, 54, NULL, 'jpg', '2019-07-13 01:27:25', '2019-07-13 01:27:25'),
(176, '44617871_242362339776869_672987646905024512_n.jpg', 45, 54, NULL, 'jpg', '2019-07-13 01:27:26', '2019-07-13 01:27:26'),
(177, '38404848_2187499281279752_2534425618292932608_n.jpg', 45, 54, NULL, 'jpg', '2019-07-13 01:27:29', '2019-07-13 01:27:29'),
(178, 'Screen-Shot-2018-08-03-at-10.16.16-PM.png', 45, 54, NULL, 'png', '2019-07-13 01:27:33', '2019-07-13 01:27:33'),
(179, '8DAD0BB3-AB02-4AD9-94CD-C662BFC93A8F.jpeg', 46, 55, NULL, 'jpeg', '2019-07-13 01:27:55', '2019-07-13 01:27:55'),
(180, 'BC663B30-CB1D-4F3B-8738-5A64E76BC31F.jpeg', 46, 55, NULL, 'jpeg', '2019-07-13 01:28:11', '2019-07-13 01:28:11'),
(181, '97B05BA7-0DFB-4232-B6F0-6415846132D6.jpeg', 46, 55, NULL, 'jpeg', '2019-07-13 01:28:24', '2019-07-13 01:28:24'),
(182, 'Alexa-Ross-4.jpg', 47, 56, NULL, 'jpg', '2019-07-13 01:28:51', '2019-07-13 01:28:51'),
(183, 'Alexa-Ross-3.jpg', 47, 56, NULL, 'jpg', '2019-07-13 01:29:03', '2019-07-13 01:29:03'),
(184, 'Alexa-Ross-2.jpg', 47, 56, NULL, 'jpg', '2019-07-13 01:29:13', '2019-07-13 01:29:13'),
(185, 'IMG_20181029_111319_703.jpg', 48, 57, NULL, 'jpg', '2019-07-13 01:29:20', '2019-07-13 01:29:20'),
(186, 'IMG_20180609_184534_681-1.jpg', 48, 57, NULL, 'jpg', '2019-07-13 01:29:24', '2019-07-13 01:29:24'),
(187, 'IMG-20190206-WA0104.jpg', 48, 57, NULL, 'jpg', '2019-07-13 01:29:26', '2019-07-13 01:29:26'),
(188, 'IMG-20180820-WA0006.jpg', 48, 57, NULL, 'jpg', '2019-07-13 01:29:31', '2019-07-13 01:29:31'),
(189, 'DSC_4746.jpg', 49, 58, NULL, 'jpg', '2019-07-13 01:29:47', '2019-07-13 01:29:47'),
(190, 'DSC_4775.jpg', 49, 58, NULL, 'jpg', '2019-07-13 01:29:52', '2019-07-13 01:29:52'),
(191, 'Alex-full-body.jpg', 50, 59, NULL, 'jpg', '2019-07-13 01:29:56', '2019-07-13 01:29:56'),
(192, 'alex-smiling-black-bkg-4.jpg', 50, 59, NULL, 'jpg', '2019-07-13 01:29:59', '2019-07-13 01:29:59'),
(193, 'Alex-Ross-resume-2018.pdf', 50, 59, NULL, 'pdf', '2019-07-13 01:30:01', '2019-07-13 01:30:01'),
(194, '20190227_201621.jpg', 51, 60, NULL, 'jpg', '2019-07-13 01:30:15', '2019-07-13 01:30:15'),
(195, 'IMG_2572_pp-copy.jpg', 51, 60, NULL, 'jpg', '2019-07-13 01:30:20', '2019-07-13 01:30:20'),
(196, 'IMG_3250_pp3-copy.jpg', 51, 60, NULL, 'jpg', '2019-07-13 01:30:27', '2019-07-13 01:30:27'),
(197, '20190114_093343.jpg', 51, 60, NULL, 'jpg', '2019-07-13 01:30:29', '2019-07-13 01:30:29'),
(198, 'Alex-Lee.pdf', 51, 60, NULL, 'pdf', '2019-07-13 01:30:31', '2019-07-13 01:30:31'),
(199, 'fmkam2-2.jpg', 52, 62, NULL, 'jpg', '2019-07-13 01:30:39', '2019-07-13 01:30:39'),
(200, 'fmkam3-2.jpg', 52, 62, NULL, 'jpg', '2019-07-13 01:30:42', '2019-07-13 01:30:42'),
(201, '119CFAF8-AF72-46AB-86EC-083029ACA5B7.jpeg', 53, 63, NULL, 'jpeg', '2019-07-13 01:31:02', '2019-07-13 01:31:02'),
(202, '86448A12-B7FB-428C-A9AE-F8D81F3B63B5.jpeg', 53, 63, NULL, 'jpeg', '2019-07-13 01:31:10', '2019-07-13 01:31:10'),
(203, '1563001285p 12.jpg', 54, NULL, 'image title 1', 'image/jpeg', NULL, NULL),
(204, '1563001285p 13.jpg', 54, NULL, 'image title 1', 'image/jpeg', NULL, NULL),
(205, '37D87556-136D-4DFF-AD00-91838AE96E45.png', 53, 63, NULL, 'png', '2019-07-13 01:31:33', '2019-07-13 01:31:33'),
(206, 'F2A18829-4686-4FFA-96B0-10D0C09B5A3C.jpeg', 55, 64, NULL, 'jpeg', '2019-07-13 01:31:56', '2019-07-13 01:31:56'),
(207, '84B60486-E681-4AF8-BE94-B6B804FE4D2C.jpeg', 55, 64, NULL, 'jpeg', '2019-07-13 01:32:03', '2019-07-13 01:32:03'),
(208, '1D8923B9-9CF4-4EAB-BA7A-2A217EB68277.jpeg', 55, 64, NULL, 'jpeg', '2019-07-13 01:32:09', '2019-07-13 01:32:09'),
(209, 'IMG_1759.jpg', 56, 65, NULL, 'jpg', '2019-07-13 01:32:18', '2019-07-13 01:32:18'),
(210, 'IMG_1925.jpg', 56, 65, NULL, 'jpg', '2019-07-13 01:32:20', '2019-07-13 01:32:20'),
(211, 'IMG_1920.jpg', 56, 65, NULL, 'jpg', '2019-07-13 01:32:22', '2019-07-13 01:32:22'),
(212, 'IMG_1923.jpg', 56, 65, NULL, 'jpg', '2019-07-13 01:32:25', '2019-07-13 01:32:25'),
(213, 'IMG_1922.jpg', 56, 65, NULL, 'jpg', '2019-07-13 01:32:27', '2019-07-13 01:32:27'),
(214, '26C4C1C0-A145-499C-9694-E28BCE0B5B1E.jpeg', 57, 66, NULL, 'jpeg', '2019-07-13 01:32:44', '2019-07-13 01:32:44'),
(215, '35E82066-5362-4FFC-8C0F-C9FEA920690B.jpeg', 57, 66, NULL, 'jpeg', '2019-07-13 01:32:53', '2019-07-13 01:32:53'),
(216, 'B8636E2D-E43F-452E-9327-5F03D1B0A445.jpeg', 57, 66, NULL, 'jpeg', '2019-07-13 01:33:03', '2019-07-13 01:33:03'),
(217, 'DEE1C844-C0C3-4D17-BDFE-23FD51418AAD.jpeg', 57, 66, NULL, 'jpeg', '2019-07-13 01:33:15', '2019-07-13 01:33:15'),
(218, '572B45D0-3494-4D7F-B612-655094AEF6D2.jpeg', 57, 66, NULL, 'jpeg', '2019-07-13 01:33:18', '2019-07-13 01:33:18'),
(219, '47FBCA9C-5BA7-4BB1-856A-38C5511D56A4.jpeg', 57, 66, NULL, 'jpeg', '2019-07-13 01:33:26', '2019-07-13 01:33:26'),
(220, '87145C09-488E-47DA-A599-BC90580C8F5E.jpeg', 58, 67, NULL, 'jpeg', '2019-07-13 01:33:40', '2019-07-13 01:33:40'),
(221, '26733546-6BC7-4447-9C39-B1CC63C100A3.jpeg', 58, 67, NULL, 'jpeg', '2019-07-13 01:33:42', '2019-07-13 01:33:42'),
(222, '76918CA4-43DC-495E-B9A5-001454251B73.jpeg', 58, 67, NULL, 'jpeg', '2019-07-13 01:33:45', '2019-07-13 01:33:45'),
(223, '6A798F66-F692-4B44-8027-7F5AF6EBE6E1.jpeg', 58, 67, NULL, 'jpeg', '2019-07-13 01:33:47', '2019-07-13 01:33:47'),
(224, 'IMG_A106C45E246B-1-1.jpeg', 60, 69, NULL, 'jpeg', '2019-07-13 01:34:06', '2019-07-13 01:34:06'),
(225, 'IMG_A106C45E246B-1-2.jpeg', 60, 69, NULL, 'jpeg', '2019-07-13 01:34:12', '2019-07-13 01:34:12'),
(226, '1ED11DE4-DFF6-4C45-BFDB-8BCEB7D00CBA.jpeg', 61, 70, NULL, 'jpeg', '2019-07-13 01:34:22', '2019-07-13 01:34:22'),
(227, '50684618-7BE3-42E6-9084-1E4481FF2039.jpeg', 61, 70, NULL, 'jpeg', '2019-07-13 01:34:28', '2019-07-13 01:34:28'),
(228, '39879308-6CE7-45CA-A132-B49E27708CE8.jpeg', 61, 70, NULL, 'jpeg', '2019-07-13 01:34:30', '2019-07-13 01:34:30'),
(229, '379E8260-4521-4427-9B20-79F702D9645D.jpeg', 61, 70, NULL, 'jpeg', '2019-07-13 01:34:33', '2019-07-13 01:34:33'),
(230, '2363.jpg', 62, 71, NULL, 'jpg', '2019-07-13 01:34:38', '2019-07-13 01:34:38'),
(231, '123.jpg', 62, 71, NULL, 'jpg', '2019-07-13 01:34:46', '2019-07-13 01:34:46'),
(232, 'Allan-Luna-Res18.pdf', 62, 71, NULL, 'pdf', '2019-07-13 01:34:50', '2019-07-13 01:34:50'),
(233, 'AdevereauxFB.jpg', 63, 72, NULL, 'jpg', '2019-07-13 01:35:00', '2019-07-13 01:35:00'),
(234, 'AdevereauxWS.jpg', 63, 72, NULL, 'jpg', '2019-07-13 01:35:08', '2019-07-13 01:35:08'),
(235, 'ADevereaux_HSG.jpg', 63, 72, NULL, 'jpg', '2019-07-13 01:35:19', '2019-07-13 01:35:19'),
(236, 'Ally-Warren-business-smile.jpg', 64, 73, NULL, 'jpg', '2019-07-13 01:35:25', '2019-07-13 01:35:25'),
(237, 'Loudermilk.png', 64, 73, NULL, 'png', '2019-07-13 01:35:29', '2019-07-13 01:35:29'),
(238, 'Ally-Warren-Standin-Resume-2019.pdf', 64, 73, NULL, 'pdf', '2019-07-13 01:35:31', '2019-07-13 01:35:31'),
(239, 'LuxPortrait-Ally-W-25474-dpownsized-for-internt.jpg', 65, 74, NULL, 'jpg', '2019-07-13 01:35:51', '2019-07-13 01:35:51'),
(240, 'LuxPortrait-Ally-W-25310.jpg', 65, 74, NULL, 'jpg', '2019-07-13 01:35:57', '2019-07-13 01:35:57'),
(241, 'Ally-Warren-lux-96-imdb-downsized-for-internet.jpg', 65, 74, NULL, 'jpg', '2019-07-13 01:36:02', '2019-07-13 01:36:02'),
(242, '31-to-headshot-format.jpg', 65, 74, NULL, 'jpg', '2019-07-13 01:36:18', '2019-07-13 01:36:18'),
(243, 'Marksman.jpg', 65, 74, NULL, 'jpg', '2019-07-13 01:36:19', '2019-07-13 01:36:19'),
(244, 'Ally-Warren-Antigua-Equestrian-Horse-Surfing-and-Trail-Guide.jpg', 65, 74, NULL, 'jpg', '2019-07-13 01:36:21', '2019-07-13 01:36:21'),
(245, 'Ally-Warren-Headshot-2.jpg', 65, 74, NULL, 'jpg', '2019-07-13 01:36:23', '2019-07-13 01:36:23'),
(246, 'Ally-Warren-Standin-Resume-APRIL-2018.pdf', 65, 74, NULL, 'pdf', '2019-07-13 01:36:26', '2019-07-13 01:36:26'),
(247, 'Allison-Warren-2014.jpg', 66, 75, NULL, 'jpg', '2019-07-13 01:36:31', '2019-07-13 01:36:31'),
(248, 'Ally-Warren-2017.jpg', 66, 75, NULL, 'jpg', '2019-07-13 01:36:34', '2019-07-13 01:36:34'),
(249, 'Ally-Warren-Bates-Motel-2015.png', 66, 75, NULL, 'png', '2019-07-13 01:36:45', '2019-07-13 01:36:45'),
(250, 'photo.jpg', 66, 75, NULL, 'jpg', '2019-07-13 01:36:52', '2019-07-13 01:36:52'),
(251, 'Ally-Warren-Standin-Resume-APRIL-2018.pdf', 66, 75, NULL, 'pdf', '2019-07-13 01:36:54', '2019-07-13 01:36:54'),
(252, 'wm31.jpg', 67, 76, NULL, 'jpg', '2019-07-13 01:37:08', '2019-07-13 01:37:08'),
(253, 'AlyciaEscoval071_WEB.jpg', 67, 76, NULL, 'jpg', '2019-07-13 01:37:15', '2019-07-13 01:37:15'),
(254, 'AlyciaEscoval002_WEB.jpg', 67, 76, NULL, 'jpg', '2019-07-13 01:37:17', '2019-07-13 01:37:17'),
(255, 'Resume-Alycia-Escoval.doc', 67, 76, NULL, 'doc', '2019-07-13 01:37:20', '2019-07-13 01:37:20'),
(256, '31562060_10156382300901477_6265396668183609344_n.jpg', 68, 77, NULL, 'jpg', '2019-07-13 01:37:31', '2019-07-13 01:37:31'),
(257, 'SmartSelect_20180430-225735_Gallery.jpg', 68, 77, NULL, 'jpg', '2019-07-13 01:37:36', '2019-07-13 01:37:36'),
(258, 'Screen-Shot-2018-04-30-at-11.16.31-PM.png', 68, 77, NULL, 'png', '2019-07-13 01:37:40', '2019-07-13 01:37:40'),
(259, 'Alysha_Seriani_CV.pdf', 68, 77, NULL, 'pdf', '2019-07-13 01:37:42', '2019-07-13 01:37:42'),
(260, 'AlyssaYong_Photo4.jpg', 69, 78, NULL, 'jpg', '2019-07-13 01:37:51', '2019-07-13 01:37:51'),
(261, 'AlyssaYong_Photo1.jpg', 69, 78, NULL, 'jpg', '2019-07-13 01:37:53', '2019-07-13 01:37:53'),
(262, 'AlyssaYong_Photo3.jpg', 69, 78, NULL, 'jpg', '2019-07-13 01:37:56', '2019-07-13 01:37:56'),
(263, '54521050_2366565563573294_3933284487936344064_n.jpg', 70, 79, NULL, 'jpg', '2019-07-13 01:38:04', '2019-07-13 01:38:04'),
(264, '54516608_645366929225942_1361065292037357568_n.jpg', 70, 79, NULL, 'jpg', '2019-07-13 01:38:08', '2019-07-13 01:38:08'),
(265, '40553652_10156323187840733_2199807954666913792_o.jpg', 70, 79, NULL, 'jpg', '2019-07-13 01:38:13', '2019-07-13 01:38:13'),
(266, '40854069_10156329065175733_2123503183434088448_n.jpg', 70, 79, NULL, 'jpg', '2019-07-13 01:38:15', '2019-07-13 01:38:15'),
(267, '53198238_10156853337725170_6287270910852333568_o.jpg', 70, 79, NULL, 'jpg', '2019-07-13 01:38:17', '2019-07-13 01:38:17'),
(268, '53897004_10156863810895170_3149413399767547904_n.jpg', 70, 79, NULL, 'jpg', '2019-07-13 01:38:19', '2019-07-13 01:38:19'),
(269, '54361776_10156863106030170_7413564837994692608_o.jpg', 70, 79, NULL, 'jpg', '2019-07-13 01:38:20', '2019-07-13 01:38:20'),
(270, '54257337_10156858827165170_8988833983173230592_o.jpg', 70, 79, NULL, 'jpg', '2019-07-13 01:38:22', '2019-07-13 01:38:22'),
(271, 'aman-full.jpg', 71, 80, NULL, 'jpg', '2019-07-13 01:38:29', '2019-07-13 01:38:29'),
(272, 'aman.jpg', 71, 80, NULL, 'jpg', '2019-07-13 01:38:31', '2019-07-13 01:38:31'),
(273, 'Aman-Webeshet-background-or-acting-resume.rtf', 71, 80, NULL, 'rtf', '2019-07-13 01:38:46', '2019-07-13 01:38:46'),
(274, 'amandaearly3.jpg', 72, 81, NULL, 'jpg', '2019-07-13 01:39:10', '2019-07-13 01:39:10'),
(275, 'amandaearly2.jpg', 72, 81, NULL, 'jpg', '2019-07-13 01:39:18', '2019-07-13 01:39:18'),
(276, 'amandaearly4.jpg', 72, 81, NULL, 'jpg', '2019-07-13 01:39:26', '2019-07-13 01:39:26'),
(277, 'amandaearly5.jpg', 72, 81, NULL, 'jpg', '2019-07-13 01:39:43', '2019-07-13 01:39:43'),
(278, 'amandaearly6.jpg', 72, 81, NULL, 'jpg', '2019-07-13 01:39:55', '2019-07-13 01:39:55'),
(279, 'amandaearly7.jpg', 72, 81, NULL, 'jpg', '2019-07-13 01:40:03', '2019-07-13 01:40:03'),
(280, 'amandaearly8.jpg', 72, 81, NULL, 'jpg', '2019-07-13 01:40:12', '2019-07-13 01:40:12'),
(281, 'amandaearly9.jpg', 72, 81, NULL, 'jpg', '2019-07-13 01:40:22', '2019-07-13 01:40:22'),
(282, 'amandaearly10.jpg', 72, 81, NULL, 'jpg', '2019-07-13 01:40:27', '2019-07-13 01:40:27'),
(283, '6CAAF24D-52CE-417C-918E-5A27FB94B717.jpeg', 73, 82, NULL, 'jpeg', '2019-07-13 01:40:33', '2019-07-13 01:40:33'),
(284, 'C7CF63D4-EDA1-422D-BD4D-FF8364EAA4FD.jpeg', 73, 82, NULL, 'jpeg', '2019-07-13 01:40:35', '2019-07-13 01:40:35'),
(285, 'EDCEEC24-BA34-473A-A094-C54B65D43807.jpeg', 74, 83, NULL, 'jpeg', '2019-07-13 01:40:40', '2019-07-13 01:40:40'),
(286, '47C57DEC-CBDE-4E8C-971E-BEEDEF80A16A.jpeg', 74, 83, NULL, 'jpeg', '2019-07-13 01:40:42', '2019-07-13 01:40:42'),
(287, '1A35F2C0-ED0B-4C62-AC06-EB166321328C.jpeg', 75, 84, NULL, 'jpeg', '2019-07-13 01:40:48', '2019-07-13 01:40:48'),
(288, 'E0B0963C-8032-4FDB-8748-35E78FCDF8DB.jpeg', 75, 84, NULL, 'jpeg', '2019-07-13 01:40:53', '2019-07-13 01:40:53'),
(289, '13658977_10153556599960881_8163353531886452907_n.jpg', 76, 85, NULL, 'jpg', '2019-07-13 01:40:59', '2019-07-13 01:40:59'),
(290, '12832449_10153275669685881_1074756340313576769_n.jpg', 76, 85, NULL, 'jpg', '2019-07-13 01:41:04', '2019-07-13 01:41:04'),
(291, '85322A77-5C7B-4C6E-8471-ED674BE8CEB4.jpeg', 77, 86, NULL, 'jpeg', '2019-07-13 01:41:11', '2019-07-13 01:41:11'),
(292, '7690638B-473E-4BED-BE4A-E7B899BD64F3.jpeg', 77, 86, NULL, 'jpeg', '2019-07-13 01:41:14', '2019-07-13 01:41:14'),
(293, 'C53C2A80-E565-4C11-BE4E-AD0B73CD5398.jpeg', 77, 86, NULL, 'jpeg', '2019-07-13 01:41:20', '2019-07-13 01:41:20'),
(294, '5632479F-0BF5-400E-9CBE-EF51669A3471.jpeg', 77, 86, NULL, 'jpeg', '2019-07-13 01:41:23', '2019-07-13 01:41:23'),
(295, '9CA8E6AB-2EF7-48CB-9824-944E418CFB9F.jpeg', 77, 86, NULL, 'jpeg', '2019-07-13 01:41:29', '2019-07-13 01:41:29'),
(296, 'A87CA63F-E0A5-44C9-8A46-C709A75EA3CE.jpeg', 77, 86, NULL, 'jpeg', '2019-07-13 01:41:34', '2019-07-13 01:41:34'),
(297, 'amandaearly3.jpg', 78, 87, NULL, 'jpg', '2019-07-13 01:41:53', '2019-07-13 01:41:53'),
(298, 'amandaearly5.jpg', 78, 87, NULL, 'jpg', '2019-07-13 01:41:57', '2019-07-13 01:41:57'),
(299, 'amandaearly6.jpg', 78, 87, NULL, 'jpg', '2019-07-13 01:42:03', '2019-07-13 01:42:03'),
(300, 'amandaearly4.jpg', 78, 87, NULL, 'jpg', '2019-07-13 01:42:09', '2019-07-13 01:42:09'),
(301, 'amandaearly9.jpg', 78, 87, NULL, 'jpg', '2019-07-13 01:42:15', '2019-07-13 01:42:15'),
(302, 'IMG_3064-copy.jpg', 79, 88, NULL, 'jpg', '2019-07-13 01:42:22', '2019-07-13 01:42:22'),
(303, 'IMG_3190.jpg', 79, 88, NULL, 'jpg', '2019-07-13 01:42:28', '2019-07-13 01:42:28'),
(304, 'IMG_3008.jpg', 79, 88, NULL, 'jpg', '2019-07-13 01:42:33', '2019-07-13 01:42:33'),
(305, 'IMG_3034.jpg', 79, 88, NULL, 'jpg', '2019-07-13 01:42:39', '2019-07-13 01:42:39'),
(306, 'IMG_3097.jpg', 79, 88, NULL, 'jpg', '2019-07-13 01:42:46', '2019-07-13 01:42:46'),
(307, 'IMG_3127.jpg', 79, 88, NULL, 'jpg', '2019-07-13 01:42:52', '2019-07-13 01:42:52'),
(308, 'IMG_3171.jpg', 79, 88, NULL, 'jpg', '2019-07-13 01:43:04', '2019-07-13 01:43:04'),
(309, 'Amanda-Jones-Resume.docx', 79, 88, NULL, 'docx', '2019-07-13 01:43:05', '2019-07-13 01:43:05'),
(310, '5G7A9039r.jpg', 80, 89, NULL, 'jpg', '2019-07-13 01:43:22', '2019-07-13 01:43:22'),
(311, '5G7A9085r.jpg', 80, 89, NULL, 'jpg', '2019-07-13 01:43:28', '2019-07-13 01:43:28'),
(312, 'Amar_first-selects_NickThiessen-28-of-319.jpg', 81, 91, NULL, 'jpg', '2019-07-13 01:43:33', '2019-07-13 01:43:33'),
(313, 'Amar_first-selects_NickThiessen-233-of-319.jpg', 81, 91, NULL, 'jpg', '2019-07-13 01:43:35', '2019-07-13 01:43:35'),
(314, 'Amar_first-selects_NickThiessen-128-of-319.jpg', 81, 91, NULL, 'jpg', '2019-07-13 01:43:40', '2019-07-13 01:43:40'),
(315, 'Amar_first-selects_NickThiessen-148-of-319.jpg', 81, 91, NULL, 'jpg', '2019-07-13 01:43:43', '2019-07-13 01:43:43'),
(316, 'Amar_first-selects_NickThiessen-164-of-319.jpg', 81, 91, NULL, 'jpg', '2019-07-13 01:43:47', '2019-07-13 01:43:47'),
(317, 'Amar_first-selects_NickThiessen-69-of-319.jpg', 81, 91, NULL, 'jpg', '2019-07-13 01:43:50', '2019-07-13 01:43:50'),
(318, 'unnamed-1.png', 82, 92, NULL, 'png', '2019-07-13 01:44:05', '2019-07-13 01:44:05'),
(319, 'unnamed-2.png', 82, 92, NULL, 'png', '2019-07-13 01:44:12', '2019-07-13 01:44:12'),
(320, 'sb-Full-onset.jpg', 83, 93, NULL, 'jpg', '2019-07-13 01:44:27', '2019-07-13 01:44:27'),
(321, '2018-09-28-Artist-13-p.jpg', 83, 93, NULL, 'jpg', '2019-07-13 01:44:38', '2019-07-13 01:44:38'),
(322, 'Amber.jpg', 83, 93, NULL, 'jpg', '2019-07-13 01:44:43', '2019-07-13 01:44:43'),
(323, 'Pet-Rabbit.jpg', 83, 93, NULL, 'jpg', '2019-07-13 01:44:53', '2019-07-13 01:44:53'),
(324, 'sb-Amber-full.jpg', 83, 93, NULL, 'jpg', '2019-07-13 01:44:59', '2019-07-13 01:44:59'),
(325, 'Amber-Petersen-Acting.doc', 83, 93, NULL, 'doc', '2019-07-13 01:45:01', '2019-07-13 01:45:01'),
(326, 'FB_IMG_1551814149327.jpg', 84, 94, NULL, 'jpg', '2019-07-13 01:45:10', '2019-07-13 01:45:10'),
(327, 'FB_IMG_1551814425525.jpg', 84, 94, NULL, 'jpg', '2019-07-13 01:45:13', '2019-07-13 01:45:13'),
(328, '20292749_10158943310915005_4881613933443139309_n.jpg', 85, 95, NULL, 'jpg', '2019-07-13 01:45:17', '2019-07-13 01:45:17'),
(329, '23722480_10159590047630537_7176947848253703756_n.jpg', 85, 95, NULL, 'jpg', '2019-07-13 01:45:19', '2019-07-13 01:45:19'),
(330, '50006.jpg', 85, 95, NULL, 'jpg', '2019-07-13 01:45:20', '2019-07-13 01:45:20'),
(331, '20245433_10158943431265005_3523232464355957471_n.jpg', 85, 95, NULL, 'jpg', '2019-07-13 01:45:22', '2019-07-13 01:45:22'),
(332, 'Full-Body.png', 86, 96, NULL, 'png', '2019-07-13 01:46:04', '2019-07-13 01:46:04'),
(333, 'Commercial-Headshot-Ambrose-Gardener.jpg', 86, 96, NULL, 'jpg', '2019-07-13 01:46:36', '2019-07-13 01:46:36'),
(334, 'TV-Film-Headshot-Ambrose-Gardener.jpg', 86, 96, NULL, 'jpg', '2019-07-13 01:47:15', '2019-07-13 01:47:15'),
(335, 'Ambrose-Gardener-Resume.pdf', 86, 96, NULL, 'pdf', '2019-07-13 01:47:17', '2019-07-13 01:47:17'),
(336, 'Amelia-Croft-05202019-FullbodyPrimCOMP.jpg', 87, 97, NULL, 'jpg', '2019-07-13 01:47:32', '2019-07-13 01:47:32'),
(337, 'AmeliaCroft05202019-Prim-WaistupCOMP.jpg', 87, 97, NULL, 'jpg', '2019-07-13 01:47:39', '2019-07-13 01:47:39'),
(338, 'AmeliaCroft05202019AddPicture1COMP.jpg', 87, 97, NULL, 'jpg', '2019-07-13 01:47:54', '2019-07-13 01:47:54'),
(339, 'AmeliaCroft05202019AddPicture2COMP.jpg', 87, 97, NULL, 'jpg', '2019-07-13 01:48:10', '2019-07-13 01:48:10'),
(340, 'AmeliaCroft05202019Addpicture3COMP.jpg', 87, 97, NULL, 'jpg', '2019-07-13 01:48:19', '2019-07-13 01:48:19'),
(341, 'Amelia-Croft-Resume-KC-MAY-2019.docx', 87, 97, NULL, 'docx', '2019-07-13 01:48:20', '2019-07-13 01:48:20'),
(342, 'IMG_1403.jpg', 88, 98, NULL, 'jpg', '2019-07-13 01:48:41', '2019-07-13 01:48:41'),
(343, 'IMG_1402.jpg', 88, 98, NULL, 'jpg', '2019-07-13 01:48:50', '2019-07-13 01:48:50'),
(344, 'IMG_0696.jpg', 88, 98, NULL, 'jpg', '2019-07-13 01:48:51', '2019-07-13 01:48:51'),
(345, 'IMG_0149.jpg', 89, 99, NULL, 'jpg', '2019-07-13 01:48:55', '2019-07-13 01:48:55'),
(346, 'IMG_0150.jpg', 89, 99, NULL, 'jpg', '2019-07-13 01:48:56', '2019-07-13 01:48:56'),
(347, 'Acting-Resume.pdf', 89, 99, NULL, 'pdf', '2019-07-13 01:48:58', '2019-07-13 01:48:58'),
(348, 'IMG-20190221-WA0017.jpg', 90, 100, NULL, 'jpg', '2019-07-13 01:49:04', '2019-07-13 01:49:04'),
(349, 'IMG_7241.jpg', 90, 100, NULL, 'jpg', '2019-07-13 01:49:13', '2019-07-13 01:49:13'),
(350, 'IMG-20190324-WA0082.jpg', 90, 100, NULL, 'jpg', '2019-07-13 01:49:19', '2019-07-13 01:49:19'),
(351, 'Anderson.pdf', 90, 100, NULL, 'pdf', '2019-07-13 01:49:22', '2019-07-13 01:49:22'),
(352, 'IMG_6192.jpg', 91, 101, NULL, 'jpg', '2019-07-13 01:49:31', '2019-07-13 01:49:31'),
(353, 'IMG_4460.jpg', 91, 101, NULL, 'jpg', '2019-07-13 01:49:36', '2019-07-13 01:49:36'),
(354, '20180628_133951.jpg', 92, 102, NULL, 'jpg', '2019-07-13 01:49:54', '2019-07-13 01:49:54'),
(355, 'IMG_20180811_1640402.jpg', 92, 102, NULL, 'jpg', '2019-07-13 01:50:01', '2019-07-13 01:50:01'),
(356, 'IMG_2261.jpg', 93, 103, NULL, 'jpg', '2019-07-13 01:50:08', '2019-07-13 01:50:08'),
(357, 'IMG_2053.jpg', 93, 103, NULL, 'jpg', '2019-07-13 01:50:16', '2019-07-13 01:50:16'),
(358, '01b958b836d55db6232ad1b9ef13d999debc78fb7a.jpg', 93, 103, NULL, 'jpg', '2019-07-13 01:50:19', '2019-07-13 01:50:19'),
(359, 'IMG_2263.jpg', 93, 103, NULL, 'jpg', '2019-07-13 01:50:22', '2019-07-13 01:50:22'),
(360, 'IMG_2264.jpg', 93, 103, NULL, 'jpg', '2019-07-13 01:50:25', '2019-07-13 01:50:25'),
(361, 'Andrea-Cummings-Restaurent-Resume-2018.pdf', 93, 103, NULL, 'pdf', '2019-07-13 01:50:29', '2019-07-13 01:50:29'),
(362, '583BE235-2D1B-4D64-9968-DFAECCBC0563.jpeg', 94, 104, NULL, 'jpeg', '2019-07-13 01:50:34', '2019-07-13 01:50:34'),
(363, '725D49F8-821B-4D7F-BE70-997603F9728A.jpeg', 94, 104, NULL, 'jpeg', '2019-07-13 01:50:37', '2019-07-13 01:50:37'),
(364, '0E61137C-CAC0-4028-ABF5-E2D8AC77EB8B.jpeg', 95, 105, NULL, 'jpeg', '2019-07-13 01:50:43', '2019-07-13 01:50:43'),
(365, 'EE386CED-78F5-4D29-8D52-B1F7E9040036.jpeg', 95, 105, NULL, 'jpeg', '2019-07-13 01:50:46', '2019-07-13 01:50:46'),
(366, 'IMG_1893.jpg', 96, 106, NULL, 'jpg', '2019-07-13 01:50:52', '2019-07-13 01:50:52'),
(367, 'andrea.jpg', 96, 106, NULL, 'jpg', '2019-07-13 01:50:54', '2019-07-13 01:50:54'),
(368, 'Resume-1-2-1.pdf', 96, 106, NULL, 'pdf', '2019-07-13 01:50:59', '2019-07-13 01:50:59'),
(369, '94C9D3D1-BA52-4ABF-8570-846397E49D90.jpeg', 97, 107, NULL, 'jpeg', '2019-07-13 01:51:14', '2019-07-13 01:51:14'),
(370, 'D2272BDB-7357-4828-B73E-26D83B21ED89.jpeg', 97, 107, NULL, 'jpeg', '2019-07-13 01:51:22', '2019-07-13 01:51:22'),
(371, '46649A2D-E53E-4686-AE5F-762C9D31476B.jpeg', 97, 107, NULL, 'jpeg', '2019-07-13 01:51:27', '2019-07-13 01:51:27'),
(372, 'DF0F94C5-6977-41DB-AA5E-DDFE6C6FA09A.jpeg', 97, 107, NULL, 'jpeg', '2019-07-13 01:51:30', '2019-07-13 01:51:30'),
(373, '94C9D3D1-BA52-4ABF-8570-846397E49D90-1.jpeg', 98, 108, NULL, 'jpeg', '2019-07-13 01:51:44', '2019-07-13 01:51:44'),
(374, 'D2272BDB-7357-4828-B73E-26D83B21ED89-1.jpeg', 98, 108, NULL, 'jpeg', '2019-07-13 01:51:57', '2019-07-13 01:51:57'),
(375, '46649A2D-E53E-4686-AE5F-762C9D31476B-1.jpeg', 98, 108, NULL, 'jpeg', '2019-07-13 01:52:07', '2019-07-13 01:52:07'),
(376, 'DF0F94C5-6977-41DB-AA5E-DDFE6C6FA09A-1.jpeg', 98, 108, NULL, 'jpeg', '2019-07-13 01:52:13', '2019-07-13 01:52:13'),
(377, '90CA8A30-7506-45A3-82C0-E8CFE061C0DF.jpeg', 98, 108, NULL, 'jpeg', '2019-07-13 01:52:18', '2019-07-13 01:52:18'),
(378, '94C9D3D1-BA52-4ABF-8570-846397E49D90-2.jpeg', 99, 109, NULL, 'jpeg', '2019-07-13 01:52:32', '2019-07-13 01:52:32'),
(379, 'D2272BDB-7357-4828-B73E-26D83B21ED89-2.jpeg', 99, 109, NULL, 'jpeg', '2019-07-13 01:52:49', '2019-07-13 01:52:49'),
(380, '46649A2D-E53E-4686-AE5F-762C9D31476B-2.jpeg', 99, 109, NULL, 'jpeg', '2019-07-13 01:52:56', '2019-07-13 01:52:56'),
(381, '94C9D3D1-BA52-4ABF-8570-846397E49D90-3.jpeg', 100, 110, NULL, 'jpeg', '2019-07-13 01:53:10', '2019-07-13 01:53:10'),
(382, 'D2272BDB-7357-4828-B73E-26D83B21ED89-3.jpeg', 100, 110, NULL, 'jpeg', '2019-07-13 01:53:17', '2019-07-13 01:53:17'),
(383, '46649A2D-E53E-4686-AE5F-762C9D31476B-3.jpeg', 100, 110, NULL, 'jpeg', '2019-07-13 01:53:28', '2019-07-13 01:53:28'),
(384, 'DF0F94C5-6977-41DB-AA5E-DDFE6C6FA09A-2.jpeg', 100, 110, NULL, 'jpeg', '2019-07-13 01:53:32', '2019-07-13 01:53:32'),
(385, '94C9D3D1-BA52-4ABF-8570-846397E49D90-4.jpeg', 101, 111, NULL, 'jpeg', '2019-07-13 01:53:46', '2019-07-13 01:53:46'),
(386, 'D2272BDB-7357-4828-B73E-26D83B21ED89-4.jpeg', 101, 111, NULL, 'jpeg', '2019-07-13 01:53:53', '2019-07-13 01:53:53'),
(387, '46649A2D-E53E-4686-AE5F-762C9D31476B-4.jpeg', 101, 111, NULL, 'jpeg', '2019-07-13 01:53:58', '2019-07-13 01:53:58'),
(388, 'DF0F94C5-6977-41DB-AA5E-DDFE6C6FA09A-3.jpeg', 101, 111, NULL, 'jpeg', '2019-07-13 01:54:01', '2019-07-13 01:54:01'),
(389, '16F7252C-A3BD-459A-A207-645257A9F719.jpeg', 102, 112, NULL, 'jpeg', '2019-07-13 01:54:14', '2019-07-13 01:54:14'),
(390, 'D69C79B5-9ED6-4C66-9057-F588FEDF10EC.png', 102, 112, NULL, 'png', '2019-07-13 01:54:23', '2019-07-13 01:54:23'),
(391, 'C21118C3-B567-43BA-A6CA-006C984428A3.png', 102, 112, NULL, 'png', '2019-07-13 01:54:39', '2019-07-13 01:54:39'),
(392, '2FFB380F-05E5-4609-B23A-534AE5BA765E.png', 102, 112, NULL, 'png', '2019-07-13 01:54:47', '2019-07-13 01:54:47'),
(393, '0339Andrew-Bourque-PRINT.jpg', 103, 113, NULL, 'jpg', '2019-07-13 01:55:10', '2019-07-13 01:55:10'),
(394, '0271Andrew-Bourque-PRINT.jpg', 103, 113, NULL, 'jpg', '2019-07-13 01:55:23', '2019-07-13 01:55:23'),
(395, '0141Andrew-Bourque-PRINT.jpg', 103, 113, NULL, 'jpg', '2019-07-13 01:55:29', '2019-07-13 01:55:29'),
(396, 'Andrew-P-Bourques-Resume.pdf', 103, 113, NULL, 'pdf', '2019-07-13 01:55:31', '2019-07-13 01:55:31'),
(397, 'body-shot.png', 104, 114, NULL, 'png', '2019-07-13 01:55:53', '2019-07-13 01:55:53'),
(398, 'body-shot-1.png', 104, 114, NULL, 'png', '2019-07-13 01:56:03', '2019-07-13 01:56:03'),
(399, '10857134_10153682099769746_6484044986935339645_o.jpg', 105, 115, NULL, 'jpg', '2019-07-13 01:56:27', '2019-07-13 01:56:27'),
(400, '14712709_1091554317580806_1569309498236169988_o.jpg', 105, 115, NULL, 'jpg', '2019-07-13 01:56:29', '2019-07-13 01:56:29'),
(401, '12248167_10154319620729746_1625348737746975736_o.jpg', 105, 115, NULL, 'jpg', '2019-07-13 01:56:32', '2019-07-13 01:56:32'),
(402, '25531852_10156920273329746_5175478650252203445_o.jpg', 105, 115, NULL, 'jpg', '2019-07-13 01:56:34', '2019-07-13 01:56:34'),
(403, '10688187_10153222868684746_5457453987965934920_o.jpg', 105, 115, NULL, 'jpg', '2019-07-13 01:56:37', '2019-07-13 01:56:37'),
(404, '17192044_1233520840050819_6923461066360647660_o.jpg', 105, 115, NULL, 'jpg', '2019-07-13 01:56:41', '2019-07-13 01:56:41'),
(405, 'IMG_0092.jpg', 105, 115, NULL, 'jpg', '2019-07-13 01:56:50', '2019-07-13 01:56:50'),
(406, '15178052_1127312654004972_8578778991063196725_n.jpg', 105, 115, NULL, 'jpg', '2019-07-13 01:56:57', '2019-07-13 01:56:57'),
(407, 'IMG_1333.jpg', 105, 115, NULL, 'jpg', '2019-07-13 01:57:05', '2019-07-13 01:57:05'),
(408, 'Resume.pdf', 105, 115, NULL, 'pdf', '2019-07-13 01:57:09', '2019-07-13 01:57:09'),
(409, 'andrew.jpg', 106, 116, NULL, 'jpg', '2019-07-13 01:57:19', '2019-07-13 01:57:19'),
(410, 'IMG_0059.jpg', 106, 116, NULL, 'jpg', '2019-07-13 01:57:21', '2019-07-13 01:57:21'),
(411, 'EA5854F3-69B5-46C6-999E-EF7E44CD628F.jpg', 106, 116, NULL, 'jpg', '2019-07-13 01:57:29', '2019-07-13 01:57:29'),
(412, 'andrew-1.jpg', 107, 117, NULL, 'jpg', '2019-07-13 01:57:45', '2019-07-13 01:57:45'),
(413, 'IMG_0059-1.jpg', 107, 117, NULL, 'jpg', '2019-07-13 01:57:47', '2019-07-13 01:57:47'),
(414, 'EA5854F3-69B5-46C6-999E-EF7E44CD628F-1.jpg', 107, 117, NULL, 'jpg', '2019-07-13 01:57:52', '2019-07-13 01:57:52'),
(415, 'Andrew-Jorgensen2.jpg', 108, 118, NULL, 'jpg', '2019-07-13 01:58:05', '2019-07-13 01:58:05'),
(416, 'Andrew-Jorgensen3.jpg', 108, 118, NULL, 'jpg', '2019-07-13 01:58:10', '2019-07-13 01:58:10'),
(417, 'Andrew-Jorgensen1.jpg', 108, 118, NULL, 'jpg', '2019-07-13 01:58:15', '2019-07-13 01:58:15'),
(418, '481AB2A6-042B-4653-A5E3-FF7416155589.jpeg', 109, 119, NULL, 'jpeg', '2019-07-13 01:58:19', '2019-07-13 01:58:19'),
(419, '9AF08F54-4FCC-4495-B851-1527766B87EA.jpeg', 109, 119, NULL, 'jpeg', '2019-07-13 01:58:21', '2019-07-13 01:58:21'),
(420, 'F150742A-8D84-4A0E-BFC7-A613B488ED91.jpeg', 109, 119, NULL, 'jpeg', '2019-07-13 01:58:23', '2019-07-13 01:58:23'),
(421, '664D46F2-A748-47FC-8C42-2F7875749DA6.jpeg', 109, 119, NULL, 'jpeg', '2019-07-13 01:58:25', '2019-07-13 01:58:25'),
(422, '8852381A-A1C0-4824-A89F-7EED21AB35A1.jpeg', 109, 119, NULL, 'jpeg', '2019-07-13 01:58:27', '2019-07-13 01:58:27'),
(423, 'D808512C-4082-48B8-AF92-3869B0007093.jpeg', 110, 120, NULL, 'jpeg', '2019-07-13 01:58:31', '2019-07-13 01:58:31'),
(424, 'D49BA6FA-ED36-4348-A49F-76129056156A.jpeg', 110, 120, NULL, 'jpeg', '2019-07-13 01:58:33', '2019-07-13 01:58:33'),
(425, 'A7FF0410-07DA-4CCB-AC2E-D9B12FD8325A.jpeg', 110, 120, NULL, 'jpeg', '2019-07-13 01:58:35', '2019-07-13 01:58:35'),
(426, 'IMG_0068-1.jpg', 111, 121, NULL, 'jpg', '2019-07-13 01:58:40', '2019-07-13 01:58:40'),
(427, 'IMG_0077-1-1.jpg', 111, 121, NULL, 'jpg', '2019-07-13 01:58:43', '2019-07-13 01:58:43'),
(428, 'IMG_2824.jpg', 111, 121, NULL, 'jpg', '2019-07-13 01:58:54', '2019-07-13 01:58:54'),
(429, 'IMG_20171113_151334_015.jpg', 111, 121, NULL, 'jpg', '2019-07-13 01:59:01', '2019-07-13 01:59:01'),
(430, 'FBB4AF5D-227C-4AA7-AE58-752B4D535C59.jpeg', 112, 122, NULL, 'jpeg', '2019-07-13 01:59:16', '2019-07-13 01:59:16'),
(431, 'B8EA019C-4F45-40BF-ADFD-F59CBBA39485.jpeg', 112, 122, NULL, 'jpeg', '2019-07-13 01:59:22', '2019-07-13 01:59:22'),
(432, 'A27E5E65-D24C-4C37-9279-A251D5FA6161.jpeg', 112, 122, NULL, 'jpeg', '2019-07-13 01:59:25', '2019-07-13 01:59:25'),
(433, 'IMG_0308.jpg', 113, 123, NULL, 'jpg', '2019-07-13 01:59:35', '2019-07-13 01:59:35'),
(434, 'IMG_0291.jpg', 113, 123, NULL, 'jpg', '2019-07-13 01:59:41', '2019-07-13 01:59:41'),
(435, 'IMG_0310.jpg', 113, 123, NULL, 'jpg', '2019-07-13 01:59:49', '2019-07-13 01:59:49'),
(436, 'IMG_0311.jpg', 113, 123, NULL, 'jpg', '2019-07-13 01:59:55', '2019-07-13 01:59:55'),
(437, 'Angela-Ibbott-Resume-2018.docx', 113, 123, NULL, 'docx', '2019-07-13 01:59:56', '2019-07-13 01:59:56'),
(438, 'Angela-Knight-Rodeo-Drive.jpeg', 114, 124, NULL, 'jpeg', '2019-07-13 02:00:04', '2019-07-13 02:00:04'),
(439, 'angela-r.-knight-as-a-fbi-agent.jpg', 114, 124, NULL, 'jpg', '2019-07-13 02:00:05', '2019-07-13 02:00:05'),
(440, 'Angela-on-sub-Hawaii.jpeg', 114, 124, NULL, 'jpeg', '2019-07-13 02:00:18', '2019-07-13 02:00:18'),
(441, 'angela-knights-profile-2019.pdf', 114, 124, NULL, 'pdf', '2019-07-13 02:00:28', '2019-07-13 02:00:28'),
(442, 'IMG_1836.jpg', 115, 125, NULL, 'jpg', '2019-07-13 02:01:03', '2019-07-13 02:01:03'),
(443, 'IMG_1851.jpg', 115, 125, NULL, 'jpg', '2019-07-13 02:01:19', '2019-07-13 02:01:19'),
(444, 'IMG_1859.jpg', 115, 125, NULL, 'jpg', '2019-07-13 02:01:31', '2019-07-13 02:01:31'),
(445, 'IMG_1836-1.jpg', 116, 126, NULL, 'jpg', '2019-07-13 02:02:15', '2019-07-13 02:02:15'),
(446, 'IMG_1851-1.jpg', 116, 126, NULL, 'jpg', '2019-07-13 02:02:37', '2019-07-13 02:02:37'),
(447, 'IMG_1859-1.jpg', 116, 126, NULL, 'jpg', '2019-07-13 02:02:54', '2019-07-13 02:02:54'),
(448, 'IMG_1836-2.jpg', 117, 127, NULL, 'jpg', '2019-07-13 02:03:23', '2019-07-13 02:03:23'),
(449, 'IMG_1851-2.jpg', 117, 127, NULL, 'jpg', '2019-07-13 02:03:44', '2019-07-13 02:03:44'),
(450, 'IMG_1859-2.jpg', 117, 127, NULL, 'jpg', '2019-07-13 02:04:04', '2019-07-13 02:04:04'),
(451, 'IMG_1836-3.jpg', 118, 128, NULL, 'jpg', '2019-07-13 02:04:27', '2019-07-13 02:04:27'),
(452, 'IMG_1851-3.jpg', 118, 128, NULL, 'jpg', '2019-07-13 02:04:36', '2019-07-13 02:04:36'),
(453, 'IMG_1859-3.jpg', 118, 128, NULL, 'jpg', '2019-07-13 02:05:22', '2019-07-13 02:05:22'),
(454, 'Screen-Shot-2019-06-17.png', 119, 129, NULL, 'png', '2019-07-13 02:05:41', '2019-07-13 02:05:41'),
(455, 'Screen-Shot-2019-06-20-at-10.48.15-AM.png', 119, 129, NULL, 'png', '2019-07-13 02:05:46', '2019-07-13 02:05:46'),
(456, '39E63912-2C7B-4FB9-96CE-FAA8A55C06B3.jpeg', 120, 130, NULL, 'jpeg', '2019-07-13 02:05:52', '2019-07-13 02:05:52'),
(457, 'A148D9E4-F4BE-4B29-A4FF-812BBBADE403.jpeg', 120, 130, NULL, 'jpeg', '2019-07-13 02:05:54', '2019-07-13 02:05:54'),
(458, 'Angie-headshot-3.jpg', 121, 131, NULL, 'jpg', '2019-07-13 02:05:58', '2019-07-13 02:05:58'),
(459, 'Angie-head-shot-1-.jpg', 121, 131, NULL, 'jpg', '2019-07-13 02:06:38', '2019-07-13 02:06:38'),
(460, 'Acting-Res.doc', 121, 131, NULL, 'doc', '2019-07-13 02:06:39', '2019-07-13 02:06:39'),
(461, 'Angie-head-shot-1-.jpg', 122, 132, NULL, 'jpg', '2019-07-13 02:07:05', '2019-07-13 02:07:05'),
(462, '2015-01-14-21.44.03.png', 122, 132, NULL, 'png', '2019-07-13 02:07:18', '2019-07-13 02:07:18'),
(463, '2015-01-29-21.26.04-1.jpg', 122, 132, NULL, 'jpg', '2019-07-13 02:07:21', '2019-07-13 02:07:21'),
(464, 'headshot-2.jpg', 122, 132, NULL, 'jpg', '2019-07-13 02:07:24', '2019-07-13 02:07:24'),
(465, 'angie-book-photo.jpg', 122, 132, NULL, 'jpg', '2019-07-13 02:07:25', '2019-07-13 02:07:25'),
(466, '2014-12-04-16.10.30.jpg', 122, 132, NULL, 'jpg', '2019-07-13 02:07:27', '2019-07-13 02:07:27'),
(467, '2015-03-06-21.52.37.png', 122, 132, NULL, 'png', '2019-07-13 02:07:30', '2019-07-13 02:07:30'),
(468, 'angie-headshot.jpg', 122, 132, NULL, 'jpg', '2019-07-13 02:07:31', '2019-07-13 02:07:31'),
(469, '2015-03-13-12.58.07-1.jpg', 122, 132, NULL, 'jpg', '2019-07-13 02:07:33', '2019-07-13 02:07:33'),
(470, 'Angie-Wangler-lucas-talent-res.pdf', 122, 132, NULL, 'pdf', '2019-07-13 02:07:35', '2019-07-13 02:07:35'),
(471, '20190317_224125.jpg', 123, 133, NULL, 'jpg', '2019-07-13 02:07:49', '2019-07-13 02:07:49'),
(472, '20190317_224150.jpg', 123, 133, NULL, 'jpg', '2019-07-13 02:07:56', '2019-07-13 02:07:56'),
(473, 'img1524311265655-2.jpg', 124, 134, NULL, 'jpg', '2019-07-13 02:08:02', '2019-07-13 02:08:02'),
(474, 'ann-camo-overalls.jpg', 124, 134, NULL, 'jpg', '2019-07-13 02:08:05', '2019-07-13 02:08:05'),
(475, 'ann3.jpg', 124, 134, NULL, 'jpg', '2019-07-13 02:08:07', '2019-07-13 02:08:07'),
(476, 'img1523302993855.jpg', 124, 134, NULL, 'jpg', '2019-07-13 02:08:11', '2019-07-13 02:08:11'),
(477, 'ann13.jpg', 124, 134, NULL, 'jpg', '2019-07-13 02:08:12', '2019-07-13 02:08:12');
INSERT INTO `media_portfolios` (`id`, `media_url`, `portfolio_id`, `performers_id`, `media_title`, `media_type`, `created_at`, `updated_at`) VALUES
(478, 'img1518342581654.jpg', 124, 134, NULL, 'jpg', '2019-07-13 02:08:16', '2019-07-13 02:08:16'),
(479, 'ann10.jpg', 124, 134, NULL, 'jpg', '2019-07-13 02:08:18', '2019-07-13 02:08:18'),
(480, 'IMG_20171207_211319006.jpg', 124, 134, NULL, 'jpg', '2019-07-13 02:08:25', '2019-07-13 02:08:25'),
(481, '4E686BAF-600C-41A1-BD4D-56A4DFCE5506.jpeg', 125, 135, NULL, 'jpeg', '2019-07-13 02:08:35', '2019-07-13 02:08:35'),
(482, '5D2602D2-9512-43E3-9F7B-3B285C91874A.jpeg', 125, 135, NULL, 'jpeg', '2019-07-13 02:08:43', '2019-07-13 02:08:43'),
(483, 'BD683DD5-7DF2-4E1F-A60D-2F2EB646A59F.jpeg', 126, 136, NULL, 'jpeg', '2019-07-13 02:08:55', '2019-07-13 02:08:55'),
(484, '80ADD652-6391-4167-A6EE-2F955A6587F3.jpeg', 126, 136, NULL, 'jpeg', '2019-07-13 02:09:05', '2019-07-13 02:09:05'),
(485, 'FDF74D14-CA68-420E-80A5-179C142DDE52.jpeg', 126, 136, NULL, 'jpeg', '2019-07-13 02:09:11', '2019-07-13 02:09:11'),
(486, 'FB_IMG_1551755695135.jpg', 127, 137, NULL, 'jpg', '2019-07-13 02:09:28', '2019-07-13 02:09:28'),
(487, 'FB_IMG_1551755843721.jpg', 127, 137, NULL, 'jpg', '2019-07-13 02:09:30', '2019-07-13 02:09:30'),
(488, 'Anna-Full-body.jpg', 128, 138, NULL, 'jpg', '2019-07-13 02:09:49', '2019-07-13 02:09:49'),
(489, 'Anna-Half-Body.jpg', 128, 138, NULL, 'jpg', '2019-07-13 02:10:00', '2019-07-13 02:10:00'),
(490, 'Anna-Full-Body-2.jpg', 128, 138, NULL, 'jpg', '2019-07-13 02:10:04', '2019-07-13 02:10:04'),
(491, 'Anna-Outdoor-Climbing-2.jpg', 128, 138, NULL, 'jpg', '2019-07-13 02:10:10', '2019-07-13 02:10:10'),
(492, 'Anna-Outdoor-Climbing.jpg', 128, 138, NULL, 'jpg', '2019-07-13 02:10:17', '2019-07-13 02:10:17'),
(493, '65485043_2359217527734561_1512577775301558272_n.jpg', 129, 139, NULL, 'jpg', '2019-07-13 02:10:24', '2019-07-13 02:10:24'),
(494, 'waistup.jpg', 129, 139, NULL, 'jpg', '2019-07-13 02:10:27', '2019-07-13 02:10:27'),
(495, 'AnnaTerebkaActingResume.pdf', 129, 139, NULL, 'pdf', '2019-07-13 02:10:31', '2019-07-13 02:10:31'),
(496, '20190322_113636.jpg', 130, 140, NULL, 'jpg', '2019-07-13 02:10:35', '2019-07-13 02:10:35'),
(497, '20190210_162450.jpg', 130, 140, NULL, 'jpg', '2019-07-13 02:10:37', '2019-07-13 02:10:37'),
(498, 'IMG_8190.jpg', 131, 141, NULL, 'jpg', '2019-07-13 02:53:59', '2019-07-13 02:53:59'),
(499, 'IMG_1071.jpg', 131, 141, NULL, 'jpg', '2019-07-13 02:54:05', '2019-07-13 02:54:05'),
(500, 'IMG_9968.jpg', 131, 141, NULL, 'jpg', '2019-07-13 02:54:13', '2019-07-13 02:54:13'),
(501, 'AKadwell-May-2018.pdf', 131, 141, NULL, 'pdf', '2019-07-13 02:54:15', '2019-07-13 02:54:15'),
(502, 'full-body-anne.jpg', 132, 142, NULL, 'jpg', '2019-07-13 02:54:33', '2019-07-13 02:54:33'),
(503, 'waist-up-anne.jpg', 132, 142, NULL, 'jpg', '2019-07-13 02:54:48', '2019-07-13 02:54:48'),
(504, '20170727_141133-1.jpg', 133, 143, NULL, 'jpg', '2019-07-13 02:54:59', '2019-07-13 02:54:59'),
(505, '20171124_202715-1.jpg', 133, 143, NULL, 'jpg', '2019-07-13 02:55:06', '2019-07-13 02:55:06'),
(506, 'IMG_2669.jpg', 134, 144, NULL, 'jpg', '2019-07-13 02:55:21', '2019-07-13 02:55:21'),
(507, 'IMG_2612.jpg', 134, 144, NULL, 'jpg', '2019-07-13 02:55:23', '2019-07-13 02:55:23'),
(508, 'IMG_4322.jpg', 134, 144, NULL, 'jpg', '2019-07-13 02:55:27', '2019-07-13 02:55:27'),
(509, '20180602_154142-11.jpg', 135, 145, NULL, 'jpg', '2019-07-13 02:55:35', '2019-07-13 02:55:35'),
(510, '20180623_153126-1.jpg', 135, 145, NULL, 'jpg', '2019-07-13 02:55:42', '2019-07-13 02:55:42'),
(511, '797D3759-AC00-42E6-A031-DC29CADBC440.jpeg', 136, 146, NULL, 'jpeg', '2019-07-13 02:55:56', '2019-07-13 02:55:56'),
(512, '113FEE54-65BB-4193-B3A9-33E6C7D0B409.jpeg', 136, 146, NULL, 'jpeg', '2019-07-13 02:56:03', '2019-07-13 02:56:03'),
(513, '2EC0CBD3-9ABE-48C2-AC21-B35BED122578.jpeg', 136, 146, NULL, 'jpeg', '2019-07-13 02:56:13', '2019-07-13 02:56:13'),
(514, 'MG_2694.jpg', 137, 147, NULL, 'jpg', '2019-07-13 02:56:24', '2019-07-13 02:56:24'),
(515, 'ann-marie-5.jpg', 137, 147, NULL, 'jpg', '2019-07-13 02:56:34', '2019-07-13 02:56:34'),
(516, 'ann-marie-3.jpg', 137, 147, NULL, 'jpg', '2019-07-13 02:56:37', '2019-07-13 02:56:37'),
(517, 'ann-marie-9.jpg', 137, 147, NULL, 'jpg', '2019-07-13 02:56:43', '2019-07-13 02:56:43'),
(518, 'Ann-Marie-Leonard-Resume.pdf', 137, 147, NULL, 'pdf', '2019-07-13 02:56:48', '2019-07-13 02:56:48'),
(519, 'IMG_2780.jpeg', 138, 148, NULL, 'jpeg', '2019-07-13 02:56:56', '2019-07-13 02:56:56'),
(520, 'Anthony-Demare-Dress-shirt-1.jpg', 139, 150, NULL, 'jpg', '2019-07-13 02:57:05', '2019-07-13 02:57:05'),
(521, 'Headshot-Dramatic-2.jpg', 139, 150, NULL, 'jpg', '2019-07-13 02:57:08', '2019-07-13 02:57:08'),
(522, 'body-shot-2017.jpg', 139, 150, NULL, 'jpg', '2019-07-13 02:57:14', '2019-07-13 02:57:14'),
(523, '991FD19F-CE94-4DA1-AF2E-A6C84913A10D.jpeg', 140, 151, NULL, 'jpeg', '2019-07-13 02:57:27', '2019-07-13 02:57:27'),
(524, '8EDBB7A2-88C2-4895-8905-4A8299FB1FCF.jpeg', 140, 151, NULL, 'jpeg', '2019-07-13 02:57:32', '2019-07-13 02:57:32'),
(525, 'A67C339A-FDD6-4A9B-9DC5-C910ABC34361.jpeg', 140, 151, NULL, 'jpeg', '2019-07-13 02:57:45', '2019-07-13 02:57:45'),
(526, 'E315CE88-4B00-4427-8003-871FB6B7B888.jpeg', 140, 151, NULL, 'jpeg', '2019-07-13 02:57:47', '2019-07-13 02:57:47'),
(527, 'full-body-Antony.jpg', 141, 152, NULL, 'jpg', '2019-07-13 02:57:50', '2019-07-13 02:57:50'),
(528, 'Waist-up.jpeg', 141, 152, NULL, 'jpeg', '2019-07-13 02:57:57', '2019-07-13 02:57:57'),
(529, 'image00049.jpeg', 141, 152, NULL, 'jpeg', '2019-07-13 02:58:03', '2019-07-13 02:58:03'),
(530, 'image00046.jpeg', 141, 152, NULL, 'jpeg', '2019-07-13 02:58:10', '2019-07-13 02:58:10'),
(531, 'image00026.jpeg', 141, 152, NULL, 'jpeg', '2019-07-13 02:58:19', '2019-07-13 02:58:19'),
(532, 'image00014.jpeg', 141, 152, NULL, 'jpeg', '2019-07-13 02:58:27', '2019-07-13 02:58:27'),
(533, 'image00007.jpeg', 141, 152, NULL, 'jpeg', '2019-07-13 02:58:34', '2019-07-13 02:58:34'),
(534, 'image00011.jpeg', 141, 152, NULL, 'jpeg', '2019-07-13 02:58:41', '2019-07-13 02:58:41'),
(535, 'image00037.jpeg', 141, 152, NULL, 'jpeg', '2019-07-13 02:58:48', '2019-07-13 02:58:48'),
(536, 'Aoy-Finals-web-size-6.jpg', 142, 153, NULL, 'jpg', '2019-07-13 02:58:55', '2019-07-13 02:58:55'),
(537, 'Aoy-Finals-web-size-7.jpg', 142, 153, NULL, 'jpg', '2019-07-13 02:58:58', '2019-07-13 02:58:58'),
(538, 'EjmTinvjvS8Dhi8kAKSk5TCVileTuZTF0ajRu9TE1JiZzVR77w1UnjclmRsIaO1NXNvQbgs190.png', 142, 153, NULL, 'png', '2019-07-13 02:59:00', '2019-07-13 02:59:00'),
(539, 'IMG_1196.jpg', 142, 153, NULL, 'jpg', '2019-07-13 02:59:13', '2019-07-13 02:59:13'),
(540, 'IMG_9492.jpg', 143, 154, NULL, 'jpg', '2019-07-13 02:59:25', '2019-07-13 02:59:25'),
(541, 'IMG_9491.jpg', 143, 154, NULL, 'jpg', '2019-07-13 02:59:33', '2019-07-13 02:59:33'),
(542, 'Snapseed.jpg', 143, 154, NULL, 'jpg', '2019-07-13 02:59:43', '2019-07-13 02:59:43'),
(543, '69F50753-8150-4CDA-8BCD-0D73EA798256.jpeg', 143, 154, NULL, 'jpeg', '2019-07-13 02:59:46', '2019-07-13 02:59:46'),
(544, 'FB_IMG_1551231456849.jpg', 144, 155, NULL, 'jpg', '2019-07-13 02:59:54', '2019-07-13 02:59:54'),
(545, 'FB_IMG_1551231382571.jpg', 144, 155, NULL, 'jpg', '2019-07-13 02:59:57', '2019-07-13 02:59:57'),
(546, 'FB_IMG_1551231401829.jpg', 144, 155, NULL, 'jpg', '2019-07-13 03:00:00', '2019-07-13 03:00:00'),
(547, 'LRM_EXPORT_20180521_211008.jpg', 145, 156, NULL, 'jpg', '2019-07-13 03:00:25', '2019-07-13 03:00:25'),
(548, '8162018202814.jpg', 145, 156, NULL, 'jpg', '2019-07-13 03:00:47', '2019-07-13 03:00:47'),
(549, 'LRM_EXPORT_20180521_211008-1.jpg', 146, 157, NULL, 'jpg', '2019-07-13 03:01:22', '2019-07-13 03:01:22'),
(550, '8162018202814-1.jpg', 146, 157, NULL, 'jpg', '2019-07-13 03:01:37', '2019-07-13 03:01:37'),
(551, '2018-07-25-00.08.00.jpg', 147, 158, NULL, 'jpg', '2019-07-13 03:01:43', '2019-07-13 03:01:43'),
(552, 'Aramesh_05.jpg', 147, 158, NULL, 'jpg', '2019-07-13 03:01:57', '2019-07-13 03:01:57'),
(553, '20170221_164432_HDR_resized.jpg', 148, 159, NULL, 'jpg', '2019-07-13 03:02:02', '2019-07-13 03:02:02'),
(554, '20180319_105202.jpg', 148, 159, NULL, 'jpg', '2019-07-13 03:02:06', '2019-07-13 03:02:06'),
(555, 'rsz_img_7704-edit.jpg', 148, 159, NULL, 'jpg', '2019-07-13 03:02:10', '2019-07-13 03:02:10'),
(556, 'IMG_20170111_122736_031_resized.jpg', 148, 159, NULL, 'jpg', '2019-07-13 03:02:12', '2019-07-13 03:02:12'),
(557, 'IMG_20170807_024820_495_resized.jpg', 148, 159, NULL, 'jpg', '2019-07-13 03:02:15', '2019-07-13 03:02:15'),
(558, '20170922_210119_resized.jpg', 148, 159, NULL, 'jpg', '2019-07-13 03:02:22', '2019-07-13 03:02:22'),
(559, 'IMG_20170105_194803_324_resized.jpg', 148, 159, NULL, 'jpg', '2019-07-13 03:02:24', '2019-07-13 03:02:24'),
(560, 'Araz-Yaghoubi-Stand-in-Res.pdf', 148, 159, NULL, 'pdf', '2019-07-13 03:02:26', '2019-07-13 03:02:26'),
(561, 'IMG_3213.png', 149, 161, NULL, 'png', '2019-07-13 03:02:40', '2019-07-13 03:02:40'),
(562, 'IMG_3217.jpg', 149, 161, NULL, 'jpg', '2019-07-13 03:02:44', '2019-07-13 03:02:44'),
(563, 'IMG_3210.png', 149, 161, NULL, 'png', '2019-07-13 03:02:50', '2019-07-13 03:02:50'),
(564, '74D3BDE6-28B6-4D90-BAA8-E9C57C8696ED.jpeg', 150, 162, NULL, 'jpeg', '2019-07-13 03:03:01', '2019-07-13 03:03:01'),
(565, 'C1597E5E-39DB-4C21-897E-1D71BFB59AF0.jpeg', 150, 162, NULL, 'jpeg', '2019-07-13 03:03:03', '2019-07-13 03:03:03'),
(566, 'G0144247.jpg', 151, 163, NULL, 'jpg', '2019-07-13 03:03:29', '2019-07-13 03:03:29'),
(567, 'G0174250.jpg', 151, 163, NULL, 'jpg', '2019-07-13 03:03:38', '2019-07-13 03:03:38'),
(568, 'G0104243.jpg', 151, 163, NULL, 'jpg', '2019-07-13 03:03:45', '2019-07-13 03:03:45'),
(569, 'Aj-Resume-final.pdf', 151, 163, NULL, 'pdf', '2019-07-13 03:03:47', '2019-07-13 03:03:47'),
(570, 'IMG_20190201_225636_resized_20190201_105712668.jpg', 152, 164, NULL, 'jpg', '2019-07-13 03:03:59', '2019-07-13 03:03:59'),
(571, 'IMG_20190201_211744-copy.jpg', 152, 164, NULL, 'jpg', '2019-07-13 03:04:06', '2019-07-13 03:04:06'),
(572, 'IMG_20180910_204133-copy.jpg', 152, 164, NULL, 'jpg', '2019-07-13 03:04:14', '2019-07-13 03:04:14'),
(573, 'IMG_20190201_224744_resized_20190201_104849870.jpg', 152, 164, NULL, 'jpg', '2019-07-13 03:04:17', '2019-07-13 03:04:17'),
(574, 'IMG_20181022_093527.jpg', 152, 164, NULL, 'jpg', '2019-07-13 03:04:23', '2019-07-13 03:04:23'),
(575, 'IMG_20190201_225507_resized_20190201_105611997.jpg', 152, 164, NULL, 'jpg', '2019-07-13 03:04:25', '2019-07-13 03:04:25'),
(576, 'IMG_20190201_225507_resized_20190201_105611997-1.jpg', 152, 164, NULL, 'jpg', '2019-07-13 03:04:28', '2019-07-13 03:04:28'),
(577, 'P1090080.jpg', 153, 165, NULL, 'jpg', '2019-07-13 03:04:37', '2019-07-13 03:04:37'),
(578, 'IMG_7210.jpg', 153, 165, NULL, 'jpg', '2019-07-13 03:04:40', '2019-07-13 03:04:40'),
(579, 'IMG_2069.jpg', 153, 165, NULL, 'jpg', '2019-07-13 03:04:42', '2019-07-13 03:04:42'),
(580, 'IMG_2478.jpg', 153, 165, NULL, 'jpg', '2019-07-13 03:04:46', '2019-07-13 03:04:46'),
(581, '6860CCB7-C763-4052-AFA1-A676E43975B6.jpeg', 154, 166, NULL, 'jpeg', '2019-07-13 03:05:07', '2019-07-13 03:05:07'),
(582, '38FA98DA-F9EC-4C60-819D-F814C48CC768.jpeg', 154, 166, NULL, 'jpeg', '2019-07-13 03:05:18', '2019-07-13 03:05:18'),
(583, 'IMG_8202.jpg', 155, 167, NULL, 'jpg', '2019-07-13 03:05:34', '2019-07-13 03:05:34'),
(584, 'IMG_7744.jpg', 155, 167, NULL, 'jpg', '2019-07-13 03:05:37', '2019-07-13 03:05:37'),
(585, 'IMG_7222.jpg', 155, 167, NULL, 'jpg', '2019-07-13 03:05:39', '2019-07-13 03:05:39'),
(586, 'IMG_5580.jpg', 155, 167, NULL, 'jpg', '2019-07-13 03:05:45', '2019-07-13 03:05:45'),
(587, 'IMG_7283.jpg', 155, 167, NULL, 'jpg', '2019-07-13 03:05:49', '2019-07-13 03:05:49'),
(588, 'IMG_5009.jpg', 155, 167, NULL, 'jpg', '2019-07-13 03:05:52', '2019-07-13 03:05:52'),
(589, 'IMG_2763.jpg', 155, 167, NULL, 'jpg', '2019-07-13 03:05:53', '2019-07-13 03:05:53'),
(590, 'IMG_4408.jpg', 155, 167, NULL, 'jpg', '2019-07-13 03:05:56', '2019-07-13 03:05:56'),
(591, 'IMG_2762.jpg', 155, 167, NULL, 'jpg', '2019-07-13 03:05:57', '2019-07-13 03:05:57'),
(592, 'art_peter2018_winter1.jpg', 156, 168, NULL, 'jpg', '2019-07-13 03:06:13', '2019-07-13 03:06:13'),
(593, 'save-new-7.jpg', 156, 168, NULL, 'jpg', '2019-07-13 03:06:28', '2019-07-13 03:06:28'),
(594, 'art_peter2018_2.jpg', 156, 168, NULL, 'jpg', '2019-07-13 03:06:52', '2019-07-13 03:06:52'),
(595, 'art_peter2018_3.jpg', 156, 168, NULL, 'jpg', '2019-07-13 03:07:06', '2019-07-13 03:07:06'),
(596, '595-4.jpg', 156, 168, NULL, 'jpg', '2019-07-13 03:07:08', '2019-07-13 03:07:08'),
(597, '595-5.jpg', 156, 168, NULL, 'jpg', '2019-07-13 03:07:09', '2019-07-13 03:07:09'),
(598, '1377014_10202128575277121_1466341437_n.jpg', 156, 168, NULL, 'jpg', '2019-07-13 03:07:11', '2019-07-13 03:07:11'),
(599, '33312108_211752486287693_3704167237033132032_n.jpg', 156, 168, NULL, 'jpg', '2019-07-13 03:07:14', '2019-07-13 03:07:14'),
(600, '595-10.jpg', 156, 168, NULL, 'jpg', '2019-07-13 03:07:15', '2019-07-13 03:07:15'),
(601, 'Lighthouse.jpg', 157, 169, NULL, 'jpg', '2019-07-13 03:07:23', '2019-07-13 03:07:23'),
(602, 'Penguins.jpg', 157, 169, NULL, 'jpg', '2019-07-13 03:07:27', '2019-07-13 03:07:27'),
(603, 'Tulips.jpg', 157, 169, NULL, 'jpg', '2019-07-13 03:07:30', '2019-07-13 03:07:30'),
(604, 'Penguins-1.jpg', 157, 169, NULL, 'jpg', '2019-07-13 03:07:38', '2019-07-13 03:07:38'),
(605, '20190302-Sequence077.jpg', 158, 171, NULL, 'jpg', '2019-07-13 03:08:04', '2019-07-13 03:08:04'),
(606, '20190302-Sequence106.jpg', 158, 171, NULL, 'jpg', '2019-07-13 03:08:23', '2019-07-13 03:08:23'),
(607, '20190302-Sequence033.jpg', 158, 171, NULL, 'jpg', '2019-07-13 03:08:45', '2019-07-13 03:08:45'),
(608, '20190302-Sequence113.jpg', 158, 171, NULL, 'jpg', '2019-07-13 03:09:33', '2019-07-13 03:09:33'),
(609, 'Asher-Percival-Resume.docx', 158, 171, NULL, 'docx', '2019-07-13 03:09:36', '2019-07-13 03:09:36'),
(610, '20190302-Sequence077-1.jpg', 159, 172, NULL, 'jpg', '2019-07-13 03:10:04', '2019-07-13 03:10:04'),
(611, '20190302-Sequence106-1.jpg', 159, 172, NULL, 'jpg', '2019-07-13 03:10:28', '2019-07-13 03:10:28'),
(612, '20190302-Sequence033-1.jpg', 159, 172, NULL, 'jpg', '2019-07-13 03:10:44', '2019-07-13 03:10:44'),
(613, '20190302-Sequence113-1.jpg', 159, 172, NULL, 'jpg', '2019-07-13 03:11:00', '2019-07-13 03:11:00'),
(614, 'Asher-Percival-Resume-1.docx', 159, 172, NULL, 'docx', '2019-07-13 03:11:07', '2019-07-13 03:11:07'),
(615, '20190302-Sequence077-2.jpg', 160, 173, NULL, 'jpg', '2019-07-13 03:11:57', '2019-07-13 03:11:57'),
(616, '20190302-Sequence106-2.jpg', 160, 173, NULL, 'jpg', '2019-07-13 03:12:39', '2019-07-13 03:12:39'),
(617, '20190302-Sequence033-2.jpg', 160, 173, NULL, 'jpg', '2019-07-13 03:13:05', '2019-07-13 03:13:05'),
(618, '20190302-Sequence113-2.jpg', 160, 173, NULL, 'jpg', '2019-07-13 03:13:41', '2019-07-13 03:13:41'),
(619, 'Asher-Percival-Resume-2.docx', 160, 173, NULL, 'docx', '2019-07-13 03:13:44', '2019-07-13 03:13:44'),
(620, '20190302-Sequence077-3.jpg', 161, 174, NULL, 'jpg', '2019-07-13 03:14:21', '2019-07-13 03:14:21'),
(621, '20190302-Sequence106-3.jpg', 161, 174, NULL, 'jpg', '2019-07-13 03:14:49', '2019-07-13 03:14:49'),
(622, '20190302-Sequence033-3.jpg', 161, 174, NULL, 'jpg', '2019-07-13 03:15:04', '2019-07-13 03:15:04'),
(623, '20190302-Sequence113-3.jpg', 161, 174, NULL, 'jpg', '2019-07-13 03:15:31', '2019-07-13 03:15:31'),
(624, 'Asher-Percival-Resume-3.docx', 161, 174, NULL, 'docx', '2019-07-13 03:15:34', '2019-07-13 03:15:34'),
(625, '20190302-Sequence077-4.jpg', 162, 175, NULL, 'jpg', '2019-07-13 03:15:57', '2019-07-13 03:15:57'),
(626, '20190302-Sequence106-4.jpg', 162, 175, NULL, 'jpg', '2019-07-13 03:16:29', '2019-07-13 03:16:29'),
(627, 'Asher-Percival-Resume-4.docx', 162, 175, NULL, 'docx', '2019-07-13 03:16:32', '2019-07-13 03:16:32'),
(628, '20190302-Sequence077-5.jpg', 163, 176, NULL, 'jpg', '2019-07-13 03:17:03', '2019-07-13 03:17:03'),
(629, '20190302-Sequence106-5.jpg', 163, 176, NULL, 'jpg', '2019-07-13 03:17:25', '2019-07-13 03:17:25'),
(630, 'Asher-Percival-Resume-5.docx', 163, 176, NULL, 'docx', '2019-07-13 03:17:28', '2019-07-13 03:17:28'),
(631, '20190302-Sequence077-6.jpg', 164, 177, NULL, 'jpg', '2019-07-13 03:17:56', '2019-07-13 03:17:56'),
(632, '20190302-Sequence106-6.jpg', 164, 177, NULL, 'jpg', '2019-07-13 03:18:34', '2019-07-13 03:18:34'),
(633, 'Asher-Full-Body.png', 165, 178, NULL, 'png', '2019-07-13 03:18:43', '2019-07-13 03:18:43'),
(634, 'Asher-Waist-Up.png', 165, 178, NULL, 'png', '2019-07-13 03:18:50', '2019-07-13 03:18:50'),
(635, 'Asher-Percival-Resume-6.docx', 165, 178, NULL, 'docx', '2019-07-13 03:18:55', '2019-07-13 03:18:55'),
(636, 'image4.jpeg', 166, 179, NULL, 'jpeg', '2019-07-13 03:19:05', '2019-07-13 03:19:05'),
(637, 'image2.jpeg', 166, 179, NULL, 'jpeg', '2019-07-13 03:19:12', '2019-07-13 03:19:12'),
(638, 'image3.jpeg', 166, 179, NULL, 'jpeg', '2019-07-13 03:19:33', '2019-07-13 03:19:33'),
(639, 'image5.jpeg', 166, 179, NULL, 'jpeg', '2019-07-13 03:19:36', '2019-07-13 03:19:36'),
(640, 'IMG_0068.png', 166, 179, NULL, 'png', '2019-07-13 03:19:52', '2019-07-13 03:19:52'),
(641, 'IMG_8272.jpg', 166, 179, NULL, 'jpg', '2019-07-13 03:20:03', '2019-07-13 03:20:03'),
(642, 'IMG_9867.jpg', 166, 179, NULL, 'jpg', '2019-07-13 03:20:11', '2019-07-13 03:20:11'),
(643, 'IMG_20180720_180738_polarr.jpg', 167, 180, NULL, 'jpg', '2019-07-13 03:20:43', '2019-07-13 03:20:43'),
(644, 'IMG_20181029_183129_334.jpg', 167, 180, NULL, 'jpg', '2019-07-13 03:20:51', '2019-07-13 03:20:51'),
(645, 'received_194015071513596_polarr.jpg', 167, 180, NULL, 'jpg', '2019-07-13 03:21:00', '2019-07-13 03:21:00'),
(646, 'received_299013970888618_polarr.jpg', 167, 180, NULL, 'jpg', '2019-07-13 03:21:09', '2019-07-13 03:21:09'),
(647, 'IMG_20180722_102539_polarr.jpg', 167, 180, NULL, 'jpg', '2019-07-13 03:21:16', '2019-07-13 03:21:16'),
(648, 'received_513636049083589_polarr.jpg', 167, 180, NULL, 'jpg', '2019-07-13 03:21:20', '2019-07-13 03:21:20'),
(649, 'IMG_20180726_223913_359.jpg', 167, 180, NULL, 'jpg', '2019-07-13 03:21:22', '2019-07-13 03:21:22'),
(650, 'IMG_0362.jpg', 168, 181, NULL, 'jpg', '2019-07-13 03:21:29', '2019-07-13 03:21:29'),
(651, 'IMG_0502.jpg', 168, 181, NULL, 'jpg', '2019-07-13 03:21:31', '2019-07-13 03:21:31'),
(652, 'IMG_0474.jpg', 168, 181, NULL, 'jpg', '2019-07-13 03:21:35', '2019-07-13 03:21:35'),
(653, 'IMG_20180511_105829_353.jpg', 169, 182, NULL, 'jpg', '2019-07-13 03:21:40', '2019-07-13 03:21:40'),
(654, 'FB_IMG_1505790499316.jpg', 169, 182, NULL, 'jpg', '2019-07-13 03:21:42', '2019-07-13 03:21:42'),
(655, '5B4AD9AC-C4DC-4F9F-91FB-2D06E2F4A146.jpeg', 170, 183, NULL, 'jpeg', '2019-07-13 03:21:54', '2019-07-13 03:21:54'),
(656, '2B6CA5A4-757B-4956-B398-7DCDCC269620.jpeg', 170, 183, NULL, 'jpeg', '2019-07-13 03:21:56', '2019-07-13 03:21:56'),
(657, '61C818C5-154A-43FE-B59A-EA5342C19E2E.jpeg', 170, 183, NULL, 'jpeg', '2019-07-13 03:21:59', '2019-07-13 03:21:59'),
(658, '5D59DF39-4E9B-4DBA-90FB-0749017DF470.jpeg', 170, 183, NULL, 'jpeg', '2019-07-13 03:22:01', '2019-07-13 03:22:01'),
(659, '4199DD91-B49B-4A4E-8710-392E3ED8B560.jpeg', 171, 184, NULL, 'jpeg', '2019-07-13 03:22:36', '2019-07-13 03:22:36'),
(660, '7FD9480D-C092-41AC-97A0-598C5494129C.jpeg', 171, 184, NULL, 'jpeg', '2019-07-13 03:22:45', '2019-07-13 03:22:45'),
(661, '638EA47B-D882-459F-A4DA-1871613AA459.jpeg', 171, 184, NULL, 'jpeg', '2019-07-13 03:22:53', '2019-07-13 03:22:53'),
(662, '44B6B983-4516-4ECA-A6F7-A49C7021A31F.jpeg', 172, 185, NULL, 'jpeg', '2019-07-13 03:22:59', '2019-07-13 03:22:59'),
(663, 'BB8F42AC-642D-45B9-83DA-21DE95C45168.jpeg', 172, 185, NULL, 'jpeg', '2019-07-13 03:23:01', '2019-07-13 03:23:01'),
(664, 'FD1C27B1-9520-4BAD-80A7-822592FA26B8.png', 172, 185, NULL, 'png', '2019-07-13 03:23:16', '2019-07-13 03:23:16'),
(665, 'R9-.jpg', 173, 186, NULL, 'jpg', '2019-07-13 03:23:32', '2019-07-13 03:23:32'),
(666, 'R4-.jpg', 173, 186, NULL, 'jpg', '2019-07-13 03:23:38', '2019-07-13 03:23:38'),
(667, 'AstridYachtCV.pages', 173, 186, NULL, 'pages', '2019-07-13 03:23:55', '2019-07-13 03:23:55'),
(668, 'MG_7123.jpg', 174, 187, NULL, 'jpg', '2019-07-13 03:24:07', '2019-07-13 03:24:07'),
(669, 'MG_7066.jpg', 174, 187, NULL, 'jpg', '2019-07-13 03:24:19', '2019-07-13 03:24:19'),
(670, '6F9B7A6F-6E19-465C-9AD4-B4E551BDB8D2.jpeg', 175, 188, NULL, 'jpeg', '2019-07-13 03:24:42', '2019-07-13 03:24:42'),
(671, 'C127A06F-E1FA-43C1-904D-FE01EAA9D921.jpeg', 175, 188, NULL, 'jpeg', '2019-07-13 03:24:45', '2019-07-13 03:24:45'),
(672, '5ADFE231-3485-43B7-B7AF-8D0455CD4715.jpeg', 176, 190, NULL, 'jpeg', '2019-07-13 03:24:58', '2019-07-13 03:24:58'),
(673, '605ECF73-A01E-4961-AF27-E6058E6242CE.jpeg', 176, 190, NULL, 'jpeg', '2019-07-13 03:25:09', '2019-07-13 03:25:09'),
(674, '161029_AngeleParker_0466.jpg', 177, 191, NULL, 'jpg', '2019-07-13 03:25:28', '2019-07-13 03:25:28'),
(675, 'atlas-amazed.jpg', 177, 191, NULL, 'jpg', '2019-07-13 03:25:34', '2019-07-13 03:25:34'),
(676, 'IMG_2945.jpg', 178, 192, NULL, 'jpg', '2019-07-13 03:25:48', '2019-07-13 03:25:48'),
(677, 'IMG_4962-1.jpg', 178, 192, NULL, 'jpg', '2019-07-13 03:25:54', '2019-07-13 03:25:54'),
(678, 'IMG_2925.jpg', 178, 192, NULL, 'jpg', '2019-07-13 03:25:59', '2019-07-13 03:25:59'),
(679, 'image-1.jpeg', 179, 193, NULL, 'jpeg', '2019-07-13 03:26:11', '2019-07-13 03:26:11'),
(680, 'image-2.jpeg', 179, 193, NULL, 'jpeg', '2019-07-13 03:26:15', '2019-07-13 03:26:15'),
(681, 'image-3.jpeg', 179, 193, NULL, 'jpeg', '2019-07-13 03:26:18', '2019-07-13 03:26:18'),
(682, 'image-4.jpeg', 179, 193, NULL, 'jpeg', '2019-07-13 03:26:22', '2019-07-13 03:26:22'),
(683, 'Audrey-bodyshot.jpg', 180, 194, NULL, 'jpg', '2019-07-13 03:26:31', '2019-07-13 03:26:31'),
(684, 'Audrey-Cooper-2.jpg', 180, 194, NULL, 'jpg', '2019-07-13 03:26:40', '2019-07-13 03:26:40'),
(685, 'Audrey-Cooper-3.jpg', 180, 194, NULL, 'jpg', '2019-07-13 03:26:47', '2019-07-13 03:26:47'),
(686, 'AUDREY-COOPER-Resume-_-Barbara-Coultish-Agencies.pdf', 180, 194, NULL, 'pdf', '2019-07-13 03:26:49', '2019-07-13 03:26:49'),
(687, 'EB17080D-7744-4B36-9652-7B670FFB207F.jpeg', 181, 195, NULL, 'jpeg', '2019-07-13 03:27:03', '2019-07-13 03:27:03'),
(688, '2318D0DB-89EA-4AD0-9DEF-F9047D105CD7.jpeg', 181, 195, NULL, 'jpeg', '2019-07-13 03:27:10', '2019-07-13 03:27:10'),
(689, '52596015_1127447817433517_3364839059435290624_n.jpg', 182, 196, NULL, 'jpg', '2019-07-13 03:27:22', '2019-07-13 03:27:22'),
(690, '52823400_262247464697748_1650349378285600768_n.jpg', 182, 196, NULL, 'jpg', '2019-07-13 03:27:23', '2019-07-13 03:27:23'),
(691, '53081071_2801044233367543_8806303439370846208_n.jpg', 182, 196, NULL, 'jpg', '2019-07-13 03:27:27', '2019-07-13 03:27:27'),
(692, '53320749_259568214970726_346427853027409920_n.jpg', 182, 196, NULL, 'jpg', '2019-07-13 03:27:29', '2019-07-13 03:27:29'),
(693, '53081071_2801044233367543_8806303439370846208_n-1.jpg', 182, 196, NULL, 'jpg', '2019-07-13 03:27:33', '2019-07-13 03:27:33'),
(694, '455B5E6F-A777-4B96-B530-8DD32DB97372.jpeg', 183, 197, NULL, 'jpeg', '2019-07-13 03:28:06', '2019-07-13 03:28:06'),
(695, '15E2EFBD-5E73-4C8C-8AE9-227B36A5C631.jpeg', 183, 197, NULL, 'jpeg', '2019-07-13 03:28:17', '2019-07-13 03:28:17'),
(696, 'A550447B-7277-4EF2-B71E-DBECA96895D9.jpeg', 183, 197, NULL, 'jpeg', '2019-07-13 03:28:40', '2019-07-13 03:28:40'),
(697, 'FD5D154D-8526-4504-A53B-232907141D87.jpeg', 184, 198, NULL, 'jpeg', '2019-07-13 03:28:54', '2019-07-13 03:28:54'),
(698, '620A1B38-CA28-4C4A-A3B1-2A035FAF42C4.jpeg', 184, 198, NULL, 'jpeg', '2019-07-13 03:29:01', '2019-07-13 03:29:01'),
(699, 'C10D23C2-2402-48C3-9F13-90F80F594AB9.jpeg', 184, 198, NULL, 'jpeg', '2019-07-13 03:29:09', '2019-07-13 03:29:09'),
(700, '2345AAE3-25C2-4CFE-A213-75577E88D6DB.jpeg', 185, 199, NULL, 'jpeg', '2019-07-13 03:29:36', '2019-07-13 03:29:36'),
(701, '2FF74FD7-0255-47F3-A8D7-50CB5386BC80.jpeg', 185, 199, NULL, 'jpeg', '2019-07-13 03:29:57', '2019-07-13 03:29:57'),
(702, 'B24F60D2-22A0-4856-A2A8-C29483A47FE1.jpeg', 185, 199, NULL, 'jpeg', '2019-07-13 03:30:12', '2019-07-13 03:30:12'),
(703, '28CCC667-2CFF-4BBD-86B9-9A620C1CEA90.jpeg', 185, 199, NULL, 'jpeg', '2019-07-13 03:30:28', '2019-07-13 03:30:28'),
(704, '20170816_2110512.jpg', 186, 200, NULL, 'jpg', '2019-07-13 03:30:38', '2019-07-13 03:30:38'),
(705, '20170816_2110512-1.jpg', 186, 200, NULL, 'jpg', '2019-07-13 03:30:40', '2019-07-13 03:30:40'),
(706, '20190217_143323.jpg', 187, 201, NULL, 'jpg', '2019-07-13 03:30:58', '2019-07-13 03:30:58'),
(707, '20190212_095108.jpg', 187, 201, NULL, 'jpg', '2019-07-13 03:31:07', '2019-07-13 03:31:07'),
(708, '20190316_172927.jpg', 187, 201, NULL, 'jpg', '2019-07-13 03:31:15', '2019-07-13 03:31:15'),
(709, 'IMG_4234.jpg', 188, 202, NULL, 'jpg', '2019-07-13 03:31:22', '2019-07-13 03:31:22'),
(710, '4migyx8N.jpeg', 188, 202, NULL, 'jpeg', '2019-07-13 03:31:25', '2019-07-13 03:31:25'),
(711, 'Fpy37BGn.jpeg', 188, 202, NULL, 'jpeg', '2019-07-13 03:31:27', '2019-07-13 03:31:27'),
(712, 'PxT3bxCt.jpeg', 188, 202, NULL, 'jpeg', '2019-07-13 03:31:31', '2019-07-13 03:31:31'),
(713, 'UBsKMPve.jpeg', 188, 202, NULL, 'jpeg', '2019-07-13 03:31:33', '2019-07-13 03:31:33'),
(714, 'B4DBE896-F2E0-4CBD-9D6F-11F65A46D6D9.jpeg', 189, 203, NULL, 'jpeg', '2019-07-13 03:31:47', '2019-07-13 03:31:47'),
(715, '00D8F3CC-F9DC-48EB-8BF9-E659ECC48B43.jpeg', 189, 203, NULL, 'jpeg', '2019-07-13 03:31:51', '2019-07-13 03:31:51'),
(716, '8589C4EC-1E55-44CD-8DBA-4E63C13EDD58.jpeg', 189, 203, NULL, 'jpeg', '2019-07-13 03:31:58', '2019-07-13 03:31:58'),
(717, 'CDE675F8-664B-448B-A3B3-B4C670F97956.jpeg', 189, 203, NULL, 'jpeg', '2019-07-13 03:32:01', '2019-07-13 03:32:01'),
(718, 'FE77ACCB-4E4E-4854-A92B-B66AFC9E9FE5.jpeg', 189, 203, NULL, 'jpeg', '2019-07-13 03:32:07', '2019-07-13 03:32:07'),
(719, 'IMG_6211-2.jpg', 190, 204, NULL, 'jpg', '2019-07-13 03:32:20', '2019-07-13 03:32:20'),
(720, 'IMG_5739.jpg', 190, 204, NULL, 'jpg', '2019-07-13 03:32:23', '2019-07-13 03:32:23'),
(721, 'IMG_6089.jpg', 190, 204, NULL, 'jpg', '2019-07-13 03:32:29', '2019-07-13 03:32:29'),
(722, 'FB_IMG_1526597147342.jpg', 191, 205, NULL, 'jpg', '2019-07-13 03:32:33', '2019-07-13 03:32:33'),
(723, 'Screenshot_20180517154840.jpg', 191, 205, NULL, 'jpg', '2019-07-13 03:32:36', '2019-07-13 03:32:36'),
(724, 'FB_IMG_1526597288201.jpg', 191, 205, NULL, 'jpg', '2019-07-13 03:32:38', '2019-07-13 03:32:38'),
(725, 'FB_IMG_1526597112197.jpg', 191, 205, NULL, 'jpg', '2019-07-13 03:32:40', '2019-07-13 03:32:40'),
(726, 'FB_IMG_1526593015125.jpg', 191, 205, NULL, 'jpg', '2019-07-13 03:32:41', '2019-07-13 03:32:41'),
(727, 'FB_IMG_1526597377054.jpg', 191, 205, NULL, 'jpg', '2019-07-13 03:32:43', '2019-07-13 03:32:43'),
(728, 'FB_IMG_1526597453739.jpg', 191, 205, NULL, 'jpg', '2019-07-13 03:32:45', '2019-07-13 03:32:45'),
(729, 'FB_IMG_1526597487881.jpg', 191, 205, NULL, 'jpg', '2019-07-13 03:32:48', '2019-07-13 03:32:48'),
(730, 'Avery-Reid-Full-Body-Smile.jpg', 192, 207, NULL, 'jpg', '2019-07-13 03:33:04', '2019-07-13 03:33:04'),
(731, 'Avery-Reid-Waist-up.jpg', 192, 207, NULL, 'jpg', '2019-07-13 03:33:12', '2019-07-13 03:33:12'),
(732, 'Artistic-CV-Avery-Reid.pdf', 192, 207, NULL, 'pdf', '2019-07-13 03:33:14', '2019-07-13 03:33:14'),
(733, 'DSC_0289.jpg', 193, 208, NULL, 'jpg', '2019-07-13 03:33:28', '2019-07-13 03:33:28'),
(734, 'DSC_0192.jpg', 193, 208, NULL, 'jpg', '2019-07-13 03:33:40', '2019-07-13 03:33:40'),
(735, 'Aya-Furukawa-resume.docx', 193, 208, NULL, 'docx', '2019-07-13 03:33:42', '2019-07-13 03:33:42'),
(736, '3C049062-7AEA-4A6F-85C3-7DAB293A6855.jpeg', 194, 209, NULL, 'jpeg', '2019-07-13 03:34:00', '2019-07-13 03:34:00'),
(737, '951DD66B-37BB-4E5B-9784-4BEF6F705876.jpeg', 194, 209, NULL, 'jpeg', '2019-07-13 03:34:07', '2019-07-13 03:34:07'),
(738, 'F66451B0-791C-4309-AA7B-2B5D11E01E98.jpeg', 194, 209, NULL, 'jpeg', '2019-07-13 03:34:23', '2019-07-13 03:34:23'),
(739, 'A5B820C8-711E-44CC-8BEB-116A035102B9.jpeg', 194, 209, NULL, 'jpeg', '2019-07-13 03:34:37', '2019-07-13 03:34:37'),
(740, '0E7BF497-C8EB-41C3-AB45-8E6DC0CA66E5.jpeg', 194, 209, NULL, 'jpeg', '2019-07-13 03:34:47', '2019-07-13 03:34:47'),
(741, 'image-2.jpg', 195, 210, NULL, 'jpg', '2019-07-13 03:34:59', '2019-07-13 03:34:59'),
(742, 'image-3.jpg', 195, 210, NULL, 'jpg', '2019-07-13 03:35:08', '2019-07-13 03:35:08'),
(743, 'image-4.jpg', 195, 210, NULL, 'jpg', '2019-07-13 03:35:22', '2019-07-13 03:35:22'),
(744, '5B85D892-8683-46AE-A2A2-FFE4455D055B.jpeg', 195, 210, NULL, 'jpeg', '2019-07-13 03:35:30', '2019-07-13 03:35:30'),
(745, 'image-5.jpg', 195, 210, NULL, 'jpg', '2019-07-13 03:35:38', '2019-07-13 03:35:38'),
(746, 'image-6.jpg', 195, 210, NULL, 'jpg', '2019-07-13 03:35:45', '2019-07-13 03:35:45'),
(747, 'BCB57348-25E4-4DA1-AAD6-23FA2A9D54DE.jpeg', 195, 210, NULL, 'jpeg', '2019-07-13 03:35:53', '2019-07-13 03:35:53'),
(748, '0A8DD7F7-7C13-4D17-AF8D-D38861E61466.jpeg', 195, 210, NULL, 'jpeg', '2019-07-13 03:35:55', '2019-07-13 03:35:55'),
(749, 'Full-Length.jpg', 196, 211, NULL, 'jpg', '2019-07-13 03:36:26', '2019-07-13 03:36:26'),
(750, 'waist-up-1.jpg', 196, 211, NULL, 'jpg', '2019-07-13 03:36:39', '2019-07-13 03:36:39'),
(751, 'IMG_1778.jpg', 196, 211, NULL, 'jpg', '2019-07-13 03:36:43', '2019-07-13 03:36:43'),
(752, 'IMG_0086.jpg', 196, 211, NULL, 'jpg', '2019-07-13 03:36:46', '2019-07-13 03:36:46'),
(753, 'Aboutme.jpg', 196, 211, NULL, 'jpg', '2019-07-13 03:36:52', '2019-07-13 03:36:52'),
(754, 'IMG_9786.jpg', 196, 211, NULL, 'jpg', '2019-07-13 03:36:55', '2019-07-13 03:36:55'),
(755, 'IMG_1454.jpg', 196, 211, NULL, 'jpg', '2019-07-13 03:36:58', '2019-07-13 03:36:58'),
(756, 'IMG_1743.jpg', 196, 211, NULL, 'jpg', '2019-07-13 03:37:01', '2019-07-13 03:37:01'),
(757, 'Ayla1606Profile_8831.jpg', 196, 211, NULL, 'jpg', '2019-07-13 03:37:11', '2019-07-13 03:37:11'),
(758, '7A539370-2CD2-4DA9-AA27-A57F005CE904.jpeg', 197, 212, NULL, 'jpeg', '2019-07-13 03:37:41', '2019-07-13 03:37:41'),
(759, '7C03ABD0-CDA2-43B3-B8BA-75189851FC07.jpeg', 197, 212, NULL, 'jpeg', '2019-07-13 03:38:06', '2019-07-13 03:38:06'),
(760, '5A0FE2DD-C4F3-47E8-9C0E-39561DCE2937.jpeg', 197, 212, NULL, 'jpeg', '2019-07-13 03:38:15', '2019-07-13 03:38:15'),
(761, '81998A91-E21A-4BF9-9944-12AE56497BDA.jpeg', 197, 212, NULL, 'jpeg', '2019-07-13 03:38:23', '2019-07-13 03:38:23'),
(762, '77897A14-0D64-46A8-A61B-E1119210762B.jpeg', 197, 212, NULL, 'jpeg', '2019-07-13 03:38:33', '2019-07-13 03:38:33'),
(763, '20181217_222117.jpg', 198, 213, NULL, 'jpg', '2019-07-13 03:38:49', '2019-07-13 03:38:49'),
(764, '20181217_221957.jpg', 198, 213, NULL, 'jpg', '2019-07-13 03:38:55', '2019-07-13 03:38:55'),
(765, 'MOV_0637.mp4', 198, 213, NULL, 'mp4', '2019-07-13 03:41:16', '2019-07-13 03:41:16'),
(766, 'IMG_0231.jpg', 199, 214, NULL, 'jpg', '2019-07-13 03:41:37', '2019-07-13 03:41:37'),
(767, 'IMG_0295.jpg', 199, 214, NULL, 'jpg', '2019-07-13 03:41:48', '2019-07-13 03:41:48'),
(768, 'IMG_4373.jpg', 199, 214, NULL, 'jpg', '2019-07-13 03:41:59', '2019-07-13 03:41:59'),
(769, 'IMG_4515.jpg', 199, 214, NULL, 'jpg', '2019-07-13 03:42:07', '2019-07-13 03:42:07'),
(770, 'PB260006.jpg', 200, 215, NULL, 'jpg', '2019-07-13 03:42:21', '2019-07-13 03:42:21'),
(771, 'PB260007.jpg', 200, 215, NULL, 'jpg', '2019-07-13 03:42:30', '2019-07-13 03:42:30'),
(772, '5F40E5FD-A545-4557-9F9B-58C08F1E21D6.jpeg', 201, 216, NULL, 'jpeg', '2019-07-13 03:42:37', '2019-07-13 03:42:37'),
(773, '36CD9921-4538-4265-BAEA-DF0AB7CC5305.jpeg', 201, 216, NULL, 'jpeg', '2019-07-13 03:42:44', '2019-07-13 03:42:44'),
(774, '5F40E5FD-A545-4557-9F9B-58C08F1E21D6-1.jpeg', 202, 217, NULL, 'jpeg', '2019-07-13 03:42:55', '2019-07-13 03:42:55'),
(775, '36CD9921-4538-4265-BAEA-DF0AB7CC5305-1.jpeg', 202, 217, NULL, 'jpeg', '2019-07-13 03:42:58', '2019-07-13 03:42:58'),
(776, '57E8B977-F575-4BD2-A3C6-A302C324837D.jpeg', 203, 218, NULL, 'jpeg', '2019-07-13 03:43:33', '2019-07-13 03:43:33'),
(777, '3D6F6193-77EF-488F-BDD6-3226262ABBAA.jpeg', 203, 218, NULL, 'jpeg', '2019-07-13 03:43:37', '2019-07-13 03:43:37'),
(778, 'FB8387CD-678D-4411-9280-CEF9B601F802.jpeg', 203, 218, NULL, 'jpeg', '2019-07-13 03:43:40', '2019-07-13 03:43:40'),
(779, 'BD221A07-792C-4A28-8597-491A24942B59.jpeg', 203, 218, NULL, 'jpeg', '2019-07-13 03:43:45', '2019-07-13 03:43:45'),
(780, '669FD65A-C0C5-4640-873E-E2FF5CE7335E.jpeg', 203, 218, NULL, 'jpeg', '2019-07-13 03:43:50', '2019-07-13 03:43:50'),
(781, '2DDB8650-C45A-46B6-A47A-10308371EB7F.jpeg', 203, 218, NULL, 'jpeg', '2019-07-13 03:43:52', '2019-07-13 03:43:52'),
(782, '24C0D90D-ECA1-4E99-BF74-26D8B684EDDA.jpeg', 203, 218, NULL, 'jpeg', '2019-07-13 03:44:03', '2019-07-13 03:44:03'),
(783, '72F3B938-4D19-472A-BA74-C34F630E805B.jpeg', 203, 218, NULL, 'jpeg', '2019-07-13 03:44:06', '2019-07-13 03:44:06'),
(784, '99CC9474-4A25-495A-B639-4E32A4710A9B.jpeg', 203, 218, NULL, 'jpeg', '2019-07-13 03:44:07', '2019-07-13 03:44:07'),
(785, 'C1D5B35B-284F-4500-948F-24C4C90D3F5F.jpeg', 204, 219, NULL, 'jpeg', '2019-07-13 03:44:32', '2019-07-13 03:44:32'),
(786, '3F911B1A-7B77-4B72-9D49-93D82339AF45.jpeg', 204, 219, NULL, 'jpeg', '2019-07-13 03:44:42', '2019-07-13 03:44:42'),
(787, 'AE5A465D-CC72-415D-8CD0-C61D87D6C6BA.jpeg', 204, 219, NULL, 'jpeg', '2019-07-13 03:44:53', '2019-07-13 03:44:53'),
(788, '4E009F21-4589-4CF1-A901-30F8B9497621.jpeg', 204, 219, NULL, 'jpeg', '2019-07-13 03:45:00', '2019-07-13 03:45:00'),
(789, '9A82D842-9122-4CA7-AECE-3E6E8FBBB1E3.jpeg', 204, 219, NULL, 'jpeg', '2019-07-13 03:45:06', '2019-07-13 03:45:06'),
(790, 'C52150B0-AE9D-44C9-B45B-63C8EDAE2322.jpeg', 204, 219, NULL, 'jpeg', '2019-07-13 03:45:16', '2019-07-13 03:45:16'),
(791, 'BE73B76D-E430-4AB4-B1D7-3BFDA576E438.jpeg', 205, 220, NULL, 'jpeg', '2019-07-13 03:45:32', '2019-07-13 03:45:32'),
(792, '08D1F3EE-5F61-4F50-B617-41A9C2D02DF4.jpeg', 205, 220, NULL, 'jpeg', '2019-07-13 03:45:38', '2019-07-13 03:45:38'),
(793, 'A538EB2E-C416-4410-BB84-3819D14BD89F.jpeg', 205, 220, NULL, 'jpeg', '2019-07-13 03:45:47', '2019-07-13 03:45:47'),
(794, 'B0660EEC-118C-4774-9C98-C37F03B2CD80.jpeg', 205, 220, NULL, 'jpeg', '2019-07-13 03:45:57', '2019-07-13 03:45:57'),
(795, 'me3.png', 206, 221, NULL, 'png', '2019-07-13 03:46:07', '2019-07-13 03:46:07'),
(796, 'me2.png', 206, 221, NULL, 'png', '2019-07-13 03:46:11', '2019-07-13 03:46:11'),
(797, 'Screenshot_20190327-203031_1.jpg', 208, 224, NULL, 'jpg', '2019-07-13 03:46:17', '2019-07-13 03:46:17'),
(798, 'IMG_8190.jpg', 207, 222, NULL, 'jpg', '2019-07-13 03:46:20', '2019-07-13 03:46:20'),
(799, 'Screenshot_20190327-203132_1.jpg', 208, 224, NULL, 'jpg', '2019-07-13 03:46:22', '2019-07-13 03:46:22'),
(800, 'IMG_1071.jpg', 207, 222, NULL, 'jpg', '2019-07-13 03:46:24', '2019-07-13 03:46:24'),
(801, 'Screenshot_20190327-203046_1.jpg', 208, 224, NULL, 'jpg', '2019-07-13 03:46:25', '2019-07-13 03:46:25'),
(802, 'IMG_9968.jpg', 207, 222, NULL, 'jpg', '2019-07-13 03:46:28', '2019-07-13 03:46:28'),
(803, 'resume_1548423991835.pdf', 208, 224, NULL, 'pdf', '2019-07-13 03:46:28', '2019-07-13 03:46:28'),
(804, 'AKadwell-May-2018.pdf', 207, 222, NULL, 'pdf', '2019-07-13 03:46:30', '2019-07-13 03:46:30'),
(805, 'ali-wong.jpg', 209, 225, NULL, 'jpg', '2019-07-13 03:46:37', '2019-07-13 03:46:37'),
(806, 'full-body-anne.jpg', 210, 226, NULL, 'jpg', '2019-07-13 03:46:46', '2019-07-13 03:46:46'),
(807, 'accordion-4.jpg', 209, 225, NULL, 'jpg', '2019-07-13 03:46:48', '2019-07-13 03:46:48'),
(808, '6-6-18d.jpg', 209, 225, NULL, 'jpg', '2019-07-13 03:46:51', '2019-07-13 03:46:51'),
(809, 'pamela-Talkin-a.jpg', 209, 225, NULL, 'jpg', '2019-07-13 03:46:57', '2019-07-13 03:46:57'),
(810, 'waist-up-anne.jpg', 210, 226, NULL, 'jpg', '2019-07-13 03:46:58', '2019-07-13 03:46:58'),
(811, 'HC-102.jpg', 209, 225, NULL, 'jpg', '2019-07-13 03:46:59', '2019-07-13 03:46:59'),
(812, 'deadpool-6.jpg', 209, 225, NULL, 'jpg', '2019-07-13 03:47:01', '2019-07-13 03:47:01'),
(813, 'Death-Note-1.jpg', 209, 225, NULL, 'jpg', '2019-07-13 03:47:05', '2019-07-13 03:47:05'),
(814, 'supernatural-6.jpg', 209, 225, NULL, 'jpg', '2019-07-13 03:47:07', '2019-07-13 03:47:07'),
(815, 'yoga-69.jpg', 209, 225, NULL, 'jpg', '2019-07-13 03:47:11', '2019-07-13 03:47:11'),
(816, '20170727_141133-1.jpg', 211, 227, NULL, 'jpg', '2019-07-13 03:47:14', '2019-07-13 03:47:14'),
(817, '20171124_202715-1.jpg', 211, 227, NULL, 'jpg', '2019-07-13 03:47:24', '2019-07-13 03:47:24'),
(818, 'EC0A7BEB-1EC0-45A0-9E72-9583776248D0.jpeg', 212, 228, NULL, 'jpeg', '2019-07-13 03:47:28', '2019-07-13 03:47:28'),
(819, '95898FEE-B7C7-4FFC-8500-C4D5B271EA26.jpeg', 212, 228, NULL, 'jpeg', '2019-07-13 03:47:30', '2019-07-13 03:47:30'),
(820, '2CC1527D-9931-4EA9-A3E9-9E379D75C99C.jpeg', 212, 228, NULL, 'jpeg', '2019-07-13 03:47:36', '2019-07-13 03:47:36'),
(821, 'IMG_2669.jpg', 213, 229, NULL, 'jpg', '2019-07-13 03:47:41', '2019-07-13 03:47:41'),
(822, 'IMG_2612.jpg', 213, 229, NULL, 'jpg', '2019-07-13 03:47:44', '2019-07-13 03:47:44'),
(823, 'IMG_20190321_1310223.jpg', 214, 230, NULL, 'jpg', '2019-07-13 03:47:44', '2019-07-13 03:47:44'),
(824, 'IMG_4322.jpg', 213, 229, NULL, 'jpg', '2019-07-13 03:47:47', '2019-07-13 03:47:47'),
(825, 'MVIMG_20190316_1034222.jpg', 214, 230, NULL, 'jpg', '2019-07-13 03:47:48', '2019-07-13 03:47:48'),
(826, '20180602_154142-11.jpg', 215, 231, NULL, 'jpg', '2019-07-13 03:47:54', '2019-07-13 03:47:54'),
(827, 'IMG_20190129_1527162.jpg', 214, 230, NULL, 'jpg', '2019-07-13 03:47:56', '2019-07-13 03:47:56'),
(828, '20180623_153126-1.jpg', 215, 231, NULL, 'jpg', '2019-07-13 03:48:03', '2019-07-13 03:48:03'),
(829, 'IMG_20190504_143653.jpg', 214, 230, NULL, 'jpg', '2019-07-13 03:48:13', '2019-07-13 03:48:13'),
(830, '797D3759-AC00-42E6-A031-DC29CADBC440.jpeg', 216, 232, NULL, 'jpeg', '2019-07-13 03:48:18', '2019-07-13 03:48:18'),
(831, '113FEE54-65BB-4193-B3A9-33E6C7D0B409.jpeg', 216, 232, NULL, 'jpeg', '2019-07-13 03:48:22', '2019-07-13 03:48:22'),
(832, 'CC6B8191-F380-47FB-A73A-C95EFBEDE15D.jpeg', 217, 233, NULL, 'jpeg', '2019-07-13 03:48:25', '2019-07-13 03:48:25'),
(833, '2EC0CBD3-9ABE-48C2-AC21-B35BED122578.jpeg', 216, 232, NULL, 'jpeg', '2019-07-13 03:48:36', '2019-07-13 03:48:36'),
(834, '5A4FECD8-3249-4DDD-B4A5-9ED1BF4DC1FE.jpeg', 217, 233, NULL, 'jpeg', '2019-07-13 03:48:38', '2019-07-13 03:48:38'),
(835, 'C9D20CA5-B763-449A-9C26-F881D59E0DC2.jpeg', 217, 233, NULL, 'jpeg', '2019-07-13 03:48:43', '2019-07-13 03:48:43'),
(836, 'MG_2694.jpg', 218, 234, NULL, 'jpg', '2019-07-13 03:48:52', '2019-07-13 03:48:52'),
(837, 'AB7B6B97-404A-4190-89B4-C6E0930F3E9A.jpeg', 217, 233, NULL, 'jpeg', '2019-07-13 03:48:58', '2019-07-13 03:48:58'),
(838, '78A50420-28FA-4FEA-BB0C-D5CA4D51567C.jpeg', 217, 233, NULL, 'jpeg', '2019-07-13 03:49:01', '2019-07-13 03:49:01'),
(839, 'ann-marie-5.jpg', 218, 234, NULL, 'jpg', '2019-07-13 03:49:08', '2019-07-13 03:49:08'),
(840, 'ann-marie-3.jpg', 218, 234, NULL, 'jpg', '2019-07-13 03:49:11', '2019-07-13 03:49:11'),
(841, 'A8A4CB92-3D75-4BD6-9CA6-823F7F22FF88.png', 217, 233, NULL, 'png', '2019-07-13 03:49:12', '2019-07-13 03:49:12'),
(842, 'IMG_8303.jpg', 219, 235, NULL, 'jpg', '2019-07-13 03:49:20', '2019-07-13 03:49:20'),
(843, 'ann-marie-9.jpg', 218, 234, NULL, 'jpg', '2019-07-13 03:49:21', '2019-07-13 03:49:21'),
(844, 'Screen-Shot-2018-02-27-at-9.29.44-PM.png', 219, 235, NULL, 'png', '2019-07-13 03:49:26', '2019-07-13 03:49:26'),
(845, 'Ann-Marie-Leonard-Resume.pdf', 218, 234, NULL, 'pdf', '2019-07-13 03:49:26', '2019-07-13 03:49:26'),
(846, '21231897_10159322901755565_2859288011971438612_n.jpg', 219, 235, NULL, 'jpg', '2019-07-13 03:49:31', '2019-07-13 03:49:31'),
(847, 'unspecified-5.jpeg', 219, 235, NULL, 'jpeg', '2019-07-13 03:49:35', '2019-07-13 03:49:35'),
(848, 'IMG_2780.jpeg', 220, 236, NULL, 'jpeg', '2019-07-13 03:49:39', '2019-07-13 03:49:39'),
(849, 'Anthony-Demare-Dress-shirt-1.jpg', 222, 239, NULL, 'jpg', '2019-07-13 03:49:49', '2019-07-13 03:49:49'),
(850, 'IMG_5987.jpg', 221, 237, NULL, 'jpg', '2019-07-13 03:49:52', '2019-07-13 03:49:52'),
(851, 'Headshot-Dramatic-2.jpg', 222, 239, NULL, 'jpg', '2019-07-13 03:49:53', '2019-07-13 03:49:53'),
(852, 'body-shot-2017.jpg', 222, 239, NULL, 'jpg', '2019-07-13 03:50:00', '2019-07-13 03:50:00'),
(853, 'IMG_5990.jpg', 221, 237, NULL, 'jpg', '2019-07-13 03:50:04', '2019-07-13 03:50:04'),
(854, 'IMG_6047.jpg', 221, 237, NULL, 'jpg', '2019-07-13 03:50:06', '2019-07-13 03:50:06'),
(855, 'IMG_5830.jpg', 221, 237, NULL, 'jpg', '2019-07-13 03:50:09', '2019-07-13 03:50:09'),
(856, '991FD19F-CE94-4DA1-AF2E-A6C84913A10D.jpeg', 223, 240, NULL, 'jpeg', '2019-07-13 03:50:12', '2019-07-13 03:50:12'),
(857, '8EDBB7A2-88C2-4895-8905-4A8299FB1FCF.jpeg', 223, 240, NULL, 'jpeg', '2019-07-13 03:50:19', '2019-07-13 03:50:19'),
(858, 'A67C339A-FDD6-4A9B-9DC5-C910ABC34361.jpeg', 223, 240, NULL, 'jpeg', '2019-07-13 03:50:30', '2019-07-13 03:50:30'),
(859, 'E315CE88-4B00-4427-8003-871FB6B7B888.jpeg', 223, 240, NULL, 'jpeg', '2019-07-13 03:50:32', '2019-07-13 03:50:32'),
(860, 'full-body-Antony.jpg', 226, 243, NULL, 'jpg', '2019-07-13 03:50:35', '2019-07-13 03:50:35'),
(861, 'Waist-up.jpeg', 226, 243, NULL, 'jpeg', '2019-07-13 03:50:39', '2019-07-13 03:50:39'),
(862, '4F37028A-F7A1-4658-A9EB-8540895E1242.jpeg', 227, 244, NULL, 'jpeg', '2019-07-13 03:50:43', '2019-07-13 03:50:43'),
(863, 'image00049.jpeg', 226, 243, NULL, 'jpeg', '2019-07-13 03:50:45', '2019-07-13 03:50:45'),
(864, 'C143EA8A-7447-4573-935E-B1C82EB862AC.jpeg', 227, 244, NULL, 'jpeg', '2019-07-13 03:50:45', '2019-07-13 03:50:45'),
(865, '1F90A718-0975-4850-8364-AD1A0A93683C.jpeg', 227, 244, NULL, 'jpeg', '2019-07-13 03:50:49', '2019-07-13 03:50:49'),
(866, 'image00046.jpeg', 226, 243, NULL, 'jpeg', '2019-07-13 03:50:50', '2019-07-13 03:50:50'),
(867, '822799E7-D219-43B6-9581-D507CE8B6212.jpeg', 227, 244, NULL, 'jpeg', '2019-07-13 03:50:53', '2019-07-13 03:50:53'),
(868, 'image00026.jpeg', 226, 243, NULL, 'jpeg', '2019-07-13 03:50:55', '2019-07-13 03:50:55'),
(869, 'image00014.jpeg', 226, 243, NULL, 'jpeg', '2019-07-13 03:51:03', '2019-07-13 03:51:03'),
(870, 'E4D1838F-2217-49DD-89E4-344974EA8B1E.jpeg', 227, 244, NULL, 'jpeg', '2019-07-13 03:51:07', '2019-07-13 03:51:07'),
(871, 'image00007.jpeg', 226, 243, NULL, 'jpeg', '2019-07-13 03:51:09', '2019-07-13 03:51:09'),
(872, 'IMG_7922pscc.jpg', 228, 245, NULL, 'jpg', '2019-07-13 03:51:17', '2019-07-13 03:51:17'),
(873, 'image00011.jpeg', 226, 243, NULL, 'jpeg', '2019-07-13 03:51:17', '2019-07-13 03:51:17'),
(874, 'image00037.jpeg', 226, 243, NULL, 'jpeg', '2019-07-13 03:51:28', '2019-07-13 03:51:28'),
(875, 'IMG_7905pscc.jpg', 228, 245, NULL, 'jpg', '2019-07-13 03:51:29', '2019-07-13 03:51:29'),
(876, 'IMG_7930pscc.jpg', 228, 245, NULL, 'jpg', '2019-07-13 03:51:32', '2019-07-13 03:51:32'),
(877, 'Aoy-Finals-web-size-6.jpg', 229, 246, NULL, 'jpg', '2019-07-13 03:51:34', '2019-07-13 03:51:34'),
(878, 'Aoy-Finals-web-size-7.jpg', 229, 246, NULL, 'jpg', '2019-07-13 03:51:37', '2019-07-13 03:51:37'),
(879, 'IMG_7914pscc.jpg', 228, 245, NULL, 'jpg', '2019-07-13 03:51:38', '2019-07-13 03:51:38'),
(880, 'EjmTinvjvS8Dhi8kAKSk5TCVileTuZTF0ajRu9TE1JiZzVR77w1UnjclmRsIaO1NXNvQbgs190.png', 229, 246, NULL, 'png', '2019-07-13 03:51:39', '2019-07-13 03:51:39'),
(881, 'IMG_7893pscc.jpg', 228, 245, NULL, 'jpg', '2019-07-13 03:51:47', '2019-07-13 03:51:47'),
(882, 'IMG_1196.jpg', 229, 246, NULL, 'jpg', '2019-07-13 03:51:51', '2019-07-13 03:51:51'),
(883, '5A61B3B8-8453-40A7-945A-7960400615BC.jpeg', 230, 247, NULL, 'jpeg', '2019-07-13 03:51:56', '2019-07-13 03:51:56'),
(884, 'AA9C3911-3037-4556-BA4D-96647D2BCAAE.jpeg', 230, 247, NULL, 'jpeg', '2019-07-13 03:51:58', '2019-07-13 03:51:58'),
(885, 'IMG_9492.jpg', 231, 248, NULL, 'jpg', '2019-07-13 03:52:07', '2019-07-13 03:52:07'),
(886, 'IMG_9491.jpg', 231, 248, NULL, 'jpg', '2019-07-13 03:52:13', '2019-07-13 03:52:13'),
(887, 'A91C400D-C432-47CE-9BF9-E6D265B2C4A8.jpeg', 230, 247, NULL, 'jpeg', '2019-07-13 03:52:18', '2019-07-13 03:52:18'),
(888, 'Snapseed.jpg', 231, 248, NULL, 'jpg', '2019-07-13 03:52:24', '2019-07-13 03:52:24'),
(889, '69F50753-8150-4CDA-8BCD-0D73EA798256.jpeg', 231, 248, NULL, 'jpeg', '2019-07-13 03:52:26', '2019-07-13 03:52:26'),
(890, '0CCEF954-C411-4F25-A820-559E7C9D8AF9.jpeg', 230, 247, NULL, 'jpeg', '2019-07-13 03:52:31', '2019-07-13 03:52:31'),
(891, 'FB_IMG_1551231456849.jpg', 232, 249, NULL, 'jpg', '2019-07-13 03:52:32', '2019-07-13 03:52:32'),
(892, 'FB_IMG_1551231382571.jpg', 232, 249, NULL, 'jpg', '2019-07-13 03:52:35', '2019-07-13 03:52:35'),
(893, 'FB_IMG_1551231401829.jpg', 232, 249, NULL, 'jpg', '2019-07-13 03:52:40', '2019-07-13 03:52:40'),
(894, '017CCDCA-17D8-4066-AA12-BFF6D68AC543.jpeg', 230, 247, NULL, 'jpeg', '2019-07-13 03:52:55', '2019-07-13 03:52:55'),
(895, '3AFF1EE5-1B02-45AB-968E-7D91B8A22566.jpeg', 230, 247, NULL, 'jpeg', '2019-07-13 03:53:13', '2019-07-13 03:53:13'),
(896, 'LRM_EXPORT_20180521_211008.jpg', 233, 250, NULL, 'jpg', '2019-07-13 03:53:19', '2019-07-13 03:53:19'),
(897, '8162018202814.jpg', 233, 250, NULL, 'jpg', '2019-07-13 03:53:38', '2019-07-13 03:53:38'),
(898, 'LRM_EXPORT_20180521_211008-1.jpg', 235, 252, NULL, 'jpg', '2019-07-13 03:54:12', '2019-07-13 03:54:12'),
(899, '8162018202814-1.jpg', 235, 252, NULL, 'jpg', '2019-07-13 03:54:31', '2019-07-13 03:54:31'),
(900, '2018-07-25-00.08.00.jpg', 236, 253, NULL, 'jpg', '2019-07-13 03:54:37', '2019-07-13 03:54:37'),
(901, 'Aramesh_05.jpg', 236, 253, NULL, 'jpg', '2019-07-13 03:54:46', '2019-07-13 03:54:46'),
(902, '20170221_164432_HDR_resized.jpg', 237, 254, NULL, 'jpg', '2019-07-13 03:54:51', '2019-07-13 03:54:51'),
(903, '20180319_105202.jpg', 237, 254, NULL, 'jpg', '2019-07-13 03:54:54', '2019-07-13 03:54:54'),
(904, 'rsz_img_7704-edit.jpg', 237, 254, NULL, 'jpg', '2019-07-13 03:54:57', '2019-07-13 03:54:57'),
(905, 'IMG_20170111_122736_031_resized.jpg', 237, 254, NULL, 'jpg', '2019-07-13 03:54:59', '2019-07-13 03:54:59'),
(906, 'IMG_20170807_024820_495_resized.jpg', 237, 254, NULL, 'jpg', '2019-07-13 03:55:02', '2019-07-13 03:55:02'),
(907, '20170922_210119_resized.jpg', 237, 254, NULL, 'jpg', '2019-07-13 03:55:07', '2019-07-13 03:55:07'),
(908, 'IMG_20170105_194803_324_resized.jpg', 237, 254, NULL, 'jpg', '2019-07-13 03:55:09', '2019-07-13 03:55:09'),
(909, 'Araz-Yaghoubi-Stand-in-Res.pdf', 237, 254, NULL, 'pdf', '2019-07-13 03:55:11', '2019-07-13 03:55:11'),
(910, 'IMG_3213.png', 238, 256, NULL, 'png', '2019-07-13 03:55:26', '2019-07-13 03:55:26'),
(911, 'IMG_3217.jpg', 238, 256, NULL, 'jpg', '2019-07-13 03:55:30', '2019-07-13 03:55:30'),
(912, 'IMG_3210.png', 238, 256, NULL, 'png', '2019-07-13 03:55:38', '2019-07-13 03:55:38'),
(913, '74D3BDE6-28B6-4D90-BAA8-E9C57C8696ED.jpeg', 239, 257, NULL, 'jpeg', '2019-07-13 03:55:47', '2019-07-13 03:55:47'),
(914, 'C1597E5E-39DB-4C21-897E-1D71BFB59AF0.jpeg', 239, 257, NULL, 'jpeg', '2019-07-13 03:55:52', '2019-07-13 03:55:52'),
(915, 'G0144247.jpg', 240, 258, NULL, 'jpg', '2019-07-13 03:56:18', '2019-07-13 03:56:18'),
(916, 'G0174250.jpg', 240, 258, NULL, 'jpg', '2019-07-13 03:56:24', '2019-07-13 03:56:24'),
(917, 'G0104243.jpg', 240, 258, NULL, 'jpg', '2019-07-13 03:56:35', '2019-07-13 03:56:35'),
(918, 'Aj-Resume-final.pdf', 240, 258, NULL, 'pdf', '2019-07-13 03:56:38', '2019-07-13 03:56:38'),
(919, 'IMG_20190201_225636_resized_20190201_105712668.jpg', 241, 259, NULL, 'jpg', '2019-07-13 03:56:47', '2019-07-13 03:56:47'),
(920, 'IMG_20190201_211744-copy.jpg', 241, 259, NULL, 'jpg', '2019-07-13 03:56:54', '2019-07-13 03:56:54'),
(921, 'IMG_20180910_204133-copy.jpg', 241, 259, NULL, 'jpg', '2019-07-13 03:57:02', '2019-07-13 03:57:02'),
(922, 'IMG_20190201_224744_resized_20190201_104849870.jpg', 241, 259, NULL, 'jpg', '2019-07-13 03:57:09', '2019-07-13 03:57:09'),
(923, 'IMG_20181022_093527.jpg', 241, 259, NULL, 'jpg', '2019-07-13 03:57:14', '2019-07-13 03:57:14'),
(924, 'IMG_20190201_225507_resized_20190201_105611997.jpg', 241, 259, NULL, 'jpg', '2019-07-13 03:57:17', '2019-07-13 03:57:17'),
(925, 'IMG_20190201_225507_resized_20190201_105611997-1.jpg', 241, 259, NULL, 'jpg', '2019-07-13 03:57:20', '2019-07-13 03:57:20'),
(926, 'P1090080.jpg', 242, 260, NULL, 'jpg', '2019-07-13 03:57:30', '2019-07-13 03:57:30'),
(927, 'IMG_7210.jpg', 242, 260, NULL, 'jpg', '2019-07-13 03:57:33', '2019-07-13 03:57:33'),
(928, 'IMG_2069.jpg', 242, 260, NULL, 'jpg', '2019-07-13 03:57:35', '2019-07-13 03:57:35'),
(929, 'IMG_2478.jpg', 242, 260, NULL, 'jpg', '2019-07-13 03:57:38', '2019-07-13 03:57:38'),
(930, '6860CCB7-C763-4052-AFA1-A676E43975B6.jpeg', 243, 261, NULL, 'jpeg', '2019-07-13 03:58:04', '2019-07-13 03:58:04'),
(931, '38FA98DA-F9EC-4C60-819D-F814C48CC768.jpeg', 243, 261, NULL, 'jpeg', '2019-07-13 03:58:13', '2019-07-13 03:58:13'),
(932, 'IMG_8202.jpg', 244, 262, NULL, 'jpg', '2019-07-13 03:58:34', '2019-07-13 03:58:34'),
(933, 'IMG_7744.jpg', 244, 262, NULL, 'jpg', '2019-07-13 03:58:37', '2019-07-13 03:58:37'),
(934, 'IMG_7222.jpg', 244, 262, NULL, 'jpg', '2019-07-13 03:58:39', '2019-07-13 03:58:39'),
(935, 'IMG_5580.jpg', 244, 262, NULL, 'jpg', '2019-07-13 03:58:44', '2019-07-13 03:58:44'),
(936, 'IMG_7283.jpg', 244, 262, NULL, 'jpg', '2019-07-13 03:58:48', '2019-07-13 03:58:48'),
(937, 'IMG_5009.jpg', 244, 262, NULL, 'jpg', '2019-07-13 03:58:51', '2019-07-13 03:58:51'),
(938, 'IMG_2763.jpg', 244, 262, NULL, 'jpg', '2019-07-13 03:58:53', '2019-07-13 03:58:53'),
(939, 'IMG_4408.jpg', 244, 262, NULL, 'jpg', '2019-07-13 03:58:54', '2019-07-13 03:58:54'),
(940, 'IMG_2762.jpg', 244, 262, NULL, 'jpg', '2019-07-13 03:58:56', '2019-07-13 03:58:56'),
(941, 'art_peter2018_winter1.jpg', 245, 263, NULL, 'jpg', '2019-07-13 03:59:23', '2019-07-13 03:59:23'),
(942, 'save-new-7.jpg', 245, 263, NULL, 'jpg', '2019-07-13 03:59:39', '2019-07-13 03:59:39'),
(943, 'art_peter2018_2.jpg', 245, 263, NULL, 'jpg', '2019-07-13 03:59:52', '2019-07-13 03:59:52'),
(944, 'art_peter2018_3.jpg', 245, 263, NULL, 'jpg', '2019-07-13 04:00:10', '2019-07-13 04:00:10'),
(945, '595-4.jpg', 245, 263, NULL, 'jpg', '2019-07-13 04:00:12', '2019-07-13 04:00:12'),
(946, '595-5.jpg', 245, 263, NULL, 'jpg', '2019-07-13 04:00:13', '2019-07-13 04:00:13'),
(947, '1377014_10202128575277121_1466341437_n.jpg', 245, 263, NULL, 'jpg', '2019-07-13 04:00:15', '2019-07-13 04:00:15'),
(948, '33312108_211752486287693_3704167237033132032_n.jpg', 245, 263, NULL, 'jpg', '2019-07-13 04:00:18', '2019-07-13 04:00:18'),
(949, '595-10.jpg', 245, 263, NULL, 'jpg', '2019-07-13 04:00:19', '2019-07-13 04:00:19'),
(950, 'Lighthouse.jpg', 246, 264, NULL, 'jpg', '2019-07-13 04:00:27', '2019-07-13 04:00:27'),
(951, 'Penguins.jpg', 246, 264, NULL, 'jpg', '2019-07-13 04:00:37', '2019-07-13 04:00:37'),
(952, 'Tulips.jpg', 246, 264, NULL, 'jpg', '2019-07-13 04:00:42', '2019-07-13 04:00:42'),
(953, 'Penguins-1.jpg', 246, 264, NULL, 'jpg', '2019-07-13 04:00:48', '2019-07-13 04:00:48'),
(954, '20190302-Sequence077.jpg', 247, 266, NULL, 'jpg', '2019-07-13 04:01:18', '2019-07-13 04:01:18'),
(955, '20190302-Sequence106.jpg', 247, 266, NULL, 'jpg', '2019-07-13 04:02:00', '2019-07-13 04:02:00'),
(956, '20190302-Sequence033.jpg', 247, 266, NULL, 'jpg', '2019-07-13 04:02:19', '2019-07-13 04:02:19'),
(957, '20190302-Sequence113.jpg', 247, 266, NULL, 'jpg', '2019-07-13 04:03:06', '2019-07-13 04:03:06');
INSERT INTO `media_portfolios` (`id`, `media_url`, `portfolio_id`, `performers_id`, `media_title`, `media_type`, `created_at`, `updated_at`) VALUES
(958, 'Asher-Percival-Resume.docx', 247, 266, NULL, 'docx', '2019-07-13 04:03:10', '2019-07-13 04:03:10'),
(959, '20190302-Sequence077-1.jpg', 248, 267, NULL, 'jpg', '2019-07-13 04:03:39', '2019-07-13 04:03:39'),
(960, '20190302-Sequence106-1.jpg', 248, 267, NULL, 'jpg', '2019-07-13 04:04:13', '2019-07-13 04:04:13'),
(961, '20190302-Sequence033-1.jpg', 248, 267, NULL, 'jpg', '2019-07-13 04:04:39', '2019-07-13 04:04:39'),
(962, '20190302-Sequence113-1.jpg', 248, 267, NULL, 'jpg', '2019-07-13 04:05:01', '2019-07-13 04:05:01'),
(963, 'Asher-Percival-Resume-1.docx', 248, 267, NULL, 'docx', '2019-07-13 04:05:05', '2019-07-13 04:05:05'),
(964, '20190302-Sequence077-2.jpg', 249, 268, NULL, 'jpg', '2019-07-13 04:05:44', '2019-07-13 04:05:44'),
(965, '20190302-Sequence106-2.jpg', 249, 268, NULL, 'jpg', '2019-07-13 04:06:06', '2019-07-13 04:06:06'),
(966, '20190302-Sequence033-2.jpg', 249, 268, NULL, 'jpg', '2019-07-13 04:06:50', '2019-07-13 04:06:50'),
(967, '20190302-Sequence113-2.jpg', 249, 268, NULL, 'jpg', '2019-07-13 04:07:10', '2019-07-13 04:07:10'),
(968, 'Asher-Percival-Resume-2.docx', 249, 268, NULL, 'docx', '2019-07-13 04:07:13', '2019-07-13 04:07:13'),
(969, '20190302-Sequence077-3.jpg', 250, 269, NULL, 'jpg', '2019-07-13 04:07:42', '2019-07-13 04:07:42'),
(970, '20190302-Sequence106-3.jpg', 250, 269, NULL, 'jpg', '2019-07-13 04:08:14', '2019-07-13 04:08:14'),
(971, '20190302-Sequence033-3.jpg', 250, 269, NULL, 'jpg', '2019-07-13 04:08:30', '2019-07-13 04:08:30'),
(972, '20190302-Sequence113-3.jpg', 250, 269, NULL, 'jpg', '2019-07-13 04:08:46', '2019-07-13 04:08:46'),
(973, 'Asher-Percival-Resume-3.docx', 250, 269, NULL, 'docx', '2019-07-13 04:08:49', '2019-07-13 04:08:49'),
(974, '20190302-Sequence077-4.jpg', 251, 270, NULL, 'jpg', '2019-07-13 04:09:25', '2019-07-13 04:09:25'),
(975, '20190302-Sequence106-4.jpg', 251, 270, NULL, 'jpg', '2019-07-13 04:09:47', '2019-07-13 04:09:47'),
(976, 'Asher-Percival-Resume-4.docx', 251, 270, NULL, 'docx', '2019-07-13 04:09:50', '2019-07-13 04:09:50'),
(977, '20190302-Sequence077-5.jpg', 252, 271, NULL, 'jpg', '2019-07-13 04:10:11', '2019-07-13 04:10:11'),
(978, '20190302-Sequence106-5.jpg', 252, 271, NULL, 'jpg', '2019-07-13 04:10:36', '2019-07-13 04:10:36'),
(979, 'Asher-Percival-Resume-5.docx', 252, 271, NULL, 'docx', '2019-07-13 04:10:39', '2019-07-13 04:10:39'),
(980, '20190302-Sequence077-6.jpg', 253, 272, NULL, 'jpg', '2019-07-13 04:11:19', '2019-07-13 04:11:19'),
(981, '20190302-Sequence106-6.jpg', 253, 272, NULL, 'jpg', '2019-07-13 04:11:43', '2019-07-13 04:11:43'),
(982, 'Asher-Full-Body.png', 254, 273, NULL, 'png', '2019-07-13 04:11:51', '2019-07-13 04:11:51'),
(983, 'Asher-Waist-Up.png', 254, 273, NULL, 'png', '2019-07-13 04:11:56', '2019-07-13 04:11:56'),
(984, 'Asher-Percival-Resume-6.docx', 254, 273, NULL, 'docx', '2019-07-13 04:11:59', '2019-07-13 04:11:59'),
(985, 'image4.jpeg', 255, 274, NULL, 'jpeg', '2019-07-13 04:12:09', '2019-07-13 04:12:09'),
(986, 'image2.jpeg', 255, 274, NULL, 'jpeg', '2019-07-13 04:12:19', '2019-07-13 04:12:19'),
(987, 'image3.jpeg', 255, 274, NULL, 'jpeg', '2019-07-13 04:12:30', '2019-07-13 04:12:30'),
(988, 'image5.jpeg', 255, 274, NULL, 'jpeg', '2019-07-13 04:12:34', '2019-07-13 04:12:34'),
(989, 'IMG_0068.png', 255, 274, NULL, 'png', '2019-07-13 04:12:49', '2019-07-13 04:12:49'),
(990, 'IMG_8272.jpg', 255, 274, NULL, 'jpg', '2019-07-13 04:12:57', '2019-07-13 04:12:57'),
(991, 'IMG_9867.jpg', 255, 274, NULL, 'jpg', '2019-07-13 04:13:04', '2019-07-13 04:13:04'),
(992, 'IMG_20180720_180738_polarr.jpg', 256, 275, NULL, 'jpg', '2019-07-13 04:13:29', '2019-07-13 04:13:29'),
(993, 'IMG_20181029_183129_334.jpg', 256, 275, NULL, 'jpg', '2019-07-13 04:13:41', '2019-07-13 04:13:41'),
(994, 'received_194015071513596_polarr.jpg', 256, 275, NULL, 'jpg', '2019-07-13 04:13:49', '2019-07-13 04:13:49'),
(995, 'received_299013970888618_polarr.jpg', 256, 275, NULL, 'jpg', '2019-07-13 04:14:07', '2019-07-13 04:14:07'),
(996, 'IMG_20180722_102539_polarr.jpg', 256, 275, NULL, 'jpg', '2019-07-13 04:14:17', '2019-07-13 04:14:17'),
(997, 'received_513636049083589_polarr.jpg', 256, 275, NULL, 'jpg', '2019-07-13 04:14:21', '2019-07-13 04:14:21'),
(998, 'IMG_20180726_223913_359.jpg', 256, 275, NULL, 'jpg', '2019-07-13 04:14:23', '2019-07-13 04:14:23'),
(999, 'IMG_0362.jpg', 257, 276, NULL, 'jpg', '2019-07-13 04:14:29', '2019-07-13 04:14:29'),
(1000, 'IMG_0502.jpg', 257, 276, NULL, 'jpg', '2019-07-13 04:14:31', '2019-07-13 04:14:31'),
(1001, 'IMG_0474.jpg', 257, 276, NULL, 'jpg', '2019-07-13 04:14:34', '2019-07-13 04:14:34'),
(1002, 'IMG_20180511_105829_353.jpg', 258, 277, NULL, 'jpg', '2019-07-13 04:14:38', '2019-07-13 04:14:38'),
(1003, 'FB_IMG_1505790499316.jpg', 258, 277, NULL, 'jpg', '2019-07-13 04:14:40', '2019-07-13 04:14:40'),
(1004, '5B4AD9AC-C4DC-4F9F-91FB-2D06E2F4A146.jpeg', 259, 278, NULL, 'jpeg', '2019-07-13 04:14:51', '2019-07-13 04:14:51'),
(1005, '2B6CA5A4-757B-4956-B398-7DCDCC269620.jpeg', 259, 278, NULL, 'jpeg', '2019-07-13 04:14:53', '2019-07-13 04:14:53'),
(1006, '61C818C5-154A-43FE-B59A-EA5342C19E2E.jpeg', 259, 278, NULL, 'jpeg', '2019-07-13 04:14:56', '2019-07-13 04:14:56'),
(1007, '5D59DF39-4E9B-4DBA-90FB-0749017DF470.jpeg', 259, 278, NULL, 'jpeg', '2019-07-13 04:14:59', '2019-07-13 04:14:59'),
(1008, '4199DD91-B49B-4A4E-8710-392E3ED8B560.jpeg', 260, 279, NULL, 'jpeg', '2019-07-13 04:15:29', '2019-07-13 04:15:29'),
(1009, '7FD9480D-C092-41AC-97A0-598C5494129C.jpeg', 260, 279, NULL, 'jpeg', '2019-07-13 04:15:41', '2019-07-13 04:15:41'),
(1010, '638EA47B-D882-459F-A4DA-1871613AA459.jpeg', 260, 279, NULL, 'jpeg', '2019-07-13 04:15:59', '2019-07-13 04:15:59'),
(1011, '44B6B983-4516-4ECA-A6F7-A49C7021A31F.jpeg', 261, 280, NULL, 'jpeg', '2019-07-13 04:16:04', '2019-07-13 04:16:04'),
(1012, 'BB8F42AC-642D-45B9-83DA-21DE95C45168.jpeg', 261, 280, NULL, 'jpeg', '2019-07-13 04:16:07', '2019-07-13 04:16:07'),
(1013, 'FD1C27B1-9520-4BAD-80A7-822592FA26B8.png', 261, 280, NULL, 'png', '2019-07-13 04:16:17', '2019-07-13 04:16:17'),
(1014, 'R9-.jpg', 262, 281, NULL, 'jpg', '2019-07-13 04:16:25', '2019-07-13 04:16:25'),
(1015, 'R4-.jpg', 262, 281, NULL, 'jpg', '2019-07-13 04:16:30', '2019-07-13 04:16:30'),
(1016, 'AstridYachtCV.pages', 262, 281, NULL, 'pages', '2019-07-13 04:16:48', '2019-07-13 04:16:48'),
(1017, 'MG_7123.jpg', 263, 282, NULL, 'jpg', '2019-07-13 04:16:59', '2019-07-13 04:16:59'),
(1018, 'MG_7066.jpg', 263, 282, NULL, 'jpg', '2019-07-13 04:17:09', '2019-07-13 04:17:09'),
(1019, '6F9B7A6F-6E19-465C-9AD4-B4E551BDB8D2.jpeg', 264, 283, NULL, 'jpeg', '2019-07-13 04:17:41', '2019-07-13 04:17:41'),
(1020, 'C127A06F-E1FA-43C1-904D-FE01EAA9D921.jpeg', 264, 283, NULL, 'jpeg', '2019-07-13 04:17:44', '2019-07-13 04:17:44'),
(1021, '5ADFE231-3485-43B7-B7AF-8D0455CD4715.jpeg', 265, 285, NULL, 'jpeg', '2019-07-13 04:17:53', '2019-07-13 04:17:53'),
(1022, '605ECF73-A01E-4961-AF27-E6058E6242CE.jpeg', 265, 285, NULL, 'jpeg', '2019-07-13 04:18:02', '2019-07-13 04:18:02'),
(1023, '161029_AngeleParker_0466.jpg', 266, 286, NULL, 'jpg', '2019-07-13 04:18:17', '2019-07-13 04:18:17'),
(1024, 'atlas-amazed.jpg', 266, 286, NULL, 'jpg', '2019-07-13 04:18:24', '2019-07-13 04:18:24'),
(1025, 'IMG_2945.jpg', 267, 287, NULL, 'jpg', '2019-07-13 04:18:38', '2019-07-13 04:18:38'),
(1026, 'IMG_4962-1.jpg', 267, 287, NULL, 'jpg', '2019-07-13 04:18:41', '2019-07-13 04:18:41'),
(1027, 'IMG_2925.jpg', 267, 287, NULL, 'jpg', '2019-07-13 04:18:45', '2019-07-13 04:18:45'),
(1028, 'image-1.jpeg', 268, 288, NULL, 'jpeg', '2019-07-13 04:18:53', '2019-07-13 04:18:53'),
(1029, 'image-2.jpeg', 268, 288, NULL, 'jpeg', '2019-07-13 04:18:55', '2019-07-13 04:18:55'),
(1030, 'image-3.jpeg', 268, 288, NULL, 'jpeg', '2019-07-13 04:18:57', '2019-07-13 04:18:57'),
(1031, 'image-4.jpeg', 268, 288, NULL, 'jpeg', '2019-07-13 04:19:00', '2019-07-13 04:19:00'),
(1032, 'Audrey-bodyshot.jpg', 269, 289, NULL, 'jpg', '2019-07-13 04:19:07', '2019-07-13 04:19:07'),
(1033, 'Audrey-Cooper-2.jpg', 269, 289, NULL, 'jpg', '2019-07-13 04:19:16', '2019-07-13 04:19:16'),
(1034, 'Audrey-Cooper-3.jpg', 269, 289, NULL, 'jpg', '2019-07-13 04:19:23', '2019-07-13 04:19:23'),
(1035, 'AUDREY-COOPER-Resume-_-Barbara-Coultish-Agencies.pdf', 269, 289, NULL, 'pdf', '2019-07-13 04:19:25', '2019-07-13 04:19:25'),
(1036, 'EB17080D-7744-4B36-9652-7B670FFB207F.jpeg', 270, 290, NULL, 'jpeg', '2019-07-13 04:19:56', '2019-07-13 04:19:56'),
(1037, '2318D0DB-89EA-4AD0-9DEF-F9047D105CD7.jpeg', 270, 290, NULL, 'jpeg', '2019-07-13 04:20:04', '2019-07-13 04:20:04'),
(1038, '52596015_1127447817433517_3364839059435290624_n.jpg', 271, 291, NULL, 'jpg', '2019-07-13 04:20:12', '2019-07-13 04:20:12'),
(1039, '52823400_262247464697748_1650349378285600768_n.jpg', 271, 291, NULL, 'jpg', '2019-07-13 04:20:14', '2019-07-13 04:20:14'),
(1040, '53081071_2801044233367543_8806303439370846208_n.jpg', 271, 291, NULL, 'jpg', '2019-07-13 04:20:16', '2019-07-13 04:20:16'),
(1041, '53320749_259568214970726_346427853027409920_n.jpg', 271, 291, NULL, 'jpg', '2019-07-13 04:20:18', '2019-07-13 04:20:18'),
(1042, '53081071_2801044233367543_8806303439370846208_n-1.jpg', 271, 291, NULL, 'jpg', '2019-07-13 04:20:24', '2019-07-13 04:20:24'),
(1043, '455B5E6F-A777-4B96-B530-8DD32DB97372.jpeg', 272, 292, NULL, 'jpeg', '2019-07-13 04:20:44', '2019-07-13 04:20:44'),
(1044, '15E2EFBD-5E73-4C8C-8AE9-227B36A5C631.jpeg', 272, 292, NULL, 'jpeg', '2019-07-13 04:20:49', '2019-07-13 04:20:49'),
(1045, 'A550447B-7277-4EF2-B71E-DBECA96895D9.jpeg', 272, 292, NULL, 'jpeg', '2019-07-13 04:21:00', '2019-07-13 04:21:00'),
(1046, 'FD5D154D-8526-4504-A53B-232907141D87.jpeg', 273, 293, NULL, 'jpeg', '2019-07-13 04:21:08', '2019-07-13 04:21:08'),
(1047, '620A1B38-CA28-4C4A-A3B1-2A035FAF42C4.jpeg', 273, 293, NULL, 'jpeg', '2019-07-13 04:21:12', '2019-07-13 04:21:12'),
(1048, 'C10D23C2-2402-48C3-9F13-90F80F594AB9.jpeg', 273, 293, NULL, 'jpeg', '2019-07-13 04:21:18', '2019-07-13 04:21:18'),
(1049, '2345AAE3-25C2-4CFE-A213-75577E88D6DB.jpeg', 274, 294, NULL, 'jpeg', '2019-07-13 04:21:45', '2019-07-13 04:21:45'),
(1050, '2FF74FD7-0255-47F3-A8D7-50CB5386BC80.jpeg', 274, 294, NULL, 'jpeg', '2019-07-13 04:22:02', '2019-07-13 04:22:02'),
(1051, 'B24F60D2-22A0-4856-A2A8-C29483A47FE1.jpeg', 274, 294, NULL, 'jpeg', '2019-07-13 04:22:25', '2019-07-13 04:22:25'),
(1052, '28CCC667-2CFF-4BBD-86B9-9A620C1CEA90.jpeg', 274, 294, NULL, 'jpeg', '2019-07-13 04:22:45', '2019-07-13 04:22:45'),
(1053, '20170816_2110512.jpg', 275, 295, NULL, 'jpg', '2019-07-13 04:22:56', '2019-07-13 04:22:56'),
(1054, '20170816_2110512-1.jpg', 275, 295, NULL, 'jpg', '2019-07-13 04:23:02', '2019-07-13 04:23:02'),
(1055, '20190217_143323.jpg', 276, 296, NULL, 'jpg', '2019-07-13 04:23:31', '2019-07-13 04:23:31'),
(1056, '20190212_095108.jpg', 276, 296, NULL, 'jpg', '2019-07-13 04:23:44', '2019-07-13 04:23:44'),
(1057, '20190316_172927.jpg', 276, 296, NULL, 'jpg', '2019-07-13 04:24:03', '2019-07-13 04:24:03'),
(1058, 'IMG_4234.jpg', 277, 297, NULL, 'jpg', '2019-07-13 04:24:10', '2019-07-13 04:24:10'),
(1059, '4migyx8N.jpeg', 277, 297, NULL, 'jpeg', '2019-07-13 04:24:13', '2019-07-13 04:24:13'),
(1060, 'Fpy37BGn.jpeg', 277, 297, NULL, 'jpeg', '2019-07-13 04:24:20', '2019-07-13 04:24:20'),
(1061, 'PxT3bxCt.jpeg', 277, 297, NULL, 'jpeg', '2019-07-13 04:24:33', '2019-07-13 04:24:33'),
(1062, 'UBsKMPve.jpeg', 277, 297, NULL, 'jpeg', '2019-07-13 04:24:36', '2019-07-13 04:24:36'),
(1063, 'B4DBE896-F2E0-4CBD-9D6F-11F65A46D6D9.jpeg', 278, 298, NULL, 'jpeg', '2019-07-13 04:24:47', '2019-07-13 04:24:47'),
(1064, '00D8F3CC-F9DC-48EB-8BF9-E659ECC48B43.jpeg', 278, 298, NULL, 'jpeg', '2019-07-13 04:24:50', '2019-07-13 04:24:50'),
(1065, '8589C4EC-1E55-44CD-8DBA-4E63C13EDD58.jpeg', 278, 298, NULL, 'jpeg', '2019-07-13 04:24:53', '2019-07-13 04:24:53'),
(1066, 'CDE675F8-664B-448B-A3B3-B4C670F97956.jpeg', 278, 298, NULL, 'jpeg', '2019-07-13 04:24:55', '2019-07-13 04:24:55'),
(1067, 'FE77ACCB-4E4E-4854-A92B-B66AFC9E9FE5.jpeg', 278, 298, NULL, 'jpeg', '2019-07-13 04:24:58', '2019-07-13 04:24:58'),
(1068, 'IMG_6211-2.jpg', 279, 299, NULL, 'jpg', '2019-07-13 04:25:09', '2019-07-13 04:25:09'),
(1069, 'IMG_5739.jpg', 279, 299, NULL, 'jpg', '2019-07-13 04:25:13', '2019-07-13 04:25:13'),
(1070, 'IMG_6089.jpg', 279, 299, NULL, 'jpg', '2019-07-13 04:25:18', '2019-07-13 04:25:18'),
(1071, 'FB_IMG_1526597147342.jpg', 280, 300, NULL, 'jpg', '2019-07-13 04:25:22', '2019-07-13 04:25:22'),
(1072, 'Screenshot_20180517154840.jpg', 280, 300, NULL, 'jpg', '2019-07-13 04:25:25', '2019-07-13 04:25:25'),
(1073, 'FB_IMG_1526597288201.jpg', 280, 300, NULL, 'jpg', '2019-07-13 04:25:27', '2019-07-13 04:25:27'),
(1074, 'FB_IMG_1526597112197.jpg', 280, 300, NULL, 'jpg', '2019-07-13 04:25:29', '2019-07-13 04:25:29'),
(1075, 'FB_IMG_1526593015125.jpg', 280, 300, NULL, 'jpg', '2019-07-13 04:25:31', '2019-07-13 04:25:31'),
(1076, 'FB_IMG_1526597377054.jpg', 280, 300, NULL, 'jpg', '2019-07-13 04:25:33', '2019-07-13 04:25:33'),
(1077, 'FB_IMG_1526597453739.jpg', 280, 300, NULL, 'jpg', '2019-07-13 04:25:35', '2019-07-13 04:25:35'),
(1078, 'FB_IMG_1526597487881.jpg', 280, 300, NULL, 'jpg', '2019-07-13 04:25:37', '2019-07-13 04:25:37'),
(1079, 'Avery-Reid-Full-Body-Smile.jpg', 281, 302, NULL, 'jpg', '2019-07-13 04:25:49', '2019-07-13 04:25:49'),
(1080, 'Avery-Reid-Waist-up.jpg', 281, 302, NULL, 'jpg', '2019-07-13 04:25:55', '2019-07-13 04:25:55'),
(1081, 'Artistic-CV-Avery-Reid.pdf', 281, 302, NULL, 'pdf', '2019-07-13 04:25:57', '2019-07-13 04:25:57'),
(1082, 'DSC_0289.jpg', 282, 303, NULL, 'jpg', '2019-07-13 04:26:11', '2019-07-13 04:26:11'),
(1083, 'DSC_0192.jpg', 282, 303, NULL, 'jpg', '2019-07-13 04:26:18', '2019-07-13 04:26:18'),
(1084, 'Aya-Furukawa-resume.docx', 282, 303, NULL, 'docx', '2019-07-13 04:26:20', '2019-07-13 04:26:20'),
(1085, '3C049062-7AEA-4A6F-85C3-7DAB293A6855.jpeg', 283, 304, NULL, 'jpeg', '2019-07-13 04:26:42', '2019-07-13 04:26:42'),
(1086, '951DD66B-37BB-4E5B-9784-4BEF6F705876.jpeg', 283, 304, NULL, 'jpeg', '2019-07-13 04:26:55', '2019-07-13 04:26:55'),
(1087, 'F66451B0-791C-4309-AA7B-2B5D11E01E98.jpeg', 283, 304, NULL, 'jpeg', '2019-07-13 04:27:08', '2019-07-13 04:27:08'),
(1088, 'A5B820C8-711E-44CC-8BEB-116A035102B9.jpeg', 283, 304, NULL, 'jpeg', '2019-07-13 04:27:20', '2019-07-13 04:27:20'),
(1089, '0E7BF497-C8EB-41C3-AB45-8E6DC0CA66E5.jpeg', 283, 304, NULL, 'jpeg', '2019-07-13 04:27:26', '2019-07-13 04:27:26'),
(1090, 'image-2.jpg', 284, 305, NULL, 'jpg', '2019-07-13 04:27:37', '2019-07-13 04:27:37'),
(1091, 'image-3.jpg', 284, 305, NULL, 'jpg', '2019-07-13 04:27:43', '2019-07-13 04:27:43'),
(1092, 'image-4.jpg', 284, 305, NULL, 'jpg', '2019-07-13 04:27:52', '2019-07-13 04:27:52'),
(1093, '5B85D892-8683-46AE-A2A2-FFE4455D055B.jpeg', 284, 305, NULL, 'jpeg', '2019-07-13 04:27:59', '2019-07-13 04:27:59'),
(1094, 'image-5.jpg', 284, 305, NULL, 'jpg', '2019-07-13 04:28:15', '2019-07-13 04:28:15'),
(1095, 'image-6.jpg', 284, 305, NULL, 'jpg', '2019-07-13 04:28:26', '2019-07-13 04:28:26'),
(1096, 'BCB57348-25E4-4DA1-AAD6-23FA2A9D54DE.jpeg', 284, 305, NULL, 'jpeg', '2019-07-13 04:28:34', '2019-07-13 04:28:34'),
(1097, '0A8DD7F7-7C13-4D17-AF8D-D38861E61466.jpeg', 284, 305, NULL, 'jpeg', '2019-07-13 04:28:37', '2019-07-13 04:28:37'),
(1098, 'Full-Length.jpg', 285, 306, NULL, 'jpg', '2019-07-13 04:29:02', '2019-07-13 04:29:02'),
(1099, 'waist-up-1.jpg', 285, 306, NULL, 'jpg', '2019-07-13 04:29:08', '2019-07-13 04:29:08'),
(1100, 'IMG_1778.jpg', 285, 306, NULL, 'jpg', '2019-07-13 04:29:10', '2019-07-13 04:29:10'),
(1101, 'IMG_0086.jpg', 285, 306, NULL, 'jpg', '2019-07-13 04:29:13', '2019-07-13 04:29:13'),
(1102, 'Aboutme.jpg', 285, 306, NULL, 'jpg', '2019-07-13 04:29:17', '2019-07-13 04:29:17'),
(1103, 'IMG_9786.jpg', 285, 306, NULL, 'jpg', '2019-07-13 04:29:20', '2019-07-13 04:29:20'),
(1104, 'IMG_1454.jpg', 285, 306, NULL, 'jpg', '2019-07-13 04:29:22', '2019-07-13 04:29:22'),
(1105, 'IMG_1743.jpg', 285, 306, NULL, 'jpg', '2019-07-13 04:29:27', '2019-07-13 04:29:27'),
(1106, 'Ayla1606Profile_8831.jpg', 285, 306, NULL, 'jpg', '2019-07-13 04:29:34', '2019-07-13 04:29:34'),
(1107, '7A539370-2CD2-4DA9-AA27-A57F005CE904.jpeg', 286, 307, NULL, 'jpeg', '2019-07-13 04:29:50', '2019-07-13 04:29:50'),
(1108, '7C03ABD0-CDA2-43B3-B8BA-75189851FC07.jpeg', 286, 307, NULL, 'jpeg', '2019-07-13 04:30:02', '2019-07-13 04:30:02'),
(1109, '5A0FE2DD-C4F3-47E8-9C0E-39561DCE2937.jpeg', 286, 307, NULL, 'jpeg', '2019-07-13 04:30:11', '2019-07-13 04:30:11'),
(1110, '81998A91-E21A-4BF9-9944-12AE56497BDA.jpeg', 286, 307, NULL, 'jpeg', '2019-07-13 04:30:20', '2019-07-13 04:30:20'),
(1111, '77897A14-0D64-46A8-A61B-E1119210762B.jpeg', 286, 307, NULL, 'jpeg', '2019-07-13 04:30:27', '2019-07-13 04:30:27'),
(1112, '20181217_222117.jpg', 287, 308, NULL, 'jpg', '2019-07-13 04:30:37', '2019-07-13 04:30:37'),
(1113, '20181217_221957.jpg', 287, 308, NULL, 'jpg', '2019-07-13 04:30:42', '2019-07-13 04:30:42'),
(1114, 'MOV_0637.mp4', 287, 308, NULL, 'mp4', '2019-07-13 04:32:54', '2019-07-13 04:32:54'),
(1115, 'IMG_0231.jpg', 288, 309, NULL, 'jpg', '2019-07-13 04:33:12', '2019-07-13 04:33:12'),
(1116, 'IMG_0295.jpg', 288, 309, NULL, 'jpg', '2019-07-13 04:33:18', '2019-07-13 04:33:18'),
(1117, 'IMG_4373.jpg', 288, 309, NULL, 'jpg', '2019-07-13 04:33:25', '2019-07-13 04:33:25'),
(1118, 'IMG_4515.jpg', 288, 309, NULL, 'jpg', '2019-07-13 04:33:31', '2019-07-13 04:33:31'),
(1119, 'PB260006.jpg', 289, 310, NULL, 'jpg', '2019-07-13 04:33:44', '2019-07-13 04:33:44'),
(1120, 'PB260007.jpg', 289, 310, NULL, 'jpg', '2019-07-13 04:33:54', '2019-07-13 04:33:54'),
(1121, '5F40E5FD-A545-4557-9F9B-58C08F1E21D6.jpeg', 290, 311, NULL, 'jpeg', '2019-07-13 04:34:01', '2019-07-13 04:34:01'),
(1122, '36CD9921-4538-4265-BAEA-DF0AB7CC5305.jpeg', 290, 311, NULL, 'jpeg', '2019-07-13 04:34:04', '2019-07-13 04:34:04'),
(1123, '5F40E5FD-A545-4557-9F9B-58C08F1E21D6-1.jpeg', 291, 312, NULL, 'jpeg', '2019-07-13 04:34:10', '2019-07-13 04:34:10'),
(1124, '36CD9921-4538-4265-BAEA-DF0AB7CC5305-1.jpeg', 291, 312, NULL, 'jpeg', '2019-07-13 04:34:13', '2019-07-13 04:34:13'),
(1125, '57E8B977-F575-4BD2-A3C6-A302C324837D.jpeg', 292, 313, NULL, 'jpeg', '2019-07-13 04:34:34', '2019-07-13 04:34:34'),
(1126, '3D6F6193-77EF-488F-BDD6-3226262ABBAA.jpeg', 292, 313, NULL, 'jpeg', '2019-07-13 04:34:38', '2019-07-13 04:34:38'),
(1127, 'FB8387CD-678D-4411-9280-CEF9B601F802.jpeg', 292, 313, NULL, 'jpeg', '2019-07-13 04:34:40', '2019-07-13 04:34:40'),
(1128, 'BD221A07-792C-4A28-8597-491A24942B59.jpeg', 292, 313, NULL, 'jpeg', '2019-07-13 04:34:47', '2019-07-13 04:34:47'),
(1129, '669FD65A-C0C5-4640-873E-E2FF5CE7335E.jpeg', 292, 313, NULL, 'jpeg', '2019-07-13 04:34:51', '2019-07-13 04:34:51'),
(1130, '2DDB8650-C45A-46B6-A47A-10308371EB7F.jpeg', 292, 313, NULL, 'jpeg', '2019-07-13 04:34:52', '2019-07-13 04:34:52'),
(1131, '24C0D90D-ECA1-4E99-BF74-26D8B684EDDA.jpeg', 292, 313, NULL, 'jpeg', '2019-07-13 04:35:03', '2019-07-13 04:35:03'),
(1132, '72F3B938-4D19-472A-BA74-C34F630E805B.jpeg', 292, 313, NULL, 'jpeg', '2019-07-13 04:35:07', '2019-07-13 04:35:07'),
(1133, '99CC9474-4A25-495A-B639-4E32A4710A9B.jpeg', 292, 313, NULL, 'jpeg', '2019-07-13 04:35:08', '2019-07-13 04:35:08'),
(1134, 'C1D5B35B-284F-4500-948F-24C4C90D3F5F.jpeg', 293, 314, NULL, 'jpeg', '2019-07-13 04:35:26', '2019-07-13 04:35:26'),
(1135, '3F911B1A-7B77-4B72-9D49-93D82339AF45.jpeg', 293, 314, NULL, 'jpeg', '2019-07-13 04:35:36', '2019-07-13 04:35:36'),
(1136, 'AE5A465D-CC72-415D-8CD0-C61D87D6C6BA.jpeg', 293, 314, NULL, 'jpeg', '2019-07-13 04:35:48', '2019-07-13 04:35:48'),
(1137, '4E009F21-4589-4CF1-A901-30F8B9497621.jpeg', 293, 314, NULL, 'jpeg', '2019-07-13 04:36:03', '2019-07-13 04:36:03'),
(1138, '9A82D842-9122-4CA7-AECE-3E6E8FBBB1E3.jpeg', 293, 314, NULL, 'jpeg', '2019-07-13 04:36:12', '2019-07-13 04:36:12'),
(1139, 'C52150B0-AE9D-44C9-B45B-63C8EDAE2322.jpeg', 293, 314, NULL, 'jpeg', '2019-07-13 04:36:19', '2019-07-13 04:36:19'),
(1140, 'BE73B76D-E430-4AB4-B1D7-3BFDA576E438.jpeg', 294, 315, NULL, 'jpeg', '2019-07-13 04:36:32', '2019-07-13 04:36:32'),
(1141, '08D1F3EE-5F61-4F50-B617-41A9C2D02DF4.jpeg', 294, 315, NULL, 'jpeg', '2019-07-13 04:36:37', '2019-07-13 04:36:37'),
(1142, 'A538EB2E-C416-4410-BB84-3819D14BD89F.jpeg', 294, 315, NULL, 'jpeg', '2019-07-13 04:36:45', '2019-07-13 04:36:45'),
(1143, 'B0660EEC-118C-4774-9C98-C37F03B2CD80.jpeg', 294, 315, NULL, 'jpeg', '2019-07-13 04:36:56', '2019-07-13 04:36:56'),
(1144, 'me3.png', 295, 316, NULL, 'png', '2019-07-13 04:37:06', '2019-07-13 04:37:06'),
(1145, 'me2.png', 295, 316, NULL, 'png', '2019-07-13 04:37:13', '2019-07-13 04:37:13'),
(1146, 'Screenshot_20190327-203031_1.jpg', 296, 318, NULL, 'jpg', '2019-07-13 04:37:18', '2019-07-13 04:37:18'),
(1147, 'Screenshot_20190327-203132_1.jpg', 296, 318, NULL, 'jpg', '2019-07-13 04:37:21', '2019-07-13 04:37:21'),
(1148, 'Screenshot_20190327-203046_1.jpg', 296, 318, NULL, 'jpg', '2019-07-13 04:37:23', '2019-07-13 04:37:23'),
(1149, 'resume_1548423991835.pdf', 296, 318, NULL, 'pdf', '2019-07-13 04:37:25', '2019-07-13 04:37:25'),
(1150, 'ali-wong.jpg', 297, 319, NULL, 'jpg', '2019-07-13 04:37:31', '2019-07-13 04:37:31'),
(1151, 'accordion-4.jpg', 297, 319, NULL, 'jpg', '2019-07-13 04:37:36', '2019-07-13 04:37:36'),
(1152, '6-6-18d.jpg', 297, 319, NULL, 'jpg', '2019-07-13 04:37:39', '2019-07-13 04:37:39'),
(1153, 'pamela-Talkin-a.jpg', 297, 319, NULL, 'jpg', '2019-07-13 04:37:44', '2019-07-13 04:37:44'),
(1154, 'HC-102.jpg', 297, 319, NULL, 'jpg', '2019-07-13 04:37:45', '2019-07-13 04:37:45'),
(1155, 'deadpool-6.jpg', 297, 319, NULL, 'jpg', '2019-07-13 04:37:48', '2019-07-13 04:37:48'),
(1156, 'Death-Note-1.jpg', 297, 319, NULL, 'jpg', '2019-07-13 04:37:54', '2019-07-13 04:37:54'),
(1157, 'supernatural-6.jpg', 297, 319, NULL, 'jpg', '2019-07-13 04:37:56', '2019-07-13 04:37:56'),
(1158, 'yoga-69.jpg', 297, 319, NULL, 'jpg', '2019-07-13 04:38:00', '2019-07-13 04:38:00'),
(1159, 'EC0A7BEB-1EC0-45A0-9E72-9583776248D0.jpeg', 298, 320, NULL, 'jpeg', '2019-07-13 04:38:35', '2019-07-13 04:38:35'),
(1160, '95898FEE-B7C7-4FFC-8500-C4D5B271EA26.jpeg', 298, 320, NULL, 'jpeg', '2019-07-13 04:38:37', '2019-07-13 04:38:37'),
(1161, '2CC1527D-9931-4EA9-A3E9-9E379D75C99C.jpeg', 298, 320, NULL, 'jpeg', '2019-07-13 04:38:45', '2019-07-13 04:38:45'),
(1162, 'IMG_20190321_1310223.jpg', 299, 321, NULL, 'jpg', '2019-07-13 04:38:55', '2019-07-13 04:38:55'),
(1163, 'MVIMG_20190316_1034222.jpg', 299, 321, NULL, 'jpg', '2019-07-13 04:39:01', '2019-07-13 04:39:01'),
(1164, 'IMG_20190129_1527162.jpg', 299, 321, NULL, 'jpg', '2019-07-13 04:39:10', '2019-07-13 04:39:10'),
(1165, 'IMG_20190504_143653.jpg', 299, 321, NULL, 'jpg', '2019-07-13 04:39:21', '2019-07-13 04:39:21'),
(1166, 'CC6B8191-F380-47FB-A73A-C95EFBEDE15D.jpeg', 300, 322, NULL, 'jpeg', '2019-07-13 04:39:34', '2019-07-13 04:39:34'),
(1167, '5A4FECD8-3249-4DDD-B4A5-9ED1BF4DC1FE.jpeg', 300, 322, NULL, 'jpeg', '2019-07-13 04:39:41', '2019-07-13 04:39:41'),
(1168, 'C9D20CA5-B763-449A-9C26-F881D59E0DC2.jpeg', 300, 322, NULL, 'jpeg', '2019-07-13 04:39:45', '2019-07-13 04:39:45'),
(1169, 'AB7B6B97-404A-4190-89B4-C6E0930F3E9A.jpeg', 300, 322, NULL, 'jpeg', '2019-07-13 04:39:52', '2019-07-13 04:39:52'),
(1170, '78A50420-28FA-4FEA-BB0C-D5CA4D51567C.jpeg', 300, 322, NULL, 'jpeg', '2019-07-13 04:39:54', '2019-07-13 04:39:54'),
(1171, 'A8A4CB92-3D75-4BD6-9CA6-823F7F22FF88.png', 300, 322, NULL, 'png', '2019-07-13 04:40:03', '2019-07-13 04:40:03'),
(1172, 'IMG_8303.jpg', 301, 323, NULL, 'jpg', '2019-07-13 04:40:11', '2019-07-13 04:40:11'),
(1173, 'Screen-Shot-2018-02-27-at-9.29.44-PM.png', 301, 323, NULL, 'png', '2019-07-13 04:40:16', '2019-07-13 04:40:16'),
(1174, '21231897_10159322901755565_2859288011971438612_n.jpg', 301, 323, NULL, 'jpg', '2019-07-13 04:40:18', '2019-07-13 04:40:18'),
(1175, 'unspecified-5.jpeg', 301, 323, NULL, 'jpeg', '2019-07-13 04:40:21', '2019-07-13 04:40:21'),
(1176, 'IMG_5987.jpg', 302, 324, NULL, 'jpg', '2019-07-13 04:40:35', '2019-07-13 04:40:35'),
(1177, 'IMG_5990.jpg', 302, 324, NULL, 'jpg', '2019-07-13 04:40:42', '2019-07-13 04:40:42'),
(1178, 'IMG_6047.jpg', 302, 324, NULL, 'jpg', '2019-07-13 04:40:45', '2019-07-13 04:40:45'),
(1179, 'IMG_5830.jpg', 302, 324, NULL, 'jpg', '2019-07-13 04:40:47', '2019-07-13 04:40:47'),
(1180, '4F37028A-F7A1-4658-A9EB-8540895E1242.jpeg', 305, 327, NULL, 'jpeg', '2019-07-13 04:41:20', '2019-07-13 04:41:20'),
(1181, 'C143EA8A-7447-4573-935E-B1C82EB862AC.jpeg', 305, 327, NULL, 'jpeg', '2019-07-13 04:41:23', '2019-07-13 04:41:23'),
(1182, '1F90A718-0975-4850-8364-AD1A0A93683C.jpeg', 305, 327, NULL, 'jpeg', '2019-07-13 04:41:28', '2019-07-13 04:41:28'),
(1183, '822799E7-D219-43B6-9581-D507CE8B6212.jpeg', 305, 327, NULL, 'jpeg', '2019-07-13 04:41:31', '2019-07-13 04:41:31'),
(1184, 'E4D1838F-2217-49DD-89E4-344974EA8B1E.jpeg', 305, 327, NULL, 'jpeg', '2019-07-13 04:41:41', '2019-07-13 04:41:41'),
(1185, 'IMG_7922pscc.jpg', 306, 328, NULL, 'jpg', '2019-07-13 04:41:57', '2019-07-13 04:41:57'),
(1186, 'IMG_7905pscc.jpg', 306, 328, NULL, 'jpg', '2019-07-13 04:42:03', '2019-07-13 04:42:03'),
(1187, 'IMG_7930pscc.jpg', 306, 328, NULL, 'jpg', '2019-07-13 04:42:07', '2019-07-13 04:42:07'),
(1188, 'IMG_7914pscc.jpg', 306, 328, NULL, 'jpg', '2019-07-13 04:42:16', '2019-07-13 04:42:16'),
(1189, 'IMG_7893pscc.jpg', 306, 328, NULL, 'jpg', '2019-07-13 04:42:23', '2019-07-13 04:42:23'),
(1190, '5A61B3B8-8453-40A7-945A-7960400615BC.jpeg', 307, 329, NULL, 'jpeg', '2019-07-13 04:42:30', '2019-07-13 04:42:30'),
(1191, 'AA9C3911-3037-4556-BA4D-96647D2BCAAE.jpeg', 307, 329, NULL, 'jpeg', '2019-07-13 04:42:32', '2019-07-13 04:42:32'),
(1192, 'A91C400D-C432-47CE-9BF9-E6D265B2C4A8.jpeg', 307, 329, NULL, 'jpeg', '2019-07-13 04:42:55', '2019-07-13 04:42:55'),
(1193, '0CCEF954-C411-4F25-A820-559E7C9D8AF9.jpeg', 307, 329, NULL, 'jpeg', '2019-07-13 04:43:08', '2019-07-13 04:43:08'),
(1194, '017CCDCA-17D8-4066-AA12-BFF6D68AC543.jpeg', 307, 329, NULL, 'jpeg', '2019-07-13 04:43:23', '2019-07-13 04:43:23'),
(1195, '3AFF1EE5-1B02-45AB-968E-7D91B8A22566.jpeg', 307, 329, NULL, 'jpeg', '2019-07-13 04:43:37', '2019-07-13 04:43:37'),
(1196, 'IMG_6931.jpg', 308, 330, NULL, 'jpg', '2019-07-13 04:44:02', '2019-07-13 04:44:02'),
(1197, 'IMG_1917.jpg', 308, 330, NULL, 'jpg', '2019-07-13 04:44:14', '2019-07-13 04:44:14'),
(1198, 'N8V7240.jpg', 308, 330, NULL, 'jpg', '2019-07-13 04:44:21', '2019-07-13 04:44:21'),
(1199, 'Chou-Image-3.jpg', 309, 331, NULL, 'jpg', '2019-07-13 04:44:46', '2019-07-13 04:44:46'),
(1200, '35992070_2066308460357568_4226545486267416576_n.jpg', 309, 331, NULL, 'jpg', '2019-07-13 04:44:47', '2019-07-13 04:44:47'),
(1201, 'DSC_0094.jpg', 309, 331, NULL, 'jpg', '2019-07-13 04:45:00', '2019-07-13 04:45:00'),
(1202, '20180528_110009.jpg', 310, 332, NULL, 'jpg', '2019-07-13 04:45:09', '2019-07-13 04:45:09'),
(1203, '20180528_105656.jpg', 310, 332, NULL, 'jpg', '2019-07-13 04:45:19', '2019-07-13 04:45:19'),
(1204, 'Updated-Resume.docx', 310, 332, NULL, 'docx', '2019-07-13 04:45:20', '2019-07-13 04:45:20'),
(1205, 'DSC_1292.jpg', 311, 333, NULL, 'jpg', '2019-07-13 04:45:35', '2019-07-13 04:45:35'),
(1206, 'DSC_1330.jpg', 311, 333, NULL, 'jpg', '2019-07-13 04:45:46', '2019-07-13 04:45:46');

-- --------------------------------------------------------

--
-- Table structure for table `media_skills`
--

CREATE TABLE `media_skills` (
  `id` int(10) UNSIGNED NOT NULL,
  `media_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skill_id` int(10) UNSIGNED NOT NULL,
  `performers_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_07_01_123340_create_performers_portfolios_table', 1),
(5, '2019_07_01_123429_create_media_portfolios_table', 1),
(6, '2019_07_01_123626_create_closets_table', 1),
(7, '2019_07_01_123627_create_media_closets_table', 1),
(9, '2019_07_01_123809_create_media_skills_table', 1),
(10, '2019_07_02_053842_create_corporate_admins_table', 1),
(11, '2019_07_02_053851_create_internal_staffs_table', 1),
(12, '2019_07_02_102243_create_ethnicities_table', 1),
(13, '2019_07_02_102355_create_regions_table', 1),
(14, '2019_07_02_102748_create_performer_status_table', 1),
(15, '2019_07_02_103119_create_skin_tones_table', 1),
(16, '2019_07_02_103237_create_hair_colors_table', 1),
(17, '2019_07_02_103500_create_hair_facials_table', 1),
(18, '2019_07_02_103744_create_body_traits_table', 1),
(19, '2019_07_02_103918_create_hair_styles_table', 1),
(20, '2019_07_02_104019_create_eyes_table', 1),
(21, '2019_07_02_104252_create_handednesses_table', 1),
(22, '2019_07_02_104404_create_vehicle_types_table', 1),
(23, '2019_07_02_104930_create_vehicle_accessories_table', 1),
(24, '2019_07_02_105713_create_vehicle_makes_table', 1),
(25, '2019_07_02_110011_create_seasons_table', 1),
(26, '2019_07_02_110241_create_wardrobe_accessories_table', 1),
(27, '2019_07_02_110444_create_piercings_table', 1),
(28, '2019_07_02_110515_create_props_table', 1),
(29, '2019_07_01_123720_create_skills_table', 2),
(30, '2019_07_09_061910_create_skill_gallery_tag_table', 3),
(31, '2019_07_09_061938_create_portfolio_gallery_tag_table', 3),
(32, '2019_07_09_061952_create_closet_gallery_tag_table', 3),
(34, '2019_07_09_064159_create_performers_details_table', 4),
(36, '2019_07_01_110659_create_performers_table', 5),
(37, '2019_07_11_071748_create_restrictions_table', 6),
(39, '2019_07_11_072045_create_nudity_table', 6),
(40, '2019_07_11_071842_create_notes_table', 7),
(41, '2019_07_12_053213_create_work_perfomers_table', 8),
(42, '2019_07_12_053224_create_project_perfomers_table', 8),
(43, '2019_07_12_053522_create_project_roles_table', 9),
(44, '2019_07_12_082731_create_performer_availability_table', 10),
(45, '2019_07_12_082741_create_performer_agendas_table', 10),
(46, '2019_07_12_082749_create_performer_unions_table', 10),
(47, '2019_07_12_072646_create_nudity_perfomers_table', 11),
(48, '2019_07_13_072605_create_restriction_perfomers_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `nationalities`
--

CREATE TABLE `nationalities` (
  `id` int(11) NOT NULL,
  `nationality` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nationalities`
--

INSERT INTO `nationalities` (`id`, `nationality`, `created_at`, `updated_at`) VALUES
(1, 'Afghan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(2, 'Albanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(3, 'Algerian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(4, 'American', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(5, 'Andorran', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(6, 'Angolan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(7, 'Argentinian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(8, 'Armenian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(9, 'Australian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(10, 'Austrian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(11, 'Azerbaijani', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(12, 'Bahamian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(13, 'Bangladeshi', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(14, 'Barbadian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(15, 'Belgian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(16, 'Belorussian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(17, 'Beninese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(18, 'Bhutanese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(19, 'Bolivian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(20, 'Bosnian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(21, 'Brazilian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(22, 'British', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(23, 'Bruneian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(24, 'Bulgarian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(25, 'Burmese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(26, 'Burundian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(27, 'Cambodian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(28, 'Cameroonian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(29, 'Canadian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(30, 'Chadian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(31, 'Chilean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(32, 'Chinese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(33, 'Colombian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(34, 'Congolese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(35, 'Croatian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(36, 'Cuban', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(37, 'Cypriot', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(38, 'Czech', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(39, 'Dane', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(40, 'Dominican', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(41, 'Dutch', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(42, 'Ecuadorean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(43, 'Egyptian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(44, 'Eritrean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(45, 'Estonian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(46, 'Ethiopian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(47, 'Fijian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(48, 'Filipino', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(49, 'Finn', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(50, 'French', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(51, 'Gabonese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(52, 'Gambian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(53, 'Georgian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(54, 'German', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(55, 'Ghanaian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(56, 'Greek', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(57, 'Grenadian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(58, 'Guatemalan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(59, 'Guinean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(60, 'Guyanese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(61, 'Haitian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(62, 'Honduran', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(63, 'Hungarian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(64, 'Icelander', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(65, 'Indian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(66, 'Indonesian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(67, 'Iranian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(68, 'Iraqi', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(69, 'Irish', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(70, 'Israeli', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(71, 'Italian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(72, 'Jamaican', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(73, 'Japanese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(74, 'Jordanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(75, 'Kazakh', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(76, 'Kenyan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(77, 'Korean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(78, 'Kuwaiti', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(79, 'Laotian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(80, 'Latvian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(81, 'Lebanese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(82, 'Liberian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(83, 'Libyan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(84, 'Liechtensteiner', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(85, 'Lithuanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(86, 'Luxembourger', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(87, 'Macedonian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(88, 'Madagascan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(89, 'Malawian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(90, 'Malaysian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(91, 'Maldivian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(92, 'Malian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(93, 'Maltese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(94, 'Mauritanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(95, 'Mauritian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(96, 'Mexican', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(97, 'Moldovan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(98, 'Monacan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(99, 'Mongolian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(100, 'Montenegrin', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(101, 'Moroccan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(102, 'Mozambican', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(103, 'Namibian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(104, 'Nepalese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(105, 'Nicaraguan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(106, 'Nigerian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(107, 'Norwegian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(108, 'Pakistani', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(109, 'Panamanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(110, 'Paraguayan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(111, 'Peruvian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(112, 'Pole', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(113, 'Portuguese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(114, 'Qatari', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(115, 'Romanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(116, 'Russian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(117, 'Rwandan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(118, 'Salvadorian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(119, 'Saudi', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(120, 'Scot', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(121, 'Senegalese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(122, 'Serbian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(123, 'Singaporean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(124, 'Slovak', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(125, 'Slovenian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(126, 'Somali', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(127, 'Spaniard', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(128, 'Sri Lankan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(129, 'Sudanese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(130, 'Surinamese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(131, 'Swazi', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(132, 'Swede', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(133, 'Swiss', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(134, 'Syrian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(135, 'Tadzhik', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(136, 'Taiwanese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(137, 'Tanzanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(138, 'Thai', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(139, 'Togolese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(140, 'Trinidadian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(141, 'Tunisian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(142, 'Turk', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(143, 'Ugandan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(144, 'Ukrainian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(145, 'Uruguayan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(146, 'Uzbek', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(147, 'Venezuelan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(148, 'Vietnamese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(149, 'Welshman', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(150, 'Yemeni', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(151, 'Yugoslav', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(152, 'Zambian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(153, 'Zimbabwean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(154, 'Right', '2019-07-11 22:57:06', '2019-07-11 18:30:00'),
(155, 'Right', '2019-07-12 04:29:35', NULL),
(156, 'Left', '2019-07-12 04:29:35', NULL),
(157, 'Right', '2019-07-12 04:29:35', NULL),
(158, 'Right', '2019-07-12 04:29:35', NULL),
(159, 'Right', '2019-07-12 04:29:35', NULL),
(160, 'Right', '2019-07-12 04:29:35', NULL),
(161, 'Right', '2019-07-12 04:29:36', NULL),
(162, 'Right', '2019-07-12 04:29:36', NULL),
(163, 'Right', '2019-07-12 04:29:36', NULL),
(164, 'Right', '2019-07-12 04:29:36', NULL),
(165, 'Right', '2019-07-12 04:31:38', NULL),
(166, 'Left', '2019-07-12 04:31:38', NULL),
(167, 'Right', '2019-07-12 04:31:39', NULL),
(168, 'Right', '2019-07-12 04:31:39', NULL),
(169, 'Right', '2019-07-12 04:31:39', NULL),
(170, 'Right', '2019-07-12 04:31:39', NULL),
(171, 'Right', '2019-07-12 04:31:39', NULL),
(172, 'Right', '2019-07-12 04:31:39', NULL),
(173, 'Right', '2019-07-12 04:31:39', NULL),
(174, 'Right', '2019-07-12 04:31:39', NULL),
(175, 'Right', '2019-07-12 04:40:58', NULL),
(176, 'Left', '2019-07-12 04:40:58', NULL),
(177, 'Right', '2019-07-12 04:40:58', NULL),
(178, 'Right', '2019-07-12 04:40:58', NULL),
(179, 'Right', '2019-07-12 04:40:58', NULL),
(180, 'Right', '2019-07-12 04:40:58', NULL),
(181, 'Right', '2019-07-12 04:40:58', NULL),
(182, 'Right', '2019-07-12 04:40:58', NULL),
(183, 'Right', '2019-07-12 04:40:58', NULL),
(184, 'Right', '2019-07-12 04:40:58', NULL),
(185, 'Right', '2019-07-12 06:07:14', NULL),
(186, 'Left', '2019-07-12 06:09:49', NULL),
(187, 'Right', '2019-07-12 06:11:03', NULL),
(188, 'Right', '2019-07-12 06:11:38', NULL),
(189, 'Right', '2019-07-12 06:13:00', NULL),
(190, 'Right', '2019-07-12 06:16:18', NULL),
(191, 'Right', '2019-07-12 06:17:11', NULL),
(192, 'Right', '2019-07-12 06:17:52', NULL),
(193, 'Right', '2019-07-12 06:19:11', NULL),
(194, 'Right', '2019-07-12 06:24:01', NULL),
(195, 'Right', '2019-07-12 09:03:04', NULL),
(196, 'Right', '2019-07-12 09:03:16', NULL),
(197, 'Left', '2019-07-12 09:07:42', NULL),
(198, 'Left', '2019-07-12 09:08:39', NULL),
(199, 'Right', '2019-07-12 09:10:04', NULL),
(200, 'Right', '2019-07-12 09:11:11', NULL),
(201, 'Right', '2019-07-12 09:12:10', NULL),
(202, 'Right', '2019-07-12 09:22:35', NULL),
(203, 'Right', '2019-07-12 09:22:37', NULL),
(204, 'Right', '2019-07-12 09:28:23', NULL),
(205, 'Right', '2019-07-12 09:28:45', NULL),
(206, 'Left', '2019-07-12 09:32:47', NULL),
(207, 'Left', '2019-07-12 09:33:28', NULL),
(208, 'Right', '2019-07-12 09:35:01', NULL),
(209, 'Right', '2019-07-12 09:35:26', NULL),
(210, 'Right', '2019-07-12 09:35:37', NULL),
(211, 'Right', '2019-07-12 09:38:12', NULL),
(212, 'Right', '2019-07-12 09:44:18', NULL),
(213, 'Left', '2019-07-12 09:44:18', NULL),
(214, 'Right', '2019-07-12 09:44:18', NULL),
(215, 'Right', '2019-07-12 09:44:18', NULL),
(216, 'Right', '2019-07-12 09:44:18', NULL),
(217, 'Right', '2019-07-12 09:44:18', NULL),
(218, 'Right', '2019-07-12 09:44:18', NULL),
(219, 'Right', '2019-07-12 09:44:18', NULL),
(220, 'Right', '2019-07-12 09:44:18', NULL),
(221, 'Right', '2019-07-12 09:44:18', NULL),
(222, 'Right', '2019-07-12 09:46:44', NULL),
(223, 'Left', '2019-07-12 09:46:44', NULL),
(224, 'Right', '2019-07-12 09:46:44', NULL),
(225, 'Right', '2019-07-12 09:46:44', NULL),
(226, 'Right', '2019-07-12 09:46:44', NULL),
(227, 'Right', '2019-07-12 09:46:44', NULL),
(228, 'Right', '2019-07-12 09:46:44', NULL),
(229, 'Right', '2019-07-12 09:46:44', NULL),
(230, 'Right', '2019-07-12 09:46:44', NULL),
(231, 'Right', '2019-07-12 09:46:44', NULL),
(232, 'Right', '2019-07-12 12:22:33', NULL),
(233, 'Left', '2019-07-12 12:24:09', NULL),
(234, 'Right', '2019-07-12 12:24:52', NULL),
(235, 'Right', '2019-07-12 12:25:41', NULL),
(236, 'Right', '2019-07-12 12:26:43', NULL),
(237, 'Right', '2019-07-12 12:29:44', NULL),
(238, 'Right', '2019-07-12 12:31:00', NULL),
(239, 'Right', '2019-07-12 12:31:52', NULL),
(240, 'Right', '2019-07-12 12:33:42', NULL),
(241, 'Right', '2019-07-12 12:34:53', NULL),
(242, 'Left', '2019-07-12 12:37:22', NULL),
(243, 'Right', '2019-07-12 12:37:42', NULL),
(244, 'Right', '2019-07-12 12:39:10', NULL),
(245, 'Right', '2019-07-12 12:39:10', NULL),
(246, 'Left', '2019-07-12 12:40:45', NULL),
(247, 'Right', '2019-07-12 12:41:33', NULL),
(248, 'Left', '2019-07-12 12:41:42', NULL),
(249, 'Right', '2019-07-12 12:42:39', NULL),
(250, 'Right', '2019-07-12 12:45:16', NULL),
(251, 'Left', '2019-07-12 12:47:46', NULL),
(252, 'Right', '2019-07-12 12:56:45', NULL),
(253, 'Left', '2019-07-12 12:58:43', NULL),
(254, 'Right', '2019-07-12 12:59:35', NULL),
(255, 'Right', '2019-07-12 13:00:05', NULL),
(256, 'Right', '2019-07-12 13:01:46', NULL),
(257, 'Right', '2019-07-12 13:03:00', NULL),
(258, 'Right', '2019-07-12 13:03:26', NULL),
(259, 'Right', '2019-07-12 13:03:36', NULL),
(260, 'Left', '2019-07-12 13:04:56', NULL),
(261, 'Left', '2019-07-12 13:05:11', NULL),
(262, 'Right', '2019-07-12 13:05:32', NULL),
(263, 'Left', '2019-07-12 13:05:36', NULL),
(264, 'Right', '2019-07-12 13:06:01', NULL),
(265, 'Right', '2019-07-12 13:06:30', NULL),
(266, 'Right', '2019-07-12 13:06:37', NULL),
(267, 'Right', '2019-07-12 13:07:04', NULL),
(268, 'Right', '2019-07-12 13:07:33', NULL),
(269, 'Right', '2019-07-12 13:07:39', NULL),
(270, 'Right', '2019-07-12 13:11:04', NULL),
(271, 'Left', '2019-07-12 13:11:05', NULL),
(272, 'Right', '2019-07-12 13:11:06', NULL),
(273, 'Right', '2019-07-12 13:11:06', NULL),
(274, 'Right', '2019-07-12 13:11:07', NULL),
(275, 'Right', '2019-07-12 13:11:08', NULL),
(276, 'Right', '2019-07-12 13:11:09', NULL),
(277, 'Right', '2019-07-12 13:11:09', NULL),
(278, 'Right', '2019-07-12 13:11:10', NULL),
(279, 'Right', '2019-07-12 13:11:12', NULL),
(280, 'Right', '2019-07-12 13:11:13', NULL),
(281, 'Right', '2019-07-12 13:11:45', NULL),
(282, 'Left', '2019-07-12 13:11:46', NULL),
(283, 'Right', '2019-07-12 13:11:46', NULL),
(284, 'Right', '2019-07-12 13:11:47', NULL),
(285, 'Right', '2019-07-12 13:11:47', NULL),
(286, 'Right', '2019-07-12 13:11:48', NULL),
(287, 'Right', '2019-07-12 13:11:49', NULL),
(288, 'Right', '2019-07-12 13:11:49', NULL),
(289, 'Right', '2019-07-12 13:11:49', NULL),
(290, 'Right', '2019-07-12 13:11:51', NULL),
(291, 'Right', '2019-07-12 13:11:51', NULL),
(292, 'Right', '2019-07-12 13:12:14', NULL),
(293, 'Left', '2019-07-12 13:12:14', NULL),
(294, 'Right', '2019-07-12 13:12:15', NULL),
(295, 'Right', '2019-07-12 13:12:15', NULL),
(296, 'Right', '2019-07-12 13:12:15', NULL),
(297, 'Right', '2019-07-12 13:12:16', NULL),
(298, 'Right', '2019-07-12 13:12:17', NULL),
(299, 'Right', '2019-07-12 13:12:17', NULL),
(300, 'Right', '2019-07-12 13:12:17', NULL),
(301, 'Right', '2019-07-12 13:12:19', NULL),
(302, 'Right', '2019-07-12 13:12:20', NULL),
(303, 'Right', '2019-07-12 13:14:08', NULL),
(304, 'Left', '2019-07-12 13:14:09', NULL),
(305, 'Right', '2019-07-12 13:14:09', NULL),
(306, 'Right', '2019-07-12 13:14:09', NULL),
(307, 'Right', '2019-07-12 13:14:10', NULL),
(308, 'Right', '2019-07-12 13:14:11', NULL),
(309, 'Right', '2019-07-12 13:14:12', NULL),
(310, 'Right', '2019-07-12 13:14:12', NULL),
(311, 'Right', '2019-07-12 13:14:13', NULL),
(312, 'Right', '2019-07-12 13:14:14', NULL),
(313, 'Right', '2019-07-12 13:14:15', NULL),
(314, 'Right', '2019-07-12 13:20:04', NULL),
(315, 'Right', '2019-07-12 13:20:09', NULL),
(316, 'Left', '2019-07-12 13:21:41', NULL),
(317, 'Left', '2019-07-12 13:21:42', NULL),
(318, 'Right', '2019-07-12 13:21:57', NULL),
(319, 'Right', '2019-07-12 13:22:15', NULL),
(320, 'Right', '2019-07-12 13:22:25', NULL),
(321, 'Right', '2019-07-12 13:22:47', NULL),
(322, 'Right', '2019-07-12 13:22:50', NULL),
(323, 'Left', '2019-07-12 13:23:37', NULL),
(324, 'Right', '2019-07-12 13:24:16', NULL),
(325, 'Right', '2019-07-12 13:24:25', NULL),
(326, 'Right', '2019-07-12 13:25:40', NULL),
(327, 'Right', '2019-07-12 13:26:02', NULL),
(328, 'Right', '2019-07-12 13:29:06', NULL),
(329, 'Left', '2019-07-12 13:31:13', NULL),
(330, 'Right', '2019-07-12 13:32:14', NULL),
(331, 'Right', '2019-07-12 13:32:45', NULL),
(332, 'Right', '2019-07-13 04:45:20', NULL),
(333, 'Right', '2019-07-13 04:45:26', NULL),
(334, 'Left', '2019-07-13 04:46:55', NULL),
(335, 'Left', '2019-07-13 04:47:21', NULL),
(336, 'Right', '2019-07-13 04:47:39', NULL),
(337, 'Right', '2019-07-13 04:48:06', NULL),
(338, 'Right', '2019-07-13 04:48:06', NULL),
(339, 'Right', '2019-07-13 04:48:45', NULL),
(340, 'Right', '2019-07-13 04:49:04', NULL),
(341, 'Right', '2019-07-13 04:50:03', NULL),
(342, 'Right', '2019-07-13 04:52:39', NULL),
(343, 'Right', '2019-07-13 04:52:45', NULL),
(344, 'Left', '2019-07-13 04:54:14', NULL),
(345, 'Left', '2019-07-13 04:54:22', NULL),
(346, 'Right', '2019-07-13 04:54:46', NULL),
(347, 'Right', '2019-07-13 04:55:00', NULL),
(348, 'Right', '2019-07-13 04:55:06', NULL),
(349, 'Right', '2019-07-13 04:55:29', NULL),
(350, 'Right', '2019-07-13 04:55:33', NULL),
(351, 'Left', '2019-07-13 04:56:10', NULL),
(352, 'Right', '2019-07-13 04:56:53', NULL),
(353, 'Right', '2019-07-13 04:56:56', NULL),
(354, 'Right', '2019-07-13 04:57:09', NULL),
(355, 'Right', '2019-07-13 04:57:35', NULL),
(356, 'Right', '2019-07-13 04:58:46', NULL),
(357, 'Right', '2019-07-13 04:58:55', NULL),
(358, 'Right', '2019-07-13 04:59:01', NULL),
(359, 'Right', '2019-07-13 04:59:19', NULL),
(360, 'Right', '2019-07-13 05:00:29', NULL),
(361, 'Left', '2019-07-13 05:00:33', NULL),
(362, 'Left', '2019-07-13 05:00:45', NULL),
(363, 'Right', '2019-07-13 05:00:58', NULL),
(364, 'Right', '2019-07-13 05:01:22', NULL),
(365, 'Right', '2019-07-13 05:01:25', NULL),
(366, 'Right', '2019-07-13 05:01:34', NULL),
(367, 'Right', '2019-07-13 05:02:12', NULL),
(368, 'Right', '2019-07-13 05:02:14', NULL),
(369, 'Right', '2019-07-13 05:02:30', NULL),
(370, 'Right', '2019-07-13 05:02:31', NULL),
(371, 'Right', '2019-07-13 05:02:53', NULL),
(372, 'Right', '2019-07-13 05:03:10', NULL),
(373, 'Right', '2019-07-13 05:03:18', NULL),
(374, 'Right', '2019-07-13 05:04:02', NULL),
(375, 'Right', '2019-07-13 05:04:39', NULL),
(376, 'Right', '2019-07-13 05:04:40', NULL),
(377, 'Right', '2019-07-13 05:05:30', NULL),
(378, 'Left', '2019-07-13 05:06:03', NULL),
(379, 'Right', '2019-07-13 05:06:10', NULL),
(380, 'Left', '2019-07-13 05:06:17', NULL),
(381, 'Right', '2019-07-13 05:06:21', NULL),
(382, 'Right', '2019-07-13 05:06:52', NULL),
(383, 'Right', '2019-07-13 05:07:00', NULL),
(384, 'Right', '2019-07-13 05:07:04', NULL),
(385, 'Right', '2019-07-13 05:07:11', NULL),
(386, 'Right', '2019-07-13 05:07:22', NULL),
(387, 'Right', '2019-07-13 05:07:47', NULL),
(388, 'Right', '2019-07-13 05:07:50', NULL),
(389, 'Right', '2019-07-13 05:07:59', NULL),
(390, 'Right', '2019-07-13 05:08:36', NULL),
(391, 'Right', '2019-07-13 05:09:03', NULL),
(392, 'Right', '2019-07-13 05:09:23', NULL),
(393, 'Right', '2019-07-13 05:12:17', NULL),
(394, 'Right', '2019-07-13 05:12:46', NULL),
(395, 'Right', '2019-07-13 05:13:24', NULL),
(396, 'Right', '2019-07-13 05:13:45', NULL),
(397, 'Right', '2019-07-13 05:16:46', NULL),
(398, 'Left', '2019-07-13 05:18:28', NULL),
(399, 'Right', '2019-07-13 05:19:19', NULL),
(400, 'Right', '2019-07-13 05:20:06', NULL),
(401, 'Right', '2019-07-13 05:21:52', NULL),
(402, 'Right', '2019-07-13 05:24:32', NULL),
(403, 'Right', '2019-07-13 05:25:34', NULL),
(404, 'Right', '2019-07-13 05:26:03', NULL),
(405, 'Right', '2019-07-13 05:28:31', NULL),
(406, 'Right', '2019-07-13 05:31:51', NULL),
(407, 'Right', '2019-07-13 05:33:29', NULL),
(408, 'Right', '2019-07-13 06:11:38', NULL),
(409, 'Right', '2019-07-13 06:16:42', NULL),
(410, 'Right', '2019-07-13 06:17:00', NULL),
(411, 'Left', '2019-07-13 06:18:20', NULL),
(412, 'Left', '2019-07-13 06:18:26', NULL),
(413, 'Right', '2019-07-13 06:19:07', NULL),
(414, 'Right', '2019-07-13 06:19:41', NULL),
(415, 'Right', '2019-07-13 06:19:52', NULL),
(416, 'Right', '2019-07-13 06:23:17', NULL),
(417, 'Right', '2019-07-13 06:23:23', NULL),
(418, 'Left', '2019-07-13 06:24:57', NULL),
(419, 'Left', '2019-07-13 06:25:07', NULL),
(420, 'Right', '2019-07-13 06:25:40', NULL),
(421, 'Right', '2019-07-13 06:25:56', NULL),
(422, 'Right', '2019-07-13 06:30:14', NULL),
(423, 'Left', '2019-07-13 06:32:11', NULL),
(424, 'Right', '2019-07-13 06:33:01', NULL),
(425, 'Right', '2019-07-13 06:33:47', NULL),
(426, 'Right', '2019-07-13 06:35:05', NULL),
(427, 'Right', '2019-07-13 06:37:35', NULL),
(428, 'Right', '2019-07-13 06:38:37', NULL),
(429, 'Right', '2019-07-13 06:39:05', NULL),
(430, 'Right', '2019-07-13 06:40:17', NULL),
(431, 'Right', '2019-07-13 06:43:15', NULL),
(432, 'Right', '2019-07-13 06:44:26', NULL),
(433, 'Left', '2019-07-13 06:56:07', NULL),
(434, 'Right', '2019-07-13 06:56:25', NULL),
(435, 'Right', '2019-07-13 06:56:47', NULL),
(436, 'Right', '2019-07-13 06:57:09', NULL),
(437, 'Right', '2019-07-13 06:59:13', NULL),
(438, 'Right', '2019-07-13 07:07:20', NULL),
(439, 'Right', '2019-07-13 07:10:42', NULL),
(440, 'Right', '2019-07-13 07:13:28', NULL),
(441, 'Right', '2019-07-13 07:18:51', NULL),
(442, 'Right', '2019-07-13 07:19:23', NULL),
(443, 'Right', '2019-07-13 07:20:38', NULL),
(444, 'Right', '2019-07-13 07:24:47', NULL),
(445, 'Right', '2019-07-13 07:28:27', NULL),
(446, 'Right', '2019-07-13 07:35:54', NULL),
(447, 'Right', '2019-07-13 07:36:39', NULL),
(448, 'Ambidextrous', '2019-07-13 07:37:35', NULL),
(449, 'Right', '2019-07-13 07:38:43', NULL),
(450, 'Right', '2019-07-13 07:40:31', NULL),
(451, 'Right', '2019-07-13 08:26:13', NULL),
(452, 'Right', '2019-07-13 08:27:14', NULL),
(453, 'Right', '2019-07-13 08:29:46', NULL),
(454, 'Right', '2019-07-13 08:30:00', NULL),
(455, 'Right', '2019-07-13 08:35:57', NULL),
(456, 'Right', '2019-07-13 08:37:38', NULL),
(457, 'Right', '2019-07-13 08:37:39', NULL),
(458, 'Right', '2019-07-13 08:39:36', NULL),
(459, 'Right', '2019-07-13 08:41:07', NULL),
(460, 'Right', '2019-07-13 08:43:44', NULL),
(461, 'Right', '2019-07-13 08:45:35', NULL),
(462, 'Right', '2019-07-13 08:46:32', NULL),
(463, 'Right', '2019-07-13 08:47:28', NULL),
(464, 'Right', '2019-07-13 08:48:34', NULL),
(465, 'Right', '2019-07-13 08:48:55', NULL),
(466, 'Right', '2019-07-13 08:53:17', NULL),
(467, 'Right', '2019-07-13 08:53:55', NULL),
(468, 'Right', '2019-07-13 08:54:19', NULL),
(469, 'Right', '2019-07-13 08:55:09', NULL),
(470, 'Ambidextrous', '2019-07-13 08:57:10', NULL),
(471, 'Right', '2019-07-13 08:57:34', NULL),
(472, 'Right', '2019-07-13 09:00:41', NULL),
(473, 'Right', '2019-07-13 09:01:33', NULL),
(474, 'Right', '2019-07-13 09:02:48', NULL),
(475, 'Right', '2019-07-13 09:07:11', NULL),
(476, 'Right', '2019-07-13 09:12:07', NULL),
(477, 'Right', '2019-07-13 09:12:44', NULL),
(478, 'Right', '2019-07-13 09:12:58', NULL),
(479, 'Right', '2019-07-13 09:15:57', NULL),
(480, 'Right', '2019-07-13 09:17:36', NULL),
(481, 'Right', '2019-07-13 09:18:37', NULL),
(482, 'Right', '2019-07-13 09:20:00', NULL),
(483, 'Right', '2019-07-13 09:22:27', NULL),
(484, 'Right', '2019-07-13 09:22:40', NULL),
(485, 'Right', '2019-07-13 09:28:56', NULL),
(486, 'Right', '2019-07-13 09:30:48', NULL),
(487, 'Right', '2019-07-13 09:30:48', NULL),
(488, 'Right', '2019-07-13 09:33:10', NULL),
(489, 'Right', '2019-07-13 09:35:05', NULL),
(490, 'Right', '2019-07-13 09:37:13', NULL),
(491, 'Right', '2019-07-13 09:38:49', NULL),
(492, 'Right', '2019-07-13 09:39:50', NULL),
(493, 'Right', '2019-07-13 09:40:40', NULL),
(494, 'Right', '2019-07-13 09:41:43', NULL),
(495, 'Right', '2019-07-13 09:41:59', NULL),
(496, 'Right', '2019-07-13 09:46:17', NULL),
(497, 'Right', '2019-07-13 09:46:48', NULL),
(498, 'Right', '2019-07-13 09:47:09', NULL),
(499, 'Right', '2019-07-13 09:48:02', NULL),
(500, 'Ambidextrous', '2019-07-13 09:50:04', NULL),
(501, 'Right', '2019-07-13 09:50:24', NULL),
(502, 'Right', '2019-07-13 09:53:02', NULL),
(503, 'Right', '2019-07-13 09:54:36', NULL),
(504, 'Right', '2019-07-13 09:55:37', NULL),
(505, 'Right', '2019-07-13 09:59:34', NULL),
(506, 'Right', '2019-07-13 10:03:31', NULL),
(507, 'Right', '2019-07-13 10:04:04', NULL),
(508, 'Right', '2019-07-13 10:04:13', NULL),
(509, 'Right', '2019-07-13 10:06:56', NULL),
(510, 'Right', '2019-07-13 10:08:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `performer_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `notes`, `performer_id`, `created_at`, `updated_at`) VALUES
(1, '<p>test</p>', 3, '2019-07-11 23:05:10', '2019-07-12 01:50:17'),
(2, NULL, 7, '2019-07-11 23:30:13', '2019-07-12 00:06:37'),
(3, '<p>GAURAV</p>', 5, '2019-07-11 23:59:14', '2019-07-12 00:04:20');

-- --------------------------------------------------------

--
-- Table structure for table `nudity`
--

CREATE TABLE `nudity` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nudity_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nudity`
--

INSERT INTO `nudity` (`id`, `nudity_name`, `created_at`, `updated_at`) VALUES
(1, 'Full Body', NULL, NULL),
(2, 'Incomplete', NULL, NULL),
(3, 'Full Frontal', NULL, NULL),
(4, 'Back Only', NULL, NULL),
(5, 'Chest Only', NULL, NULL),
(6, 'Implied', NULL, NULL),
(7, 'Simuilated Sex (Hetero)', NULL, NULL),
(8, 'Simulated Sex (Bisexual)', NULL, NULL),
(9, 'Simulated Sex (Same Sex)', NULL, NULL),
(10, 'Nonea', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `nudity_perfomers`
--

CREATE TABLE `nudity_perfomers` (
  `id` int(10) UNSIGNED NOT NULL,
  `performer_id` int(10) UNSIGNED DEFAULT NULL,
  `nudity_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('developer.foremost@gmail.com', '$2y$10$8qTjOo3qeicapdWwcJu5uuTIeFgmNDuReHCX9/mn6jDVN7HbS7zKy', '2019-07-03 07:38:43'),
('gaurav@gmail.com', '$2y$10$WMyjtKE7R1fSsCrhkiYJ5OGLpgxBCgmovA2aOpy9mXxrLxl/p3Hb6', '2019-07-12 03:09:16');

-- --------------------------------------------------------

--
-- Table structure for table `performers`
--

CREATE TABLE `performers` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middleName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postalCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` enum('male','female','transgender') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'male',
  `applicationfor` enum('Adult','Child','Teen') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Adult',
  `instagram` text COLLATE utf8mb4_unicode_ci,
  `twitter` text COLLATE utf8mb4_unicode_ci,
  `linkedin` text COLLATE utf8mb4_unicode_ci,
  `facebook` text COLLATE utf8mb4_unicode_ci,
  `imdb_url` text COLLATE utf8mb4_unicode_ci,
  `agent_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `union_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `union_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `union_det` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload_primary_url` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `performers`
--

INSERT INTO `performers` (`id`, `firstName`, `middleName`, `lastName`, `email`, `password`, `phonenumber`, `address`, `city`, `province`, `postalCode`, `country`, `date_of_birth`, `gender`, `applicationfor`, `instagram`, `twitter`, `linkedin`, `facebook`, `imdb_url`, `agent_name`, `union_name`, `union_number`, `agent`, `union_det`, `upload_primary_url`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Abidali', NULL, 'Dhabra', 'admin@gmail.com', '$2y$10$hTzdn3oCW6R3Vj10zoav6.soMAw32Nidvvwbs0KBQ.HmnM3XyBkCa', '(989) 890-8017', 'At & post - Badarpur , Ta-Vadnagar', NULL, NULL, NULL, NULL, '2019-02-07', 'male', 'Adult', 'asd', 'asdasd', 'asdasd', 'asd', NULL, 'asdasd', 'asda', 'asdad', NULL, NULL, NULL, NULL, '2019-07-13 00:57:22', '2019-07-13 00:57:22'),
(2, ' Crystal', 'Ann', 'Tremblay ', 'tapadera@shaw.ca', '$2y$10$wWULIEomCaS9HosXP14CO.oC/01kailmU/aRVabEwMzVDYqA/t.uW', '6043169338', 'Box 662 ', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1969-11-10', 'female', 'Adult', NULL, 'TREMBLAY ,  CRYSTAL ANN', NULL, 'CrystL Ann', NULL, '', '', NULL, 'No', 'Yes', '95450D90-F49B-44EB-95B6-D5CFCEA44461.jpeg', NULL, NULL, '2019-07-13 00:59:50'),
(3, ' Gregory', 'Matthew', 'Coltman', 'darkone021@gmail.com', '$2y$10$8q3JLCciNQINvAe2pGrVLOZuPDfGM0U24v12kiNcwfP1EUrsMVLyS', '7789892634', '#20 17097 64th ave', 'SURREY', 'BC', 'V3S 1Y5', 'Canada', '1974-04-20', 'male', 'Adult', NULL, 'COLTMAN,  GREGORY MATTHEW', NULL, NULL, NULL, '', '', NULL, 'Yes', 'Yes', NULL, NULL, NULL, NULL),
(4, 'Aaliyah', 'Layne', 'Seidlitz', 'aaliyah.layne.seidlitz@gmail.com', '$2y$10$fTUVaMUGzlRPt8Mj8gWBW.vmCvkUmGX9ERJZHVgN932fXIjmNVEQm', '2505896482', '1154 Mason St #6', 'Victoria', 'BC', 'V8T 1A6', 'Canada', '2003-11-19', 'female', 'Teen', 'aaliyah.seidlitz', 'SEIDLITZ, AALIYAH LAYNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190501_122510.jpg', NULL, NULL, '2019-07-13 01:00:20'),
(6, 'Aaliyah ', NULL, 'Lamirand ', 'jl-rose-86@hotmail.com', '$2y$10$0UvSbBv9OKFp5ZmjG4pM9.WYM.ppgqMr9y6NkVR3tBUJd4yY0kksy', '6048326701', '113 scowlitz access road', 'Lake errock', 'BC', 'V0M1N0 ', 'Canada', '2010-06-13', 'female', 'Child', NULL, 'LAMIRAND , AALIYAH', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190320_221535.jpg', NULL, NULL, '2019-07-13 01:01:45'),
(7, 'Aanya', NULL, 'Sethi', 'karishma.sethi@hotmail.com', '$2y$10$qbVWUOqOC.YqebeMGw0xMOp6dMeGaJ1icBMEX/ZWKijVNiBmNPfzq', '2508881285', '526 gurunank lane', 'Victoria ', 'BC', 'V9c 0M2', 'Canada', '2012-10-23', 'female', 'Child', NULL, 'SETHI, AANYA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '99553287-618C-4845-8FA4-29E23828E04A.jpeg', NULL, NULL, '2019-07-13 01:02:19'),
(8, 'Aaron', 'Richard,Victor', 'Callihoo', 'aaroncallihoo@yahoo.com', '$2y$10$HX0t1alBLQO0YJ.YjXDMfOx4ipEBeGGXjQWHGHZDcHl6V5BwoUN6y', '2508994720', '3639 woodland dr ', 'Port coquitlam ', 'BC', 'V3b4r5', 'Canada', '1982-07-08', 'male', 'Adult', NULL, 'CALLIHOO, AARON RICHARD,VICTOR', NULL, 'Aaron Callihoo', NULL, '', '', NULL, 'No', 'No', '1B4DE5CC-2C8D-456B-A695-A8F3D798CEF6.jpeg', NULL, NULL, '2019-07-13 01:02:44'),
(9, 'Aaron', 'Konosuke', 'Takahashi', 'terucanada@hotmail.com', '$2y$10$y.GgnnHYKVh.hFLGSPK7ZOI02wcMbexS0.1y9GScvLyj6y/faEI4i', '6047602535', '11058 148A street', 'Surrey', 'BC', 'V3R 3Z4', 'Canada', '2007-09-24', 'male', 'Child', NULL, 'TAKAHASHI, AARON KONOSUKE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'F0D7FC7C-C472-4216-8018-F1C3FDC24971.jpeg', NULL, NULL, '2019-07-13 01:03:11'),
(10, 'Aaron', 'Michael', 'Gelowitz', 'aarongelowitz@shaw.ca', '$2y$10$W8w1Me8srijwQ355C3JS3Of3evAtOTeKWtJvw9.gDjeL6cEXni/om', '6047990084', '45513 Market Way, Apt 113', 'Chilliwack', 'BC', 'V2R6A5', 'Canada', '1980-09-19', 'male', 'Adult', NULL, 'GELOWITZ, AARON MICHAEL', NULL, NULL, NULL, '', '', NULL, 'No', 'Yes', 'smile.jpg', NULL, NULL, '2019-07-13 01:03:51'),
(11, 'Aaron', 'Joshua ', 'Guenther ', 'aaron.guenther@hotmail.com', '$2y$10$BPtvmS8DtvIzcE59IEoqeO6L13jYJg/tgWspKobphNGrlFp5CATri', '2509455218', '1057 Edgehill place', 'Kamloops', 'BC', 'V2C 0G6', 'Canada', '1994-12-29', 'male', 'Adult', 'Aaronguenther22 ', 'GUENTHER , AARON JOSHUA', NULL, 'Aaron Guenther ', NULL, '', '', NULL, 'No', 'No', '14E84286-3F50-44AB-B464-37C8DEED5E73.jpeg', NULL, NULL, '2019-07-13 01:04:04'),
(12, 'Abbie', NULL, 'Maslin', 'abbiemaslin399@gmail.com', '$2y$10$krmbHp2wh7M5SJ5J5ZvhjOBbejJ4vVuulUjW6psTlUrNtxO7LCYXu', '6043169925', '6841 Centennial Ave', 'Agassiz', 'BC', 'V0M1A3', 'Canada', '1992-10-26', 'female', 'Adult', NULL, 'MASLIN, ABBIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_4263.jpg', NULL, NULL, '2019-07-13 01:04:22'),
(13, 'Abby', 'Natsuko', 'Kobayakawa', 'abkob@shaw.ca', '$2y$10$T5lXKZSwZUbOltBYjl3tUucK1ug6u31xCk0aSJ.hPpAsi892Tp/.K', '6048386511', '4950 Buxton Street', 'Burnaby', 'BC', 'V5H 1J5', 'Canada', '1958-07-24', 'female', 'Adult', NULL, 'KOBAYAKAWA, ABBY NATSUKO', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '601EFD52-3136-4EDC-A38A-2C0246775459.jpeg', NULL, NULL, '2019-07-13 01:05:14'),
(14, 'Abby', 'Louise', 'Lamb', 'rtlamb@shaw.ca', '$2y$10$6U8ggjJboqayHn15h7Fe1uyqe3lrcq8Oe2LsGZ23jZik6JvTtDra6', '7782312556', '2813 Mara Drive', 'Coquitlam ', 'BC', 'V3c5t9 ', 'Canada', '2005-06-16', 'female', 'Teen', NULL, 'LAMB, ABBY LOUISE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '62EC0676-52E1-4565-8626-6B6DC1141518.jpeg', NULL, NULL, '2019-07-13 01:05:50'),
(16, 'Abdourahmane', NULL, 'Wane', 'yvewane7991@gmail.com', '$2y$10$uTaD.Sq4uSe7.FylqPNsyObV71B3EcgJuK9WbK2Jzi/zXMZBw9DI2', '7788751616', '214-1990 west 6th ave. ', 'VANCOUVER', 'BC', 'V6J 4V4', 'Canada', '1972-10-02', 'male', 'Adult', NULL, 'WANE, ABDOURAHMANE', NULL, 'Ab Wane', NULL, '', '', NULL, 'Yes', 'Yes', 'IMG_0836.jpeg', NULL, NULL, '2019-07-13 01:07:12'),
(17, 'Abigayle ', 'Shirley', 'Kotyk', 'trm52002@yahoo.ca', '$2y$10$XNMFfj2vkSmoHM/SS7K5B.EH1VXHV3fmTyNweH2uT0GhPtuAq6Ws2', '604-506-7070', '541 Hodgson Rd ', 'Williams Lake ', 'BC', 'V2G 3P8', 'Canada', '2010-09-25', 'transgender', 'Child', NULL, 'KOTYK, ABIGAYLE  SHIRLEY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(18, 'Adam', NULL, 'Fedyk', 'AFMonarch@gmail.com', '$2y$10$acDx/KI0YJWCZLpXab9GG.FidyAiCCBp8d20jkG3tkcSBAebWZYrq', '604-358-2994', '1748 E 12th Ave', 'Vancouver', 'BC', 'V5N 2A5', 'Canada', '1988-04-12', 'female', 'Adult', 'https://www.instagram.com/adam.virtue/', 'FEDYK, ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Adam-Fedyk-Headshot-Smiling.jpg', NULL, NULL, '2019-07-13 01:07:39'),
(19, 'Adam', 'Michael', 'Barker', 'adamwheelman@gmail.com', '$2y$10$xzCbjr8.e92NU1xtJkA3UO4NRQvvrZ9E0Bg8P9f.oH7z4BeF0fO0.', '6049383596', '966 maple st', 'Whiterock', 'BC', 'V4b4m5', 'Canada', '1985-02-24', 'male', 'Adult', NULL, 'BARKER, ADAM MICHAEL', NULL, NULL, NULL, '', '', NULL, 'No', 'Yes', '14DFE901-DDEA-499D-85EB-11BB9E7E60C3.jpeg', NULL, NULL, '2019-07-13 01:08:05'),
(20, 'Adam', 'Evan', 'Brodie', 'adambrodie@ymail.com', '$2y$10$yjrw70MhlU2fZmEooEhuyOc5X7//oewvlFRCop.SF5Ywh.L1ZNOky', '6045183900', '65521 Dogwood Drive', 'Hope', 'BC', 'V0X 1L1 ', 'Canada', '1982-08-25', 'male', 'Adult', NULL, 'BRODIE, ADAM EVAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '170290_10150352152455154_4075676_o.jpg', NULL, NULL, '2019-07-13 01:08:21'),
(21, 'Adam', NULL, 'Muxlow', 'maddmux@gmail.com', '$2y$10$LuzFfQ7Bc/gKWKgQGQJH3uiOyTGqGh/H2uJ/nDQWOeWY/SJaXB16W', '7783447053', '34250 Fraser street', 'Abbotsford', 'BC', 'V2s1x9', 'Canada', '1981-09-15', 'male', 'Adult', NULL, 'MUXLOW, ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(22, 'Adam', NULL, 'Boughanmi', 'boughanmi@gmail.com', '$2y$10$FaBMrywb1q4IUuOfoGaTb.gex0sLlb5Uv1a/Xrb1Aad/K5ISWInfG', '6044401085', '1603-1199 Seymour Street', 'Vancouver', 'BC', 'V6B1k3', 'Canada', '1985-10-11', 'male', 'Adult', NULL, 'BOUGHANMI, ADAM', NULL, 'https://www.facebook.com/boughanmia', NULL, '', '', NULL, 'Yes', 'Yes', 'DSC00599.jpg', NULL, NULL, '2019-07-13 01:08:47'),
(23, 'Adam', 'Nicola Takeshi', 'Nishi', 'adamnishi93@gmail.com', '$2y$10$.jQ6o8LKdLAtGYCME/TkjuvEYae/sqSl8iHM65DJIqKvMZXcHTRDy', '6048375740', '3120 Regent Street', 'Richmond', 'BC', 'V7E 2M4', 'Canada', '1993-10-27', 'male', 'Adult', '@adamnishi', 'NISHI, ADAM NICOLA TAKESHI', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Adam-Linkedin-photo.png', NULL, NULL, '2019-07-13 01:09:47'),
(24, 'Adam', 'Omar', 'Ghomari', 'adamghomari@gmail.com', '$2y$10$BzaHcUr9NUC7JiEsD1baEOq07F.NKjC09i.Ai7kpsRdyJQFr0t1ie', '6044013690', '1705 Wallace Street', 'Vancouver', 'BC', 'V6R 4J7', 'Canada', '2000-03-10', 'male', 'Adult', 'https://www.instagram.com/adam_ghomari/?hl=en', 'GHOMARI, ADAM OMAR', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Headshot2.jpg', NULL, NULL, '2019-07-13 01:09:56'),
(25, 'Adam', NULL, 'Harris', 'adamharris_15@hotmail.com', '$2y$10$fDGgcCJuGdmAngTYTaxM0egF48hJZX5vwgXTZ748WZtwGZr9fx6ne', '2505722326', '924 glenshee place', 'Kamloops ', 'BC', 'V2e1k6', 'Canada', '1988-01-31', 'male', 'Adult', 'Adamharris15', 'HARRIS, ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'Yes', 'D90E0068-13E2-4DE3-96DC-312C93820FE3.jpeg', NULL, NULL, '2019-07-13 01:10:21'),
(26, 'Adam ', NULL, 'Brown ', 'M.adam.brown87@gmail.com', '$2y$10$5vVf74Dsuoe78odLA6bumewoCcfGHQAJIAP7YrQmtJMeweXNyE5IW', '2892009903', '1408, 1147 quadra St', 'Victoria', 'BC', 'V8W 2K5', 'Canada', '1987-07-24', 'male', 'Adult', NULL, 'BROWN , ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Adam-20190304-050.jpg', NULL, NULL, '2019-07-13 01:10:34'),
(27, 'Addison', 'Daen', 'Sawatzky', 'brentsawa@hotmail.com', '$2y$10$3KSCjWESSjoYh237uyeWnOpKW23ZBat2Ob/F9CbDw2/85ovXRSTKO', '6047981458', '2083 Aberdeen Drive', 'Agassiz', 'BC', 'V0M 1A1', 'Canada', '2009-05-21', 'female', 'Child', NULL, 'SAWATZKY, ADDISON DAEN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'image000000_21.jpg', NULL, NULL, '2019-07-13 01:10:41'),
(28, 'Addison', 'Lee', 'Pinsonneault', 'ml123456@gmail.com', '$2y$10$Bin07tl0ct3W9AT6AVwo1Oq9T5qHDGLYbYi9M5R8O2oob4sieu1fS', '2508186688', '771 Canterbury road', 'Victoria', 'BC', 'V8X3E4', 'Canada', '2006-08-14', 'female', 'Child', NULL, 'PINSONNEAULT, ADDISON LEE', NULL, 'https://www.facebook.com/mandy.lee.1650332', NULL, '', '', NULL, 'No', 'No', 'IMG_20190319_1208462.jpg', NULL, NULL, '2019-07-13 01:10:58'),
(29, 'Adeola', 'Phasilat', 'Mustapha', 'addypmustapha@gmail.com', '$2y$10$V0tUqIbyzi7TdhAfe8xlAOeorpZxZNBdhp7um8mgXhaJjOqqgq.BC', '6043381341', '201-3031 Kingsway', 'Vancouver', 'BC', 'V5R5J6', 'Canada', '1993-09-01', 'female', 'Adult', NULL, 'MUSTAPHA, ADEOLA PHASILAT', NULL, 'https://www.facebook.com/people/Addy-Mustapha/100015769728562', NULL, '', '', NULL, 'No', 'No', 'IMG_1638.jpg', NULL, NULL, '2019-07-13 01:11:42'),
(30, 'Adrean', NULL, 'MacDonald', 'Azilkie55@gmail.com', '$2y$10$cbp0IZqsrvKfSICwE.WO4OTKqPavsNxPMCjnagl1.e6wkpAMLBpL6', '6049975940', 'PO Box 23,', 'Lake Errock', 'BC', 'V0M 1N0', 'Canada', '1960-08-04', 'female', 'Adult', NULL, 'MACDONALD, ADREAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_20160519_101805.jpg', NULL, NULL, '2019-07-13 01:12:00'),
(31, 'Adrian', NULL, 'Heim', 'adrian.heim93@gmail.com', '$2y$10$lBzzFA87lnim8A4eJAhdweG/1Q7RWIXzlOWKXQdJljQYl0c3CWIPG', '8199939350', '5-547 Herald st. ', 'Victoria', 'BC', 'V8W 1S5', 'Canada', '1993-12-03', 'male', 'Adult', NULL, 'HEIM, ADRIAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_2244.jpg', NULL, NULL, '2019-07-13 01:12:21'),
(32, 'Adriana', 'Rachel', 'Depaschoal', 'missekko1@gmail.com', '$2y$10$DnTcJ/rXZhsz1hT/qHSSROh5KRgMOd/9bx3MZeoNlZGDXF27HUMzW', '7787235135', '3676 McRae Cresent ', 'Port Coquitlam ', 'BC', 'V3B4P1', 'Canada', '1981-12-07', 'female', 'Adult', 'Esquaredsquad ', 'DEPASCHOAL, ADRIANA RACHEL', NULL, 'Ekko lostnsoundz', NULL, '', '', NULL, 'No', 'No', 'IMG_1349.png', NULL, NULL, '2019-07-13 01:12:30'),
(33, 'Adriana', NULL, 'Laurent', 'adriana.seibt@gmail.com', '$2y$10$wVTAcOrfpBfS5fknd2aKJukIg7rp4rPRg12DqrGmuI7MS0KafhHfK', '2369987807', '3430 8th Ave W', 'Vancouver', 'BC', 'V6R 1Y5', 'Canada', '1994-05-17', 'female', 'Adult', 'adri.laurent', 'LAURENT, ADRIANA', 'https://www.linkedin.com/in/adriana-laurent-040b9011a/', NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_8563.jpg', NULL, NULL, '2019-07-13 01:13:18'),
(34, 'Adriana', NULL, 'Gomes ', 'getinspired.adriana@gmail.com', '$2y$10$vc0NtEs2V502SJ4cTkAvO.7thajsKNr7sMAKFjsiKxKjnhgYjALlW', '7789685402', '5- 2544 Cornwall Avenue', 'Vancouver', 'BC', 'V6K1C2', 'Canada', '1975-11-24', 'female', 'Adult', NULL, 'GOMES , ADRIANA', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', '6265AB9A-733E-48B5-9EA8-A341EEE3E8F5.jpeg', NULL, NULL, '2019-07-13 01:13:41'),
(35, 'Adriana ', NULL, 'Caldwell ', 'adricaldwell@gmail.com', '$2y$10$mBaY9qFCIqT89C4mJ2v3qOXKy.o4aShfiAGpKQTgcqx/.gaAD8Io2', '7782311292', '5829 booth ave ', 'Burnaby', 'BC', 'V5H3A9 ', 'Canada', '1985-05-16', 'female', 'Adult', 'Adricaldwell ', 'CALDWELL , ADRIANA', NULL, 'Adriana Caldwell ', 'Adriana Caldwell ', '', '', NULL, 'Yes', 'No', '20180406_162143.jpg', NULL, NULL, '2019-07-13 01:14:00'),
(36, 'Adriano ', 'Miranda ', 'Esteves', 'esteves.adriano@hotmail.com', '$2y$10$OgK3adS4FNSpaJGJkxVB2O.gncOkHb1B7v.yBD5eHf0Yo1LsxtRwy', '7788375655', '312-4355 Maywood', 'Burnaby', 'BC', 'V5H 2J8', 'Canada', '1981-06-06', 'male', 'Adult', 'Esteves81', 'ESTEVES, ADRIANO  MIRANDA', NULL, 'Esteves.adriano', NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(37, 'Aidan', NULL, 'Battley', 'aidanbattley@gmail.com', '$2y$10$PelkbTZF8ViJtgTiO0fCP.0.8rO5dkwazTI3isrthk7d1aNoLqKTW', '604-782-8443', '2274 - 179th st.', 'Surrey', 'BC', 'V3Z9V6', 'Canada', '1998-12-03', 'male', 'Adult', 'https://www.instagram.com/aidanbattley/?hl=en', 'BATTLEY, AIDAN', NULL, 'https://www.facebook.com/AidanBattleyPro/', 'https://www.imdb.com/name/nm9235298/', '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(41, 'Aiden', 'Colin', 'Hopkins', 'hoppyfam@yahoo.ca', '$2y$10$aSRx3frym2z6.Mdc0UViwOuapTamEG/sH5Er6QQiCmQCBR92ff2F2', '7786780210', '225 Lascelles Cres', 'Victoria', 'BC', 'V9C1C5', 'Canada', '2009-03-30', 'male', 'Child', NULL, 'HOPKINS, AIDEN COLIN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_20190430_222358.jpg', NULL, NULL, '2019-07-13 01:15:14'),
(42, 'Aileen ', 'Elizabeth ', 'Penner', 'aileenpen@gmail.com', '$2y$10$28ZpUeegUrTxk/Oq8xkh8ekchQ4Nw/XgDtMkqHKOtfdjzEX.Z/Cl6', '2502087582', 'P.O. Box 315 / 98 Rockwell Drive ', 'Harrison Hot Springs ', 'BC', 'V0M 1K0', 'Canada', '1975-04-05', 'female', 'Adult', 'pennerton', 'PENNER, AILEEN  ELIZABETH', NULL, 'https://www.facebook.com/aileen.penner', 'https://www.imdb.com/name/nm8355266/', '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(43, 'Akari', 'Lucille', 'Kobayashi', 'peah_honey@hotmail.com', '$2y$10$SydA6IQGH2xSvrYim2Sv/OqBxHv23Rc3/EzLewxPkNJ83JpiXaZd6', '604-230-0415', '4755 Moss Street', 'Vancouver', 'BC', 'V5R3T3', 'Canada', '2010-03-14', 'female', 'Child', NULL, 'KOBAYASHI, AKARI LUCILLE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_9289.jpg', NULL, NULL, '2019-07-13 01:16:13'),
(44, 'Akira ', NULL, 'Ito', 'iloveinter11@gmail.com', '$2y$10$FOejdSmj.7a2GKtmScOZP.skI3ncCiNgE/c8sXZT1G3uSg7TdaHZG', '7782227021', '92 GEIKIE STREET', 'Jasper ', 'AB', 'T0E 1E0', 'Canada', '1997-05-14', 'male', 'Adult', NULL, 'ITO, AKIRA', NULL, 'https://www.facebook.com/profile.php?id=100007690270648', NULL, '', '', NULL, 'No', 'No', 'image-9.jpg', NULL, NULL, '2019-07-13 01:16:47'),
(46, 'Alan', NULL, 'Yu', 'alan@alanjhyu.com', '$2y$10$dYT9pNUrfawgnj1smlrpeutjT/9wNMAG9N4/o0nce9IJRyJxdwykq', '6043158628', '212-5875 Imperial St', 'Burnaby', 'BC', 'V5J1G4', 'Canada', '1980-03-18', 'male', 'Adult', NULL, 'YU, ALAN', NULL, 'https://www.facebook.com/alanjhyu', 'http://www.imdb.com/name/nm3595948/', '', '', NULL, 'Yes', 'No', '3.jpg', NULL, NULL, '2019-07-13 01:23:09'),
(47, 'Alan', NULL, 'Jones', 'alanbjones@protonmail.com', '$2y$10$vAM3LioldsJ.XQFqlnGhQepjbrJdirscOFo6vpVBLzMna8FTtq.qO', '604-615-6027', '33428 Harbour Avenue', 'Mission', 'BC', 'V2V 2W4', 'Canada', '1954-10-05', 'male', 'Adult', NULL, 'JONES, ALAN', NULL, 'Alan Jones Mission BC', NULL, '', '', NULL, 'No', 'No', 'F82F3967-4B29-490B-8906-A2CBD8805FB2.jpeg', NULL, NULL, '2019-07-13 01:23:26'),
(48, 'Alan', 'Frank', 'Homer', 'alhomer56@gmail.com', '$2y$10$M7hiA9gCotLYBqbgFWjXo.njguVXJwwQj6EuBVeEuqYaDFTcqG.Qu', '2503181772', '760 Settlement Road ', 'Kamloops ', 'BC', 'V2b8c9 ', 'Canada', '1968-06-24', 'male', 'Adult', 'alan.homer', 'HOMER, ALAN FRANK', NULL, 'Alan Homer', NULL, '', '', NULL, 'No', 'No', 'F970BD9C-4F96-4C96-BE46-8B67C71BEAD0.jpeg', NULL, NULL, '2019-07-13 01:23:45'),
(49, 'Alana', 'Dawn', 'Der', 'sookiebooboosausage@gmail.com', '$2y$10$FVrWSUW2Lv/2QvzGMmw7Aegy0HHjtUYWbY1ZnbjmxpTSM0YVT35iW', '6043785267', 'Box 125', 'Boston Bar', 'BC', 'V0K1C0', 'Canada', '1975-05-25', 'female', 'Adult', NULL, 'DER, ALANA DAWN', NULL, 'Alana dawn ', NULL, '', '', NULL, 'No', 'No', '20180902_141520.jpg', NULL, NULL, '2019-07-13 01:25:18'),
(50, 'Alana ', 'Cosco', 'Richardson', 'alana-richardson@live.com', '$2y$10$aFKpMM6Ivh8iVfVDB45a.OFGX1r15SRjWUoMX3sKNhVsjFRK36QPS', '604-897-7973', '301 - 528 Como Lake Avenue', 'Coquitlam', 'BC', 'V3J3M1', 'Canada', '1991-10-17', 'female', 'Adult', NULL, 'RICHARDSON, ALANA  COSCO', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Alana-Richardson-Face-Close-Up.png', NULL, NULL, '2019-07-13 01:26:14'),
(51, 'Albert ', NULL, 'Chen', 'bertchen123@gmail.com', '$2y$10$au5DtJzml5i0ZY14y0NytOeBlmBj9BTkRvrbsS7LVzHxNbZ6V84c2', '7788225168', '4584 Rumble st', 'Burnaby', 'BC', 'V5J 2A5', 'Canada', '2000-10-11', 'male', 'Adult', NULL, 'CHEN, ALBERT', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Albert_face.jpg', NULL, NULL, '2019-07-13 01:26:27'),
(52, 'Alden', 'C. ', 'Mayfield', 'okyacm@yahoo.com', '$2y$10$hSBNHoZ19jFQAnjihW6HC.H61aSUM9OOU8v.XX4jXDYJ.VIKONPsK', '778-549-2384', '20053A 68th Avenue', 'Langley', 'BC', 'V2Y 0T5', 'Canada', '2018-08-23', 'male', 'Adult', NULL, 'MAYFIELD, ALDEN C.', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Mayfield.jpg', NULL, NULL, '2019-07-13 01:26:42'),
(53, 'Aleah', 'Hazel Rose', 'Klassen', 'kelly_klassen@shaw.ca', '$2y$10$deJp0Hr9VTbn8exa.LEctevdxUYk7mgALVJIU2K08FthCwEa5iN8a', '6046142025', '34602 Somerset ave', 'Abbotsford ', 'BC', 'V2S6M9 ', 'Canada', '2009-06-16', 'female', 'Child', NULL, 'KLASSEN, ALEAH HAZEL ROSE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '2C3FF14F-B540-4D8C-BAA4-395547535E31.jpeg', NULL, NULL, '2019-07-13 01:26:51'),
(54, 'Alejandra', NULL, 'Gadea  Lopez', 'ale.gl_08@hotmail.com', '$2y$10$gE5u5LwJuel9y8oWANkexelJgBC66xvy.7vqQHf0H5Cx5tMtQniL.', '6047263428', '6116 128st', 'Surrey', 'BC', 'v3x1t1', 'Canada', '1996-12-08', 'female', 'Adult', '@alejandra_xoxo', 'GADEA  LOPEZ, ALEJANDRA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Screen-Shot-2018-08-03-at-10.17.05-PM.png', NULL, NULL, '2019-07-13 01:27:12'),
(55, 'Alex', NULL, 'Millar', 'amjpd5@gmail.com', '$2y$10$Ad4jF2oaBTIg.ZJY4CmdvOYsaxhvR1lb/1BCa5MO/e6ULYn2QAwNO', '6048974646', '6241 Golf Road', 'Agassiz', 'BC', 'V0M 1A3', 'Canada', '2003-12-10', 'male', 'Teen', NULL, 'MILLAR, ALEX', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '1BA8CE4E-0191-4D6A-8C20-A832676830DE.jpeg', NULL, NULL, '2019-07-13 01:27:38'),
(56, 'Alexa', 'Rae', 'Ross', 'alexa4loans@aol.com', '$2y$10$qDKluCsWbN4A9FzTlfKdte.7G3BWgDB2JwGqX8o4.V/wscsSL0vD2', '778-240-2776', '29433 Silver Crescent', 'Mission', 'BC', 'V4S1J1', 'Canada', '1976-04-26', 'female', 'Adult', NULL, 'ROSS, ALEXA RAE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Alexa-Ross-1.jpg', NULL, NULL, '2019-07-13 01:28:46'),
(57, 'Alexa', NULL, 'Taylor', 'alexa_taylor@hotmail.com', '$2y$10$MDDdbPMjZM9p8gwJLpQANuEIWaYYMS9pIyUyKzklWwfErAQgX4emm', '7788668169', '108-2131 W 3rd Avenue', 'Vancouver', 'BC', 'V6K 1L3', 'Canada', '1989-09-18', 'female', 'Adult', NULL, 'TAYLOR, ALEXA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_20180622_164746_812-1.jpg', NULL, NULL, '2019-07-13 01:29:18'),
(58, 'Alexa', 'Nadine', 'Kortas', 'karen.winfield@gmail.com', '$2y$10$rqK1f7QlWOo7e2v/Iycbwu6l.p27GZaWP1D5kwz4WkVW7xBtE2sdy', '403-680-4820', '3541 Proudfoot Place', 'Victoria', 'BC', 'V9C 4L9', 'Canada', '2005-05-10', 'female', 'Teen', NULL, 'KORTAS, ALEXA NADINE', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'DSC_4718.jpg', NULL, NULL, '2019-07-13 01:29:38'),
(59, 'Alexander', 'Jay', 'Ross', 'aross8725@gmail.com', '$2y$10$5oHCyM1NSxorAFExs2foqu.i.JfUkx6Vr2wno9dhgJS9LIQ/d/0Ee', '6047868607', '23231 Aurora Place', 'Maple Ridge', 'BC', 'V2X 0J5', 'Canada', '1982-05-04', 'male', 'Adult', NULL, 'ROSS, ALEXANDER JAY', NULL, NULL, NULL, '', '', NULL, 'Yes', 'Yes', 'alex-nb-.jpg', NULL, NULL, '2019-07-13 01:29:53'),
(60, 'Alexander', NULL, 'Lee', 'alexsunlee@gmail.com', '$2y$10$ajqg4Eon2qZqjYysEZVkqutTz/yuFlLFpwiWNIsRu4lB4qnSIjhu6', '7789997756', '1040 Westmount Drive', 'Port Moody', 'BC', 'v3h1k9', 'Canada', '1969-01-03', 'male', 'Adult', NULL, 'LEE, ALEXANDER', NULL, NULL, NULL, '', '', NULL, 'Yes', 'Yes', 'sf_180907_0280.jpg', NULL, NULL, '2019-07-13 01:30:05'),
(61, 'Alexander ', 'Levi', 'Killam', 'irwin@baypub.ca', '$2y$10$UE5oddCYtHeS8PfzAQvbReNbwO6lmSxYTfX6oIPE0EpTocnBCN3mq', '12507100010', '1433 Highland place', 'Cobble Hill ', 'BC', 'V0R 1L3', 'Canada', '2008-05-11', 'male', 'Child', NULL, 'KILLAM, ALEXANDER  LEVI', NULL, NULL, 'Alexander Killam', '', '', NULL, 'Yes', 'Yes', NULL, NULL, NULL, NULL),
(62, 'Alexey', NULL, 'Gammer', 'alexeygammer@gmail.com', '$2y$10$12W.84NPHVKLEdPAIL/YfOJSTKYsWKjjZMr0f8h4iMXStfQdWiFVG', '250-486-0795', '113 Van Horne St', 'Penticton', 'BC', 'V2A 4K1', 'Canada', '1984-07-08', 'male', 'Adult', NULL, 'GAMMER, ALEXEY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'fmkam1-2.jpg', NULL, NULL, '2019-07-13 01:30:36'),
(63, 'Alexia ', 'Lynne', 'Bell', 'thesnooz@hotmail.com', '$2y$10$cstyhSpqlY1pBTTp33Jpb.kEio9M0f85MsrPPly/assqLSy8Rso5G', '2505747663', '47220 Vista Place', 'Chilliwack ', 'BC', 'V2R0R9 ', 'Canada', '2011-04-05', 'female', 'Child', NULL, 'BELL, ALEXIA  LYNNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '6450DE71-0BD3-463E-8298-A431B460C1E3.jpeg', NULL, NULL, '2019-07-13 01:30:58'),
(64, 'Alexis', NULL, 'Lawlor', 'hollykara81@gmail.com', '$2y$10$kSQgaDAMMXXxuFYRsTjCZeVxBu4nN9SjWAlV6rIZrSnL5mxY/qLdK', '6047938908', '5238 crimson ridge ', 'Chilliwack', 'BC', 'V2r5w7 ', 'Canada', '2009-08-28', 'female', 'Child', NULL, 'LAWLOR, ALEXIS', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '67FC4588-05F8-4EF7-8AE3-BB4EBA61C38F.jpeg', NULL, NULL, '2019-07-13 01:31:42'),
(65, 'Alexis ', NULL, 'MacRae', 'alexismmacrae@hotmail.com', '$2y$10$PQgZFU7haVB9rsfN2GlfeubwWJ9uKFKFoBrxphkjYFqoOVTZGE/7q', '7783192645', '2057 e1st avenue ', 'Vancouver ', 'BC', 'V5n1b6', 'Canada', '1990-11-29', 'female', 'Adult', 'Alexis.macrae', 'MACRAE, ALEXIS', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_1919.jpg', NULL, NULL, '2019-07-13 01:32:11'),
(66, 'Alice ', NULL, 'Kyle', 'bailey.ja@gmail.com', '$2y$10$ICJ3O35Y45YpMzsa5Vw8qeO1KBnicRQQk4nZUZKPERUaG1y7.Kr12', '2508181996', '2656 Bukin Dr E', 'Victoria ', 'BC', 'V9e1h4 ', 'Canada', '2010-12-24', 'female', 'Child', NULL, 'KYLE, ALICE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'C755D968-C0F9-4480-B5F6-EF7D06F363E9.jpeg', NULL, NULL, '2019-07-13 01:32:33'),
(67, 'Alicia', NULL, 'Chappell', 'Alicia.chappell86@gmail.com', '$2y$10$.dIFdYmr.1rSzRZj/Xk69ODizCd9XwoOnriFvRg8tk0NQ2ly6GKZW', '7786846009', '46158 stoneview drive, unit 1', 'Chilliwack BC', 'BC', 'V2R 5W8', 'Canada', '1986-07-07', 'female', 'Adult', 'Aliciaannechappell', 'CHAPPELL, ALICIA', NULL, 'https://m.facebook.com/alicia.chappell.773', NULL, '', '', NULL, 'No', 'No', 'E10276A4-1961-48FE-BF27-536D06363BE2.jpeg', NULL, NULL, '2019-07-13 01:33:37'),
(68, 'Alicia-Rae', 'Michelle', 'Borth', 'aliciaborth@yahoo.ca', '$2y$10$Ouf0Af24phWU5Ytswzbg2ujb8ZLxy8wA16BLO7vDNcHuZixXdL9Ee', '2508989423', '5733 North Island Highway', 'Courtenay', 'BC', 'V9j1t3', 'Canada', '1997-05-06', 'transgender', 'Adult', 'https://www.instagram.com/aliciaborth/', 'BORTH, ALICIA-RAE MICHELLE', 'https://www.linkedin.com/in/alicia-b-54571183/', NULL, NULL, '', '', NULL, 'No', 'No', '0E54AE77-C2D8-468A-B702-C3CB49309C3D.jpg', NULL, NULL, '2019-07-13 01:33:52'),
(69, 'Alisa', NULL, 'Gil Silvestre', 'alisa.silvestre@gmail.com', '$2y$10$xK4.Zq9yshPzjQXMmSfgl.tfK3gqFprDr4M.3t0FCf.dlJtrrwhBC', '6042402426', '5751 Cedarwood Street', 'burnaby', 'BC', 'v5g2k7', 'Canada', '1996-02-15', 'female', 'Adult', NULL, 'GIL SILVESTRE, ALISA', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'IMG_A106C45E246B-1.jpeg', NULL, NULL, '2019-07-13 01:33:59'),
(70, 'Aliza', NULL, 'Boszak', 'xaajboss@icloud.com', '$2y$10$m15..BPDyuVYGSDENigIMu198XeWzbgiLUaWtkDO/yunE/AF4p7CC', '2368884790', '2563 152 st', 'Surrey', 'BC', 'V4p1n4', 'Canada', '2004-03-07', 'female', 'Teen', NULL, 'BOSZAK, ALIZA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '7B343806-F697-4FAB-AF1D-195FF43F98DD.jpeg', NULL, NULL, '2019-07-13 01:34:17'),
(71, 'Allan', NULL, 'Luna', 'luna.allan@hotmail.com', '$2y$10$KPWRRQGIyUBWMRX7rubP9.iBlNUS.8ZTLLcLsn8Wy9U1Jiz4fchIq', '7788783418', '1696 W.', 'Vancouver', 'BC', 'v6p2v7', 'Canada', '1987-06-21', 'male', 'Adult', NULL, 'LUNA, ALLAN', NULL, NULL, NULL, '', '', NULL, 'Yes', 'Yes', 'thumbnail_IMG_1189.jpg', NULL, NULL, '2019-07-13 01:34:36'),
(72, 'Allison', NULL, 'Devereaux', 'allie.dev@gmail.com', '$2y$10$Mum/nYNErfqHxepeX259R.E0yTHJzWlqM6ykzxbY7IFsDiqt3WBdm', '604-838-6525', '804-1924 Barclay St', 'Vancouver', 'BC', 'V6G1L3', 'Canada', '1996-07-20', 'female', 'Adult', 'alliedevereaux', 'DEVEREAUX, ALLISON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'ADevereauxHS_NG.jpg', NULL, NULL, '2019-07-13 01:34:55'),
(73, 'Allison', NULL, 'Warren', 'AllyWarren@yahoo.com', '$2y$10$CBETLnqZt0ZEeDif7KqVNOnl.lm6bV/q7VdEMhxWAt/Rvl1e8a7ma', '6045124472', '1117-2012 Fullerton Avenue', 'North Vancouver', 'BC', 'V7P 3E3', 'Canada', '1967-12-09', 'female', 'Adult', 'screensiren', 'WARREN, ALLISON', 'hotmarkets', 'ally warren', 'IMDB.me/allywarren', '', '', NULL, 'Yes', 'Yes', 'Ally-Warren-Current-2018.jpg', NULL, NULL, '2019-07-13 01:35:21'),
(74, 'ALLISON (Ally)', NULL, 'WARREN ', 'AllyWarren@yahoo.com', '$2y$10$DNSUIHsu.vgHz.StGYdS6.kvFAsW7LrCnr.H4zxKTmbtWRK3KFxKq', '6045124472', '1117-2012 Fullerton Avenue', 'North Vancouver', 'BC', 'V7P 3E3', 'Canada', '1974-12-09', 'female', 'Adult', 'screensiren', 'WARREN , ALLISON (ALLY)', 'hotmarkets', 'ally warren', 'allywarren', '', '', NULL, 'No', 'Yes', 'AllyWarren-Mar-2018.png', NULL, NULL, '2019-07-13 01:35:44'),
(75, 'Ally', NULL, 'Warren', 'alleywarren@hotmail.com', '$2y$10$wsqwGJ.HSBlfFRsKmt3ONuj7DcW2LaJbGgTYIv6f.fKblEYSmYty.', '6045124472', '1117-2012 Fullerton Avenue', 'North Vancouver', 'BC', 'V7P3E3', 'Canada', '1978-12-09', 'female', 'Adult', NULL, 'WARREN, ALLY', NULL, NULL, 'imdb.me/allywarren', '', '', NULL, 'No', 'Yes', 'Ally-Warren-Current-2018.jpg', NULL, NULL, '2019-07-13 01:36:28'),
(76, 'Alycia', NULL, 'Escoval', 'escovalalycia@gmail.com', '$2y$10$VxWlT8177/qRr4Td/FN/zeyVTFpBzYoRP4E7LoJxF8OZ4aZFugUTe', '7786790552', '2835 Ronald RD ', 'Victoria', 'BC', 'v9b2l7', 'Canada', '1998-09-17', 'female', 'Adult', NULL, 'ESCOVAL, ALYCIA', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'AlyciaEscoval063_WEB.jpg', NULL, NULL, '2019-07-13 01:36:59'),
(77, 'Alysha', NULL, 'Seriani', 'alyshaseriani@gmail.com', '$2y$10$xmcv8n31mVNk.Chls5MTY.uq0F0avfykU2LaTX0lB3RByUy.oDwWq', '7782556591', '#405 - 2056 Franklin Street', 'Vancouver', 'BC', 'V5L1R3', 'Canada', '1994-03-07', 'female', 'Adult', 'http://instagram.com/alyseriani', 'SERIANI, ALYSHA', 'https://www.linkedin.com/in/alyshaseriani/', 'https://www.facebook.com/alyshaseriani', 'https://www.imdb.com/name/nm7032168/?ref_=nv_sr_2', '', '', NULL, 'No', 'No', '20180425_133708.jpg', NULL, NULL, '2019-07-13 01:37:29'),
(78, 'Alyssa', NULL, 'Yong', 'alyssayong@hotmail.com', '$2y$10$B4XKxp4avnhA8GkjgQayWOvDGFSwvml6vaCtM4n1ZpwEWWEmPXaAe', '7787880800', '#204-5733 Vine street', 'Vancouver', 'BC', 'V6M4A2', 'Canada', '1999-01-12', 'female', 'Adult', '@_alyssayong', 'YONG, ALYSSA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'AlyssaYong_Photo2.jpg', NULL, NULL, '2019-07-13 01:37:47'),
(79, 'Amadora', 'Kimberly', 'Neufeld', 'dustinmarks@hotmail.com', '$2y$10$LFrMTCTsDpFhCpl7j.kFBOyxfhDEbDyxBXuKB8wUzx6DcHhjAXVhu', '6043166469', 'PO box 858, 459 Naismith Avenue', 'Harrison Hot Springs', 'BC', 'V0M1K0', 'Canada', '2008-04-21', 'female', 'Child', NULL, 'NEUFELD, AMADORA KIMBERLY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '54364864_424206491667970_3421900994202566656_n.jpg', NULL, NULL, '2019-07-13 01:38:00'),
(80, 'Aman', NULL, 'Webeshet', 'amanwebelew@gmail.com', '$2y$10$qRZ2lGKZjiZmAEAKQOCBLeuE9ltgVYPZcpBjwZmFZnc.5xmtjygim', '778 708 6983', '3311 venables ', 'Vancouver', 'BC', 'v5k2s7', 'Canada', '1992-01-24', 'male', 'Adult', NULL, 'WEBESHET, AMAN', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'aman-webeshet-web_4949.jpg', NULL, NULL, '2019-07-13 01:38:27'),
(81, 'Amanda', NULL, 'Early', 'amandaearly78@gmail.com', '$2y$10$/SoaC15sOGolNUFpGmB6gubKRDPVs3GWkijTsRAq1KxcxetF4bpuu', '604-999-1897', '20757 90 Ave Langley', 'Langley', 'BC', 'V1M 2N4', 'Canada', '2000-02-06', 'female', 'Adult', 'amanda.early', 'EARLY, AMANDA', NULL, 'Amanda Early', NULL, '', '', NULL, 'Yes', 'No', 'amandaearly1.jpg', NULL, NULL, '2019-07-13 01:38:58'),
(82, 'Amanda', 'May', 'Klop', 'amandaklop1@gmail.com', '$2y$10$Tbth8c1dVhBYLM0u0Rdwfuqal/m31LVfU7KniW2tRJY2ztoyGF7ze', '6048190708', '4905 hunt rd ', 'Agassiz ', 'BC', 'V0m1a3', 'Canada', '1990-05-11', 'female', 'Adult', '@_amanda_klop', 'KLOP, AMANDA MAY', NULL, 'Amanda klop', NULL, '', '', NULL, 'No', 'No', '005BE3EF-1077-4C7B-9B07-DA6128AC970B.jpeg', NULL, NULL, '2019-07-13 01:40:31'),
(83, 'Amanda', 'Judy', 'Lac seul', 'amandalacseul93@gmail.com', '$2y$10$wWJj2gvVnh4tqKp0gFekzuN8UYc1h5pi50s7XQyqn5Ge9zycOptum', '+1 (306) 485-1470', '46179 princess ave ', 'Chilliwack', 'BC', 'V2p 4l9', 'Canada', '1993-03-18', 'female', 'Adult', 'Amandalacseul', 'LAC SEUL, AMANDA JUDY', NULL, 'Amanda Lacseul ', NULL, '', '', NULL, 'No', 'No', '98A9A6AA-552F-4084-9E72-FBE1EE4BD8D0.jpeg', NULL, NULL, '2019-07-13 01:40:38'),
(84, 'Amanda', 'Lea', 'Christie', 'a_manda.a@hotmail.com', '$2y$10$/T3A7bRbuQiutv766b6SKOXTf1..7VeTfnCKUSOqokDNh/K6Wa.0e', '6043165778', '1747 garden place ', 'Agassiz', 'BC', 'V0m 1a2', 'Canada', '1991-02-28', 'female', 'Adult', 'aamandachristie', 'CHRISTIE, AMANDA LEA', 'https://www.linkedin.com/feed/', 'Amanda Christie', NULL, '', '', NULL, 'No', 'No', 'E1B56772-D94B-423E-91F0-10A31C7B8790.jpeg', NULL, NULL, '2019-07-13 01:40:44'),
(85, 'Amanda', 'Joan', 'Ned', 'eyes_life@hotmail.com', '$2y$10$F0jyiJRUa3K9v80vcJn7.ufN6vEMQm/RHSDh1t7mKSNAe3pYmMuPC', '7786844882', '205-9245 Edwards Street', 'Chilliiwack', 'BC', 'V2P4C5', 'Canada', '1982-10-08', 'male', 'Adult', 'Am7anda', 'NED, AMANDA JOAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '18077072_10154264894335881_5702663079757369986_o.jpg', NULL, NULL, '2019-07-13 01:40:57'),
(86, 'Amanda', NULL, 'Hendrickson', 'amycfn@gmail.com', '$2y$10$AWKnyguiKAtfdwc3EfKzbelRwqMFbER574uhbNNuolLqLzkH5sgqe', '6048605808', 'PO box 2239', 'Hope', 'BC', 'V0X 1L0', 'Canada', '1981-02-12', 'female', 'Adult', NULL, 'HENDRICKSON, AMANDA', NULL, 'Amy Peters Hendrickson ', NULL, '', '', NULL, 'No', 'No', '3C71AD4E-7FC3-4671-8B74-D00D144C25CF.jpeg', NULL, NULL, '2019-07-13 01:41:07'),
(88, 'Amanda ', 'Taylor ', 'Jones ', 'atjones@quadro.net', '$2y$10$S0FB6etlMv350/nCNK9F6u4upUnMFtohGrkhIkMPnnCGb0.QkgxCK', '519-282-1343', '7600 Glover Rd', 'Langley ', 'BC', 'V2Y 1Y1', 'Canada', '1999-09-09', 'female', 'Adult', 'a_jones 9', 'JONES , AMANDA  TAYLOR', NULL, 'Amanda Jones ', NULL, '', '', NULL, 'No', 'No', 'IMG_3116-copy.jpg', NULL, NULL, '2019-07-13 01:42:18'),
(89, 'Amandine', NULL, 'Bidgood', 'mand.b@me.com', '$2y$10$mHbE5bUVqD1HImBtMrge8O/FncKOjFOQgD93bHRQDFBBgP7.u2jcG', '7782235410', '3404-833 Seymour St', 'Vancouver', 'BC', 'V6B 0G4', 'Canada', '1973-03-14', 'female', 'Adult', NULL, 'BIDGOOD, AMANDINE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '5G7A9106r.jpg', NULL, NULL, '2019-07-13 01:43:16'),
(90, 'Amaris', NULL, 'Marks', 'meredith.holly@icloud.com', '$2y$10$YWo3kz96d9DhjAS/0Httju1NCVoieYjSnbUgeE5TDoXQ18X5ehF6C', '2505883778', '1345 Neild Rd', 'Metchosin', 'BC', 'V9C4H4', 'Canada', '2009-01-23', 'female', 'Child', 'Tribeofmarks', 'MARKS, AMARIS', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(91, 'amarpreet ', 'Singh', 'chhina', 'amarpreetchhina@yahoo.in', '$2y$10$0CNY2V.GpcnEPvIH7Sq0POs2/CzighreuyyhXGLTBMTmWTkEHcalu', '604-613-6768', '12979 74 Avenue', 'surrey', 'BC', 'V3W 1C3', 'Canada', '1994-04-06', 'male', 'Adult', '@amarpreetchhina@amuchhina', 'CHHINA, AMARPREET  SINGH', NULL, 'chhinaamarpreet', NULL, '', '', NULL, 'No', 'No', 'Amar_first-selects_NickThiessen-311-of-319.jpg', NULL, NULL, '2019-07-13 01:43:30'),
(92, 'Amber', 'Erica', 'Webber', 'amberlightning@gmail.com', '$2y$10$jO8zfeickzQRE7SUUI7CyewiD6L8Pz0VdYuG6vacZROWcI1KtkbM.', '16048081629', '#11 - 1575 east 1st ave', 'vancouver', 'BC', 'v5n1a6', 'Canada', '1980-06-18', 'male', 'Adult', '@amberlightning', 'WEBBER, AMBER ERICA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'unnamed.png', NULL, NULL, '2019-07-13 01:43:58'),
(93, 'Amber', 'Natashia', 'Petersen', 'art@amberpetersen.ca', '$2y$10$C3WhfKfjHOMQpmOkqpJuDuulEwYzBhvtTpXkoQ5HbeebkQTdz2/lS', '7789771110', '12835 Gulfview Road, PO Box 103', 'Madeira Park', 'BC', 'V0N 2H0', 'Canada', '1983-03-11', 'female', 'Adult', 'http://www.instagram.com/ambernp', 'PETERSEN, AMBER NATASHIA', NULL, 'http://www.facebook.com/ambernatashiapetersen', 'http://www.imdb.me/amberpetersen', '', '', NULL, 'No', 'No', 'Amber-Petersen-headshot.jpg', NULL, NULL, '2019-07-13 01:44:23'),
(94, 'Amber', NULL, 'Tauber', 'amber.justine.tauber@gmail.com', '$2y$10$e.C9.TFpLnhHD9vyWyDQzeiXAjfJRKIAPlQrqUqKPfFdg4OtqQlPy', '6049911511', '9 9493 Broadway st ', 'Chilliwack', 'BC', 'V2p 5t8', 'Canada', '1990-09-30', 'female', 'Adult', NULL, 'TAUBER, AMBER', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'FB_IMG_1551814012677.jpg', NULL, NULL, '2019-07-13 01:45:06'),
(95, 'Amber', NULL, 'Lote', 'amberlote@gmail.com', '$2y$10$JOSBXc5t/gf4l6n0WfUJFu9VjmkP0AHOpFXDKv4WJh86reaj3Hpdu', '2507449230', '2120 Tanlee Crescent', 'Saanichton', 'BC', 'V8M 1M8', 'Canada', '1989-11-16', 'female', 'Adult', '@theyogapixie', 'LOTE, AMBER', NULL, 'Amber Lote', NULL, '', '', NULL, 'No', 'No', '20245546_10158943397130005_8009042789014940340_n.jpg', NULL, NULL, '2019-07-13 01:45:15'),
(96, 'Ambrose', NULL, 'Gardener', 'ambrose.c.gardener@hotmail.com', '$2y$10$iNXwEShvvxFC.J9vAROf7.Kx1i9eVl8b6njAiNRX0be.Oz5ni7y.S', '604 440 4518', '102-1110 Davie St', 'Vancouver', 'BC', 'V6E 1N1', 'Canada', '1992-03-19', 'male', 'Adult', 'https://www.instagram.com/ambrose_gardener/', 'GARDENER, AMBROSE', NULL, 'https://www.facebook.com/AmbroseChristopher', 'https://www.imdb.com/name/nm7563410/?ref_=fn_al_nm_1', '', '', NULL, 'Yes', 'No', 'HS-Smile.jpg', NULL, NULL, '2019-07-13 01:45:50'),
(97, 'Amelia', NULL, 'Croft', 'gilliancroft@icloud.com', '$2y$10$VQPWoei34shw4YJtm4o5IOyn14ocwBK2Y9ltkmcZ5itcs4w.1iI3W', '(250) 858-2789 Texts picked up frequently.', '2065 Pauls Terrace', 'Victoria', 'BC', 'V8N 2Z4', 'Canada', '2004-12-29', 'female', 'Teen', NULL, 'CROFT, AMELIA', NULL, 'https://www.facebook.com/AmeliaCroftFanPage/', 'https://pro.imdb.com/name/nm6249350?ref_=nm_nv_mp_profile', '', '', NULL, 'Yes', 'No', 'AmeliaCroft05202019PrimCloseupCOMP.jpg', NULL, NULL, '2019-07-13 01:47:25'),
(98, 'Amelie ', 'Leah Marie', 'French', 'hellokittyninja@gmail.com', '$2y$10$sooIJigqVDq40DstviXfzugkCUjB.OhgdcJMZSJn197aeXV0U2BVC', '6047814262', '1119 Jolivet Cres.', 'Victoria', 'BC', 'V8X 3P3', 'Canada', '2009-02-02', 'female', 'Child', NULL, 'FRENCH, AMELIE  LEAH MARIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_1401-2.jpg', NULL, NULL, '2019-07-13 01:48:31'),
(99, 'Amie', NULL, 'Sokugawa', 'amiesokugawa@gmail.com', '$2y$10$zx2xyRXoxkHAyHvdCg0Nbe2/cUyjuBF8.MtIE8RUGL5aY8L7nm7L6', '6049619277', '325 Alberta Street', 'New Westminster', 'BC', 'V3L 3J4', 'Canada', '1999-08-22', 'female', 'Adult', NULL, 'SOKUGAWA, AMIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_0151.jpg', NULL, NULL, '2019-07-13 01:48:53'),
(100, 'Amira', NULL, 'Anderson', 'amira.marseilles@gmail.com', '$2y$10$i1v6zj0F3.HHpMrrQJJtYeLp6Ws/3LHE0XONv4VrBPtvroTxAj.0G', '2896874882', '183 East Georgia St', 'Vancouver', 'BC', 'V6A 0E5', 'Canada', '1995-07-14', 'female', 'Adult', '@amira.m.anderson', 'ANDERSON, AMIRA', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'AmiraAndersonHeadshot2.jpg', NULL, NULL, '2019-07-13 01:49:01'),
(101, 'Ana', NULL, 'Arciniega', 'arciniega.anapao@gmail.com', '$2y$10$5VXDV38tOKNfWT2bVHndYOuMIlWk1kiqmr7P.v/59rFSDwpKyyxi.', '7787141912', '2885 Capilano Road', 'North Vancouver', 'BC', 'V7R4H4', 'Canada', '1997-01-01', 'female', 'Adult', 'Anaarciniega', 'ARCINIEGA, ANA', NULL, 'Aarciniegat', NULL, '', '', NULL, 'No', 'No', 'IMG_6043.jpg', NULL, NULL, '2019-07-13 01:49:26'),
(102, 'Andrea', NULL, 'Mayo', 'victorandrea2016@gmail.com', '$2y$10$ljMKAaUnDFaNpZmxdCb7ZOfoUbDcJ1u91KGJ6unWkFzoLT3Ki4Sde', '7785226296', '6204 Logan Lane', 'Vancouver', 'BC', 'V6T2K9', 'Canada', '1984-09-13', 'female', 'Adult', NULL, 'MAYO, ANDREA', 'https://www.linkedin.com/in/andreamayo/', NULL, 'https://m.imdb.com/name/nm4328243/', '', '', NULL, 'No', 'No', '20180623_154415.jpg', NULL, NULL, '2019-07-13 01:49:43'),
(103, 'Andrea', 'Catherine', 'Cummings', 'andreacummings02@hotmail.com', '$2y$10$gvxWzPtGoqX4boVDDtwVe.95sgw/LTnjMHjvpmOFqWldLKv7Xs42W', '7788924342', '#308 4363 Halifax Street', 'Burnaby', 'BC', 'v5c 5z3', 'Canada', '2018-06-11', 'female', 'Adult', 'a.cmmngs', 'CUMMINGS, ANDREA CATHERINE', NULL, 'cummingsandrea', NULL, '', '', NULL, 'No', 'No', 'IMG_0126-Copy.jpg', NULL, NULL, '2019-07-13 01:50:05'),
(104, 'Andrea', 'Leah', 'Punt', 'andreapunt@hotmail.ca', '$2y$10$8roPWw48jkTwKeCs8WE3Y.slYirL56jNg3G9bdLvkezJyKCYAN4i6', '6046799340', '794 Upper Crescent', 'Britannia beach', 'BC', 'V0N1J0', 'Canada', '1984-07-20', 'female', 'Adult', NULL, 'PUNT, ANDREA LEAH', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '24198E50-DA76-4F08-BBCD-363779D8C822.jpeg', NULL, NULL, '2019-07-13 01:50:32'),
(105, 'Andrea', 'Michelle', 'MacSween', 'andreamack_10@hotmail.com', '$2y$10$.ktk3mK8gbGaB6.RDiBHmeTJbif//pX63hRVatxWlypxnQyqn1SRu', '9028801124', '5474 McCallum Road', 'Agassiz', 'BC', 'V0m1a1 ', 'Canada', '1987-09-21', 'female', 'Adult', 'Andimichelle1021', 'MACSWEEN, ANDREA MICHELLE', NULL, 'Andrea Michelle ', NULL, '', '', NULL, 'No', 'No', '215D24D9-6B7C-4A7F-9CC0-9402A373C432.jpeg', NULL, NULL, '2019-07-13 01:50:40'),
(106, 'Andrea', NULL, 'Aidoo', 'andrea1023@live.com', '$2y$10$a1VJly3iAvYlftyykW5qNeVwO9rC./HUk6Kl38GwAXSbjiRMJD6fq', '7788613504', '1021 st andrews st', 'new westminister', 'BC', 'v3m1w4', 'Canada', '2001-10-06', 'female', 'Adult', 'https://www.instagram.com/andrea.aidoo/', 'AIDOO, ANDREA', NULL, 'https://www.facebook.com/andrea.aidoo.3', NULL, '', '', NULL, 'No', 'No', 'IMG_3122.jpg', NULL, NULL, '2019-07-13 01:50:49'),
(107, 'Andrej', NULL, 'Krestan', 'krexpro@gmail.com', '$2y$10$/j1.b14QEwgHC.Dm1.8vEeM1vvm9ZFkRQsCDyCFAKh4YVK9X0u2z2', '12508868629', '140-6171 willingdon ave', 'burnaby', 'BC', 'v5h2t9', 'Canada', '1989-12-15', 'male', 'Adult', 'andrej_wsb', 'KRESTAN, ANDREJ', 'Andrej Krestan', 'Andrej Krestan', 'https://m.imdb.com/name/nm8091833/?ref=m_nv_sr_1', '', '', NULL, 'Yes', 'Yes', '667FB7C5-D215-4FE7-A253-99333488D12B.jpeg', NULL, NULL, '2019-07-13 01:51:03'),
(112, 'Andrew', 'Steven', 'Mykichuk', 'andrewmykichuk@gmail.com', '$2y$10$Kupu.Q6OIuA8FrLbgULf9OkcVPfLQHuiU5OS79ydZ/J.wmRnwAZt6', '7789967025', '1530 Mariners Walk', 'Vancouver', 'BC', 'V6J4X9', 'Canada', '1983-07-25', 'male', 'Adult', NULL, 'MYKICHUK, ANDREW STEVEN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '2921820F-E6CA-4062-BA67-D591EF1F2050.jpeg', NULL, NULL, '2019-07-13 01:54:07'),
(113, 'Andrew', 'Patrick', 'Bourque', 'andrewbourque92@gmail.com', '$2y$10$k12bRoFPewHM/Hflyvp34.VEEpLwOnOpKTGxZ96a3CrLQrXYHa.hG', '15147734022', '3875 west 4th avenue', 'Vancouver', 'BC', 'V6R 4H8', 'Canada', '1992-11-24', 'male', 'Adult', NULL, 'BOURQUE, ANDREW PATRICK', 'https://www.linkedin.com/in/andrew-patrick-bourque/', 'https://www.facebook.com/andrew.bourque.39', NULL, '', '', NULL, 'Yes', 'No', 'photo-5.jpg', NULL, NULL, '2019-07-13 01:54:49'),
(114, 'Andrew', 'David', 'Derbitsky', 'aderbitsky@gmail.com', '$2y$10$SoRX9WVc180S0MyZHhXnzeBJpbsoTZZk1zGQJ0SFnGi4g/wC7P7w6', '6047890814', '#30 460 W16th Ave', 'Vancouver', 'BC', 'V5Y 1Z3', 'Canada', '1987-12-28', 'male', 'Adult', NULL, 'DERBITSKY, ANDREW DAVID', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'face-shot.png', NULL, NULL, '2019-07-13 01:55:43'),
(115, 'Andrew', NULL, 'Pilcher', 'inversight@gmail.com', '$2y$10$bj15zN1IqNoJ3vJt9kxbsOy.N7R7BebziZk/CvUpjeWPd35AG7Cl6', '236 986 8565', '1110 Cloverley Street ', 'North Vancouver', 'BC', 'V7L 1N6', 'Canada', '1982-06-26', 'male', 'Adult', NULL, 'PILCHER, ANDREW', 'https://www.linkedin.com/in/andrew-pilcher-58a199116/', 'https://www.facebook.com/andrew.pilcher.180', 'https://www.imdb.com/name/nm7765942/?ref_=nv_sr_1', '', '', NULL, 'No', 'No', 'IMG_1279.jpg', NULL, NULL, '2019-07-13 01:56:24'),
(116, 'Andrew', NULL, 'Wang', 'andrew-wang@outlook.com', '$2y$10$O/2SJ3hliZerTXTeA6BXf.yTPFjybcAu4Gfons4nQAscVL7.vQKDe', '6047005216', '150-8473 163 St', 'Surrey', 'BC', 'V4N 6M7', 'Canada', '1997-12-19', 'male', 'Adult', 'aw_fmuf', 'WANG, ANDREW', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_2734.jpg', NULL, NULL, '2019-07-13 01:57:11'),
(118, 'Andrew', 'William', 'Jorgensen', 'r031kona@gmail.com', '$2y$10$lAil9PYsUcpV4./ZtCbk4eKrTIV8WxvCeCoxJkd83N87jrDl80S2e', '2505710138', '133 Knox st', 'Kamloops', 'BC', 'V2B 3Z1', 'Canada', '1961-12-28', 'male', 'Adult', NULL, 'JORGENSEN, ANDREW WILLIAM', NULL, 'Andy Jorgensen', NULL, '', '', NULL, 'No', 'No', 'Andrew-Jorgensen4.jpg', NULL, NULL, '2019-07-13 01:57:57'),
(119, 'Andrew ', 'Michael ', 'Beha', 'andrewbeha@msn.com', '$2y$10$5YpPLdYQJqxXHD5Yxa0GmOmVLPnjLsbsihxTaxGZvavCc5XItiVQC', '7024454649', '1495 Richards st #1508', 'Vancouver ', 'BC', 'V6z3e3', 'Canada', '1978-05-10', 'male', 'Adult', 'www.instagram.com/andrewbeha', 'BEHA, ANDREW  MICHAEL', NULL, 'www.facebook.com/andrewbeha', 'www.imbd.me/andrewbeha', '', '', NULL, 'Yes', 'Yes', '51C9A2F6-9CF6-47AB-BA08-BAEF03239F4B.jpeg', NULL, NULL, '2019-07-13 01:58:17'),
(121, 'Andy', NULL, 'Vu', 'andyvu92@gmail.com', '$2y$10$uPu0Qz2jjSNOcwcpxJtYrOun/8QmqVysHENmvOjgB8r2vnzFSL4aC', '6046006850', '4733 Bruce street', 'Vancouver', 'BC', 'V5N3Z7', 'Canada', '1999-07-17', 'male', 'Adult', 'https://www.facebook.com/andyvuu17', 'VU, ANDY', NULL, 'https://www.instagram.com/andyvu17/?hl=en', NULL, '', '', NULL, 'No', 'No', 'b90ec4767e6b9f7e3484d49247153922.png', NULL, NULL, '2019-07-13 01:58:38'),
(122, 'andy', NULL, 'choe', 'andychoe94@gmail.com', '$2y$10$ggdrbRZXsJMyr/SQRjeXFuc1BnNEhD0GIAK.FbtuGJ1H16AsPG.Di', '6043458256', '#910 3438 vanness avenue', 'vancouver', 'BC', 'v5r6e7 ', 'Canada', '1994-09-12', 'male', 'Adult', 'Alloftheabove_____', 'CHOE, ANDY', NULL, 'andy chan ok choe', NULL, '', '', NULL, 'No', 'No', '41674090-E1E2-4C2D-AA5A-053643CB99FB.jpeg', NULL, NULL, '2019-07-13 01:59:14'),
(123, 'Angela', 'Maria', 'Negrin Ibbott', 'ibbottangela@gmail.com', '$2y$10$ZYt2AzzK2y/QxYTrACr8f.VL.AN5J2U0x9RnB3cS74rxytxM5obey', '7789945552', '3282 Allan Rd', 'North Vancouver', 'BC', 'V7J 3C5', 'Canada', '1999-12-02', 'female', 'Adult', 'johnwaynegacyofficial', 'NEGRIN IBBOTT, ANGELA MARIA', 'Annie Negrin', NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_0285.jpg', NULL, NULL, '2019-07-13 01:59:31'),
(124, 'Angela', NULL, 'Knight', 'angela22knight@gmail.com', '$2y$10$bRTsV1nF8FW8XqYK0pxuze3s6VKR81lwapywRQ0HcTk0HaTiSPkbO', '778 384 8466', '224-5600 Andrews Road', 'Richmond', 'BC', 'V7E 6N1', 'Canada', '1967-06-19', 'female', 'Adult', NULL, 'KNIGHT, ANGELA', 'https://ca.linkedin.com/in/angela-r-knight-00ba2043', NULL, 'https://www.imdb.com/name/nm1862808/', '', '', NULL, 'Yes', 'No', 'angela-headshot-copy.jpg', NULL, NULL, '2019-07-13 01:59:58'),
(125, 'Angela', NULL, 'Dolomont', 'ADolomont@shaw.ca', '$2y$10$SA1L6l5sXzddZ/6Wd/OHM.4zLX73FMOe28dOmavzCGE4P7XKR3.xO', '604-785-2878', 'B461 - 603 Hot Springs Road', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1973-10-26', 'male', 'Adult', NULL, 'DOLOMONT, ANGELA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_1844.jpg', NULL, NULL, '2019-07-13 02:00:44'),
(129, 'Angelina', NULL, 'Chizh', 'angelchizh@hotmail.com', '$2y$10$yCussmzhzWTptOh.kEFVku7QP8xBN3RmYQ7Osu6AQeBzuu8OXkvEa', '4388700213', '1372 Seymour st', 'Vancouver', 'BC', 'V6B0L1', 'Canada', '1998-07-14', 'female', 'Adult', NULL, 'CHIZH, ANGELINA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Screen-Shot-2019-06-20-at-10.52.39-AM.png', NULL, NULL, '2019-07-13 02:05:34'),
(130, 'Angie', NULL, 'Wojciechowska ', 'angie@off-leash.ca', '$2y$10$b66oo8/QSwI07ktfqKIwhupDraUmayHFHLnXXojhlZinIZgjTB28q', '60161822236', '1402-850 Royal Ave', 'New Westminster ', 'BC', 'V3M 1A6', 'Canada', '1970-11-10', 'female', 'Adult', 'Angieoffleash', 'WOJCIECHOWSKA , ANGIE', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', '40A54AB0-F078-4F32-9058-79804CCBF23B.jpeg', NULL, NULL, '2019-07-13 02:05:50'),
(131, 'Angie', 'Linda', 'Wangler', 'angiewangler@gmail.com', '$2y$10$u0.JL5iemzt4YoJaWUqAxO2LsqoEF4kk45mlBW6ZmAH0n0cKmhNFy', '604.704.7622', '814 Jackson Ave', 'Vancouver', 'BC', 'V6A 3C1', 'Canada', '1982-03-04', 'female', 'Adult', 'https://www.instagram.com/angiewangler/', 'WANGLER, ANGIE LINDA', NULL, 'https://www.facebook.com/angie.wangler.5', NULL, '', '', NULL, 'No', 'No', 'Angie-headshot-1.jpg', NULL, NULL, '2019-07-13 02:05:57'),
(133, 'Anika', NULL, 'Bird', 'asbird04@gmail.com', '$2y$10$4zgW9VPdZG/jmqPC5UeBFe3/vPF8WRHH6FN2h1uO8KCfLotSMi9v.', '6047997374', 'Box 73', 'Agassiz ', 'BC', 'V0M1A0', 'Canada', '2004-01-11', 'transgender', 'Teen', NULL, 'BIRD, ANIKA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190317_224259.jpg', NULL, NULL, '2019-07-13 02:07:42'),
(134, 'Ann', NULL, 'Stokes', 'keepityellow@hotmail.com', '$2y$10$NBIVUQfGW8woC5tcWmWB2.4FfOYUePxLamyYHIeM3W30RCx/zOLoC', '6044424256', '1605-1331 Alberni street', 'Vancouver', 'BC', 'V6E4S1', 'Canada', '1983-10-07', 'female', 'Adult', NULL, 'STOKES, ANN', NULL, 'https://m.facebook.com/ann.chan.37201?refid=8', NULL, '', '', NULL, 'Yes', 'Yes', 'Screenshot_20180502-063406-2.png', NULL, NULL, '2019-07-13 02:08:00'),
(135, 'Ann', NULL, 'Johnstone ', 'annjohnstone1@hotmail.com', '$2y$10$9PJcsWmBhtLESTyvnB55mueHoCyUvyYA.IreibvWX0568dr26sPMi', '6043162893', '6983 Kalyna Dr', 'Agassiz ', 'BC', 'V0M1A3', 'Canada', '1958-06-23', 'male', 'Adult', NULL, 'JOHNSTONE , ANN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '3249B9ED-A7C6-4FFD-A7B7-597879A4FF0D.jpeg', NULL, NULL, '2019-07-13 02:08:28'),
(136, 'Ann Margaret', NULL, 'Mortel', 'annmortel@hotmail.com', '$2y$10$ELNpcrNAC57/ySYR/YWUgur7NeEH6TTKzqLYWI5E9/FCgcdnzTwKG', '7788472316', '7453 Culloden St', 'Vancouver', 'BC', 'V5x4k3', 'Canada', '1987-01-16', 'female', 'Adult', 'ann_margaret16', 'MORTEL, ANN MARGARET', NULL, 'https://www.facebook.com/profile.php?id=734154866', NULL, '', '', NULL, 'No', 'No', '3512D266-1E1D-41F1-8E5B-F33B84CCBFBE.jpeg', NULL, NULL, '2019-07-13 02:08:51'),
(137, 'Anna', NULL, 'Wheeler', 'jesse.annamarie@gmail.com', '$2y$10$CSHHj9YWbtP9Y9d9OGQsqe8lb9g95tQpiPG5vEFF8bThCQq3pGcAC', '6047024644', '1636 Vimy Rd ', 'Agassiz ', 'BC', 'V0m1a2', 'Canada', '1987-05-19', 'female', 'Adult', '@wheelerhaus ', 'WHEELER, ANNA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'NFK_Profile-10.jpg', NULL, NULL, '2019-07-13 02:09:26'),
(138, 'Anna', NULL, 'Lee', 'climbingmom1@gmail.com', '$2y$10$arf50t7/PpCqks3.x5cMjeln4yWB6UUthKfWHqKui8yvXkReKy.mK', '604 754 7987', '252 E 17th Street', 'North Vancouver', 'BC', 'V7L 2V7', 'Canada', '1960-04-27', 'female', 'Adult', 'climbingmom1', 'LEE, ANNA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Anna-Headshot.jpg', NULL, NULL, '2019-07-13 02:09:35'),
(139, 'Anna', 'Helena', 'Terebka', 'aterebka@hotmail.com', '$2y$10$NV6C8JJRpKQfGgxlLMcFj.bl4Wwh7pZ7q0hknViTNQoS7p4LI81/K', '2504885529', '2011 Green Mountain Rd.', 'PENTICTON', 'BC', 'V2A0E6', 'Canada', '1981-12-14', 'female', 'Adult', '@atreebook', 'TEREBKA, ANNA HELENA', NULL, 'Anna Terebka', 'https://l.facebook.com/l.php?u=https%3A%2F%2Fpro.imdb.com%2Fname%2Fnm9744729%3Fs%3D22b5d80b-0575-83d7-02ea-14b6c1bd2b25%26fbclid%3DIwAR18qxgLsXObH2dWK7PpKpVsWshONabCsSNrTBIspOpcX7uiKduJh5Yyn3c&h=AT3UJb7sUO-LiLjuOcD0FVcEo0e0UGL4aUCdVElyVuKi76JwJ3PO4DFObnu4Qfwvq6DiaIgGtlTUpgikI-fmKyOhMrlTtOsdqCJfmUTO4Ypp0o3qZsdSwAKFVkBkJ3sFLlQ', '', '', NULL, 'Yes', 'No', '65568799_329363321317789_1618900240469852160_n.jpg', NULL, NULL, '2019-07-13 02:10:20'),
(140, 'Annabella', 'Sophia Doreen Katarina', 'Escoval', 'gescoval1@gmail.com', '$2y$10$ad2GwRppWPDuI9RH4Am.7uMt6LjWXf3ekmksBPGA0/FneodFZ0aku', '2507271364', '2835 Ronald Road', 'Victoria', 'BC', 'V9B 2L7', 'Canada', '2005-08-05', 'female', 'Teen', NULL, 'ESCOVAL, ANNABELLA SOPHIA DOREEN KATARINA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190415_135641.jpg', NULL, NULL, '2019-07-13 02:10:34'),
(141, 'Anne', NULL, 'Kadwell', 'akadwell23@me.com', '$2y$10$iEMgZ1zIo8V4i3QANeArOus/q7zXsVaYs.PeQofAj32rs3CzC2Fkm', '604 306 8920', 'PO Box 3156', 'Garibaldi Highlands (within Squamish)', 'BC', 'V0N1T0', 'Canada', '1968-08-19', 'female', 'Adult', NULL, 'KADWELL, ANNE', 'www.linkedin.com/in/ak2317', NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_2339.jpg', NULL, NULL, '2019-07-13 02:53:47'),
(142, 'Anne', NULL, 'Lin', 'anneikeda@gmail.com', '$2y$10$wCC9xH/TxG26FfrJKw.R7.N3MPDmy8/WLO6BK1JIpRpaA0prdFlNy', '7789287325', '15677 102 Avenue', 'Surrey', 'BC', 'V4N2G5', 'Canada', '1981-06-24', 'female', 'Adult', NULL, 'LIN, ANNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'anne-face.jpg', NULL, NULL, '2019-07-13 02:54:21'),
(143, 'Annelyn', 'Ruby', 'Victor', 'avictor.cheam@gmail.com', '$2y$10$Z2TluMqDc0VWpUJblp9Yiu9uHQ9JWn3bp73dAfsHV8k9jysa3DFhO', '604-316-8884', '5, 1700 Mackay Cres', 'Agassiz', 'BC', 'V0M 1A3', 'Canada', '2004-01-23', 'female', 'Teen', NULL, 'VICTOR, ANNELYN RUBY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20170727_180653-1.jpg', NULL, NULL, '2019-07-13 02:54:51');
INSERT INTO `performers` (`id`, `firstName`, `middleName`, `lastName`, `email`, `password`, `phonenumber`, `address`, `city`, `province`, `postalCode`, `country`, `date_of_birth`, `gender`, `applicationfor`, `instagram`, `twitter`, `linkedin`, `facebook`, `imdb_url`, `agent_name`, `union_name`, `union_number`, `agent`, `union_det`, `upload_primary_url`, `remember_token`, `created_at`, `updated_at`) VALUES
(144, 'Anne-Marie', NULL, 'Dupperon', 'dupperonam@gmail.com', '$2y$10$3APorf/4X9HVOgqU/UMkJe14dJxJfaf4/FrD3GxM1xx05ag09r/6i', '6043163900', '6950 Oakwood drive', 'Agassiz', 'BC', 'V0M 1A3', 'Canada', '1954-10-07', 'female', 'Adult', NULL, 'DUPPERON, ANNE-MARIE', NULL, 'Petunia Dupperon/AnnieDupperon', NULL, '', '', NULL, 'No', 'No', 'IMG_0352.jpg', NULL, NULL, '2019-07-13 02:55:12'),
(145, 'Annette', 'Audrey ', 'Gehring ', 'Mrsbuy@hotmail.com', '$2y$10$4SXN3wHg8URRH/97E9o7zOshyiKuac0yRUlC6R5jLD/49f5SvR/gW', '6044401174', '8698 206B St', 'Langley', 'BC', 'V1M3X5', 'Canada', '2006-06-02', 'male', 'Child', NULL, 'GEHRING , ANNETTE AUDREY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20180623_153108-1.jpg', NULL, NULL, '2019-07-13 02:55:31'),
(146, 'Anni', 'Monica ', 'Torikka', 'anniandthegirls@hotmail.com', '$2y$10$3IptD1CJcUQ7dKnGA0jTU.WhhoQdi658tm0WeXx3lWlzk.k/Wac7C', '2503055122', '456 Mulberry Ave ', 'Kamloops ', 'BC', 'V2b2r5 ', 'Canada', '1958-01-24', 'female', 'Adult', NULL, 'TORIKKA, ANNI MONICA', NULL, 'Anni Torikka ', NULL, '', '', NULL, 'No', 'No', 'CB7F1A14-FA60-4B68-A660-CC1581F42FED.jpeg', NULL, NULL, '2019-07-13 02:55:50'),
(147, 'Ann-Marie', NULL, 'Leonard', 'amleonard43@gmail.com', '$2y$10$YNRzIYszgHQApdA7umGbX.ZBvexFSFLH1j.yCzxrApdPNSSnOUdra', '514-601-4889', '3528 4th av.', 'vancouver', 'BC', 'V6R 1N8', 'Canada', '1982-10-19', 'female', 'Adult', NULL, 'LEONARD, ANN-MARIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'ann-marie-8.jpg', NULL, NULL, '2019-07-13 02:56:17'),
(148, 'Anthea', 'Eowyn', 'Mackey', 'loribonertz@gmail.com', '$2y$10$VMC1XhULg8KJ3IiiyM9uRuiN6bilTRr7xIkqy7hZiLe2oXTa.ucc6', '778-867-2095', '1805 Primrose Crescent', 'Kamloops', 'BC', 'V1S 0A6', 'Canada', '2003-01-20', 'male', 'Teen', 'theamackey', 'MACKEY, ANTHEA EOWYN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_0372.jpeg', NULL, NULL, '2019-07-13 02:56:52'),
(149, 'Anthony', NULL, 'Anglehart', 'cassandracurdie@hotmail.com', '$2y$10$qA8hXt7TmYnqd1DZjq7aKOpGSbnye9eM86cjowC5m5D4/myuiWquC', '604-316-7465', '7373 Elm Rd', 'Agassiz', 'BC', 'V0M 1A2', 'Canada', '2007-11-11', 'male', 'Child', 'anthony_anglehart', 'ANGLEHART, ANTHONY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(150, 'Anthony', NULL, 'Demare', 'tonydemare@gmail.com', '$2y$10$K6Bhm6MI7ia7/3alu3z4N.bzdjyk6lUyUZE71qmYwG5dO2I13oPhi', '6043079815', '1171 Marsden court', 'Burnaby', 'BC', 'V5A3K2', 'Canada', '1991-02-02', 'male', 'Adult', 'ademare', 'DEMARE, ANTHONY', NULL, NULL, 'https://m.imdb.com/name/nm8800098/', '', '', NULL, 'Yes', 'Yes', 'Anthony-Demare-blazer-headshot.jpg', NULL, NULL, '2019-07-13 02:57:02'),
(151, 'Anthony ', NULL, 'Rodriguez ', 'rsamudio1490@gmail.com', '$2y$10$ULlaa14P9/CMxCE9AQpKaOY4mWealyVQWq6hFFHqwpRx1GsJVRDOK', '7785354696', '2335 W 51 st ave ', 'Vancouver ', 'BC', 'V6p1e8 ', 'Canada', '1990-08-14', 'male', 'Adult', NULL, 'RODRIGUEZ , ANTHONY', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'C7E5022E-F30F-414C-9E79-8093480330F6.jpeg', NULL, NULL, '2019-07-13 02:57:22'),
(152, 'Antony', NULL, 'Le', 'tnyle55@hotmail.com', '$2y$10$zEKWbc4oOIxDUbD7sLk4mejM6Tn.rm2YRvWhP0GukbsHih6J6pK4W', '778-839-2052', '5869 123A Street', 'Surrey', 'BC', 'V3X 1Y3', 'Canada', '1987-08-27', 'male', 'Adult', NULL, 'LE, ANTONY', 'https://www.linkedin.com/in/antonyle/', 'https://www.facebook.com/antonyle55', NULL, '', '', NULL, 'Yes', 'No', '10494602_10154464288035074_6908255480926597801_n.jpg', NULL, NULL, '2019-07-13 02:57:48'),
(153, 'AOY', NULL, 'CHIU', 'aoyc@shaw.ca', '$2y$10$T7QMj3VQWK.eywxVzp5zKePRPZxI5OGZVqVmGcT/MMr6lMRfy4ZMO', '7789866762', '8380 Francis Road', 'Richmond', 'BC', 'V6Y 1A4', 'Canada', '2016-12-10', 'female', 'Teen', NULL, 'CHIU, AOY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Aoy-Finals-web-size-1.jpg', NULL, NULL, '2019-07-13 02:58:51'),
(154, 'Apidi', NULL, 'Onyalo', 'apidionyalo@hotmail.com', '$2y$10$en65prI8NxiRM6clOc4S.uXDNdEDIHbiS1xrZTSuwRqbXfRmUnwBO', '6043769228', '103-2290 Wall St', 'Vancouver', 'BC', 'V5L 1B6', 'Canada', '1985-08-17', 'female', 'Adult', '@apidi', 'ONYALO, APIDI', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_9496.jpg', NULL, NULL, '2019-07-13 02:59:20'),
(155, 'April', 'Stephanie', 'Bryce ', 'Patiencepayton@gmail.com', '$2y$10$f5VCawuUUEdXpExW51CYnue9ao5D3g/ph/3qnQkf9VvBpvFqEzOM2', '6046445861', '217a 7155 Hall road', 'surrey', 'BC', 'V3W 4X4', 'Canada', '1985-01-22', 'female', 'Adult', NULL, 'BRYCE , APRIL STEPHANIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'FB_IMG_1551231470203.jpg', NULL, NULL, '2019-07-13 02:59:51'),
(156, 'April ', 'Evelyn', 'Steegstra', 'april.gallinger@gmail.com', '$2y$10$kBpn5TwhrBx7DCLqbu64f.PIYJh70FspxwP.p31IyeRBTiofEgpua', '604-378-2636', '9875 Gracemar Dr', 'Chilliwack', 'BC', 'V2P7P1', 'Canada', '1997-09-13', 'female', 'Adult', NULL, 'STEEGSTRA, APRIL  EVELYN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'LRM_EXPORT_20180628_200513.jpg', NULL, NULL, '2019-07-13 03:00:12'),
(158, 'Aramesh', NULL, 'Atash', 'arameshatash@gmail.com', '$2y$10$dCatknlHEyIEX3e3r0oiGOY5zSaI0ZjbMzHbW2.Lay3DPi3R4JrsG', '6047828064', '3C, 328 Taylor Way', 'West Vancouver', 'BC', 'V7T 2Y4', 'Canada', '1985-04-01', 'female', 'Adult', '@meshtash', 'ATASH, ARAMESH', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'PAN04393.jpg', NULL, NULL, '2019-07-13 03:01:41'),
(159, 'Araz', NULL, 'Yaghoubi', 'ARAZ.YAGHOUBI93@GMAIL.COM', '$2y$10$n88bMQh8ixC/FIF0zdfddO/qH50v8AbAoR283JpSlGE7.MLgaLigS', '778-384-8078', '109-200 Keary St', 'New Westminster', 'BC', 'V3L 0A6', 'Canada', '1993-09-12', 'male', 'Adult', NULL, 'YAGHOUBI, ARAZ', NULL, NULL, 'http://www.imdb.com/name/nm7113303/', '', '', NULL, 'Yes', 'Yes', 'unnamed-4.jpg', NULL, NULL, '2019-07-13 03:01:59'),
(160, 'Aria ', 'Sophia', 'Sium', 'guebezail@gmail.com', '$2y$10$GB4.pO6iXLT5RBEx.iR9fea3rvR3c/dVfVTAUehEE.Cv88dA41rAy', '6045189420', '13909 59a ave', 'Surrey', 'BC', 'V3xX0G6', 'Canada', '2015-10-17', 'female', 'Child', '@luluhotstuff', 'SIUM, ARIA  SOPHIA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(161, 'Aricin', 'Reid', 'seto', 'lgseto@telus.net', '$2y$10$6M4T8V1dFfNk445uJtBYR.zEMSunkYkRbmZ1XidQOmKj73qq/RFgy', '2508885796', '4258 cedar hill road', 'Victoria', 'BC', 'v8n 3c5', 'Canada', '2008-06-20', 'male', 'Child', NULL, 'SETO, ARICIN REID', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'IMG_3215.png', NULL, NULL, '2019-07-13 03:02:34'),
(162, 'Arika', NULL, 'Barnsley', 'alikaiwata@gmail.com', '$2y$10$sPLU39KCpzLN5kBKPWh4PuRImtb4oBoiLCqQ1O4xESeCqlNfCSiNu', '6047046773', '208 1683 Adanac Street', 'Vancouver', 'BC', 'V5L 2C7', 'Canada', '1989-10-09', 'female', 'Adult', NULL, 'BARNSLEY, ARIKA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'E5C6931A-8315-4ED5-9364-3E4862055334.jpeg', NULL, NULL, '2019-07-13 03:02:53'),
(163, 'Arne', 'Jenson', 'Zabell', 'ajzabell@live.ca', '$2y$10$Rp5JPv88ZVhPp8DX3RbJxeqDF5Pz/71sCxotVgax6snhIzFVGvlwC', '6048690443', '14470 Alpine Blvd.', 'Hope', 'BC', 'v0x1l5', 'Canada', '1996-12-16', 'male', 'Adult', '@ajzabell', 'ZABELL, ARNE JENSON', NULL, 'facebook.com/SuuhDude', NULL, '', '', NULL, 'No', 'No', 'G0254258.jpg', NULL, NULL, '2019-07-13 03:03:13'),
(164, 'Arnie', NULL, 'Gauthier', 'arnie@arnie.ca', '$2y$10$pGxahelqRbPWhnsXTeiAyuXLBjsgqIzsiw.Nc5IBEMGk89.OgRfRS', '604-916-4349', '3534 Cambridge St.', 'Vancouver', 'BC', 'V5K 1M4', 'Canada', '1957-09-06', 'male', 'Adult', NULL, 'GAUTHIER, ARNIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_20190201_201702.jpg', NULL, NULL, '2019-07-13 03:03:56'),
(165, 'Arthur', 'Thomas', 'Buffie', 'artbuf@telus.net', '$2y$10$tmsowrrdCPLGNB1qWQTbTuHQxHu/PE1WTt4Ua6PvOqsx4a6u4dGt.', '7783864775', '16125 79 Ave', 'Surrey', 'BC', 'V4N0K3', 'Canada', '1956-06-28', 'male', 'Adult', NULL, 'BUFFIE, ARTHUR THOMAS', NULL, 'https://www.facebook.com/art.buffie', NULL, '', '', NULL, 'Yes', 'Yes', 'Art.jpg', NULL, NULL, '2019-07-13 03:04:30'),
(166, 'Arthur', NULL, 'Walker', 'ambulat@icloud.com', '$2y$10$/XWfrFSazyejRuqRW.GveO8Y4A28w5IaCwWu8cjA5RmYxo4a.cbxu', '7789913876', '4791 Argyle St.', 'Vancouver', 'BC', 'V5N 3X9', 'Canada', '2007-07-27', 'female', 'Child', NULL, 'WALKER, ARTHUR', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '3E951120-C7CA-465A-A8EC-C5802B0BEFAB.jpeg', NULL, NULL, '2019-07-13 03:04:57'),
(167, 'Artur', 'Edmundovich', 'Peter', 'artemisauclair@gmail.com', '$2y$10$WjGAupJ6nm97B/wcJp52HO9TFzwN8PKHUiSQNLL8f9HgurY6SdsKW', '7788296370', '3260 parker st ', 'Vancouver', 'BC', 'V5k 2v8', 'Canada', '1993-08-06', 'male', 'Adult', 'Cyberbohemian', 'PETER, ARTUR EDMUNDOVICH', NULL, 'Artemis Auclair', NULL, '', '', NULL, 'Yes', 'Yes', 'IMG_9540.jpg', NULL, NULL, '2019-07-13 03:05:24'),
(169, 'asccreative', 'freeman', 'casting', 'asccreative@info.com', '$2y$10$DRmrlJQ7rVlE00MQBWELs.tAOpuL29twfXgSTIPHAPWAg3dG.UEme', '7878788', 'asccreative', 'Vancouver', 'BC', 'A1A1A1', 'Canada', '1999-12-10', 'male', 'Adult', 'instagram', 'CASTING, ASCCREATIVE FREEMAN', 'linkedin', 'facebook', 'imdb', '', '', NULL, 'No', 'No', 'Hydrangeas.jpg', NULL, NULL, '2019-07-13 03:07:19'),
(170, 'asdasdsa', NULL, 'asdsadsa', 'sdfsad@dfsd.com', '$2y$10$9YwPupAOMIQa7uoKHASwwetcidonp2VFcZzysY6TLDKSjN9mt5dbG', 'asdsaa', 'sadsa', 'asdsadas', 'BC', 'sadsa', 'Canada', '2019-05-07', 'male', 'Adult', NULL, 'ASDSADSA, ASDASDSA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(179, 'Ashlee', NULL, 'Nunn', 'ashleenunn91@gmail.com', '$2y$10$UkWFJNIqx4eUirvUyyi7T.xGM48YjEjVxq798L9ZVem/CYybfIHGq', '7782401181', '83 17097 64 ave', 'Surrey', 'BC', 'V3S 1Y5', 'Canada', '1991-08-01', 'female', 'Adult', NULL, 'NUNN, ASHLEE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_0069.png', NULL, NULL, '2019-07-13 03:19:03'),
(180, 'Ashley', 'Maddison ', 'Richter', 'thornswolfbane@gmail.com', '$2y$10$PasA7fKxZLTSQb9oLRLbXOIUX31uoUpJnBfyj/Li1PVAbV.ECVu/O', '+1 07785330774', '#2 242 Ontario street', 'Victoria', 'BC', 'V8V1N2', 'Canada', '1999-07-22', 'female', 'Adult', 'ash_nan22 ', 'RICHTER, ASHLEY MADDISON', NULL, 'Ashley Richter', NULL, '', '', NULL, 'No', 'No', 'IMG_20180720_180534.jpg', NULL, NULL, '2019-07-13 03:20:18'),
(181, 'Ashley', NULL, 'Clements', 'duckleburry@gmail.com', '$2y$10$XbRoOmPQ92FFiiaWTLs/VeBb99INUjKdahJsfob0P294QgQXZg7n.', '2365221987', '46025 Cleveland ave', 'Chilliwack', 'BC', 'V2p2w9 ', 'Canada', '1992-08-12', 'female', 'Adult', NULL, 'CLEMENTS, ASHLEY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_0395.jpg', NULL, NULL, '2019-07-13 03:21:26'),
(182, 'Ashley', 'Marie', 'Fitger', 'ashleemarierose@outlook.com', '$2y$10$blLR/IXLJEtBMt.4tdtmTu0KK4rWlbHYyBsnoKP9Xn5i1AebiYaqe', '2503181567', 'PO Box 1124', 'Barriere', 'BC', 'V0E 1E0', 'Canada', '1997-04-27', 'female', 'Adult', 'ashley_marie_fitger', 'FITGER, ASHLEY MARIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'FB_IMG_1505757754718.jpg', NULL, NULL, '2019-07-13 03:21:37'),
(183, 'Ashley ', 'Teagan ', 'Essig ', 'equinoxacres@shaw.ca', '$2y$10$qrHT5Kxtd0lxpRvrIkHSfur2/85vM4zXDgkYCnUMp4asFaSIwtIGG', '2507131114', '2065 Sanders rd ', 'Nanoose ', 'BC', 'V9p9c2 ', 'Canada', '2006-05-14', 'female', 'Teen', NULL, 'ESSIG , ASHLEY  TEAGAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '76C4F2B9-93E4-4BEA-BD7F-855A4127BE2C.jpeg', NULL, NULL, '2019-07-13 03:21:46'),
(184, 'Ashton', 'Rose', 'Fong ', 'tabicat10@shaw.ca', '$2y$10$lGGE0UZrPZeqvoKCbbPGaOYKkK72R8w0kJ94Iqmqj6Q3HXSE3TRW.', '2506619508', '2904 Pickford rd', 'Victoria', 'BC', 'V9b2k2 ', 'Canada', '2006-08-08', 'female', 'Child', 'Ashton11fong', 'FONG , ASHTON ROSE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'C91A0F4C-E229-4906-A147-9ECDF3784242.jpeg', NULL, NULL, '2019-07-13 03:22:16'),
(185, 'Asia ', NULL, 'Domisiewicz ', 'domisiewicz@hotmail.con', '$2y$10$k3oRCOHl3aEob1IMgbpKA.fUHsnkiRBIEJL74.WP6MlI0h/3NcQ0W', '604710623', '113 3875 w 4th', 'Vancouver ', 'BC', 'V5R 3h8', 'Canada', '1980-04-02', 'female', 'Adult', 'https://instagram.com/p/BdZfo6PhR_4/', 'DOMISIEWICZ , ASIA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '98C84537-0731-4B92-A24C-C4BC70BE66E6.jpeg', NULL, NULL, '2019-07-13 03:22:56'),
(186, 'Astrid', NULL, 'Santilli', 'santilli.astrid@gmail.com', '$2y$10$svsH7rh2qzMEo9aFOR4Fe.wYrMoSq0NCLGPBtlNHR7DCl6D48aBQ6', '-9920', '1902-1288 West Cordova Street', 'Vancouver', 'BC', 'V6C 3R3', 'Canada', '1992-05-07', 'female', 'Adult', 'astridsantilli', 'SANTILLI, ASTRID', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_6044-Edit-2.jpg', NULL, NULL, '2019-07-13 03:23:25'),
(188, 'Asuka', NULL, 'Nakamura', 'aska_ally@hotmail.com', '$2y$10$U47kPklMyY6O2wN2eLqh8O6q7hHlQB9kYtEuPsnPfTZ1lJD81Q8KG', '7788385717', '1-230 Salter street', 'New Westminster ', 'BC', 'V3M0G1', 'Canada', '1976-03-08', 'female', 'Adult', NULL, 'NAKAMURA, ASUKA', NULL, 'AskaAllyNakamura', NULL, '', '', NULL, 'No', 'No', '218DBD69-4895-4164-A33E-1CD5EB8E3E8C.jpeg', NULL, NULL, '2019-07-13 03:24:31'),
(189, 'Atlas', NULL, 'Parker', NULL, '$2y$10$bkgazANEu3wMG8JACYqwjuvlw9f3.zaTlmLs/Zxt0w0lLVI0MBokO', NULL, NULL, NULL, 'BC', NULL, 'Canada', '2012-09-29', 'male', 'Child', NULL, 'PARKER, ATLAS', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(190, 'Atlas', NULL, 'Parker', 'angeleparker@shaw.ca', '$2y$10$XTBwsDNL8yhHXsUHTzcu8OawV10Omlz9oqkA1CIs214eLjKKrkeMi', '6042022857', '2540 Wallace crescent ', 'Vancouver', 'BC', 'V6r3v4 ', 'Canada', '2012-09-29', 'male', 'Child', NULL, 'PARKER, ATLAS', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', '1423FCE9-ED76-4F72-A73B-C6502BE108E4.jpeg', NULL, NULL, '2019-07-13 03:24:51'),
(192, 'Atsushi', NULL, 'Kumo', 'kumoestate@gmail.com', '$2y$10$/1YYWUEsHN.6OfHK4tulF.FJ/XprOQxWbWSOZmg.fNcQwqmLT0E1W', '7789810188', '4300 Hermitage Dr', 'Richmond', 'BC', 'V7E 4N4', 'Canada', '1984-05-28', 'male', 'Adult', NULL, 'KUMO, ATSUSHI', NULL, 'Spencer Kumo', NULL, '', '', NULL, 'Yes', 'Yes', 'IMG_4962.jpg', NULL, NULL, '2019-07-13 03:25:38'),
(193, 'Aubriana ', 'Marrie', 'Frick', 'Shelleyfrick@shaw.ca', '$2y$10$w0svL8kIyEacjMY0M8dMYe0SKNsP7gzVJH4OH.IMOrfsItSGWLP6y', '250-857-5698', '2797 Lake End Rd', 'Victoria', 'BC', 'V9B 5T5', 'Canada', '2006-06-05', 'female', 'Teen', NULL, 'FRICK, AUBRIANA  MARRIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'image.jpeg', NULL, NULL, '2019-07-13 03:26:06'),
(194, 'Audrey', 'Barbara', 'Cooper', 'coultishmodels@telus.net', '$2y$10$QvLXYc36iU7ylqT2x0eSrODkx3DnOzIFyXbUv/pEj44zTQ.Q.WrFu', '250.516.4883', '8-50 Dallas Road', 'Victoria', 'BC', 'V8V1A2', 'Canada', '2011-04-28', 'female', 'Child', NULL, 'COOPER, AUDREY BARBARA', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'Audrey-Cooper.jpg', NULL, NULL, '2019-07-13 03:26:29'),
(195, 'August ', NULL, 'Bramhoff', 'augustbramhoff@rocketmail.com', '$2y$10$4vp6nfuyWWqRRyrPi3OI0ebUMeOwIf9UmIDKcrtDDZxJ9L4qQDTAi', '7785543772', '613-3588 Crowley Drive ', 'Vancouver ', 'BC', 'V5R 6H3', 'Canada', '1984-08-13', 'transgender', 'Adult', NULL, 'BRAMHOFF, AUGUST', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '58802B45-77C1-49DC-89C1-B7DA5099AD22.jpeg', NULL, NULL, '2019-07-13 03:26:53'),
(196, 'Austen', NULL, 'Crick', 'austencrick@outlook.com', '$2y$10$4nOO2EChlmoFFgzF1Zx4A.GROGXNCcvX3ShJJjVNQsLkuOgaY051W', '6049161778', '1759 Kenmore Rd', 'Victoria', 'BC', 'V8N 5B3', 'Canada', '1995-10-25', 'female', 'Adult', 'https://www.instagram.com/austenadele/', 'CRICK, AUSTEN', NULL, 'https://www.facebook.com/austen.crick', NULL, '', '', NULL, 'No', 'No', '52810359_552293598616168_7026577826379726848_n.jpg', NULL, NULL, '2019-07-13 03:27:17'),
(197, 'Austin', 'James', 'Friesen', 'anelephantstear@hotmail.com', '$2y$10$/FCm/pvypfUavlSpGQFvJeT7bqKXDLX53yQ0twa5c1oxD1PDxnVZi', '2508842493', '860 Pratt Road', 'Mill Bay', 'BC', 'V0R2P1', 'Canada', '2007-05-01', 'male', 'Child', NULL, 'FRIESEN, AUSTIN JAMES', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'DD7CBCEC-762A-4A44-9847-09E9AD4F4C24.jpeg', NULL, NULL, '2019-07-13 03:27:41'),
(199, 'Austin Rose', NULL, 'Kivinen', 'emilykivinen@gmail.com', '$2y$10$XI1l4o/MbtU24LmUCHEIBO3eRe22cPcHEfVfNqIJY1ajxJGfO9.C.', '7782550141', '35321 Corbett Place', 'Abbotsford', 'BC', 'V3G1K1 ', 'Canada', '2013-08-21', 'female', 'Child', NULL, 'KIVINEN, AUSTIN ROSE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '88CF22C2-4B9C-48D7-BDF2-F3B76D613100.jpeg', NULL, NULL, '2019-07-13 03:29:17'),
(200, 'Autumn', NULL, 'Bak', 'nqbak@telus.net', '$2y$10$i1/OvWdh3ZovtiUD6VWE3.GN2Fn57R16Le9hYmkwlRrposARfYgKO', '6042404729', '5215 Chamberlayne Ave', 'Delta', 'BC', 'V4K2J7', 'Canada', '2004-09-03', 'female', 'Teen', NULL, 'BAK, AUTUMN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20180728_140404.jpg', NULL, NULL, '2019-07-13 03:30:35'),
(201, 'Ava', 'Mackenzie', 'Jalava', 'Amieg@hotmail.com', '$2y$10$ULIJ34xK8eguy2tbQ3JJTu9ANfPWKorVuVmzwcS6h7E9OgjtoFlJS', '6048452678', 'Po Box 121', 'Rosedale', 'BC', 'V0X 1X0', 'Canada', '2014-10-15', 'female', 'Child', NULL, 'JALAVA, AVA MACKENZIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'FB_IMG_1552834931528.jpg', NULL, NULL, '2019-07-13 03:30:44'),
(202, 'Ava', 'Lynn', 'Lancaster', 'paullancaster@shaw.ca', '$2y$10$Aq8LfnIZkVYXaVYWmKRtN.ZBUI3DLgk2xjyjaRYMuqRxdctrGMsHm', '250-858-8572', '#2-4619 Elk Lake Dr', 'Victoria', 'BC', 'V8Z-5M2', 'Canada', '2007-12-27', 'female', 'Child', NULL, 'LANCASTER, AVA LYNN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_4549.jpg', NULL, NULL, '2019-07-13 03:31:19'),
(203, 'Ava', NULL, 'McGaghey', 'jmteam@shaw.ca', '$2y$10$zAsj6/T2f/HqR2J4EB49zuAeN2ulBqtbi0y70i15VGkAOLVPPxdHa', '2508933153', '2258 Setchfield Ave', 'Victoria', 'BC', 'V9B 6N8', 'Canada', '2005-04-27', 'female', 'Teen', NULL, 'MCGAGHEY, AVA', NULL, 'Ava McGaghey', NULL, '', '', NULL, 'No', 'No', '40410D09-123C-461E-875E-34FCD61182AA.jpeg', NULL, NULL, '2019-07-13 03:31:46'),
(204, 'Ava', 'Natalie', 'Morris', 'kmorris77@live.ca', '$2y$10$Tmg8pyUKgwaGYylbAqi95OPyUo/aD0REy0bxcU8TavgXVs.ay1Gni', '2505723761', 'Box 466, 35 Grosskleg Way', 'Lake Cowichan', 'BC', 'V0R 2G0', 'Canada', '2008-09-10', 'female', 'Child', NULL, 'MORRIS, AVA NATALIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_6209.jpg', NULL, NULL, '2019-07-13 03:32:13'),
(205, 'Avery', NULL, 'Fane', 'averyfane.com@gmail.com', '$2y$10$C1WDCgxnQjYjrIEdKlPATeJK5h6gTsttEdWwIDbdjHK6RLMobTo3G', '17783185571', '308-2238 Kingsway', 'Vancouver', 'BC', 'V5N2T7', 'Canada', '1981-11-02', 'male', 'Adult', NULL, 'FANE, AVERY', 'https://www.linkedin.com/in/averyfaneactual', 'Facebook.com/averyfaneactual', 'Imdb.me/averyfane', '', '', NULL, 'Yes', 'Yes', 'Screenshot_20180517-154337.jpg', NULL, NULL, '2019-07-13 03:32:32'),
(206, 'Avery', 'June', 'Hilliard', 'paulahilliard@shaw.ca', '$2y$10$dUWS8RP6UXIDTF11qQE9cun2/NpE78.VRKN0kyeiOz3DHlTiRVY/u', '778-840-4741', '284 W  St  James Road', 'North Vancouver', 'BC', 'V7N 2P3', 'Canada', '2005-02-26', 'female', 'Teen', 'averyhilliard_', 'HILLIARD, AVERY JUNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(207, 'Avery', NULL, 'Reid', 'avery.reid@outlook.com', '$2y$10$qoucorfgHEI2Rci0f5hA9OlJmrow9vQsPmHYskdfkmCHBuFs6Sz4C', '514-806-4724', '938 Sicamore Dr', 'Kamloops', 'BC', 'V2B 6S2', 'Canada', '1996-10-30', 'female', 'Adult', NULL, 'REID, AVERY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Avery-Reid-Close-Headshot.jpg', NULL, NULL, '2019-07-13 03:32:59'),
(208, 'Aya', 'Mikki', 'Furukawa', 'timetwosleep@gmail.com', '$2y$10$z7gSKCznV30K..jCEEdyAekTwotSOJhhbZ/xEkM823owLI.aXFDyi', '778-887-8464', '11-839 West 17th Street', 'North Vancouver', 'BC', 'V7P3N9', 'Canada', '1999-04-12', 'female', 'Adult', NULL, 'FURUKAWA, AYA MIKKI', NULL, NULL, 'https://www.imdb.com/name/nm4961141/', '', '', NULL, 'Yes', 'No', 'af-10-Edit-1.jpg', NULL, NULL, '2019-07-13 03:33:21'),
(209, 'Ayanna', 'Sophia Elizabeth', 'Bermudez-Boivin', 'arielleboivin@gmail.com', '$2y$10$BBtUjs7qgRz2xyzpVTFs7OigQcglG2815VAotgXeG6dRxiL3aZYHm', '7789773692', '2890 Leigh Rd', 'Victoria', 'BC', 'V9B4G3 ', 'Canada', '2009-08-03', 'female', 'Child', NULL, 'BERMUDEZ-BOIVIN, AYANNA SOPHIA ELIZABETH', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '4EE73652-6B7C-417D-8060-E75B761D16E6.jpeg', NULL, NULL, '2019-07-13 03:33:48'),
(210, 'Ayden', 'James', 'Stevenson', 'jodynlinnea@gmail.com', '$2y$10$qjf9kOr2/CJoj4NncojVQ.KnFgEcEF9d2Np99W6mgfBodkTd5gMSK', '2508963189', '898 Lakeside Pl', 'Langford', 'BC', 'V9B 4H7', 'Canada', '2007-01-19', 'male', 'Teen', NULL, 'STEVENSON, AYDEN JAMES', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'image-1.jpg', NULL, NULL, '2019-07-13 03:34:53'),
(211, 'Ayla', NULL, 'Amano', 'ayla.amano@gmail.com', '$2y$10$BLz2OmG/ihcZFAGYfO36GeL5O1C3h1bpil9pp8M4u.PSa2EwWnN/C', '7787511716', '778 East 17th Avenue', 'Vancouver', 'BC', 'V5V 1B9', 'Canada', '1987-04-03', 'female', 'Adult', NULL, 'AMANO, AYLA', 'https://www.linkedin.com/feed/', NULL, NULL, '', '', NULL, 'No', 'No', 'Close-up-smiling.jpg', NULL, NULL, '2019-07-13 03:36:23'),
(212, 'Ayla', 'Patricia Anne ', 'Colley', 'desirewickenden@gmail.com', '$2y$10$zk4X2/hK09g20sH2VbLNuO3GkyNR/7EA5Hk3mbMF8ClRfUSQbfiAG', '6047687108', '#41 3942 Columbia valley rd ', 'Cultus lake ', 'BC', 'V2R 5B2', 'Canada', '2009-04-01', 'female', 'Child', NULL, 'COLLEY, AYLA PATRICIA ANNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '80E9CBF6-EB34-420A-9A2F-AB0B5BD09F58.jpeg', NULL, NULL, '2019-07-13 03:37:28'),
(213, 'Ayumu', 'Eric', 'Kojima ', 'kaoru1.kojima@gmail.com', '$2y$10$daw3QaMKyZzxetbWmSNCL.0O6hkrHuvNqFLaoAeovQlQWTDpJ.RsS', '6043582152', '312-13th street, west', 'Vancouver', 'BC', '1v5 4b5', 'Canada', '2006-10-29', 'male', 'Child', NULL, 'KOJIMA , AYUMU ERIC', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20181217_221710.jpg', NULL, NULL, '2019-07-13 03:38:40'),
(215, 'Azusa', NULL, 'Ramos', 'azusa0902@hotmail.com', '$2y$10$idIMkNE.p7Zn..NZvpUKpO.A8mDXXE6WbU2eKdoGs3wXpvxjbwFIu', '778-895-9489', '12029 211st', 'maple ridge', 'BC', 'v2x 8k6', 'Canada', '1980-09-02', 'female', 'Adult', NULL, 'RAMOS, AZUSA', NULL, 'azusa ramos', NULL, '', '', NULL, 'No', 'No', 'Capture.jpg', NULL, NULL, '2019-07-13 03:42:09'),
(216, 'Baffeh', 'Prince/Peter', 'Sheriff ', 'sheriff.bafeh@gmail.com', '$2y$10$/m2Z.sp2RuE0EilmADFniexzBoa6AaO2lPEj0Kiz7FpH8iI7Nn2Uy', '2369880996', '#1105-8 smithe mews', 'Vancouver ', 'BC', 'V6B 0B5', 'Canada', '1995-05-09', 'male', 'Adult', '@vancityprince', 'SHERIFF , BAFFEH PRINCE/PETER', NULL, 'Bafeh sheriff ', NULL, '', '', NULL, 'No', 'No', '0CA011D4-4C0B-4B31-B4CF-237EAC3A44D7.jpeg', NULL, NULL, '2019-07-13 03:42:33'),
(218, 'Bahaneh ', NULL, 'Grewal', 'bahanehgrewal@gmail.com', '$2y$10$xn0e0VbaKZTN6/2OfmN8zuOrVDc8VxK9RDLfCx1YGyS/NDZ/J0k7a', '6047290727', '1-7198 Barnet Rd ', 'Burnaby ', 'BC', 'V5A 1C9 ', 'Canada', '1969-07-18', 'female', 'Adult', '@Bon.Bahar ', 'GREWAL, BAHANEH', NULL, NULL, 'http://www.agencyclick.com/BahanehG/resume', '', '', NULL, 'Yes', 'Yes', '3C6F5A38-AA72-4D79-B7F5-2BB9403F731F.jpeg', NULL, NULL, '2019-07-13 03:43:11'),
(219, 'Bahar', NULL, 'Babasadegh ', 'spring1870@yahoo.com', '$2y$10$dFB85SabMNos0Jp3H6N/BOpxDBK5gvAfpTFqP6I/eNBao5VG23kI2', '7789960494', '1104 -7351 halifax st', 'Burnaby ', 'BC', 'V5a4h2 ', 'Canada', '1979-04-08', 'female', 'Adult', NULL, 'BABASADEGH , BAHAR', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '1ABB6BC5-4033-47A0-8682-C6F9A625C867.jpeg', NULL, NULL, '2019-07-13 03:44:17'),
(221, 'Bailey', NULL, 'Bourre', 'bbourre86@gmail.com', '$2y$10$NGMgvJVsbWdxZXkra/dQc.xQ07tcqWo7VX1PbiFod8nlpwu1bf6f.', '2368873136', '5-20649 Edelweiss Drive', 'Agassiz', 'BC', 'V0M 1A1', 'Canada', '1986-08-24', 'female', 'Adult', NULL, 'BOURRE, BAILEY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'me.jpg', NULL, NULL, '2019-07-13 03:46:00'),
(223, 'Baldeep', NULL, 'Bahat', 'dbbahat@gmail.com', '$2y$10$4sFVndlMT42xebI88bYQVeAqaXfqHLwcr.rdIU3ed7cDUPgE65SeS', '7788652500', '8523 152st', 'Surrey ', 'BC', 'V3s3m8', 'Canada', '1986-01-20', 'male', 'Adult', NULL, 'BAHAT, BALDEEP', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(224, 'Barak', NULL, 'Idi', 'idibarak4@gmail.com', '$2y$10$tZo9kYVSmd9ugv45pegsbesoRJQmRBNHs4YYlVp0khdSamxQr9.b.', '2368803705', '8139 145 street', 'Surrey', 'BC', 'V3S 9J6', 'Canada', '1994-03-13', 'male', 'Adult', 'www.instagram.com/officialafrobar', 'IDI, BARAK', NULL, 'https://www.facebook.com/afrobarmusic/', NULL, '', '', NULL, 'No', 'No', 'PSX_20190113_235822_1.jpg', NULL, NULL, '2019-07-13 03:46:14'),
(225, 'Barbara', NULL, 'Pearce', 'b.jpearce@hotmail.com', '$2y$10$xSjqF8ddG.nGDY4VEHDdyOYp5U7Kp3HYL0TNxeKu7I/5k.JMpTc6u', '778 228-1072', '#11-7883 Knight St', 'Vancouver', 'BC', 'V5P 2X5', 'Canada', '1957-12-21', 'female', 'Adult', NULL, 'PEARCE, BARBARA', NULL, 'https://www.facebook.com/profile.php?id=100009183627308', 'https://www.imdb.com/name/nm9376848/', '', '', NULL, 'Yes', 'Yes', 'me-6-6-18b.jpg', NULL, NULL, '2019-07-13 03:46:32'),
(228, 'Barbara', NULL, 'Renaud', 'brenaud@shaw.ca', '$2y$10$XY98RuTzU18YilETMH5rW.2NtU9jlyYdA/36nXWh7Rw/w3JY1FH6a', '604-607-4796', '22910 Purdey Avenue', 'Maple Ridge', 'BC', 'V2X 7M1', 'Canada', '1963-04-20', 'female', 'Adult', NULL, 'RENAUD, BARBARA', NULL, 'https://www.facebook.com/barbara.renaud.18', 'https://www.imdb.com/name/nm2781842/', '', '', NULL, 'No', 'No', 'A77A0029-298A-4F4A-BA5C-7423C10EFE9D.png', NULL, NULL, '2019-07-13 03:47:26'),
(230, 'Bauer', 'Keenan', 'Pinsonneault', 'ml123456@gmail.com', '$2y$10$GgYUeJIkVSwbMnCqRoNrvuzna3ZKDziAzTtzd3lLnG48ma4vCA1P2', '2508186688', '771 Canterbury road', 'Victoria', 'BC', 'V8X3E4', 'Canada', '2009-11-18', 'male', 'Child', NULL, 'PINSONNEAULT, BAUER KEENAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'MVIMG_20190321_1048432.jpg', NULL, NULL, '2019-07-13 03:47:41'),
(233, 'Becca', NULL, 'Burr', 'beccaburr0@gmail.com', '$2y$10$QbpqN3D8s.l6DBjX85x9ueMXLhUY.WQS8aga1tvXwxb8fokbMItJK', '6047012588', '1688 parkwood drive ', 'Agassiz ', 'BC', 'V0m1a2', 'Canada', '1993-01-15', 'female', 'Adult', NULL, 'BURR, BECCA', NULL, 'Becca burr ', NULL, '', '', NULL, 'No', 'No', '5B9DA0A4-8A49-47AD-A6D0-0572B08B210D.jpeg', NULL, NULL, '2019-07-13 03:48:20'),
(235, 'Becky', 'I', 'Goebel', 'goebelbecky@gmail.com', '$2y$10$//2ajC7dLlggACJc8vEqwe98ut3gxKvdqdnMbXxd/uJZ3oMQstDGe', '604-354-5016', '55 East 12th Ave. Unit 207', 'Vancouver', 'BC', 'V5T4J4', 'Canada', '1991-04-22', 'female', 'Adult', '@actuallyitsaxel', 'GOEBEL, BECKY I', 'https://www.linkedin.com/in/beckygoebel/', 'https://www.facebook.com/actuallyitsaxel', 'https://www.imdb.com/name/nm8613011/', '', '', NULL, 'No', 'No', 'IMG_2361.jpg', NULL, NULL, '2019-07-13 03:49:15'),
(237, 'Bella', 'Rose', 'McCairns', 'janetmccairns@telus.net', '$2y$10$5M3rEJTmOG3mHgJedBXeTeWsH7f4wX6blUYW8Jot3bWov4WUqKy.q', '604-315-5576', '208 - 3600 Windcrest Drive', 'North Vancouver', 'BC', 'V7G2S5', 'Canada', '2003-03-06', 'female', 'Teen', 'bella_rose_m', 'MCCAIRNS, BELLA ROSE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_3374.jpg', NULL, NULL, '2019-07-13 03:49:46'),
(241, 'Ben', NULL, 'Loyst', 'benjaminloyst@gmail.com', '$2y$10$w76Oj/rvOKsLUEciPZcR/umagmB2ccES9rQ/wd4Pr9VHtutAH4be2', '2506676711', '220 Machleary Street', 'Nanaimo', 'BC', 'V9R 2G6', 'Canada', '1999-10-11', 'male', 'Adult', NULL, 'LOYST, BEN', NULL, 'https://www.facebook.com/ben.loyst', NULL, '', '', NULL, 'No', 'No', '1-Ben-Loyst-8X10-2-1.jpg', NULL, NULL, '2019-07-13 03:50:19'),
(244, 'Bernadette', 'Villalon', 'Ortilla', 'pinkyortilla@gmail.com', '$2y$10$bDRXdssCalV/dGuTcoyxEu9ipX4j4taGXIyWizYJPedHBcCuoCvbe', '6047936054', '690 Coquihalla street', 'Hope', 'BC', 'V0x 1L0', 'Canada', '2018-10-27', 'female', 'Adult', 'Pinkypot27', 'ORTILLA, BERNADETTE VILLALON', NULL, 'Pinky_102789@yahoo.com', NULL, '', '', NULL, 'No', 'No', '22FE6D32-6F10-4F72-9ECE-6ADE6605F943.jpeg', NULL, NULL, '2019-07-13 03:50:39'),
(245, 'Bernadette Anne Therese', NULL, 'Hall', 'annalice.hall@gmail.com', '$2y$10$i3we93/rnoXX1DhYecMEQe4FFi9oMyY.69kK86He/hQetz13bkq9C', '7783196133', 'Suite 313 9344 Cameron St', 'Burnaby', 'BC', 'V3J1L9', 'Canada', '2000-11-22', 'female', 'Teen', NULL, 'HALL, BERNADETTE ANNE THERESE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_7892pscc.jpg', NULL, NULL, '2019-07-13 03:51:11'),
(247, 'Bernard', NULL, 'Gambier', 'gmb.bernard@gmail.com', '$2y$10$60pZBF7ZGk6B3ku0cLf2h.F77NEQn4UIizH9iLEfdrhZM2DUtGJpu', '6043639673', '439 Mclean dr', 'Vancouver', 'BC', 'V5l 3M5', 'Canada', '1993-06-01', 'male', 'Adult', 'Bernard_gmb', 'GAMBIER, BERNARD', NULL, 'Bernard Gambier', NULL, '', '', NULL, 'Yes', 'No', '5D42764E-E1C6-46BA-A35A-013A1D23DBC4.jpeg', NULL, NULL, '2019-07-13 03:51:50'),
(251, 'Bernarda', NULL, 'Antony', 'bernarda.antony@gmail.com', '$2y$10$6fZjosrj9A.iyOR3F5GYRu.wLi5eUn8Tqel7XUSGCePpnVEXwLLCW', '+1(604)401-2278', '3333 Commercial Drive Unit 106', 'Vancouver', 'BC', 'V5N4E5', 'Canada', '1991-01-22', 'female', 'Adult', NULL, 'ANTONY, BERNARDA', 'https://www.linkedin.com/in/bernardaantony/', 'https://www.facebook.com/bernarda.antony', NULL, '', '', NULL, 'No', 'No', 'IMG_2365.jpg', NULL, NULL, '2019-07-13 03:53:27'),
(331, 'Bethanea', 'Hsin-Yue', ' Chou', 'bethanea.chou6@gmail.com', '$2y$10$Cq1elCzqZDDREjehY7S79uAe5vFYM5y.UXuF.XDwFUT77xz4jPuMm', '6043521447', '2540 Hoskins Road', 'North Vancouver', 'BC', 'V7J3A3', 'Canada', '2002-03-10', 'female', 'Adult', '_bc.j', 'CHOU, BETHANEA HSIN-YUE', NULL, 'Bethanea Chou', NULL, '', '', NULL, 'No', 'No', 'PFP.jpg', NULL, NULL, '2019-07-13 04:44:38'),
(332, 'Bharat ', NULL, 'Arora', 'arorabharat144@gmail.com', '$2y$10$jKt385PvvZFzIeGaLCzB9OGlG3f0P7qMFwr50r76erWvng2StoL9u', '6044451597', '#206 - 4635 Imperial Street', 'Burnaby', 'BC', 'V5J 1B9', 'Canada', '1997-08-15', 'male', 'Adult', NULL, 'ARORA, BHARAT', 'linkedin.com/in/bharat-arora-3887a98a/', 'facebook.com/arorabharat144', NULL, '', '', NULL, 'Yes', 'No', 'IMG_20180425_031033_467.jpg', NULL, NULL, '2019-07-13 04:45:05'),
(333, 'Bill', NULL, 'Croft', 'billcroft69@hotmail.com', '$2y$10$8qLhQBt5QOP3iXFnxlcwG.jC2BY4Uno0OoPgIMHxNbQZju3lOxFym', '6047625768', '209-325 e 6th', 'vancouver', 'BC', 'V5T1J9', 'Canada', '1952-12-27', 'male', 'Adult', NULL, 'CROFT, BILL', NULL, NULL, 'http://www.imdb.com/name/nm0188457/?ref_=nv_sr_1', '', '', NULL, 'Yes', 'Yes', 'DSC_1320.jpg', NULL, NULL, '2019-07-13 04:45:28'),
(334, 'Bill', '-', 'shibicky', 'billshibicky@shaw.ca', '$2y$10$8qhfT33YOVbKgNXppbd15Osx1apl1EDw5rVOvFoJBLsg.xAcWUTBS', '6048504736', '1968 N. Parallel', 'abbotsford', 'BC', 'v2c 3e3', 'Canada', '1964-01-25', 'male', 'Adult', NULL, 'SHIBICKY, BILL -', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `performers_bk`
--

CREATE TABLE `performers_bk` (
  `id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `firstName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middleName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(99) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postalCode` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` enum('male','female','transgender') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'male',
  `applicationfor` enum('Adult','Child','Teen') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Adult',
  `instagram` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `twitter` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `linkedin` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `facebook` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `imdb_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `agent_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `union_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `union_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `union_det` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload_primary_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `performers_bk`
--

INSERT INTO `performers_bk` (`id`, `firstName`, `middleName`, `lastName`, `email`, `password`, `phonenumber`, `address`, `city`, `province`, `postalCode`, `country`, `date_of_birth`, `gender`, `applicationfor`, `instagram`, `twitter`, `linkedin`, `facebook`, `imdb_url`, `agent_name`, `union_name`, `union_number`, `agent`, `union_det`, `upload_primary_url`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Abidali', NULL, 'Dhabra', 'admin@gmail.com', '$2y$10$hTzdn3oCW6R3Vj10zoav6.soMAw32Nidvvwbs0KBQ.HmnM3XyBkCa', '(989) 890-8017', 'At & post - Badarpur , Ta-Vadnagar', NULL, NULL, NULL, NULL, '2019-02-07', 'male', 'Adult', 'asd', 'asdasd', 'asdasd', 'asd', NULL, 'asdasd', 'asda', 'asdad', NULL, NULL, NULL, NULL, '2019-07-13 00:57:22', '2019-07-13 00:57:22'),
(2, ' Crystal', 'Ann', 'Tremblay ', 'tapadera@shaw.ca', '$2y$10$wWULIEomCaS9HosXP14CO.oC/01kailmU/aRVabEwMzVDYqA/t.uW', '6043169338', 'Box 662 ', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1969-11-10', 'female', 'Adult', NULL, 'TREMBLAY ,  CRYSTAL ANN', NULL, 'CrystL Ann', NULL, '', '', NULL, 'No', 'Yes', '95450D90-F49B-44EB-95B6-D5CFCEA44461.jpeg', NULL, NULL, '2019-07-13 00:59:50'),
(3, ' Gregory', 'Matthew', 'Coltman', 'darkone021@gmail.com', '$2y$10$8q3JLCciNQINvAe2pGrVLOZuPDfGM0U24v12kiNcwfP1EUrsMVLyS', '7789892634', '#20 17097 64th ave', 'SURREY', 'BC', 'V3S 1Y5', 'Canada', '1974-04-20', 'male', 'Adult', NULL, 'COLTMAN,  GREGORY MATTHEW', NULL, NULL, NULL, '', '', NULL, 'Yes', 'Yes', NULL, NULL, NULL, NULL),
(4, 'Aaliyah', 'Layne', 'Seidlitz', 'aaliyah.layne.seidlitz@gmail.com', '$2y$10$fTUVaMUGzlRPt8Mj8gWBW.vmCvkUmGX9ERJZHVgN932fXIjmNVEQm', '2505896482', '1154 Mason St #6', 'Victoria', 'BC', 'V8T 1A6', 'Canada', '2003-11-19', 'female', 'Teen', 'aaliyah.seidlitz', 'SEIDLITZ, AALIYAH LAYNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190501_122510.jpg', NULL, NULL, '2019-07-13 01:00:20'),
(5, 'Aaliyah', 'Layne', 'Seidlitz', 'aaliyah.layne.seidlitz@gmail.com', '$2y$10$/HeIn57Pb7.jHVjBh9p7De4z5GJHYE8/AxVIj07kSQLUqlkSNm3sm', '2505896482', '1154 Mason St #6', 'Victoria', 'BC', 'V8T 1A6', 'Canada', '2003-11-19', 'female', 'Teen', 'aaliyah.seidlitz', 'SEIDLITZ, AALIYAH LAYNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190501_122510-1.jpg', NULL, NULL, '2019-07-13 01:00:45'),
(6, 'Aaliyah ', NULL, 'Lamirand ', 'jl-rose-86@hotmail.com', '$2y$10$0UvSbBv9OKFp5ZmjG4pM9.WYM.ppgqMr9y6NkVR3tBUJd4yY0kksy', '6048326701', '113 scowlitz access road', 'Lake errock', 'BC', 'V0M1N0 ', 'Canada', '2010-06-13', 'female', 'Child', NULL, 'LAMIRAND , AALIYAH', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190320_221535.jpg', NULL, NULL, '2019-07-13 01:01:45'),
(7, 'Aanya', NULL, 'Sethi', 'karishma.sethi@hotmail.com', '$2y$10$qbVWUOqOC.YqebeMGw0xMOp6dMeGaJ1icBMEX/ZWKijVNiBmNPfzq', '2508881285', '526 gurunank lane', 'Victoria ', 'BC', 'V9c 0M2', 'Canada', '2012-10-23', 'female', 'Child', NULL, 'SETHI, AANYA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '99553287-618C-4845-8FA4-29E23828E04A.jpeg', NULL, NULL, '2019-07-13 01:02:19'),
(8, 'Aaron', 'Richard,Victor', 'Callihoo', 'aaroncallihoo@yahoo.com', '$2y$10$HX0t1alBLQO0YJ.YjXDMfOx4ipEBeGGXjQWHGHZDcHl6V5BwoUN6y', '2508994720', '3639 woodland dr ', 'Port coquitlam ', 'BC', 'V3b4r5', 'Canada', '1982-07-08', 'male', 'Adult', NULL, 'CALLIHOO, AARON RICHARD,VICTOR', NULL, 'Aaron Callihoo', NULL, '', '', NULL, 'No', 'No', '1B4DE5CC-2C8D-456B-A695-A8F3D798CEF6.jpeg', NULL, NULL, '2019-07-13 01:02:44'),
(9, 'Aaron', 'Konosuke', 'Takahashi', 'terucanada@hotmail.com', '$2y$10$y.GgnnHYKVh.hFLGSPK7ZOI02wcMbexS0.1y9GScvLyj6y/faEI4i', '6047602535', '11058 148A street', 'Surrey', 'BC', 'V3R 3Z4', 'Canada', '2007-09-24', 'male', 'Child', NULL, 'TAKAHASHI, AARON KONOSUKE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'F0D7FC7C-C472-4216-8018-F1C3FDC24971.jpeg', NULL, NULL, '2019-07-13 01:03:11'),
(10, 'Aaron', 'Michael', 'Gelowitz', 'aarongelowitz@shaw.ca', '$2y$10$W8w1Me8srijwQ355C3JS3Of3evAtOTeKWtJvw9.gDjeL6cEXni/om', '6047990084', '45513 Market Way, Apt 113', 'Chilliwack', 'BC', 'V2R6A5', 'Canada', '1980-09-19', 'male', 'Adult', NULL, 'GELOWITZ, AARON MICHAEL', NULL, NULL, NULL, '', '', NULL, 'No', 'Yes', 'smile.jpg', NULL, NULL, '2019-07-13 01:03:51'),
(11, 'Aaron', 'Joshua ', 'Guenther ', 'aaron.guenther@hotmail.com', '$2y$10$BPtvmS8DtvIzcE59IEoqeO6L13jYJg/tgWspKobphNGrlFp5CATri', '2509455218', '1057 Edgehill place', 'Kamloops', 'BC', 'V2C 0G6', 'Canada', '1994-12-29', 'male', 'Adult', 'Aaronguenther22 ', 'GUENTHER , AARON JOSHUA', NULL, 'Aaron Guenther ', NULL, '', '', NULL, 'No', 'No', '14E84286-3F50-44AB-B464-37C8DEED5E73.jpeg', NULL, NULL, '2019-07-13 01:04:04'),
(12, 'Abbie', NULL, 'Maslin', 'abbiemaslin399@gmail.com', '$2y$10$krmbHp2wh7M5SJ5J5ZvhjOBbejJ4vVuulUjW6psTlUrNtxO7LCYXu', '6043169925', '6841 Centennial Ave', 'Agassiz', 'BC', 'V0M1A3', 'Canada', '1992-10-26', 'female', 'Adult', NULL, 'MASLIN, ABBIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_4263.jpg', NULL, NULL, '2019-07-13 01:04:22'),
(13, 'Abby', 'Natsuko', 'Kobayakawa', 'abkob@shaw.ca', '$2y$10$T5lXKZSwZUbOltBYjl3tUucK1ug6u31xCk0aSJ.hPpAsi892Tp/.K', '6048386511', '4950 Buxton Street', 'Burnaby', 'BC', 'V5H 1J5', 'Canada', '1958-07-24', 'female', 'Adult', NULL, 'KOBAYAKAWA, ABBY NATSUKO', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '601EFD52-3136-4EDC-A38A-2C0246775459.jpeg', NULL, NULL, '2019-07-13 01:05:14'),
(14, 'Abby', 'Louise', 'Lamb', 'rtlamb@shaw.ca', '$2y$10$6U8ggjJboqayHn15h7Fe1uyqe3lrcq8Oe2LsGZ23jZik6JvTtDra6', '7782312556', '2813 Mara Drive', 'Coquitlam ', 'BC', 'V3c5t9 ', 'Canada', '2005-06-16', 'female', 'Teen', NULL, 'LAMB, ABBY LOUISE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '62EC0676-52E1-4565-8626-6B6DC1141518.jpeg', NULL, NULL, '2019-07-13 01:05:50'),
(15, 'Abby', 'Louise', 'Lamb', 'rtlamb@shaw.ca', '$2y$10$FmUkJ8Ye8qO0kqRhG1KeWewTFi43m0Ti80Bq2a0sFy8OuztgsgkIq', '7782312556', '2813 Mara Dr', 'Coquitlam ', 'BC', 'V3c5t9 ', 'Canada', '2005-06-16', 'female', 'Teen', NULL, 'LAMB, ABBY LOUISE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '26F2BC11-0066-4A84-8D2E-2FC0D8DF76DA.jpeg', NULL, NULL, '2019-07-13 01:06:48'),
(16, 'Abdourahmane', NULL, 'Wane', 'yvewane7991@gmail.com', '$2y$10$uTaD.Sq4uSe7.FylqPNsyObV71B3EcgJuK9WbK2Jzi/zXMZBw9DI2', '7788751616', '214-1990 west 6th ave. ', 'VANCOUVER', 'BC', 'V6J 4V4', 'Canada', '1972-10-02', 'male', 'Adult', NULL, 'WANE, ABDOURAHMANE', NULL, 'Ab Wane', NULL, '', '', NULL, 'Yes', 'Yes', 'IMG_0836.jpeg', NULL, NULL, '2019-07-13 01:07:12'),
(17, 'Abigayle ', 'Shirley', 'Kotyk', 'trm52002@yahoo.ca', '$2y$10$XNMFfj2vkSmoHM/SS7K5B.EH1VXHV3fmTyNweH2uT0GhPtuAq6Ws2', '604-506-7070', '541 Hodgson Rd ', 'Williams Lake ', 'BC', 'V2G 3P8', 'Canada', '2010-09-25', 'transgender', 'Child', NULL, 'KOTYK, ABIGAYLE  SHIRLEY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(18, 'Adam', NULL, 'Fedyk', 'AFMonarch@gmail.com', '$2y$10$acDx/KI0YJWCZLpXab9GG.FidyAiCCBp8d20jkG3tkcSBAebWZYrq', '604-358-2994', '1748 E 12th Ave', 'Vancouver', 'BC', 'V5N 2A5', 'Canada', '1988-04-12', 'female', 'Adult', 'https://www.instagram.com/adam.virtue/', 'FEDYK, ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Adam-Fedyk-Headshot-Smiling.jpg', NULL, NULL, '2019-07-13 01:07:39'),
(19, 'Adam', 'Michael', 'Barker', 'adamwheelman@gmail.com', '$2y$10$xzCbjr8.e92NU1xtJkA3UO4NRQvvrZ9E0Bg8P9f.oH7z4BeF0fO0.', '6049383596', '966 maple st', 'Whiterock', 'BC', 'V4b4m5', 'Canada', '1985-02-24', 'male', 'Adult', NULL, 'BARKER, ADAM MICHAEL', NULL, NULL, NULL, '', '', NULL, 'No', 'Yes', '14DFE901-DDEA-499D-85EB-11BB9E7E60C3.jpeg', NULL, NULL, '2019-07-13 01:08:05'),
(20, 'Adam', 'Evan', 'Brodie', 'adambrodie@ymail.com', '$2y$10$yjrw70MhlU2fZmEooEhuyOc5X7//oewvlFRCop.SF5Ywh.L1ZNOky', '6045183900', '65521 Dogwood Drive', 'Hope', 'BC', 'V0X 1L1 ', 'Canada', '1982-08-25', 'male', 'Adult', NULL, 'BRODIE, ADAM EVAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '170290_10150352152455154_4075676_o.jpg', NULL, NULL, '2019-07-13 01:08:21'),
(21, 'Adam', NULL, 'Muxlow', 'maddmux@gmail.com', '$2y$10$LuzFfQ7Bc/gKWKgQGQJH3uiOyTGqGh/H2uJ/nDQWOeWY/SJaXB16W', '7783447053', '34250 Fraser street', 'Abbotsford', 'BC', 'V2s1x9', 'Canada', '1981-09-15', 'male', 'Adult', NULL, 'MUXLOW, ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(22, 'Adam', NULL, 'Boughanmi', 'boughanmi@gmail.com', '$2y$10$FaBMrywb1q4IUuOfoGaTb.gex0sLlb5Uv1a/Xrb1Aad/K5ISWInfG', '6044401085', '1603-1199 Seymour Street', 'Vancouver', 'BC', 'V6B1k3', 'Canada', '1985-10-11', 'male', 'Adult', NULL, 'BOUGHANMI, ADAM', NULL, 'https://www.facebook.com/boughanmia', NULL, '', '', NULL, 'Yes', 'Yes', 'DSC00599.jpg', NULL, NULL, '2019-07-13 01:08:47'),
(23, 'Adam', 'Nicola Takeshi', 'Nishi', 'adamnishi93@gmail.com', '$2y$10$.jQ6o8LKdLAtGYCME/TkjuvEYae/sqSl8iHM65DJIqKvMZXcHTRDy', '6048375740', '3120 Regent Street', 'Richmond', 'BC', 'V7E 2M4', 'Canada', '1993-10-27', 'male', 'Adult', '@adamnishi', 'NISHI, ADAM NICOLA TAKESHI', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Adam-Linkedin-photo.png', NULL, NULL, '2019-07-13 01:09:47'),
(24, 'Adam', 'Omar', 'Ghomari', 'adamghomari@gmail.com', '$2y$10$BzaHcUr9NUC7JiEsD1baEOq07F.NKjC09i.Ai7kpsRdyJQFr0t1ie', '6044013690', '1705 Wallace Street', 'Vancouver', 'BC', 'V6R 4J7', 'Canada', '2000-03-10', 'male', 'Adult', 'https://www.instagram.com/adam_ghomari/?hl=en', 'GHOMARI, ADAM OMAR', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Headshot2.jpg', NULL, NULL, '2019-07-13 01:09:56'),
(25, 'Adam', NULL, 'Harris', 'adamharris_15@hotmail.com', '$2y$10$fDGgcCJuGdmAngTYTaxM0egF48hJZX5vwgXTZ748WZtwGZr9fx6ne', '2505722326', '924 glenshee place', 'Kamloops ', 'BC', 'V2e1k6', 'Canada', '1988-01-31', 'male', 'Adult', 'Adamharris15', 'HARRIS, ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'Yes', 'D90E0068-13E2-4DE3-96DC-312C93820FE3.jpeg', NULL, NULL, '2019-07-13 01:10:21'),
(26, 'Adam ', NULL, 'Brown ', 'M.adam.brown87@gmail.com', '$2y$10$5vVf74Dsuoe78odLA6bumewoCcfGHQAJIAP7YrQmtJMeweXNyE5IW', '2892009903', '1408, 1147 quadra St', 'Victoria', 'BC', 'V8W 2K5', 'Canada', '1987-07-24', 'male', 'Adult', NULL, 'BROWN , ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Adam-20190304-050.jpg', NULL, NULL, '2019-07-13 01:10:34'),
(27, 'Addison', 'Daen', 'Sawatzky', 'brentsawa@hotmail.com', '$2y$10$3KSCjWESSjoYh237uyeWnOpKW23ZBat2Ob/F9CbDw2/85ovXRSTKO', '6047981458', '2083 Aberdeen Drive', 'Agassiz', 'BC', 'V0M 1A1', 'Canada', '2009-05-21', 'female', 'Child', NULL, 'SAWATZKY, ADDISON DAEN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'image000000_21.jpg', NULL, NULL, '2019-07-13 01:10:41'),
(28, 'Addison', 'Lee', 'Pinsonneault', 'ml123456@gmail.com', '$2y$10$Bin07tl0ct3W9AT6AVwo1Oq9T5qHDGLYbYi9M5R8O2oob4sieu1fS', '2508186688', '771 Canterbury road', 'Victoria', 'BC', 'V8X3E4', 'Canada', '2006-08-14', 'female', 'Child', NULL, 'PINSONNEAULT, ADDISON LEE', NULL, 'https://www.facebook.com/mandy.lee.1650332', NULL, '', '', NULL, 'No', 'No', 'IMG_20190319_1208462.jpg', NULL, NULL, '2019-07-13 01:10:58'),
(29, 'Adeola', 'Phasilat', 'Mustapha', 'addypmustapha@gmail.com', '$2y$10$V0tUqIbyzi7TdhAfe8xlAOeorpZxZNBdhp7um8mgXhaJjOqqgq.BC', '6043381341', '201-3031 Kingsway', 'Vancouver', 'BC', 'V5R5J6', 'Canada', '1993-09-01', 'female', 'Adult', NULL, 'MUSTAPHA, ADEOLA PHASILAT', NULL, 'https://www.facebook.com/people/Addy-Mustapha/100015769728562', NULL, '', '', NULL, 'No', 'No', 'IMG_1638.jpg', NULL, NULL, '2019-07-13 01:11:42'),
(30, 'Adrean', NULL, 'MacDonald', 'Azilkie55@gmail.com', '$2y$10$cbp0IZqsrvKfSICwE.WO4OTKqPavsNxPMCjnagl1.e6wkpAMLBpL6', '6049975940', 'PO Box 23,', 'Lake Errock', 'BC', 'V0M 1N0', 'Canada', '1960-08-04', 'female', 'Adult', NULL, 'MACDONALD, ADREAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_20160519_101805.jpg', NULL, NULL, '2019-07-13 01:12:00'),
(31, 'Adrian', NULL, 'Heim', 'adrian.heim93@gmail.com', '$2y$10$lBzzFA87lnim8A4eJAhdweG/1Q7RWIXzlOWKXQdJljQYl0c3CWIPG', '8199939350', '5-547 Herald st. ', 'Victoria', 'BC', 'V8W 1S5', 'Canada', '1993-12-03', 'male', 'Adult', NULL, 'HEIM, ADRIAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_2244.jpg', NULL, NULL, '2019-07-13 01:12:21'),
(32, 'Adriana', 'Rachel', 'Depaschoal', 'missekko1@gmail.com', '$2y$10$DnTcJ/rXZhsz1hT/qHSSROh5KRgMOd/9bx3MZeoNlZGDXF27HUMzW', '7787235135', '3676 McRae Cresent ', 'Port Coquitlam ', 'BC', 'V3B4P1', 'Canada', '1981-12-07', 'female', 'Adult', 'Esquaredsquad ', 'DEPASCHOAL, ADRIANA RACHEL', NULL, 'Ekko lostnsoundz', NULL, '', '', NULL, 'No', 'No', 'IMG_1349.png', NULL, NULL, '2019-07-13 01:12:30'),
(33, 'Adriana', NULL, 'Laurent', 'adriana.seibt@gmail.com', '$2y$10$wVTAcOrfpBfS5fknd2aKJukIg7rp4rPRg12DqrGmuI7MS0KafhHfK', '2369987807', '3430 8th Ave W', 'Vancouver', 'BC', 'V6R 1Y5', 'Canada', '1994-05-17', 'female', 'Adult', 'adri.laurent', 'LAURENT, ADRIANA', 'https://www.linkedin.com/in/adriana-laurent-040b9011a/', NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_8563.jpg', NULL, NULL, '2019-07-13 01:13:18'),
(34, 'Adriana', NULL, 'Gomes ', 'getinspired.adriana@gmail.com', '$2y$10$vc0NtEs2V502SJ4cTkAvO.7thajsKNr7sMAKFjsiKxKjnhgYjALlW', '7789685402', '5- 2544 Cornwall Avenue', 'Vancouver', 'BC', 'V6K1C2', 'Canada', '1975-11-24', 'female', 'Adult', NULL, 'GOMES , ADRIANA', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', '6265AB9A-733E-48B5-9EA8-A341EEE3E8F5.jpeg', NULL, NULL, '2019-07-13 01:13:41'),
(35, 'Adriana ', NULL, 'Caldwell ', 'adricaldwell@gmail.com', '$2y$10$mBaY9qFCIqT89C4mJ2v3qOXKy.o4aShfiAGpKQTgcqx/.gaAD8Io2', '7782311292', '5829 booth ave ', 'Burnaby', 'BC', 'V5H3A9 ', 'Canada', '1985-05-16', 'female', 'Adult', 'Adricaldwell ', 'CALDWELL , ADRIANA', NULL, 'Adriana Caldwell ', 'Adriana Caldwell ', '', '', NULL, 'Yes', 'No', '20180406_162143.jpg', NULL, NULL, '2019-07-13 01:14:00'),
(36, 'Adriano ', 'Miranda ', 'Esteves', 'esteves.adriano@hotmail.com', '$2y$10$OgK3adS4FNSpaJGJkxVB2O.gncOkHb1B7v.yBD5eHf0Yo1LsxtRwy', '7788375655', '312-4355 Maywood', 'Burnaby', 'BC', 'V5H 2J8', 'Canada', '1981-06-06', 'male', 'Adult', 'Esteves81', 'ESTEVES, ADRIANO  MIRANDA', NULL, 'Esteves.adriano', NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(37, 'Aidan', NULL, 'Battley', 'aidanbattley@gmail.com', '$2y$10$PelkbTZF8ViJtgTiO0fCP.0.8rO5dkwazTI3isrthk7d1aNoLqKTW', '604-782-8443', '2274 - 179th st.', 'Surrey', 'BC', 'V3Z9V6', 'Canada', '1998-12-03', 'male', 'Adult', 'https://www.instagram.com/aidanbattley/?hl=en', 'BATTLEY, AIDAN', NULL, 'https://www.facebook.com/AidanBattleyPro/', 'https://www.imdb.com/name/nm9235298/', '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(38, 'Aidan', NULL, 'Battley', 'aidanbattley@gmail.com', '$2y$10$yxk2u5obTtcWZsLysdRZC.fRlqYsZBfC8vYcGX6oOJX4VsVZIat0a', '604-782-8443', '2274 - 179th st.', 'Surrey', 'BC', 'V3Z9V6', 'Canada', '1998-12-03', 'male', 'Adult', 'https://www.instagram.com/aidanbattley/?hl=en', 'BATTLEY, AIDAN', NULL, 'https://www.facebook.com/AidanBattleyPro/', 'https://www.imdb.com/name/nm9235298/', '', '', NULL, 'No', 'Yes', 'Aidan-CarletonID.jpeg', NULL, NULL, '2019-07-13 01:14:31'),
(39, 'Aidan', 'Brent', 'Battley', 'aidanbattley@gmail.com', '$2y$10$DsDwIrERJmSmdWNhHA/XC.VBrWPIcu27HMwqeR9w3.skrXJNTlOeq', '604-782-8443', '2274 - 179th st.', 'Surrey', 'BC', 'V3Z9V6', 'Canada', '1998-12-03', 'male', 'Adult', 'https://www.instagram.com/aidanbatt/', 'BATTLEY, AIDAN BRENT', NULL, 'https://www.facebook.com/AidanBattleyPro/', 'https://www.imdb.com/name/nm9235298/bio', '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(40, 'Aidan', 'Brent', 'Battley', 'aidanbattley@gmail.com', '$2y$10$UnhjF5.RYXpzSh6zmzgFvO1hHDMDBzFtgQC.o0Yk0MIgNvRDl.7zq', '604-782-8443', '2274 - 179th st.', 'Surrey', 'BC', 'V3Z9V6', 'Canada', '1998-12-03', 'male', 'Adult', 'https://www.instagram.com/aidanbatt/', 'BATTLEY, AIDAN BRENT', NULL, 'https://www.facebook.com/AidanBattleyPro/', 'https://www.imdb.com/name/nm9235298/bio', '', '', NULL, 'No', 'Yes', 'Aidan-CarletonID-1.jpeg', NULL, NULL, '2019-07-13 01:14:48'),
(41, 'Aiden', 'Colin', 'Hopkins', 'hoppyfam@yahoo.ca', '$2y$10$aSRx3frym2z6.Mdc0UViwOuapTamEG/sH5Er6QQiCmQCBR92ff2F2', '7786780210', '225 Lascelles Cres', 'Victoria', 'BC', 'V9C1C5', 'Canada', '2009-03-30', 'male', 'Child', NULL, 'HOPKINS, AIDEN COLIN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_20190430_222358.jpg', NULL, NULL, '2019-07-13 01:15:14'),
(42, 'Aileen ', 'Elizabeth ', 'Penner', 'aileenpen@gmail.com', '$2y$10$28ZpUeegUrTxk/Oq8xkh8ekchQ4Nw/XgDtMkqHKOtfdjzEX.Z/Cl6', '2502087582', 'P.O. Box 315 / 98 Rockwell Drive ', 'Harrison Hot Springs ', 'BC', 'V0M 1K0', 'Canada', '1975-04-05', 'female', 'Adult', 'pennerton', 'PENNER, AILEEN  ELIZABETH', NULL, 'https://www.facebook.com/aileen.penner', 'https://www.imdb.com/name/nm8355266/', '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(43, 'Akari', 'Lucille', 'Kobayashi', 'peah_honey@hotmail.com', '$2y$10$SydA6IQGH2xSvrYim2Sv/OqBxHv23Rc3/EzLewxPkNJ83JpiXaZd6', '604-230-0415', '4755 Moss Street', 'Vancouver', 'BC', 'V5R3T3', 'Canada', '2010-03-14', 'female', 'Child', NULL, 'KOBAYASHI, AKARI LUCILLE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_9289.jpg', NULL, NULL, '2019-07-13 01:16:13'),
(44, 'Akira ', NULL, 'Ito', 'iloveinter11@gmail.com', '$2y$10$FOejdSmj.7a2GKtmScOZP.skI3ncCiNgE/c8sXZT1G3uSg7TdaHZG', '7782227021', '92 GEIKIE STREET', 'Jasper ', 'AB', 'T0E 1E0', 'Canada', '1997-05-14', 'male', 'Adult', NULL, 'ITO, AKIRA', NULL, 'https://www.facebook.com/profile.php?id=100007690270648', NULL, '', '', NULL, 'No', 'No', 'image-9.jpg', NULL, NULL, '2019-07-13 01:16:47'),
(46, 'Alan', NULL, 'Yu', 'alan@alanjhyu.com', '$2y$10$dYT9pNUrfawgnj1smlrpeutjT/9wNMAG9N4/o0nce9IJRyJxdwykq', '6043158628', '212-5875 Imperial St', 'Burnaby', 'BC', 'V5J1G4', 'Canada', '1980-03-18', 'male', 'Adult', NULL, 'YU, ALAN', NULL, 'https://www.facebook.com/alanjhyu', 'http://www.imdb.com/name/nm3595948/', '', '', NULL, 'Yes', 'No', '3.jpg', NULL, NULL, '2019-07-13 01:23:09'),
(47, 'Alan', NULL, 'Jones', 'alanbjones@protonmail.com', '$2y$10$vAM3LioldsJ.XQFqlnGhQepjbrJdirscOFo6vpVBLzMna8FTtq.qO', '604-615-6027', '33428 Harbour Avenue', 'Mission', 'BC', 'V2V 2W4', 'Canada', '1954-10-05', 'male', 'Adult', NULL, 'JONES, ALAN', NULL, 'Alan Jones Mission BC', NULL, '', '', NULL, 'No', 'No', 'F82F3967-4B29-490B-8906-A2CBD8805FB2.jpeg', NULL, NULL, '2019-07-13 01:23:26'),
(48, 'Alan', 'Frank', 'Homer', 'alhomer56@gmail.com', '$2y$10$M7hiA9gCotLYBqbgFWjXo.njguVXJwwQj6EuBVeEuqYaDFTcqG.Qu', '2503181772', '760 Settlement Road ', 'Kamloops ', 'BC', 'V2b8c9 ', 'Canada', '1968-06-24', 'male', 'Adult', 'alan.homer', 'HOMER, ALAN FRANK', NULL, 'Alan Homer', NULL, '', '', NULL, 'No', 'No', 'F970BD9C-4F96-4C96-BE46-8B67C71BEAD0.jpeg', NULL, NULL, '2019-07-13 01:23:45'),
(49, 'Alana', 'Dawn', 'Der', 'sookiebooboosausage@gmail.com', '$2y$10$FVrWSUW2Lv/2QvzGMmw7Aegy0HHjtUYWbY1ZnbjmxpTSM0YVT35iW', '6043785267', 'Box 125', 'Boston Bar', 'BC', 'V0K1C0', 'Canada', '1975-05-25', 'female', 'Adult', NULL, 'DER, ALANA DAWN', NULL, 'Alana dawn ', NULL, '', '', NULL, 'No', 'No', '20180902_141520.jpg', NULL, NULL, '2019-07-13 01:25:18'),
(50, 'Alana ', 'Cosco', 'Richardson', 'alana-richardson@live.com', '$2y$10$aFKpMM6Ivh8iVfVDB45a.OFGX1r15SRjWUoMX3sKNhVsjFRK36QPS', '604-897-7973', '301 - 528 Como Lake Avenue', 'Coquitlam', 'BC', 'V3J3M1', 'Canada', '1991-10-17', 'female', 'Adult', NULL, 'RICHARDSON, ALANA  COSCO', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Alana-Richardson-Face-Close-Up.png', NULL, NULL, '2019-07-13 01:26:14'),
(51, 'Albert ', NULL, 'Chen', 'bertchen123@gmail.com', '$2y$10$au5DtJzml5i0ZY14y0NytOeBlmBj9BTkRvrbsS7LVzHxNbZ6V84c2', '7788225168', '4584 Rumble st', 'Burnaby', 'BC', 'V5J 2A5', 'Canada', '2000-10-11', 'male', 'Adult', NULL, 'CHEN, ALBERT', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Albert_face.jpg', NULL, NULL, '2019-07-13 01:26:27'),
(52, 'Alden', 'C. ', 'Mayfield', 'okyacm@yahoo.com', '$2y$10$hSBNHoZ19jFQAnjihW6HC.H61aSUM9OOU8v.XX4jXDYJ.VIKONPsK', '778-549-2384', '20053A 68th Avenue', 'Langley', 'BC', 'V2Y 0T5', 'Canada', '2018-08-23', 'male', 'Adult', NULL, 'MAYFIELD, ALDEN C.', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Mayfield.jpg', NULL, NULL, '2019-07-13 01:26:42'),
(53, 'Aleah', 'Hazel Rose', 'Klassen', 'kelly_klassen@shaw.ca', '$2y$10$deJp0Hr9VTbn8exa.LEctevdxUYk7mgALVJIU2K08FthCwEa5iN8a', '6046142025', '34602 Somerset ave', 'Abbotsford ', 'BC', 'V2S6M9 ', 'Canada', '2009-06-16', 'female', 'Child', NULL, 'KLASSEN, ALEAH HAZEL ROSE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '2C3FF14F-B540-4D8C-BAA4-395547535E31.jpeg', NULL, NULL, '2019-07-13 01:26:51'),
(54, 'Alejandra', NULL, 'Gadea  Lopez', 'ale.gl_08@hotmail.com', '$2y$10$gE5u5LwJuel9y8oWANkexelJgBC66xvy.7vqQHf0H5Cx5tMtQniL.', '6047263428', '6116 128st', 'Surrey', 'BC', 'v3x1t1', 'Canada', '1996-12-08', 'female', 'Adult', '@alejandra_xoxo', 'GADEA  LOPEZ, ALEJANDRA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Screen-Shot-2018-08-03-at-10.17.05-PM.png', NULL, NULL, '2019-07-13 01:27:12'),
(55, 'Alex', NULL, 'Millar', 'amjpd5@gmail.com', '$2y$10$Ad4jF2oaBTIg.ZJY4CmdvOYsaxhvR1lb/1BCa5MO/e6ULYn2QAwNO', '6048974646', '6241 Golf Road', 'Agassiz', 'BC', 'V0M 1A3', 'Canada', '2003-12-10', 'male', 'Teen', NULL, 'MILLAR, ALEX', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '1BA8CE4E-0191-4D6A-8C20-A832676830DE.jpeg', NULL, NULL, '2019-07-13 01:27:38'),
(56, 'Alexa', 'Rae', 'Ross', 'alexa4loans@aol.com', '$2y$10$qDKluCsWbN4A9FzTlfKdte.7G3BWgDB2JwGqX8o4.V/wscsSL0vD2', '778-240-2776', '29433 Silver Crescent', 'Mission', 'BC', 'V4S1J1', 'Canada', '1976-04-26', 'female', 'Adult', NULL, 'ROSS, ALEXA RAE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Alexa-Ross-1.jpg', NULL, NULL, '2019-07-13 01:28:46'),
(57, 'Alexa', NULL, 'Taylor', 'alexa_taylor@hotmail.com', '$2y$10$MDDdbPMjZM9p8gwJLpQANuEIWaYYMS9pIyUyKzklWwfErAQgX4emm', '7788668169', '108-2131 W 3rd Avenue', 'Vancouver', 'BC', 'V6K 1L3', 'Canada', '1989-09-18', 'female', 'Adult', NULL, 'TAYLOR, ALEXA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_20180622_164746_812-1.jpg', NULL, NULL, '2019-07-13 01:29:18'),
(58, 'Alexa', 'Nadine', 'Kortas', 'karen.winfield@gmail.com', '$2y$10$rqK1f7QlWOo7e2v/Iycbwu6l.p27GZaWP1D5kwz4WkVW7xBtE2sdy', '403-680-4820', '3541 Proudfoot Place', 'Victoria', 'BC', 'V9C 4L9', 'Canada', '2005-05-10', 'female', 'Teen', NULL, 'KORTAS, ALEXA NADINE', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'DSC_4718.jpg', NULL, NULL, '2019-07-13 01:29:38'),
(59, 'Alexander', 'Jay', 'Ross', 'aross8725@gmail.com', '$2y$10$5oHCyM1NSxorAFExs2foqu.i.JfUkx6Vr2wno9dhgJS9LIQ/d/0Ee', '6047868607', '23231 Aurora Place', 'Maple Ridge', 'BC', 'V2X 0J5', 'Canada', '1982-05-04', 'male', 'Adult', NULL, 'ROSS, ALEXANDER JAY', NULL, NULL, NULL, '', '', NULL, 'Yes', 'Yes', 'alex-nb-.jpg', NULL, NULL, '2019-07-13 01:29:53'),
(60, 'Alexander', NULL, 'Lee', 'alexsunlee@gmail.com', '$2y$10$ajqg4Eon2qZqjYysEZVkqutTz/yuFlLFpwiWNIsRu4lB4qnSIjhu6', '7789997756', '1040 Westmount Drive', 'Port Moody', 'BC', 'v3h1k9', 'Canada', '1969-01-03', 'male', 'Adult', NULL, 'LEE, ALEXANDER', NULL, NULL, NULL, '', '', NULL, 'Yes', 'Yes', 'sf_180907_0280.jpg', NULL, NULL, '2019-07-13 01:30:05'),
(61, 'Alexander ', 'Levi', 'Killam', 'irwin@baypub.ca', '$2y$10$UE5oddCYtHeS8PfzAQvbReNbwO6lmSxYTfX6oIPE0EpTocnBCN3mq', '12507100010', '1433 Highland place', 'Cobble Hill ', 'BC', 'V0R 1L3', 'Canada', '2008-05-11', 'male', 'Child', NULL, 'KILLAM, ALEXANDER  LEVI', NULL, NULL, 'Alexander Killam', '', '', NULL, 'Yes', 'Yes', NULL, NULL, NULL, NULL),
(62, 'Alexey', NULL, 'Gammer', 'alexeygammer@gmail.com', '$2y$10$12W.84NPHVKLEdPAIL/YfOJSTKYsWKjjZMr0f8h4iMXStfQdWiFVG', '250-486-0795', '113 Van Horne St', 'Penticton', 'BC', 'V2A 4K1', 'Canada', '1984-07-08', 'male', 'Adult', NULL, 'GAMMER, ALEXEY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'fmkam1-2.jpg', NULL, NULL, '2019-07-13 01:30:36'),
(63, 'Alexia ', 'Lynne', 'Bell', 'thesnooz@hotmail.com', '$2y$10$cstyhSpqlY1pBTTp33Jpb.kEio9M0f85MsrPPly/assqLSy8Rso5G', '2505747663', '47220 Vista Place', 'Chilliwack ', 'BC', 'V2R0R9 ', 'Canada', '2011-04-05', 'female', 'Child', NULL, 'BELL, ALEXIA  LYNNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '6450DE71-0BD3-463E-8298-A431B460C1E3.jpeg', NULL, NULL, '2019-07-13 01:30:58'),
(64, 'Alexis', NULL, 'Lawlor', 'hollykara81@gmail.com', '$2y$10$kSQgaDAMMXXxuFYRsTjCZeVxBu4nN9SjWAlV6rIZrSnL5mxY/qLdK', '6047938908', '5238 crimson ridge ', 'Chilliwack', 'BC', 'V2r5w7 ', 'Canada', '2009-08-28', 'female', 'Child', NULL, 'LAWLOR, ALEXIS', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '67FC4588-05F8-4EF7-8AE3-BB4EBA61C38F.jpeg', NULL, NULL, '2019-07-13 01:31:42'),
(65, 'Alexis ', NULL, 'MacRae', 'alexismmacrae@hotmail.com', '$2y$10$PQgZFU7haVB9rsfN2GlfeubwWJ9uKFKFoBrxphkjYFqoOVTZGE/7q', '7783192645', '2057 e1st avenue ', 'Vancouver ', 'BC', 'V5n1b6', 'Canada', '1990-11-29', 'female', 'Adult', 'Alexis.macrae', 'MACRAE, ALEXIS', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_1919.jpg', NULL, NULL, '2019-07-13 01:32:11'),
(66, 'Alice ', NULL, 'Kyle', 'bailey.ja@gmail.com', '$2y$10$ICJ3O35Y45YpMzsa5Vw8qeO1KBnicRQQk4nZUZKPERUaG1y7.Kr12', '2508181996', '2656 Bukin Dr E', 'Victoria ', 'BC', 'V9e1h4 ', 'Canada', '2010-12-24', 'female', 'Child', NULL, 'KYLE, ALICE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'C755D968-C0F9-4480-B5F6-EF7D06F363E9.jpeg', NULL, NULL, '2019-07-13 01:32:33'),
(67, 'Alicia', NULL, 'Chappell', 'Alicia.chappell86@gmail.com', '$2y$10$.dIFdYmr.1rSzRZj/Xk69ODizCd9XwoOnriFvRg8tk0NQ2ly6GKZW', '7786846009', '46158 stoneview drive, unit 1', 'Chilliwack BC', 'BC', 'V2R 5W8', 'Canada', '1986-07-07', 'female', 'Adult', 'Aliciaannechappell', 'CHAPPELL, ALICIA', NULL, 'https://m.facebook.com/alicia.chappell.773', NULL, '', '', NULL, 'No', 'No', 'E10276A4-1961-48FE-BF27-536D06363BE2.jpeg', NULL, NULL, '2019-07-13 01:33:37'),
(68, 'Alicia-Rae', 'Michelle', 'Borth', 'aliciaborth@yahoo.ca', '$2y$10$Ouf0Af24phWU5Ytswzbg2ujb8ZLxy8wA16BLO7vDNcHuZixXdL9Ee', '2508989423', '5733 North Island Highway', 'Courtenay', 'BC', 'V9j1t3', 'Canada', '1997-05-06', 'transgender', 'Adult', 'https://www.instagram.com/aliciaborth/', 'BORTH, ALICIA-RAE MICHELLE', 'https://www.linkedin.com/in/alicia-b-54571183/', NULL, NULL, '', '', NULL, 'No', 'No', '0E54AE77-C2D8-468A-B702-C3CB49309C3D.jpg', NULL, NULL, '2019-07-13 01:33:52'),
(69, 'Alisa', NULL, 'Gil Silvestre', 'alisa.silvestre@gmail.com', '$2y$10$xK4.Zq9yshPzjQXMmSfgl.tfK3gqFprDr4M.3t0FCf.dlJtrrwhBC', '6042402426', '5751 Cedarwood Street', 'burnaby', 'BC', 'v5g2k7', 'Canada', '1996-02-15', 'female', 'Adult', NULL, 'GIL SILVESTRE, ALISA', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'IMG_A106C45E246B-1.jpeg', NULL, NULL, '2019-07-13 01:33:59'),
(70, 'Aliza', NULL, 'Boszak', 'xaajboss@icloud.com', '$2y$10$m15..BPDyuVYGSDENigIMu198XeWzbgiLUaWtkDO/yunE/AF4p7CC', '2368884790', '2563 152 st', 'Surrey', 'BC', 'V4p1n4', 'Canada', '2004-03-07', 'female', 'Teen', NULL, 'BOSZAK, ALIZA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '7B343806-F697-4FAB-AF1D-195FF43F98DD.jpeg', NULL, NULL, '2019-07-13 01:34:17'),
(71, 'Allan', NULL, 'Luna', 'luna.allan@hotmail.com', '$2y$10$KPWRRQGIyUBWMRX7rubP9.iBlNUS.8ZTLLcLsn8Wy9U1Jiz4fchIq', '7788783418', '1696 W.', 'Vancouver', 'BC', 'v6p2v7', 'Canada', '1987-06-21', 'male', 'Adult', NULL, 'LUNA, ALLAN', NULL, NULL, NULL, '', '', NULL, 'Yes', 'Yes', 'thumbnail_IMG_1189.jpg', NULL, NULL, '2019-07-13 01:34:36'),
(72, 'Allison', NULL, 'Devereaux', 'allie.dev@gmail.com', '$2y$10$Mum/nYNErfqHxepeX259R.E0yTHJzWlqM6ykzxbY7IFsDiqt3WBdm', '604-838-6525', '804-1924 Barclay St', 'Vancouver', 'BC', 'V6G1L3', 'Canada', '1996-07-20', 'female', 'Adult', 'alliedevereaux', 'DEVEREAUX, ALLISON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'ADevereauxHS_NG.jpg', NULL, NULL, '2019-07-13 01:34:55'),
(73, 'Allison', NULL, 'Warren', 'AllyWarren@yahoo.com', '$2y$10$CBETLnqZt0ZEeDif7KqVNOnl.lm6bV/q7VdEMhxWAt/Rvl1e8a7ma', '6045124472', '1117-2012 Fullerton Avenue', 'North Vancouver', 'BC', 'V7P 3E3', 'Canada', '1967-12-09', 'female', 'Adult', 'screensiren', 'WARREN, ALLISON', 'hotmarkets', 'ally warren', 'IMDB.me/allywarren', '', '', NULL, 'Yes', 'Yes', 'Ally-Warren-Current-2018.jpg', NULL, NULL, '2019-07-13 01:35:21'),
(74, 'ALLISON (Ally)', NULL, 'WARREN ', 'AllyWarren@yahoo.com', '$2y$10$DNSUIHsu.vgHz.StGYdS6.kvFAsW7LrCnr.H4zxKTmbtWRK3KFxKq', '6045124472', '1117-2012 Fullerton Avenue', 'North Vancouver', 'BC', 'V7P 3E3', 'Canada', '1974-12-09', 'female', 'Adult', 'screensiren', 'WARREN , ALLISON (ALLY)', 'hotmarkets', 'ally warren', 'allywarren', '', '', NULL, 'No', 'Yes', 'AllyWarren-Mar-2018.png', NULL, NULL, '2019-07-13 01:35:44'),
(75, 'Ally', NULL, 'Warren', 'alleywarren@hotmail.com', '$2y$10$wsqwGJ.HSBlfFRsKmt3ONuj7DcW2LaJbGgTYIv6f.fKblEYSmYty.', '6045124472', '1117-2012 Fullerton Avenue', 'North Vancouver', 'BC', 'V7P3E3', 'Canada', '1978-12-09', 'female', 'Adult', NULL, 'WARREN, ALLY', NULL, NULL, 'imdb.me/allywarren', '', '', NULL, 'No', 'Yes', 'Ally-Warren-Current-2018.jpg', NULL, NULL, '2019-07-13 01:36:28'),
(76, 'Alycia', NULL, 'Escoval', 'escovalalycia@gmail.com', '$2y$10$VxWlT8177/qRr4Td/FN/zeyVTFpBzYoRP4E7LoJxF8OZ4aZFugUTe', '7786790552', '2835 Ronald RD ', 'Victoria', 'BC', 'v9b2l7', 'Canada', '1998-09-17', 'female', 'Adult', NULL, 'ESCOVAL, ALYCIA', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'AlyciaEscoval063_WEB.jpg', NULL, NULL, '2019-07-13 01:36:59'),
(77, 'Alysha', NULL, 'Seriani', 'alyshaseriani@gmail.com', '$2y$10$xmcv8n31mVNk.Chls5MTY.uq0F0avfykU2LaTX0lB3RByUy.oDwWq', '7782556591', '#405 - 2056 Franklin Street', 'Vancouver', 'BC', 'V5L1R3', 'Canada', '1994-03-07', 'female', 'Adult', 'http://instagram.com/alyseriani', 'SERIANI, ALYSHA', 'https://www.linkedin.com/in/alyshaseriani/', 'https://www.facebook.com/alyshaseriani', 'https://www.imdb.com/name/nm7032168/?ref_=nv_sr_2', '', '', NULL, 'No', 'No', '20180425_133708.jpg', NULL, NULL, '2019-07-13 01:37:29'),
(78, 'Alyssa', NULL, 'Yong', 'alyssayong@hotmail.com', '$2y$10$B4XKxp4avnhA8GkjgQayWOvDGFSwvml6vaCtM4n1ZpwEWWEmPXaAe', '7787880800', '#204-5733 Vine street', 'Vancouver', 'BC', 'V6M4A2', 'Canada', '1999-01-12', 'female', 'Adult', '@_alyssayong', 'YONG, ALYSSA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'AlyssaYong_Photo2.jpg', NULL, NULL, '2019-07-13 01:37:47'),
(79, 'Amadora', 'Kimberly', 'Neufeld', 'dustinmarks@hotmail.com', '$2y$10$LFrMTCTsDpFhCpl7j.kFBOyxfhDEbDyxBXuKB8wUzx6DcHhjAXVhu', '6043166469', 'PO box 858, 459 Naismith Avenue', 'Harrison Hot Springs', 'BC', 'V0M1K0', 'Canada', '2008-04-21', 'female', 'Child', NULL, 'NEUFELD, AMADORA KIMBERLY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '54364864_424206491667970_3421900994202566656_n.jpg', NULL, NULL, '2019-07-13 01:38:00'),
(80, 'Aman', NULL, 'Webeshet', 'amanwebelew@gmail.com', '$2y$10$qRZ2lGKZjiZmAEAKQOCBLeuE9ltgVYPZcpBjwZmFZnc.5xmtjygim', '778 708 6983', '3311 venables ', 'Vancouver', 'BC', 'v5k2s7', 'Canada', '1992-01-24', 'male', 'Adult', NULL, 'WEBESHET, AMAN', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'aman-webeshet-web_4949.jpg', NULL, NULL, '2019-07-13 01:38:27'),
(81, 'Amanda', NULL, 'Early', 'amandaearly78@gmail.com', '$2y$10$/SoaC15sOGolNUFpGmB6gubKRDPVs3GWkijTsRAq1KxcxetF4bpuu', '604-999-1897', '20757 90 Ave Langley', 'Langley', 'BC', 'V1M 2N4', 'Canada', '2000-02-06', 'female', 'Adult', 'amanda.early', 'EARLY, AMANDA', NULL, 'Amanda Early', NULL, '', '', NULL, 'Yes', 'No', 'amandaearly1.jpg', NULL, NULL, '2019-07-13 01:38:58'),
(82, 'Amanda', 'May', 'Klop', 'amandaklop1@gmail.com', '$2y$10$Tbth8c1dVhBYLM0u0Rdwfuqal/m31LVfU7KniW2tRJY2ztoyGF7ze', '6048190708', '4905 hunt rd ', 'Agassiz ', 'BC', 'V0m1a3', 'Canada', '1990-05-11', 'female', 'Adult', '@_amanda_klop', 'KLOP, AMANDA MAY', NULL, 'Amanda klop', NULL, '', '', NULL, 'No', 'No', '005BE3EF-1077-4C7B-9B07-DA6128AC970B.jpeg', NULL, NULL, '2019-07-13 01:40:31'),
(83, 'Amanda', 'Judy', 'Lac seul', 'amandalacseul93@gmail.com', '$2y$10$wWJj2gvVnh4tqKp0gFekzuN8UYc1h5pi50s7XQyqn5Ge9zycOptum', '+1 (306) 485-1470', '46179 princess ave ', 'Chilliwack', 'BC', 'V2p 4l9', 'Canada', '1993-03-18', 'female', 'Adult', 'Amandalacseul', 'LAC SEUL, AMANDA JUDY', NULL, 'Amanda Lacseul ', NULL, '', '', NULL, 'No', 'No', '98A9A6AA-552F-4084-9E72-FBE1EE4BD8D0.jpeg', NULL, NULL, '2019-07-13 01:40:38'),
(84, 'Amanda', 'Lea', 'Christie', 'a_manda.a@hotmail.com', '$2y$10$/T3A7bRbuQiutv766b6SKOXTf1..7VeTfnCKUSOqokDNh/K6Wa.0e', '6043165778', '1747 garden place ', 'Agassiz', 'BC', 'V0m 1a2', 'Canada', '1991-02-28', 'female', 'Adult', 'aamandachristie', 'CHRISTIE, AMANDA LEA', 'https://www.linkedin.com/feed/', 'Amanda Christie', NULL, '', '', NULL, 'No', 'No', 'E1B56772-D94B-423E-91F0-10A31C7B8790.jpeg', NULL, NULL, '2019-07-13 01:40:44'),
(85, 'Amanda', 'Joan', 'Ned', 'eyes_life@hotmail.com', '$2y$10$F0jyiJRUa3K9v80vcJn7.ufN6vEMQm/RHSDh1t7mKSNAe3pYmMuPC', '7786844882', '205-9245 Edwards Street', 'Chilliiwack', 'BC', 'V2P4C5', 'Canada', '1982-10-08', 'male', 'Adult', 'Am7anda', 'NED, AMANDA JOAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '18077072_10154264894335881_5702663079757369986_o.jpg', NULL, NULL, '2019-07-13 01:40:57'),
(86, 'Amanda', NULL, 'Hendrickson', 'amycfn@gmail.com', '$2y$10$AWKnyguiKAtfdwc3EfKzbelRwqMFbER574uhbNNuolLqLzkH5sgqe', '6048605808', 'PO box 2239', 'Hope', 'BC', 'V0X 1L0', 'Canada', '1981-02-12', 'female', 'Adult', NULL, 'HENDRICKSON, AMANDA', NULL, 'Amy Peters Hendrickson ', NULL, '', '', NULL, 'No', 'No', '3C71AD4E-7FC3-4671-8B74-D00D144C25CF.jpeg', NULL, NULL, '2019-07-13 01:41:07'),
(87, 'Amanda', 'Kaila', 'Early', 'amandaearly78@gmail.com', '$2y$10$DgPnEP4yJabDzT8IXXSywe9o0zkD7HHFLYSTRabq25VSPF.E8RBcK', '604-999-1897', '20757 90 Ave', 'Langley ', 'BC', 'V1M 2N4', 'Canada', '2000-02-06', 'male', 'Adult', 'amanda.early', 'EARLY, AMANDA KAILA', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'amandaearly1.jpg', NULL, NULL, '2019-07-13 01:41:46'),
(88, 'Amanda ', 'Taylor ', 'Jones ', 'atjones@quadro.net', '$2y$10$S0FB6etlMv350/nCNK9F6u4upUnMFtohGrkhIkMPnnCGb0.QkgxCK', '519-282-1343', '7600 Glover Rd', 'Langley ', 'BC', 'V2Y 1Y1', 'Canada', '1999-09-09', 'female', 'Adult', 'a_jones 9', 'JONES , AMANDA  TAYLOR', NULL, 'Amanda Jones ', NULL, '', '', NULL, 'No', 'No', 'IMG_3116-copy.jpg', NULL, NULL, '2019-07-13 01:42:18'),
(89, 'Amandine', NULL, 'Bidgood', 'mand.b@me.com', '$2y$10$mHbE5bUVqD1HImBtMrge8O/FncKOjFOQgD93bHRQDFBBgP7.u2jcG', '7782235410', '3404-833 Seymour St', 'Vancouver', 'BC', 'V6B 0G4', 'Canada', '1973-03-14', 'female', 'Adult', NULL, 'BIDGOOD, AMANDINE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '5G7A9106r.jpg', NULL, NULL, '2019-07-13 01:43:16'),
(90, 'Amaris', NULL, 'Marks', 'meredith.holly@icloud.com', '$2y$10$YWo3kz96d9DhjAS/0Httju1NCVoieYjSnbUgeE5TDoXQ18X5ehF6C', '2505883778', '1345 Neild Rd', 'Metchosin', 'BC', 'V9C4H4', 'Canada', '2009-01-23', 'female', 'Child', 'Tribeofmarks', 'MARKS, AMARIS', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(91, 'amarpreet ', 'Singh', 'chhina', 'amarpreetchhina@yahoo.in', '$2y$10$0CNY2V.GpcnEPvIH7Sq0POs2/CzighreuyyhXGLTBMTmWTkEHcalu', '604-613-6768', '12979 74 Avenue', 'surrey', 'BC', 'V3W 1C3', 'Canada', '1994-04-06', 'male', 'Adult', '@amarpreetchhina@amuchhina', 'CHHINA, AMARPREET  SINGH', NULL, 'chhinaamarpreet', NULL, '', '', NULL, 'No', 'No', 'Amar_first-selects_NickThiessen-311-of-319.jpg', NULL, NULL, '2019-07-13 01:43:30'),
(92, 'Amber', 'Erica', 'Webber', 'amberlightning@gmail.com', '$2y$10$jO8zfeickzQRE7SUUI7CyewiD6L8Pz0VdYuG6vacZROWcI1KtkbM.', '16048081629', '#11 - 1575 east 1st ave', 'vancouver', 'BC', 'v5n1a6', 'Canada', '1980-06-18', 'male', 'Adult', '@amberlightning', 'WEBBER, AMBER ERICA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'unnamed.png', NULL, NULL, '2019-07-13 01:43:58'),
(93, 'Amber', 'Natashia', 'Petersen', 'art@amberpetersen.ca', '$2y$10$C3WhfKfjHOMQpmOkqpJuDuulEwYzBhvtTpXkoQ5HbeebkQTdz2/lS', '7789771110', '12835 Gulfview Road, PO Box 103', 'Madeira Park', 'BC', 'V0N 2H0', 'Canada', '1983-03-11', 'female', 'Adult', 'http://www.instagram.com/ambernp', 'PETERSEN, AMBER NATASHIA', NULL, 'http://www.facebook.com/ambernatashiapetersen', 'http://www.imdb.me/amberpetersen', '', '', NULL, 'No', 'No', 'Amber-Petersen-headshot.jpg', NULL, NULL, '2019-07-13 01:44:23'),
(94, 'Amber', NULL, 'Tauber', 'amber.justine.tauber@gmail.com', '$2y$10$e.C9.TFpLnhHD9vyWyDQzeiXAjfJRKIAPlQrqUqKPfFdg4OtqQlPy', '6049911511', '9 9493 Broadway st ', 'Chilliwack', 'BC', 'V2p 5t8', 'Canada', '1990-09-30', 'female', 'Adult', NULL, 'TAUBER, AMBER', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'FB_IMG_1551814012677.jpg', NULL, NULL, '2019-07-13 01:45:06'),
(95, 'Amber', NULL, 'Lote', 'amberlote@gmail.com', '$2y$10$JOSBXc5t/gf4l6n0WfUJFu9VjmkP0AHOpFXDKv4WJh86reaj3Hpdu', '2507449230', '2120 Tanlee Crescent', 'Saanichton', 'BC', 'V8M 1M8', 'Canada', '1989-11-16', 'female', 'Adult', '@theyogapixie', 'LOTE, AMBER', NULL, 'Amber Lote', NULL, '', '', NULL, 'No', 'No', '20245546_10158943397130005_8009042789014940340_n.jpg', NULL, NULL, '2019-07-13 01:45:15'),
(96, 'Ambrose', NULL, 'Gardener', 'ambrose.c.gardener@hotmail.com', '$2y$10$iNXwEShvvxFC.J9vAROf7.Kx1i9eVl8b6njAiNRX0be.Oz5ni7y.S', '604 440 4518', '102-1110 Davie St', 'Vancouver', 'BC', 'V6E 1N1', 'Canada', '1992-03-19', 'male', 'Adult', 'https://www.instagram.com/ambrose_gardener/', 'GARDENER, AMBROSE', NULL, 'https://www.facebook.com/AmbroseChristopher', 'https://www.imdb.com/name/nm7563410/?ref_=fn_al_nm_1', '', '', NULL, 'Yes', 'No', 'HS-Smile.jpg', NULL, NULL, '2019-07-13 01:45:50'),
(97, 'Amelia', NULL, 'Croft', 'gilliancroft@icloud.com', '$2y$10$VQPWoei34shw4YJtm4o5IOyn14ocwBK2Y9ltkmcZ5itcs4w.1iI3W', '(250) 858-2789 Texts picked up frequently.', '2065 Pauls Terrace', 'Victoria', 'BC', 'V8N 2Z4', 'Canada', '2004-12-29', 'female', 'Teen', NULL, 'CROFT, AMELIA', NULL, 'https://www.facebook.com/AmeliaCroftFanPage/', 'https://pro.imdb.com/name/nm6249350?ref_=nm_nv_mp_profile', '', '', NULL, 'Yes', 'No', 'AmeliaCroft05202019PrimCloseupCOMP.jpg', NULL, NULL, '2019-07-13 01:47:25'),
(98, 'Amelie ', 'Leah Marie', 'French', 'hellokittyninja@gmail.com', '$2y$10$sooIJigqVDq40DstviXfzugkCUjB.OhgdcJMZSJn197aeXV0U2BVC', '6047814262', '1119 Jolivet Cres.', 'Victoria', 'BC', 'V8X 3P3', 'Canada', '2009-02-02', 'female', 'Child', NULL, 'FRENCH, AMELIE  LEAH MARIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_1401-2.jpg', NULL, NULL, '2019-07-13 01:48:31'),
(99, 'Amie', NULL, 'Sokugawa', 'amiesokugawa@gmail.com', '$2y$10$zx2xyRXoxkHAyHvdCg0Nbe2/cUyjuBF8.MtIE8RUGL5aY8L7nm7L6', '6049619277', '325 Alberta Street', 'New Westminster', 'BC', 'V3L 3J4', 'Canada', '1999-08-22', 'female', 'Adult', NULL, 'SOKUGAWA, AMIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_0151.jpg', NULL, NULL, '2019-07-13 01:48:53'),
(100, 'Amira', NULL, 'Anderson', 'amira.marseilles@gmail.com', '$2y$10$i1v6zj0F3.HHpMrrQJJtYeLp6Ws/3LHE0XONv4VrBPtvroTxAj.0G', '2896874882', '183 East Georgia St', 'Vancouver', 'BC', 'V6A 0E5', 'Canada', '1995-07-14', 'female', 'Adult', '@amira.m.anderson', 'ANDERSON, AMIRA', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'AmiraAndersonHeadshot2.jpg', NULL, NULL, '2019-07-13 01:49:01'),
(101, 'Ana', NULL, 'Arciniega', 'arciniega.anapao@gmail.com', '$2y$10$5VXDV38tOKNfWT2bVHndYOuMIlWk1kiqmr7P.v/59rFSDwpKyyxi.', '7787141912', '2885 Capilano Road', 'North Vancouver', 'BC', 'V7R4H4', 'Canada', '1997-01-01', 'female', 'Adult', 'Anaarciniega', 'ARCINIEGA, ANA', NULL, 'Aarciniegat', NULL, '', '', NULL, 'No', 'No', 'IMG_6043.jpg', NULL, NULL, '2019-07-13 01:49:26'),
(102, 'Andrea', NULL, 'Mayo', 'victorandrea2016@gmail.com', '$2y$10$ljMKAaUnDFaNpZmxdCb7ZOfoUbDcJ1u91KGJ6unWkFzoLT3Ki4Sde', '7785226296', '6204 Logan Lane', 'Vancouver', 'BC', 'V6T2K9', 'Canada', '1984-09-13', 'female', 'Adult', NULL, 'MAYO, ANDREA', 'https://www.linkedin.com/in/andreamayo/', NULL, 'https://m.imdb.com/name/nm4328243/', '', '', NULL, 'No', 'No', '20180623_154415.jpg', NULL, NULL, '2019-07-13 01:49:43'),
(103, 'Andrea', 'Catherine', 'Cummings', 'andreacummings02@hotmail.com', '$2y$10$gvxWzPtGoqX4boVDDtwVe.95sgw/LTnjMHjvpmOFqWldLKv7Xs42W', '7788924342', '#308 4363 Halifax Street', 'Burnaby', 'BC', 'v5c 5z3', 'Canada', '2018-06-11', 'female', 'Adult', 'a.cmmngs', 'CUMMINGS, ANDREA CATHERINE', NULL, 'cummingsandrea', NULL, '', '', NULL, 'No', 'No', 'IMG_0126-Copy.jpg', NULL, NULL, '2019-07-13 01:50:05'),
(104, 'Andrea', 'Leah', 'Punt', 'andreapunt@hotmail.ca', '$2y$10$8roPWw48jkTwKeCs8WE3Y.slYirL56jNg3G9bdLvkezJyKCYAN4i6', '6046799340', '794 Upper Crescent', 'Britannia beach', 'BC', 'V0N1J0', 'Canada', '1984-07-20', 'female', 'Adult', NULL, 'PUNT, ANDREA LEAH', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '24198E50-DA76-4F08-BBCD-363779D8C822.jpeg', NULL, NULL, '2019-07-13 01:50:32'),
(105, 'Andrea', 'Michelle', 'MacSween', 'andreamack_10@hotmail.com', '$2y$10$.ktk3mK8gbGaB6.RDiBHmeTJbif//pX63hRVatxWlypxnQyqn1SRu', '9028801124', '5474 McCallum Road', 'Agassiz', 'BC', 'V0m1a1 ', 'Canada', '1987-09-21', 'female', 'Adult', 'Andimichelle1021', 'MACSWEEN, ANDREA MICHELLE', NULL, 'Andrea Michelle ', NULL, '', '', NULL, 'No', 'No', '215D24D9-6B7C-4A7F-9CC0-9402A373C432.jpeg', NULL, NULL, '2019-07-13 01:50:40'),
(106, 'Andrea', NULL, 'Aidoo', 'andrea1023@live.com', '$2y$10$a1VJly3iAvYlftyykW5qNeVwO9rC./HUk6Kl38GwAXSbjiRMJD6fq', '7788613504', '1021 st andrews st', 'new westminister', 'BC', 'v3m1w4', 'Canada', '2001-10-06', 'female', 'Adult', 'https://www.instagram.com/andrea.aidoo/', 'AIDOO, ANDREA', NULL, 'https://www.facebook.com/andrea.aidoo.3', NULL, '', '', NULL, 'No', 'No', 'IMG_3122.jpg', NULL, NULL, '2019-07-13 01:50:49'),
(107, 'Andrej', NULL, 'Krestan', 'krexpro@gmail.com', '$2y$10$/j1.b14QEwgHC.Dm1.8vEeM1vvm9ZFkRQsCDyCFAKh4YVK9X0u2z2', '12508868629', '140-6171 willingdon ave', 'burnaby', 'BC', 'v5h2t9', 'Canada', '1989-12-15', 'male', 'Adult', 'andrej_wsb', 'KRESTAN, ANDREJ', 'Andrej Krestan', 'Andrej Krestan', 'https://m.imdb.com/name/nm8091833/?ref=m_nv_sr_1', '', '', NULL, 'Yes', 'Yes', '667FB7C5-D215-4FE7-A253-99333488D12B.jpeg', NULL, NULL, '2019-07-13 01:51:03'),
(108, 'Andrej', NULL, 'Krestan', 'krexpro@gmail.com', '$2y$10$AJ.vrvViB0UWat3406Nnt.lkT0V/XsrzcLzNl2Rjv.dYCCdzXCmsK', '12508868629', '140-6171 willingdon ave', 'burnaby', 'BC', 'v5h2t9', 'Canada', '1989-12-15', 'male', 'Adult', 'andrej_wsb', 'KRESTAN, ANDREJ', 'Andrej Krestan', 'Andrej Krestan', 'https://m.imdb.com/name/nm8091833/?ref=m_nv_sr_1', '', '', NULL, 'Yes', 'Yes', '667FB7C5-D215-4FE7-A253-99333488D12B-1.jpeg', NULL, NULL, '2019-07-13 01:51:33'),
(109, 'Andrej', NULL, 'Krestan', 'krexpro@gmail.com', '$2y$10$TwnVC3pWP47zrM.0Qz7WeuTkECdbIf2.UR.I34r6q6wf37FmNEMGq', '12508868629', '140-6171 willingdon ave', 'burnaby', 'BC', 'v5h2t9', 'Canada', '1989-12-15', 'male', 'Adult', 'andrej_wsb', 'KRESTAN, ANDREJ', 'Andrej Krestan', 'Andrej Krestan', 'https://m.imdb.com/name/nm8091833/?ref=m_nv_sr_1', '', '', NULL, 'Yes', 'Yes', '667FB7C5-D215-4FE7-A253-99333488D12B-2.jpeg', NULL, NULL, '2019-07-13 01:52:21'),
(110, 'Andrej', NULL, 'Krestan', 'krexpro@gmail.com', '$2y$10$JzJejniLSzSsOXmiyWvRyeuzNaAxq8vR6v/2KN.v.Be5A0Epa3n1a', '12508868629', '140-6171 willingdon ave', 'burnaby', 'BC', 'v5h2t9', 'Canada', '1989-12-15', 'male', 'Adult', 'andrej_wsb', 'KRESTAN, ANDREJ', 'Andrej Krestan', 'Andrej Krestan', 'https://m.imdb.com/name/nm8091833/?ref=m_nv_sr_1', '', '', NULL, 'Yes', 'Yes', '667FB7C5-D215-4FE7-A253-99333488D12B-3.jpeg', NULL, NULL, '2019-07-13 01:53:00'),
(111, 'Andrej', NULL, 'Krestan', 'krexpro@gmail.com', '$2y$10$dOWq3W.5qV4hJ/bfcsn38u0NIebmCBYo5drjY05R4P5DU2Zr4I0Fi', '12508868629', '140-6171 willingdon ave', 'burnaby', 'BC', 'v5h2t9', 'Canada', '1989-12-15', 'male', 'Adult', 'andrej_wsb', 'KRESTAN, ANDREJ', 'Andrej Krestan', 'Andrej Krestan', 'https://m.imdb.com/name/nm8091833/?ref=m_nv_sr_1', '', '', NULL, 'Yes', 'Yes', '667FB7C5-D215-4FE7-A253-99333488D12B-4.jpeg', NULL, NULL, '2019-07-13 01:53:35'),
(112, 'Andrew', 'Steven', 'Mykichuk', 'andrewmykichuk@gmail.com', '$2y$10$Kupu.Q6OIuA8FrLbgULf9OkcVPfLQHuiU5OS79ydZ/J.wmRnwAZt6', '7789967025', '1530 Mariners Walk', 'Vancouver', 'BC', 'V6J4X9', 'Canada', '1983-07-25', 'male', 'Adult', NULL, 'MYKICHUK, ANDREW STEVEN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '2921820F-E6CA-4062-BA67-D591EF1F2050.jpeg', NULL, NULL, '2019-07-13 01:54:07'),
(113, 'Andrew', 'Patrick', 'Bourque', 'andrewbourque92@gmail.com', '$2y$10$k12bRoFPewHM/Hflyvp34.VEEpLwOnOpKTGxZ96a3CrLQrXYHa.hG', '15147734022', '3875 west 4th avenue', 'Vancouver', 'BC', 'V6R 4H8', 'Canada', '1992-11-24', 'male', 'Adult', NULL, 'BOURQUE, ANDREW PATRICK', 'https://www.linkedin.com/in/andrew-patrick-bourque/', 'https://www.facebook.com/andrew.bourque.39', NULL, '', '', NULL, 'Yes', 'No', 'photo-5.jpg', NULL, NULL, '2019-07-13 01:54:49'),
(114, 'Andrew', 'David', 'Derbitsky', 'aderbitsky@gmail.com', '$2y$10$SoRX9WVc180S0MyZHhXnzeBJpbsoTZZk1zGQJ0SFnGi4g/wC7P7w6', '6047890814', '#30 460 W16th Ave', 'Vancouver', 'BC', 'V5Y 1Z3', 'Canada', '1987-12-28', 'male', 'Adult', NULL, 'DERBITSKY, ANDREW DAVID', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'face-shot.png', NULL, NULL, '2019-07-13 01:55:43'),
(115, 'Andrew', NULL, 'Pilcher', 'inversight@gmail.com', '$2y$10$bj15zN1IqNoJ3vJt9kxbsOy.N7R7BebziZk/CvUpjeWPd35AG7Cl6', '236 986 8565', '1110 Cloverley Street ', 'North Vancouver', 'BC', 'V7L 1N6', 'Canada', '1982-06-26', 'male', 'Adult', NULL, 'PILCHER, ANDREW', 'https://www.linkedin.com/in/andrew-pilcher-58a199116/', 'https://www.facebook.com/andrew.pilcher.180', 'https://www.imdb.com/name/nm7765942/?ref_=nv_sr_1', '', '', NULL, 'No', 'No', 'IMG_1279.jpg', NULL, NULL, '2019-07-13 01:56:24'),
(116, 'Andrew', NULL, 'Wang', 'andrew-wang@outlook.com', '$2y$10$O/2SJ3hliZerTXTeA6BXf.yTPFjybcAu4Gfons4nQAscVL7.vQKDe', '6047005216', '150-8473 163 St', 'Surrey', 'BC', 'V4N 6M7', 'Canada', '1997-12-19', 'male', 'Adult', 'aw_fmuf', 'WANG, ANDREW', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_2734.jpg', NULL, NULL, '2019-07-13 01:57:11'),
(117, 'Andrew', NULL, 'Wang', 'andrew-wang@outlook.com', '$2y$10$o6OopeN8m8vF4WUD0WIRZeXo584HvKuJ/O9TQhieKXY0oqH36dmAa', '6047005216', '150-8473 163 St', 'Surrey', 'BC', 'V4N 6M7', 'Canada', '1997-12-19', 'male', 'Adult', 'aw_fmuf', 'WANG, ANDREW', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_2734-1.jpg', NULL, NULL, '2019-07-13 01:57:31'),
(118, 'Andrew', 'William', 'Jorgensen', 'r031kona@gmail.com', '$2y$10$lAil9PYsUcpV4./ZtCbk4eKrTIV8WxvCeCoxJkd83N87jrDl80S2e', '2505710138', '133 Knox st', 'Kamloops', 'BC', 'V2B 3Z1', 'Canada', '1961-12-28', 'male', 'Adult', NULL, 'JORGENSEN, ANDREW WILLIAM', NULL, 'Andy Jorgensen', NULL, '', '', NULL, 'No', 'No', 'Andrew-Jorgensen4.jpg', NULL, NULL, '2019-07-13 01:57:57'),
(119, 'Andrew ', 'Michael ', 'Beha', 'andrewbeha@msn.com', '$2y$10$5YpPLdYQJqxXHD5Yxa0GmOmVLPnjLsbsihxTaxGZvavCc5XItiVQC', '7024454649', '1495 Richards st #1508', 'Vancouver ', 'BC', 'V6z3e3', 'Canada', '1978-05-10', 'male', 'Adult', 'www.instagram.com/andrewbeha', 'BEHA, ANDREW  MICHAEL', NULL, 'www.facebook.com/andrewbeha', 'www.imbd.me/andrewbeha', '', '', NULL, 'Yes', 'Yes', '51C9A2F6-9CF6-47AB-BA08-BAEF03239F4B.jpeg', NULL, NULL, '2019-07-13 01:58:17'),
(120, 'Andrew ', 'Michael', 'Beha', 'andrewbeha@msn.com', '$2y$10$LbTMnR4nTQ91i3KK.M2hsucPcfnnZySx7zjQ3W5FPJWknYsVtzTWe', '7024454649', '1495 richards st #1508', 'Vancouver', 'BC', 'V6z3e3', 'Canada', '1978-05-10', 'male', 'Adult', 'www.instagram.com/andrewbeha', 'BEHA, ANDREW  MICHAEL', 'www.linkedin.com/andrewbeha', 'www.facebook.com/andrewbeha', 'www.imdb.me/andrewbeha', '', '', NULL, 'No', 'Yes', 'EE0FC9CC-5300-4640-B362-76C7352F050B.jpeg', NULL, NULL, '2019-07-13 01:58:29'),
(121, 'Andy', NULL, 'Vu', 'andyvu92@gmail.com', '$2y$10$uPu0Qz2jjSNOcwcpxJtYrOun/8QmqVysHENmvOjgB8r2vnzFSL4aC', '6046006850', '4733 Bruce street', 'Vancouver', 'BC', 'V5N3Z7', 'Canada', '1999-07-17', 'male', 'Adult', 'https://www.facebook.com/andyvuu17', 'VU, ANDY', NULL, 'https://www.instagram.com/andyvu17/?hl=en', NULL, '', '', NULL, 'No', 'No', 'b90ec4767e6b9f7e3484d49247153922.png', NULL, NULL, '2019-07-13 01:58:38'),
(122, 'andy', NULL, 'choe', 'andychoe94@gmail.com', '$2y$10$ggdrbRZXsJMyr/SQRjeXFuc1BnNEhD0GIAK.FbtuGJ1H16AsPG.Di', '6043458256', '#910 3438 vanness avenue', 'vancouver', 'BC', 'v5r6e7 ', 'Canada', '1994-09-12', 'male', 'Adult', 'Alloftheabove_____', 'CHOE, ANDY', NULL, 'andy chan ok choe', NULL, '', '', NULL, 'No', 'No', '41674090-E1E2-4C2D-AA5A-053643CB99FB.jpeg', NULL, NULL, '2019-07-13 01:59:14'),
(123, 'Angela', 'Maria', 'Negrin Ibbott', 'ibbottangela@gmail.com', '$2y$10$ZYt2AzzK2y/QxYTrACr8f.VL.AN5J2U0x9RnB3cS74rxytxM5obey', '7789945552', '3282 Allan Rd', 'North Vancouver', 'BC', 'V7J 3C5', 'Canada', '1999-12-02', 'female', 'Adult', 'johnwaynegacyofficial', 'NEGRIN IBBOTT, ANGELA MARIA', 'Annie Negrin', NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_0285.jpg', NULL, NULL, '2019-07-13 01:59:31'),
(124, 'Angela', NULL, 'Knight', 'angela22knight@gmail.com', '$2y$10$bRTsV1nF8FW8XqYK0pxuze3s6VKR81lwapywRQ0HcTk0HaTiSPkbO', '778 384 8466', '224-5600 Andrews Road', 'Richmond', 'BC', 'V7E 6N1', 'Canada', '1967-06-19', 'female', 'Adult', NULL, 'KNIGHT, ANGELA', 'https://ca.linkedin.com/in/angela-r-knight-00ba2043', NULL, 'https://www.imdb.com/name/nm1862808/', '', '', NULL, 'Yes', 'No', 'angela-headshot-copy.jpg', NULL, NULL, '2019-07-13 01:59:58'),
(125, 'Angela', NULL, 'Dolomont', 'ADolomont@shaw.ca', '$2y$10$SA1L6l5sXzddZ/6Wd/OHM.4zLX73FMOe28dOmavzCGE4P7XKR3.xO', '604-785-2878', 'B461 - 603 Hot Springs Road', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1973-10-26', 'male', 'Adult', NULL, 'DOLOMONT, ANGELA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_1844.jpg', NULL, NULL, '2019-07-13 02:00:44'),
(126, 'Angela', NULL, 'Dolomont', 'ADolomont@shaw.ca', '$2y$10$zu5pEIRglX.M0uMZtOiq..YxyLxM7xdo9qHzT3ZqZD7LNTVSxVQEe', '604-785-2878', 'B461 - 603 Hot Springs Road', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1973-10-26', 'male', 'Adult', NULL, 'DOLOMONT, ANGELA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_1844-1.jpg', NULL, NULL, '2019-07-13 02:01:51'),
(127, 'Angela', NULL, 'Dolomont', 'ADolomont@shaw.ca', '$2y$10$NHqJ2pAZtaP3.9FI6OmyMuHh0gxgJFyBbA4XZZ//8goQtaGI47Ed2', '604-785-2878', 'B461 - 603 Hot Springs Road', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1973-10-26', 'male', 'Adult', NULL, 'DOLOMONT, ANGELA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_1844-2.jpg', NULL, NULL, '2019-07-13 02:03:09');
INSERT INTO `performers_bk` (`id`, `firstName`, `middleName`, `lastName`, `email`, `password`, `phonenumber`, `address`, `city`, `province`, `postalCode`, `country`, `date_of_birth`, `gender`, `applicationfor`, `instagram`, `twitter`, `linkedin`, `facebook`, `imdb_url`, `agent_name`, `union_name`, `union_number`, `agent`, `union_det`, `upload_primary_url`, `remember_token`, `created_at`, `updated_at`) VALUES
(128, 'Angela', NULL, 'Dolomont', 'ADolomont@shaw.ca', '$2y$10$09exTSJoMTNs/Wo4LAjk7.TUfzR4jjt8bnCV/OFM.vWxtaQPnNrA2', '604-785-2878', 'B461 - 603 Hot Springs Road', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1973-10-26', 'male', 'Adult', NULL, 'DOLOMONT, ANGELA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_1844-3.jpg', NULL, NULL, '2019-07-13 02:04:17'),
(129, 'Angelina', NULL, 'Chizh', 'angelchizh@hotmail.com', '$2y$10$yCussmzhzWTptOh.kEFVku7QP8xBN3RmYQ7Osu6AQeBzuu8OXkvEa', '4388700213', '1372 Seymour st', 'Vancouver', 'BC', 'V6B0L1', 'Canada', '1998-07-14', 'female', 'Adult', NULL, 'CHIZH, ANGELINA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Screen-Shot-2019-06-20-at-10.52.39-AM.png', NULL, NULL, '2019-07-13 02:05:34'),
(130, 'Angie', NULL, 'Wojciechowska ', 'angie@off-leash.ca', '$2y$10$b66oo8/QSwI07ktfqKIwhupDraUmayHFHLnXXojhlZinIZgjTB28q', '60161822236', '1402-850 Royal Ave', 'New Westminster ', 'BC', 'V3M 1A6', 'Canada', '1970-11-10', 'female', 'Adult', 'Angieoffleash', 'WOJCIECHOWSKA , ANGIE', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', '40A54AB0-F078-4F32-9058-79804CCBF23B.jpeg', NULL, NULL, '2019-07-13 02:05:50'),
(131, 'Angie', 'Linda', 'Wangler', 'angiewangler@gmail.com', '$2y$10$u0.JL5iemzt4YoJaWUqAxO2LsqoEF4kk45mlBW6ZmAH0n0cKmhNFy', '604.704.7622', '814 Jackson Ave', 'Vancouver', 'BC', 'V6A 3C1', 'Canada', '1982-03-04', 'female', 'Adult', 'https://www.instagram.com/angiewangler/', 'WANGLER, ANGIE LINDA', NULL, 'https://www.facebook.com/angie.wangler.5', NULL, '', '', NULL, 'No', 'No', 'Angie-headshot-1.jpg', NULL, NULL, '2019-07-13 02:05:57'),
(132, 'Angie ', 'Linda', 'Wangler', 'angiewangler@gmail.com', '$2y$10$CltIEPswSi9LqOv5YiN0g.70/GxVYkZaSUUDOPAmH9o97m4aOgYP.', '604.7044.7622', '814 Jackson Ave', 'Vancouver', 'BC', 'V6A 3C1', 'Canada', '1982-03-04', 'female', 'Adult', 'https://www.instagram.com/angiewangler/', 'WANGLER, ANGIE  LINDA', NULL, 'https://www.facebook.com/angie.wangler.5', 'https://www.imdb.com/name/nm4088201/', '', '', NULL, 'No', 'No', 'Angie-headshot-1.jpg', NULL, NULL, '2019-07-13 02:06:42'),
(133, 'Anika', NULL, 'Bird', 'asbird04@gmail.com', '$2y$10$4zgW9VPdZG/jmqPC5UeBFe3/vPF8WRHH6FN2h1uO8KCfLotSMi9v.', '6047997374', 'Box 73', 'Agassiz ', 'BC', 'V0M1A0', 'Canada', '2004-01-11', 'transgender', 'Teen', NULL, 'BIRD, ANIKA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190317_224259.jpg', NULL, NULL, '2019-07-13 02:07:42'),
(134, 'Ann', NULL, 'Stokes', 'keepityellow@hotmail.com', '$2y$10$NBIVUQfGW8woC5tcWmWB2.4FfOYUePxLamyYHIeM3W30RCx/zOLoC', '6044424256', '1605-1331 Alberni street', 'Vancouver', 'BC', 'V6E4S1', 'Canada', '1983-10-07', 'female', 'Adult', NULL, 'STOKES, ANN', NULL, 'https://m.facebook.com/ann.chan.37201?refid=8', NULL, '', '', NULL, 'Yes', 'Yes', 'Screenshot_20180502-063406-2.png', NULL, NULL, '2019-07-13 02:08:00'),
(135, 'Ann', NULL, 'Johnstone ', 'annjohnstone1@hotmail.com', '$2y$10$9PJcsWmBhtLESTyvnB55mueHoCyUvyYA.IreibvWX0568dr26sPMi', '6043162893', '6983 Kalyna Dr', 'Agassiz ', 'BC', 'V0M1A3', 'Canada', '1958-06-23', 'male', 'Adult', NULL, 'JOHNSTONE , ANN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '3249B9ED-A7C6-4FFD-A7B7-597879A4FF0D.jpeg', NULL, NULL, '2019-07-13 02:08:28'),
(136, 'Ann Margaret', NULL, 'Mortel', 'annmortel@hotmail.com', '$2y$10$ELNpcrNAC57/ySYR/YWUgur7NeEH6TTKzqLYWI5E9/FCgcdnzTwKG', '7788472316', '7453 Culloden St', 'Vancouver', 'BC', 'V5x4k3', 'Canada', '1987-01-16', 'female', 'Adult', 'ann_margaret16', 'MORTEL, ANN MARGARET', NULL, 'https://www.facebook.com/profile.php?id=734154866', NULL, '', '', NULL, 'No', 'No', '3512D266-1E1D-41F1-8E5B-F33B84CCBFBE.jpeg', NULL, NULL, '2019-07-13 02:08:51'),
(137, 'Anna', NULL, 'Wheeler', 'jesse.annamarie@gmail.com', '$2y$10$CSHHj9YWbtP9Y9d9OGQsqe8lb9g95tQpiPG5vEFF8bThCQq3pGcAC', '6047024644', '1636 Vimy Rd ', 'Agassiz ', 'BC', 'V0m1a2', 'Canada', '1987-05-19', 'female', 'Adult', '@wheelerhaus ', 'WHEELER, ANNA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'NFK_Profile-10.jpg', NULL, NULL, '2019-07-13 02:09:26'),
(138, 'Anna', NULL, 'Lee', 'climbingmom1@gmail.com', '$2y$10$arf50t7/PpCqks3.x5cMjeln4yWB6UUthKfWHqKui8yvXkReKy.mK', '604 754 7987', '252 E 17th Street', 'North Vancouver', 'BC', 'V7L 2V7', 'Canada', '1960-04-27', 'female', 'Adult', 'climbingmom1', 'LEE, ANNA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Anna-Headshot.jpg', NULL, NULL, '2019-07-13 02:09:35'),
(139, 'Anna', 'Helena', 'Terebka', 'aterebka@hotmail.com', '$2y$10$NV6C8JJRpKQfGgxlLMcFj.bl4Wwh7pZ7q0hknViTNQoS7p4LI81/K', '2504885529', '2011 Green Mountain Rd.', 'PENTICTON', 'BC', 'V2A0E6', 'Canada', '1981-12-14', 'female', 'Adult', '@atreebook', 'TEREBKA, ANNA HELENA', NULL, 'Anna Terebka', 'https://l.facebook.com/l.php?u=https%3A%2F%2Fpro.imdb.com%2Fname%2Fnm9744729%3Fs%3D22b5d80b-0575-83d7-02ea-14b6c1bd2b25%26fbclid%3DIwAR18qxgLsXObH2dWK7PpKpVsWshONabCsSNrTBIspOpcX7uiKduJh5Yyn3c&h=AT3UJb7sUO-LiLjuOcD0FVcEo0e0UGL4aUCdVElyVuKi76JwJ3PO4DFObnu4Qfwvq6DiaIgGtlTUpgikI-fmKyOhMrlTtOsdqCJfmUTO4Ypp0o3qZsdSwAKFVkBkJ3sFLlQ', '', '', NULL, 'Yes', 'No', '65568799_329363321317789_1618900240469852160_n.jpg', NULL, NULL, '2019-07-13 02:10:20'),
(140, 'Annabella', 'Sophia Doreen Katarina', 'Escoval', 'gescoval1@gmail.com', '$2y$10$ad2GwRppWPDuI9RH4Am.7uMt6LjWXf3ekmksBPGA0/FneodFZ0aku', '2507271364', '2835 Ronald Road', 'Victoria', 'BC', 'V9B 2L7', 'Canada', '2005-08-05', 'female', 'Teen', NULL, 'ESCOVAL, ANNABELLA SOPHIA DOREEN KATARINA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190415_135641.jpg', NULL, NULL, '2019-07-13 02:10:34'),
(141, 'Anne', NULL, 'Kadwell', 'akadwell23@me.com', '$2y$10$iEMgZ1zIo8V4i3QANeArOus/q7zXsVaYs.PeQofAj32rs3CzC2Fkm', '604 306 8920', 'PO Box 3156', 'Garibaldi Highlands (within Squamish)', 'BC', 'V0N1T0', 'Canada', '1968-08-19', 'female', 'Adult', NULL, 'KADWELL, ANNE', 'www.linkedin.com/in/ak2317', NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_2339.jpg', NULL, NULL, '2019-07-13 02:53:47'),
(142, 'Anne', NULL, 'Lin', 'anneikeda@gmail.com', '$2y$10$wCC9xH/TxG26FfrJKw.R7.N3MPDmy8/WLO6BK1JIpRpaA0prdFlNy', '7789287325', '15677 102 Avenue', 'Surrey', 'BC', 'V4N2G5', 'Canada', '1981-06-24', 'female', 'Adult', NULL, 'LIN, ANNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'anne-face.jpg', NULL, NULL, '2019-07-13 02:54:21'),
(143, 'Annelyn', 'Ruby', 'Victor', 'avictor.cheam@gmail.com', '$2y$10$Z2TluMqDc0VWpUJblp9Yiu9uHQ9JWn3bp73dAfsHV8k9jysa3DFhO', '604-316-8884', '5, 1700 Mackay Cres', 'Agassiz', 'BC', 'V0M 1A3', 'Canada', '2004-01-23', 'female', 'Teen', NULL, 'VICTOR, ANNELYN RUBY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20170727_180653-1.jpg', NULL, NULL, '2019-07-13 02:54:51'),
(144, 'Anne-Marie', NULL, 'Dupperon', 'dupperonam@gmail.com', '$2y$10$3APorf/4X9HVOgqU/UMkJe14dJxJfaf4/FrD3GxM1xx05ag09r/6i', '6043163900', '6950 Oakwood drive', 'Agassiz', 'BC', 'V0M 1A3', 'Canada', '1954-10-07', 'female', 'Adult', NULL, 'DUPPERON, ANNE-MARIE', NULL, 'Petunia Dupperon/AnnieDupperon', NULL, '', '', NULL, 'No', 'No', 'IMG_0352.jpg', NULL, NULL, '2019-07-13 02:55:12'),
(145, 'Annette', 'Audrey ', 'Gehring ', 'Mrsbuy@hotmail.com', '$2y$10$4SXN3wHg8URRH/97E9o7zOshyiKuac0yRUlC6R5jLD/49f5SvR/gW', '6044401174', '8698 206B St', 'Langley', 'BC', 'V1M3X5', 'Canada', '2006-06-02', 'male', 'Child', NULL, 'GEHRING , ANNETTE AUDREY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20180623_153108-1.jpg', NULL, NULL, '2019-07-13 02:55:31'),
(146, 'Anni', 'Monica ', 'Torikka', 'anniandthegirls@hotmail.com', '$2y$10$3IptD1CJcUQ7dKnGA0jTU.WhhoQdi658tm0WeXx3lWlzk.k/Wac7C', '2503055122', '456 Mulberry Ave ', 'Kamloops ', 'BC', 'V2b2r5 ', 'Canada', '1958-01-24', 'female', 'Adult', NULL, 'TORIKKA, ANNI MONICA', NULL, 'Anni Torikka ', NULL, '', '', NULL, 'No', 'No', 'CB7F1A14-FA60-4B68-A660-CC1581F42FED.jpeg', NULL, NULL, '2019-07-13 02:55:50'),
(147, 'Ann-Marie', NULL, 'Leonard', 'amleonard43@gmail.com', '$2y$10$YNRzIYszgHQApdA7umGbX.ZBvexFSFLH1j.yCzxrApdPNSSnOUdra', '514-601-4889', '3528 4th av.', 'vancouver', 'BC', 'V6R 1N8', 'Canada', '1982-10-19', 'female', 'Adult', NULL, 'LEONARD, ANN-MARIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'ann-marie-8.jpg', NULL, NULL, '2019-07-13 02:56:17'),
(148, 'Anthea', 'Eowyn', 'Mackey', 'loribonertz@gmail.com', '$2y$10$VMC1XhULg8KJ3IiiyM9uRuiN6bilTRr7xIkqy7hZiLe2oXTa.ucc6', '778-867-2095', '1805 Primrose Crescent', 'Kamloops', 'BC', 'V1S 0A6', 'Canada', '2003-01-20', 'male', 'Teen', 'theamackey', 'MACKEY, ANTHEA EOWYN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_0372.jpeg', NULL, NULL, '2019-07-13 02:56:52'),
(149, 'Anthony', NULL, 'Anglehart', 'cassandracurdie@hotmail.com', '$2y$10$qA8hXt7TmYnqd1DZjq7aKOpGSbnye9eM86cjowC5m5D4/myuiWquC', '604-316-7465', '7373 Elm Rd', 'Agassiz', 'BC', 'V0M 1A2', 'Canada', '2007-11-11', 'male', 'Child', 'anthony_anglehart', 'ANGLEHART, ANTHONY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(150, 'Anthony', NULL, 'Demare', 'tonydemare@gmail.com', '$2y$10$K6Bhm6MI7ia7/3alu3z4N.bzdjyk6lUyUZE71qmYwG5dO2I13oPhi', '6043079815', '1171 Marsden court', 'Burnaby', 'BC', 'V5A3K2', 'Canada', '1991-02-02', 'male', 'Adult', 'ademare', 'DEMARE, ANTHONY', NULL, NULL, 'https://m.imdb.com/name/nm8800098/', '', '', NULL, 'Yes', 'Yes', 'Anthony-Demare-blazer-headshot.jpg', NULL, NULL, '2019-07-13 02:57:02'),
(151, 'Anthony ', NULL, 'Rodriguez ', 'rsamudio1490@gmail.com', '$2y$10$ULlaa14P9/CMxCE9AQpKaOY4mWealyVQWq6hFFHqwpRx1GsJVRDOK', '7785354696', '2335 W 51 st ave ', 'Vancouver ', 'BC', 'V6p1e8 ', 'Canada', '1990-08-14', 'male', 'Adult', NULL, 'RODRIGUEZ , ANTHONY', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'C7E5022E-F30F-414C-9E79-8093480330F6.jpeg', NULL, NULL, '2019-07-13 02:57:22'),
(152, 'Antony', NULL, 'Le', 'tnyle55@hotmail.com', '$2y$10$zEKWbc4oOIxDUbD7sLk4mejM6Tn.rm2YRvWhP0GukbsHih6J6pK4W', '778-839-2052', '5869 123A Street', 'Surrey', 'BC', 'V3X 1Y3', 'Canada', '1987-08-27', 'male', 'Adult', NULL, 'LE, ANTONY', 'https://www.linkedin.com/in/antonyle/', 'https://www.facebook.com/antonyle55', NULL, '', '', NULL, 'Yes', 'No', '10494602_10154464288035074_6908255480926597801_n.jpg', NULL, NULL, '2019-07-13 02:57:48'),
(153, 'AOY', NULL, 'CHIU', 'aoyc@shaw.ca', '$2y$10$T7QMj3VQWK.eywxVzp5zKePRPZxI5OGZVqVmGcT/MMr6lMRfy4ZMO', '7789866762', '8380 Francis Road', 'Richmond', 'BC', 'V6Y 1A4', 'Canada', '2016-12-10', 'female', 'Teen', NULL, 'CHIU, AOY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Aoy-Finals-web-size-1.jpg', NULL, NULL, '2019-07-13 02:58:51'),
(154, 'Apidi', NULL, 'Onyalo', 'apidionyalo@hotmail.com', '$2y$10$en65prI8NxiRM6clOc4S.uXDNdEDIHbiS1xrZTSuwRqbXfRmUnwBO', '6043769228', '103-2290 Wall St', 'Vancouver', 'BC', 'V5L 1B6', 'Canada', '1985-08-17', 'female', 'Adult', '@apidi', 'ONYALO, APIDI', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_9496.jpg', NULL, NULL, '2019-07-13 02:59:20'),
(155, 'April', 'Stephanie', 'Bryce ', 'Patiencepayton@gmail.com', '$2y$10$f5VCawuUUEdXpExW51CYnue9ao5D3g/ph/3qnQkf9VvBpvFqEzOM2', '6046445861', '217a 7155 Hall road', 'surrey', 'BC', 'V3W 4X4', 'Canada', '1985-01-22', 'female', 'Adult', NULL, 'BRYCE , APRIL STEPHANIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'FB_IMG_1551231470203.jpg', NULL, NULL, '2019-07-13 02:59:51'),
(156, 'April ', 'Evelyn', 'Steegstra', 'april.gallinger@gmail.com', '$2y$10$kBpn5TwhrBx7DCLqbu64f.PIYJh70FspxwP.p31IyeRBTiofEgpua', '604-378-2636', '9875 Gracemar Dr', 'Chilliwack', 'BC', 'V2P7P1', 'Canada', '1997-09-13', 'female', 'Adult', NULL, 'STEEGSTRA, APRIL  EVELYN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'LRM_EXPORT_20180628_200513.jpg', NULL, NULL, '2019-07-13 03:00:12'),
(157, 'April ', 'Evelyn', 'Steegstra', 'april.gallinger@gmail.com', '$2y$10$WZpTQity9CJcWRzL1QOwmuH0/KUKm5rbAgJLVBOYGDz6.xkamFLkm', '604-378-2636', '9875 Gracemar Dr', 'Chilliwack', 'BC', 'V2P7P1', 'Canada', '1997-09-13', 'female', 'Adult', NULL, 'STEEGSTRA, APRIL  EVELYN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'LRM_EXPORT_20180628_200513-1.jpg', NULL, NULL, '2019-07-13 03:01:08'),
(158, 'Aramesh', NULL, 'Atash', 'arameshatash@gmail.com', '$2y$10$dCatknlHEyIEX3e3r0oiGOY5zSaI0ZjbMzHbW2.Lay3DPi3R4JrsG', '6047828064', '3C, 328 Taylor Way', 'West Vancouver', 'BC', 'V7T 2Y4', 'Canada', '1985-04-01', 'female', 'Adult', '@meshtash', 'ATASH, ARAMESH', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'PAN04393.jpg', NULL, NULL, '2019-07-13 03:01:41'),
(159, 'Araz', NULL, 'Yaghoubi', 'ARAZ.YAGHOUBI93@GMAIL.COM', '$2y$10$n88bMQh8ixC/FIF0zdfddO/qH50v8AbAoR283JpSlGE7.MLgaLigS', '778-384-8078', '109-200 Keary St', 'New Westminster', 'BC', 'V3L 0A6', 'Canada', '1993-09-12', 'male', 'Adult', NULL, 'YAGHOUBI, ARAZ', NULL, NULL, 'http://www.imdb.com/name/nm7113303/', '', '', NULL, 'Yes', 'Yes', 'unnamed-4.jpg', NULL, NULL, '2019-07-13 03:01:59'),
(160, 'Aria ', 'Sophia', 'Sium', 'guebezail@gmail.com', '$2y$10$GB4.pO6iXLT5RBEx.iR9fea3rvR3c/dVfVTAUehEE.Cv88dA41rAy', '6045189420', '13909 59a ave', 'Surrey', 'BC', 'V3xX0G6', 'Canada', '2015-10-17', 'female', 'Child', '@luluhotstuff', 'SIUM, ARIA  SOPHIA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(161, 'Aricin', 'Reid', 'seto', 'lgseto@telus.net', '$2y$10$6M4T8V1dFfNk445uJtBYR.zEMSunkYkRbmZ1XidQOmKj73qq/RFgy', '2508885796', '4258 cedar hill road', 'Victoria', 'BC', 'v8n 3c5', 'Canada', '2008-06-20', 'male', 'Child', NULL, 'SETO, ARICIN REID', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'IMG_3215.png', NULL, NULL, '2019-07-13 03:02:34'),
(162, 'Arika', NULL, 'Barnsley', 'alikaiwata@gmail.com', '$2y$10$sPLU39KCpzLN5kBKPWh4PuRImtb4oBoiLCqQ1O4xESeCqlNfCSiNu', '6047046773', '208 1683 Adanac Street', 'Vancouver', 'BC', 'V5L 2C7', 'Canada', '1989-10-09', 'female', 'Adult', NULL, 'BARNSLEY, ARIKA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'E5C6931A-8315-4ED5-9364-3E4862055334.jpeg', NULL, NULL, '2019-07-13 03:02:53'),
(163, 'Arne', 'Jenson', 'Zabell', 'ajzabell@live.ca', '$2y$10$Rp5JPv88ZVhPp8DX3RbJxeqDF5Pz/71sCxotVgax6snhIzFVGvlwC', '6048690443', '14470 Alpine Blvd.', 'Hope', 'BC', 'v0x1l5', 'Canada', '1996-12-16', 'male', 'Adult', '@ajzabell', 'ZABELL, ARNE JENSON', NULL, 'facebook.com/SuuhDude', NULL, '', '', NULL, 'No', 'No', 'G0254258.jpg', NULL, NULL, '2019-07-13 03:03:13'),
(164, 'Arnie', NULL, 'Gauthier', 'arnie@arnie.ca', '$2y$10$pGxahelqRbPWhnsXTeiAyuXLBjsgqIzsiw.Nc5IBEMGk89.OgRfRS', '604-916-4349', '3534 Cambridge St.', 'Vancouver', 'BC', 'V5K 1M4', 'Canada', '1957-09-06', 'male', 'Adult', NULL, 'GAUTHIER, ARNIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_20190201_201702.jpg', NULL, NULL, '2019-07-13 03:03:56'),
(165, 'Arthur', 'Thomas', 'Buffie', 'artbuf@telus.net', '$2y$10$tmsowrrdCPLGNB1qWQTbTuHQxHu/PE1WTt4Ua6PvOqsx4a6u4dGt.', '7783864775', '16125 79 Ave', 'Surrey', 'BC', 'V4N0K3', 'Canada', '1956-06-28', 'male', 'Adult', NULL, 'BUFFIE, ARTHUR THOMAS', NULL, 'https://www.facebook.com/art.buffie', NULL, '', '', NULL, 'Yes', 'Yes', 'Art.jpg', NULL, NULL, '2019-07-13 03:04:30'),
(166, 'Arthur', NULL, 'Walker', 'ambulat@icloud.com', '$2y$10$/XWfrFSazyejRuqRW.GveO8Y4A28w5IaCwWu8cjA5RmYxo4a.cbxu', '7789913876', '4791 Argyle St.', 'Vancouver', 'BC', 'V5N 3X9', 'Canada', '2007-07-27', 'female', 'Child', NULL, 'WALKER, ARTHUR', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '3E951120-C7CA-465A-A8EC-C5802B0BEFAB.jpeg', NULL, NULL, '2019-07-13 03:04:57'),
(167, 'Artur', 'Edmundovich', 'Peter', 'artemisauclair@gmail.com', '$2y$10$WjGAupJ6nm97B/wcJp52HO9TFzwN8PKHUiSQNLL8f9HgurY6SdsKW', '7788296370', '3260 parker st ', 'Vancouver', 'BC', 'V5k 2v8', 'Canada', '1993-08-06', 'male', 'Adult', 'Cyberbohemian', 'PETER, ARTUR EDMUNDOVICH', NULL, 'Artemis Auclair', NULL, '', '', NULL, 'Yes', 'Yes', 'IMG_9540.jpg', NULL, NULL, '2019-07-13 03:05:24'),
(168, 'Artur', 'Edmundovich', 'Peter', 'artemisauclair@gmail.com', '$2y$10$8vZj7x5rslTLy42NUzuK6uUYoBW9a9jvRODEYGreoUFSFZzNDouaS', '778-829-6370', 'BSMT - 3260 Parker St', 'Vancouver', 'BC', 'v5k 2v8', 'Canada', '1993-08-06', 'male', 'Adult', 'https://www.instagram.com/cyberbohemian/', 'PETER, ARTUR EDMUNDOVICH', NULL, 'https://www.facebook.com/art.auclair', 'https://www.imdb.com/name/nm8843319/', '', '', NULL, 'No', 'Yes', 'art_peter_nofacialhair.jpg', NULL, NULL, '2019-07-13 03:06:07'),
(169, 'asccreative', 'freeman', 'casting', 'asccreative@info.com', '$2y$10$DRmrlJQ7rVlE00MQBWELs.tAOpuL29twfXgSTIPHAPWAg3dG.UEme', '7878788', 'asccreative', 'Vancouver', 'BC', 'A1A1A1', 'Canada', '1999-12-10', 'male', 'Adult', 'instagram', 'CASTING, ASCCREATIVE FREEMAN', 'linkedin', 'facebook', 'imdb', '', '', NULL, 'No', 'No', 'Hydrangeas.jpg', NULL, NULL, '2019-07-13 03:07:19'),
(170, 'asdasdsa', NULL, 'asdsadsa', 'sdfsad@dfsd.com', '$2y$10$9YwPupAOMIQa7uoKHASwwetcidonp2VFcZzysY6TLDKSjN9mt5dbG', 'asdsaa', 'sadsa', 'asdsadas', 'BC', 'sadsa', 'Canada', '2019-05-07', 'male', 'Adult', NULL, 'ASDSADSA, ASDASDSA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(171, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$CmCgiPSq4E8kfPA00qkV0OaBeyesU9zHjZGfMnjSr1qVbNEovnv0y', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064.jpg', NULL, NULL, '2019-07-13 03:07:51'),
(172, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$hfZUNGeGFWKbv18im1u9guKLeHNQxMqxA6zNtOzW/jHMZxmZk0kz2', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064-1.jpg', NULL, NULL, '2019-07-13 03:09:50'),
(173, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$3j9X1NTcQIpKz7xTtPvqju67AfXi.EtnRI1oKvLK9.vAcfVz3e0EW', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064-2.jpg', NULL, NULL, '2019-07-13 03:11:23'),
(174, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$78DgMti8tPHjUd07iVR4mOYii/2NMtsm3myZel4HD6BxV5WBSKkwS', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064-3.jpg', NULL, NULL, '2019-07-13 03:14:02'),
(175, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$dR80.72ITqPY5QpZuxSfLOIoZA3AIxWcBsCru.DyBWEiynZxJ2LCO', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064-4.jpg', NULL, NULL, '2019-07-13 03:15:45'),
(176, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$crco12fnb.XX1.Gu6GcxGOz8.O2KLnzE1fWAncCBrm8J9SOooaUja', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064-5.jpg', NULL, NULL, '2019-07-13 03:16:41'),
(177, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$A2Z0rkPglFixP.xT5rL3N.0RNdcTTIX4ZU3GWIfTox2qc5Qh3CyHG', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064-6.jpg', NULL, NULL, '2019-07-13 03:17:39'),
(178, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$h.LNBqMAUN2YAoa4N6G5h.LjrgtR8sXotsdFsDE2yhzj04fF6XuTu', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Asher-Headshot-1.png', NULL, NULL, '2019-07-13 03:18:38'),
(179, 'Ashlee', NULL, 'Nunn', 'ashleenunn91@gmail.com', '$2y$10$UkWFJNIqx4eUirvUyyi7T.xGM48YjEjVxq798L9ZVem/CYybfIHGq', '7782401181', '83 17097 64 ave', 'Surrey', 'BC', 'V3S 1Y5', 'Canada', '1991-08-01', 'female', 'Adult', NULL, 'NUNN, ASHLEE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_0069.png', NULL, NULL, '2019-07-13 03:19:03'),
(180, 'Ashley', 'Maddison ', 'Richter', 'thornswolfbane@gmail.com', '$2y$10$PasA7fKxZLTSQb9oLRLbXOIUX31uoUpJnBfyj/Li1PVAbV.ECVu/O', '+1 07785330774', '#2 242 Ontario street', 'Victoria', 'BC', 'V8V1N2', 'Canada', '1999-07-22', 'female', 'Adult', 'ash_nan22 ', 'RICHTER, ASHLEY MADDISON', NULL, 'Ashley Richter', NULL, '', '', NULL, 'No', 'No', 'IMG_20180720_180534.jpg', NULL, NULL, '2019-07-13 03:20:18'),
(181, 'Ashley', NULL, 'Clements', 'duckleburry@gmail.com', '$2y$10$XbRoOmPQ92FFiiaWTLs/VeBb99INUjKdahJsfob0P294QgQXZg7n.', '2365221987', '46025 Cleveland ave', 'Chilliwack', 'BC', 'V2p2w9 ', 'Canada', '1992-08-12', 'female', 'Adult', NULL, 'CLEMENTS, ASHLEY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_0395.jpg', NULL, NULL, '2019-07-13 03:21:26'),
(182, 'Ashley', 'Marie', 'Fitger', 'ashleemarierose@outlook.com', '$2y$10$blLR/IXLJEtBMt.4tdtmTu0KK4rWlbHYyBsnoKP9Xn5i1AebiYaqe', '2503181567', 'PO Box 1124', 'Barriere', 'BC', 'V0E 1E0', 'Canada', '1997-04-27', 'female', 'Adult', 'ashley_marie_fitger', 'FITGER, ASHLEY MARIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'FB_IMG_1505757754718.jpg', NULL, NULL, '2019-07-13 03:21:37'),
(183, 'Ashley ', 'Teagan ', 'Essig ', 'equinoxacres@shaw.ca', '$2y$10$qrHT5Kxtd0lxpRvrIkHSfur2/85vM4zXDgkYCnUMp4asFaSIwtIGG', '2507131114', '2065 Sanders rd ', 'Nanoose ', 'BC', 'V9p9c2 ', 'Canada', '2006-05-14', 'female', 'Teen', NULL, 'ESSIG , ASHLEY  TEAGAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '76C4F2B9-93E4-4BEA-BD7F-855A4127BE2C.jpeg', NULL, NULL, '2019-07-13 03:21:46'),
(184, 'Ashton', 'Rose', 'Fong ', 'tabicat10@shaw.ca', '$2y$10$lGGE0UZrPZeqvoKCbbPGaOYKkK72R8w0kJ94Iqmqj6Q3HXSE3TRW.', '2506619508', '2904 Pickford rd', 'Victoria', 'BC', 'V9b2k2 ', 'Canada', '2006-08-08', 'female', 'Child', 'Ashton11fong', 'FONG , ASHTON ROSE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'C91A0F4C-E229-4906-A147-9ECDF3784242.jpeg', NULL, NULL, '2019-07-13 03:22:16'),
(185, 'Asia ', NULL, 'Domisiewicz ', 'domisiewicz@hotmail.con', '$2y$10$k3oRCOHl3aEob1IMgbpKA.fUHsnkiRBIEJL74.WP6MlI0h/3NcQ0W', '604710623', '113 3875 w 4th', 'Vancouver ', 'BC', 'V5R 3h8', 'Canada', '1980-04-02', 'female', 'Adult', 'https://instagram.com/p/BdZfo6PhR_4/', 'DOMISIEWICZ , ASIA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '98C84537-0731-4B92-A24C-C4BC70BE66E6.jpeg', NULL, NULL, '2019-07-13 03:22:56'),
(186, 'Astrid', NULL, 'Santilli', 'santilli.astrid@gmail.com', '$2y$10$svsH7rh2qzMEo9aFOR4Fe.wYrMoSq0NCLGPBtlNHR7DCl6D48aBQ6', '-9920', '1902-1288 West Cordova Street', 'Vancouver', 'BC', 'V6C 3R3', 'Canada', '1992-05-07', 'female', 'Adult', 'astridsantilli', 'SANTILLI, ASTRID', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_6044-Edit-2.jpg', NULL, NULL, '2019-07-13 03:23:25'),
(187, 'Astrid', NULL, 'Santilli', 'santilli.astrid@gmail.com', '$2y$10$5/6Tvy0PAEcJd2xTO/vevO0Qg.1swCw3/vqOZa5ekRKqR588EjN3W', '17543028865', '1902-1288 West Cordova Street', 'Vancouver', 'BC', 'V6C 3R3', 'Canada', '1992-05-07', 'female', 'Adult', NULL, 'SANTILLI, ASTRID', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_6044-Edit-2.jpg', NULL, NULL, '2019-07-13 03:24:01'),
(188, 'Asuka', NULL, 'Nakamura', 'aska_ally@hotmail.com', '$2y$10$U47kPklMyY6O2wN2eLqh8O6q7hHlQB9kYtEuPsnPfTZ1lJD81Q8KG', '7788385717', '1-230 Salter street', 'New Westminster ', 'BC', 'V3M0G1', 'Canada', '1976-03-08', 'female', 'Adult', NULL, 'NAKAMURA, ASUKA', NULL, 'AskaAllyNakamura', NULL, '', '', NULL, 'No', 'No', '218DBD69-4895-4164-A33E-1CD5EB8E3E8C.jpeg', NULL, NULL, '2019-07-13 03:24:31'),
(189, 'Atlas', NULL, 'Parker', NULL, '$2y$10$bkgazANEu3wMG8JACYqwjuvlw9f3.zaTlmLs/Zxt0w0lLVI0MBokO', NULL, NULL, NULL, 'BC', NULL, 'Canada', '2012-09-29', 'male', 'Child', NULL, 'PARKER, ATLAS', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(190, 'Atlas', NULL, 'Parker', 'angeleparker@shaw.ca', '$2y$10$XTBwsDNL8yhHXsUHTzcu8OawV10Omlz9oqkA1CIs214eLjKKrkeMi', '6042022857', '2540 Wallace crescent ', 'Vancouver', 'BC', 'V6r3v4 ', 'Canada', '2012-09-29', 'male', 'Child', NULL, 'PARKER, ATLAS', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', '1423FCE9-ED76-4F72-A73B-C6502BE108E4.jpeg', NULL, NULL, '2019-07-13 03:24:51'),
(191, 'Atlas', NULL, 'Parker', 'angeleparker@shaw.ca', '$2y$10$L3wk5poTGVCrxAtVboEf5.SF25QGDD5BEtyYB/wBbKuWsk/8XM5eO', '6045657240', '2540 Wallace Crescent', 'Vancouver', 'BC', 'V6R3V4', 'Canada', '2012-09-29', 'male', 'Child', NULL, 'PARKER, ATLAS', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'atlas-cute.jpg', NULL, NULL, '2019-07-13 03:25:19'),
(192, 'Atsushi', NULL, 'Kumo', 'kumoestate@gmail.com', '$2y$10$/1YYWUEsHN.6OfHK4tulF.FJ/XprOQxWbWSOZmg.fNcQwqmLT0E1W', '7789810188', '4300 Hermitage Dr', 'Richmond', 'BC', 'V7E 4N4', 'Canada', '1984-05-28', 'male', 'Adult', NULL, 'KUMO, ATSUSHI', NULL, 'Spencer Kumo', NULL, '', '', NULL, 'Yes', 'Yes', 'IMG_4962.jpg', NULL, NULL, '2019-07-13 03:25:38'),
(193, 'Aubriana ', 'Marrie', 'Frick', 'Shelleyfrick@shaw.ca', '$2y$10$w0svL8kIyEacjMY0M8dMYe0SKNsP7gzVJH4OH.IMOrfsItSGWLP6y', '250-857-5698', '2797 Lake End Rd', 'Victoria', 'BC', 'V9B 5T5', 'Canada', '2006-06-05', 'female', 'Teen', NULL, 'FRICK, AUBRIANA  MARRIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'image.jpeg', NULL, NULL, '2019-07-13 03:26:06'),
(194, 'Audrey', 'Barbara', 'Cooper', 'coultishmodels@telus.net', '$2y$10$QvLXYc36iU7ylqT2x0eSrODkx3DnOzIFyXbUv/pEj44zTQ.Q.WrFu', '250.516.4883', '8-50 Dallas Road', 'Victoria', 'BC', 'V8V1A2', 'Canada', '2011-04-28', 'female', 'Child', NULL, 'COOPER, AUDREY BARBARA', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'Audrey-Cooper.jpg', NULL, NULL, '2019-07-13 03:26:29'),
(195, 'August ', NULL, 'Bramhoff', 'augustbramhoff@rocketmail.com', '$2y$10$4vp6nfuyWWqRRyrPi3OI0ebUMeOwIf9UmIDKcrtDDZxJ9L4qQDTAi', '7785543772', '613-3588 Crowley Drive ', 'Vancouver ', 'BC', 'V5R 6H3', 'Canada', '1984-08-13', 'transgender', 'Adult', NULL, 'BRAMHOFF, AUGUST', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '58802B45-77C1-49DC-89C1-B7DA5099AD22.jpeg', NULL, NULL, '2019-07-13 03:26:53'),
(196, 'Austen', NULL, 'Crick', 'austencrick@outlook.com', '$2y$10$4nOO2EChlmoFFgzF1Zx4A.GROGXNCcvX3ShJJjVNQsLkuOgaY051W', '6049161778', '1759 Kenmore Rd', 'Victoria', 'BC', 'V8N 5B3', 'Canada', '1995-10-25', 'female', 'Adult', 'https://www.instagram.com/austenadele/', 'CRICK, AUSTEN', NULL, 'https://www.facebook.com/austen.crick', NULL, '', '', NULL, 'No', 'No', '52810359_552293598616168_7026577826379726848_n.jpg', NULL, NULL, '2019-07-13 03:27:17'),
(197, 'Austin', 'James', 'Friesen', 'anelephantstear@hotmail.com', '$2y$10$/FCm/pvypfUavlSpGQFvJeT7bqKXDLX53yQ0twa5c1oxD1PDxnVZi', '2508842493', '860 Pratt Road', 'Mill Bay', 'BC', 'V0R2P1', 'Canada', '2007-05-01', 'male', 'Child', NULL, 'FRIESEN, AUSTIN JAMES', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'DD7CBCEC-762A-4A44-9847-09E9AD4F4C24.jpeg', NULL, NULL, '2019-07-13 03:27:41'),
(198, 'Austin', 'J', 'Friesen', 'anelephantstear@hotmail.com', '$2y$10$l6Gf74M3eVSel/KeKtusm.kalkpnI50sPmjz/QhO1IrDr68whQuM2', '2508842493', '860 Pratt Road', 'Mill Bay', 'BC', 'V0R2P1', 'Canada', '2007-05-01', 'male', 'Child', NULL, 'FRIESEN, AUSTIN J', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'C353CA81-AB18-48BD-BC27-6B24A94B6DEB.jpeg', NULL, NULL, '2019-07-13 03:28:46'),
(199, 'Austin Rose', NULL, 'Kivinen', 'emilykivinen@gmail.com', '$2y$10$XI1l4o/MbtU24LmUCHEIBO3eRe22cPcHEfVfNqIJY1ajxJGfO9.C.', '7782550141', '35321 Corbett Place', 'Abbotsford', 'BC', 'V3G1K1 ', 'Canada', '2013-08-21', 'female', 'Child', NULL, 'KIVINEN, AUSTIN ROSE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '88CF22C2-4B9C-48D7-BDF2-F3B76D613100.jpeg', NULL, NULL, '2019-07-13 03:29:17'),
(200, 'Autumn', NULL, 'Bak', 'nqbak@telus.net', '$2y$10$i1/OvWdh3ZovtiUD6VWE3.GN2Fn57R16Le9hYmkwlRrposARfYgKO', '6042404729', '5215 Chamberlayne Ave', 'Delta', 'BC', 'V4K2J7', 'Canada', '2004-09-03', 'female', 'Teen', NULL, 'BAK, AUTUMN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20180728_140404.jpg', NULL, NULL, '2019-07-13 03:30:35'),
(201, 'Ava', 'Mackenzie', 'Jalava', 'Amieg@hotmail.com', '$2y$10$ULIJ34xK8eguy2tbQ3JJTu9ANfPWKorVuVmzwcS6h7E9OgjtoFlJS', '6048452678', 'Po Box 121', 'Rosedale', 'BC', 'V0X 1X0', 'Canada', '2014-10-15', 'female', 'Child', NULL, 'JALAVA, AVA MACKENZIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'FB_IMG_1552834931528.jpg', NULL, NULL, '2019-07-13 03:30:44'),
(202, 'Ava', 'Lynn', 'Lancaster', 'paullancaster@shaw.ca', '$2y$10$Aq8LfnIZkVYXaVYWmKRtN.ZBUI3DLgk2xjyjaRYMuqRxdctrGMsHm', '250-858-8572', '#2-4619 Elk Lake Dr', 'Victoria', 'BC', 'V8Z-5M2', 'Canada', '2007-12-27', 'female', 'Child', NULL, 'LANCASTER, AVA LYNN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_4549.jpg', NULL, NULL, '2019-07-13 03:31:19'),
(203, 'Ava', NULL, 'McGaghey', 'jmteam@shaw.ca', '$2y$10$zAsj6/T2f/HqR2J4EB49zuAeN2ulBqtbi0y70i15VGkAOLVPPxdHa', '2508933153', '2258 Setchfield Ave', 'Victoria', 'BC', 'V9B 6N8', 'Canada', '2005-04-27', 'female', 'Teen', NULL, 'MCGAGHEY, AVA', NULL, 'Ava McGaghey', NULL, '', '', NULL, 'No', 'No', '40410D09-123C-461E-875E-34FCD61182AA.jpeg', NULL, NULL, '2019-07-13 03:31:46'),
(204, 'Ava', 'Natalie', 'Morris', 'kmorris77@live.ca', '$2y$10$Tmg8pyUKgwaGYylbAqi95OPyUo/aD0REy0bxcU8TavgXVs.ay1Gni', '2505723761', 'Box 466, 35 Grosskleg Way', 'Lake Cowichan', 'BC', 'V0R 2G0', 'Canada', '2008-09-10', 'female', 'Child', NULL, 'MORRIS, AVA NATALIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_6209.jpg', NULL, NULL, '2019-07-13 03:32:13'),
(205, 'Avery', NULL, 'Fane', 'averyfane.com@gmail.com', '$2y$10$C1WDCgxnQjYjrIEdKlPATeJK5h6gTsttEdWwIDbdjHK6RLMobTo3G', '17783185571', '308-2238 Kingsway', 'Vancouver', 'BC', 'V5N2T7', 'Canada', '1981-11-02', 'male', 'Adult', NULL, 'FANE, AVERY', 'https://www.linkedin.com/in/averyfaneactual', 'Facebook.com/averyfaneactual', 'Imdb.me/averyfane', '', '', NULL, 'Yes', 'Yes', 'Screenshot_20180517-154337.jpg', NULL, NULL, '2019-07-13 03:32:32'),
(206, 'Avery', 'June', 'Hilliard', 'paulahilliard@shaw.ca', '$2y$10$dUWS8RP6UXIDTF11qQE9cun2/NpE78.VRKN0kyeiOz3DHlTiRVY/u', '778-840-4741', '284 W  St  James Road', 'North Vancouver', 'BC', 'V7N 2P3', 'Canada', '2005-02-26', 'female', 'Teen', 'averyhilliard_', 'HILLIARD, AVERY JUNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(207, 'Avery', NULL, 'Reid', 'avery.reid@outlook.com', '$2y$10$qoucorfgHEI2Rci0f5hA9OlJmrow9vQsPmHYskdfkmCHBuFs6Sz4C', '514-806-4724', '938 Sicamore Dr', 'Kamloops', 'BC', 'V2B 6S2', 'Canada', '1996-10-30', 'female', 'Adult', NULL, 'REID, AVERY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Avery-Reid-Close-Headshot.jpg', NULL, NULL, '2019-07-13 03:32:59'),
(208, 'Aya', 'Mikki', 'Furukawa', 'timetwosleep@gmail.com', '$2y$10$z7gSKCznV30K..jCEEdyAekTwotSOJhhbZ/xEkM823owLI.aXFDyi', '778-887-8464', '11-839 West 17th Street', 'North Vancouver', 'BC', 'V7P3N9', 'Canada', '1999-04-12', 'female', 'Adult', NULL, 'FURUKAWA, AYA MIKKI', NULL, NULL, 'https://www.imdb.com/name/nm4961141/', '', '', NULL, 'Yes', 'No', 'af-10-Edit-1.jpg', NULL, NULL, '2019-07-13 03:33:21'),
(209, 'Ayanna', 'Sophia Elizabeth', 'Bermudez-Boivin', 'arielleboivin@gmail.com', '$2y$10$BBtUjs7qgRz2xyzpVTFs7OigQcglG2815VAotgXeG6dRxiL3aZYHm', '7789773692', '2890 Leigh Rd', 'Victoria', 'BC', 'V9B4G3 ', 'Canada', '2009-08-03', 'female', 'Child', NULL, 'BERMUDEZ-BOIVIN, AYANNA SOPHIA ELIZABETH', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '4EE73652-6B7C-417D-8060-E75B761D16E6.jpeg', NULL, NULL, '2019-07-13 03:33:48'),
(210, 'Ayden', 'James', 'Stevenson', 'jodynlinnea@gmail.com', '$2y$10$qjf9kOr2/CJoj4NncojVQ.KnFgEcEF9d2Np99W6mgfBodkTd5gMSK', '2508963189', '898 Lakeside Pl', 'Langford', 'BC', 'V9B 4H7', 'Canada', '2007-01-19', 'male', 'Teen', NULL, 'STEVENSON, AYDEN JAMES', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'image-1.jpg', NULL, NULL, '2019-07-13 03:34:53'),
(211, 'Ayla', NULL, 'Amano', 'ayla.amano@gmail.com', '$2y$10$BLz2OmG/ihcZFAGYfO36GeL5O1C3h1bpil9pp8M4u.PSa2EwWnN/C', '7787511716', '778 East 17th Avenue', 'Vancouver', 'BC', 'V5V 1B9', 'Canada', '1987-04-03', 'female', 'Adult', NULL, 'AMANO, AYLA', 'https://www.linkedin.com/feed/', NULL, NULL, '', '', NULL, 'No', 'No', 'Close-up-smiling.jpg', NULL, NULL, '2019-07-13 03:36:23'),
(212, 'Ayla', 'Patricia Anne ', 'Colley', 'desirewickenden@gmail.com', '$2y$10$zk4X2/hK09g20sH2VbLNuO3GkyNR/7EA5Hk3mbMF8ClRfUSQbfiAG', '6047687108', '#41 3942 Columbia valley rd ', 'Cultus lake ', 'BC', 'V2R 5B2', 'Canada', '2009-04-01', 'female', 'Child', NULL, 'COLLEY, AYLA PATRICIA ANNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '80E9CBF6-EB34-420A-9A2F-AB0B5BD09F58.jpeg', NULL, NULL, '2019-07-13 03:37:28'),
(213, 'Ayumu', 'Eric', 'Kojima ', 'kaoru1.kojima@gmail.com', '$2y$10$daw3QaMKyZzxetbWmSNCL.0O6hkrHuvNqFLaoAeovQlQWTDpJ.RsS', '6043582152', '312-13th street, west', 'Vancouver', 'BC', '1v5 4b5', 'Canada', '2006-10-29', 'male', 'Child', NULL, 'KOJIMA , AYUMU ERIC', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20181217_221710.jpg', NULL, NULL, '2019-07-13 03:38:40'),
(214, 'AYUMU', 'ERIC', 'KOJIMA', 'kaoru1.kojima@gmail.com', '$2y$10$6WiPpXDQnoUuZ4maUoLMoODzyS2utdPZ0NrGO.Urz5wO2WiO39O4C', '7788398107', 'L312, 13th Street, West', 'North Vancouver', 'BC', 'V7M 1N8', 'Canada', '2006-10-29', 'male', 'Child', NULL, 'KOJIMA, AYUMU ERIC', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_4460.jpg', NULL, NULL, '2019-07-13 03:41:24'),
(215, 'Azusa', NULL, 'Ramos', 'azusa0902@hotmail.com', '$2y$10$idIMkNE.p7Zn..NZvpUKpO.A8mDXXE6WbU2eKdoGs3wXpvxjbwFIu', '778-895-9489', '12029 211st', 'maple ridge', 'BC', 'v2x 8k6', 'Canada', '1980-09-02', 'female', 'Adult', NULL, 'RAMOS, AZUSA', NULL, 'azusa ramos', NULL, '', '', NULL, 'No', 'No', 'Capture.jpg', NULL, NULL, '2019-07-13 03:42:09'),
(216, 'Baffeh', 'Prince/Peter', 'Sheriff ', 'sheriff.bafeh@gmail.com', '$2y$10$/m2Z.sp2RuE0EilmADFniexzBoa6AaO2lPEj0Kiz7FpH8iI7Nn2Uy', '2369880996', '#1105-8 smithe mews', 'Vancouver ', 'BC', 'V6B 0B5', 'Canada', '1995-05-09', 'male', 'Adult', '@vancityprince', 'SHERIFF , BAFFEH PRINCE/PETER', NULL, 'Bafeh sheriff ', NULL, '', '', NULL, 'No', 'No', '0CA011D4-4C0B-4B31-B4CF-237EAC3A44D7.jpeg', NULL, NULL, '2019-07-13 03:42:33'),
(217, 'Baffeh', 'Prince/Peter', 'Sheriff ', 'sheriff.bafeh@gmail.com', '$2y$10$FZbiG53RV6XS./8N9/pDc.jOyAcj8BIkT5Yc07Wxy0bjudp0PUNuC', '2369880996', '#1105-8 smithe mews', 'Vancouver ', 'BC', 'V6B 0B5', 'Canada', '1995-05-09', 'male', 'Adult', '@vancityprince', 'SHERIFF , BAFFEH PRINCE/PETER', NULL, 'Bafeh sheriff ', NULL, '', '', NULL, 'No', 'No', '0CA011D4-4C0B-4B31-B4CF-237EAC3A44D7-1.jpeg', NULL, NULL, '2019-07-13 03:42:50'),
(218, 'Bahaneh ', NULL, 'Grewal', 'bahanehgrewal@gmail.com', '$2y$10$xn0e0VbaKZTN6/2OfmN8zuOrVDc8VxK9RDLfCx1YGyS/NDZ/J0k7a', '6047290727', '1-7198 Barnet Rd ', 'Burnaby ', 'BC', 'V5A 1C9 ', 'Canada', '1969-07-18', 'female', 'Adult', '@Bon.Bahar ', 'GREWAL, BAHANEH', NULL, NULL, 'http://www.agencyclick.com/BahanehG/resume', '', '', NULL, 'Yes', 'Yes', '3C6F5A38-AA72-4D79-B7F5-2BB9403F731F.jpeg', NULL, NULL, '2019-07-13 03:43:11'),
(219, 'Bahar', NULL, 'Babasadegh ', 'spring1870@yahoo.com', '$2y$10$dFB85SabMNos0Jp3H6N/BOpxDBK5gvAfpTFqP6I/eNBao5VG23kI2', '7789960494', '1104 -7351 halifax st', 'Burnaby ', 'BC', 'V5a4h2 ', 'Canada', '1979-04-08', 'female', 'Adult', NULL, 'BABASADEGH , BAHAR', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '1ABB6BC5-4033-47A0-8682-C6F9A625C867.jpeg', NULL, NULL, '2019-07-13 03:44:17'),
(220, 'Bahar', NULL, 'Babasadegh ', 'spring1870@yahoo.com', '$2y$10$1sxzTF1y36MDfvkb5GLrTeQ9PtFpqXird1Pmtvh5u4s1Mr/TiD8KO', '7789960494', '1104-7351 Halifax st', 'Burnaby ', 'BC', 'V5a4h2 ', 'Canada', '1979-04-08', 'female', 'Adult', NULL, 'BABASADEGH , BAHAR', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '4329ABC8-95D8-4703-9349-30BF138DE013.jpeg', NULL, NULL, '2019-07-13 03:45:25'),
(221, 'Bailey', NULL, 'Bourre', 'bbourre86@gmail.com', '$2y$10$NGMgvJVsbWdxZXkra/dQc.xQ07tcqWo7VX1PbiFod8nlpwu1bf6f.', '2368873136', '5-20649 Edelweiss Drive', 'Agassiz', 'BC', 'V0M 1A1', 'Canada', '1986-08-24', 'female', 'Adult', NULL, 'BOURRE, BAILEY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'me.jpg', NULL, NULL, '2019-07-13 03:46:00'),
(222, 'Anne', NULL, 'Kadwell', 'akadwell23@me.com', '$2y$10$fc5nHCc3VCWg/nMkjg4Je.BkJ7SoOZTNMKhnYZjX1mKhRxHeCunxq', '604 306 8920', 'PO Box 3156', 'Garibaldi Highlands (within Squamish)', 'BC', 'V0N1T0', 'Canada', '1968-08-19', 'female', 'Adult', NULL, 'KADWELL, ANNE', 'www.linkedin.com/in/ak2317', NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_2339.jpg', NULL, NULL, '2019-07-13 03:46:12'),
(223, 'Baldeep', NULL, 'Bahat', 'dbbahat@gmail.com', '$2y$10$4sFVndlMT42xebI88bYQVeAqaXfqHLwcr.rdIU3ed7cDUPgE65SeS', '7788652500', '8523 152st', 'Surrey ', 'BC', 'V3s3m8', 'Canada', '1986-01-20', 'male', 'Adult', NULL, 'BAHAT, BALDEEP', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(224, 'Barak', NULL, 'Idi', 'idibarak4@gmail.com', '$2y$10$tZo9kYVSmd9ugv45pegsbesoRJQmRBNHs4YYlVp0khdSamxQr9.b.', '2368803705', '8139 145 street', 'Surrey', 'BC', 'V3S 9J6', 'Canada', '1994-03-13', 'male', 'Adult', 'www.instagram.com/officialafrobar', 'IDI, BARAK', NULL, 'https://www.facebook.com/afrobarmusic/', NULL, '', '', NULL, 'No', 'No', 'PSX_20190113_235822_1.jpg', NULL, NULL, '2019-07-13 03:46:14'),
(225, 'Barbara', NULL, 'Pearce', 'b.jpearce@hotmail.com', '$2y$10$xSjqF8ddG.nGDY4VEHDdyOYp5U7Kp3HYL0TNxeKu7I/5k.JMpTc6u', '778 228-1072', '#11-7883 Knight St', 'Vancouver', 'BC', 'V5P 2X5', 'Canada', '1957-12-21', 'female', 'Adult', NULL, 'PEARCE, BARBARA', NULL, 'https://www.facebook.com/profile.php?id=100009183627308', 'https://www.imdb.com/name/nm9376848/', '', '', NULL, 'Yes', 'Yes', 'me-6-6-18b.jpg', NULL, NULL, '2019-07-13 03:46:32'),
(226, 'Anne', NULL, 'Lin', 'anneikeda@gmail.com', '$2y$10$59g/JtzQ9gvtYSkCD6nF8uH7F2rYeeabx/Dd2hMwYwSkesLmzBIiS', '7789287325', '15677 102 Avenue', 'Surrey', 'BC', 'V4N2G5', 'Canada', '1981-06-24', 'female', 'Adult', NULL, 'LIN, ANNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'anne-face.jpg', NULL, NULL, '2019-07-13 03:46:36'),
(227, 'Annelyn', 'Ruby', 'Victor', 'avictor.cheam@gmail.com', '$2y$10$JkB2uEJFn264AjhXlQcVGuSF9kHagtkTJbA3ec14p9xoGEt2TwXNC', '604-316-8884', '5, 1700 Mackay Cres', 'Agassiz', 'BC', 'V0M 1A3', 'Canada', '2004-01-23', 'female', 'Teen', NULL, 'VICTOR, ANNELYN RUBY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20170727_180653-1.jpg', NULL, NULL, '2019-07-13 03:47:04'),
(228, 'Barbara', NULL, 'Renaud', 'brenaud@shaw.ca', '$2y$10$XY98RuTzU18YilETMH5rW.2NtU9jlyYdA/36nXWh7Rw/w3JY1FH6a', '604-607-4796', '22910 Purdey Avenue', 'Maple Ridge', 'BC', 'V2X 7M1', 'Canada', '1963-04-20', 'female', 'Adult', NULL, 'RENAUD, BARBARA', NULL, 'https://www.facebook.com/barbara.renaud.18', 'https://www.imdb.com/name/nm2781842/', '', '', NULL, 'No', 'No', 'A77A0029-298A-4F4A-BA5C-7423C10EFE9D.png', NULL, NULL, '2019-07-13 03:47:26'),
(229, 'Anne-Marie', NULL, 'Dupperon', 'dupperonam@gmail.com', '$2y$10$0PKmwNSUfyjTXyzcR6nexekv/6IHA27g/1pCVTvsgcjzzv3GANd6q', '6043163900', '6950 Oakwood drive', 'Agassiz', 'BC', 'V0M 1A3', 'Canada', '1954-10-07', 'female', 'Adult', NULL, 'DUPPERON, ANNE-MARIE', NULL, 'Petunia Dupperon/AnnieDupperon', NULL, '', '', NULL, 'No', 'No', 'IMG_0352.jpg', NULL, NULL, '2019-07-13 03:47:32'),
(230, 'Bauer', 'Keenan', 'Pinsonneault', 'ml123456@gmail.com', '$2y$10$GgYUeJIkVSwbMnCqRoNrvuzna3ZKDziAzTtzd3lLnG48ma4vCA1P2', '2508186688', '771 Canterbury road', 'Victoria', 'BC', 'V8X3E4', 'Canada', '2009-11-18', 'male', 'Child', NULL, 'PINSONNEAULT, BAUER KEENAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'MVIMG_20190321_1048432.jpg', NULL, NULL, '2019-07-13 03:47:41'),
(231, 'Annette', 'Audrey ', 'Gehring ', 'Mrsbuy@hotmail.com', '$2y$10$F6.LofPA4KdmS5q2rGeK4OMcmf6DKj5ECdhZV0pPwdma//RDsPbzC', '6044401174', '8698 206B St', 'Langley', 'BC', 'V1M3X5', 'Canada', '2006-06-02', 'male', 'Child', NULL, 'GEHRING , ANNETTE AUDREY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20180623_153108-1.jpg', NULL, NULL, '2019-07-13 03:47:50'),
(232, 'Anni', 'Monica ', 'Torikka', 'anniandthegirls@hotmail.com', '$2y$10$0uolJRfj4rU/0zmea2F9uOuyGvIc2R5KOCvFi0rZgydqbOlbHODXy', '2503055122', '456 Mulberry Ave ', 'Kamloops ', 'BC', 'V2b2r5 ', 'Canada', '1958-01-24', 'female', 'Adult', NULL, 'TORIKKA, ANNI MONICA', NULL, 'Anni Torikka ', NULL, '', '', NULL, 'No', 'No', 'CB7F1A14-FA60-4B68-A660-CC1581F42FED.jpeg', NULL, NULL, '2019-07-13 03:48:12'),
(233, 'Becca', NULL, 'Burr', 'beccaburr0@gmail.com', '$2y$10$QbpqN3D8s.l6DBjX85x9ueMXLhUY.WQS8aga1tvXwxb8fokbMItJK', '6047012588', '1688 parkwood drive ', 'Agassiz ', 'BC', 'V0m1a2', 'Canada', '1993-01-15', 'female', 'Adult', NULL, 'BURR, BECCA', NULL, 'Becca burr ', NULL, '', '', NULL, 'No', 'No', '5B9DA0A4-8A49-47AD-A6D0-0572B08B210D.jpeg', NULL, NULL, '2019-07-13 03:48:20'),
(234, 'Ann-Marie', NULL, 'Leonard', 'amleonard43@gmail.com', '$2y$10$j3ElCQbn71nrjY1ubX.AuubozhTXwWZCeQrkpkMr.4e10mEtc0/au', '514-601-4889', '3528 4th av.', 'vancouver', 'BC', 'V6R 1N8', 'Canada', '1982-10-19', 'female', 'Adult', NULL, 'LEONARD, ANN-MARIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'ann-marie-8.jpg', NULL, NULL, '2019-07-13 03:48:44'),
(235, 'Becky', 'I', 'Goebel', 'goebelbecky@gmail.com', '$2y$10$//2ajC7dLlggACJc8vEqwe98ut3gxKvdqdnMbXxd/uJZ3oMQstDGe', '604-354-5016', '55 East 12th Ave. Unit 207', 'Vancouver', 'BC', 'V5T4J4', 'Canada', '1991-04-22', 'female', 'Adult', '@actuallyitsaxel', 'GOEBEL, BECKY I', 'https://www.linkedin.com/in/beckygoebel/', 'https://www.facebook.com/actuallyitsaxel', 'https://www.imdb.com/name/nm8613011/', '', '', NULL, 'No', 'No', 'IMG_2361.jpg', NULL, NULL, '2019-07-13 03:49:15'),
(236, 'Anthea', 'Eowyn', 'Mackey', 'loribonertz@gmail.com', '$2y$10$P8WM1S42HbWwE8k20x0TCue/s6y/55JkMmfcURBsIgYs6RdSMJOU6', '778-867-2095', '1805 Primrose Crescent', 'Kamloops', 'BC', 'V1S 0A6', 'Canada', '2003-01-20', 'male', 'Teen', 'theamackey', 'MACKEY, ANTHEA EOWYN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_0372.jpeg', NULL, NULL, '2019-07-13 03:49:35'),
(237, 'Bella', 'Rose', 'McCairns', 'janetmccairns@telus.net', '$2y$10$5M3rEJTmOG3mHgJedBXeTeWsH7f4wX6blUYW8Jot3bWov4WUqKy.q', '604-315-5576', '208 - 3600 Windcrest Drive', 'North Vancouver', 'BC', 'V7G2S5', 'Canada', '2003-03-06', 'female', 'Teen', 'bella_rose_m', 'MCCAIRNS, BELLA ROSE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_3374.jpg', NULL, NULL, '2019-07-13 03:49:46'),
(238, 'Anthony', NULL, 'Anglehart', 'cassandracurdie@hotmail.com', '$2y$10$NkhP.352GLb1535Sl4EWwu1XfIxKvurAUSlGp8jYvdzOmUZhUJQYm', '604-316-7465', '7373 Elm Rd', 'Agassiz', 'BC', 'V0M 1A2', 'Canada', '2007-11-11', 'male', 'Child', 'anthony_anglehart', 'ANGLEHART, ANTHONY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(239, 'Anthony', NULL, 'Demare', 'tonydemare@gmail.com', '$2y$10$N7j5xDKkQQTo2L/puLbcJ.GEFTiVAvPj9N4C8coFyvDAqDGR7XfIO', '6043079815', '1171 Marsden court', 'Burnaby', 'BC', 'V5A3K2', 'Canada', '1991-02-02', 'male', 'Adult', 'ademare', 'DEMARE, ANTHONY', NULL, NULL, 'https://m.imdb.com/name/nm8800098/', '', '', NULL, 'Yes', 'Yes', 'Anthony-Demare-blazer-headshot.jpg', NULL, NULL, '2019-07-13 03:49:47'),
(240, 'Anthony ', NULL, 'Rodriguez ', 'rsamudio1490@gmail.com', '$2y$10$rB8hlb23H2iwpahlTv1ptOZBHKZwqu3TsyZTOQ6SIetAjDKxPBKwy', '7785354696', '2335 W 51 st ave ', 'Vancouver ', 'BC', 'V6p1e8 ', 'Canada', '1990-08-14', 'male', 'Adult', NULL, 'RODRIGUEZ , ANTHONY', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'C7E5022E-F30F-414C-9E79-8093480330F6.jpeg', NULL, NULL, '2019-07-13 03:50:07'),
(241, 'Ben', NULL, 'Loyst', 'benjaminloyst@gmail.com', '$2y$10$w76Oj/rvOKsLUEciPZcR/umagmB2ccES9rQ/wd4Pr9VHtutAH4be2', '2506676711', '220 Machleary Street', 'Nanaimo', 'BC', 'V9R 2G6', 'Canada', '1999-10-11', 'male', 'Adult', NULL, 'LOYST, BEN', NULL, 'https://www.facebook.com/ben.loyst', NULL, '', '', NULL, 'No', 'No', '1-Ben-Loyst-8X10-2-1.jpg', NULL, NULL, '2019-07-13 03:50:19'),
(242, 'Ben', NULL, 'Loyst', 'benjaminloyst@gmail.com', '$2y$10$1LWnyQSGKNK6XSS5T0AWsOQ.oe6yIJ2bVk6cpA.BW.tUQrRowljCW', '2506676711', '220 Machleary Street', 'Nanaimo', 'BC', 'V9R 2G6', 'Canada', '1999-10-11', 'male', 'Adult', NULL, 'LOYST, BEN', NULL, 'https://www.facebook.com/ben.loyst', NULL, '', '', NULL, 'No', 'No', '1-Ben-Loyst-8X10-2-1-1.jpg', NULL, NULL, '2019-07-13 03:50:35'),
(243, 'Antony', NULL, 'Le', 'tnyle55@hotmail.com', '$2y$10$sW/xfB0/NgYxNR8cOR1x2.8.Mt/6MgtCFcv8RRrSNG8dVHSHPyKum', '778-839-2052', '5869 123A Street', 'Surrey', 'BC', 'V3X 1Y3', 'Canada', '1987-08-27', 'male', 'Adult', NULL, 'LE, ANTONY', 'https://www.linkedin.com/in/antonyle/', 'https://www.facebook.com/antonyle55', NULL, '', '', NULL, 'Yes', 'No', '10494602_10154464288035074_6908255480926597801_n.jpg', NULL, NULL, '2019-07-13 03:50:33'),
(244, 'Bernadette', 'Villalon', 'Ortilla', 'pinkyortilla@gmail.com', '$2y$10$bDRXdssCalV/dGuTcoyxEu9ipX4j4taGXIyWizYJPedHBcCuoCvbe', '6047936054', '690 Coquihalla street', 'Hope', 'BC', 'V0x 1L0', 'Canada', '2018-10-27', 'female', 'Adult', 'Pinkypot27', 'ORTILLA, BERNADETTE VILLALON', NULL, 'Pinky_102789@yahoo.com', NULL, '', '', NULL, 'No', 'No', '22FE6D32-6F10-4F72-9ECE-6ADE6605F943.jpeg', NULL, NULL, '2019-07-13 03:50:39'),
(245, 'Bernadette Anne Therese', NULL, 'Hall', 'annalice.hall@gmail.com', '$2y$10$i3we93/rnoXX1DhYecMEQe4FFi9oMyY.69kK86He/hQetz13bkq9C', '7783196133', 'Suite 313 9344 Cameron St', 'Burnaby', 'BC', 'V3J1L9', 'Canada', '2000-11-22', 'female', 'Teen', NULL, 'HALL, BERNADETTE ANNE THERESE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_7892pscc.jpg', NULL, NULL, '2019-07-13 03:51:11'),
(246, 'AOY', NULL, 'CHIU', 'aoyc@shaw.ca', '$2y$10$oBHMvnFMR3/5L1n9PmyfIu3kvvjwjfjYXbaHAOvaupbU9rpK4j7da', '7789866762', '8380 Francis Road', 'Richmond', 'BC', 'V6Y 1A4', 'Canada', '2016-12-10', 'female', 'Teen', NULL, 'CHIU, AOY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Aoy-Finals-web-size-1.jpg', NULL, NULL, '2019-07-13 03:51:31'),
(247, 'Bernard', NULL, 'Gambier', 'gmb.bernard@gmail.com', '$2y$10$60pZBF7ZGk6B3ku0cLf2h.F77NEQn4UIizH9iLEfdrhZM2DUtGJpu', '6043639673', '439 Mclean dr', 'Vancouver', 'BC', 'V5l 3M5', 'Canada', '1993-06-01', 'male', 'Adult', 'Bernard_gmb', 'GAMBIER, BERNARD', NULL, 'Bernard Gambier', NULL, '', '', NULL, 'Yes', 'No', '5D42764E-E1C6-46BA-A35A-013A1D23DBC4.jpeg', NULL, NULL, '2019-07-13 03:51:50'),
(248, 'Apidi', NULL, 'Onyalo', 'apidionyalo@hotmail.com', '$2y$10$QzMKe3YWiJhPxebTVropOuSbeP5XqmCaepkjn9fI57vDLXeslho.6', '6043769228', '103-2290 Wall St', 'Vancouver', 'BC', 'V5L 1B6', 'Canada', '1985-08-17', 'female', 'Adult', '@apidi', 'ONYALO, APIDI', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_9496.jpg', NULL, NULL, '2019-07-13 03:51:57'),
(249, 'April', 'Stephanie', 'Bryce ', 'Patiencepayton@gmail.com', '$2y$10$gW4kd4Gl1vPFwlx5OXGkbOyIxjR8WazLFLZt7og.F0tvZtLX1ohse', '6046445861', '217a 7155 Hall road', 'surrey', 'BC', 'V3W 4X4', 'Canada', '1985-01-22', 'female', 'Adult', NULL, 'BRYCE , APRIL STEPHANIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'FB_IMG_1551231470203.jpg', NULL, NULL, '2019-07-13 03:52:29'),
(250, 'April ', 'Evelyn', 'Steegstra', 'april.gallinger@gmail.com', '$2y$10$FmWE5hnG9AllwkeFj3x8Ce8TIvuvKXB7vT/XD36Pn5kwR6zxbkoEG', '604-378-2636', '9875 Gracemar Dr', 'Chilliwack', 'BC', 'V2P7P1', 'Canada', '1997-09-13', 'female', 'Adult', NULL, 'STEEGSTRA, APRIL  EVELYN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'LRM_EXPORT_20180628_200513.jpg', NULL, NULL, '2019-07-13 03:52:55'),
(251, 'Bernarda', NULL, 'Antony', 'bernarda.antony@gmail.com', '$2y$10$6fZjosrj9A.iyOR3F5GYRu.wLi5eUn8Tqel7XUSGCePpnVEXwLLCW', '+1(604)401-2278', '3333 Commercial Drive Unit 106', 'Vancouver', 'BC', 'V5N4E5', 'Canada', '1991-01-22', 'female', 'Adult', NULL, 'ANTONY, BERNARDA', 'https://www.linkedin.com/in/bernardaantony/', 'https://www.facebook.com/bernarda.antony', NULL, '', '', NULL, 'No', 'No', 'IMG_2365.jpg', NULL, NULL, '2019-07-13 03:53:27'),
(252, 'April ', 'Evelyn', 'Steegstra', 'april.gallinger@gmail.com', '$2y$10$PmUchDQisce7Dy.wtlgQuO1sItMF1vGLjG1eslfboGPoQiOi.T6.W', '604-378-2636', '9875 Gracemar Dr', 'Chilliwack', 'BC', 'V2P7P1', 'Canada', '1997-09-13', 'female', 'Adult', NULL, 'STEEGSTRA, APRIL  EVELYN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'LRM_EXPORT_20180628_200513-1.jpg', NULL, NULL, '2019-07-13 03:53:49'),
(253, 'Aramesh', NULL, 'Atash', 'arameshatash@gmail.com', '$2y$10$mTimeNME9hvcMBwAEGodxOuWtEhB0LU7oQglw0UBSnFXaR1TiaO0q', '6047828064', '3C, 328 Taylor Way', 'West Vancouver', 'BC', 'V7T 2Y4', 'Canada', '1985-04-01', 'female', 'Adult', '@meshtash', 'ATASH, ARAMESH', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'PAN04393.jpg', NULL, NULL, '2019-07-13 03:54:35'),
(254, 'Araz', NULL, 'Yaghoubi', 'ARAZ.YAGHOUBI93@GMAIL.COM', '$2y$10$FBr.dxoAqD.b0ze/.8M8M.PE..M2lTX1OFuc4bhASvqpfF3PkQ8Ki', '778-384-8078', '109-200 Keary St', 'New Westminster', 'BC', 'V3L 0A6', 'Canada', '1993-09-12', 'male', 'Adult', NULL, 'YAGHOUBI, ARAZ', NULL, NULL, 'http://www.imdb.com/name/nm7113303/', '', '', NULL, 'Yes', 'Yes', 'unnamed-4.jpg', NULL, NULL, '2019-07-13 03:54:48'),
(255, 'Aria ', 'Sophia', 'Sium', 'guebezail@gmail.com', '$2y$10$yQYsNHNetAJ.9aG73BNb1Okexbx3EVm4jOg4K0JvncYlLLyQIUX46', '6045189420', '13909 59a ave', 'Surrey', 'BC', 'V3xX0G6', 'Canada', '2015-10-17', 'female', 'Child', '@luluhotstuff', 'SIUM, ARIA  SOPHIA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL);
INSERT INTO `performers_bk` (`id`, `firstName`, `middleName`, `lastName`, `email`, `password`, `phonenumber`, `address`, `city`, `province`, `postalCode`, `country`, `date_of_birth`, `gender`, `applicationfor`, `instagram`, `twitter`, `linkedin`, `facebook`, `imdb_url`, `agent_name`, `union_name`, `union_number`, `agent`, `union_det`, `upload_primary_url`, `remember_token`, `created_at`, `updated_at`) VALUES
(256, 'Aricin', 'Reid', 'seto', 'lgseto@telus.net', '$2y$10$8kLrQZMP4VbbrfVgENzVj.MotgqfcgeItV5io2SLCAx/GeDh1Y31i', '2508885796', '4258 cedar hill road', 'Victoria', 'BC', 'v8n 3c5', 'Canada', '2008-06-20', 'male', 'Child', NULL, 'SETO, ARICIN REID', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'IMG_3215.png', NULL, NULL, '2019-07-13 03:55:16'),
(257, 'Arika', NULL, 'Barnsley', 'alikaiwata@gmail.com', '$2y$10$LykuaKHrwk4oI.Tk93ucTuzUemxHlvGpRPQcFXbpvcy/3mResS92.', '6047046773', '208 1683 Adanac Street', 'Vancouver', 'BC', 'V5L 2C7', 'Canada', '1989-10-09', 'female', 'Adult', NULL, 'BARNSLEY, ARIKA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'E5C6931A-8315-4ED5-9364-3E4862055334.jpeg', NULL, NULL, '2019-07-13 03:55:40'),
(258, 'Arne', 'Jenson', 'Zabell', 'ajzabell@live.ca', '$2y$10$7kfNaerxqXM/QP8Dshgobe7DsqAt09EpSRS6AWSxITXtnffa8wFeu', '6048690443', '14470 Alpine Blvd.', 'Hope', 'BC', 'v0x1l5', 'Canada', '1996-12-16', 'male', 'Adult', '@ajzabell', 'ZABELL, ARNE JENSON', NULL, 'facebook.com/SuuhDude', NULL, '', '', NULL, 'No', 'No', 'G0254258.jpg', NULL, NULL, '2019-07-13 03:56:06'),
(259, 'Arnie', NULL, 'Gauthier', 'arnie@arnie.ca', '$2y$10$xtKs5yyUiU9uT7ku05AhLeZqjNESJy8u0Gm/sThP4uzDtmK5Pn9ni', '604-916-4349', '3534 Cambridge St.', 'Vancouver', 'BC', 'V5K 1M4', 'Canada', '1957-09-06', 'male', 'Adult', NULL, 'GAUTHIER, ARNIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_20190201_201702.jpg', NULL, NULL, '2019-07-13 03:56:45'),
(260, 'Arthur', 'Thomas', 'Buffie', 'artbuf@telus.net', '$2y$10$FF9p3BuN6QcnsFwkzFGhMO.3pmKvTuTl4/FNt0iCPFtq19Rb/eSQC', '7783864775', '16125 79 Ave', 'Surrey', 'BC', 'V4N0K3', 'Canada', '1956-06-28', 'male', 'Adult', NULL, 'BUFFIE, ARTHUR THOMAS', NULL, 'https://www.facebook.com/art.buffie', NULL, '', '', NULL, 'Yes', 'Yes', 'Art.jpg', NULL, NULL, '2019-07-13 03:57:22'),
(261, 'Arthur', NULL, 'Walker', 'ambulat@icloud.com', '$2y$10$6.flXUGh3slhsC1jDCc7a.mCwrbW6VA1BlpIXPEOGY.AHNPDM80H2', '7789913876', '4791 Argyle St.', 'Vancouver', 'BC', 'V5N 3X9', 'Canada', '2007-07-27', 'female', 'Child', NULL, 'WALKER, ARTHUR', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '3E951120-C7CA-465A-A8EC-C5802B0BEFAB.jpeg', NULL, NULL, '2019-07-13 03:57:49'),
(262, 'Artur', 'Edmundovich', 'Peter', 'artemisauclair@gmail.com', '$2y$10$TDpGdlKT2uwlKGMceBIcBuaBUICEzV3pqiKzrac7vKMMMch75G6aO', '7788296370', '3260 parker st ', 'Vancouver', 'BC', 'V5k 2v8', 'Canada', '1993-08-06', 'male', 'Adult', 'Cyberbohemian', 'PETER, ARTUR EDMUNDOVICH', NULL, 'Artemis Auclair', NULL, '', '', NULL, 'Yes', 'Yes', 'IMG_9540.jpg', NULL, NULL, '2019-07-13 03:58:20'),
(263, 'Artur', 'Edmundovich', 'Peter', 'artemisauclair@gmail.com', '$2y$10$R8FFsBpcAAVCUcLVE5XfAuqybxiejc4EObuFWAm/5r7c5MUfduEVm', '778-829-6370', 'BSMT - 3260 Parker St', 'Vancouver', 'BC', 'v5k 2v8', 'Canada', '1993-08-06', 'male', 'Adult', 'https://www.instagram.com/cyberbohemian/', 'PETER, ARTUR EDMUNDOVICH', NULL, 'https://www.facebook.com/art.auclair', 'https://www.imdb.com/name/nm8843319/', '', '', NULL, 'No', 'Yes', 'art_peter_nofacialhair.jpg', NULL, NULL, '2019-07-13 03:59:11'),
(264, 'asccreative', 'freeman', 'casting', 'asccreative@info.com', '$2y$10$HS.coy1Ahu8gSHHJ7PH/HexOnUvNC/fhDG8aLmPhGkHdHkxG647RS', '7878788', 'asccreative', 'Vancouver', 'BC', 'A1A1A1', 'Canada', '1999-12-10', 'male', 'Adult', 'instagram', 'CASTING, ASCCREATIVE FREEMAN', 'linkedin', 'facebook', 'imdb', '', '', NULL, 'No', 'No', 'Hydrangeas.jpg', NULL, NULL, '2019-07-13 04:00:24'),
(265, 'asdasdsa', NULL, 'asdsadsa', 'sdfsad@dfsd.com', '$2y$10$GOmDt2uNqN3L4y6YzpOKvOd4s/8kwcFV5NoGnCZzIGc0uquBQxibu', 'asdsaa', 'sadsa', 'asdsadas', 'BC', 'sadsa', 'Canada', '2019-05-07', 'male', 'Adult', NULL, 'ASDSADSA, ASDASDSA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(266, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$U25T2ay7cnKZaTtYVuw6d.kWFckGWTxUfs3KU9MQcMzz3RealoaQa', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064.jpg', NULL, NULL, '2019-07-13 04:00:59'),
(267, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$vZvRciOdP4eqQY/hOh2WYOSlYaLUzbUSFG3P/L8FeGsuSBdohddqu', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064-1.jpg', NULL, NULL, '2019-07-13 04:03:23'),
(268, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$/e9uGpS2Zf6q5.IJu6N6COzgVL/13MkfhRr1q2Mp/ABBJ8FquX2kW', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064-2.jpg', NULL, NULL, '2019-07-13 04:05:15'),
(269, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$rFlo3Z4WcVheP8IPI2UTq.hFRxy0ZxcuGnxQAfA.JB4rIZDSAXzJC', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064-3.jpg', NULL, NULL, '2019-07-13 04:07:23'),
(270, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$qnBteRdUWNKjxOBYjlaBUOCXFPZ8l79BKvAOdcIlHC9n0SkQD/GZq', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064-4.jpg', NULL, NULL, '2019-07-13 04:09:01'),
(271, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$duzdUXdMlh15aNaV1ZpURuEPjKeJJ/uXo4HeNsj2Ro7Y5UOzex1Mm', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064-5.jpg', NULL, NULL, '2019-07-13 04:09:58'),
(272, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$61yo0Sv2028AD0MQgRQK0.JpKKFKjr0UHqHhit1j6gv2mJ1gZmWNe', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20190302-Sequence064-6.jpg', NULL, NULL, '2019-07-13 04:10:53'),
(273, 'Asher', 'Grayson', 'Percival', 'gordcrys@telus.net', '$2y$10$08r4iN2IIYaDKvc2MAuuqeWVZY7cy8m.ZoD3zdgbttzYwfMnj6ZZG', '250-882-4096', '2791 Lakehurst Drive ', 'Victoria ', 'BC', 'V9B 4S4', 'Canada', '2009-07-14', 'male', 'Child', 'https://www.instagram.com/ashergrayson/', 'PERCIVAL, ASHER GRAYSON', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Asher-Headshot-1.png', NULL, NULL, '2019-07-13 04:11:47'),
(274, 'Ashlee', NULL, 'Nunn', 'ashleenunn91@gmail.com', '$2y$10$8dvHIDaDLraTPgLDXywiWuJmE0IvGrRg2Vj7PoB8nnr75bWb02C82', '7782401181', '83 17097 64 ave', 'Surrey', 'BC', 'V3S 1Y5', 'Canada', '1991-08-01', 'female', 'Adult', NULL, 'NUNN, ASHLEE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_0069.png', NULL, NULL, '2019-07-13 04:12:06'),
(275, 'Ashley', 'Maddison ', 'Richter', 'thornswolfbane@gmail.com', '$2y$10$ixb/ipVJE36z/VAG8GMAweBfKIh3zIaqS7Wq/vN/4y4Rb5tWIqjt2', '+1 07785330774', '#2 242 Ontario street', 'Victoria', 'BC', 'V8V1N2', 'Canada', '1999-07-22', 'female', 'Adult', 'ash_nan22 ', 'RICHTER, ASHLEY MADDISON', NULL, 'Ashley Richter', NULL, '', '', NULL, 'No', 'No', 'IMG_20180720_180534.jpg', NULL, NULL, '2019-07-13 04:13:11'),
(276, 'Ashley', NULL, 'Clements', 'duckleburry@gmail.com', '$2y$10$BLT3XWpd3C8MFOrgZSa0AuOqJdug5/ZR3zkAKbpsf5ffD4iOm6Dxq', '2365221987', '46025 Cleveland ave', 'Chilliwack', 'BC', 'V2p2w9 ', 'Canada', '1992-08-12', 'female', 'Adult', NULL, 'CLEMENTS, ASHLEY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_0395.jpg', NULL, NULL, '2019-07-13 04:14:26'),
(277, 'Ashley', 'Marie', 'Fitger', 'ashleemarierose@outlook.com', '$2y$10$bdHKtJSsOC9ZyGiLD7yehu0IBsYRJXJBeNNPja7wY1JXbFkTA6ZkK', '2503181567', 'PO Box 1124', 'Barriere', 'BC', 'V0E 1E0', 'Canada', '1997-04-27', 'female', 'Adult', 'ashley_marie_fitger', 'FITGER, ASHLEY MARIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'FB_IMG_1505757754718.jpg', NULL, NULL, '2019-07-13 04:14:36'),
(278, 'Ashley ', 'Teagan ', 'Essig ', 'equinoxacres@shaw.ca', '$2y$10$pcMbffB5Gr13PzMipHV84.P2Hp9MfypaDsa5vmyF34AjXyJrCpQbW', '2507131114', '2065 Sanders rd ', 'Nanoose ', 'BC', 'V9p9c2 ', 'Canada', '2006-05-14', 'female', 'Teen', NULL, 'ESSIG , ASHLEY  TEAGAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '76C4F2B9-93E4-4BEA-BD7F-855A4127BE2C.jpeg', NULL, NULL, '2019-07-13 04:14:42'),
(279, 'Ashton', 'Rose', 'Fong ', 'tabicat10@shaw.ca', '$2y$10$MhWB9o9XLuYm8lVWgwk64.nZY0pTJ4OAwUxiZQfnU1T/5jbLgc5Be', '2506619508', '2904 Pickford rd', 'Victoria', 'BC', 'V9b2k2 ', 'Canada', '2006-08-08', 'female', 'Child', 'Ashton11fong', 'FONG , ASHTON ROSE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'C91A0F4C-E229-4906-A147-9ECDF3784242.jpeg', NULL, NULL, '2019-07-13 04:15:08'),
(280, 'Asia ', NULL, 'Domisiewicz ', 'domisiewicz@hotmail.con', '$2y$10$BobptpM/UGIOHJ2uCHrBxu/5FmQNlDk1Uhb0GXPnLCDIoQRSS1W7y', '604710623', '113 3875 w 4th', 'Vancouver ', 'BC', 'V5R 3h8', 'Canada', '1980-04-02', 'female', 'Adult', 'https://instagram.com/p/BdZfo6PhR_4/', 'DOMISIEWICZ , ASIA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '98C84537-0731-4B92-A24C-C4BC70BE66E6.jpeg', NULL, NULL, '2019-07-13 04:16:02'),
(281, 'Astrid', NULL, 'Santilli', 'santilli.astrid@gmail.com', '$2y$10$UbSkPhFLS9mgIIAb.whn.Ona/wnKVAXdPSM7BryJdBjsedKRSp5DO', '-9920', '1902-1288 West Cordova Street', 'Vancouver', 'BC', 'V6C 3R3', 'Canada', '1992-05-07', 'female', 'Adult', 'astridsantilli', 'SANTILLI, ASTRID', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_6044-Edit-2.jpg', NULL, NULL, '2019-07-13 04:16:21'),
(282, 'Astrid', NULL, 'Santilli', 'santilli.astrid@gmail.com', '$2y$10$2HQJu5euO7WxQZyduWCsfubd8zajo/dVrQ/956Z1SfSOar2K/Lew2', '17543028865', '1902-1288 West Cordova Street', 'Vancouver', 'BC', 'V6C 3R3', 'Canada', '1992-05-07', 'female', 'Adult', NULL, 'SANTILLI, ASTRID', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_6044-Edit-2.jpg', NULL, NULL, '2019-07-13 04:16:52'),
(283, 'Asuka', NULL, 'Nakamura', 'aska_ally@hotmail.com', '$2y$10$vD5M3vmNnG8Yc3f2a/kYiO6RUerm6YIEnFPbq8xD0VH1/5.ScaUuG', '7788385717', '1-230 Salter street', 'New Westminster ', 'BC', 'V3M0G1', 'Canada', '1976-03-08', 'female', 'Adult', NULL, 'NAKAMURA, ASUKA', NULL, 'AskaAllyNakamura', NULL, '', '', NULL, 'No', 'No', '218DBD69-4895-4164-A33E-1CD5EB8E3E8C.jpeg', NULL, NULL, '2019-07-13 04:17:22'),
(284, 'Atlas', NULL, 'Parker', NULL, '$2y$10$4X5Rvm.jIWSnHaw/r25xUelrBHxrdH93.cijFZcWaczMVBUp/kmbW', NULL, NULL, NULL, 'BC', NULL, 'Canada', '2012-09-29', 'male', 'Child', NULL, 'PARKER, ATLAS', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(285, 'Atlas', NULL, 'Parker', 'angeleparker@shaw.ca', '$2y$10$s3VMLEJwWqwcW64W4Prez.LnMLz9jtC3cVvi9sWnu7fUqSbXzosZG', '6042022857', '2540 Wallace crescent ', 'Vancouver', 'BC', 'V6r3v4 ', 'Canada', '2012-09-29', 'male', 'Child', NULL, 'PARKER, ATLAS', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', '1423FCE9-ED76-4F72-A73B-C6502BE108E4.jpeg', NULL, NULL, '2019-07-13 04:17:47'),
(286, 'Atlas', NULL, 'Parker', 'angeleparker@shaw.ca', '$2y$10$Oavfj7yhbwSJJGzSms.jgeuCmrBXC2PCgWdzT3ePiyGan45.q8M/q', '6045657240', '2540 Wallace Crescent', 'Vancouver', 'BC', 'V6R3V4', 'Canada', '2012-09-29', 'male', 'Child', NULL, 'PARKER, ATLAS', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'atlas-cute.jpg', NULL, NULL, '2019-07-13 04:18:08'),
(287, 'Atsushi', NULL, 'Kumo', 'kumoestate@gmail.com', '$2y$10$zPsN6lSnhDeRy61qfYPTX.5vDnTSVtSAXE/tPQ7/SFyAAGoWDZicm', '7789810188', '4300 Hermitage Dr', 'Richmond', 'BC', 'V7E 4N4', 'Canada', '1984-05-28', 'male', 'Adult', NULL, 'KUMO, ATSUSHI', NULL, 'Spencer Kumo', NULL, '', '', NULL, 'Yes', 'Yes', 'IMG_4962.jpg', NULL, NULL, '2019-07-13 04:18:28'),
(288, 'Aubriana ', 'Marrie', 'Frick', 'Shelleyfrick@shaw.ca', '$2y$10$.txZM34ujBvApxEpgppPpeNGjHHc8vflhF/A9697fz2XkIKhgOsUu', '250-857-5698', '2797 Lake End Rd', 'Victoria', 'BC', 'V9B 5T5', 'Canada', '2006-06-05', 'female', 'Teen', NULL, 'FRICK, AUBRIANA  MARRIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'image.jpeg', NULL, NULL, '2019-07-13 04:18:50'),
(289, 'Audrey', 'Barbara', 'Cooper', 'coultishmodels@telus.net', '$2y$10$7TCgcuIOI9BnkNrqBln7LOqtUxrBSCs1RQkN6F7XRaPZw3bRqaUf6', '250.516.4883', '8-50 Dallas Road', 'Victoria', 'BC', 'V8V1A2', 'Canada', '2011-04-28', 'female', 'Child', NULL, 'COOPER, AUDREY BARBARA', NULL, NULL, NULL, '', '', NULL, 'Yes', 'No', 'Audrey-Cooper.jpg', NULL, NULL, '2019-07-13 04:19:04'),
(290, 'August ', NULL, 'Bramhoff', 'augustbramhoff@rocketmail.com', '$2y$10$UxxkpwC7ff4grPCREHrEz.zBzO23kZshFGgAK0swNSqTxeS3MX/LW', '7785543772', '613-3588 Crowley Drive ', 'Vancouver ', 'BC', 'V5R 6H3', 'Canada', '1984-08-13', 'transgender', 'Adult', NULL, 'BRAMHOFF, AUGUST', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '58802B45-77C1-49DC-89C1-B7DA5099AD22.jpeg', NULL, NULL, '2019-07-13 04:19:28'),
(291, 'Austen', NULL, 'Crick', 'austencrick@outlook.com', '$2y$10$ZFIy978OUe3ZxybYa3XAdOYWBzib/Sj3Umo6KZvRtnjaZXNfu/mf.', '6049161778', '1759 Kenmore Rd', 'Victoria', 'BC', 'V8N 5B3', 'Canada', '1995-10-25', 'female', 'Adult', 'https://www.instagram.com/austenadele/', 'CRICK, AUSTEN', NULL, 'https://www.facebook.com/austen.crick', NULL, '', '', NULL, 'No', 'No', '52810359_552293598616168_7026577826379726848_n.jpg', NULL, NULL, '2019-07-13 04:20:10'),
(292, 'Austin', 'James', 'Friesen', 'anelephantstear@hotmail.com', '$2y$10$bPYfjo/jHOGBxH9L.OHwkuFb0t3fOowkUV8JVs9JXTSx5lHz5lUz6', '2508842493', '860 Pratt Road', 'Mill Bay', 'BC', 'V0R2P1', 'Canada', '2007-05-01', 'male', 'Child', NULL, 'FRIESEN, AUSTIN JAMES', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'DD7CBCEC-762A-4A44-9847-09E9AD4F4C24.jpeg', NULL, NULL, '2019-07-13 04:20:31'),
(293, 'Austin', 'J', 'Friesen', 'anelephantstear@hotmail.com', '$2y$10$hCwhwrn02lxQA5Tx68qfIuXCPpn1fWVWUckMEarXn1T.1UzYszq/W', '2508842493', '860 Pratt Road', 'Mill Bay', 'BC', 'V0R2P1', 'Canada', '2007-05-01', 'male', 'Child', NULL, 'FRIESEN, AUSTIN J', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'C353CA81-AB18-48BD-BC27-6B24A94B6DEB.jpeg', NULL, NULL, '2019-07-13 04:21:04'),
(294, 'Austin Rose', NULL, 'Kivinen', 'emilykivinen@gmail.com', '$2y$10$YrXQN/6zhGNK6agwq/O.7ORRa4FlM60U4Po6lAAjxqdXa40.fkfam', '7782550141', '35321 Corbett Place', 'Abbotsford', 'BC', 'V3G1K1 ', 'Canada', '2013-08-21', 'female', 'Child', NULL, 'KIVINEN, AUSTIN ROSE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '88CF22C2-4B9C-48D7-BDF2-F3B76D613100.jpeg', NULL, NULL, '2019-07-13 04:21:33'),
(295, 'Autumn', NULL, 'Bak', 'nqbak@telus.net', '$2y$10$SPyKG9ghGIgYuqeFzOkIG.TLA8XI.fwo..L/vNZonNaJw.ayJMjeq', '6042404729', '5215 Chamberlayne Ave', 'Delta', 'BC', 'V4K2J7', 'Canada', '2004-09-03', 'female', 'Teen', NULL, 'BAK, AUTUMN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20180728_140404.jpg', NULL, NULL, '2019-07-13 04:22:53'),
(296, 'Ava', 'Mackenzie', 'Jalava', 'Amieg@hotmail.com', '$2y$10$GLcRVoYqiSck7jaZT8Q.fe83cKZAf6dr994k1a40wMN1b.Faf8RtC', '6048452678', 'Po Box 121', 'Rosedale', 'BC', 'V0X 1X0', 'Canada', '2014-10-15', 'female', 'Child', NULL, 'JALAVA, AVA MACKENZIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'FB_IMG_1552834931528.jpg', NULL, NULL, '2019-07-13 04:23:07'),
(297, 'Ava', 'Lynn', 'Lancaster', 'paullancaster@shaw.ca', '$2y$10$9X7DdPclUAbarUfKSKwqx.GtRmW1tRtrL/EDeZSAUVg9niKMpF3mG', '250-858-8572', '#2-4619 Elk Lake Dr', 'Victoria', 'BC', 'V8Z-5M2', 'Canada', '2007-12-27', 'female', 'Child', NULL, 'LANCASTER, AVA LYNN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_4549.jpg', NULL, NULL, '2019-07-13 04:24:07'),
(298, 'Ava', NULL, 'McGaghey', 'jmteam@shaw.ca', '$2y$10$D6t6GxTLN.akd9gJDroeZeAH9H2A9R.5nofazM6JC.6mOBRVEfIGi', '2508933153', '2258 Setchfield Ave', 'Victoria', 'BC', 'V9B 6N8', 'Canada', '2005-04-27', 'female', 'Teen', NULL, 'MCGAGHEY, AVA', NULL, 'Ava McGaghey', NULL, '', '', NULL, 'No', 'No', '40410D09-123C-461E-875E-34FCD61182AA.jpeg', NULL, NULL, '2019-07-13 04:24:46'),
(299, 'Ava', 'Natalie', 'Morris', 'kmorris77@live.ca', '$2y$10$AF0DnGE7Vo95Ba2QYzs9OuHMywDDYwwpVqijGzj7ctPvKlwznvBqW', '2505723761', 'Box 466, 35 Grosskleg Way', 'Lake Cowichan', 'BC', 'V0R 2G0', 'Canada', '2008-09-10', 'female', 'Child', NULL, 'MORRIS, AVA NATALIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_6209.jpg', NULL, NULL, '2019-07-13 04:25:03'),
(300, 'Avery', NULL, 'Fane', 'averyfane.com@gmail.com', '$2y$10$tlBjy4p1q.0zz1TKjd5MM.E9UzY0N2MX6nyZy/bHo66hcgN9NQ/oa', '17783185571', '308-2238 Kingsway', 'Vancouver', 'BC', 'V5N2T7', 'Canada', '1981-11-02', 'male', 'Adult', NULL, 'FANE, AVERY', 'https://www.linkedin.com/in/averyfaneactual', 'Facebook.com/averyfaneactual', 'Imdb.me/averyfane', '', '', NULL, 'Yes', 'Yes', 'Screenshot_20180517-154337.jpg', NULL, NULL, '2019-07-13 04:25:21'),
(301, 'Avery', 'June', 'Hilliard', 'paulahilliard@shaw.ca', '$2y$10$QoO2VfB7/BVz1gJTQFCweeKTtqqcau9kh5D/aoOk0SR0oao6/vq7G', '778-840-4741', '284 W  St  James Road', 'North Vancouver', 'BC', 'V7N 2P3', 'Canada', '2005-02-26', 'female', 'Teen', 'averyhilliard_', 'HILLIARD, AVERY JUNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(302, 'Avery', NULL, 'Reid', 'avery.reid@outlook.com', '$2y$10$r01HGq4TsCAnFzRqI9GMG.OlRQmxpJpTi3kgar1Y5.qSc4YRqw666', '514-806-4724', '938 Sicamore Dr', 'Kamloops', 'BC', 'V2B 6S2', 'Canada', '1996-10-30', 'female', 'Adult', NULL, 'REID, AVERY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'Avery-Reid-Close-Headshot.jpg', NULL, NULL, '2019-07-13 04:25:45'),
(303, 'Aya', 'Mikki', 'Furukawa', 'timetwosleep@gmail.com', '$2y$10$HV4zyMw3miCOkis/DUBgwOiUL8dKbA7xW3b.tniB/xGw0Tc7JL2BG', '778-887-8464', '11-839 West 17th Street', 'North Vancouver', 'BC', 'V7P3N9', 'Canada', '1999-04-12', 'female', 'Adult', NULL, 'FURUKAWA, AYA MIKKI', NULL, NULL, 'https://www.imdb.com/name/nm4961141/', '', '', NULL, 'Yes', 'No', 'af-10-Edit-1.jpg', NULL, NULL, '2019-07-13 04:26:02'),
(304, 'Ayanna', 'Sophia Elizabeth', 'Bermudez-Boivin', 'arielleboivin@gmail.com', '$2y$10$kO9njkihb/HzbguvbTfI.uiuY4l9R78wefi6h200eKmlo91Ekc3FK', '7789773692', '2890 Leigh Rd', 'Victoria', 'BC', 'V9B4G3 ', 'Canada', '2009-08-03', 'female', 'Child', NULL, 'BERMUDEZ-BOIVIN, AYANNA SOPHIA ELIZABETH', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '4EE73652-6B7C-417D-8060-E75B761D16E6.jpeg', NULL, NULL, '2019-07-13 04:26:27'),
(305, 'Ayden', 'James', 'Stevenson', 'jodynlinnea@gmail.com', '$2y$10$VWhj6YbZyA4UiByR2Yykkuf70wh3w6/mBjGY9FXULs92CDV6bx10K', '2508963189', '898 Lakeside Pl', 'Langford', 'BC', 'V9B 4H7', 'Canada', '2007-01-19', 'male', 'Teen', NULL, 'STEVENSON, AYDEN JAMES', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'image-1.jpg', NULL, NULL, '2019-07-13 04:27:31'),
(306, 'Ayla', NULL, 'Amano', 'ayla.amano@gmail.com', '$2y$10$/EQ2Un7Nc4koSH6o2/dT/uJg40XScYEnoffGLcqou5AHZhziTQ2gi', '7787511716', '778 East 17th Avenue', 'Vancouver', 'BC', 'V5V 1B9', 'Canada', '1987-04-03', 'female', 'Adult', NULL, 'AMANO, AYLA', 'https://www.linkedin.com/feed/', NULL, NULL, '', '', NULL, 'No', 'No', 'Close-up-smiling.jpg', NULL, NULL, '2019-07-13 04:29:00'),
(307, 'Ayla', 'Patricia Anne ', 'Colley', 'desirewickenden@gmail.com', '$2y$10$iH4t1fKDx0Hfo1aDFO9seevVXmUWYXQqfJ2earGwP1k5OkTJXzE22', '6047687108', '#41 3942 Columbia valley rd ', 'Cultus lake ', 'BC', 'V2R 5B2', 'Canada', '2009-04-01', 'female', 'Child', NULL, 'COLLEY, AYLA PATRICIA ANNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '80E9CBF6-EB34-420A-9A2F-AB0B5BD09F58.jpeg', NULL, NULL, '2019-07-13 04:29:43'),
(308, 'Ayumu', 'Eric', 'Kojima ', 'kaoru1.kojima@gmail.com', '$2y$10$0H477zo98KYi09UoEjIeOuS6M0ICbpDLmeiRYGbtTEeWgjcLNj48G', '6043582152', '312-13th street, west', 'Vancouver', 'BC', '1v5 4b5', 'Canada', '2006-10-29', 'male', 'Child', NULL, 'KOJIMA , AYUMU ERIC', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '20181217_221710.jpg', NULL, NULL, '2019-07-13 04:30:31'),
(309, 'AYUMU', 'ERIC', 'KOJIMA', 'kaoru1.kojima@gmail.com', '$2y$10$BHnI7JwASpjSkRoR5dfEM.Pg.73tCarw.YAk4Z5/xBT/9cb0l2XRW', '7788398107', 'L312, 13th Street, West', 'North Vancouver', 'BC', 'V7M 1N8', 'Canada', '2006-10-29', 'male', 'Child', NULL, 'KOJIMA, AYUMU ERIC', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_4460.jpg', NULL, NULL, '2019-07-13 04:33:01'),
(310, 'Azusa', NULL, 'Ramos', 'azusa0902@hotmail.com', '$2y$10$hGTmfRin/usluGnXrxWSHegexNRyJTbJ6cxO9udM9j2Mv9Hi4MEti', '778-895-9489', '12029 211st', 'maple ridge', 'BC', 'v2x 8k6', 'Canada', '1980-09-02', 'female', 'Adult', NULL, 'RAMOS, AZUSA', NULL, 'azusa ramos', NULL, '', '', NULL, 'No', 'No', 'Capture.jpg', NULL, NULL, '2019-07-13 04:33:33'),
(311, 'Baffeh', 'Prince/Peter', 'Sheriff ', 'sheriff.bafeh@gmail.com', '$2y$10$bRlGJNyUnxdF/U3Mn3u1hennzP/Dbg0oG4VU54uBE5YtrWirugzb6', '2369880996', '#1105-8 smithe mews', 'Vancouver ', 'BC', 'V6B 0B5', 'Canada', '1995-05-09', 'male', 'Adult', '@vancityprince', 'SHERIFF , BAFFEH PRINCE/PETER', NULL, 'Bafeh sheriff ', NULL, '', '', NULL, 'No', 'No', '0CA011D4-4C0B-4B31-B4CF-237EAC3A44D7.jpeg', NULL, NULL, '2019-07-13 04:33:58'),
(312, 'Baffeh', 'Prince/Peter', 'Sheriff ', 'sheriff.bafeh@gmail.com', '$2y$10$ZNP/fqb0hv.iS6xtn7EvCeuRDIDLOR86VVILHl.6zvxbcucaEArTK', '2369880996', '#1105-8 smithe mews', 'Vancouver ', 'BC', 'V6B 0B5', 'Canada', '1995-05-09', 'male', 'Adult', '@vancityprince', 'SHERIFF , BAFFEH PRINCE/PETER', NULL, 'Bafeh sheriff ', NULL, '', '', NULL, 'No', 'No', '0CA011D4-4C0B-4B31-B4CF-237EAC3A44D7-1.jpeg', NULL, NULL, '2019-07-13 04:34:07'),
(313, 'Bahaneh ', NULL, 'Grewal', 'bahanehgrewal@gmail.com', '$2y$10$AaYeKqsvQYPdTPWXg7EZJ.CsZf91o92lnsmobvyqJzWWw6Ajz714W', '6047290727', '1-7198 Barnet Rd ', 'Burnaby ', 'BC', 'V5A 1C9 ', 'Canada', '1969-07-18', 'female', 'Adult', '@Bon.Bahar ', 'GREWAL, BAHANEH', NULL, NULL, 'http://www.agencyclick.com/BahanehG/resume', '', '', NULL, 'Yes', 'Yes', '3C6F5A38-AA72-4D79-B7F5-2BB9403F731F.jpeg', NULL, NULL, '2019-07-13 04:34:22'),
(314, 'Bahar', NULL, 'Babasadegh ', 'spring1870@yahoo.com', '$2y$10$jjC29Gxtv4XggFI.vvHM9OevaGfrZpp5.Y.xCew8bKi3YjzRC2rv2', '7789960494', '1104 -7351 halifax st', 'Burnaby ', 'BC', 'V5a4h2 ', 'Canada', '1979-04-08', 'female', 'Adult', NULL, 'BABASADEGH , BAHAR', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '1ABB6BC5-4033-47A0-8682-C6F9A625C867.jpeg', NULL, NULL, '2019-07-13 04:35:13'),
(315, 'Bahar', NULL, 'Babasadegh ', 'spring1870@yahoo.com', '$2y$10$MozTFxpzU5ttg6hZUdeRAup9NcSdeum1m7VoXKFZ0Okqi5l1gC.Vu', '7789960494', '1104-7351 Halifax st', 'Burnaby ', 'BC', 'V5a4h2 ', 'Canada', '1979-04-08', 'female', 'Adult', NULL, 'BABASADEGH , BAHAR', NULL, NULL, NULL, '', '', NULL, 'No', 'No', '4329ABC8-95D8-4703-9349-30BF138DE013.jpeg', NULL, NULL, '2019-07-13 04:36:26'),
(316, 'Bailey', NULL, 'Bourre', 'bbourre86@gmail.com', '$2y$10$Kdcxmt.twBwNjdTEAeXkCenY7KxezWfCuGMYrCUlpsHe65Nu/Zt8G', '2368873136', '5-20649 Edelweiss Drive', 'Agassiz', 'BC', 'V0M 1A1', 'Canada', '1986-08-24', 'female', 'Adult', NULL, 'BOURRE, BAILEY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'me.jpg', NULL, NULL, '2019-07-13 04:36:59'),
(317, 'Baldeep', NULL, 'Bahat', 'dbbahat@gmail.com', '$2y$10$D0seBMqC9JgN.kYrIzzMOOn1TM3jGl3x/5xtfuvyExWohFh7HSgTu', '7788652500', '8523 152st', 'Surrey ', 'BC', 'V3s3m8', 'Canada', '1986-01-20', 'male', 'Adult', NULL, 'BAHAT, BALDEEP', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(318, 'Barak', NULL, 'Idi', 'idibarak4@gmail.com', '$2y$10$Qq4Rxpz0gC.TXz4zbzuOYOrr1FF3ZW1VYt.3CsVvg./ylaE1X7j7a', '2368803705', '8139 145 street', 'Surrey', 'BC', 'V3S 9J6', 'Canada', '1994-03-13', 'male', 'Adult', 'www.instagram.com/officialafrobar', 'IDI, BARAK', NULL, 'https://www.facebook.com/afrobarmusic/', NULL, '', '', NULL, 'No', 'No', 'PSX_20190113_235822_1.jpg', NULL, NULL, '2019-07-13 04:37:16'),
(319, 'Barbara', NULL, 'Pearce', 'b.jpearce@hotmail.com', '$2y$10$W6jlxySJld1eXz7LqEkEwOWGmHP8Mtn.rUaZX5O9ekm5FtEuEVFdS', '778 228-1072', '#11-7883 Knight St', 'Vancouver', 'BC', 'V5P 2X5', 'Canada', '1957-12-21', 'female', 'Adult', NULL, 'PEARCE, BARBARA', NULL, 'https://www.facebook.com/profile.php?id=100009183627308', 'https://www.imdb.com/name/nm9376848/', '', '', NULL, 'Yes', 'Yes', 'me-6-6-18b.jpg', NULL, NULL, '2019-07-13 04:37:28'),
(320, 'Barbara', NULL, 'Renaud', 'brenaud@shaw.ca', '$2y$10$GVoYvpo8HrLXCTEFvAU4RunVb4SW2vOOE8RjDKNQRuUwJfOMWNyNS', '604-607-4796', '22910 Purdey Avenue', 'Maple Ridge', 'BC', 'V2X 7M1', 'Canada', '1963-04-20', 'female', 'Adult', NULL, 'RENAUD, BARBARA', NULL, 'https://www.facebook.com/barbara.renaud.18', 'https://www.imdb.com/name/nm2781842/', '', '', NULL, 'No', 'No', 'A77A0029-298A-4F4A-BA5C-7423C10EFE9D.png', NULL, NULL, '2019-07-13 04:38:33'),
(321, 'Bauer', 'Keenan', 'Pinsonneault', 'ml123456@gmail.com', '$2y$10$xQda52wJLsksa6ROJ.LUweYJxgt8RXCbv4wW94n0i6rXJU720je0.', '2508186688', '771 Canterbury road', 'Victoria', 'BC', 'V8X3E4', 'Canada', '2009-11-18', 'male', 'Child', NULL, 'PINSONNEAULT, BAUER KEENAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'MVIMG_20190321_1048432.jpg', NULL, NULL, '2019-07-13 04:38:51'),
(322, 'Becca', NULL, 'Burr', 'beccaburr0@gmail.com', '$2y$10$Zoi8apiXTIsuhLF7ouoC7uRz2xqqx.Vw8qCOHkNVSLEMkmAsBWHq2', '6047012588', '1688 parkwood drive ', 'Agassiz ', 'BC', 'V0m1a2', 'Canada', '1993-01-15', 'female', 'Adult', NULL, 'BURR, BECCA', NULL, 'Becca burr ', NULL, '', '', NULL, 'No', 'No', '5B9DA0A4-8A49-47AD-A6D0-0572B08B210D.jpeg', NULL, NULL, '2019-07-13 04:39:28'),
(323, 'Becky', 'I', 'Goebel', 'goebelbecky@gmail.com', '$2y$10$ts3Fa6MXTrElR4BKk06ES.WgYx48KvRIaJ//8EoyU.WIc1X0swkBi', '604-354-5016', '55 East 12th Ave. Unit 207', 'Vancouver', 'BC', 'V5T4J4', 'Canada', '1991-04-22', 'female', 'Adult', '@actuallyitsaxel', 'GOEBEL, BECKY I', 'https://www.linkedin.com/in/beckygoebel/', 'https://www.facebook.com/actuallyitsaxel', 'https://www.imdb.com/name/nm8613011/', '', '', NULL, 'No', 'No', 'IMG_2361.jpg', NULL, NULL, '2019-07-13 04:40:07'),
(324, 'Bella', 'Rose', 'McCairns', 'janetmccairns@telus.net', '$2y$10$eGkeMWOc1BRfkhs1WDzQreWt/6n9H5vK1gqP68oPkegDVvt61yTja', '604-315-5576', '208 - 3600 Windcrest Drive', 'North Vancouver', 'BC', 'V7G2S5', 'Canada', '2003-03-06', 'female', 'Teen', 'bella_rose_m', 'MCCAIRNS, BELLA ROSE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_3374.jpg', NULL, NULL, '2019-07-13 04:40:29'),
(325, 'Ben', NULL, 'Loyst', 'benjaminloyst@gmail.com', '$2y$10$SL3nK8ivq99RQsQLsS631.PaQv1lG0tdz9g.DxjL3ol4emmangcAW', '2506676711', '220 Machleary Street', 'Nanaimo', 'BC', 'V9R 2G6', 'Canada', '1999-10-11', 'male', 'Adult', NULL, 'LOYST, BEN', NULL, 'https://www.facebook.com/ben.loyst', NULL, '', '', NULL, 'No', 'No', '1-Ben-Loyst-8X10-2-1.jpg', NULL, NULL, '2019-07-13 04:41:03'),
(326, 'Ben', NULL, 'Loyst', 'benjaminloyst@gmail.com', '$2y$10$2z3H7I/s5BnFdxj9IxebXeFA/ZCrYyOXHGlE0B8nxvCqiUA2rz442', '2506676711', '220 Machleary Street', 'Nanaimo', 'BC', 'V9R 2G6', 'Canada', '1999-10-11', 'male', 'Adult', NULL, 'LOYST, BEN', NULL, 'https://www.facebook.com/ben.loyst', NULL, '', '', NULL, 'No', 'No', '1-Ben-Loyst-8X10-2-1-1.jpg', NULL, NULL, '2019-07-13 04:41:13'),
(327, 'Bernadette', 'Villalon', 'Ortilla', 'pinkyortilla@gmail.com', '$2y$10$umBUhh7v9HfzTxHSBPS4ouFZ.saltzj7l76OPl/0ZDNsau4EfAZiy', '6047936054', '690 Coquihalla street', 'Hope', 'BC', 'V0x 1L0', 'Canada', '2018-10-27', 'female', 'Adult', 'Pinkypot27', 'ORTILLA, BERNADETTE VILLALON', NULL, 'Pinky_102789@yahoo.com', NULL, '', '', NULL, 'No', 'No', '22FE6D32-6F10-4F72-9ECE-6ADE6605F943.jpeg', NULL, NULL, '2019-07-13 04:41:16'),
(328, 'Bernadette Anne Therese', NULL, 'Hall', 'annalice.hall@gmail.com', '$2y$10$bTGd6UCp2FBJsXdbkK1jBuFLMBUpOxRYmQ7OIwwj4Qic6LpXwd9wq', '7783196133', 'Suite 313 9344 Cameron St', 'Burnaby', 'BC', 'V3J1L9', 'Canada', '2000-11-22', 'female', 'Teen', NULL, 'HALL, BERNADETTE ANNE THERESE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', 'IMG_7892pscc.jpg', NULL, NULL, '2019-07-13 04:41:48'),
(329, 'Bernard', NULL, 'Gambier', 'gmb.bernard@gmail.com', '$2y$10$ra7IiOL9l3b2kEDI7AKeYuOhNAWhdxL9piPskHI/yXHW1j3/M8t1m', '6043639673', '439 Mclean dr', 'Vancouver', 'BC', 'V5l 3M5', 'Canada', '1993-06-01', 'male', 'Adult', 'Bernard_gmb', 'GAMBIER, BERNARD', NULL, 'Bernard Gambier', NULL, '', '', NULL, 'Yes', 'No', '5D42764E-E1C6-46BA-A35A-013A1D23DBC4.jpeg', NULL, NULL, '2019-07-13 04:42:25'),
(330, 'Bernarda', NULL, 'Antony', 'bernarda.antony@gmail.com', '$2y$10$1M3VNVvLAjO60AXNjdK.gO0tZP5sVGstVn/iqOPsi32/JVO20oQSK', '+1(604)401-2278', '3333 Commercial Drive Unit 106', 'Vancouver', 'BC', 'V5N4E5', 'Canada', '1991-01-22', 'female', 'Adult', NULL, 'ANTONY, BERNARDA', 'https://www.linkedin.com/in/bernardaantony/', 'https://www.facebook.com/bernarda.antony', NULL, '', '', NULL, 'No', 'No', 'IMG_2365.jpg', NULL, NULL, '2019-07-13 04:43:47'),
(331, 'Bethanea', 'Hsin-Yue', ' Chou', 'bethanea.chou6@gmail.com', '$2y$10$Cq1elCzqZDDREjehY7S79uAe5vFYM5y.UXuF.XDwFUT77xz4jPuMm', '6043521447', '2540 Hoskins Road', 'North Vancouver', 'BC', 'V7J3A3', 'Canada', '2002-03-10', 'female', 'Adult', '_bc.j', 'CHOU, BETHANEA HSIN-YUE', NULL, 'Bethanea Chou', NULL, '', '', NULL, 'No', 'No', 'PFP.jpg', NULL, NULL, '2019-07-13 04:44:38'),
(332, 'Bharat ', NULL, 'Arora', 'arorabharat144@gmail.com', '$2y$10$jKt385PvvZFzIeGaLCzB9OGlG3f0P7qMFwr50r76erWvng2StoL9u', '6044451597', '#206 - 4635 Imperial Street', 'Burnaby', 'BC', 'V5J 1B9', 'Canada', '1997-08-15', 'male', 'Adult', NULL, 'ARORA, BHARAT', 'linkedin.com/in/bharat-arora-3887a98a/', 'facebook.com/arorabharat144', NULL, '', '', NULL, 'Yes', 'No', 'IMG_20180425_031033_467.jpg', NULL, NULL, '2019-07-13 04:45:05'),
(333, 'Bill', NULL, 'Croft', 'billcroft69@hotmail.com', '$2y$10$8qLhQBt5QOP3iXFnxlcwG.jC2BY4Uno0OoPgIMHxNbQZju3lOxFym', '6047625768', '209-325 e 6th', 'vancouver', 'BC', 'V5T1J9', 'Canada', '1952-12-27', 'male', 'Adult', NULL, 'CROFT, BILL', NULL, NULL, 'http://www.imdb.com/name/nm0188457/?ref_=nv_sr_1', '', '', NULL, 'Yes', 'Yes', 'DSC_1320.jpg', NULL, NULL, '2019-07-13 04:45:28'),
(334, 'Bill', '-', 'shibicky', 'billshibicky@shaw.ca', '$2y$10$8qhfT33YOVbKgNXppbd15Osx1apl1EDw5rVOvFoJBLsg.xAcWUTBS', '6048504736', '1968 N. Parallel', 'abbotsford', 'BC', 'v2c 3e3', 'Canada', '1964-01-25', 'male', 'Adult', NULL, 'SHIBICKY, BILL -', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `performers_details`
--

CREATE TABLE `performers_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hairStyle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hairFacial` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bgPerformerDetails` text COLLATE utf8mb4_unicode_ci,
  `unionAffiliations` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unionNum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visa_student` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visa_international` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visa_open` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visa_film` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visa_film_other` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shirt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agentDetails` longtext COLLATE utf8mb4_unicode_ci,
  `castingTitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cdnCitizenship` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dayPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eveningPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailSecondary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `applicationFor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral1_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral1_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral2_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral2_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContact1_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContact1_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContact1_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContact2_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContact2_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContact2_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driversLicense` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leftHandDrive` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `neck` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chest` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insea` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resume` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bgPerformer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `availability` text COLLATE utf8mb4_unicode_ci,
  `cdnCitizen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bcResident` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taxes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `languageFirst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `languageSecond` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `performer_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `performers_details`
--

INSERT INTO `performers_details` (`id`, `hairStyle`, `hairFacial`, `bgPerformerDetails`, `unionAffiliations`, `unionNum`, `visa_student`, `visa_international`, `visa_open`, `visa_film`, `visa_film_other`, `shirt`, `agentDetails`, `castingTitle`, `job`, `cdnCitizenship`, `visa`, `dayPhone`, `eveningPhone`, `emailSecondary`, `applicationFor`, `referral1_name`, `referral1_phone`, `referral2_name`, `referral2_phone`, `emergencyContact1_name`, `emergencyContact1_phone`, `emergencyContact1_email`, `emergencyContact2_name`, `emergencyContact2_phone`, `emergencyContact2_email`, `driversLicense`, `leftHandDrive`, `neck`, `chest`, `insea`, `resume`, `bgPerformer`, `availability`, `cdnCitizen`, `bcResident`, `taxes`, `sin`, `languageFirst`, `languageSecond`, `performer_id`, `created_at`, `updated_at`) VALUES
(1, 'Long', 'No ', NULL, 'Full Union', 'BCGEU ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6043169338', '6043169338', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Keith  Anderson ', '6047984968', 'foanders219@hotmail.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Would love the opportunity to be involved in this and experience something fun !', 'Canadian Citizen', 'Yes', 'Yes', '944', 'English', NULL, 2, NULL, NULL),
(2, 'Military ', 'Short beard ( will shave for appropriate rolls)', 'The good doctor  ( continuity doctor in suit several episodes)\n \nColony   resistance fighter (12days) \n                 Grey hat soldier.   ( 10 days)\n\nRiverdale multiple episodes teacher, bobcat, \n                   and drive passerby.\n\nMan in the high castle. Multiple days \n                      Nazi scientist/ soldier\n\nProject blue book. multiple days passerby\n\nA million little things.  Multiple days\n                     Family guest. \n\n Multiple hallmark MOW. Passerby\n\n Travels.    Emergency patient.  Arrested by \n                     main charter. Sae stunt\n\n Timeless. Passerby\n\n Dirk Gently.  Multiple days Queens guard \n                        wendimoore ( featured)\n \n The flash. Policeman with mustache\n                    ( Featured )\n\n Supergirl.   Passerby\n\n Arrow.       Multiple days  Russian henchmen, \n                    passerby\n\n X- files.  Multiple days. Astronaut. \n                 ( photo shoot), passerby', 'Ubcp', 'Ex-04169', NULL, NULL, NULL, NULL, NULL, NULL, 'HellKat talent\nPh# 604-816-4528\nKatie@hellkattalent.ca', NULL, NULL, NULL, NULL, '7789892634', '6045760038', NULL, 'Adult', 'Katie bowdring', '6045760038', 'Rick Coltman', '6045339392', 'Katie bowdring', '6045760038', 'ktk@yahoo.ca', 'Rick Coltman', '6045339392', 'rickcoltman@hotmail.com', 'Yes', 'Yes', '16', '42', NULL, 'No', 'Yes', 'Monday to Saturday.  Sundays stunt training', 'Canadian Citizen', 'Yes', 'Yes', '5866', 'English', NULL, 3, NULL, NULL),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kids ages 6-19 in Victoria,BC', NULL, NULL, NULL, '2505896482', '2505896482', 'ryannegirard@hotmail.ca', 'Teen', NULL, NULL, NULL, NULL, 'Ryanne Girard', '2505896482', 'ryannegirard@hotmail.ca', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'I believe I am free for the whole two weeks of the shot (July 1st-15th )', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 4, NULL, NULL),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kids ages 6-19 in Victoria,BC', NULL, NULL, NULL, '2505896482', '2505896482', 'ryannegirard@hotmail.ca', 'Teen', NULL, NULL, NULL, NULL, 'Ryanne Girard', '2505896482', 'ryannegirard@hotmail.ca', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'I believe I am free for the whole two weeks of the shot (July 1st-15th )', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 5, NULL, NULL),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '6048326701', '6048326701', NULL, 'Child', NULL, NULL, NULL, NULL, 'Jackie louis', '6046150067', 'ja_louis@yahoo.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 6, NULL, NULL),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '2508881285', '2508881285', NULL, 'Child', 'Gillian Croft of spotlight academy', NULL, NULL, NULL, 'Karishma sethi', '2508881285', 'karishma.sethi@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'July 1-10 and August 1-31', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Hindi', 7, NULL, NULL),
(7, 'Zero fade Ceaser', 'Thin chin strap', 'Broken Badges , The Wishing Tree , J. K. Rowling story', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Extra', NULL, NULL, NULL, '6049448567', '2508994720', 'aaroncallihoo@yahoo.com', 'Adult', 'Charlene Callihoo the Good Dr', '6047905356', 'Jake Callihoo , Twilight Zone ', '6042304429', 'Donna Callihoo', '7783887428', 'dccal1@hotmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'At the Moment my schedule is completley open as i have taken time off from Physically active work to focas on healing from knee ingury ,while looking into finding the right Barber School for me . I have been cutting mens hair from my home for almost 5 years and i would love to make it my carreer .So this would b nice and easy on my knee + i love the people and the sceen , ive always had fun being in the back ground .', 'Canadian Citizen', 'Yes', 'Yes', '4937', 'English', 'English', 8, NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '6045851976', '6047602535', NULL, 'Child', 'Terumi Takahashi', NULL, 'Terumi Takahashi', NULL, 'Terumi Takahashi', '6047602535', 'terucanada@hotmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Fluently speak Japanese\nAvailable to write Japanese \nGood sing \n', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', NULL, 9, NULL, NULL),
(9, NULL, NULL, NULL, 'Full Union', '1271', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6047990084', '6047990084', 'aarongelowitz@shaw.ca', 'Adult', NULL, NULL, NULL, NULL, 'Sharon Goldthorp', '6043167315', 'britshaw@shaw.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'march 25-29 7am-7pm', 'Canadian Citizen', 'Yes', 'Yes', '8298', 'ENGLISH', NULL, 10, NULL, NULL),
(10, 'Short clean cut', 'Clean shaven ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kamlooops', NULL, NULL, NULL, '2509455218', '2509455218', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Adam guenther', '17786827487', 'adam_guenther@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Friday-sunday', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 11, NULL, NULL),
(11, NULL, NULL, 'IBABC advert', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk in Agassiz, BC', NULL, NULL, NULL, '6043169925', '6043169925', 'abbiemaslin399@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Freddie Maslin', '6043168970', 'freddiemaslin399@hotmail.com', 'Dean Cradock', '6047956865', NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Evenings ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 12, NULL, NULL),
(12, 'Short', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '6048386511', '6044356511', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Martin Kobayakawa', '6044356511', 'mkobayakawa@shaw.ca', NULL, NULL, NULL, 'Yes', 'Yes', '14.5', NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'Yes', '7886', 'English', 'Japanese', 13, NULL, NULL),
(13, NULL, NULL, 'Netflix Lost in Space S2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7782312556', '6045522556', NULL, 'Teen', NULL, NULL, NULL, NULL, 'Tina LAMB', '7782312556', 'rtlamb@shaw.ca', 'Rick Lamb', '7782312813', 'rtlamb@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Weekdays and weekends', 'Canadian Citizen', 'Yes', 'Yes', '2233', 'English ', NULL, 14, NULL, NULL),
(14, NULL, 'No', 'Netflix Lost in Space', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ' Boys & Girls Ages 12-16 Years Old Posting', NULL, NULL, NULL, '7782312556', '6045522556', 'tinalamb2813@gmail.com', 'Teen', NULL, NULL, NULL, NULL, 'Tina Lamb-mom', '7782312556', 'rtlamb@shaw.ca', 'Rick Lamb-dad', '7782312813', 'spuzzum71@gmail.com', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Available any day/time', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'None', 15, NULL, NULL),
(15, 'short', 'goatee', 'I have been working as a background worker for nine years. A few of the shows I have worked on are: Prison Break, Falling Skies, Godzilla, Flash, Arrow, Commercials, Psyche, Once Upon a Time, Elysium, and many more. ', 'Apprentice Union', '26150', NULL, NULL, NULL, NULL, NULL, NULL, 'Dallas Talent, number phone 6048360222', 'African- Canadian, Caribbean- Canadian and Mixed Race Males & Females Ages 17-65', NULL, NULL, NULL, '7788751616', '7788751616', 'yvewane7991@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Gina Wane', '6048741614', 'hdutot@hotmail.com', 'Gina Wane', '7788771616', 'hdutot@hotmail.com', 'Yes', 'Yes', '15.5', '36.5', NULL, 'Yes', 'Yes', 'Monday to Sunday ', 'Canadian Citizen', 'Yes', 'Yes', '750446072', 'French', 'English', 16, NULL, NULL),
(16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '604-506-7070', '604-506-7070', NULL, 'Child', 'Garry Dersksen Reel kids ', '604-465-8144', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'No', NULL, 'English', NULL, 17, NULL, NULL),
(17, 'Buzz', 'Clean or Scruff', 'This was in 2009.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-358-2994', '604-358-2994', NULL, 'Adult', 'David Gauthier', '778-952-5905', 'Andy Suganda', '778-892-5080', 'Sam Fedyk', '403-869-8783', 'samfedyk@shaw.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'Yes', 'Currently: any day, any time.', 'Canadian Citizen', 'Yes', 'Yes', '4163', 'English', 'French', 18, NULL, NULL),
(18, 'Surf', '5oclock', NULL, 'Iatse loc 891 carpenters', 'Iatse loc 891', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6049383696', '6049383596', NULL, 'Adult', 'Mike bouchard', '6045546611', 'Tom clifford', '6043179467', 'Kelvie barker', '6043170835', 'kelvieb@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 19, NULL, NULL),
(19, NULL, 'Stubble', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IN HOPE, BC: Small Town People with Lots of Character Ages 35 to 60', NULL, NULL, NULL, '6045183900', '6045183900', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Melanie Skolovy-Brodie', '7788781776', 'melanieanns@live.com', 'Bob Brodie', '6043088412', NULL, 'No', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '6326', 'English', 'N/A', 20, NULL, NULL),
(20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7783447053', '7783447053', NULL, 'Adult', 'Julie wilson', '+1 (604) 649-6960', 'Katie ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '4808', 'English', NULL, 21, NULL, NULL),
(21, 'Short style, Military, police style', 'upon request', 'Most of the TV shows and Films shot in the lower mainland since 2010 to 2017. http://www.agencyclick.com/adamboughanmi', 'Full Union', '60479', NULL, NULL, NULL, NULL, NULL, NULL, 'Locol Color :604-685-0315', 'Background acting, SAE, Stand-in, Acting.', NULL, NULL, NULL, '6044401085', '6044401085', 'boughanmi@gmail.com', 'Adult', 'Local Color', '604-685-0315', NULL, NULL, '911', '911', '911@911.com', NULL, NULL, NULL, 'Yes', 'Yes', '15.5-16', 'M,42', NULL, 'Yes', 'Yes', 'Weekend Mostly from Friday 6PM to Sunday 11PM and, anytime on special projects only.', 'Canadian Citizen', 'Yes', 'Yes', '8531', 'French, Arabic', 'English', 22, NULL, NULL),
(22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese man extra', NULL, NULL, NULL, '6048375740', '6048375740', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Gerry Nishi', '6048399320', 'adamnishi93@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', 'Weekdays after 5:00pm\nWeekends any time', 'Canadian Citizen', 'Yes', 'Yes', '9330', 'English', NULL, 23, NULL, NULL),
(23, 'Curly ', 'Sruff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Anything ', NULL, NULL, NULL, '6044013690', '6044013690', 'adamghomari@gmail.com', 'Adult', 'Sara', '7789960494', 'Sara\'s talent partner ', '7783207241', 'Tamara Ghomari', '6042216029', 'tghomari@yahoo.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Currently always available', 'Canadian Citizen', 'Yes', 'Yes', '6263', 'English ', 'French ', 24, NULL, NULL),
(24, NULL, NULL, NULL, 'Full Union', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kamloops extra', NULL, NULL, NULL, '2505722326', '2505722326', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Greg', '2508191369', 'adamharris_15@hotmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'I work 4 on 4 off. I could do 8 Days of the extra roles during Kamloops filming ', 'Canadian Citizen', 'Yes', 'Yes', '2880', 'English', NULL, 25, NULL, NULL),
(25, 'Braids/pulled back', NULL, 'Xmas bells are ringing, the last bridesmaid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '2892009903', '2892009903', 'M.adam.brown87@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Alicia Brown ', '9057064683', 'alicia.brown16@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'As needed ', 'Canadian Citizen', 'Yes', 'Yes', '5472', 'English ', NULL, 26, NULL, NULL),
(26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '6047981458', '6047981458', 'brentsawa@hotmail.com', 'Child', NULL, NULL, NULL, NULL, 'Meghan Sawatzky', '6047017140', 'omeghan@hotmail.com', 'Pauline Sawatzky', '6047935304', 'paulsawa@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 27, NULL, NULL),
(27, 'Wavy', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Xs', NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '2508186688', '2508186688', 'gccmandy@gmail.com', 'Child', NULL, NULL, NULL, NULL, 'Mandy lee', '2508186688', 'ml123456@gmail.com', 'Kevin Pinsonneault', '7786768720', 'rppempire@yahoo.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'July 1-15', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', 'None', 28, NULL, NULL),
(28, 'Braids or Shoulder Length Straight Hair', NULL, 'I appeared in the audience on ABC\'s The View as well as GMA Day', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Black Extras Ages 6-65', NULL, NULL, NULL, '6043381341', '6043381341', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Fadeke Mustapha', '19252069185', 'fadekemustapha@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'I\'m available on weekends all day, and weekdays after 6. For specific/urgent shoots I can take vacation days. ', 'Canadian Citizen', 'Yes', 'Yes', '9148', 'English', 'N/A ', 29, NULL, NULL),
(29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Townsfolk in Agassiz', NULL, NULL, NULL, '6049975940', '6049975940', 'Adreanmacdonald@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Jonni Skinner', '7788621595', 'jonnette66@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'Yes', '8998', 'English', NULL, 30, NULL, NULL),
(30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8199939350', '8199939350', NULL, 'Adult', 'Suzannah Raudaschl', '7783503452', 'Tony Scott', '8198499411', 'Suzannah Raudaschl', '7783503452', 'suzannahraudaschl@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '9497', 'English', NULL, 31, NULL, NULL),
(31, 'Long waves ', 'Eyebrows black ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'All secondary /non-speaking /speaking  and ', NULL, NULL, NULL, '7787235135', '7787235135', NULL, 'Adult', 'Blue Mancuma', NULL, 'New Image Alumni graduate /James Michael Hanson ', '6047674131', 'Tabitha Herrington ', '6048345349', 'jhansone2@gmail.com', 'James Hanson ', '6047674131', 'lostnsoundz@gmail.com', 'Yes', 'No', NULL, '32', NULL, 'No', 'Yes', '21-jump street \nLook who\'s talking two \nStand in for the omen Little girl main character.... \nflexible as to most things that can give two weeks notice exempt holidays. Otherwise in general willing to be flexible. ', 'Canadian Citizen', 'Yes', 'Yes', '8867', 'English', 'Italian ', 32, NULL, NULL),
(32, 'curly hair (2c-3a)', NULL, NULL, NULL, NULL, 'Yes', 'Yes', 'Yes', 'Yes', NULL, NULL, NULL, 'African- Canadian, Caribbean- Canadian and Mixed Race Males & Females Ages 17-65', NULL, NULL, NULL, '2369987807', '2369987807', 'adriana.seibt@gmail.com', 'Adult', 'Sebastian Hill-Esbrand ', '7787515714', NULL, NULL, 'Sebastian Hill-Esbrand', '7787515714', 's.hill-esbrand@outlook.com', NULL, NULL, NULL, 'No', 'No', '13.7 inches', NULL, NULL, 'Yes', 'No', 'I\'m available between April 23rd and May 2nd, and then May 6th to May 20th (upon request), and June 3rd-6th (upon request)', 'Visa', 'Yes', 'Yes', NULL, 'Spanish', 'English ', 33, NULL, NULL),
(33, 'Curly ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sarah Davis- Premier Talent \n(604) 687-4909', 'No', NULL, NULL, NULL, '7789685402', '7789685402', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Justine Lilgert', '(778) 987-6406', 'jlilgert@gmail.com', 'Mary Gritter ', '4032493590', NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Working full time with flexible employer', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 34, NULL, NULL),
(34, 'Long ', 'No ', 'Descendants 1 , wonder , zoo, ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Lloyd Talent ', NULL, NULL, NULL, NULL, '6044541019', '7782311295', NULL, 'Adult', 'Cindy Akers ', '7789888364', 'Allen white ', '(604) 522-7410', 'Ana Caldwell ', '7788854859', 'caldwell.ana@gmail.com', 'Brian Caldwell', '6044541019', 'bricaldwell@gmail.com', 'Yes', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Anytime! ', 'Canadian Citizen', 'Yes', 'Yes', '7332', 'English ', 'Spanish ', 35, NULL, NULL),
(35, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', 'Yes', 'No', NULL, NULL, NULL, 'I\'m currently working to In-Motion and BCF casting', NULL, NULL, NULL, '7788375655', '7788375655', 'esteves.adriano@hotmail.com', 'Adult', 'Tynan', '+1 250-891-2101', 'Marcos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Visa', 'Yes', 'Yes', '9062', 'Portuguese', 'English', 36, NULL, NULL),
(36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-782-8443', '604-541-1456', 'abattley@shaw.ca', 'Adult', 'Candace & Doug  Field', '604-736-3869 ', 'Brent Battley', '604-541-1456', 'Brent or Louise Battley', '604-541-1456', 'RADBATPAD@SHAW.CA', 'Ainslie Mills', '604-882-0820', 'oriana@telus.net', 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '1315', 'English', 'French', 37, NULL, NULL),
(37, 'straight/slight wave', 'none/as required', 'Wonder, Legion, Hit The Road, The Predator, Deadpool2, The Man In The High Castle (two episodes) Legends Of Tomorrow.', 'UBCP/ACTRA background /extra member', '4322', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-782-8443', '604-541-1456', 'abattley@shaw.ca', 'Adult', 'Candace & Doug  Field', '604-736-3869 ', 'Brent Battley', '604-541-1456', 'Brent or Louise Battley', '604-541-1456', 'RADBATPAD@SHAW.CA', 'Ainslie Mills', '604-882-0820', 'oriana@telus.net', 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'May, June, July, August 2018', 'Canadian Citizen', 'Yes', 'Yes', '1315', 'English', 'French', 38, NULL, NULL),
(38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-782-8443', '604-541-1456', 'abattley@shaw.ca', 'Adult', 'Doug and Candice Field', '604-736-3869', 'Brent Battley', '604-541-1456', 'Brent or Louise Battley (parents)', '604-541-1456', 'RADBATPAD@SHAW.CA', 'Ainslie Mills (aunt)', '604-882-0820', NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '1315', 'English', 'French', 39, NULL, NULL),
(39, 'Straight/ slight wave', 'no', 'Wonder,  Legion,  Hit The Road,  The Predator,  Legends Of Tomorrow,  Deadpool 2,  \nThe Man In The High Castle (two episodes)', 'UBCP', '4322', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-782-8443', '604-541-1456', 'abattley@shaw.ca', 'Adult', 'Doug and Candice Field', '604-736-3869', 'Brent Battley', '604-541-1456', 'Brent or Louise Battley (parents)', '604-541-1456', 'RADBATPAD@SHAW.CA', 'Ainslie Mills (Aunt)', '604-882-0820', 'oriana@telus.net', 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'April, May, June, July, August  2018', 'Canadian Citizen', 'Yes', 'Yes', '1315', 'English', 'French', 40, NULL, NULL),
(40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '7786780210', '7786780210', NULL, 'Child', NULL, NULL, NULL, NULL, 'Courtney Hopkins', '778-678-0210', 'hoppyfam@yahoo.ca', 'Phillip Hopkins', '778-678-0212', NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Available all of July', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 41, NULL, NULL),
(41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk in Agassiz, BC', NULL, NULL, NULL, '2502087582', '2502087582', NULL, 'Adult', 'Chris Vautour', '2502131189', 'Kyla Norsewothy', '6048807344', 'Rosa Quintana', '604-796-9822', 'rquin@shaw.ca', 'Marina Avros', '1780-404-961', NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '1535', 'english', 'none/french', 42, NULL, NULL),
(42, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese cast', NULL, NULL, NULL, '604-230-0415', '604-230-0415', NULL, 'Child', NULL, NULL, NULL, NULL, 'Kazuki Kobayashi', '604-230-0415', 'peach_honey@hotmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Kyokushi Karate for Bule Belt\nKendo\nDance', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Japanese', 43, NULL, NULL),
(43, 'Very short ', 'Nothing ', NULL, NULL, NULL, 'No', 'No', 'No', 'Yes', NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '7782227021', '7782227021', NULL, 'Adult', NULL, NULL, NULL, NULL, 'SELC VANCOUVER ', '6046399075', 'admin@selccareercollege.ca', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'Yes', 'No', 'Any time any day is no problem.\n', 'Visa', 'No', 'No', '3193', 'Japanese ', 'English ', 44, NULL, NULL),
(44, 'Short', 'No', 'Arrow, Salvation, Reaper, Tomorrow People, Birth of the Dragon, Backstrom, Fringe, Stargate Universe, Star Trek (Too many to list all as I have worked on a lot.  I believe Sandra casted a lot of these too) \n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sassy Talent \nSerena Renna â€“ Principal Agent\n\nPO Box 16045\n617 Belmont Street\nNew Westminster, BC V3M 6W6\nPhone: (604) 785-6690\n\n___________________\nShowbiz Management\n120A â€“ 3989 Henning Drive\nBurnaby, BC V5C 6N5\nCanada\n\ninfo@showbizmanagement.com\n\nTel: 604.435.7469 ', NULL, NULL, NULL, NULL, '6043158628', '6043158628', NULL, 'Adult', 'Sassy Talent (Current Principal Agent Serena)', '604-785-6690', 'Showbiz Management (Current Background Agent Roland)', '604-435-7469 ', 'Maureen Yu', '6044282750', 'nabntm@gmail.com', NULL, NULL, NULL, 'Yes', 'No', '14', NULL, NULL, 'Yes', 'Yes', 'Flexible based on what happens, due to how last minute all film stuff is usually, such as audition calls or if booked for some kind of work.', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Chinese (Cantonese)', 46, NULL, NULL),
(45, 'Short', 'Very short mustache and goatee, willing to shave', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-615-6027', '604-615-6027', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Wesley Jones', '604 845 0392', 'wesleyalanjones@gmail.com', 'Nathan Jones', '604 798 7212', 'bluehaven_21@yahoo.ca', 'Yes', 'Yes', '16', '42', NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'French', 47, NULL, NULL),
(46, NULL, 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '2503181772', '2503181772', 'ahomer@msn.com', 'Adult', NULL, NULL, NULL, NULL, 'Gary Homer', '2503679897', 'alhomer56@gmail.com', 'Elfriede Homer', '2503679897', NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'I work Monday to Thursday 7:30-530. Depending on how long you need me for. I can and would take holidays. ', 'Canadian Citizen', 'Yes', 'Yes', '887', 'English ', NULL, 48, NULL, NULL),
(47, 'Two tone ', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6043785267', '60437852674', 'sookiebooboosausage@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Dave wells ', '6042067434', 'Sookiebooboosausage@gmail.com', 'Greg wells ', '6047919497', 'sookiebooboosausage@gmail.com', 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Avaliable any day any time ', 'Canadian Citizen', 'Yes', 'Yes', '877', 'English', 'English', 49, NULL, NULL),
(48, 'Wavy', NULL, 'Cheats (2002), Smailville (2007-2009), Human Target (2010)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-897-7973', '604-897-7973', NULL, 'Adult', 'Natalie Cosco', '604-618-6005', 'Sandra Freeman', 'sandra@freemancasting.com', 'Natalie Cosco', '604-618-6005', 'natalie_cosco@me.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', NULL, 'Canadian Citizen', 'Yes', 'No', '241', 'English', NULL, 50, NULL, NULL),
(49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7788225168', '7788225168', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Kelly', '6044364669', 'kellyliu5636@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'No', NULL, 'English ', 'Mandarin', 51, NULL, NULL),
(50, 'short', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Any background role will suffice.', NULL, NULL, NULL, '7785492384', '7785492384', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Young May', '7785496167', 'okyacm@yahoo.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '6539', 'English', 'NA', 52, NULL, NULL),
(51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Youth medium', NULL, 'Background for Republic of Sarah', NULL, NULL, NULL, '6046142025', '6046142025', NULL, 'Child', NULL, NULL, NULL, NULL, 'Al Klassen', '6045561219', 'al_klassen@shaw.ca', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Available for background for Republic of Sarah over spring break', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 53, NULL, NULL),
(52, NULL, NULL, 'loudermilk, twilight zone ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'background/extra', NULL, NULL, NULL, '6047263428', '6047263428', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Frank', '6047265452', 'ale.gl_08@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Available Fri, sat, sun anytime\nmon after 5pm\nTues after 1pm\nwed after 4pm\nThurs after 1pm\n', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'english', 'spanish', 54, NULL, NULL),
(53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Agassiz townfolk', NULL, NULL, NULL, '6048974646', '6048976633', NULL, 'Teen', NULL, NULL, NULL, NULL, 'Sarah Bell', '6048974646', 'amjpd5@gmail.com', 'Thomas Bell', '6048976633', NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Available the whole week of March 25th', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 55, NULL, NULL),
(54, 'Straight', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Age 4-14 in Agassiz, BC', NULL, NULL, NULL, '778-240-2776', '778-240-2776', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Aaron Ross', '778-240-8771', 'aaron@stemcellofamerica.com', 'Carly Fedyshen', '778-847-0977', 'carlyri@yahoo.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Available anytime', 'Canadian Citizen', 'Yes', 'Yes', '1528', 'English', 'N/A', 56, NULL, NULL),
(55, 'Straight', NULL, 'Hellcats, Level Up, Lucifer, The 100, The Possession, The Company You Keep, The Secret Circle,  Once Upon A Time, Hector and the Search for Happiness ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No - I would just like to be considered for upcoming roles', NULL, NULL, NULL, '7788668169', '7788668199', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Niall Lennon', '6048288347', 'niall.lennon2@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'I am usually only available on weekends because I work full-time Monday to Friday; however, I can be free some weekdays here and there. ', 'Canadian Citizen', 'Yes', 'Yes', '5212', 'English', 'N/A', 57, NULL, NULL),
(56, NULL, NULL, 'Pace Musical Theatre Academy - 2019 Spring performance', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CTM International\nSandra Webster-Worthy\nswworthy@gmail.com', 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '7784309685', '7784309685', 'karen.winfield@gmail.com', 'Teen', 'Sandy Webster-Worthy', '250-920-8343', NULL, NULL, 'Karen Winfield', '4036199685', 'karen.winfield@gmail.com', 'Andrew Kortas', '250-888-6676', 'kortas@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Available for both weeks', 'Canadian Citizen', 'Yes', 'Yes', '1109', 'English', NULL, 58, NULL, NULL),
(57, 'standard (a bit shorter on sides , parted at side)', 'none', 'Sabrina, RIverdale, Charmed, Man in the High Castle, Series of Unfortunate Events, A Million Little Things, Dream On, The Flash, Unspeakable, Sacred Lies, The Exorcist, Twilight Zone, iZombie, The Detour, Good Boys, Cosmos, Descendants 3, The Art of Racing in the Rain, Descendants 3, Possibility, Critters, 2 Hearts, Small Town Christmas,You Me Her', 'UBCP Extra', 'EX-05101 ', NULL, NULL, NULL, NULL, NULL, NULL, 'Picobello Talent ', 'Boys & Girls Ages 12-16 Years Old', NULL, NULL, NULL, '6047868607', '6047868607', NULL, 'Adult', 'Picobello Talent Agency', '7787715860', NULL, NULL, 'LindsayRoss', '7789844612', 'lross@sd42.ca', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability', 'Canadian Citizen', 'Yes', 'Yes', '5810', 'English', NULL, 59, NULL, NULL),
(58, NULL, NULL, 'Twilight Zone, Skyscraper, Noelle, MOW\'s, Terror, Salvation, Man In the High Castle, Supergirl, Riverdale', 'UBCP Extra', 'EX04235', NULL, NULL, NULL, NULL, NULL, NULL, 'InMotion', 'Open casting for asian model', NULL, NULL, NULL, '7789997756', '7789997756', 'alexsunlee@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Carla Lee', '7789999744', 'carlamay@shaw.ca', 'Eleanor Chan', '6049370593', 'lechan@shaw.ca', 'Yes', 'Yes', '16', NULL, NULL, 'Yes', 'Yes', NULL, 'Canadian Citizen', 'Yes', 'Yes', '2865', 'English', NULL, 60, NULL, NULL),
(59, NULL, NULL, NULL, 'Apprentice Union', '28742', NULL, NULL, NULL, NULL, NULL, NULL, 'Maureen Goodwill    MoGood Talent  1-778-838-9444 \nMaureen@mogoodtalent.com\n', 'Boys Ages 8 to 11 Years old on the Island', NULL, NULL, NULL, '250-743-1033', '250-743-1033', 'killamirwin@yahoo.com', 'Child', 'Maureen at MoGood Talent', '1-778-838-9444', 'Irwin Killam', '12507100010', 'Irwin Killam', '12507100010', 'irwin@baypub.ca', 'Chris Killam', '250-715-8733', 'chris@baypub.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 61, NULL, NULL),
(60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Military Type Men & Women ages 18-50 in Kamloops, BC Area', NULL, NULL, NULL, '250-486-0795', '250-486-0795', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Haley Macintyre', '250-486-8544', 'haleymacintyre@gmail.com', 'Nick Gammer', '250-496-5507', 'ngammer@shaw.ca', 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Completely available.', 'Canadian Citizen', 'Yes', 'Yes', '5594', 'English', NULL, 62, NULL, NULL),
(61, 'Long and curly ', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-07 00:00:00', NULL, 'No', NULL, NULL, NULL, '2505747663', '2505747663', NULL, 'Child', NULL, NULL, NULL, NULL, 'Susy Bell', '2505747663', 'thesnooz@hotmail.com', 'Kyle Bell', '2505742087', NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Any time ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English ', 'English ', 63, NULL, NULL),
(62, 'Long ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Girls 12-14', NULL, 'Friendly townsfolk in aggasis', NULL, NULL, NULL, '6047938908', '6047938908', NULL, 'Child', NULL, NULL, NULL, NULL, 'Kara hawley', '6047938908', 'hollykara81@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 64, NULL, NULL),
(63, 'Short', NULL, 'Coyote science ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Members of the LGBTQ Community', NULL, NULL, NULL, '7783192645', '7783192645', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Tyson', '2508148959', 'energyrepublic@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Flexible ', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', NULL, 65, NULL, NULL),
(64, 'Straight with bangs', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Girls 7x', NULL, 'Victoria casting call for kids 6-19', NULL, NULL, NULL, '2503601821', '2503601821', 'bailey.ja@gmail.com', 'Child', NULL, NULL, NULL, NULL, 'Jen Kyle', '2508181996', 'bailey.ja@gmail.com', 'Bryan Kyle', '2505148545', 'bryan.kyle@gmail.con', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Available most of summer vacation and around school around hours during school year.', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English ', NULL, 66, NULL, NULL),
(65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7786846009', '7786846009', 'Alicia.chappell86@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Joe Charlie', '7786845044', 'Jcharlie1307@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Whenever needed ', 'Canadian Citizen', 'Yes', 'Yes', '7656', 'English ', NULL, 67, NULL, NULL),
(66, NULL, 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2508989423', '2508989423', NULL, 'Adult', 'Jennifer', '2504658299', 'Mel ', '2503381323', 'Kieran', '2507025881', 'kierancjones@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'Yes', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '5527', 'English', NULL, 68, NULL, NULL),
(67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'boss management inc.\nLaura Cooper \n6048422949\n', 'background ', NULL, NULL, NULL, '6042402426', '6042402426', NULL, 'Adult', NULL, NULL, NULL, NULL, 'teresa gil felts', '6047240045', 'teresagil7@shaw.ca', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'open ', 'Canadian Citizen', 'Yes', 'Yes', '4965', 'English', NULL, 69, NULL, NULL),
(68, 'Curly bob', NULL, 'Many Fo not remember all the names but a few I remember are : Godzilla 2013 version, Nickelodeon Spilting David, Fairl Odd Summer And Fast Layne ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Black Extras Ages 6-65', NULL, NULL, NULL, '2368884790', '2368884790', NULL, 'Teen', NULL, NULL, NULL, NULL, 'Amanda boszak', '2368884790', 'xaajboss@icloud.com', 'Javan holmes', '7789601143', 'javanh@hotmail.co.uk', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Available all days and hours that are transit friendly', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 70, NULL, NULL),
(69, NULL, 'Can grow full beard', 'Please refer to resume. \n', 'Apprentice Union', '28911', NULL, NULL, NULL, NULL, NULL, NULL, 'Showbiz Management', NULL, NULL, NULL, NULL, '7788783418', '7788783418', 'luna.allan@hotmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Susan Pinter', '7788377755', 'lordsantini@hotmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'Yes', 'Open Availability via Showbiz Management', 'Canadian Citizen', 'Yes', 'Yes', '814', 'Spanish', 'English', 71, NULL, NULL),
(70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-838-6525', '604-838-6525', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Lynn Devereaux', '613-277-6333', 'lynn.devereaux@gmail.com', 'Aislyn Devereaux', '902-293-0233', 'ace.devereaux@gmail.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '1848', 'English', NULL, 72, NULL, NULL),
(71, 'Classic', 'none', 'Since 1994 - Continuity UNI-COPS and DETECTIVES: PSYCH The TV SHOW, MOTIVE, ARCTIC AIR\n\nMost recently:  INK - Forensic Photographer Crime Scene \nA Million Little Things - Car Saleswoman\nLegends of Tomorrow - Continuity TIME AGENT\n\n', 'Full Union', '1423', NULL, NULL, NULL, NULL, NULL, NULL, 'Please text, phone or email me directly for EXTRA WORK, DOUBLING or STAND IN\nFor Principal Actor Theatrical, Commercial or Print - danny@bossmanagement.com', 'Continuity Extra or Day Call Utility Stand in', NULL, NULL, NULL, '6045124472', '6045124472', 'warrenalley@gmail.com', 'Adult', 'UBCP ACTRA (since 1994)', '604-689-0727', 'UBCP ACTRA ( SAG since 1997', '604-689-0727', 'Iris Pidduck', '778-239-7623', 'ipidduck@shaw.ca', 'bruce pidduck', '604-551-8100', 'westpac@shaw.ca', 'Yes', 'Yes', '14', '36', NULL, 'Yes', 'Yes', 'Please check with Agency Click and ACTRA online for current up to date avails.\nGenerally I am available 7 days/nights a week. ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'French', 73, NULL, NULL),
(72, 'Classic', 'none', 'I have been a member of UBCP since 1994, it would be impossible to list all the bg I have done over the years. I usually get type cast as a FBI/CIA, unicop or detective or military as I have a working knowledge of fireamrs and  my firearms safety certification and special skills driving at high speeds and maneuvers.  Falling Skies: Militia firing 1/4 loads from machine gun, Arctic Air Unicop (high speed driving stopping on set marks). I am also an accomplished Equestrian and have worked as a Special Skills extra  on camera horse handler in Flicka 2. Often playing a range of characters from Everygirl cocktail waitress to boardroom executive, and aristocracy.', 'SAG-AFTRA, ACTRA', 'UBCP #1423 / ACTRA 01-04354', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6045124472', '6045124472', 'AllyWarren@me.com', 'Adult', 'Keith Martin Gordey, President UBCP', '604-689-0727', 'Ellie Harvey, Executive Liaison UBCP Stand-in Committee', '604-689-0727', 'Iris Pidduck', '778-239-7623', 'ipidduck@shaw.ca', 'Carolyn Lupton', '604-551-8422', 'Clupton@hotmail.com', 'Yes', 'Yes', '14', '36', NULL, 'Yes', 'Yes', 'I keep up avails on agencyclick.  I am currently daycalling stand-in and looking to job share or show call if its a good fit. I love working fraterdays! Will consider bg if a \"specific\" in very small groups (no cattle calls), law enforcement especially if special skills. I drive automatic and standard, trucks and farm equipment. ', 'Canadian Citizen', 'Yes', 'Yes', '6573', 'English', 'French', 74, NULL, NULL),
(73, 'Classic', 'none', 'too many to name; on and off since 1994', 'Full Union', '1423', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'stand in (or bg)', NULL, NULL, NULL, '6045124472', '6045124472', 'alleywarren@hotmail.com', 'Adult', 'Allison Warren', '6045124472', NULL, NULL, 'iris pidducj', '778=239-7623', 'ipidduck@shaw.ca', 'bruce pidduck', '604-551-8100', 'westpac@shaw.ca', 'Yes', 'Yes', '14', '36', NULL, 'Yes', 'Yes', 'Firearms, special skills driving, accomplished equestrian and horse wrangler.', 'Canadian Citizen', 'Yes', 'Yes', '6573', 'English', 'French', 75, NULL, NULL),
(74, NULL, NULL, 'Hallmark, Disney, ect.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CTM International \n250 9208343', NULL, NULL, NULL, NULL, '7786790552', '7786790552', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Gail Escoval', '2507271364', 'gescoval1@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full Availability', 'Canadian Citizen', 'Yes', 'Yes', '7232', 'English', 'Portuguese', 76, NULL, NULL),
(75, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7782556591', '7782556591', NULL, 'Adult', 'Lori Lozinski', '604-506-0167', 'Ryan Ermacora', '604-368-2906', 'Maximilian Hill', '778-321-0603', 'maxjameshill@gmail.com', 'Nicki Seriani', '604-839-4969', 'nickiseriani@gmail.com', 'Yes', 'No', NULL, NULL, NULL, 'Yes', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 77, NULL, NULL),
(76, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Asian Males & Females Ages 6-65 (No Dyed Hair)', NULL, NULL, NULL, '7787880800', '7787880800', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Foh Sing Yong', '6047192973', 'frederickyong@yahoo.ca', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 78, NULL, NULL),
(77, 'long straight', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', NULL, 'Agassiz townfolk', NULL, NULL, NULL, '6043166469', '6043166469', NULL, 'Child', 'Willa Balfour', '604-616-7465', 'Michael Bendner', '778-889-7959', 'Dustin Neufeld', '6043166469', 'dustinmarks@hotmail.com', 'Carmen Neufeld', '6046151144', 'cneufel4@hotmail.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Monday  to Sunday', 'Canadian Citizen', 'Yes', 'Yes', '4296', 'English', NULL, 79, NULL, NULL),
(78, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Talentbid', NULL, NULL, NULL, NULL, '778 708 6983', '778 708 6983', NULL, 'Adult', 'Bahar (Oh boy productions)', '778 996 0494', 'Martina Stancekova', '604 375 2894', 'Martina Stancekova', '604 375 2894', 'martina.stancekova@gmail.com', 'Nuru Mahamoued ', '604 767 8483', NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '3929', 'English', 'Amharic', 80, NULL, NULL),
(79, NULL, NULL, 'Nickelodeon\'s movie \"Blurt\"', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Hollywood North Extras  2nd Floor, 5050 Kingsway Burnaby, BC V5H 4H2  P: 604-466-3045   info@hollywoodnorthextras.com\n', 'Background Acting', NULL, NULL, NULL, '604-999-1897', '604-999-1897', NULL, 'Adult', 'James Forsyth Casting (BCF Casting)', 'www.jamesforsythcasting.com (casting@bcfcasting.com)', NULL, NULL, 'Carolyn Early', '604-839-4426', 'carolynearly@yahoo.ca', 'Sienna Early', '604-341-1412', 'sienna.early@gmail.com', 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Currently Tuesday\'s and Friday\'s', 'Canadian Citizen', 'Yes', 'Yes', '6126', 'English', NULL, 81, NULL, NULL),
(80, NULL, 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly townsfolk', NULL, NULL, NULL, '6048190708', '6048190708', 'amandaklop1@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Alex klop', '6048454964', 'alexanderklop@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 82, NULL, NULL),
(81, 'Long straight ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk in Agassiz,BC', NULL, NULL, NULL, '+1 (306) 485-1470', '+1 (306) 485-1470', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Taylor japp', '+1 (306) 485-1470', 'tmjapp@gmail.com', 'James', '6048456004', 'jlacseul@gmail.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'Yes', '4923', 'English', NULL, 83, NULL, NULL),
(82, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6043165778', '6043165778', NULL, 'Adult', 'Chelsey Folk ', '(604) 819-7871', 'Kim scott', '+1 (604) 897-7712', 'Teddy Tooke', '6047984099', 'a_manda.a@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 84, NULL, NULL),
(83, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '7786844882', '7786844882', 'eyes_life@hotmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Jennifer Mallard', '7786842404', 'Jennifer.leann19@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '7436', 'English', NULL, 85, NULL, NULL),
(84, NULL, NULL, 'I have worked in theatre a show called â€œmedicineâ€ when I was teenager', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Towns folk for Agassiz BC', NULL, NULL, NULL, '6048699994', '6048605808', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Carol peters', '6048603734', 'cpeters507@gmail.com', 'Tom Hendrickson ', '6048603406', NULL, 'Yes', 'Yes', '13?', NULL, NULL, 'No', 'Yes', 'I do have a job but am willing to work around it.', 'Canadian Citizen', 'Yes', 'Yes', '4889', 'English', NULL, 86, NULL, NULL),
(85, NULL, NULL, 'Nickelodeon\'s movie \"Blurt\", Netflix\'s To All the Boys I\'ve Loved Before', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'HollywoodNorth Extras 604-466-3045 hollywoodnorthextras.com', NULL, NULL, NULL, NULL, '604-999-1897', '604-999-1897', 'amandaearly2000@yahoo.ca', 'Adult', NULL, NULL, NULL, NULL, 'Carolyn Early', '604-839-4426', 'carolynearly@yahoo.ca', 'Sienna Early', '604-341-1412', 'sienna.early@gmail.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Currently I am available Monday-Friday', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 87, NULL, NULL),
(86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '519-282-1343', '519-282-1343', NULL, 'Adult', 'Karen Bergen ', '778-288-6444', 'John LeRoy', '604-351-5868', 'Larry Jones ', '519-282-7132', 'lajones@quadro.net', 'Angela Jones ', '519-317-6069', 'lajones@quadro.net', 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '2974', 'English ', 'Can speak basic french', 88, NULL, NULL),
(87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7782235410', '7782235410', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Joe Bidgood', '7783199022', 'joe@fallingonion.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, '32', NULL, 'No', 'No', 'Nearly always avaiable', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'French', 'English', 89, NULL, NULL),
(88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '2505883778', '2505883778', NULL, 'Child', NULL, NULL, NULL, NULL, 'Meredith ', NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English ', NULL, 90, NULL, NULL),
(89, 'long hair ', 'mustache and beard', NULL, NULL, NULL, 'Yes', 'Yes', 'No', 'No', 'masters in administrative sciences', NULL, NULL, NULL, NULL, NULL, NULL, '604-613-6768', '604-613-6768', 'gurisran.16@gmail.com', 'Adult', 'Gurjaipal Singh', '778-242-9191', 'Tanveer Bhatia', '778-861-9497', 'Rajinder  Chhina', '778-683-7727', 'rajinderchhina2025@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Visa', 'Yes', 'No', NULL, 'punjabi', 'english', 91, NULL, NULL),
(90, 'bob', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '16048081629', '16048081629', 'ambergoatmonkey@hotmail.com', 'Adult', 'Carrie Brownstein', 'n/a', ' SEARCH + DESTROY', 'n/a', 'Joshua Wells', '16048082392', 'radiowells@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'N/A', 92, NULL, NULL),
(91, 'Straight', NULL, 'Star Trek Discovery, Suits, Handmaid\'s Tale, Designated Survivor, Molly\'s Game, Man Seeking Woman, The Romanoffs, Salvation, Condor, The Girlfriend Experience, Conviction, Taken, Incorporated, Issues, Stockholm, La Boda de Valentina, Nutcracker, Volvo commercial, Force Point commercial, Monster commercial, Busweisser commercial, BB&T commercial, Verizon commercial, Rogers commercial, TD Bank commercial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no- but Sunshine Coast based', NULL, NULL, NULL, '7789771110', '7789771110', 'amberpetersen@ymail.com', 'Adult', 'Charlie DeBouvoise', '9732203927', 'Madeleine Claude', '9173731799', 'Ray Miron', '6048806951', 'ray.l.miron@gmail.com', 'Deborah Allen', '4039333680', 'debloom@telus.net', 'Yes', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Very flexible schedule, but looking for Sunshine Coast specific productions.', 'Canadian Citizen', 'Yes', 'Yes', '1134', 'English', 'French', 93, NULL, NULL),
(92, 'Pony tail/ bun ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6049911511', '6049978095', NULL, 'Adult', NULL, NULL, NULL, NULL, 'David neeve', '6049978095', 'dneeve88@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English ', NULL, 94, NULL, NULL);
INSERT INTO `performers_details` (`id`, `hairStyle`, `hairFacial`, `bgPerformerDetails`, `unionAffiliations`, `unionNum`, `visa_student`, `visa_international`, `visa_open`, `visa_film`, `visa_film_other`, `shirt`, `agentDetails`, `castingTitle`, `job`, `cdnCitizenship`, `visa`, `dayPhone`, `eveningPhone`, `emailSecondary`, `applicationFor`, `referral1_name`, `referral1_phone`, `referral2_name`, `referral2_phone`, `emergencyContact1_name`, `emergencyContact1_phone`, `emergencyContact1_email`, `emergencyContact2_name`, `emergencyContact2_phone`, `emergencyContact2_email`, `driversLicense`, `leftHandDrive`, `neck`, `chest`, `insea`, `resume`, `bgPerformer`, `availability`, `cdnCitizen`, `bcResident`, `taxes`, `sin`, `languageFirst`, `languageSecond`, `performer_id`, `created_at`, `updated_at`) VALUES
(93, NULL, NULL, 'To Be Fat Like Me (tv movie)\nSorority Wars (tv movie)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2507449230', '2507449230', 'amberlote@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Steve Dawson', '2508183963', 'stevehobdawson@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Mondays\nWednesdays\nFridays\nSundays', 'Canadian Citizen', 'Yes', 'Yes', '7777', 'English', NULL, 95, NULL, NULL),
(94, 'High and tight', 'Clean', 'Minority Report, The Magicians, Colony, Altered Carbon, Hit The Road.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Daniel Mathias @ DCMathias Management 778 689 8481', 'Stand-in, background/extra, open to all', NULL, NULL, NULL, '604 440 4518', '604 440 4518', NULL, 'Adult', 'Daniel Mathias ', '778 689 8481', NULL, NULL, 'Susanne Schonthaler', '778 840 5874', 'thalersus@hotmail.com', NULL, NULL, NULL, 'No', 'No', '14.5 in', '39 in', NULL, 'Yes', 'Yes', 'Unavailable April 4th and 5th', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', NULL, 96, NULL, NULL),
(95, 'Long', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kathy Carpenter, \nKC Talent Agency \n1-604-377-6366 (cell) or 1-604-734-0003', 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '(250) 472-1585 please don\'t leave message', '(250) 858-2789 or (250) 472-1585', NULL, 'Teen', 'Jacqui Kaese (Spotlight Academy)', '1-250-714-2555', 'Brent Lanyon', '1-250-727-1969', 'Gillian Croft/ Andrew Croft', '(250) 858-2789', 'gilliancroft@shaw.ca', 'Maddy Lamarche', '(250) 891-3183', 'madiola@hotmail.ca', 'No', 'No', NULL, NULL, NULL, 'Yes', 'No', '\nAvailable for most days and times', 'Canadian Citizen', 'Yes', 'Yes', '8480', 'English', 'Learning French', 97, NULL, NULL),
(96, 'wavy', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'size child 10 ', NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '6047814262', '6047814262', NULL, 'Child', NULL, NULL, NULL, NULL, 'Andy French', '778-677-0951', 'afrencho@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '421', 'English', 'French', 98, NULL, NULL),
(97, 'Straight ', NULL, 'Godzilla, Cabin in the Woods, Manny\'s Best Friend, Jinxed, Escape From Mr.Lemoncello\'s Library, Xfinity CM, Amazon Audible CM, American Express CM, Blue Cross CM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '6045205827', '6045205827', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Eric Sokugawa', '6042182397', 'esokugawa@gmail.com', NULL, NULL, NULL, 'Yes', 'No', '13 1/4\"', '28\"', NULL, 'Yes', 'Yes', NULL, 'Canadian Citizen', 'Yes', 'Yes', '7208', 'English', 'Japanese/French', 99, NULL, NULL),
(98, 'Afro (versatile)', NULL, 'Anne with an E, Frankie Drake', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tyman Stewart - The Characters Talent Agency\ntyman@thecharacters.com', NULL, NULL, NULL, NULL, '2896874882', '2896874882', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Kevin O\'Connor', '5195801742', 'koconnor798@gmail.com', 'Audia Anderson', '2896860225', 'audiaa.anderson@gmail.com', 'Yes', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Working full time as a hostess, flexible hours', 'Canadian Citizen', 'Yes', 'Yes', '3215', 'Patois/English', 'French', 100, NULL, NULL),
(99, 'Straight long', NULL, NULL, NULL, NULL, 'Yes', 'Yes', 'Yes', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7787141912', '7787141912', NULL, 'Adult', 'Katrina McCarthy ', '(604)-7220405', 'Alana King ', '(778) 872-3554', 'Katrina McCarthy', '6047220405', 'katrinamc@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Visa', 'Yes', 'Yes', '9780', 'Spanish', 'English', 101, NULL, NULL),
(100, 'Straight/wavy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'BG Extra', NULL, NULL, NULL, '7785226296', '7785226296', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Victor Veiga', '778-681-4332', 'victorandrea2016@gmail.com', 'Maria Mayo', '1-780-441-9783', 'maryjorgemayo@yahoo.com', 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '2658', 'Spanish', 'English', 102, NULL, NULL),
(101, 'Long straight hair', 'n/a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '7788924342', '7788924342', NULL, 'Adult', '(Referred by) Sandra-Ken Freeman', '6042222543', NULL, NULL, 'Reggie Cummings', '604 299 4342', 'acumm@shaw.ca', 'Chelsea Rembisz', '7788982893', 'cfrembisz@gmail.com', 'Yes', 'No', NULL, NULL, NULL, 'Yes', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'N/A', 103, NULL, NULL),
(102, NULL, NULL, 'Years ago I did from 2000 to 2013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '604679934', '6046799340', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Colleen Punt', '6049800742', 'cpunt2002@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Anytime right now', 'Canadian Citizen', 'Yes', 'Yes', '7925', 'English', 'N/a', 104, NULL, NULL),
(103, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '9028801124', '9028801124', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Wanda', '9022211005', 'wandajm@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '3511', 'English', NULL, 105, NULL, NULL),
(104, 'curly', 'none', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Black Background Actors', NULL, NULL, NULL, '7788613504', '7788613405', 'andrea1023@live.com', 'Adult', NULL, NULL, NULL, NULL, 'Farida Oren', '6047677443', 'andrea1023@live.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'Yes', 'No', 'open availability', 'Canadian Citizen', 'Yes', 'Yes', '9649', 'English', NULL, 106, NULL, NULL),
(105, 'awesome', 'shadow/clean shaven/short beard', 'I have worked on most of the shows in Vancouver area in past 3 years', 'UBCP', 'AM28464', NULL, NULL, NULL, NULL, NULL, NULL, 'Local Color. 6046850315', NULL, NULL, NULL, NULL, '12508868629', '12508868629', 'akrestanwfg@gmail.com', 'Adult', 'Local Color Agency', '6046850315', NULL, NULL, 'Pavol Krestan', '12508851238', 'pkrestanwfg@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', '15.5', '38', NULL, 'No', 'Yes', 'I am available 24/7', 'Canadian Citizen', 'Yes', 'Yes', '8380', 'Slovak and Czech', 'English, French, Polish ', 107, NULL, NULL),
(106, 'awesome', 'shadow/clean shaven/short beard', 'I have worked on most of the shows in Vancouver area in past 3 years', 'UBCP', 'AM28464', NULL, NULL, NULL, NULL, NULL, NULL, 'Local Color. 6046850315', NULL, NULL, NULL, NULL, '12508868629', '12508868629', 'akrestanwfg@gmail.com', 'Adult', 'Local Color Agency', '6046850315', NULL, NULL, 'Pavol Krestan', '12508851238', 'pkrestanwfg@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', '15.5', '38', NULL, 'No', 'Yes', 'I am available 24/7', 'Canadian Citizen', 'Yes', 'Yes', '8380', 'Slovak and Czech', 'English, French, Polish ', 108, NULL, NULL),
(107, 'awesome', 'shadow/clean shaven/short beard', 'I have worked on most of the shows in Vancouver area in past 3 years', 'UBCP', 'AM28464', NULL, NULL, NULL, NULL, NULL, NULL, 'Local Color. 6046850315', NULL, NULL, NULL, NULL, '12508868629', '12508868629', 'akrestanwfg@gmail.com', 'Adult', 'Local Color Agency', '6046850315', NULL, NULL, 'Pavol Krestan', '12508851238', 'pkrestanwfg@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', '15.5', '38', NULL, 'No', 'Yes', 'I am available 24/7', 'Canadian Citizen', 'Yes', 'Yes', '8380', 'Slovak and Czech', 'English, French, Polish ', 109, NULL, NULL),
(108, 'awesome :)', 'shadow/clean shaven/short beard', 'I have worked on most of the shows in Vancouver area in past 3 years', 'UBCP', 'AM28464', NULL, NULL, NULL, NULL, NULL, NULL, 'Local Color. 6046850315', NULL, NULL, NULL, NULL, '12508868629', '12508868629', 'akrestanwfg@gmail.com', 'Adult', 'Local Color Agency', '6046850315', NULL, NULL, 'Pavol Krestan', '12508851238', 'pkrestanwfg@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', '15.5', '38', NULL, 'No', 'Yes', 'I am available 24/7', 'Canadian Citizen', 'Yes', 'Yes', '8380', 'Slovak and Czech', 'English, French, Polish ', 110, NULL, NULL),
(109, 'awesome :)', 'shadow/clean shaven/short beard', 'I have worked on most of the shows in Vancouver area in past 3 years', 'UBCP', 'AM28464', NULL, NULL, NULL, NULL, NULL, NULL, 'Local Color. 6046850315', NULL, NULL, NULL, NULL, '12508868629', '12508868629', 'akrestanwfg@gmail.com', 'Adult', 'Local Color Agency', '6046850315', NULL, NULL, 'Pavol Krestan', '12508851238', 'pkrestanwfg@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', '15.5', '38', NULL, 'No', 'Yes', 'I am available 24/7', 'Canadian Citizen', 'Yes', 'Yes', '8380', 'Slovak and Czech', 'English, French, Polish ', 111, NULL, NULL),
(110, 'Straight ', 'Beard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7789967025', '7789967025', NULL, 'Adult', 'Holly LeRoy', '6044412449', 'Kathryn Knott', '7782391389', 'Peter Mykichuk', '2046336540', 'petermykichuk@hotmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '9700', 'English', 'Basic French', 112, NULL, NULL),
(111, NULL, NULL, 'Legends of Tomorrow, A series of unfortunate events, Supernatural, Charmed, Valley of the Boom, Riverdale, Sabrina', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Boss management', NULL, NULL, NULL, NULL, '15147734022', '5147734022', NULL, 'Adult', 'Asia Domisiewicz', '6047106323', 'Patrick Bourque', '5064613405', 'Catherine Bourque', '5064591874', 'catherinebourque@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', '15.5', '44', NULL, 'Yes', 'Yes', 'Available at request.', 'Canadian Citizen', 'Yes', 'Yes', '2850', 'English', 'French', 113, NULL, NULL),
(112, NULL, 'Beard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Extra', NULL, NULL, NULL, '6047890814', '6047890814', NULL, 'Adult', 'Jeff Terrell', '6043121334', NULL, NULL, 'Harry Derbitsky', '6049438720', 'Der_123456@yahoo.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '1929', 'English', NULL, 114, NULL, NULL),
(113, NULL, NULL, NULL, NULL, NULL, 'No', 'No', 'Yes', 'No', 'I have an open work permit given to me while I wait for my permanent residency.', NULL, NULL, NULL, NULL, NULL, NULL, '236 986 8565', '236 986 8565', 'pandapilch@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Jacqueline Pilcher', '236 668 1045', 'jgaby97@shaw.ca', 'Suzanne Gaby', '250 589 5963', 'sgaby@contextresearch.ca', 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', 'I am available Monday to Friday and Sunday at any time.', 'Visa', 'Yes', 'Yes', '8557', 'English', NULL, 115, NULL, NULL),
(114, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6047005216', '6047005216', 'andrew-wang@outlook.com', 'Adult', NULL, NULL, NULL, NULL, 'Echo Wang', '7788659312', 'echo74wang@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Monday weds friday sat sun all day\n\nTues thurs before 5pm', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Mandarin', 116, NULL, NULL),
(115, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6047005216', '6047005216', 'andrew-wang@outlook.com', 'Adult', NULL, NULL, NULL, NULL, 'Echo Wang', '7788659312', 'echo74wang@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Monday weds friday sat sun all day\n\nTues thurs before 5pm', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Mandarin', 117, NULL, NULL),
(116, NULL, 'Mustache', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Military Type Men & Women ages 18-50 in Kamloops, BC Area * decide on a picture I look young for my age*', NULL, NULL, NULL, '2505710138', '2505710138', 'asjorgn@telus.net', 'Adult', NULL, NULL, NULL, NULL, 'Sandi Jorgensen', '250-571-0127', 'lexi_01@telus.net', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Available as needed during this casting call (July 15-29th)', 'Canadian Citizen', 'Yes', 'Yes', '1648', 'English', NULL, 118, NULL, NULL),
(117, 'Shortish spiking ', 'Sometimes ', 'My resume is on www.agencyclick.com/andrewbeha', 'UBCP AM ', '28381', NULL, NULL, NULL, NULL, NULL, NULL, 'Iâ€™m currently with Picobello but leaving due to no work over the last few months ', NULL, NULL, NULL, NULL, '7024454649', '7024454649', NULL, 'Adult', 'Edith Beha', '2145322632', 'Konrad Beha', '2145322632', 'Aleisha Fazekas', '6046008822', 'aleishafazekas@gmail.com', 'Edith Beha ', '2145322632', 'edith54beha@hotmail.com', 'Yes', 'No', '16â€', NULL, NULL, 'No', 'Yes', 'Iâ€™m available everyday ', 'Canadian Citizen', 'Yes', 'Yes', '1453', 'English ', 'German ', 119, NULL, NULL),
(118, 'Styled back ', 'Sometimes ', 'Riverdale,Arrow,unreal lots of shows over the last 3 1/2 years ', 'Apprentice Union', '28381', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Any roles ', NULL, NULL, NULL, '7024454649', '7024454649', NULL, 'Adult', 'LA hilts ', NULL, 'Lisa Ratki', NULL, 'Konrad Beha', '2145322632', 'edith54beha@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', '16.5', NULL, NULL, 'Yes', 'Yes', 'Iâ€™m available everyday ', 'Canadian Citizen', 'Yes', 'Yes', '1453', 'English', 'German ', 120, NULL, NULL),
(119, 'Comb over', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Extra', NULL, NULL, NULL, '6046006850', '6046006850', 'andyvu92@gmail.com', 'Adult', 'Bahar', '7789960494', 'Simona', '778320724', 'Xuong Phung', '6046537145', 'andyvu92@gmail.com', 'Hue Vu', '7786684918', 'andyvu92@gmail.com', 'No', 'No', 'n/a', 'n/a', NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '772', 'English', 'Vietnamese', 121, NULL, NULL),
(120, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, '6043458256', '6043458256', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Daniel Choe', '7787733863', 'danielchoe91@hotmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', '5 days a week including weekend', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'Korean', NULL, 122, NULL, NULL),
(121, 'Bangs', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7789945552', '7789945552', 'aca08angela@vfs.com', 'Adult', 'Daylen Luchsinger', '6049033797', 'Carrie Brownstein', '0', 'Glen Ibbott', '6043688843', 'glen.ibbott@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'Yes', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 123, NULL, NULL),
(122, 'Long straight', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Inmotion Talent Ltd. 604 336 5220', 'stand-in, double, ', NULL, NULL, NULL, '778 384 8466', '778 384 8466', NULL, 'Adult', NULL, NULL, NULL, NULL, 'shawn clare', '778 829 8464', 'shawndavidclare@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, '35', NULL, 'Yes', 'Yes', NULL, 'Canadian Citizen', 'Yes', 'Yes', '4141', 'English', 'Japanese', 124, NULL, NULL),
(123, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Agassiz Town Folk', NULL, NULL, NULL, '604-785-2878', '604-785-2878', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Dave Dolomont', '604-785-2787', 'ADolomont@shaw.ca', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'All day everyday', 'Canadian Citizen', 'Yes', 'Yes', '7131', 'English', NULL, 125, NULL, NULL),
(124, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Agassiz Town Folk', NULL, NULL, NULL, '604-785-2878', '604-785-2878', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Dave Dolomont', '604-785-2787', 'ADolomont@shaw.ca', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'All day everyday', 'Canadian Citizen', 'Yes', 'Yes', '7131', 'English', NULL, 126, NULL, NULL),
(125, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Agassiz Town Folk', NULL, NULL, NULL, '604-785-2878', '604-785-2878', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Dave Dolomont', '604-785-2787', 'ADolomont@shaw.ca', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'All day everyday', 'Canadian Citizen', 'Yes', 'Yes', '7131', 'English', NULL, 127, NULL, NULL),
(126, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Agassiz Town Folk', NULL, NULL, NULL, '604-785-2878', '604-785-2878', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Dave Dolomont', '604-785-2787', 'ADolomont@shaw.ca', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'All day everyday', 'Canadian Citizen', 'Yes', 'Yes', '7131', 'English', NULL, 128, NULL, NULL),
(127, 'curly ', NULL, '90210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'background extra', NULL, NULL, NULL, '4388700213', '4388700213', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Luidmila', '7787726722', 'milachizh03@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'full availability ', 'Canadian Citizen', 'Yes', 'Yes', '7576', 'English', 'Russian', 129, NULL, NULL),
(128, NULL, 'None', 'Too many to list', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Show Biz', NULL, NULL, NULL, NULL, '6046182236', '6046182236', 'agnieszka1970@gmail.com', 'Adult', 'Renata Slowinski ', '6045180148', 'Tad Nazar', '6045120274', 'Gerry Owens', '6047266404', 'gogerry09@gmail.con', NULL, NULL, NULL, 'Yes', 'Yes', '13', '36', NULL, 'No', 'Yes', 'I have a flexible schedule\n', 'Canadian Citizen', 'Yes', 'Yes', '2976', 'Polish', 'English', 130, NULL, NULL),
(129, 'Long, straight', 'No', 'The Hulk\nBury Your Heart At Wounded Knee', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Extras', NULL, NULL, NULL, '604.704.7622', '604.704.7622', NULL, 'Adult', 'Nick Brooks', '13433634983', 'Matthew Murray', '16472426493', 'Conrad Matejczuk', '6044462445', 'conradblack3000@gmail.com', 'Gwen Flavel', '13063511506', 'gwen.zohner@accesscomm.ca', 'Yes', 'No', '12\'', '28', NULL, 'Yes', 'Yes', 'Full Time\nMon-Sun', 'Canadian Citizen', 'Yes', 'Yes', '6329', 'English', 'N/A', 131, NULL, NULL),
(130, 'straight', 'Right', 'The Hulk, Bury My Heart At Wounded Knee', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No, I am open to all that would apply ', NULL, NULL, NULL, '604.704.7622', '604.7622', NULL, 'Adult', 'Mathew Murray', '6472426493', 'Hayley Toane', '6476181241', 'Conrad Matejczuk', '6044462445', 'conradblack3000@gmail.com', 'Gwen Flavel', '3063511506', 'zohnerg22@gmail.com', 'Yes', 'Yes', '12\'', '28', NULL, 'Yes', 'Yes', 'Open\nMon-Sun AM-PM-AM', 'Canadian Citizen', 'Yes', 'Yes', '329', 'English ', 'No second language ', 132, NULL, NULL),
(131, 'Short', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '6047997374', '6047997374', NULL, 'Teen', NULL, NULL, NULL, NULL, 'Nichole Sanderson', '6047994209', 'nicholesanderson.5@gmail.com', 'Karen Hudson ', '6047993566', 'shiningstar_572@hotmail.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'March 25-29', 'Canadian Citizen', 'Yes', 'No', NULL, 'English ', NULL, 133, NULL, NULL),
(132, 'Brown', 'No', 'See above resume for this year ', 'UBCP', '4042', NULL, NULL, NULL, NULL, NULL, NULL, 'Dallas talent\n6044154783', NULL, NULL, NULL, NULL, '6044424256', '6044424256', NULL, 'Adult', 'Keith dallas', '6044154783', 'Vince tsang', '6048689789', 'Christine stokes', '6046035543', 'gcstokes@gmail.com', 'Eric jordan ', '6043679095', 'eric.jordan@vpd.ca', 'Yes', 'No', NULL, NULL, NULL, 'Yes', 'Yes', '2019-07-24 00:00:00', 'Canadian Citizen', 'Yes', 'Yes', '3827', 'English ', NULL, 134, NULL, NULL),
(133, 'Curly', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '6043162893', '6043162893', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Ron Johnstone ', '6043168000', 'ronjohnstone78@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, '36', NULL, 'No', 'No', 'I am available for all of the friendly folks from Agassiz dates in late March. ', 'Canadian Citizen', 'Yes', 'Yes', '736', 'English ', NULL, 135, NULL, NULL),
(134, NULL, NULL, '8 years ago,I was an extra in a detergent company back in the Philippines. ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '7788472316', '7788472316', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Archile Mortel', '7788469540', 'archilemortel@hotmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Monday, Tuesday and Friday including weekends. ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'Tagalog', 'English', 136, NULL, NULL),
(135, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly towns folk Agassiz ', NULL, NULL, NULL, '6047024644', '6047024644', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Jesse wheeler', '6043169647', 'jesse.annamarie@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English ', NULL, 137, NULL, NULL),
(136, 'straight', 'none', 'The Interview\nCollosus\nDARC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604 754 7987', '604 754 7987', NULL, 'Adult', 'Miranda Kostash', NULL, NULL, NULL, 'Terry McColl', '604 868 6995', 'tmccoll@gmail.com', 'Jason McColl', '604 805 8690', 'jasonlmccoll@gmail.com', 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Most days and evenings', 'Canadian Citizen', 'Yes', 'Yes', '4875', 'English', 'Cantonese', 138, NULL, NULL),
(137, 'Wavy', 'N/A', '***please refer to resume for full list and details\nWedding in the Vineyard\nSummer in the Vineyard\nA Fathers Nightmare\nI\'m Not A Bad Person\nUndying\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Danny Abraham (featured Roles) - 778-888-7593\nBoss Management (Paul Cousins) - Background 604-488-1444', 'Military Type Men & Women ages 18-50 in Kamloops, BC Area', NULL, NULL, NULL, '2504939446', '2504939446', 'aterebka@hotmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Ryan Fulcher', '2506891569', 'aterebka@hotmail.com', 'Gloria Fulcher', '2504953452', NULL, 'Yes', 'Yes', '14\"', 'cross chest: 14.5\"', NULL, 'Yes', 'Yes', 'I may not be available on July 15th IF i book a commercial that I recently got a call back for.  Other than that, I am available.', 'Canadian Citizen', 'Yes', 'Yes', '6267', 'English', 'Polish', 139, NULL, NULL),
(138, 'Straight', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '2507271364', '2503910607', 'gescoval@shaw.ca', 'Teen', 'Sandy Webster-Worthy', '250 920-8343', NULL, NULL, 'Gail Escoval', '2507271364', 'gescoval1@gmail.com', 'Americo Escoval', '250-727=1362', 'aescoval@tld.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Open all summer 2019\navailable during school year in local area', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 140, NULL, NULL),
(139, 'med hair, full and straight', 'none ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IN SQUAMISH,BC: Small Town People with Lots of Character Ages 35 to 60', NULL, NULL, NULL, '604 306 8920', '604 306 8920', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Michael Roblin', '604-848-9328', 'mroblin@dynamicpm.ca', NULL, NULL, NULL, 'Yes', 'No', '13.5\"', '42\"', NULL, 'Yes', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Plants', 141, NULL, NULL),
(140, NULL, NULL, 'Hot Tub Time Machine, Ramona and Beezus, Psych', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '7789287325', '7789287325', 'anneikeda@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Ken Lin', '6047257325', 'klin1022@gmail.com', 'Henry Ikeda', '604-597-1312', NULL, 'Yes', 'Yes', '12.5', '32', NULL, 'No', 'Yes', 'Any day. I also submitted my son\'s application (Maxton Lin) so if you could schedule us together that would be great. ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Japanese', 142, NULL, NULL),
(141, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '604-316-8884', '604-316-8884', 'amvictor@gmail.com', 'Teen', NULL, NULL, NULL, NULL, 'Melanie Victor', '6043168884', 'amvictor@gmail.com', 'Andrew Victor', '604-316-3784', 'amvictor@gmail.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Halq\'emeylem', 143, NULL, NULL),
(142, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Boys & Girls Ages 12-16 Years Old', NULL, NULL, NULL, '6043163900', '6043163900', 'xxxkanata@yahoo.com', 'Adult', NULL, NULL, NULL, NULL, 'Ella Monro MD', '250 438 0917', 'emonro@gmail.com', 'Carolyn Schillimore', '604 7995379', 'carekipp@live.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '9520', 'english', 'n/a', 144, NULL, NULL),
(143, 'Straight ', 'No', 'Timmy Failure  (Airborne )', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-10 00:00:00', NULL, 'No', NULL, NULL, NULL, '7782986453', '6044401174', NULL, 'Child', NULL, NULL, NULL, NULL, 'Viviana ', '6044401174', 'viviana.gehring@hotmail.com', 'Michael ', '6047861378', 'redswiss@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', NULL, 'Canadian Citizen', 'Yes', 'No', '8363', 'English', 'None', 145, NULL, NULL),
(144, 'Straight', NULL, 'Lucifer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Extra', NULL, NULL, NULL, '2503055122', '2503055122', NULL, 'Adult', 'Coco ', '6048052390', 'Ana Nunes', '7783164157', 'Armin', '6043582339', 'arminfrischknecht@hotmail.com', 'Sean ', '6042306285', 'sean.gabriel@sjrb.ca', 'Yes', 'Yes', '13', NULL, NULL, 'No', 'Yes', 'When required ', 'Canadian Citizen', 'Yes', 'Yes', '85', 'English ', NULL, 146, NULL, NULL),
(145, 'Curly-wavy', NULL, 'the day after tomorrow\nAviator\nMinuit le soir \nTaking lives\n(more in 2003 and 2004 but can\'t remeber all)\n2018 Cheval serpent \n2018 The bold type \n2018 Blood and treasure ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '514-601-4889', '514-601-4889', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Roselyn Ross', '1 (250) 540-0194', 'rozr@telus.net', 'Paul Stephen', '236-668-6413', NULL, 'Yes', 'No', '12', '33', NULL, 'Yes', 'Yes', 'Will be back in Vancouver first week of sept. Will be available all the time at first. Will let you know when it changes.', 'Canadian Citizen', 'No', 'Yes', '430', 'French', 'English', 147, NULL, NULL),
(146, 'long', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Military Type Men & Women ages 18-50 in Kamloops,BC Area', NULL, NULL, NULL, '778-867-2059', '778-867-2059', NULL, 'Teen', NULL, NULL, NULL, NULL, 'Lori Bonertz', '778-867-2059', 'loribonertz@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'available all shooting days', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 148, NULL, NULL),
(147, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-316-7465', '604-316-7465', NULL, 'Child', NULL, NULL, NULL, NULL, 'Nicole Curdie', '604-798-0321', 'nicolecurdie98@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'anytime', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 149, NULL, NULL),
(148, 'Curly', 'Yes', 'Hallmark films, good doctor, 100, power rangers', 'Apprentice Union', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Connekt creative-principal\nHollywood north - bg', NULL, NULL, NULL, NULL, '6043079815', '6043079818', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Dave demare', '6047614933', 'davedemare@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Open all summer', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Italian', 150, NULL, NULL),
(149, 'Waivy', 'Yes', 'The good doctor\nDeadly class\nSuper girl\nZoeâ€™s extraordinary playlist \nSiren\nSee\nLimetown\nInk\nLeague of legends \nAltered Carbon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Picobello Talent - 778-771-5860', 'Yes', NULL, NULL, NULL, '7785354696', '7785354696', 'rasitalvital@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'John Andreas', '6048416688', 'johnandreas8@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', '14.5', '42', NULL, 'No', 'Yes', 'Always available ', 'Canadian Citizen', 'Yes', 'Yes', '7465', 'English ', 'Spanish ', 151, NULL, NULL),
(150, NULL, NULL, 'Limetown, Super Girl, Where There\'s a Will, Fringe, Stargate Universe, Life Unexpected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dallas Talent.  604-415-4783', 'no', NULL, NULL, NULL, '778-839-2052', '778-839-2052', 'tnyle55@hotmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Bruce Luong', '604-961-337', 'b.luong@hotmail.com', 'Philiip Luong', '778-895-8222', 'ph.luong@yahoo.com', 'Yes', 'No', '15.5', NULL, NULL, 'No', 'Yes', '7 days a week. My agent and I use agencyclick.com for scheduling and I update it every day or two.', 'Canadian Citizen', 'Yes', 'Yes', '8958', 'English', 'Vietnamese', 152, NULL, NULL),
(151, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6047288922', '7789866762', NULL, 'Teen', 'THERESA CHIU', '6047288922', 'TOBY CHIU', '6047832633', 'THERESA CHIU', '6047288922', 'theresajc@shaw.ca', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'ENGLISH/CANTONESE', 'ITALIAN/MANDARIN', 153, NULL, NULL),
(152, 'Wavy or braids', NULL, 'Supergirl, Good Doctor, Van Helsing,Arrow, the 100, Snowpiercer,Blue Esme, A Million Little Things, Limetown, Batwoman ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'African Canadian ', NULL, NULL, NULL, '6043769228', '6043769228', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Sandy Onyalo ', '16137241702', 'mamasap@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, '34', NULL, 'No', 'Yes', 'Open availability Monday to Friday ', 'Canadian Citizen', 'Yes', 'Yes', '2729', 'English ', NULL, 154, NULL, NULL),
(153, 'Layered', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Extra, pretty much anything ', NULL, NULL, NULL, '7785912320', '6046445861', 'Patiencepayton@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Brad Freemantle', '6043529992', 'freemantlebrad30@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Available at anytime ', 'Canadian Citizen', 'Yes', 'Yes', '31', 'English', NULL, 155, NULL, NULL),
(154, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '604-378-2636', '604-378-2636', 'april.gallinger@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Caleb Steegstra', '6049973371', 'calebsteegstra@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Weekends ', 'Canadian Citizen', 'Yes', 'Yes', '5629', 'English', NULL, 156, NULL, NULL),
(155, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '604-378-2636', '604-378-2636', 'april.gallinger@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Caleb Steegstra', '6049973371', 'calebsteegstra@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Weekends ', 'Canadian Citizen', 'Yes', 'Yes', '5629', 'English', NULL, 157, NULL, NULL),
(156, 'bob', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'arameshatash@gmail.com, 6047828064', '6047828064', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Arash Atash', '7783184656', 'a_atash@yahoo.com', 'Sholeh Atash', '6047814302', 'sholeh.atash@gmail.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '8827', 'English, Persian', 'Farsi, Persian', 158, NULL, NULL),
(157, 'SHORT, SLIGHT UNDERCUT', 'CAN GROW WITHIN 2-3 DAYS', 'Flash, Arrow, Supergirl, Riverdale, The Mountains Between Us, Predator, Supernatural, Good Doctor, The Crossing, In Between Lives, The 100, The Magicians, Once Upon A Time, DC Legends of Tomorrow, Travelers, etc.', 'UBCP (FULL MEMBER)', '13243', NULL, NULL, NULL, NULL, NULL, NULL, 'INMOTION TALENT - OFFICE #604-336-5220\nJADE MICHELLE - 778-223-9113\nILISH OLIVER - 778-668-8484\nCHELSEA STRAND - 604-761-3104', NULL, NULL, NULL, NULL, '778-384-8078', '778-384-8078', 'araz.yaghoubi@yahoo.ca', 'Adult', 'Ilish Oliver', '778-668-8484', 'Jade Michelle', '778-223-9113', 'Ilish Oliver', '778-668-8484', 'ilisholiver@gmail.com', 'Shamsee', '778-751-2281', NULL, 'Yes', 'No', '16.5/17', NULL, NULL, 'Yes', 'Yes', 'FULL AVAILABILITY', 'Canadian Citizen', 'Yes', 'No', '347', 'ENGLISH', 'FARSI, TURKISH', 159, NULL, NULL),
(158, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Black Children Ages 2 to 13', NULL, NULL, NULL, '6045189420', '6045189420', NULL, 'Child', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 160, NULL, NULL),
(159, NULL, NULL, 'Pup Stars Christmas\nPuppy Prep Academy\nHall mark movie  \"Christmas Bells Are Ringing \"', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'xlarge  boys  14', 'Barb coultish   \n210-645 Fort street \nVictoria  BC\nV8W 1G2          250382-2670', 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '2508885796', '2504773121', 'lgseto@telus.net', 'Child', 'Coultish Management', '2503822670', NULL, NULL, 'laurie seto', '2508885796', 'lgseto@telus.net', 'Graham Seto', '2508897438', 'lgseto@telus.net', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'available at all times ', 'Canadian Citizen', 'Yes', 'Yes', '7618', 'english', NULL, 161, NULL, NULL),
(160, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', 'No', 'Yes', NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '6047046773', '6047046773', NULL, 'Adult', 'Jodie melrose ', '2368333705', NULL, NULL, 'Jodie Melrose', '2368333705', 'jodie.melrose@hotmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Flexible availability', 'Visa', 'Yes', 'Yes', '7282', 'English ', NULL, 162, NULL, NULL),
(161, 'Long (flowing)', 'NA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IN HOPE, BC: Small Town People with Lots of Character Ages 20-80', NULL, NULL, NULL, '6048690443', '6048690443', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Kate zabell/ arne zabell', '6048690443', 'arnezabell@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '8841', 'English', 'French', 163, NULL, NULL),
(162, 'long for a man', 'short well trimmed beard/mustache', 'Deadly Class, The Flash, Backstrom, Evil Men, Proof, Big Valley, Full Flood, The Company You Keep.  There are another dozen that I don\'t recall the names of.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, '604-916-4349', '604-916-4349', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Trish LaNauze', '604-298-6536', 'trish.l@shaw.ca', NULL, NULL, NULL, 'Yes', 'Yes', '15', NULL, NULL, 'No', 'Yes', 'The only days I\'m not available are Sundays and Tuesdays.', 'Canadian Citizen', 'Yes', 'Yes', '7465', 'English', NULL, 164, NULL, NULL),
(163, NULL, 'Mustache', 'The A Team\nAlice\nPretty Little Liars\n50/50\nSupernatural\nShooter\nOnce Upon A Time\nBirth of the Dragon\nIZOMBIE\nFifty Shades of Grey\nImpastor\nZoo\nWhen We Rise\nDead of Summer\nWonder\nShut Eye\nFrequency\nAngel in Training\nRiverdale\nLucifer\nThe Crossing\nThe Magicians\nTrial and Error\nValley of the Boom\nSearch and Destroy\nSiren\n', 'Apprentice Union', '27564', NULL, NULL, NULL, NULL, NULL, NULL, 'Showbiz Management  604-435-7469', 'IN HOPE, BC: Small Town People with Lots of Character Ages 35 to 60', NULL, NULL, NULL, '7783864775', '6045724647', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Myrna Buffie', '6043515440', 'gimyne59@telus.net', NULL, NULL, NULL, 'Yes', 'Yes', '15.5', '42', NULL, 'No', 'Yes', 'Available most days except Fridays and Saturdays', 'Canadian Citizen', 'Yes', 'Yes', '2050', 'English', NULL, 165, NULL, NULL),
(164, 'Traditional boys cut', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Gap 12-14', NULL, 'Extra for â€œThe Terrorâ€', NULL, NULL, NULL, '7789913876', '7789913876', NULL, 'Child', 'Teena Shinkawa', NULL, NULL, NULL, 'Peter Poon', '6047903846', 'poon.king.wai@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Available depending on schoolwork', 'Canadian Citizen', 'Yes', 'Yes', '6832', 'English', NULL, 166, NULL, NULL),
(165, 'Easy flexible med length cut', 'Blond', 'Cult\nThe 100 season 1,3,4,5\nSupernatural\nAltered Carbon\nOnce upon a time\nPercy Jackson 2\nTomorrow People\nFlash\nArrow\nIf I stay\nPower Rangers\nThe Magicians\n', 'Full union member since 2017 april\nEntertainment and show business performer since 2011', '69386', NULL, NULL, NULL, NULL, NULL, NULL, 'Keith Dallas of Dallas Talent. Phone number (604) 415-4783\nEmail agentkeithdallas@gmail.com', NULL, NULL, NULL, NULL, '7788296370', '7788296370', 'emodete@gmail.com', 'Adult', 'Keith Dallas/Dallas Talent', '(604) 415-4783', 'Devyn Neufeld', '+1 (604) 716-3700', 'Julie Peter ', '+1 (778) 953-1787', 'juliaauclair@ymail.com', 'Rene', '(778) 288-0891', NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Very flexible! \nMonday - Friday ', 'Canadian Citizen', 'Yes', 'Yes', '6291', 'Russian', 'English', 167, NULL, NULL),
(166, 'Varies', 'some', 'Percy Jackson 2 2012\nPackage deal - 2013\nBackstrom - 2013\nCedar Cove - 2013\nThe Killing - 2013\nGIRLFRIENDS GUIDE TO DIVORCE 2014 - 2017\nArrow 2014 - 2018\nFlash 2014 - 2018\nMotive - 2014\niZombie 2015 - 2017\nRoadies - 2015 \nFAIB Further Adventures In BabySitting 2015\nMistresses Season 3 - 2015 \nArmy of One 2015 \nCOBALT 2015\nBrain On Fire 2015\nBesties 2015\nLOOKINGLASS (AKA THE FRANKENSTEIN CODE) 2015\nSHIRT 2016\nFREQUENCY 2016 Season 1\nLegends 2016 - 2018\nThe 100 Seasons 1,3,4,5\nLucifer 2016 - 2017\nSupergirl 2017 - 2018\nAltered Carbon 2017 \nUNREAL - 2017\nWisdom OF The Crowd 2017\nRiverdale 2017 Season 2 \nLife Sentence 2017\nTHE HUNT/ Freaky Friday 2 2017\nSacred Lies 2018\nValley Of The Boom 2018\nTrial & Error 2018\nAlways Be My Maybe 2018\n', 'Full union member since 2017. ', '60386', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '778-829-6370', '778-829-6370', 'emodete@gmail.com', 'Adult', 'Tim (Carson Books and Records) (relationship: Employer)', '604-222-8787', 'Devyn Neufeld (relationship: family friend)', '604-716-3700', 'Julia Peter (mother)', '778-953-1787', 'juliaauclair@ymail.com', 'RenÃ¨', '778-288-0891', NULL, 'No', 'No', 'M', '36', NULL, 'No', 'Yes', 'full time, any time. 5 days a week or more \nPlease call to book\n\nThank you Kindly\n\nArt Peter', 'Canadian Citizen', 'No', 'Yes', '6291', 'Russian', 'English', 168, NULL, NULL),
(167, 'Blue', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asccreative', NULL, NULL, NULL, 'asd', 'adaa', NULL, 'Adult', NULL, NULL, NULL, NULL, 'freeman', '78787787878', 'freeman@info.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Please provide your availability details:\nPlease provide your availability details:\nPlease provide your availability details:\n', 'Canadian Citizen', 'Yes', 'Yes', '7898', 'English', NULL, 169, NULL, NULL),
(168, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, 'adsaa', 'asdsad', NULL, 'Adult', NULL, NULL, NULL, NULL, 'sadsa', 'asdas', 'sadsa@sadasdsa.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'asdasdsa', NULL, 170, NULL, NULL),
(169, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing, ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 171, NULL, NULL),
(170, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 172, NULL, NULL),
(171, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 173, NULL, NULL),
(172, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 174, NULL, NULL),
(173, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 175, NULL, NULL),
(174, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 176, NULL, NULL),
(175, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s\nHe does have a resume but trying to get the application to send so I\'ve removed and will send in an email ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 177, NULL, NULL),
(176, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 178, NULL, NULL),
(177, NULL, NULL, 'A million little things', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Registered Nurses with Operating Room Experience', NULL, NULL, NULL, '7782401181', '7782401181', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Devon Mahussier', '6049967362', 'devonmahussier@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'variable but decently flexible', 'Canadian Citizen', 'Yes', 'Yes', '495', 'English', NULL, 179, NULL, NULL),
(178, NULL, NULL, 'Descendants 3 - cheerleader \nRichard Says Goodbye -student  ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Background actor ', NULL, NULL, NULL, '+1 07785330774', '+1 07785330774', 'thornswolfbane@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Elaine ', '2505950098', 'carolrichter22@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', NULL, 'Canadian Citizen', 'Yes', 'Yes', '5588', 'English', NULL, 180, NULL, NULL),
(179, 'Wavy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2365221987', '2365221987', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Phil clements', '7782517445', 'lendonlee@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', '3992', 'English', NULL, 181, NULL, NULL),
(180, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Military Type Men & Women ages 18-50 in Kamloops, BC Area', NULL, NULL, NULL, '2503181567', '2503181567', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Sherrie Humphrey', '2506721872', 'spinx_lady@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '7757', 'English', NULL, 182, NULL, NULL),
(181, NULL, 'No ', 'Chesapeake shores\nThe play date- Hallmark ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No ', NULL, NULL, NULL, '2507131114', '2504680201', NULL, 'Teen', 'Jacqui Kaese ', '2507142555', 'Brianna Wiens or Peter Kent ', NULL, 'Jayne Essig ', '2507131114', 'equinoxacres@shaw.ca', 'Denny Essig ', '2507146246', 'equinoxacres@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Available the dates indicated (July 1-15) ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English ', NULL, 183, NULL, NULL),
(182, 'Long', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Youth med/large', NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '2506619508', '2506619508', NULL, 'Child', NULL, NULL, NULL, NULL, 'Tabitha Fong ', '5506619508', 'tabicat10@shaw.ca', 'Thomas fong', '2506860179', 'bugged@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Whenever will make myself available ', 'Canadian Citizen', 'Yes', 'No', NULL, 'English ', NULL, 184, NULL, NULL),
(183, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6047106323', '6047106323', NULL, 'Adult', 'Bobbi Baker ', 'â€­1 (250) 213-7998â€¬', 'Christian Laub', 'â€­(778) 858-6496â€¬', 'Andrew Bourque ', 'â€­+1 (514) 773-4022â€¬', 'andrewbourque92@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, '30â€', NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English ', 'Polish ', 185, NULL, NULL),
(184, NULL, NULL, 'Packaged Deal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-9920', '-9920', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Codee Palmer', '-9919', 'santilli.astrid@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'Yes', 'ASAP', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 186, NULL, NULL);
INSERT INTO `performers_details` (`id`, `hairStyle`, `hairFacial`, `bgPerformerDetails`, `unionAffiliations`, `unionNum`, `visa_student`, `visa_international`, `visa_open`, `visa_film`, `visa_film_other`, `shirt`, `agentDetails`, `castingTitle`, `job`, `cdnCitizenship`, `visa`, `dayPhone`, `eveningPhone`, `emailSecondary`, `applicationFor`, `referral1_name`, `referral1_phone`, `referral2_name`, `referral2_phone`, `emergencyContact1_name`, `emergencyContact1_phone`, `emergencyContact1_email`, `emergencyContact2_name`, `emergencyContact2_phone`, `emergencyContact2_email`, `driversLicense`, `leftHandDrive`, `neck`, `chest`, `insea`, `resume`, `bgPerformer`, `availability`, `cdnCitizen`, `bcResident`, `taxes`, `sin`, `languageFirst`, `languageSecond`, `performer_id`, `created_at`, `updated_at`) VALUES
(185, NULL, NULL, 'Packaged Deal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '17543028865', '17543028865', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Terry Lynn Krenn', '6043073809', 'tl.krenn@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Most weekdays, weekends and night.', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Italian and Spanish', 187, NULL, NULL),
(186, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '7788385717', '7788385717', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Glen Fullerton ', '7788385716', 'glen_fullerton@hotmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'Yes', '1355', 'Japanese', 'English', 188, NULL, NULL),
(187, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, NULL, NULL, NULL, 'Child', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 189, NULL, NULL),
(188, 'Curly ', NULL, 'Disney movie in Calgary December 2018 (diphtheria patient)  \nAlso SOC for hallmark movie just in time for Xmas 2016\nGracepoint (Fox TV) body double for child 2014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5/6 t', 'Debbie Mahood at Lucas talent 685-0345', 'No', NULL, NULL, NULL, '6042022857', '2506341771', NULL, 'Child', NULL, NULL, NULL, NULL, 'Michelle st louis', '6042247736', 'neiljamesparker@shaw.ca', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 190, NULL, NULL),
(189, 'Curly', NULL, 'Hallmark Just in Time for Christmas (SOC) in Victoria, Gracepoint - body double, Fox TV, Disney Movie \"Alaska\" (Oct 2018) in Calgary ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Youth S', 'Debbis Mahood Lucas Talent 604-685-0345', 'Kids ages 6-19 in Victoria,BC', NULL, NULL, NULL, '6042022857', '6042022857', NULL, 'Child', NULL, NULL, NULL, NULL, 'Neil Parker', '6045657240', 'neiljamesparker@shaw.ca', 'Michelle St Louis', '6042247736', 'michellestlouis2@gmail.com', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'July 1-15 in Victoria, outside of these dates he is available in Vancouver. Brother Tate Parker and father Neil Parker also experienced background performers', 'Canadian Citizen', 'Yes', 'Yes', '5528', 'English', NULL, 191, NULL, NULL),
(190, 'Crew Cut', 'Clean Shaven', 'Man in the High Castle, Supergirl, Riverdale, A Million Little Things, Skyscraper, Predator, The Good Doctor, Lucifer, I-zombie, ', 'Apprentice Union', 'AM- 29405', NULL, NULL, NULL, NULL, NULL, NULL, 'Showbiz Management (604) 435-7469', 'Japanese People', NULL, NULL, NULL, '7789810188', '7789810188', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Kiyoko Kumo', '604-277-0570', 'umesann@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', '15', '36', NULL, 'No', 'Yes', 'Any date with a couple days notice', 'Canadian Citizen', 'Yes', 'Yes', '9905', 'Japanese', 'English', 192, NULL, NULL),
(191, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kids ages 6-19 in Victoria,BC', NULL, NULL, NULL, '250-857-5698', '250-857-5698', 'Shelleyfrick@shaw.ca', 'Teen', 'Sandra Webster-Worthy', NULL, NULL, NULL, 'Shelley Frick', '250-857-5698', 'Shelleyfrick@shaw.ca', 'Carl Frick', '250-208-1501', 'carlfrick@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Available Up until July 12', 'Canadian Citizen', 'Yes', 'Yes', '8279', 'English', 'French', 193, NULL, NULL),
(192, 'Long with bangs', 'No', 'Featured Background in \'Love at First Dance\'\nBackground in \'Christmas Bells are Ringing\'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'small to medium  (slim but tall)', 'Coultish Management \nBarbara Coultish\n210 - 645 Fort Street\nVictoria BC  V8W 1G2\n250.382.2670\n', 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '250.383.4883', '250.383.4883', NULL, 'Child', 'Barbara Coultish', '250.382.2670', NULL, NULL, 'Laura Cooper', 'H -250.383.4883 / Mobile -250.516.4883 ', 'coultishmodels@telus.net', 'Cam Cooper', 'H -250.480.8052 /W-250.475.2669 / Mobile:-250.383.4883 ', 'cam@cooperpacific.ca', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Audrey\'s schedule is fairly flexible and we will try to make her available if you ask her to set.  She is not available on July 15th', 'Canadian Citizen', 'Yes', 'Yes', '1776', 'English', NULL, 194, NULL, NULL),
(193, NULL, NULL, 'Student Productions', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7785543772', '7785543772', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Anne Laflamme ', '6045819666', 'too@oldforanemail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Every day except Tuesdays', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 195, NULL, NULL),
(194, 'Curly ', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N/A', NULL, NULL, NULL, '6049161778', '6049161778', 'austenadele@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Tyrell Collins', '2506672133', 'tyrell194@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, '36', NULL, 'No', 'No', 'I run my own business in Victoria, I have flexible hours with some notice.', 'Canadian Citizen', 'Yes', 'Yes', '362', 'English', 'N/A', 196, NULL, NULL),
(195, 'Clean cut, ', 'N/A', 'Sonic the Hedgehog 2018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-11 00:00:00', NULL, 'Boys Ages 8 to 11 Years old on the Island', NULL, NULL, NULL, '2508842493', '2508842493', NULL, 'Child', 'Jacqui Kaese', '2507142555', NULL, NULL, 'Angela Friesen', '2508842493', 'anelephantstear@hotmail.com', 'Trevor Friesen', '2508823052', 'trevorfriesen123@gmail.com', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Available weekends and days. \nNights if needed, although I think thereâ€™s restrictions to this. ', 'Canadian Citizen', 'Yes', 'No', '6652', 'English', 'N/A', 197, NULL, NULL),
(196, NULL, 'No', 'Sonic the Hedgehog (1) , 4 summer weddings,(1)\nChesapeake Shores, (1)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Size 12', NULL, 'No', NULL, NULL, NULL, '2508842493', '25088423', NULL, 'Child', 'Jacqui Kaese -Spotlight Academy', '2507142555', 'Andrea Hughes ', '6043051074', 'Angie Friesen', '2508842493', 'anelephantstear@hotmail.com', 'Trevor Friesen', '2508823052', 'tfriesen123@gmail.com', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Monday through Sunday\n', 'Canadian Citizen', 'Yes', 'Yes', '6652', 'English', 'NA', 198, NULL, NULL),
(197, 'Straight', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '7782550141', '7782550141', NULL, 'Child', NULL, NULL, NULL, NULL, 'Emily Kivinen', '7782550141', 'emilykivinen@gmail.con', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 199, NULL, NULL),
(198, NULL, NULL, 'Delta Film Academy - Tripped Up', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Camp Kids', NULL, NULL, NULL, '6042404729', '6042404729', NULL, 'Teen', NULL, NULL, NULL, NULL, 'Nadine Bak', '6042404729', 'nqbak@telus.net', 'Andrew Bak', '6042402407', 'antiquus@telus.net', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Aug 12 - 19, 2018', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', NULL, 200, NULL, NULL),
(199, 'Long and curly', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Size 6', NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '6048452678', '6048452678', 'Amieg@hotmail.com', 'Child', NULL, NULL, NULL, NULL, 'Amie jalava', '6048452678', 'Amieg@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 201, NULL, NULL),
(200, 'wavy/curly', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'youth large', NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '250-858-8572', '250-858-8572', 'sharon.rubin.2005@gmail.com', 'Child', 'Tia Niedjalski', '250-893-5750', NULL, NULL, 'Sharon Rubin', '250-507-4131', 'sharon.rubin.2005@gmail.com', 'Paul Lancaster', '250-858-8572', 'paullancaster@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', NULL, 202, NULL, NULL),
(201, NULL, NULL, 'All in Madonna independent film shot in Victoria', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Background work or anything principle', NULL, NULL, NULL, '2508933153', '2508933153', 'jmteam@me.com', 'Teen', 'Jacqui Kaese Spotlight Academy ', '2507142555', NULL, NULL, 'Maria McGaghey', '2508933153', 'jmteam@me.com', 'John McGaghey', '2508893089', 'jmteam@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Usually always avaliable', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English ', NULL, 203, NULL, NULL),
(202, 'Long Bob', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Youth Size 12  - Adult Size Extra Small', NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '2505723761', '2505723761', 'kmorris77@live.ca', 'Child', 'Alan Hopkins', NULL, NULL, NULL, 'Kirsty Morris', '2505723761', 'kmorris77@live.ca', 'Craig Morris', '250-572-3294', 'cgm73@live.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Available all of July - August 2019', 'Canadian Citizen', 'Yes', 'No', '4002', 'English', NULL, 204, NULL, NULL),
(203, 'Short', 'Varrying', 'Thousands of shows since I started BG in 2012, a lot of SKF as well', 'Dgc,ia,Teamsters permits. Actra/ubcp full', '12299', NULL, NULL, NULL, NULL, NULL, NULL, 'Local Color', NULL, NULL, NULL, NULL, '17783185571', '7783185571', 'averized@gmail.com', 'Adult', 'Dale Bredeson (1st AD, not sure why you need references for bg?)', '6047874138', 'Alex Campbel (fellow actor)', '6046536533', 'Norman Klenman, grand father', '7788852412', 'nklenman@shaw.ca', NULL, NULL, NULL, 'Yes', 'No', '16', NULL, NULL, 'Yes', 'Yes', 'Full-time flexible', 'Canadian Citizen', 'Yes', 'Yes', '4003', 'English', 'Japanese', 205, NULL, NULL),
(204, 'long wavey', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '778-840-7522', '604-924-4451', 'paulahilliard@shaw.ca', 'Teen', 'Tara Fuhre', '604-671-7407', 'Rachel Attie', '604-831-2963', 'Paula Hilliard', '778-840-7522', 'paulahilliard@shaw.ca', 'Shawn Hilliard', '604-671-5202', 'shawn@somersetcustomehomes.ca', 'No', 'No', '12\"', '31 1/2\"', NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 206, NULL, NULL),
(205, NULL, 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '514-806-4724', '514-806-4724', NULL, 'Adult', 'Annie Walls', '1-604-816-7383', NULL, NULL, 'Sherry Ezzard', '250-319-8084', 'sezzard@hotmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', 'Full availability - Monday through Sunday', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 207, NULL, NULL),
(206, 'bob hair', NULL, 'Kim possible\n\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MVM Agency Brenda Wong  Mobile: 604-318-8104 Email: brenda@mvmagency.ca ', NULL, NULL, NULL, NULL, '778-887-8464', '778-887-8464', 'youkaien@gmail.com', 'Adult', 'Jason Furukawa 1st Assistant Director (Charmed)', '604-816-1567', 'Brenda Wong', '604-318-8104', 'Jason Furukawa', '604-816-1567', 'fujikurosawa@gmail.com', 'Akane Furukawa', '604-230-9316', 'neko-66@hotmail.co.jp', 'Yes', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'All days -extremely flexible schedule\n\nFrom morning till evening', 'Canadian Citizen', 'Yes', 'Yes', '7016', 'English', 'Japanese', 208, NULL, NULL),
(207, 'Curly', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8 or small ', NULL, 'Any role- she was made for the stage.', NULL, NULL, NULL, '7789773692', '7789773692', 'arielleboivin@gmail.com', 'Child', 'Paul Thompson', '(250) 216-0349', NULL, NULL, 'Jason Lelievre', '2505164265', 'jasonlelievre34@gmail.com', 'Sally Boivin', '2505142713', 'sallyboivin@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English ', NULL, 209, NULL, NULL),
(208, 'Waivy', 'Light', 'Pace Academy theatre 3rd year Both Shows - Christmas and Spring Show', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Background', NULL, NULL, NULL, '2508963189', '6048977001', 'linneadstevenson@gmail.com', 'Teen', 'Pace Academy ', '2508963189', 'Jody Stevenson', '2508963189', 'Linnea Stevenson', '2508963189', 'linneadstevenson@gmail.com', 'Jody Stevenson', '2508963189', 'jodynlinnea@gmail.com', 'No', 'No', '13.5 inch ', '40 inches ', NULL, 'No', 'Yes', 'Anytime ', 'Canadian Citizen', 'Yes', 'Yes', '1262', 'English', 'None', 210, NULL, NULL),
(209, 'Straight/natural wave', 'None', 'Shortland Street, New Zealand\n', NULL, NULL, 'No', 'Yes', 'No', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7787511716', '7787511716', 'ayla@aylaamano.com', 'Adult', 'Matthew Wilmot', '6421479360', 'Kelly Seabourne', '6493033849', 'Chako Amano', '64226574958', 'chakoamano@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Currently flexible / open availability ', 'Visa', 'Yes', 'Yes', '8860', 'English', NULL, 211, NULL, NULL),
(210, 'Straight ', 'None ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-10 00:00:00', NULL, 'No ', NULL, NULL, NULL, '6047687108', '6047687108', NULL, 'Child', NULL, '6047687108', NULL, '6047687108', 'Desire Colley ', '6047687108', 'desirewickenden@gmail.com', 'Jordan. colley ', '6047981107', 'jordancolley29@gmail.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Anytime ', 'Canadian Citizen', 'Yes', 'Yes', '5300', 'English ', NULL, 212, NULL, NULL),
(211, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6043582152', '7788867904', 'kaoru1.kojima@gmail.com', 'Child', 'Shinobu Kojima ', '7788867904', NULL, NULL, 'Kaoru Kojima', '7788398107', 'kaoru1.kojima@gmail.com', 'Shinobu Kojima ', '7788867904', NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'No', '3200', 'English ', 'Japanese ', 213, NULL, NULL),
(212, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7788398107', '7788398107', NULL, 'Child', NULL, NULL, NULL, NULL, 'kaoru kojima', '7788398107', 'kaoru1.kojima@gmail.com', 'SHINOBU KOJIMA', '7788867904', NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '3200', 'ENGLISH', 'JAPANESE', 214, NULL, NULL),
(213, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '778-895-9489', '778-895-9489', 'dakkila@gmail.com', 'Adult', 'anne lin', '778-928-7325', 'sumi hamada ', '604-618-0254', 'dakkila Ramos', '778-885-8186', 'dakkila@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'my  work schedule is same as school schedule, available after 2pm on weekday, I\'m off weekend, holiday and winter, spring and summer vacation. ', 'Canadian Citizen', 'Yes', 'Yes', '1674', 'Japaniese ', 'Engligh ', 215, NULL, NULL),
(214, NULL, NULL, 'Colbalt\nProof\nFAIB\nFlash\nThe Advocate\nRoadies\niZombie\nBates Motel\nThe 100\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'I would want more principal or commercial work but background is fine to start! ', NULL, NULL, NULL, '2369880996', '2369880996', 'vancityprince.hyf@gmail.com', 'Adult', 'Lisa Towers', '6043294454', NULL, NULL, 'Kumba Agnes', '7788790692', 'kumbaagnes@hotmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Iâ€™m free Monday-Thursday \n', 'Canadian Citizen', 'Yes', 'Yes', '4427', 'English ', 'Creole ', 216, NULL, NULL),
(215, NULL, NULL, 'Colbalt\nProof\nFAIB\nFlash\nThe Advocate\nRoadies\niZombie\nBates Motel\nThe 100\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'I would want more principal or commercial work but background is fine to start! ', NULL, NULL, NULL, '2369880996', '2369880996', 'vancityprince.hyf@gmail.com', 'Adult', 'Lisa Towers', '6043294454', NULL, NULL, 'Kumba Agnes', '7788790692', 'kumbaagnes@hotmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Iâ€™m free Monday-Thursday \n', 'Canadian Citizen', 'Yes', 'Yes', '4427', 'English ', 'Creole ', 217, NULL, NULL),
(216, 'Straight bob', 'None', 'Salvation 2, Dead Pool2, Hallmarks, Arrow, The Good Doctor, The Flash, The Crossing, Skyscraper, Riverdale, Once Upon a Time, Haily Dean 6, Girl in the Bathtub,,Valley of the Boom, Harry and Megan,â€Royal Romanceâ€, Marrying Mr Darcy,  Spontaneaous, Darrow &Darrow 2, Noelle, UnReal 4, The Simone Biles story, Colony 3, Eggplant Emoji, Siren 1, iZombie, The Exorcist, Life Sentence, Supergirl, Supernatural, The Arrangement, Xfiles, Take Two, ', 'Background Union Member ', '4434', NULL, NULL, NULL, NULL, NULL, NULL, 'Lucas Talent \n1238 Homer St \n604-685-0345  ', NULL, NULL, NULL, NULL, '6047290727', '6047290727', 'bonbahar@gmail.com', 'Adult', 'Derek Nordick Lucas Talent ', 'â€­+1 (604) 318-4240â€¬', 'Jen Lee Lucas Talent ', 'â€­+1 (604) 808-6271â€¬', 'Rav Grewal ( Husband) ', '6047817721', 'rgrewal.3d@gmail.com', 'Fernanda Gracco ', 'â€­(778) 889-2610â€¬', 'feberto@icloud.com', 'Yes', 'Yes', '14.5', '35-37', NULL, 'No', 'Yes', 'I am available FT. ', 'Canadian Citizen', 'Yes', 'Yes', '237', 'Farsi', 'English ', 218, NULL, NULL),
(217, 'Straight ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Any role', NULL, NULL, NULL, '7789960494', '7789960494', NULL, 'Adult', 'Simona', '7783207241', 'Maryam', '6047797761', 'Maryam ', '6047797761', 'mojganpolosi@yahoo.ca', 'Zara', '7788617215', 'zahraehtemam@yahoo.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '1223', 'Farsi/', 'English ', 219, NULL, NULL),
(218, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Background ', NULL, NULL, NULL, '7789960494', '7789960494', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Simona', '7783207241', 'spring1870casting@gmail.com', 'Zara', '7788617215', 'zaraehtemam@yahoo.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Monday/Tuesday/Wednesday/Thursday/Friday/Sunday and night shift', 'Canadian Citizen', 'Yes', 'Yes', '1223', 'Farsi', 'English ', 220, NULL, NULL),
(219, 'Straight', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open Casting Call Agassiz BC', NULL, NULL, NULL, '2368873136', '2368873136', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Brennan Bateman', '7752379086', 'uniquelyyours.bc@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'open availability', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 221, NULL, NULL),
(220, 'med hair, full and straight', 'none ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IN SQUAMISH,BC: Small Town People with Lots of Character Ages 35 to 60', NULL, NULL, NULL, '604 306 8920', '604 306 8920', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Michael Roblin', '604-848-9328', 'mroblin@dynamicpm.ca', NULL, NULL, NULL, 'Yes', 'No', '13.5\"', '42\"', NULL, 'Yes', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Plants', 222, NULL, NULL),
(221, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7788652500', '7788652500', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Rajdeep', '6048314331', 'rajarjun224@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'Punjabi hindi English ', 'English ', 223, NULL, NULL),
(222, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', 'No', 'No', NULL, NULL, NULL, 'Singer /songwriter /modal', NULL, NULL, NULL, '2368803705', '2368803705', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Paska Aber ', '256772386221', 'paskaber@yahoo.com', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL, NULL, 'Yes', 'No', 'Am available Monday to Friday \nAround 8:00am to 7:00pm', 'Visa', 'Yes', 'Yes', '3602', 'Luo', 'English', 224, NULL, NULL),
(223, 'slightly layered', 'na', 'Over 300 days on set as a Background Performer since 2015\n2 days on set as an Actor\nContinuity on Bates Motel S4 (Mental Patient)\nContinuity on The 100 S3, 4, 5 (Blue Cliff Ambassador http://the100.wikia.com/wiki/File:Blue_Cliff_Ambassador.jpg)\nContinuity on The Magicians (Professor/Librarian)\nContinuity on The Descendants 2 (Sea Hag https://www.youtube.com/watch?v=4Vv-zcAoer8)\nContinuity on Riverdale (teacher) \nContinuity on Deadpool 2 (Mutant Prisoner - 20 days on set) \nContinuity on Sacred Lies (Cult Member)\nContinuity on Salvation (Chief Justice of the Supreme Court)\nContinuity on The Women of Marwen (trailer trash Mom)\nWhen We Rise played Clerk of the Court (Pamela Talkin)\nWhen We Rise played Nun\nIzombie (White Zombie)\nSupernatural played Nun\nand many, many other parts including everything from high end gala guest to homeless addict', 'UBCP/ACTRA', '60708', NULL, NULL, NULL, NULL, NULL, NULL, 'ShowBiz Management', NULL, NULL, NULL, NULL, '778 228-1072', '778 228-1072', NULL, 'Adult', 'Roland Pointer ShowBiz Management', '604 780-0566', 'James Forsyth BCF Casting', '604 817-2272', 'Hanoch Pearce son', '778 836-0992', 'hpearce888@gmail.com', 'Judi Wilson sister', '778 689-6879', 'wilson.judi2010@gmail.com', 'Yes', 'Yes', '12\"', NULL, NULL, 'Yes', 'Yes', 'I have full time availability', 'Canadian Citizen', 'Yes', 'Yes', '5064', 'English', NULL, 225, NULL, NULL),
(224, NULL, NULL, 'Hot Tub Time Machine, Ramona and Beezus, Psych', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '7789287325', '7789287325', 'anneikeda@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Ken Lin', '6047257325', 'klin1022@gmail.com', 'Henry Ikeda', '604-597-1312', NULL, 'Yes', 'Yes', '12.5', '32', NULL, 'No', 'Yes', 'Any day. I also submitted my son\'s application (Maxton Lin) so if you could schedule us together that would be great. ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Japanese', 226, NULL, NULL),
(225, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '604-316-8884', '604-316-8884', 'amvictor@gmail.com', 'Teen', NULL, NULL, NULL, NULL, 'Melanie Victor', '6043168884', 'amvictor@gmail.com', 'Andrew Victor', '604-316-3784', 'amvictor@gmail.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Halq\'emeylem', 227, NULL, NULL),
(226, NULL, NULL, 'The Rhino Brothers (film)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-467-8963', '604-467-8963', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Marcel Renaud', '604-607-4795', 'mjrenaud@shaw.ca', 'Margaret Stevenson', '604-463-9432', 'margstevenson@hotmail.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Available full week of March 25 to 29', 'Canadian Citizen', 'Yes', 'Yes', '7960', 'English', NULL, 228, NULL, NULL),
(227, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Boys & Girls Ages 12-16 Years Old', NULL, NULL, NULL, '6043163900', '6043163900', 'xxxkanata@yahoo.com', 'Adult', NULL, NULL, NULL, NULL, 'Ella Monro MD', '250 438 0917', 'emonro@gmail.com', 'Carolyn Schillimore', '604 7995379', 'carekipp@live.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '9520', 'english', 'n/a', 229, NULL, NULL),
(228, 'Clean cut', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Xs', NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '2508186688', '2508186688', 'gccmandy@gmail.com', 'Child', NULL, NULL, NULL, NULL, 'Mandy Lee', '2508186688', 'ml123456@gmail.com', 'Kevin Pinsonneault', '7786868720', 'rppempire@yahoo.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'July 1-15', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', 'French', 230, NULL, NULL),
(229, 'Straight ', 'No', 'Timmy Failure  (Airborne )', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-10 00:00:00', NULL, 'No', NULL, NULL, NULL, '7782986453', '6044401174', NULL, 'Child', NULL, NULL, NULL, NULL, 'Viviana ', '6044401174', 'viviana.gehring@hotmail.com', 'Michael ', '6047861378', 'redswiss@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', NULL, 'Canadian Citizen', 'Yes', 'No', '8363', 'English', 'None', 231, NULL, NULL),
(230, 'Straight', NULL, 'Lucifer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Extra', NULL, NULL, NULL, '2503055122', '2503055122', NULL, 'Adult', 'Coco ', '6048052390', 'Ana Nunes', '7783164157', 'Armin', '6043582339', 'arminfrischknecht@hotmail.com', 'Sean ', '6042306285', 'sean.gabriel@sjrb.ca', 'Yes', 'Yes', '13', NULL, NULL, 'No', 'Yes', 'When required ', 'Canadian Citizen', 'Yes', 'Yes', '85', 'English ', NULL, 232, NULL, NULL),
(231, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6047012588', '6047012588', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Josh burr ', '6047951129', 'joshandbecca@hotmail.ca', 'Don loeppky', '6047033233', NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', 'Evenings and anytime on weekends. \nFor week day hours I would need to give one weeks notice to my current employer ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 233, NULL, NULL),
(232, 'Curly-wavy', NULL, 'the day after tomorrow\nAviator\nMinuit le soir \nTaking lives\n(more in 2003 and 2004 but can\'t remeber all)\n2018 Cheval serpent \n2018 The bold type \n2018 Blood and treasure ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '514-601-4889', '514-601-4889', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Roselyn Ross', '1 (250) 540-0194', 'rozr@telus.net', 'Paul Stephen', '236-668-6413', NULL, 'Yes', 'No', '12', '33', NULL, 'Yes', 'Yes', 'Will be back in Vancouver first week of sept. Will be available all the time at first. Will let you know when it changes.', 'Canadian Citizen', 'No', 'Yes', '430', 'French', 'English', 234, NULL, NULL),
(233, 'long & wavey', 'No', 'Continuity character on Riverdale, the 100s, many many movies, lots of music videos. Been working in the film industry for lots of years as a special skills motorcyclist.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-354-5016', '604-354-5016', NULL, 'Adult', ' Coco Bikadoroff @ Double Agents', '604-805-2390', 'Rob Raco @ Riverdale', '323-681-3101', 'Blaine Connolly', '604-354-5016', 'blaine.connolly88@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'I work freelance for myself and create my own schedule. ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 235, NULL, NULL),
(234, 'long', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Military Type Men & Women ages 18-50 in Kamloops,BC Area', NULL, NULL, NULL, '778-867-2059', '778-867-2059', NULL, 'Teen', NULL, NULL, NULL, NULL, 'Lori Bonertz', '778-867-2059', 'loribonertz@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'available all shooting days', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 236, NULL, NULL),
(235, 'Wavy bob', 'n/a', 'Supernatural, A Series of Unfortunate Events, Riverdale', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-315-5576', '604-315-5576', 'janetmccairns@telus.net', 'Teen', 'LA Hiltz', '604-839-5833', 'Lorne Davidson', '604-290-4531', 'Janet McCairns', '604-315-5576', 'janetmccairns@telus.net', 'Lorne Davidson', '6042904531', 'dmb_live@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Always available', 'Canadian Citizen', 'Yes', 'Yes', '6851', 'English', NULL, 237, NULL, NULL),
(236, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-316-7465', '604-316-7465', NULL, 'Child', NULL, NULL, NULL, NULL, 'Nicole Curdie', '604-798-0321', 'nicolecurdie98@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'anytime', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 238, NULL, NULL),
(237, 'Curly', 'Yes', 'Hallmark films, good doctor, 100, power rangers', 'Apprentice Union', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Connekt creative-principal\nHollywood north - bg', NULL, NULL, NULL, NULL, '6043079815', '6043079818', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Dave demare', '6047614933', 'davedemare@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Open all summer', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Italian', 239, NULL, NULL),
(238, 'Waivy', 'Yes', 'The good doctor\nDeadly class\nSuper girl\nZoeâ€™s extraordinary playlist \nSiren\nSee\nLimetown\nInk\nLeague of legends \nAltered Carbon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Picobello Talent - 778-771-5860', 'Yes', NULL, NULL, NULL, '7785354696', '7785354696', 'rasitalvital@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'John Andreas', '6048416688', 'johnandreas8@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', '14.5', '42', NULL, 'No', 'Yes', 'Always available ', 'Canadian Citizen', 'Yes', 'Yes', '7465', 'English ', 'Spanish ', 240, NULL, NULL),
(239, NULL, 'none', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2506676711', '2506676711', 'benjaminloyst@gmail.com', 'Adult', 'Bahar', '7789960494', 'Simona', '7783207241', 'Lynn Tissington', '12506199992', 'lynntissington@sd68.bc.ca', 'David Loyst', '16047606787', 'Davidloyst@shaw.ca', 'No', 'No', '14.5', NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '7165', 'English', 'French', 241, NULL, NULL),
(240, NULL, 'none', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2506676711', '2506676711', 'benjaminloyst@gmail.com', 'Adult', 'Bahar', '7789960494', 'Simona', '7783207241', 'Lynn Tissington', '12506199992', 'lynntissington@sd68.bc.ca', 'David', '1 604 760 6787', 'davidloyst@shaw.ca', 'No', 'No', '14.5', NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '7165', 'English', 'French', 242, NULL, NULL),
(241, NULL, NULL, 'Limetown, Super Girl, Where There\'s a Will, Fringe, Stargate Universe, Life Unexpected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dallas Talent.  604-415-4783', 'no', NULL, NULL, NULL, '778-839-2052', '778-839-2052', 'tnyle55@hotmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Bruce Luong', '604-961-337', 'b.luong@hotmail.com', 'Philiip Luong', '778-895-8222', 'ph.luong@yahoo.com', 'Yes', 'No', '15.5', NULL, NULL, 'No', 'Yes', '7 days a week. My agent and I use agencyclick.com for scheduling and I update it every day or two.', 'Canadian Citizen', 'Yes', 'Yes', '8958', 'English', 'Vietnamese', 243, NULL, NULL),
(242, NULL, NULL, 'A Dogâ€™s Way Home', NULL, NULL, 'No', 'No', 'No', 'Yes', NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6047936054', '6047936054', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Derek schmidt', '6047121987', 'daschmidt217@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'I am pretty flexible', 'Visa', 'Yes', 'Yes', '3051', 'Filipino', 'English', 244, NULL, NULL),
(243, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No. Applicant just turned 18 years old.', NULL, NULL, NULL, '7783196133', '7783196133', 'onehallfamily@gmail.com', 'Teen', NULL, NULL, NULL, NULL, 'Annalice Hall ', '7783196133', 'annalice.hall@gmail.com', 'Milon Hall ', '7787107251', 'onehallfamily@gmail.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Currently in Grade 12th, very flexible with schedule. Available to work any time and any day. Thank you.', 'Canadian Citizen', 'Yes', 'No', NULL, 'Filipino', 'English', 245, NULL, NULL),
(244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6047288922', '7789866762', NULL, 'Teen', 'THERESA CHIU', '6047288922', 'TOBY CHIU', '6047832633', 'THERESA CHIU', '6047288922', 'theresajc@shaw.ca', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'ENGLISH/CANTONESE', 'ITALIAN/MANDARIN', 246, NULL, NULL),
(245, NULL, 'Yes', 'The Twilight zone, Surveillance, Altered Carbon', NULL, NULL, 'No', 'Yes', 'No', 'Yes', NULL, NULL, 'Talent bid', NULL, NULL, NULL, NULL, '3', '7', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Patrick or Catherine Gambier', '+33 164077738', 'bernard_77_@live.fr', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Tuesday, Wednesday, Thursday (until 5pm), Friday, Saturday (until 3.30pm) , Sunday (until 5pm)', 'Visa', 'Yes', 'Yes', '975', 'French', 'English', 247, NULL, NULL),
(246, 'Wavy or braids', NULL, 'Supergirl, Good Doctor, Van Helsing,Arrow, the 100, Snowpiercer,Blue Esme, A Million Little Things, Limetown, Batwoman ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'African Canadian ', NULL, NULL, NULL, '6043769228', '6043769228', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Sandy Onyalo ', '16137241702', 'mamasap@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, '34', NULL, 'No', 'Yes', 'Open availability Monday to Friday ', 'Canadian Citizen', 'Yes', 'Yes', '2729', 'English ', NULL, 248, NULL, NULL),
(247, 'Layered', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Extra, pretty much anything ', NULL, NULL, NULL, '7785912320', '6046445861', 'Patiencepayton@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Brad Freemantle', '6043529992', 'freemantlebrad30@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Available at anytime ', 'Canadian Citizen', 'Yes', 'Yes', '31', 'English', NULL, 249, NULL, NULL),
(248, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '604-378-2636', '604-378-2636', 'april.gallinger@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Caleb Steegstra', '6049973371', 'calebsteegstra@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Weekends ', 'Canadian Citizen', 'Yes', 'Yes', '5629', 'English', NULL, 250, NULL, NULL),
(249, NULL, NULL, 'Background Audience - Tensai Shimura Doubutsu-en, and couple other Tv shows', NULL, NULL, 'No', 'No', 'No', 'No', 'I have a Canadian PR (but not a Canadian Citizen)', NULL, NULL, 'Japanese People', NULL, NULL, NULL, '5pm', '5pm', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Hilda K', '+1(604)230-4085', 'hilda.kisielewski@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', NULL, 'Visa', 'Yes', 'Yes', '398', 'Indonesian', 'Japanese', 251, NULL, NULL),
(250, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '604-378-2636', '604-378-2636', 'april.gallinger@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Caleb Steegstra', '6049973371', 'calebsteegstra@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Weekends ', 'Canadian Citizen', 'Yes', 'Yes', '5629', 'English', NULL, 252, NULL, NULL),
(251, 'bob', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'arameshatash@gmail.com, 6047828064', '6047828064', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Arash Atash', '7783184656', 'a_atash@yahoo.com', 'Sholeh Atash', '6047814302', 'sholeh.atash@gmail.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '8827', 'English, Persian', 'Farsi, Persian', 253, NULL, NULL),
(252, 'SHORT, SLIGHT UNDERCUT', 'CAN GROW WITHIN 2-3 DAYS', 'Flash, Arrow, Supergirl, Riverdale, The Mountains Between Us, Predator, Supernatural, Good Doctor, The Crossing, In Between Lives, The 100, The Magicians, Once Upon A Time, DC Legends of Tomorrow, Travelers, etc.', 'UBCP (FULL MEMBER)', '13243', NULL, NULL, NULL, NULL, NULL, NULL, 'INMOTION TALENT - OFFICE #604-336-5220\nJADE MICHELLE - 778-223-9113\nILISH OLIVER - 778-668-8484\nCHELSEA STRAND - 604-761-3104', NULL, NULL, NULL, NULL, '778-384-8078', '778-384-8078', 'araz.yaghoubi@yahoo.ca', 'Adult', 'Ilish Oliver', '778-668-8484', 'Jade Michelle', '778-223-9113', 'Ilish Oliver', '778-668-8484', 'ilisholiver@gmail.com', 'Shamsee', '778-751-2281', NULL, 'Yes', 'No', '16.5/17', NULL, NULL, 'Yes', 'Yes', 'FULL AVAILABILITY', 'Canadian Citizen', 'Yes', 'No', '347', 'ENGLISH', 'FARSI, TURKISH', 254, NULL, NULL),
(253, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Black Children Ages 2 to 13', NULL, NULL, NULL, '6045189420', '6045189420', NULL, 'Child', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 255, NULL, NULL),
(254, NULL, NULL, 'Pup Stars Christmas\nPuppy Prep Academy\nHall mark movie  \"Christmas Bells Are Ringing \"', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'xlarge  boys  14', 'Barb coultish   \n210-645 Fort street \nVictoria  BC\nV8W 1G2          250382-2670', 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '2508885796', '2504773121', 'lgseto@telus.net', 'Child', 'Coultish Management', '2503822670', NULL, NULL, 'laurie seto', '2508885796', 'lgseto@telus.net', 'Graham Seto', '2508897438', 'lgseto@telus.net', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'available at all times ', 'Canadian Citizen', 'Yes', 'Yes', '7618', 'english', NULL, 256, NULL, NULL),
(255, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', 'No', 'Yes', NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '6047046773', '6047046773', NULL, 'Adult', 'Jodie melrose ', '2368333705', NULL, NULL, 'Jodie Melrose', '2368333705', 'jodie.melrose@hotmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Flexible availability', 'Visa', 'Yes', 'Yes', '7282', 'English ', NULL, 257, NULL, NULL),
(256, 'Long (flowing)', 'NA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IN HOPE, BC: Small Town People with Lots of Character Ages 20-80', NULL, NULL, NULL, '6048690443', '6048690443', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Kate zabell/ arne zabell', '6048690443', 'arnezabell@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '8841', 'English', 'French', 258, NULL, NULL),
(257, 'long for a man', 'short well trimmed beard/mustache', 'Deadly Class, The Flash, Backstrom, Evil Men, Proof, Big Valley, Full Flood, The Company You Keep.  There are another dozen that I don\'t recall the names of.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, '604-916-4349', '604-916-4349', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Trish LaNauze', '604-298-6536', 'trish.l@shaw.ca', NULL, NULL, NULL, 'Yes', 'Yes', '15', NULL, NULL, 'No', 'Yes', 'The only days I\'m not available are Sundays and Tuesdays.', 'Canadian Citizen', 'Yes', 'Yes', '7465', 'English', NULL, 259, NULL, NULL),
(258, NULL, 'Mustache', 'The A Team\nAlice\nPretty Little Liars\n50/50\nSupernatural\nShooter\nOnce Upon A Time\nBirth of the Dragon\nIZOMBIE\nFifty Shades of Grey\nImpastor\nZoo\nWhen We Rise\nDead of Summer\nWonder\nShut Eye\nFrequency\nAngel in Training\nRiverdale\nLucifer\nThe Crossing\nThe Magicians\nTrial and Error\nValley of the Boom\nSearch and Destroy\nSiren\n', 'Apprentice Union', '27564', NULL, NULL, NULL, NULL, NULL, NULL, 'Showbiz Management  604-435-7469', 'IN HOPE, BC: Small Town People with Lots of Character Ages 35 to 60', NULL, NULL, NULL, '7783864775', '6045724647', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Myrna Buffie', '6043515440', 'gimyne59@telus.net', NULL, NULL, NULL, 'Yes', 'Yes', '15.5', '42', NULL, 'No', 'Yes', 'Available most days except Fridays and Saturdays', 'Canadian Citizen', 'Yes', 'Yes', '2050', 'English', NULL, 260, NULL, NULL),
(259, 'Traditional boys cut', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Gap 12-14', NULL, 'Extra for â€œThe Terrorâ€', NULL, NULL, NULL, '7789913876', '7789913876', NULL, 'Child', 'Teena Shinkawa', NULL, NULL, NULL, 'Peter Poon', '6047903846', 'poon.king.wai@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Available depending on schoolwork', 'Canadian Citizen', 'Yes', 'Yes', '6832', 'English', NULL, 261, NULL, NULL),
(260, 'Easy flexible med length cut', 'Blond', 'Cult\nThe 100 season 1,3,4,5\nSupernatural\nAltered Carbon\nOnce upon a time\nPercy Jackson 2\nTomorrow People\nFlash\nArrow\nIf I stay\nPower Rangers\nThe Magicians\n', 'Full union member since 2017 april\nEntertainment and show business performer since 2011', '69386', NULL, NULL, NULL, NULL, NULL, NULL, 'Keith Dallas of Dallas Talent. Phone number (604) 415-4783\nEmail agentkeithdallas@gmail.com', NULL, NULL, NULL, NULL, '7788296370', '7788296370', 'emodete@gmail.com', 'Adult', 'Keith Dallas/Dallas Talent', '(604) 415-4783', 'Devyn Neufeld', '+1 (604) 716-3700', 'Julie Peter ', '+1 (778) 953-1787', 'juliaauclair@ymail.com', 'Rene', '(778) 288-0891', NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Very flexible! \nMonday - Friday ', 'Canadian Citizen', 'Yes', 'Yes', '6291', 'Russian', 'English', 262, NULL, NULL),
(261, 'Varies', 'some', 'Percy Jackson 2 2012\nPackage deal - 2013\nBackstrom - 2013\nCedar Cove - 2013\nThe Killing - 2013\nGIRLFRIENDS GUIDE TO DIVORCE 2014 - 2017\nArrow 2014 - 2018\nFlash 2014 - 2018\nMotive - 2014\niZombie 2015 - 2017\nRoadies - 2015 \nFAIB Further Adventures In BabySitting 2015\nMistresses Season 3 - 2015 \nArmy of One 2015 \nCOBALT 2015\nBrain On Fire 2015\nBesties 2015\nLOOKINGLASS (AKA THE FRANKENSTEIN CODE) 2015\nSHIRT 2016\nFREQUENCY 2016 Season 1\nLegends 2016 - 2018\nThe 100 Seasons 1,3,4,5\nLucifer 2016 - 2017\nSupergirl 2017 - 2018\nAltered Carbon 2017 \nUNREAL - 2017\nWisdom OF The Crowd 2017\nRiverdale 2017 Season 2 \nLife Sentence 2017\nTHE HUNT/ Freaky Friday 2 2017\nSacred Lies 2018\nValley Of The Boom 2018\nTrial & Error 2018\nAlways Be My Maybe 2018\n', 'Full union member since 2017. ', '60386', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '778-829-6370', '778-829-6370', 'emodete@gmail.com', 'Adult', 'Tim (Carson Books and Records) (relationship: Employer)', '604-222-8787', 'Devyn Neufeld (relationship: family friend)', '604-716-3700', 'Julia Peter (mother)', '778-953-1787', 'juliaauclair@ymail.com', 'RenÃ¨', '778-288-0891', NULL, 'No', 'No', 'M', '36', NULL, 'No', 'Yes', 'full time, any time. 5 days a week or more \nPlease call to book\n\nThank you Kindly\n\nArt Peter', 'Canadian Citizen', 'No', 'Yes', '6291', 'Russian', 'English', 263, NULL, NULL),
(262, 'Blue', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asccreative', NULL, NULL, NULL, 'asd', 'adaa', NULL, 'Adult', NULL, NULL, NULL, NULL, 'freeman', '78787787878', 'freeman@info.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Please provide your availability details:\nPlease provide your availability details:\nPlease provide your availability details:\n', 'Canadian Citizen', 'Yes', 'Yes', '7898', 'English', NULL, 264, NULL, NULL),
(263, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, 'adsaa', 'asdsad', NULL, 'Adult', NULL, NULL, NULL, NULL, 'sadsa', 'asdas', 'sadsa@sadasdsa.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'asdasdsa', NULL, 265, NULL, NULL),
(264, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing, ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 266, NULL, NULL),
(265, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 267, NULL, NULL),
(266, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 268, NULL, NULL),
(267, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 269, NULL, NULL),
(268, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 270, NULL, NULL),
(269, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 271, NULL, NULL),
(270, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s\nHe does have a resume but trying to get the application to send so I\'ve removed and will send in an email ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 272, NULL, NULL),
(271, NULL, NULL, 'Puppy Prep Academy, Christmas Bells are ringing,  Also starred in a feature film \"Asher\" which you can find on youtube:  https://www.youtube.com/watch?v=uB1xBxCTUuI&t=30s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Small / Medium childs', NULL, 'Open casting call for Victoria Area kids in July that was posted', NULL, NULL, NULL, '250-882-4096', '250-882-4096', NULL, 'Child', 'Jacqui Kaese', '12507142555', 'Sandy Webster Worthy', '12509208343', 'Crystal Percival', '250-882-4096', 'gordcrys@telus.net', 'Gord Percival', '250-882-6015', 'gordcrys@telus.net', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 273, NULL, NULL);
INSERT INTO `performers_details` (`id`, `hairStyle`, `hairFacial`, `bgPerformerDetails`, `unionAffiliations`, `unionNum`, `visa_student`, `visa_international`, `visa_open`, `visa_film`, `visa_film_other`, `shirt`, `agentDetails`, `castingTitle`, `job`, `cdnCitizenship`, `visa`, `dayPhone`, `eveningPhone`, `emailSecondary`, `applicationFor`, `referral1_name`, `referral1_phone`, `referral2_name`, `referral2_phone`, `emergencyContact1_name`, `emergencyContact1_phone`, `emergencyContact1_email`, `emergencyContact2_name`, `emergencyContact2_phone`, `emergencyContact2_email`, `driversLicense`, `leftHandDrive`, `neck`, `chest`, `insea`, `resume`, `bgPerformer`, `availability`, `cdnCitizen`, `bcResident`, `taxes`, `sin`, `languageFirst`, `languageSecond`, `performer_id`, `created_at`, `updated_at`) VALUES
(272, NULL, NULL, 'A million little things', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Registered Nurses with Operating Room Experience', NULL, NULL, NULL, '7782401181', '7782401181', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Devon Mahussier', '6049967362', 'devonmahussier@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'variable but decently flexible', 'Canadian Citizen', 'Yes', 'Yes', '495', 'English', NULL, 274, NULL, NULL),
(273, NULL, NULL, 'Descendants 3 - cheerleader \nRichard Says Goodbye -student  ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Background actor ', NULL, NULL, NULL, '+1 07785330774', '+1 07785330774', 'thornswolfbane@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Elaine ', '2505950098', 'carolrichter22@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', NULL, 'Canadian Citizen', 'Yes', 'Yes', '5588', 'English', NULL, 275, NULL, NULL),
(274, 'Wavy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2365221987', '2365221987', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Phil clements', '7782517445', 'lendonlee@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', '3992', 'English', NULL, 276, NULL, NULL),
(275, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Military Type Men & Women ages 18-50 in Kamloops, BC Area', NULL, NULL, NULL, '2503181567', '2503181567', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Sherrie Humphrey', '2506721872', 'spinx_lady@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '7757', 'English', NULL, 277, NULL, NULL),
(276, NULL, 'No ', 'Chesapeake shores\nThe play date- Hallmark ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No ', NULL, NULL, NULL, '2507131114', '2504680201', NULL, 'Teen', 'Jacqui Kaese ', '2507142555', 'Brianna Wiens or Peter Kent ', NULL, 'Jayne Essig ', '2507131114', 'equinoxacres@shaw.ca', 'Denny Essig ', '2507146246', 'equinoxacres@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Available the dates indicated (July 1-15) ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English ', NULL, 278, NULL, NULL),
(277, 'Long', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Youth med/large', NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '2506619508', '2506619508', NULL, 'Child', NULL, NULL, NULL, NULL, 'Tabitha Fong ', '5506619508', 'tabicat10@shaw.ca', 'Thomas fong', '2506860179', 'bugged@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Whenever will make myself available ', 'Canadian Citizen', 'Yes', 'No', NULL, 'English ', NULL, 279, NULL, NULL),
(278, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6047106323', '6047106323', NULL, 'Adult', 'Bobbi Baker ', 'â€­1 (250) 213-7998â€¬', 'Christian Laub', 'â€­(778) 858-6496â€¬', 'Andrew Bourque ', 'â€­+1 (514) 773-4022â€¬', 'andrewbourque92@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, '30â€', NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English ', 'Polish ', 280, NULL, NULL),
(279, NULL, NULL, 'Packaged Deal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-9920', '-9920', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Codee Palmer', '-9919', 'santilli.astrid@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'Yes', 'ASAP', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 281, NULL, NULL),
(280, NULL, NULL, 'Packaged Deal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '17543028865', '17543028865', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Terry Lynn Krenn', '6043073809', 'tl.krenn@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Most weekdays, weekends and night.', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Italian and Spanish', 282, NULL, NULL),
(281, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '7788385717', '7788385717', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Glen Fullerton ', '7788385716', 'glen_fullerton@hotmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'Yes', '1355', 'Japanese', 'English', 283, NULL, NULL),
(282, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, NULL, NULL, NULL, 'Child', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 284, NULL, NULL),
(283, 'Curly ', NULL, 'Disney movie in Calgary December 2018 (diphtheria patient)  \nAlso SOC for hallmark movie just in time for Xmas 2016\nGracepoint (Fox TV) body double for child 2014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5/6 t', 'Debbie Mahood at Lucas talent 685-0345', 'No', NULL, NULL, NULL, '6042022857', '2506341771', NULL, 'Child', NULL, NULL, NULL, NULL, 'Michelle st louis', '6042247736', 'neiljamesparker@shaw.ca', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 285, NULL, NULL),
(284, 'Curly', NULL, 'Hallmark Just in Time for Christmas (SOC) in Victoria, Gracepoint - body double, Fox TV, Disney Movie \"Alaska\" (Oct 2018) in Calgary ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Youth S', 'Debbis Mahood Lucas Talent 604-685-0345', 'Kids ages 6-19 in Victoria,BC', NULL, NULL, NULL, '6042022857', '6042022857', NULL, 'Child', NULL, NULL, NULL, NULL, 'Neil Parker', '6045657240', 'neiljamesparker@shaw.ca', 'Michelle St Louis', '6042247736', 'michellestlouis2@gmail.com', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'July 1-15 in Victoria, outside of these dates he is available in Vancouver. Brother Tate Parker and father Neil Parker also experienced background performers', 'Canadian Citizen', 'Yes', 'Yes', '5528', 'English', NULL, 286, NULL, NULL),
(285, 'Crew Cut', 'Clean Shaven', 'Man in the High Castle, Supergirl, Riverdale, A Million Little Things, Skyscraper, Predator, The Good Doctor, Lucifer, I-zombie, ', 'Apprentice Union', 'AM- 29405', NULL, NULL, NULL, NULL, NULL, NULL, 'Showbiz Management (604) 435-7469', 'Japanese People', NULL, NULL, NULL, '7789810188', '7789810188', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Kiyoko Kumo', '604-277-0570', 'umesann@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', '15', '36', NULL, 'No', 'Yes', 'Any date with a couple days notice', 'Canadian Citizen', 'Yes', 'Yes', '9905', 'Japanese', 'English', 287, NULL, NULL),
(286, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kids ages 6-19 in Victoria,BC', NULL, NULL, NULL, '250-857-5698', '250-857-5698', 'Shelleyfrick@shaw.ca', 'Teen', 'Sandra Webster-Worthy', NULL, NULL, NULL, 'Shelley Frick', '250-857-5698', 'Shelleyfrick@shaw.ca', 'Carl Frick', '250-208-1501', 'carlfrick@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Available Up until July 12', 'Canadian Citizen', 'Yes', 'Yes', '8279', 'English', 'French', 288, NULL, NULL),
(287, 'Long with bangs', 'No', 'Featured Background in \'Love at First Dance\'\nBackground in \'Christmas Bells are Ringing\'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'small to medium  (slim but tall)', 'Coultish Management \nBarbara Coultish\n210 - 645 Fort Street\nVictoria BC  V8W 1G2\n250.382.2670\n', 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '250.383.4883', '250.383.4883', NULL, 'Child', 'Barbara Coultish', '250.382.2670', NULL, NULL, 'Laura Cooper', 'H -250.383.4883 / Mobile -250.516.4883 ', 'coultishmodels@telus.net', 'Cam Cooper', 'H -250.480.8052 /W-250.475.2669 / Mobile:-250.383.4883 ', 'cam@cooperpacific.ca', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Audrey\'s schedule is fairly flexible and we will try to make her available if you ask her to set.  She is not available on July 15th', 'Canadian Citizen', 'Yes', 'Yes', '1776', 'English', NULL, 289, NULL, NULL),
(288, NULL, NULL, 'Student Productions', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7785543772', '7785543772', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Anne Laflamme ', '6045819666', 'too@oldforanemail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Every day except Tuesdays', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 290, NULL, NULL),
(289, 'Curly ', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N/A', NULL, NULL, NULL, '6049161778', '6049161778', 'austenadele@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Tyrell Collins', '2506672133', 'tyrell194@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, '36', NULL, 'No', 'No', 'I run my own business in Victoria, I have flexible hours with some notice.', 'Canadian Citizen', 'Yes', 'Yes', '362', 'English', 'N/A', 291, NULL, NULL),
(290, 'Clean cut, ', 'N/A', 'Sonic the Hedgehog 2018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-11 00:00:00', NULL, 'Boys Ages 8 to 11 Years old on the Island', NULL, NULL, NULL, '2508842493', '2508842493', NULL, 'Child', 'Jacqui Kaese', '2507142555', NULL, NULL, 'Angela Friesen', '2508842493', 'anelephantstear@hotmail.com', 'Trevor Friesen', '2508823052', 'trevorfriesen123@gmail.com', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Available weekends and days. \nNights if needed, although I think thereâ€™s restrictions to this. ', 'Canadian Citizen', 'Yes', 'No', '6652', 'English', 'N/A', 292, NULL, NULL),
(291, NULL, 'No', 'Sonic the Hedgehog (1) , 4 summer weddings,(1)\nChesapeake Shores, (1)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Size 12', NULL, 'No', NULL, NULL, NULL, '2508842493', '25088423', NULL, 'Child', 'Jacqui Kaese -Spotlight Academy', '2507142555', 'Andrea Hughes ', '6043051074', 'Angie Friesen', '2508842493', 'anelephantstear@hotmail.com', 'Trevor Friesen', '2508823052', 'tfriesen123@gmail.com', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Monday through Sunday\n', 'Canadian Citizen', 'Yes', 'Yes', '6652', 'English', 'NA', 293, NULL, NULL),
(292, 'Straight', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '7782550141', '7782550141', NULL, 'Child', NULL, NULL, NULL, NULL, 'Emily Kivinen', '7782550141', 'emilykivinen@gmail.con', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 294, NULL, NULL),
(293, NULL, NULL, 'Delta Film Academy - Tripped Up', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Camp Kids', NULL, NULL, NULL, '6042404729', '6042404729', NULL, 'Teen', NULL, NULL, NULL, NULL, 'Nadine Bak', '6042404729', 'nqbak@telus.net', 'Andrew Bak', '6042402407', 'antiquus@telus.net', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Aug 12 - 19, 2018', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', NULL, 295, NULL, NULL),
(294, 'Long and curly', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Size 6', NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '6048452678', '6048452678', 'Amieg@hotmail.com', 'Child', NULL, NULL, NULL, NULL, 'Amie jalava', '6048452678', 'Amieg@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 296, NULL, NULL),
(295, 'wavy/curly', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'youth large', NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '250-858-8572', '250-858-8572', 'sharon.rubin.2005@gmail.com', 'Child', 'Tia Niedjalski', '250-893-5750', NULL, NULL, 'Sharon Rubin', '250-507-4131', 'sharon.rubin.2005@gmail.com', 'Paul Lancaster', '250-858-8572', 'paullancaster@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', NULL, 297, NULL, NULL),
(296, NULL, NULL, 'All in Madonna independent film shot in Victoria', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Background work or anything principle', NULL, NULL, NULL, '2508933153', '2508933153', 'jmteam@me.com', 'Teen', 'Jacqui Kaese Spotlight Academy ', '2507142555', NULL, NULL, 'Maria McGaghey', '2508933153', 'jmteam@me.com', 'John McGaghey', '2508893089', 'jmteam@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Usually always avaliable', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English ', NULL, 298, NULL, NULL),
(297, 'Long Bob', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Youth Size 12  - Adult Size Extra Small', NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '2505723761', '2505723761', 'kmorris77@live.ca', 'Child', 'Alan Hopkins', NULL, NULL, NULL, 'Kirsty Morris', '2505723761', 'kmorris77@live.ca', 'Craig Morris', '250-572-3294', 'cgm73@live.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Available all of July - August 2019', 'Canadian Citizen', 'Yes', 'No', '4002', 'English', NULL, 299, NULL, NULL),
(298, 'Short', 'Varrying', 'Thousands of shows since I started BG in 2012, a lot of SKF as well', 'Dgc,ia,Teamsters permits. Actra/ubcp full', '12299', NULL, NULL, NULL, NULL, NULL, NULL, 'Local Color', NULL, NULL, NULL, NULL, '17783185571', '7783185571', 'averized@gmail.com', 'Adult', 'Dale Bredeson (1st AD, not sure why you need references for bg?)', '6047874138', 'Alex Campbel (fellow actor)', '6046536533', 'Norman Klenman, grand father', '7788852412', 'nklenman@shaw.ca', NULL, NULL, NULL, 'Yes', 'No', '16', NULL, NULL, 'Yes', 'Yes', 'Full-time flexible', 'Canadian Citizen', 'Yes', 'Yes', '4003', 'English', 'Japanese', 300, NULL, NULL),
(299, 'long wavey', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '778-840-7522', '604-924-4451', 'paulahilliard@shaw.ca', 'Teen', 'Tara Fuhre', '604-671-7407', 'Rachel Attie', '604-831-2963', 'Paula Hilliard', '778-840-7522', 'paulahilliard@shaw.ca', 'Shawn Hilliard', '604-671-5202', 'shawn@somersetcustomehomes.ca', 'No', 'No', '12\"', '31 1/2\"', NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 301, NULL, NULL),
(300, NULL, 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '514-806-4724', '514-806-4724', NULL, 'Adult', 'Annie Walls', '1-604-816-7383', NULL, NULL, 'Sherry Ezzard', '250-319-8084', 'sezzard@hotmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', 'Full availability - Monday through Sunday', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 302, NULL, NULL),
(301, 'bob hair', NULL, 'Kim possible\n\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MVM Agency Brenda Wong  Mobile: 604-318-8104 Email: brenda@mvmagency.ca ', NULL, NULL, NULL, NULL, '778-887-8464', '778-887-8464', 'youkaien@gmail.com', 'Adult', 'Jason Furukawa 1st Assistant Director (Charmed)', '604-816-1567', 'Brenda Wong', '604-318-8104', 'Jason Furukawa', '604-816-1567', 'fujikurosawa@gmail.com', 'Akane Furukawa', '604-230-9316', 'neko-66@hotmail.co.jp', 'Yes', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'All days -extremely flexible schedule\n\nFrom morning till evening', 'Canadian Citizen', 'Yes', 'Yes', '7016', 'English', 'Japanese', 303, NULL, NULL),
(302, 'Curly', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8 or small ', NULL, 'Any role- she was made for the stage.', NULL, NULL, NULL, '7789773692', '7789773692', 'arielleboivin@gmail.com', 'Child', 'Paul Thompson', '(250) 216-0349', NULL, NULL, 'Jason Lelievre', '2505164265', 'jasonlelievre34@gmail.com', 'Sally Boivin', '2505142713', 'sallyboivin@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Full availability ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English ', NULL, 304, NULL, NULL),
(303, 'Waivy', 'Light', 'Pace Academy theatre 3rd year Both Shows - Christmas and Spring Show', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Background', NULL, NULL, NULL, '2508963189', '6048977001', 'linneadstevenson@gmail.com', 'Teen', 'Pace Academy ', '2508963189', 'Jody Stevenson', '2508963189', 'Linnea Stevenson', '2508963189', 'linneadstevenson@gmail.com', 'Jody Stevenson', '2508963189', 'jodynlinnea@gmail.com', 'No', 'No', '13.5 inch ', '40 inches ', NULL, 'No', 'Yes', 'Anytime ', 'Canadian Citizen', 'Yes', 'Yes', '1262', 'English', 'None', 305, NULL, NULL),
(304, 'Straight/natural wave', 'None', 'Shortland Street, New Zealand\n', NULL, NULL, 'No', 'Yes', 'No', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7787511716', '7787511716', 'ayla@aylaamano.com', 'Adult', 'Matthew Wilmot', '6421479360', 'Kelly Seabourne', '6493033849', 'Chako Amano', '64226574958', 'chakoamano@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Currently flexible / open availability ', 'Visa', 'Yes', 'Yes', '8860', 'English', NULL, 306, NULL, NULL),
(305, 'Straight ', 'None ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-10 00:00:00', NULL, 'No ', NULL, NULL, NULL, '6047687108', '6047687108', NULL, 'Child', NULL, '6047687108', NULL, '6047687108', 'Desire Colley ', '6047687108', 'desirewickenden@gmail.com', 'Jordan. colley ', '6047981107', 'jordancolley29@gmail.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Anytime ', 'Canadian Citizen', 'Yes', 'Yes', '5300', 'English ', NULL, 307, NULL, NULL),
(306, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6043582152', '7788867904', 'kaoru1.kojima@gmail.com', 'Child', 'Shinobu Kojima ', '7788867904', NULL, NULL, 'Kaoru Kojima', '7788398107', 'kaoru1.kojima@gmail.com', 'Shinobu Kojima ', '7788867904', NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'No', '3200', 'English ', 'Japanese ', 308, NULL, NULL),
(307, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7788398107', '7788398107', NULL, 'Child', NULL, NULL, NULL, NULL, 'kaoru kojima', '7788398107', 'kaoru1.kojima@gmail.com', 'SHINOBU KOJIMA', '7788867904', NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '3200', 'ENGLISH', 'JAPANESE', 309, NULL, NULL),
(308, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '778-895-9489', '778-895-9489', 'dakkila@gmail.com', 'Adult', 'anne lin', '778-928-7325', 'sumi hamada ', '604-618-0254', 'dakkila Ramos', '778-885-8186', 'dakkila@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'my  work schedule is same as school schedule, available after 2pm on weekday, I\'m off weekend, holiday and winter, spring and summer vacation. ', 'Canadian Citizen', 'Yes', 'Yes', '1674', 'Japaniese ', 'Engligh ', 310, NULL, NULL),
(309, NULL, NULL, 'Colbalt\nProof\nFAIB\nFlash\nThe Advocate\nRoadies\niZombie\nBates Motel\nThe 100\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'I would want more principal or commercial work but background is fine to start! ', NULL, NULL, NULL, '2369880996', '2369880996', 'vancityprince.hyf@gmail.com', 'Adult', 'Lisa Towers', '6043294454', NULL, NULL, 'Kumba Agnes', '7788790692', 'kumbaagnes@hotmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Iâ€™m free Monday-Thursday \n', 'Canadian Citizen', 'Yes', 'Yes', '4427', 'English ', 'Creole ', 311, NULL, NULL),
(310, NULL, NULL, 'Colbalt\nProof\nFAIB\nFlash\nThe Advocate\nRoadies\niZombie\nBates Motel\nThe 100\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'I would want more principal or commercial work but background is fine to start! ', NULL, NULL, NULL, '2369880996', '2369880996', 'vancityprince.hyf@gmail.com', 'Adult', 'Lisa Towers', '6043294454', NULL, NULL, 'Kumba Agnes', '7788790692', 'kumbaagnes@hotmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Iâ€™m free Monday-Thursday \n', 'Canadian Citizen', 'Yes', 'Yes', '4427', 'English ', 'Creole ', 312, NULL, NULL),
(311, 'Straight bob', 'None', 'Salvation 2, Dead Pool2, Hallmarks, Arrow, The Good Doctor, The Flash, The Crossing, Skyscraper, Riverdale, Once Upon a Time, Haily Dean 6, Girl in the Bathtub,,Valley of the Boom, Harry and Megan,â€Royal Romanceâ€, Marrying Mr Darcy,  Spontaneaous, Darrow &Darrow 2, Noelle, UnReal 4, The Simone Biles story, Colony 3, Eggplant Emoji, Siren 1, iZombie, The Exorcist, Life Sentence, Supergirl, Supernatural, The Arrangement, Xfiles, Take Two, ', 'Background Union Member ', '4434', NULL, NULL, NULL, NULL, NULL, NULL, 'Lucas Talent \n1238 Homer St \n604-685-0345  ', NULL, NULL, NULL, NULL, '6047290727', '6047290727', 'bonbahar@gmail.com', 'Adult', 'Derek Nordick Lucas Talent ', 'â€­+1 (604) 318-4240â€¬', 'Jen Lee Lucas Talent ', 'â€­+1 (604) 808-6271â€¬', 'Rav Grewal ( Husband) ', '6047817721', 'rgrewal.3d@gmail.com', 'Fernanda Gracco ', 'â€­(778) 889-2610â€¬', 'feberto@icloud.com', 'Yes', 'Yes', '14.5', '35-37', NULL, 'No', 'Yes', 'I am available FT. ', 'Canadian Citizen', 'Yes', 'Yes', '237', 'Farsi', 'English ', 313, NULL, NULL),
(312, 'Straight ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Any role', NULL, NULL, NULL, '7789960494', '7789960494', NULL, 'Adult', 'Simona', '7783207241', 'Maryam', '6047797761', 'Maryam ', '6047797761', 'mojganpolosi@yahoo.ca', 'Zara', '7788617215', 'zahraehtemam@yahoo.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '1223', 'Farsi/', 'English ', 314, NULL, NULL),
(313, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Background ', NULL, NULL, NULL, '7789960494', '7789960494', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Simona', '7783207241', 'spring1870casting@gmail.com', 'Zara', '7788617215', 'zaraehtemam@yahoo.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Monday/Tuesday/Wednesday/Thursday/Friday/Sunday and night shift', 'Canadian Citizen', 'Yes', 'Yes', '1223', 'Farsi', 'English ', 315, NULL, NULL),
(314, 'Straight', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open Casting Call Agassiz BC', NULL, NULL, NULL, '2368873136', '2368873136', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Brennan Bateman', '7752379086', 'uniquelyyours.bc@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'open availability', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 316, NULL, NULL),
(315, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7788652500', '7788652500', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Rajdeep', '6048314331', 'rajarjun224@gmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'Punjabi hindi English ', 'English ', 317, NULL, NULL),
(316, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', 'No', 'No', NULL, NULL, NULL, 'Singer /songwriter /modal', NULL, NULL, NULL, '2368803705', '2368803705', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Paska Aber ', '256772386221', 'paskaber@yahoo.com', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL, NULL, 'Yes', 'No', 'Am available Monday to Friday \nAround 8:00am to 7:00pm', 'Visa', 'Yes', 'Yes', '3602', 'Luo', 'English', 318, NULL, NULL),
(317, 'slightly layered', 'na', 'Over 300 days on set as a Background Performer since 2015\n2 days on set as an Actor\nContinuity on Bates Motel S4 (Mental Patient)\nContinuity on The 100 S3, 4, 5 (Blue Cliff Ambassador http://the100.wikia.com/wiki/File:Blue_Cliff_Ambassador.jpg)\nContinuity on The Magicians (Professor/Librarian)\nContinuity on The Descendants 2 (Sea Hag https://www.youtube.com/watch?v=4Vv-zcAoer8)\nContinuity on Riverdale (teacher) \nContinuity on Deadpool 2 (Mutant Prisoner - 20 days on set) \nContinuity on Sacred Lies (Cult Member)\nContinuity on Salvation (Chief Justice of the Supreme Court)\nContinuity on The Women of Marwen (trailer trash Mom)\nWhen We Rise played Clerk of the Court (Pamela Talkin)\nWhen We Rise played Nun\nIzombie (White Zombie)\nSupernatural played Nun\nand many, many other parts including everything from high end gala guest to homeless addict', 'UBCP/ACTRA', '60708', NULL, NULL, NULL, NULL, NULL, NULL, 'ShowBiz Management', NULL, NULL, NULL, NULL, '778 228-1072', '778 228-1072', NULL, 'Adult', 'Roland Pointer ShowBiz Management', '604 780-0566', 'James Forsyth BCF Casting', '604 817-2272', 'Hanoch Pearce son', '778 836-0992', 'hpearce888@gmail.com', 'Judi Wilson sister', '778 689-6879', 'wilson.judi2010@gmail.com', 'Yes', 'Yes', '12\"', NULL, NULL, 'Yes', 'Yes', 'I have full time availability', 'Canadian Citizen', 'Yes', 'Yes', '5064', 'English', NULL, 319, NULL, NULL),
(318, NULL, NULL, 'The Rhino Brothers (film)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-467-8963', '604-467-8963', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Marcel Renaud', '604-607-4795', 'mjrenaud@shaw.ca', 'Margaret Stevenson', '604-463-9432', 'margstevenson@hotmail.com', 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Available full week of March 25 to 29', 'Canadian Citizen', 'Yes', 'Yes', '7960', 'English', NULL, 320, NULL, NULL),
(319, 'Clean cut', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Xs', NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '2508186688', '2508186688', 'gccmandy@gmail.com', 'Child', NULL, NULL, NULL, NULL, 'Mandy Lee', '2508186688', 'ml123456@gmail.com', 'Kevin Pinsonneault', '7786868720', 'rppempire@yahoo.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'July 1-15', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', 'French', 321, NULL, NULL),
(320, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6047012588', '6047012588', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Josh burr ', '6047951129', 'joshandbecca@hotmail.ca', 'Don loeppky', '6047033233', NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', 'Evenings and anytime on weekends. \nFor week day hours I would need to give one weeks notice to my current employer ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 322, NULL, NULL),
(321, 'long & wavey', 'No', 'Continuity character on Riverdale, the 100s, many many movies, lots of music videos. Been working in the film industry for lots of years as a special skills motorcyclist.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-354-5016', '604-354-5016', NULL, 'Adult', ' Coco Bikadoroff @ Double Agents', '604-805-2390', 'Rob Raco @ Riverdale', '323-681-3101', 'Blaine Connolly', '604-354-5016', 'blaine.connolly88@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'I work freelance for myself and create my own schedule. ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 323, NULL, NULL),
(322, 'Wavy bob', 'n/a', 'Supernatural, A Series of Unfortunate Events, Riverdale', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-315-5576', '604-315-5576', 'janetmccairns@telus.net', 'Teen', 'LA Hiltz', '604-839-5833', 'Lorne Davidson', '604-290-4531', 'Janet McCairns', '604-315-5576', 'janetmccairns@telus.net', 'Lorne Davidson', '6042904531', 'dmb_live@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Always available', 'Canadian Citizen', 'Yes', 'Yes', '6851', 'English', NULL, 324, NULL, NULL),
(323, NULL, 'none', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2506676711', '2506676711', 'benjaminloyst@gmail.com', 'Adult', 'Bahar', '7789960494', 'Simona', '7783207241', 'Lynn Tissington', '12506199992', 'lynntissington@sd68.bc.ca', 'David Loyst', '16047606787', 'Davidloyst@shaw.ca', 'No', 'No', '14.5', NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '7165', 'English', 'French', 325, NULL, NULL),
(324, NULL, 'none', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2506676711', '2506676711', 'benjaminloyst@gmail.com', 'Adult', 'Bahar', '7789960494', 'Simona', '7783207241', 'Lynn Tissington', '12506199992', 'lynntissington@sd68.bc.ca', 'David', '1 604 760 6787', 'davidloyst@shaw.ca', 'No', 'No', '14.5', NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '7165', 'English', 'French', 326, NULL, NULL),
(325, NULL, NULL, 'A Dogâ€™s Way Home', NULL, NULL, 'No', 'No', 'No', 'Yes', NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6047936054', '6047936054', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Derek schmidt', '6047121987', 'daschmidt217@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'I am pretty flexible', 'Visa', 'Yes', 'Yes', '3051', 'Filipino', 'English', 327, NULL, NULL),
(326, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No. Applicant just turned 18 years old.', NULL, NULL, NULL, '7783196133', '7783196133', 'onehallfamily@gmail.com', 'Teen', NULL, NULL, NULL, NULL, 'Annalice Hall ', '7783196133', 'annalice.hall@gmail.com', 'Milon Hall ', '7787107251', 'onehallfamily@gmail.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Currently in Grade 12th, very flexible with schedule. Available to work any time and any day. Thank you.', 'Canadian Citizen', 'Yes', 'No', NULL, 'Filipino', 'English', 328, NULL, NULL),
(327, NULL, 'Yes', 'The Twilight zone, Surveillance, Altered Carbon', NULL, NULL, 'No', 'Yes', 'No', 'Yes', NULL, NULL, 'Talent bid', NULL, NULL, NULL, NULL, '3', '7', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Patrick or Catherine Gambier', '+33 164077738', 'bernard_77_@live.fr', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Tuesday, Wednesday, Thursday (until 5pm), Friday, Saturday (until 3.30pm) , Sunday (until 5pm)', 'Visa', 'Yes', 'Yes', '975', 'French', 'English', 329, NULL, NULL),
(328, NULL, NULL, 'Background Audience - Tensai Shimura Doubutsu-en, and couple other Tv shows', NULL, NULL, 'No', 'No', 'No', 'No', 'I have a Canadian PR (but not a Canadian Citizen)', NULL, NULL, 'Japanese People', NULL, NULL, NULL, '5pm', '5pm', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Hilda K', '+1(604)230-4085', 'hilda.kisielewski@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', NULL, 'Visa', 'Yes', 'Yes', '398', 'Indonesian', 'Japanese', 330, NULL, NULL),
(329, 'long straight', 'none except eyebrows', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Asian People', NULL, NULL, NULL, '6043521447', '6043521447', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Jessica Jen', '6045617205', 'jessica1739jen@gmail.com', NULL, NULL, NULL, 'No', 'No', '11 inches around', '31 inches around', NULL, 'No', 'No', 'Free on April 19th - 23rd, 29th, May 1st, 3rd - 6', 'Canadian Citizen', 'Yes', 'Yes', '4501', 'English', 'French or Mandarin', 331, NULL, NULL),
(330, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', 'Yes', 'Yes', NULL, NULL, 'Bahar\n7789960494\nspring1870casting@gmail.com', NULL, NULL, NULL, NULL, '6044451597', '6044451597', NULL, 'Adult', 'Bahar ', '7789960494', 'Simona', '7789960494', 'Kritika Arora', '7789950292', 'arorakritika92@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', NULL, 'Visa', 'Yes', 'Yes', '557', 'Punjabi', 'English', 332, NULL, NULL),
(331, NULL, 'sometimes, upon request, grey', 'Deadpool2\nAltered Carbon\nA Series of Unfortunate Events\nHardpowder\n', NULL, 'UBCP/ACTRA', NULL, NULL, NULL, NULL, NULL, NULL, 'Carrier Talent, Darren Boidman, 604-683-8641\nTwins Plus, 604-364-6656', NULL, NULL, NULL, NULL, '6047625768', '6047625768', NULL, 'Adult', 'Brent Fidler, Theatre Crossing', '604-720-6424', 'Marco Holbein, Speedbump Theatre', '604-834-6456', 'Bill Croft', '604-762-5768', 'billcroft69@hotmail.com', NULL, NULL, NULL, 'Yes', 'Yes', '17', '42', NULL, 'Yes', 'Yes', '2019-07-24 00:00:00', 'Canadian Citizen', 'Yes', 'Yes', '149', 'english', NULL, 333, NULL, NULL),
(332, 'straight', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IN HOPE, BC: Small Town People with Lots of Character Ages 20-80', NULL, NULL, NULL, '604 850 4736', '6048504736', 'billshibicky@shaw.ca', 'Adult', 'william shibicky', '6048504736', 'william shibicky', '6048504736', 'Louise Shibicky', '604 302 1927', 'lshibicky@shaw.ca', 'william shibicky', '6048504736', 'billshibicky@shaw.ca', 'Yes', 'Yes', '18', '46', NULL, 'Yes', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'english', '-', 334, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `performer_agendas`
--

CREATE TABLE `performer_agendas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `agenda` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `performer_availability`
--

CREATE TABLE `performer_availability` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `availability` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `performer_availability`
--

INSERT INTO `performer_availability` (`id`, `availability`, `status_color`, `created_at`, `updated_at`) VALUES
(1, 'Available', '#4D9E39', NULL, NULL),
(2, 'Not Available', '#000000', NULL, NULL),
(3, 'Partial Avail', '#95D1A8', NULL, NULL),
(4, 'Full Avail', '#1EA247', NULL, NULL),
(5, 'Booked Night Shoot', '#7E0000', NULL, NULL),
(6, '1st Refusal', '#FFCE00', NULL, NULL),
(7, '2nd Refusal', '#FF1DD4', NULL, NULL),
(8, 'Pre-booked', '#000000', NULL, NULL),
(9, 'Booked', '#FF3434', NULL, NULL),
(10, 'Main Unit Episode', '#000000', NULL, NULL),
(11, 'Second Unit Episode', '#000000', NULL, NULL),
(12, 'Stand-In', '#000000', NULL, NULL),
(13, 'Wardrobe Fitting', '#000000', NULL, NULL),
(14, 'Camera Test', '#000000', NULL, NULL),
(15, 'Still Shoot', '#000000', NULL, NULL),
(16, 'Picture Cast', '#000000', NULL, NULL),
(17, 'Night Shoot', '#000000', NULL, NULL),
(18, 'Half-day', '#000000', NULL, NULL),
(19, 'Pending', '#1DBAFF', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `performer_portfolios`
--

CREATE TABLE `performer_portfolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_video` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `performer_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `performer_portfolios`
--

INSERT INTO `performer_portfolios` (`id`, `portfolio_title`, `description`, `is_video`, `performer_id`, `created_at`, `updated_at`) VALUES
(1, 'MISC', NULL, 'no', 2, '2019-07-13 00:59:38', '2019-07-13 00:59:38'),
(2, 'MISC', NULL, 'no', 4, '2019-07-13 01:00:14', '2019-07-13 01:00:14'),
(3, 'MISC', NULL, 'no', 5, '2019-07-13 01:00:40', '2019-07-13 01:00:40'),
(4, 'MISC', NULL, 'no', 6, '2019-07-13 01:01:14', '2019-07-13 01:01:14'),
(5, 'MISC', NULL, 'no', 7, '2019-07-13 01:02:11', '2019-07-13 01:02:11'),
(6, 'MISC', NULL, 'no', 8, '2019-07-13 01:02:40', '2019-07-13 01:02:40'),
(7, 'MISC', NULL, 'no', 9, '2019-07-13 01:03:01', '2019-07-13 01:03:01'),
(8, 'MISC', NULL, 'no', 10, '2019-07-13 01:03:48', '2019-07-13 01:03:48'),
(9, 'MISC', NULL, 'no', 11, '2019-07-13 01:03:59', '2019-07-13 01:03:59'),
(10, 'MISC', NULL, 'no', 12, '2019-07-13 01:04:18', '2019-07-13 01:04:18'),
(11, 'MISC', NULL, 'no', 13, '2019-07-13 01:05:06', '2019-07-13 01:05:06'),
(12, 'MISC', NULL, 'no', 14, '2019-07-13 01:05:45', '2019-07-13 01:05:45'),
(13, 'MISC', NULL, 'no', 15, '2019-07-13 01:06:43', '2019-07-13 01:06:43'),
(14, 'MISC', NULL, 'no', 16, '2019-07-13 01:07:08', '2019-07-13 01:07:08'),
(15, 'MISC', NULL, 'no', 18, '2019-07-13 01:07:36', '2019-07-13 01:07:36'),
(16, 'MISC', NULL, 'no', 19, '2019-07-13 01:08:01', '2019-07-13 01:08:01'),
(17, 'MISC', NULL, 'no', 20, '2019-07-13 01:08:19', '2019-07-13 01:08:19'),
(18, 'MISC', NULL, 'no', 22, '2019-07-13 01:08:38', '2019-07-13 01:08:38'),
(19, 'MISC', NULL, 'no', 23, '2019-07-13 01:09:06', '2019-07-13 01:09:06'),
(20, 'MISC', NULL, 'no', 24, '2019-07-13 01:09:54', '2019-07-13 01:09:54'),
(21, 'MISC', NULL, 'no', 25, '2019-07-13 01:10:17', '2019-07-13 01:10:17'),
(22, 'MISC', NULL, 'no', 26, '2019-07-13 01:10:32', '2019-07-13 01:10:32'),
(23, 'MISC', NULL, 'no', 27, '2019-07-13 01:10:39', '2019-07-13 01:10:39'),
(24, 'MISC', NULL, 'no', 28, '2019-07-13 01:10:49', '2019-07-13 01:10:49'),
(25, 'MISC', NULL, 'no', 29, '2019-07-13 01:11:39', '2019-07-13 01:11:39'),
(26, 'MISC', NULL, 'no', 30, '2019-07-13 01:11:55', '2019-07-13 01:11:55'),
(27, 'MISC', NULL, 'no', 31, '2019-07-13 01:12:18', '2019-07-13 01:12:18'),
(28, 'MISC', NULL, 'no', 32, '2019-07-13 01:12:26', '2019-07-13 01:12:26'),
(29, 'MISC', NULL, 'no', 33, '2019-07-13 01:13:15', '2019-07-13 01:13:15'),
(30, 'MISC', NULL, 'no', 34, '2019-07-13 01:13:38', '2019-07-13 01:13:38'),
(31, 'MISC', NULL, 'no', 35, '2019-07-13 01:13:54', '2019-07-13 01:13:54'),
(32, 'MISC', NULL, 'no', 38, '2019-07-13 01:14:27', '2019-07-13 01:14:27'),
(33, 'MISC', NULL, 'no', 40, '2019-07-13 01:14:46', '2019-07-13 01:14:46'),
(34, 'MISC', NULL, 'no', 41, '2019-07-13 01:15:11', '2019-07-13 01:15:11'),
(35, 'MISC', NULL, 'no', 43, '2019-07-13 01:15:55', '2019-07-13 01:15:55'),
(36, 'MISC', NULL, 'no', 44, '2019-07-13 01:16:43', '2019-07-13 01:16:43'),
(37, 'MISC', NULL, 'no', 46, '2019-07-13 01:23:07', '2019-07-13 01:23:07'),
(38, 'MISC', NULL, 'no', 47, '2019-07-13 01:23:24', '2019-07-13 01:23:24'),
(39, 'MISC', NULL, 'no', 48, '2019-07-13 01:23:42', '2019-07-13 01:23:42'),
(40, 'MISC', NULL, 'no', 49, '2019-07-13 01:25:10', '2019-07-13 01:25:10'),
(41, 'MISC', NULL, 'no', 50, '2019-07-13 01:26:07', '2019-07-13 01:26:07'),
(42, 'MISC', NULL, 'no', 51, '2019-07-13 01:26:25', '2019-07-13 01:26:25'),
(43, 'MISC', NULL, 'no', 52, '2019-07-13 01:26:40', '2019-07-13 01:26:40'),
(44, 'MISC', NULL, 'no', 53, '2019-07-13 01:26:47', '2019-07-13 01:26:47'),
(45, 'MISC', NULL, 'no', 54, '2019-07-13 01:27:09', '2019-07-13 01:27:09'),
(46, 'MISC', NULL, 'no', 55, '2019-07-13 01:27:33', '2019-07-13 01:27:33'),
(47, 'MISC', NULL, 'no', 56, '2019-07-13 01:28:24', '2019-07-13 01:28:24'),
(48, 'MISC', NULL, 'no', 57, '2019-07-13 01:29:14', '2019-07-13 01:29:14'),
(49, 'MISC', NULL, 'no', 58, '2019-07-13 01:29:32', '2019-07-13 01:29:32'),
(50, 'MISC', NULL, 'no', 59, '2019-07-13 01:29:52', '2019-07-13 01:29:52'),
(51, 'MISC', NULL, 'no', 60, '2019-07-13 01:30:02', '2019-07-13 01:30:02'),
(52, 'MISC', NULL, 'no', 62, '2019-07-13 01:30:32', '2019-07-13 01:30:32'),
(53, 'MISC', NULL, 'no', 63, '2019-07-13 01:30:42', '2019-07-13 01:30:42'),
(54, 'Title Gallery', 'asd', 'no', 1, '2019-07-13 01:31:31', '2019-07-13 01:31:31'),
(55, 'MISC', NULL, 'no', 64, '2019-07-13 01:31:33', '2019-07-13 01:31:33'),
(56, 'MISC', NULL, 'no', 65, '2019-07-13 01:32:10', '2019-07-13 01:32:10'),
(57, 'MISC', NULL, 'no', 66, '2019-07-13 01:32:27', '2019-07-13 01:32:27'),
(58, 'MISC', NULL, 'no', 67, '2019-07-13 01:33:26', '2019-07-13 01:33:26'),
(59, 'MISC', NULL, 'no', 68, '2019-07-13 01:33:47', '2019-07-13 01:33:47'),
(60, 'MISC', NULL, 'no', 69, '2019-07-13 01:33:53', '2019-07-13 01:33:53'),
(61, 'MISC', NULL, 'no', 70, '2019-07-13 01:34:13', '2019-07-13 01:34:13'),
(62, 'MISC', NULL, 'no', 71, '2019-07-13 01:34:33', '2019-07-13 01:34:33'),
(63, 'MISC', NULL, 'no', 72, '2019-07-13 01:34:51', '2019-07-13 01:34:51'),
(64, 'MISC', NULL, 'no', 73, '2019-07-13 01:35:19', '2019-07-13 01:35:19'),
(65, 'MISC', NULL, 'no', 74, '2019-07-13 01:35:32', '2019-07-13 01:35:32'),
(66, 'MISC', NULL, 'no', 75, '2019-07-13 01:36:26', '2019-07-13 01:36:26'),
(67, 'MISC', NULL, 'no', 76, '2019-07-13 01:36:54', '2019-07-13 01:36:54'),
(68, 'MISC', NULL, 'no', 77, '2019-07-13 01:37:21', '2019-07-13 01:37:21'),
(69, 'MISC', NULL, 'no', 78, '2019-07-13 01:37:42', '2019-07-13 01:37:42'),
(70, 'MISC', NULL, 'no', 79, '2019-07-13 01:37:56', '2019-07-13 01:37:56'),
(71, 'MISC', NULL, 'no', 80, '2019-07-13 01:38:22', '2019-07-13 01:38:22'),
(72, 'MISC', NULL, 'no', 81, '2019-07-13 01:38:47', '2019-07-13 01:38:47'),
(73, 'MISC', NULL, 'no', 82, '2019-07-13 01:40:28', '2019-07-13 01:40:28'),
(74, 'MISC', NULL, 'no', 83, '2019-07-13 01:40:36', '2019-07-13 01:40:36'),
(75, 'MISC', NULL, 'no', 84, '2019-07-13 01:40:42', '2019-07-13 01:40:42'),
(76, 'MISC', NULL, 'no', 85, '2019-07-13 01:40:54', '2019-07-13 01:40:54'),
(77, 'MISC', NULL, 'no', 86, '2019-07-13 01:41:04', '2019-07-13 01:41:04'),
(78, 'MISC', NULL, 'no', 87, '2019-07-13 01:41:35', '2019-07-13 01:41:35'),
(79, 'MISC', NULL, 'no', 88, '2019-07-13 01:42:16', '2019-07-13 01:42:16'),
(80, 'MISC', NULL, 'no', 89, '2019-07-13 01:43:06', '2019-07-13 01:43:06'),
(81, 'MISC', NULL, 'no', 91, '2019-07-13 01:43:28', '2019-07-13 01:43:28'),
(82, 'MISC', NULL, 'no', 92, '2019-07-13 01:43:50', '2019-07-13 01:43:50'),
(83, 'MISC', NULL, 'no', 93, '2019-07-13 01:44:13', '2019-07-13 01:44:13'),
(84, 'MISC', NULL, 'no', 94, '2019-07-13 01:45:01', '2019-07-13 01:45:01'),
(85, 'MISC', NULL, 'no', 95, '2019-07-13 01:45:14', '2019-07-13 01:45:14'),
(86, 'MISC', NULL, 'no', 96, '2019-07-13 01:45:23', '2019-07-13 01:45:23'),
(87, 'MISC', NULL, 'no', 97, '2019-07-13 01:47:17', '2019-07-13 01:47:17'),
(88, 'MISC', NULL, 'no', 98, '2019-07-13 01:48:21', '2019-07-13 01:48:21'),
(89, 'MISC', NULL, 'no', 99, '2019-07-13 01:48:52', '2019-07-13 01:48:52'),
(90, 'MISC', NULL, 'no', 100, '2019-07-13 01:48:59', '2019-07-13 01:48:59'),
(91, 'MISC', NULL, 'no', 101, '2019-07-13 01:49:23', '2019-07-13 01:49:23'),
(92, 'MISC', NULL, 'no', 102, '2019-07-13 01:49:36', '2019-07-13 01:49:36'),
(93, 'MISC', NULL, 'no', 103, '2019-07-13 01:50:02', '2019-07-13 01:50:02'),
(94, 'MISC', NULL, 'no', 104, '2019-07-13 01:50:30', '2019-07-13 01:50:30'),
(95, 'MISC', NULL, 'no', 105, '2019-07-13 01:50:38', '2019-07-13 01:50:38'),
(96, 'MISC', NULL, 'no', 106, '2019-07-13 01:50:46', '2019-07-13 01:50:46'),
(97, 'MISC', NULL, 'no', 107, '2019-07-13 01:51:00', '2019-07-13 01:51:00'),
(98, 'MISC', NULL, 'no', 108, '2019-07-13 01:51:31', '2019-07-13 01:51:31'),
(99, 'MISC', NULL, 'no', 109, '2019-07-13 01:52:18', '2019-07-13 01:52:18'),
(100, 'MISC', NULL, 'no', 110, '2019-07-13 01:52:56', '2019-07-13 01:52:56'),
(101, 'MISC', NULL, 'no', 111, '2019-07-13 01:53:32', '2019-07-13 01:53:32'),
(102, 'MISC', NULL, 'no', 112, '2019-07-13 01:54:01', '2019-07-13 01:54:01'),
(103, 'MISC', NULL, 'no', 113, '2019-07-13 01:54:48', '2019-07-13 01:54:48'),
(104, 'MISC', NULL, 'no', 114, '2019-07-13 01:55:32', '2019-07-13 01:55:32'),
(105, 'MISC', NULL, 'no', 115, '2019-07-13 01:56:03', '2019-07-13 01:56:03'),
(106, 'MISC', NULL, 'no', 116, '2019-07-13 01:57:09', '2019-07-13 01:57:09'),
(107, 'MISC', NULL, 'no', 117, '2019-07-13 01:57:30', '2019-07-13 01:57:30'),
(108, 'MISC', NULL, 'no', 118, '2019-07-13 01:57:52', '2019-07-13 01:57:52'),
(109, 'MISC', NULL, 'no', 119, '2019-07-13 01:58:15', '2019-07-13 01:58:15'),
(110, 'MISC', NULL, 'no', 120, '2019-07-13 01:58:27', '2019-07-13 01:58:27'),
(111, 'MISC', NULL, 'no', 121, '2019-07-13 01:58:36', '2019-07-13 01:58:36'),
(112, 'MISC', NULL, 'no', 122, '2019-07-13 01:59:01', '2019-07-13 01:59:01'),
(113, 'MISC', NULL, 'no', 123, '2019-07-13 01:59:25', '2019-07-13 01:59:25'),
(114, 'MISC', NULL, 'no', 124, '2019-07-13 01:59:57', '2019-07-13 01:59:57'),
(115, 'MISC', NULL, 'no', 125, '2019-07-13 02:00:29', '2019-07-13 02:00:29'),
(116, 'MISC', NULL, 'no', 126, '2019-07-13 02:01:31', '2019-07-13 02:01:31'),
(117, 'MISC', NULL, 'no', 127, '2019-07-13 02:02:54', '2019-07-13 02:02:54'),
(118, 'MISC', NULL, 'no', 128, '2019-07-13 02:04:05', '2019-07-13 02:04:05'),
(119, 'MISC', NULL, 'no', 129, '2019-07-13 02:05:22', '2019-07-13 02:05:22'),
(120, 'MISC', NULL, 'no', 130, '2019-07-13 02:05:46', '2019-07-13 02:05:46'),
(121, 'MISC', NULL, 'no', 131, '2019-07-13 02:05:54', '2019-07-13 02:05:54'),
(122, 'MISC', NULL, 'no', 132, '2019-07-13 02:06:39', '2019-07-13 02:06:39'),
(123, 'MISC', NULL, 'no', 133, '2019-07-13 02:07:36', '2019-07-13 02:07:36'),
(124, 'MISC', NULL, 'no', 134, '2019-07-13 02:07:56', '2019-07-13 02:07:56'),
(125, 'MISC', NULL, 'no', 135, '2019-07-13 02:08:26', '2019-07-13 02:08:26'),
(126, 'MISC', NULL, 'no', 136, '2019-07-13 02:08:44', '2019-07-13 02:08:44'),
(127, 'MISC', NULL, 'no', 137, '2019-07-13 02:09:12', '2019-07-13 02:09:12'),
(128, 'MISC', NULL, 'no', 138, '2019-07-13 02:09:31', '2019-07-13 02:09:31'),
(129, 'MISC', NULL, 'no', 139, '2019-07-13 02:10:17', '2019-07-13 02:10:17'),
(130, 'MISC', NULL, 'no', 140, '2019-07-13 02:10:32', '2019-07-13 02:10:32'),
(131, 'MISC', NULL, 'no', 141, '2019-07-13 02:53:39', '2019-07-13 02:53:39'),
(132, 'MISC', NULL, 'no', 142, '2019-07-13 02:54:16', '2019-07-13 02:54:16'),
(133, 'MISC', NULL, 'no', 143, '2019-07-13 02:54:48', '2019-07-13 02:54:48'),
(134, 'MISC', NULL, 'no', 144, '2019-07-13 02:55:06', '2019-07-13 02:55:06'),
(135, 'MISC', NULL, 'no', 145, '2019-07-13 02:55:28', '2019-07-13 02:55:28'),
(136, 'MISC', NULL, 'no', 146, '2019-07-13 02:55:43', '2019-07-13 02:55:43'),
(137, 'MISC', NULL, 'no', 147, '2019-07-13 02:56:13', '2019-07-13 02:56:13'),
(138, 'MISC', NULL, 'no', 148, '2019-07-13 02:56:48', '2019-07-13 02:56:48'),
(139, 'MISC', NULL, 'no', 150, '2019-07-13 02:56:56', '2019-07-13 02:56:56'),
(140, 'MISC', NULL, 'no', 151, '2019-07-13 02:57:15', '2019-07-13 02:57:15'),
(141, 'MISC', NULL, 'no', 152, '2019-07-13 02:57:47', '2019-07-13 02:57:47'),
(142, 'MISC', NULL, 'no', 153, '2019-07-13 02:58:48', '2019-07-13 02:58:48'),
(143, 'MISC', NULL, 'no', 154, '2019-07-13 02:59:14', '2019-07-13 02:59:14'),
(144, 'MISC', NULL, 'no', 155, '2019-07-13 02:59:47', '2019-07-13 02:59:47'),
(145, 'MISC', NULL, 'no', 156, '2019-07-13 03:00:00', '2019-07-13 03:00:00'),
(146, 'MISC', NULL, 'no', 157, '2019-07-13 03:00:48', '2019-07-13 03:00:48'),
(147, 'MISC', NULL, 'no', 158, '2019-07-13 03:01:38', '2019-07-13 03:01:38'),
(148, 'MISC', NULL, 'no', 159, '2019-07-13 03:01:57', '2019-07-13 03:01:57'),
(149, 'MISC', NULL, 'no', 161, '2019-07-13 03:02:27', '2019-07-13 03:02:27'),
(150, 'MISC', NULL, 'no', 162, '2019-07-13 03:02:51', '2019-07-13 03:02:51'),
(151, 'MISC', NULL, 'no', 163, '2019-07-13 03:03:04', '2019-07-13 03:03:04'),
(152, 'MISC', NULL, 'no', 164, '2019-07-13 03:03:48', '2019-07-13 03:03:48'),
(153, 'MISC', NULL, 'no', 165, '2019-07-13 03:04:28', '2019-07-13 03:04:28'),
(154, 'MISC', NULL, 'no', 166, '2019-07-13 03:04:46', '2019-07-13 03:04:46'),
(155, 'MISC', NULL, 'no', 167, '2019-07-13 03:05:19', '2019-07-13 03:05:19'),
(156, 'MISC', NULL, 'no', 168, '2019-07-13 03:05:58', '2019-07-13 03:05:58'),
(157, 'MISC', NULL, 'no', 169, '2019-07-13 03:07:16', '2019-07-13 03:07:16'),
(158, 'MISC', NULL, 'no', 171, '2019-07-13 03:07:39', '2019-07-13 03:07:39'),
(159, 'MISC', NULL, 'no', 172, '2019-07-13 03:09:36', '2019-07-13 03:09:36'),
(160, 'MISC', NULL, 'no', 173, '2019-07-13 03:11:08', '2019-07-13 03:11:08'),
(161, 'MISC', NULL, 'no', 174, '2019-07-13 03:13:45', '2019-07-13 03:13:45'),
(162, 'MISC', NULL, 'no', 175, '2019-07-13 03:15:35', '2019-07-13 03:15:35'),
(163, 'MISC', NULL, 'no', 176, '2019-07-13 03:16:32', '2019-07-13 03:16:32'),
(164, 'MISC', NULL, 'no', 177, '2019-07-13 03:17:28', '2019-07-13 03:17:28'),
(165, 'MISC', NULL, 'no', 178, '2019-07-13 03:18:34', '2019-07-13 03:18:34'),
(166, 'MISC', NULL, 'no', 179, '2019-07-13 03:18:55', '2019-07-13 03:18:55'),
(167, 'MISC', NULL, 'no', 180, '2019-07-13 03:20:12', '2019-07-13 03:20:12'),
(168, 'MISC', NULL, 'no', 181, '2019-07-13 03:21:23', '2019-07-13 03:21:23'),
(169, 'MISC', NULL, 'no', 182, '2019-07-13 03:21:36', '2019-07-13 03:21:36'),
(170, 'MISC', NULL, 'no', 183, '2019-07-13 03:21:42', '2019-07-13 03:21:42'),
(171, 'MISC', NULL, 'no', 184, '2019-07-13 03:22:02', '2019-07-13 03:22:02'),
(172, 'MISC', NULL, 'no', 185, '2019-07-13 03:22:53', '2019-07-13 03:22:53'),
(173, 'MISC', NULL, 'no', 186, '2019-07-13 03:23:17', '2019-07-13 03:23:17'),
(174, 'MISC', NULL, 'no', 187, '2019-07-13 03:23:56', '2019-07-13 03:23:56'),
(175, 'MISC', NULL, 'no', 188, '2019-07-13 03:24:19', '2019-07-13 03:24:19'),
(176, 'MISC', NULL, 'no', 190, '2019-07-13 03:24:46', '2019-07-13 03:24:46'),
(177, 'MISC', NULL, 'no', 191, '2019-07-13 03:25:09', '2019-07-13 03:25:09'),
(178, 'MISC', NULL, 'no', 192, '2019-07-13 03:25:34', '2019-07-13 03:25:34'),
(179, 'MISC', NULL, 'no', 193, '2019-07-13 03:26:00', '2019-07-13 03:26:00'),
(180, 'MISC', NULL, 'no', 194, '2019-07-13 03:26:22', '2019-07-13 03:26:22'),
(181, 'MISC', NULL, 'no', 195, '2019-07-13 03:26:50', '2019-07-13 03:26:50'),
(182, 'MISC', NULL, 'no', 196, '2019-07-13 03:27:10', '2019-07-13 03:27:10'),
(183, 'MISC', NULL, 'no', 197, '2019-07-13 03:27:34', '2019-07-13 03:27:34'),
(184, 'MISC', NULL, 'no', 198, '2019-07-13 03:28:41', '2019-07-13 03:28:41'),
(185, 'MISC', NULL, 'no', 199, '2019-07-13 03:29:10', '2019-07-13 03:29:10'),
(186, 'MISC', NULL, 'no', 200, '2019-07-13 03:30:29', '2019-07-13 03:30:29'),
(187, 'MISC', NULL, 'no', 201, '2019-07-13 03:30:41', '2019-07-13 03:30:41'),
(188, 'MISC', NULL, 'no', 202, '2019-07-13 03:31:16', '2019-07-13 03:31:16'),
(189, 'MISC', NULL, 'no', 203, '2019-07-13 03:31:34', '2019-07-13 03:31:34'),
(190, 'MISC', NULL, 'no', 204, '2019-07-13 03:32:07', '2019-07-13 03:32:07'),
(191, 'MISC', NULL, 'no', 205, '2019-07-13 03:32:29', '2019-07-13 03:32:29'),
(192, 'MISC', NULL, 'no', 207, '2019-07-13 03:32:48', '2019-07-13 03:32:48'),
(193, 'MISC', NULL, 'no', 208, '2019-07-13 03:33:15', '2019-07-13 03:33:15'),
(194, 'MISC', NULL, 'no', 209, '2019-07-13 03:33:42', '2019-07-13 03:33:42'),
(195, 'MISC', NULL, 'no', 210, '2019-07-13 03:34:47', '2019-07-13 03:34:47'),
(196, 'MISC', NULL, 'no', 211, '2019-07-13 03:35:56', '2019-07-13 03:35:56'),
(197, 'MISC', NULL, 'no', 212, '2019-07-13 03:37:12', '2019-07-13 03:37:12'),
(198, 'MISC', NULL, 'no', 213, '2019-07-13 03:38:33', '2019-07-13 03:38:33'),
(199, 'MISC', NULL, 'no', 214, '2019-07-13 03:41:16', '2019-07-13 03:41:16'),
(200, 'MISC', NULL, 'no', 215, '2019-07-13 03:42:07', '2019-07-13 03:42:07'),
(201, 'MISC', NULL, 'no', 216, '2019-07-13 03:42:30', '2019-07-13 03:42:30'),
(202, 'MISC', NULL, 'no', 217, '2019-07-13 03:42:45', '2019-07-13 03:42:45'),
(203, 'MISC', NULL, 'no', 218, '2019-07-13 03:42:59', '2019-07-13 03:42:59'),
(204, 'MISC', NULL, 'no', 219, '2019-07-13 03:44:08', '2019-07-13 03:44:08'),
(205, 'MISC', NULL, 'no', 220, '2019-07-13 03:45:17', '2019-07-13 03:45:17'),
(206, 'MISC', NULL, 'no', 221, '2019-07-13 03:45:57', '2019-07-13 03:45:57'),
(207, 'MISC', NULL, 'no', 222, '2019-07-13 03:46:08', '2019-07-13 03:46:08'),
(208, 'MISC', NULL, 'no', 224, '2019-07-13 03:46:12', '2019-07-13 03:46:12'),
(209, 'MISC', NULL, 'no', 225, '2019-07-13 03:46:29', '2019-07-13 03:46:29'),
(210, 'MISC', NULL, 'no', 226, '2019-07-13 03:46:31', '2019-07-13 03:46:31'),
(211, 'MISC', NULL, 'no', 227, '2019-07-13 03:46:59', '2019-07-13 03:46:59'),
(212, 'MISC', NULL, 'no', 228, '2019-07-13 03:47:12', '2019-07-13 03:47:12'),
(213, 'MISC', NULL, 'no', 229, '2019-07-13 03:47:24', '2019-07-13 03:47:24'),
(214, 'MISC', NULL, 'no', 230, '2019-07-13 03:47:37', '2019-07-13 03:47:37'),
(215, 'MISC', NULL, 'no', 231, '2019-07-13 03:47:47', '2019-07-13 03:47:47'),
(216, 'MISC', NULL, 'no', 232, '2019-07-13 03:48:03', '2019-07-13 03:48:03'),
(217, 'MISC', NULL, 'no', 233, '2019-07-13 03:48:13', '2019-07-13 03:48:13'),
(218, 'MISC', NULL, 'no', 234, '2019-07-13 03:48:38', '2019-07-13 03:48:38'),
(219, 'MISC', NULL, 'no', 235, '2019-07-13 03:49:12', '2019-07-13 03:49:12'),
(220, 'MISC', NULL, 'no', 236, '2019-07-13 03:49:27', '2019-07-13 03:49:27'),
(221, 'MISC', NULL, 'no', 237, '2019-07-13 03:49:36', '2019-07-13 03:49:36'),
(222, 'MISC', NULL, 'no', 239, '2019-07-13 03:49:39', '2019-07-13 03:49:39'),
(223, 'MISC', NULL, 'no', 240, '2019-07-13 03:50:01', '2019-07-13 03:50:01'),
(224, 'MISC', NULL, 'no', 241, '2019-07-13 03:50:09', '2019-07-13 03:50:09'),
(225, 'MISC', NULL, 'no', 242, '2019-07-13 03:50:19', '2019-07-13 03:50:19'),
(226, 'MISC', NULL, 'no', 243, '2019-07-13 03:50:32', '2019-07-13 03:50:32'),
(227, 'MISC', NULL, 'no', 244, '2019-07-13 03:50:35', '2019-07-13 03:50:35'),
(228, 'MISC', NULL, 'no', 245, '2019-07-13 03:51:07', '2019-07-13 03:51:07'),
(229, 'MISC', NULL, 'no', 246, '2019-07-13 03:51:28', '2019-07-13 03:51:28'),
(230, 'MISC', NULL, 'no', 247, '2019-07-13 03:51:47', '2019-07-13 03:51:47'),
(231, 'MISC', NULL, 'no', 248, '2019-07-13 03:51:51', '2019-07-13 03:51:51'),
(232, 'MISC', NULL, 'no', 249, '2019-07-13 03:52:27', '2019-07-13 03:52:27'),
(233, 'MISC', NULL, 'no', 250, '2019-07-13 03:52:40', '2019-07-13 03:52:40'),
(234, 'MISC', NULL, 'no', 251, '2019-07-13 03:53:14', '2019-07-13 03:53:14'),
(235, 'MISC', NULL, 'no', 252, '2019-07-13 03:53:39', '2019-07-13 03:53:39'),
(236, 'MISC', NULL, 'no', 253, '2019-07-13 03:54:31', '2019-07-13 03:54:31'),
(237, 'MISC', NULL, 'no', 254, '2019-07-13 03:54:46', '2019-07-13 03:54:46'),
(238, 'MISC', NULL, 'no', 256, '2019-07-13 03:55:11', '2019-07-13 03:55:11'),
(239, 'MISC', NULL, 'no', 257, '2019-07-13 03:55:38', '2019-07-13 03:55:38'),
(240, 'MISC', NULL, 'no', 258, '2019-07-13 03:55:53', '2019-07-13 03:55:53'),
(241, 'MISC', NULL, 'no', 259, '2019-07-13 03:56:38', '2019-07-13 03:56:38'),
(242, 'MISC', NULL, 'no', 260, '2019-07-13 03:57:20', '2019-07-13 03:57:20'),
(243, 'MISC', NULL, 'no', 261, '2019-07-13 03:57:38', '2019-07-13 03:57:38'),
(244, 'MISC', NULL, 'no', 262, '2019-07-13 03:58:14', '2019-07-13 03:58:14'),
(245, 'MISC', NULL, 'no', 263, '2019-07-13 03:58:56', '2019-07-13 03:58:56'),
(246, 'MISC', NULL, 'no', 264, '2019-07-13 04:00:20', '2019-07-13 04:00:20'),
(247, 'MISC', NULL, 'no', 266, '2019-07-13 04:00:49', '2019-07-13 04:00:49'),
(248, 'MISC', NULL, 'no', 267, '2019-07-13 04:03:10', '2019-07-13 04:03:10'),
(249, 'MISC', NULL, 'no', 268, '2019-07-13 04:05:05', '2019-07-13 04:05:05'),
(250, 'MISC', NULL, 'no', 269, '2019-07-13 04:07:14', '2019-07-13 04:07:14'),
(251, 'MISC', NULL, 'no', 270, '2019-07-13 04:08:49', '2019-07-13 04:08:49'),
(252, 'MISC', NULL, 'no', 271, '2019-07-13 04:09:50', '2019-07-13 04:09:50'),
(253, 'MISC', NULL, 'no', 272, '2019-07-13 04:10:40', '2019-07-13 04:10:40'),
(254, 'MISC', NULL, 'no', 273, '2019-07-13 04:11:44', '2019-07-13 04:11:44'),
(255, 'MISC', NULL, 'no', 274, '2019-07-13 04:12:00', '2019-07-13 04:12:00'),
(256, 'MISC', NULL, 'no', 275, '2019-07-13 04:13:04', '2019-07-13 04:13:04'),
(257, 'MISC', NULL, 'no', 276, '2019-07-13 04:14:23', '2019-07-13 04:14:23'),
(258, 'MISC', NULL, 'no', 277, '2019-07-13 04:14:34', '2019-07-13 04:14:34'),
(259, 'MISC', NULL, 'no', 278, '2019-07-13 04:14:40', '2019-07-13 04:14:40'),
(260, 'MISC', NULL, 'no', 279, '2019-07-13 04:14:59', '2019-07-13 04:14:59'),
(261, 'MISC', NULL, 'no', 280, '2019-07-13 04:16:00', '2019-07-13 04:16:00'),
(262, 'MISC', NULL, 'no', 281, '2019-07-13 04:16:18', '2019-07-13 04:16:18'),
(263, 'MISC', NULL, 'no', 282, '2019-07-13 04:16:48', '2019-07-13 04:16:48'),
(264, 'MISC', NULL, 'no', 283, '2019-07-13 04:17:09', '2019-07-13 04:17:09'),
(265, 'MISC', NULL, 'no', 285, '2019-07-13 04:17:45', '2019-07-13 04:17:45'),
(266, 'MISC', NULL, 'no', 286, '2019-07-13 04:18:03', '2019-07-13 04:18:03'),
(267, 'MISC', NULL, 'no', 287, '2019-07-13 04:18:24', '2019-07-13 04:18:24'),
(268, 'MISC', NULL, 'no', 288, '2019-07-13 04:18:45', '2019-07-13 04:18:45'),
(269, 'MISC', NULL, 'no', 289, '2019-07-13 04:19:00', '2019-07-13 04:19:00'),
(270, 'MISC', NULL, 'no', 290, '2019-07-13 04:19:25', '2019-07-13 04:19:25'),
(271, 'MISC', NULL, 'no', 291, '2019-07-13 04:20:04', '2019-07-13 04:20:04'),
(272, 'MISC', NULL, 'no', 292, '2019-07-13 04:20:24', '2019-07-13 04:20:24'),
(273, 'MISC', NULL, 'no', 293, '2019-07-13 04:21:00', '2019-07-13 04:21:00'),
(274, 'MISC', NULL, 'no', 294, '2019-07-13 04:21:18', '2019-07-13 04:21:18'),
(275, 'MISC', NULL, 'no', 295, '2019-07-13 04:22:45', '2019-07-13 04:22:45'),
(276, 'MISC', NULL, 'no', 296, '2019-07-13 04:23:02', '2019-07-13 04:23:02'),
(277, 'MISC', NULL, 'no', 297, '2019-07-13 04:24:04', '2019-07-13 04:24:04'),
(278, 'MISC', NULL, 'no', 298, '2019-07-13 04:24:36', '2019-07-13 04:24:36'),
(279, 'MISC', NULL, 'no', 299, '2019-07-13 04:24:58', '2019-07-13 04:24:58'),
(280, 'MISC', NULL, 'no', 300, '2019-07-13 04:25:18', '2019-07-13 04:25:18'),
(281, 'MISC', NULL, 'no', 302, '2019-07-13 04:25:38', '2019-07-13 04:25:38'),
(282, 'MISC', NULL, 'no', 303, '2019-07-13 04:25:58', '2019-07-13 04:25:58'),
(283, 'MISC', NULL, 'no', 304, '2019-07-13 04:26:21', '2019-07-13 04:26:21'),
(284, 'MISC', NULL, 'no', 305, '2019-07-13 04:27:26', '2019-07-13 04:27:26'),
(285, 'MISC', NULL, 'no', 306, '2019-07-13 04:28:38', '2019-07-13 04:28:38'),
(286, 'MISC', NULL, 'no', 307, '2019-07-13 04:29:35', '2019-07-13 04:29:35'),
(287, 'MISC', NULL, 'no', 308, '2019-07-13 04:30:27', '2019-07-13 04:30:27'),
(288, 'MISC', NULL, 'no', 309, '2019-07-13 04:32:55', '2019-07-13 04:32:55'),
(289, 'MISC', NULL, 'no', 310, '2019-07-13 04:33:32', '2019-07-13 04:33:32'),
(290, 'MISC', NULL, 'no', 311, '2019-07-13 04:33:55', '2019-07-13 04:33:55'),
(291, 'MISC', NULL, 'no', 312, '2019-07-13 04:34:04', '2019-07-13 04:34:04'),
(292, 'MISC', NULL, 'no', 313, '2019-07-13 04:34:13', '2019-07-13 04:34:13'),
(293, 'MISC', NULL, 'no', 314, '2019-07-13 04:35:08', '2019-07-13 04:35:08'),
(294, 'MISC', NULL, 'no', 315, '2019-07-13 04:36:19', '2019-07-13 04:36:19'),
(295, 'MISC', NULL, 'no', 316, '2019-07-13 04:36:57', '2019-07-13 04:36:57'),
(296, 'MISC', NULL, 'no', 318, '2019-07-13 04:37:13', '2019-07-13 04:37:13'),
(297, 'MISC', NULL, 'no', 319, '2019-07-13 04:37:26', '2019-07-13 04:37:26'),
(298, 'MISC', NULL, 'no', 320, '2019-07-13 04:38:01', '2019-07-13 04:38:01'),
(299, 'MISC', NULL, 'no', 321, '2019-07-13 04:38:46', '2019-07-13 04:38:46'),
(300, 'MISC', NULL, 'no', 322, '2019-07-13 04:39:21', '2019-07-13 04:39:21'),
(301, 'MISC', NULL, 'no', 323, '2019-07-13 04:40:04', '2019-07-13 04:40:04'),
(302, 'MISC', NULL, 'no', 324, '2019-07-13 04:40:21', '2019-07-13 04:40:21'),
(303, 'MISC', NULL, 'no', 325, '2019-07-13 04:40:47', '2019-07-13 04:40:47'),
(304, 'MISC', NULL, 'no', 326, '2019-07-13 04:41:03', '2019-07-13 04:41:03'),
(305, 'MISC', NULL, 'no', 327, '2019-07-13 04:41:13', '2019-07-13 04:41:13'),
(306, 'MISC', NULL, 'no', 328, '2019-07-13 04:41:42', '2019-07-13 04:41:42'),
(307, 'MISC', NULL, 'no', 329, '2019-07-13 04:42:23', '2019-07-13 04:42:23'),
(308, 'MISC', NULL, 'no', 330, '2019-07-13 04:43:37', '2019-07-13 04:43:37'),
(309, 'MISC', NULL, 'no', 331, '2019-07-13 04:44:21', '2019-07-13 04:44:21'),
(310, 'MISC', NULL, 'no', 332, '2019-07-13 04:45:00', '2019-07-13 04:45:00'),
(311, 'MISC', NULL, 'no', 333, '2019-07-13 04:45:20', '2019-07-13 04:45:20'),
(312, 'MISC', NULL, 'no', 334, '2019-07-13 04:45:46', '2019-07-13 04:45:46');

-- --------------------------------------------------------

--
-- Table structure for table `performer_status`
--

CREATE TABLE `performer_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `performer_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `performer_status`
--

INSERT INTO `performer_status` (`id`, `performer_status`, `created_at`, `updated_at`) VALUES
(1, 'Active', NULL, NULL),
(2, 'Do Not Book', NULL, NULL),
(3, 'Flagged', NULL, NULL),
(4, 'No Longer Working', NULL, NULL),
(5, 'Warning #1', NULL, NULL),
(6, 'Warning #2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `performer_unions`
--

CREATE TABLE `performer_unions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `union` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `performer_unions`
--

INSERT INTO `performer_unions` (`id`, `union`, `created_at`, `updated_at`) VALUES
(1, 'Screen Actors Guild', NULL, NULL),
(2, 'Screen Actors Guild Eligible', NULL, NULL),
(3, 'UBCP', NULL, NULL),
(4, 'UBCP Apprentice', NULL, NULL),
(5, 'UBCP Extra', NULL, NULL),
(6, 'UDA', NULL, NULL),
(7, 'UDA Stagiaire', NULL, NULL),
(8, 'RQD', NULL, NULL),
(9, 'ANDA', NULL, NULL),
(10, 'ACTRA', NULL, NULL),
(11, 'ACTRA Apprentice', NULL, NULL),
(12, 'AABP', NULL, NULL),
(13, 'EQUITY', NULL, NULL),
(14, 'EQUITY Permitee', NULL, NULL),
(15, 'AEA', NULL, NULL),
(16, 'CAEA', NULL, NULL),
(17, 'CAEA Apprentice', NULL, NULL),
(18, 'BAEA', NULL, NULL),
(19, 'MEAA', NULL, NULL),
(20, 'NON-UNION', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pets`
--

CREATE TABLE `pets` (
  `id` int(11) NOT NULL,
  `petName` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pets`
--

INSERT INTO `pets` (`id`, `petName`, `created_at`, `updated_at`) VALUES
(1, 'Abyssinian', '2019-07-03 11:16:40', '0000-00-00'),
(2, 'Afghan Hound', '2019-07-03 11:16:40', '0000-00-00'),
(3, 'African Grey', '2019-07-03 11:16:40', '0000-00-00'),
(4, 'Airedale Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(5, 'Akita', '2019-07-03 11:16:40', '0000-00-00'),
(6, 'Alaskan Malamute', '2019-07-03 11:16:40', '0000-00-00'),
(7, 'American Bulldog', '2019-07-03 11:16:40', '0000-00-00'),
(8, 'American Eskimo', '2019-07-03 11:16:40', '0000-00-00'),
(9, 'American Shorthair', '2019-07-03 11:16:40', '0000-00-00'),
(10, 'Anatolian Shepherd', '2019-07-03 11:16:40', '0000-00-00'),
(11, 'Australian Cattle Dog', '2019-07-03 11:16:40', '0000-00-00'),
(12, 'Australian Kelpie', '2019-07-03 11:16:40', '0000-00-00'),
(13, 'Australian Shepherd', '2019-07-03 11:16:40', '0000-00-00'),
(14, 'Basenji', '2019-07-03 11:16:40', '0000-00-00'),
(15, 'Basset Hound', '2019-07-03 11:16:40', '0000-00-00'),
(16, 'Beagle', '2019-07-03 11:16:40', '0000-00-00'),
(17, 'Bearded Collie', '2019-07-03 11:16:40', '0000-00-00'),
(18, 'Bearded Dragon', '2019-07-03 11:16:40', '0000-00-00'),
(19, 'Beauceron', '2019-07-03 11:16:40', '0000-00-00'),
(20, 'Belgian Malinois', '2019-07-03 11:16:40', '0000-00-00'),
(21, 'Belgian Shepherd', '2019-07-03 11:16:40', '0000-00-00'),
(22, 'Bengal', '2019-07-03 11:16:40', '0000-00-00'),
(23, 'Bernese Mountain Dog', '2019-07-03 11:16:40', '0000-00-00'),
(24, 'Bichon Frise', '2019-07-03 11:16:40', '0000-00-00'),
(25, 'Birman', '2019-07-03 11:16:40', '0000-00-00'),
(26, 'Black Labrador Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(27, 'Bloodhound', '2019-07-03 11:16:40', '0000-00-00'),
(28, 'Boa', '2019-07-03 11:16:40', '0000-00-00'),
(29, 'Bombay', '2019-07-03 11:16:40', '0000-00-00'),
(30, 'Border Collie', '2019-07-03 11:16:40', '0000-00-00'),
(31, 'Border Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(32, 'Borzoi', '2019-07-03 11:16:40', '0000-00-00'),
(33, 'Boston Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(34, 'Bouvier Des Flanders', '2019-07-03 11:16:40', '0000-00-00'),
(35, 'Boxer', '2019-07-03 11:16:40', '0000-00-00'),
(36, 'Britany Spaniel', '2019-07-03 11:16:40', '0000-00-00'),
(37, 'British Shorthair', '2019-07-03 11:16:40', '0000-00-00'),
(38, 'Brussels Griffon', '2019-07-03 11:16:40', '0000-00-00'),
(39, 'Budgie', '2019-07-03 11:16:40', '0000-00-00'),
(40, 'Bull Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(41, 'Bullmastiff', '2019-07-03 11:16:40', '0000-00-00'),
(42, 'Burmese', '2019-07-03 11:16:40', '0000-00-00'),
(43, 'Cairn Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(44, 'Canary', '2019-07-03 11:16:40', '0000-00-00'),
(45, 'Cane Corso Mastiff', '2019-07-03 11:16:40', '0000-00-00'),
(46, 'Catahoula Leopard Dog', '2019-07-03 11:16:40', '0000-00-00'),
(47, 'Chameleon', '2019-07-03 11:16:40', '0000-00-00'),
(48, 'Chesapeake Bay Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(49, 'Chihauhua', '2019-07-03 11:16:40', '0000-00-00'),
(50, 'Chinese Crested', '2019-07-03 11:16:40', '0000-00-00'),
(51, 'Chocolate Labrador Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(52, 'Chocolate Point Siamese', '2019-07-03 11:16:40', '0000-00-00'),
(53, 'Chow Chow', '2019-07-03 11:16:40', '0000-00-00'),
(54, 'Cockapoo', '2019-07-03 11:16:40', '0000-00-00'),
(55, 'Cockatiel', '2019-07-03 11:16:40', '0000-00-00'),
(56, 'Cockatoo', '2019-07-03 11:16:40', '0000-00-00'),
(57, 'Cocker Spaniel', '2019-07-03 11:16:40', '0000-00-00'),
(58, 'Conure', '2019-07-03 11:16:40', '0000-00-00'),
(59, 'Coonhound', '2019-07-03 11:16:40', '0000-00-00'),
(60, 'Corgi', '2019-07-03 11:16:40', '0000-00-00'),
(61, 'Corn Snake', '2019-07-03 11:16:40', '0000-00-00'),
(62, 'Cornish Rex', '2019-07-03 11:16:40', '0000-00-00'),
(63, 'Crow', '2019-07-03 11:16:40', '0000-00-00'),
(64, 'Dachshund', '2019-07-03 11:16:40', '0000-00-00'),
(65, 'Dalmation', '2019-07-03 11:16:40', '0000-00-00'),
(66, 'Devon Rex', '2019-07-03 11:16:40', '0000-00-00'),
(67, 'Doberman Pinscher', '2019-07-03 11:16:40', '0000-00-00'),
(68, 'Dogo Argentino', '2019-07-03 11:16:40', '0000-00-00'),
(69, 'Dogue De Bordeaux', '2019-07-03 11:16:40', '0000-00-00'),
(70, 'Dove', '2019-07-03 11:16:40', '0000-00-00'),
(71, 'Egyptian Mau', '2019-07-03 11:16:40', '0000-00-00'),
(72, 'Electus', '2019-07-03 11:16:40', '0000-00-00'),
(73, 'English Bulldog', '2019-07-03 11:16:40', '0000-00-00'),
(74, 'English Pointer', '2019-07-03 11:16:40', '0000-00-00'),
(75, 'English Setter', '2019-07-03 11:16:40', '0000-00-00'),
(76, 'Finch', '2019-07-03 11:16:40', '0000-00-00'),
(77, 'Flame Point Siamese', '2019-07-03 11:16:40', '0000-00-00'),
(78, 'Flat Coated Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(79, 'Fox Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(80, 'Foxhound', '2019-07-03 11:16:40', '0000-00-00'),
(81, 'French Bulldog', '2019-07-03 11:16:40', '0000-00-00'),
(82, 'Gecko', '2019-07-03 11:16:40', '0000-00-00'),
(83, 'German Shepherd', '2019-07-03 11:16:40', '0000-00-00'),
(84, 'German Shorthaired Pointer', '2019-07-03 11:16:40', '0000-00-00'),
(85, 'German Wirehaired Pointer', '2019-07-03 11:16:40', '0000-00-00'),
(86, 'Giant Schnauzer', '2019-07-03 11:16:40', '0000-00-00'),
(87, 'Golden Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(88, 'Great Dane', '2019-07-03 11:16:40', '0000-00-00'),
(89, 'Great Pyrenees', '2019-07-03 11:16:40', '0000-00-00'),
(90, 'Greyhound', '2019-07-03 11:16:40', '0000-00-00'),
(91, 'Havanese', '2019-07-03 11:16:40', '0000-00-00'),
(92, 'Hawk', '2019-07-03 11:16:40', '0000-00-00'),
(93, 'Himalayan', '2019-07-03 11:16:40', '0000-00-00'),
(94, 'Husky', '2019-07-03 11:16:40', '0000-00-00'),
(95, 'Ibizian Hound', '2019-07-03 11:16:40', '0000-00-00'),
(96, 'Iguana', '2019-07-03 11:16:40', '0000-00-00'),
(97, 'Irish Setter', '2019-07-03 11:16:40', '0000-00-00'),
(98, 'Irish Wolfhound', '2019-07-03 11:16:40', '0000-00-00'),
(99, 'Italian Greyhound', '2019-07-03 11:16:40', '0000-00-00'),
(100, 'Jack Russell Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(101, 'Japanese Chin', '2019-07-03 11:16:40', '0000-00-00'),
(102, 'Karelian Bear Dog', '2019-07-03 11:16:40', '0000-00-00'),
(103, 'Keeshond', '2019-07-03 11:16:40', '0000-00-00'),
(104, 'King Charles Spaniel', '2019-07-03 11:16:40', '0000-00-00'),
(105, 'Komondor', '2019-07-03 11:16:40', '0000-00-00'),
(106, 'Leopard Gecko', '2019-07-03 11:16:40', '0000-00-00'),
(107, 'Lhasa Apso', '2019-07-03 11:16:40', '0000-00-00'),
(108, 'Lilac Point Siamese', '2019-07-03 11:16:40', '0000-00-00'),
(109, 'Lovebird', '2019-07-03 11:16:40', '0000-00-00'),
(110, 'Macaw', '2019-07-03 11:16:40', '0000-00-00'),
(111, 'Maine Coon', '2019-07-03 11:16:40', '0000-00-00'),
(112, 'Maltese', '2019-07-03 11:16:40', '0000-00-00'),
(113, 'Manchester Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(114, 'Manx', '2019-07-03 11:16:40', '0000-00-00'),
(115, 'Maremma Sheepdog', '2019-07-03 11:16:40', '0000-00-00'),
(116, 'Mastiff', '2019-07-03 11:16:40', '0000-00-00'),
(117, 'Miniature Pinscher', '2019-07-03 11:16:40', '0000-00-00'),
(118, 'Mountain Cur', '2019-07-03 11:16:40', '0000-00-00'),
(119, 'Munchkin', '2019-07-03 11:16:40', '0000-00-00'),
(120, 'Newfoundland', '2019-07-03 11:16:40', '0000-00-00'),
(121, 'Norfolk Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(122, 'Norwegian Elkhound', '2019-07-03 11:16:40', '0000-00-00'),
(123, 'Norwegian Forest Cat', '2019-07-03 11:16:40', '0000-00-00'),
(124, 'Nova Scotia Duck-Tolling Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(125, 'Ocicat', '2019-07-03 11:16:40', '0000-00-00'),
(126, 'Old English Sheepdog', '2019-07-03 11:16:40', '0000-00-00'),
(127, 'Oriental Shorthair', '2019-07-03 11:16:40', '0000-00-00'),
(128, 'Papillon', '2019-07-03 11:16:40', '0000-00-00'),
(129, 'Parakeet', '2019-07-03 11:16:40', '0000-00-00'),
(130, 'Parrot', '2019-07-03 11:16:40', '0000-00-00'),
(131, 'Pekingese', '2019-07-03 11:16:40', '0000-00-00'),
(132, 'Persian', '2019-07-03 11:16:40', '0000-00-00'),
(133, 'Pharaoh Hound', '2019-07-03 11:16:40', '0000-00-00'),
(134, 'Pit Bull Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(135, 'Pointer', '2019-07-03 11:16:40', '0000-00-00'),
(136, 'Pomeranian', '2019-07-03 11:16:40', '0000-00-00'),
(137, 'Poodle', '2019-07-03 11:16:40', '0000-00-00'),
(138, 'Portuguese Water Dog', '2019-07-03 11:16:40', '0000-00-00'),
(139, 'Pug', '2019-07-03 11:16:40', '0000-00-00'),
(140, 'Python', '2019-07-03 11:16:40', '0000-00-00'),
(141, 'Ragdoll', '2019-07-03 11:16:40', '0000-00-00'),
(142, 'Rat Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(143, 'Rhodesian Ridgeback', '2019-07-03 11:16:40', '0000-00-00'),
(144, 'Rottweiler', '2019-07-03 11:16:40', '0000-00-00'),
(145, 'Russian Blue', '2019-07-03 11:16:40', '0000-00-00'),
(146, 'Saint Bernard', '2019-07-03 11:16:40', '0000-00-00'),
(147, 'Saluki', '2019-07-03 11:16:40', '0000-00-00'),
(148, 'Samoyed', '2019-07-03 11:16:40', '0000-00-00'),
(149, 'Savannah', '2019-07-03 11:16:40', '0000-00-00'),
(150, 'Schipperke', '2019-07-03 11:16:40', '0000-00-00'),
(151, 'Schnauzer', '2019-07-03 11:16:40', '0000-00-00'),
(152, 'Scottish Deerhound', '2019-07-03 11:16:40', '0000-00-00'),
(153, 'Scottish Fold', '2019-07-03 11:16:40', '0000-00-00'),
(154, 'Scottish Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(155, 'Seal Point Siamese', '2019-07-03 11:16:40', '0000-00-00'),
(156, 'Setter', '2019-07-03 11:16:40', '0000-00-00'),
(157, 'Shar Pei', '2019-07-03 11:16:40', '0000-00-00'),
(158, 'Sheep Dog', '2019-07-03 11:16:40', '0000-00-00'),
(159, 'Shepherd', '2019-07-03 11:16:40', '0000-00-00'),
(160, 'Shetland Sheepdog', '2019-07-03 11:16:40', '0000-00-00'),
(161, 'Shiba Inue', '2019-07-03 11:16:40', '0000-00-00'),
(162, 'Shih Tzu', '2019-07-03 11:16:40', '0000-00-00'),
(163, 'Siamese', '2019-07-03 11:16:40', '0000-00-00'),
(164, 'Siberian', '2019-07-03 11:16:40', '0000-00-00'),
(165, 'Siberian Husky', '2019-07-03 11:16:40', '0000-00-00'),
(166, 'Silky Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(167, 'Snowshoe', '2019-07-03 11:16:40', '0000-00-00'),
(168, 'Somali', '2019-07-03 11:16:40', '0000-00-00'),
(169, 'Spaniel', '2019-07-03 11:16:40', '0000-00-00'),
(170, 'Sphynx', '2019-07-03 11:16:40', '0000-00-00'),
(171, 'Spitz', '2019-07-03 11:16:40', '0000-00-00'),
(172, 'Springer Spaniel', '2019-07-03 11:16:40', '0000-00-00'),
(173, 'Staffordshire Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(174, 'Standard Poodle', '2019-07-03 11:16:40', '0000-00-00'),
(175, 'Tabby', '2019-07-03 11:16:40', '0000-00-00'),
(176, 'Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(177, 'Tortoise Shell', '2019-07-03 11:16:40', '0000-00-00'),
(178, 'Turkish Angora', '2019-07-03 11:16:40', '0000-00-00'),
(179, 'Vizsla', '2019-07-03 11:16:40', '0000-00-00'),
(180, 'Weimaraner', '2019-07-03 11:16:40', '0000-00-00'),
(181, 'West Highland Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(182, 'Wheaten Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(183, 'Whippet', '2019-07-03 11:16:40', '0000-00-00'),
(184, 'Yellow Labrador Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(185, 'Yorkshire Terrier', '2019-07-03 11:16:40', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `piercings`
--

CREATE TABLE `piercings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `piercing` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `piercings`
--

INSERT INTO `piercings` (`id`, `piercing`, `created_at`, `updated_at`) VALUES
(1, 'Has Vehicle', NULL, NULL),
(2, 'Rain Towers', NULL, NULL),
(3, 'Will kiss Male', NULL, NULL),
(4, 'Will Kiss Female', NULL, NULL),
(5, 'Will kiss partner', NULL, NULL),
(6, 'Will Kiss', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_gallery_tags`
--

CREATE TABLE `portfolio_gallery_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_perfomers`
--

CREATE TABLE `project_perfomers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IMDC_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resume_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_roles`
--

CREATE TABLE `project_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `project_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `props`
--

CREATE TABLE `props` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `prop` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `props`
--

INSERT INTO `props` (`id`, `prop`, `created_at`, `updated_at`) VALUES
(1, 'Acoustic Guitar', NULL, NULL),
(2, 'Banjo', NULL, NULL),
(3, 'Baseball Gear', NULL, NULL),
(4, 'Bass Guitar', NULL, NULL),
(5, 'Bicycle', NULL, NULL),
(6, 'BMX', NULL, NULL),
(7, 'Boat', NULL, NULL),
(8, 'bongo drums', NULL, NULL),
(9, 'Boxing Gear', NULL, NULL),
(10, 'Camping Gear', NULL, NULL),
(11, 'Cruiser Bike', NULL, NULL),
(12, 'Crutches', NULL, NULL),
(13, 'Dance Shoes', NULL, NULL),
(14, 'Downhill Skis', NULL, NULL),
(15, 'Drums', NULL, NULL),
(16, 'Electric Guitar', NULL, NULL),
(17, 'Fiddle', NULL, NULL),
(18, 'Fire Dancing Equipment', NULL, NULL),
(19, 'Firearms', NULL, NULL),
(20, 'Fireman Gear', NULL, NULL),
(21, 'Fishing Gear', NULL, NULL),
(22, 'Flute', NULL, NULL),
(23, 'Football Gear', NULL, NULL),
(24, 'Golf Clubs', NULL, NULL),
(25, 'Harmonica', NULL, NULL),
(26, 'Harp', NULL, NULL),
(27, 'Hockey Gear', NULL, NULL),
(28, 'Ice Skates', NULL, NULL),
(29, 'Instrument', NULL, NULL),
(30, 'Jogging Stroller', NULL, NULL),
(31, 'Juggling Props', NULL, NULL),
(32, 'Keyboard', NULL, NULL),
(33, 'long board', NULL, NULL),
(34, 'Luggage', NULL, NULL),
(35, 'Mandolin', NULL, NULL),
(36, 'Mountain Bike', NULL, NULL),
(37, 'MTN Climbing Gear', NULL, NULL),
(38, 'Paramedic Gear', NULL, NULL),
(39, 'Racquetball Gear', NULL, NULL),
(40, 'Road Bike', NULL, NULL),
(41, 'Rock Climbing Gear', NULL, NULL),
(42, 'Roller Blades', NULL, NULL),
(43, 'Saxophone', NULL, NULL),
(44, 'Scooter', NULL, NULL),
(45, 'Scuba Gear', NULL, NULL),
(46, 'Sitar', NULL, NULL),
(47, 'Skateboard', NULL, NULL),
(48, 'Snare Drum', NULL, NULL),
(49, 'Snowboard Gear', NULL, NULL),
(50, 'Squash Racquet', NULL, NULL),
(51, 'Stand-up Bass', NULL, NULL),
(52, 'Stenograph', NULL, NULL),
(53, 'Stilts', NULL, NULL),
(54, 'Stroller', NULL, NULL),
(55, 'Surfboard', NULL, NULL),
(56, 'Tennis Equipment', NULL, NULL),
(57, 'Trombone', NULL, NULL),
(58, 'Trumpet', NULL, NULL),
(59, 'Tuba', NULL, NULL),
(60, 'Ukelele', NULL, NULL),
(61, 'Umbrella', NULL, NULL),
(62, 'Vintage Bike', NULL, NULL),
(63, 'Violin', NULL, NULL),
(64, 'Wakeboard', NULL, NULL),
(65, 'Western Saddle', NULL, NULL),
(66, 'Wheelchair', NULL, NULL),
(67, 'Windsurfer', NULL, NULL),
(68, 'Wooden Drums', NULL, NULL),
(69, 'X Country Skis', NULL, NULL),
(70, 'Xylophone', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `region`, `created_at`, `updated_at`) VALUES
(1, 'Abbotsford', NULL, NULL),
(2, 'Armstrong', NULL, NULL),
(3, 'Burnaby', NULL, NULL),
(4, 'Campbell River', NULL, NULL),
(5, 'Castlegar', NULL, NULL),
(6, 'Chilliwack', NULL, NULL),
(7, 'Colwood', NULL, NULL),
(8, 'Coquitlam', NULL, NULL),
(9, 'Courtenay', NULL, NULL),
(10, 'Cranbrook', NULL, NULL),
(11, 'Dawson Creek', NULL, NULL),
(12, 'Duncan', NULL, NULL),
(13, 'Enderby', NULL, NULL),
(14, 'Fernie', NULL, NULL),
(15, 'Fort St. John', NULL, NULL),
(16, 'Grand Forks', NULL, NULL),
(17, 'Greenwood', NULL, NULL),
(18, 'Kamloops', NULL, NULL),
(19, 'Kelowna', NULL, NULL),
(20, 'Kimberley', NULL, NULL),
(21, 'Langford', NULL, NULL),
(22, 'Langley', NULL, NULL),
(23, 'Merritt', NULL, NULL),
(24, 'Nanaimo', NULL, NULL),
(25, 'Nelson', NULL, NULL),
(26, 'New Westminster', NULL, NULL),
(27, 'North Vancouver', NULL, NULL),
(28, 'Parksville', NULL, NULL),
(29, 'Penticton', NULL, NULL),
(30, 'Pitt Meadows', NULL, NULL),
(31, 'Port Alberni', NULL, NULL),
(32, 'Port Coquitlam', NULL, NULL),
(33, 'Port Moody', NULL, NULL),
(34, 'Powell River', NULL, NULL),
(35, 'Prince George', NULL, NULL),
(36, 'Prince Rupert', NULL, NULL),
(37, 'Quesnel', NULL, NULL),
(38, 'Revelstoke', NULL, NULL),
(39, 'Richmond', NULL, NULL),
(40, 'Rossland', NULL, NULL),
(41, 'Salmon Arm', NULL, NULL),
(42, 'Surrey', NULL, NULL),
(43, 'Terrace', NULL, NULL),
(44, 'Trail', NULL, NULL),
(45, 'Vancouver', NULL, NULL),
(46, 'Vernon', NULL, NULL),
(47, 'Victoria', NULL, NULL),
(48, 'West Kelowna', NULL, NULL),
(49, 'White Rock', NULL, NULL),
(50, 'Williams Lake', NULL, NULL),
(51, '100 Mile House', NULL, NULL),
(52, 'Barriere', NULL, NULL),
(53, 'Central Saanich', NULL, NULL),
(54, 'Chetwynd', NULL, NULL),
(55, 'Clearwater', NULL, NULL),
(56, 'Coldstream', NULL, NULL),
(57, 'Delta', NULL, NULL),
(58, 'Elkford', NULL, NULL),
(59, 'Fort St. James', NULL, NULL),
(60, 'Highlands', NULL, NULL),
(61, 'Hope', NULL, NULL),
(62, 'Houston', NULL, NULL),
(63, 'Hudsons Hope', NULL, NULL),
(64, 'Invermere', NULL, NULL),
(65, 'Kent', NULL, NULL),
(66, 'Kitimat', NULL, NULL),
(67, 'Lake Country', NULL, NULL),
(68, 'Lantzville', NULL, NULL),
(69, 'Lillooet', NULL, NULL),
(70, 'Logan Lake', NULL, NULL),
(71, 'Mackenzie', NULL, NULL),
(72, 'Maple Ridge', NULL, NULL),
(73, 'Metchosin', NULL, NULL),
(74, 'Mission', NULL, NULL),
(75, 'New Hazelton', NULL, NULL),
(76, 'North Cowichan', NULL, NULL),
(77, 'North Saanich', NULL, NULL),
(78, 'North Vancouver', NULL, NULL),
(79, 'Oak Bay', NULL, NULL),
(80, 'Peachland', NULL, NULL),
(81, 'Port Edward', NULL, NULL),
(82, 'Port Hardy', NULL, NULL),
(83, 'Saanich', NULL, NULL),
(84, 'Sechelt', NULL, NULL),
(85, 'Sicamous', NULL, NULL),
(86, 'Sooke', NULL, NULL),
(87, 'Sparwood', NULL, NULL),
(88, 'Squamish', NULL, NULL),
(89, 'Stewart', NULL, NULL),
(90, 'Summerland', NULL, NULL),
(91, 'Taylor', NULL, NULL),
(92, 'Tofino', NULL, NULL),
(93, 'Tumbler Ridge', NULL, NULL),
(94, 'Ucluelet', NULL, NULL),
(95, 'Vanderhoof', NULL, NULL),
(96, 'Wells', NULL, NULL),
(97, 'West Vancouver', NULL, NULL),
(98, 'Bowen Island', NULL, NULL),
(99, 'Alberni-Clayoquot', NULL, NULL),
(100, 'Bulkley-Nechako', NULL, NULL),
(101, 'Capital', NULL, NULL),
(102, 'Cariboo', NULL, NULL),
(103, 'Central Coast', NULL, NULL),
(104, 'Central Kootenay', NULL, NULL),
(105, 'Central Okanagan', NULL, NULL),
(106, 'Columbia Shuswap', NULL, NULL),
(107, 'Comox Valley', NULL, NULL),
(108, 'Cowichan Valley', NULL, NULL),
(109, 'East Kootenay', NULL, NULL),
(110, 'Fraser Valley', NULL, NULL),
(111, 'Fraser-Fort George', NULL, NULL),
(112, 'Greater Vancouver', NULL, NULL),
(113, 'Kitimat-Stikine', NULL, NULL),
(114, 'Kootenay Boundary', NULL, NULL),
(115, 'Mount Waddington', NULL, NULL),
(116, 'Nanaimo', NULL, NULL),
(117, 'North Okanagan', NULL, NULL),
(118, 'Okanagan-Similkameen', NULL, NULL),
(119, 'Peace River', NULL, NULL),
(120, 'Powell River', NULL, NULL),
(121, 'Skeena-Queen Charlotte', NULL, NULL),
(122, 'Squamish-Lillooet', NULL, NULL),
(123, 'Strathcona', NULL, NULL),
(124, 'Sunshine Coast', NULL, NULL),
(125, 'Thompson-Nicola', NULL, NULL),
(126, 'Comox', NULL, NULL),
(127, 'Creston', NULL, NULL),
(128, 'Gibsons', NULL, NULL),
(129, 'Golden', NULL, NULL),
(130, 'Ladysmith', NULL, NULL),
(131, 'Lake Cowichan', NULL, NULL),
(132, 'Oliver', NULL, NULL),
(133, 'Osoyoos', NULL, NULL),
(134, 'Port McNeill', NULL, NULL),
(135, 'Princeton', NULL, NULL),
(136, 'Qualicum Beach', NULL, NULL),
(137, 'Sidney', NULL, NULL),
(138, 'Smithers', NULL, NULL),
(139, 'View Royal', NULL, NULL),
(140, 'Esquimalt', NULL, NULL),
(141, 'Langley', NULL, NULL),
(142, 'Spallumcheen', NULL, NULL),
(143, 'Alert Bay', NULL, NULL),
(144, 'Anmore', NULL, NULL),
(145, 'Ashcroft', NULL, NULL),
(146, 'Belcarra', NULL, NULL),
(147, 'Burns Lake', NULL, NULL),
(148, 'Cache Creek', NULL, NULL),
(149, 'Canal Flats', NULL, NULL),
(150, 'Chase', NULL, NULL),
(151, 'Clinton', NULL, NULL),
(152, 'Cumberland', NULL, NULL),
(153, 'Fraser Lake', NULL, NULL),
(154, 'Fruitvale', NULL, NULL),
(155, 'Gold River', NULL, NULL),
(156, 'Granisle', NULL, NULL),
(157, 'Harrison Hot Springs', NULL, NULL),
(158, 'Hazelton', NULL, NULL),
(159, 'Kaslo', NULL, NULL),
(160, 'Keremeos', NULL, NULL),
(161, 'Lions Bay', NULL, NULL),
(162, 'Lumby', NULL, NULL),
(163, 'Lytton', NULL, NULL),
(164, 'Masset', NULL, NULL),
(165, 'McBride', NULL, NULL),
(166, 'Midway', NULL, NULL),
(167, 'Montrose', NULL, NULL),
(168, 'Nakusp', NULL, NULL),
(169, 'New Denver', NULL, NULL),
(170, 'Pemberton', NULL, NULL),
(171, 'Port Alice', NULL, NULL),
(172, 'Port Clements', NULL, NULL),
(173, 'Pouce Coupe', NULL, NULL),
(174, 'Queen Charlotte', NULL, NULL),
(175, 'Radium Hot Springs', NULL, NULL),
(176, 'Salmo', NULL, NULL),
(177, 'Sayward', NULL, NULL),
(178, 'Silverton', NULL, NULL),
(179, 'Slocan', NULL, NULL),
(180, 'Tahsis', NULL, NULL),
(181, 'Telkwa', NULL, NULL),
(182, 'Valemount', NULL, NULL),
(183, 'Warfield', NULL, NULL),
(184, 'Zeballos', NULL, NULL),
(185, 'Sechelt', NULL, NULL),
(186, 'Jumbo Glacier', NULL, NULL),
(187, 'Sun Peaks', NULL, NULL),
(188, 'Northern Rockies', NULL, NULL),
(189, 'Whistler', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `restrictions`
--

CREATE TABLE `restrictions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `restriction_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `restrictions`
--

INSERT INTO `restrictions` (`id`, `restriction_name`, `created_at`, `updated_at`) VALUES
(1, 'Has Vehicle', NULL, NULL),
(2, 'Rain towers', NULL, NULL),
(3, 'Will Kiss Male', NULL, NULL),
(4, 'Will Kiss Female', NULL, NULL),
(5, 'Will Kiss Partner', NULL, NULL),
(6, 'Will Kiss', NULL, NULL),
(7, 'Cigar', NULL, NULL),
(8, 'Herbal Cigarette', NULL, NULL),
(9, 'Smoke other', NULL, NULL),
(10, 'Cigarette', NULL, NULL),
(11, 'Vape', NULL, NULL),
(12, 'Open Working Day', NULL, NULL),
(13, 'Applicable to Film', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `restriction_perfomers`
--

CREATE TABLE `restriction_perfomers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `performer_id` int(10) UNSIGNED DEFAULT NULL,
  `restriction_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seasons`
--

CREATE TABLE `seasons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `season` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seasons`
--

INSERT INTO `seasons` (`id`, `season`, `created_at`, `updated_at`) VALUES
(1, 'All Season', NULL, NULL),
(2, 'Fall', NULL, NULL),
(3, 'Spring', NULL, NULL),
(4, 'Summer', NULL, NULL),
(5, 'Winter', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` int(10) UNSIGNED NOT NULL,
  `skill_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `performer_id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_video` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skill_gallery_tags`
--

CREATE TABLE `skill_gallery_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skill_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skin_tones`
--

CREATE TABLE `skin_tones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `skin_tone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `skin_tones`
--

INSERT INTO `skin_tones` (`id`, `skin_tone`, `created_at`, `updated_at`) VALUES
(1, 'Black', NULL, NULL),
(2, 'Brown', NULL, NULL),
(3, 'Dark', NULL, NULL),
(4, 'Dark Brown', NULL, NULL),
(5, 'Fair', NULL, NULL),
(6, 'Light', NULL, NULL),
(7, 'Medium', NULL, NULL),
(8, 'Olive', NULL, NULL),
(9, 'Tan', NULL, NULL),
(10, 'Very Fair', NULL, NULL),
(26, 'White', NULL, NULL),
(27, 'Dark/ olive', NULL, NULL),
(28, 'Yellow', NULL, NULL),
(29, 'Olive/fair', NULL, NULL),
(30, 'Light with tan', NULL, NULL),
(31, 'Olive, Caucasian ', NULL, NULL),
(32, 'Tanned', NULL, NULL),
(33, 'Medium-Dark', NULL, NULL),
(34, 'Medium mid tone.  Changing seasonally ', NULL, NULL),
(35, 'light Caramel ', NULL, NULL),
(36, 'Caucasian ', NULL, NULL),
(37, 'White, tanned in summer', NULL, NULL),
(38, 'Medium/Pale with light facial freckles', NULL, NULL),
(39, 'light to latin tan', NULL, NULL),
(40, 'White/Pale', NULL, NULL),
(41, 'Light Olive', NULL, NULL),
(42, 'Black light skinned', NULL, NULL),
(43, 'Biege', NULL, NULL),
(44, 'beige', NULL, NULL),
(45, 'Pale ', NULL, NULL),
(46, 'Light-medium', NULL, NULL),
(47, 'Extremely Pale', NULL, NULL),
(48, 'Very Pale', NULL, NULL),
(49, 'Fiar', NULL, NULL),
(50, 'Light- olive', NULL, NULL),
(51, 'Chocolate brown', NULL, NULL),
(52, 'Warm ', NULL, NULL),
(53, 'White, pale to normal', NULL, NULL),
(54, 'Fair/medium', NULL, NULL),
(55, 'Caucasian/White', NULL, NULL),
(56, 'Light tan to tan', NULL, NULL),
(57, 'Light to medium ', NULL, NULL),
(58, 'Peach', NULL, NULL),
(59, 'medium tan', NULL, NULL),
(60, 'Med/light', NULL, NULL),
(61, 'Mid', NULL, NULL),
(62, 'mid-tan', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_accessories`
--

CREATE TABLE `vehicle_accessories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vehicle_accessory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_accessories`
--

INSERT INTO `vehicle_accessories` (`id`, `vehicle_accessory`, `created_at`, `updated_at`) VALUES
(11, 'ATV Plow', NULL, NULL),
(12, 'Bike Rack (Back)', NULL, NULL),
(13, 'Bike Rack (Roof)', NULL, NULL),
(14, 'Front Jeep Bumpers', NULL, NULL),
(15, 'Front Truck Bumper', NULL, NULL),
(16, 'Front Winch', NULL, NULL),
(17, 'Hard Top', NULL, NULL),
(18, 'Lift Kit', NULL, NULL),
(19, 'Mesh Doors', NULL, NULL),
(20, 'Mounted Flood Lights', NULL, NULL),
(21, 'Rear Jeep Bumpers', NULL, NULL),
(22, 'Rear Truck Job Boxes', NULL, NULL),
(23, 'Rear Winch', NULL, NULL),
(24, 'Roof Racks', NULL, NULL),
(25, 'Soft Top', NULL, NULL),
(26, 'Truck Canopy', NULL, NULL),
(27, 'Winter Tires', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_makes`
--

CREATE TABLE `vehicle_makes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vehicle_make` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_makes`
--

INSERT INTO `vehicle_makes` (`id`, `vehicle_make`, `created_at`, `updated_at`) VALUES
(1, 'Acura', NULL, NULL),
(2, 'Alfa Romeo', NULL, NULL),
(3, 'AMC', NULL, NULL),
(4, 'Aprilla', NULL, NULL),
(5, 'Ariel Atom', NULL, NULL),
(6, 'Aston Martin', NULL, NULL),
(7, 'Audi', NULL, NULL),
(8, 'Austin Healey', NULL, NULL),
(9, 'Bentley', NULL, NULL),
(10, 'BMW', NULL, NULL),
(11, 'Bugatti', NULL, NULL),
(12, 'Buick', NULL, NULL),
(13, 'Cadillac', NULL, NULL),
(14, 'Caterham', NULL, NULL),
(15, 'Chevrolet', NULL, NULL),
(16, 'Chrysler', NULL, NULL),
(17, 'Citroen', NULL, NULL),
(18, 'Daewoo', NULL, NULL),
(19, 'Daihatsu', NULL, NULL),
(20, 'Datsun', NULL, NULL),
(21, 'De Tomaso', NULL, NULL),
(22, 'Dodge', NULL, NULL),
(23, 'Ducati', NULL, NULL),
(24, 'Eagle', NULL, NULL),
(25, 'Ferrari', NULL, NULL),
(26, 'Fiat', NULL, NULL),
(27, 'Fisker', NULL, NULL),
(28, 'Ford', NULL, NULL),
(29, 'Geo', NULL, NULL),
(30, 'GMC', NULL, NULL),
(31, 'Harley Davison', NULL, NULL),
(32, 'Holden', NULL, NULL),
(33, 'Honda', NULL, NULL),
(34, 'Hummer', NULL, NULL),
(35, 'Hyundai', NULL, NULL),
(36, 'Infiniti', NULL, NULL),
(37, 'Isuzu', NULL, NULL),
(38, 'Jaguar', NULL, NULL),
(39, 'Jeep', NULL, NULL),
(40, 'Kawasaki', NULL, NULL),
(41, 'Kia', NULL, NULL),
(42, 'Koenigsegg', NULL, NULL),
(43, 'Lamborghini', NULL, NULL),
(44, 'Lancia', NULL, NULL),
(45, 'Land Rover', NULL, NULL),
(46, 'Lexus', NULL, NULL),
(47, 'Lincoln', NULL, NULL),
(48, 'Lotus', NULL, NULL),
(49, 'Maserati', NULL, NULL),
(50, 'Maybach', NULL, NULL),
(51, 'Mazda', NULL, NULL),
(52, 'McLaren', NULL, NULL),
(53, 'Mercedes', NULL, NULL),
(54, 'Mercury', NULL, NULL),
(55, 'MG', NULL, NULL),
(56, 'Mini', NULL, NULL),
(57, 'Mitsubishi', NULL, NULL),
(58, 'Morgan', NULL, NULL),
(59, 'Mosler/Rossion', NULL, NULL),
(60, 'Nissan', NULL, NULL),
(61, 'Noble', NULL, NULL),
(62, 'Oldsmobile', NULL, NULL),
(63, 'Opel', NULL, NULL),
(64, 'Other', NULL, NULL),
(65, 'Pagani', NULL, NULL),
(66, 'Peugeot', NULL, NULL),
(67, 'Piaggio', NULL, NULL),
(68, 'Plymouth', NULL, NULL),
(69, 'Pontiac', NULL, NULL),
(70, 'Porsche', NULL, NULL),
(71, 'Proton', NULL, NULL),
(72, 'Ram', NULL, NULL),
(73, 'Renault', NULL, NULL),
(74, 'Rolls Royce', NULL, NULL),
(75, 'Saab', NULL, NULL),
(76, 'Saleen', NULL, NULL),
(77, 'Saturn', NULL, NULL),
(78, 'Scion', NULL, NULL),
(79, 'Seat', NULL, NULL),
(80, 'Shelby', NULL, NULL),
(81, 'Skoda', NULL, NULL),
(82, 'Smart', NULL, NULL),
(83, 'Subaru', NULL, NULL),
(84, 'Suzuki', NULL, NULL),
(85, 'Tesla', NULL, NULL),
(86, 'Toyota', NULL, NULL),
(87, 'Triumph', NULL, NULL),
(88, 'Vauxhall', NULL, NULL),
(89, 'Vespa', NULL, NULL),
(90, 'Volkswagen', NULL, NULL),
(91, 'Volvo', NULL, NULL),
(92, 'Westfield', NULL, NULL),
(93, 'Yamaha', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_types`
--

CREATE TABLE `vehicle_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vehicle_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_types`
--

INSERT INTO `vehicle_types` (`id`, `vehicle_type`, `created_at`, `updated_at`) VALUES
(1, 'Camper', NULL, NULL),
(2, 'Collector', NULL, NULL),
(3, 'Compact', NULL, NULL),
(4, 'Convertible', NULL, NULL),
(5, 'Crossover', NULL, NULL),
(6, 'Custom', NULL, NULL),
(7, 'Motorcycle', NULL, NULL),
(8, 'Other', NULL, NULL),
(9, 'Pickup Truck', NULL, NULL),
(10, 'RV', NULL, NULL),
(11, 'Scooter', NULL, NULL),
(12, 'Sedan', NULL, NULL),
(13, 'Sports Car', NULL, NULL),
(14, 'Station Wagon', NULL, NULL),
(15, 'SUV', NULL, NULL),
(16, 'Van', NULL, NULL),
(17, 'Vintage', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wardrobe_accessories`
--

CREATE TABLE `wardrobe_accessories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `wardrobe_accessory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wardrobe_accessories`
--

INSERT INTO `wardrobe_accessories` (`id`, `wardrobe_accessory`, `created_at`, `updated_at`) VALUES
(1, 'Hat', NULL, NULL),
(2, 'Earrings', NULL, NULL),
(3, 'Bag/Purse', NULL, NULL),
(4, 'Shoes', NULL, NULL),
(5, 'Scarves', NULL, NULL),
(6, 'Jewellery', NULL, NULL),
(7, 'Umbrella', NULL, NULL),
(8, 'Hair Accessories', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wardrobe_types`
--

CREATE TABLE `wardrobe_types` (
  `id` int(11) NOT NULL,
  `wardrobe_type` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wardrobe_types`
--

INSERT INTO `wardrobe_types` (`id`, `wardrobe_type`, `created_at`, `updated_at`) VALUES
(1, 'Athletic / Sports / Exercise', '2019-07-03 12:21:56', '0000-00-00'),
(2, 'Casual', '2019-07-03 12:21:56', '0000-00-00'),
(3, 'Upscale Casual', '2019-07-03 12:21:56', '0000-00-00'),
(4, 'Formal', '2019-07-03 12:21:56', '0000-00-00'),
(5, 'Semi-Formal', '2019-07-03 12:21:56', '0000-00-00'),
(6, 'Cocktail Attire', '2019-07-03 12:21:56', '0000-00-00'),
(7, 'Wedding Attire', '2019-07-03 12:21:56', '0000-00-00'),
(8, 'Business', '2019-07-03 12:21:56', '0000-00-00'),
(9, 'Business Casual', '2019-07-03 12:21:56', '0000-00-00'),
(10, 'Traditional Ethnic', '2019-07-03 12:21:56', '0000-00-00'),
(11, 'Country / Western', '2019-07-03 12:21:56', '0000-00-00'),
(12, 'Outdoors / Hiking / Camping', '2019-07-03 12:21:56', '0000-00-00'),
(13, 'Swimwear', '2019-07-03 12:21:56', '0000-00-00'),
(14, 'Costumes', '2019-07-03 12:21:56', '0000-00-00'),
(15, 'Sleepwear', '2019-07-03 12:21:56', '0000-00-00'),
(16, 'Nightclub', '2019-07-03 12:21:56', '0000-00-00'),
(17, 'Outerwear', '2019-07-03 12:21:56', '0000-00-00'),
(18, 'Undergarments', '2019-07-03 12:21:56', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `work_perfomers`
--

CREATE TABLE `work_perfomers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `resume_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appearance`
--
ALTER TABLE `appearance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `appearance_ethnicity_id_foreign` (`ethnicity_id`),
  ADD KEY `appearance_performer_id_foreign` (`performer_id`),
  ADD KEY `appearance_skin_tone_id_foreign` (`skin_tone_id`),
  ADD KEY `appearance_hair_color_id_foreign` (`hair_color_id`),
  ADD KEY `appearance_eyes_id_foreign` (`eyes_id`),
  ADD KEY `appearance_handedness_id_foreign` (`handedness_id`);

--
-- Indexes for table `body_traits`
--
ALTER TABLE `body_traits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `closets`
--
ALTER TABLE `closets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `closets_performer_id_foreign` (`performer_id`);

--
-- Indexes for table `closet_gallery_tags`
--
ALTER TABLE `closet_gallery_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `closet_gallery_tags_closet_id_foreign` (`closet_id`);

--
-- Indexes for table `corporate_admins`
--
ALTER TABLE `corporate_admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `corporate_admins_email_unique` (`email`);

--
-- Indexes for table `ethnicities`
--
ALTER TABLE `ethnicities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eyes`
--
ALTER TABLE `eyes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hair_colors`
--
ALTER TABLE `hair_colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hair_facials`
--
ALTER TABLE `hair_facials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hair_lengths`
--
ALTER TABLE `hair_lengths`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_uniq_cmn_hair_length_name` (`hair_length`) USING BTREE;

--
-- Indexes for table `hair_styles`
--
ALTER TABLE `hair_styles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `handednesses`
--
ALTER TABLE `handednesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internal_staffs`
--
ALTER TABLE `internal_staffs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `internal_staffs_email_unique` (`email`);

--
-- Indexes for table `media_closets`
--
ALTER TABLE `media_closets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_closets_closet_id_foreign` (`closet_id`);

--
-- Indexes for table `media_portfolios`
--
ALTER TABLE `media_portfolios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_portfolios_portfolio_id_foreign` (`portfolio_id`);

--
-- Indexes for table `media_skills`
--
ALTER TABLE `media_skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_skills_skill_id_foreign` (`skill_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nationalities`
--
ALTER TABLE `nationalities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nudity`
--
ALTER TABLE `nudity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nudity_perfomers`
--
ALTER TABLE `nudity_perfomers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nudity_perfomers_performer_id_foreign` (`performer_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `performers`
--
ALTER TABLE `performers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `performers_union_number_unique` (`union_number`);

--
-- Indexes for table `performers_details`
--
ALTER TABLE `performers_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `performers_details_performer_id_foreign` (`performer_id`);

--
-- Indexes for table `performer_agendas`
--
ALTER TABLE `performer_agendas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `performer_availability`
--
ALTER TABLE `performer_availability`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `performer_portfolios`
--
ALTER TABLE `performer_portfolios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `performer_portfolios_performer_id_foreign` (`performer_id`);

--
-- Indexes for table `performer_status`
--
ALTER TABLE `performer_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `performer_unions`
--
ALTER TABLE `performer_unions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pets`
--
ALTER TABLE `pets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `piercings`
--
ALTER TABLE `piercings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_gallery_tags`
--
ALTER TABLE `portfolio_gallery_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_gallery_tags_portfolio_id_foreign` (`portfolio_id`);

--
-- Indexes for table `project_perfomers`
--
ALTER TABLE `project_perfomers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_perfomers_resume_id_foreign` (`resume_id`);

--
-- Indexes for table `project_roles`
--
ALTER TABLE `project_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_roles_project_id_foreign` (`project_id`);

--
-- Indexes for table `props`
--
ALTER TABLE `props`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restrictions`
--
ALTER TABLE `restrictions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restriction_perfomers`
--
ALTER TABLE `restriction_perfomers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `restriction_perfomers_performer_id_foreign` (`performer_id`);

--
-- Indexes for table `seasons`
--
ALTER TABLE `seasons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `skills_performer_id_foreign` (`performer_id`);

--
-- Indexes for table `skill_gallery_tags`
--
ALTER TABLE `skill_gallery_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `skill_gallery_tags_skill_id_foreign` (`skill_id`);

--
-- Indexes for table `skin_tones`
--
ALTER TABLE `skin_tones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_accessories`
--
ALTER TABLE `vehicle_accessories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_makes`
--
ALTER TABLE `vehicle_makes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_types`
--
ALTER TABLE `vehicle_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wardrobe_accessories`
--
ALTER TABLE `wardrobe_accessories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wardrobe_types`
--
ALTER TABLE `wardrobe_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `work_perfomers`
--
ALTER TABLE `work_perfomers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appearance`
--
ALTER TABLE `appearance`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=331;

--
-- AUTO_INCREMENT for table `body_traits`
--
ALTER TABLE `body_traits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `closets`
--
ALTER TABLE `closets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `closet_gallery_tags`
--
ALTER TABLE `closet_gallery_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `corporate_admins`
--
ALTER TABLE `corporate_admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ethnicities`
--
ALTER TABLE `ethnicities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `eyes`
--
ALTER TABLE `eyes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `hair_colors`
--
ALTER TABLE `hair_colors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `hair_facials`
--
ALTER TABLE `hair_facials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `hair_lengths`
--
ALTER TABLE `hair_lengths`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `hair_styles`
--
ALTER TABLE `hair_styles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `handednesses`
--
ALTER TABLE `handednesses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `internal_staffs`
--
ALTER TABLE `internal_staffs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `media_closets`
--
ALTER TABLE `media_closets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media_portfolios`
--
ALTER TABLE `media_portfolios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1207;

--
-- AUTO_INCREMENT for table `media_skills`
--
ALTER TABLE `media_skills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `nationalities`
--
ALTER TABLE `nationalities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=511;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nudity`
--
ALTER TABLE `nudity`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `nudity_perfomers`
--
ALTER TABLE `nudity_perfomers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `performers`
--
ALTER TABLE `performers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=335;

--
-- AUTO_INCREMENT for table `performers_details`
--
ALTER TABLE `performers_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=333;

--
-- AUTO_INCREMENT for table `performer_agendas`
--
ALTER TABLE `performer_agendas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `performer_availability`
--
ALTER TABLE `performer_availability`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `performer_portfolios`
--
ALTER TABLE `performer_portfolios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=313;

--
-- AUTO_INCREMENT for table `performer_status`
--
ALTER TABLE `performer_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `performer_unions`
--
ALTER TABLE `performer_unions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `pets`
--
ALTER TABLE `pets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- AUTO_INCREMENT for table `piercings`
--
ALTER TABLE `piercings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `portfolio_gallery_tags`
--
ALTER TABLE `portfolio_gallery_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_perfomers`
--
ALTER TABLE `project_perfomers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_roles`
--
ALTER TABLE `project_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `props`
--
ALTER TABLE `props`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;

--
-- AUTO_INCREMENT for table `restrictions`
--
ALTER TABLE `restrictions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `restriction_perfomers`
--
ALTER TABLE `restriction_perfomers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seasons`
--
ALTER TABLE `seasons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skill_gallery_tags`
--
ALTER TABLE `skill_gallery_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skin_tones`
--
ALTER TABLE `skin_tones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `vehicle_accessories`
--
ALTER TABLE `vehicle_accessories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `vehicle_makes`
--
ALTER TABLE `vehicle_makes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `vehicle_types`
--
ALTER TABLE `vehicle_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `wardrobe_accessories`
--
ALTER TABLE `wardrobe_accessories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wardrobe_types`
--
ALTER TABLE `wardrobe_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `work_perfomers`
--
ALTER TABLE `work_perfomers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appearance`
--
ALTER TABLE `appearance`
  ADD CONSTRAINT `appearance_ethnicity_id_foreign` FOREIGN KEY (`ethnicity_id`) REFERENCES `ethnicities` (`id`),
  ADD CONSTRAINT `appearance_eyes_id_foreign` FOREIGN KEY (`eyes_id`) REFERENCES `eyes` (`id`),
  ADD CONSTRAINT `appearance_hair_color_id_foreign` FOREIGN KEY (`hair_color_id`) REFERENCES `hair_colors` (`id`),
  ADD CONSTRAINT `appearance_handedness_id_foreign` FOREIGN KEY (`handedness_id`) REFERENCES `handednesses` (`id`),
  ADD CONSTRAINT `appearance_performer_id_foreign` FOREIGN KEY (`performer_id`) REFERENCES `performers` (`id`),
  ADD CONSTRAINT `appearance_skin_tone_id_foreign` FOREIGN KEY (`skin_tone_id`) REFERENCES `skin_tones` (`id`);

--
-- Constraints for table `closets`
--
ALTER TABLE `closets`
  ADD CONSTRAINT `closets_performer_id_foreign` FOREIGN KEY (`performer_id`) REFERENCES `performers` (`id`);

--
-- Constraints for table `closet_gallery_tags`
--
ALTER TABLE `closet_gallery_tags`
  ADD CONSTRAINT `closet_gallery_tags_closet_id_foreign` FOREIGN KEY (`closet_id`) REFERENCES `closets` (`id`);

--
-- Constraints for table `media_closets`
--
ALTER TABLE `media_closets`
  ADD CONSTRAINT `media_closets_closet_id_foreign` FOREIGN KEY (`closet_id`) REFERENCES `closets` (`id`);

--
-- Constraints for table `media_portfolios`
--
ALTER TABLE `media_portfolios`
  ADD CONSTRAINT `media_portfolios_portfolio_id_foreign` FOREIGN KEY (`portfolio_id`) REFERENCES `performer_portfolios` (`id`);

--
-- Constraints for table `media_skills`
--
ALTER TABLE `media_skills`
  ADD CONSTRAINT `media_skills_skill_id_foreign` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`);

--
-- Constraints for table `nudity_perfomers`
--
ALTER TABLE `nudity_perfomers`
  ADD CONSTRAINT `nudity_perfomers_performer_id_foreign` FOREIGN KEY (`performer_id`) REFERENCES `performers` (`id`);

--
-- Constraints for table `performers_details`
--
ALTER TABLE `performers_details`
  ADD CONSTRAINT `performers_details_performer_id_foreign` FOREIGN KEY (`performer_id`) REFERENCES `performers` (`id`);

--
-- Constraints for table `portfolio_gallery_tags`
--
ALTER TABLE `portfolio_gallery_tags`
  ADD CONSTRAINT `portfolio_gallery_tags_portfolio_id_foreign` FOREIGN KEY (`portfolio_id`) REFERENCES `performer_portfolios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
