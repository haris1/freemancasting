<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () { 
    return view('welcome');
});*/

// Auth::routes();
/** start route performers **/
//Route::group(['domain' => 'performers.freemancasting.com','namespace'=>'Performers','as'=>'performers.'],function(){
Route::group(['prefix' => 'performers','namespace'=>'Performers','as'=>'performers.'],function(){

	Route::group(['namespace'=>'Auth'],function(){

		Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
		Route::post('register', 'RegisterController@register')->name('signup');
		Route::get('checkedEmail', 'RegisterController@checkedEmail')->name('checkedEmail');
		Route::get('checkedUnionNumber', 'RegisterController@checkedUnionNumber')->name('checkedUnionNumber');
		Route::get('checkUser', 'LoginController@checkUser')->name('checkUser');

		Route::post('imageUpload', 'RegisterController@imageUpload')->name('imageUpload');
		Route::post('addgroup', 'RegisterController@addGroup')->name('addGroup');
		Route::post('grouplist', 'RegisterController@groupList')->name('groupList');

		Route::get('login', 'LoginController@showLoginForm')->name('login');
		Route::post('login', 'LoginController@login')->name('login');
		Route::post('login', 'LoginController@login')->name('login');
		Route::post('logout', 'LoginController@logout')->name('logout');
		Route::post('performers/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
		Route::get('performers/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
		Route::post('performers/password/update', 'ResetPasswordController@reset')->name('password.update');
		Route::get('performers/password/reset/{token}', 
			'ResetPasswordController@showResetForm')->name('password.reset');
	});    
	

	/** AUTH ROUTE **/

	Route::post('agentlist','PortfolioController@agentlist')->name('agentlist');
	Route::post('addskill','PortfolioController@addskill')->name('addskill');
	Route::post('updateskilltitle','SkillController@updateskilltitle')->name('updateskilltitle');
	Route::post('addskillsection','SkillController@addskillsection')->name('addskillsection');
	Route::post('editskillsection','SkillController@editskillsection')->name('editskillsection');
	Route::post('getSkillTitle','SkillController@getSkillTitle')->name('getSkillTitle');
	Route::post('deletefieldsection','SkillController@deletefieldsection')->name('deletefieldsection');
	Route::post('deleteskill','SkillController@deleteskill')->name('deleteskill');
	Route::post('addinagentlist','PortfolioController@addinagentlist')->name('addinagentlist');
	Route::post('addagentlist','PortfolioController@addagentlist')->name('addagentlist');
	Route::group(['middleware' => ['auth:web']],function(){
		
		Route::get('/','PortfolioController@index')->name('home');
		Route::get('/portfolio','PortfolioController@index')->name('portfolio');
		
	
		Route::get('/closet','ClosetController@index')->name('closet'); 
		Route::get('/skills','SkillController@index')->name('skills');		 
		Route::get('/appearance','AppearanceController@index')->name('appearance');
		Route::post('/appearance','AppearanceController@store'); 
		Route::get('/rental','RentalController@index')->name('rental'); 
		Route::get('/resume','ResumeController@index')->name('resume');	
		Route::post('/deleteresume','ResumeController@deleteResume')->name('deleteresume');

		// delete project and role in resume
		Route::get('/removeresume','ResumeController@removeProject')->name('removeProject');
		Route::get('/removeprojectrole','ResumeController@removeProjectRole')->name('removeProjectRole');

		Route::get('/restriction','RestrictionController@index')->name('restriction'); 
		Route::get('/notes','NoteController@index')->name('note');		 
		Route::get('/documents','DocumentsController@index')->name('documents');
		Route::post('/adddocument','DocumentsController@addDocument')->name('adddocument');
		Route::post('/removedocument','DocumentsController@removeDocument')->name('removedocument');

		Route::get('/editdocument','DocumentsController@editDocument')->name('editDocument');
	});


 
		Route::post('/portfolio/media','PortfolioController@storePortfolioMedia')->name('store.portfolio.media');

		Route::post('/portfolio/images','PortfolioController@storePortfolioImages')->name('store.portfolio.images');

		Route::post('/portfolio/video','PortfolioController@storePortfolioVideo')->name('store.portfolio.video');

		Route::get('/portfolio/gallery/slide/image','PortfolioController@gallerySlideImages')->name('view.gallery.images');

		Route::get('/portfolio/view/gallery/{id}','PortfolioController@viewGallery')->name('view.gallery');

		Route::get('/portfolio/video/show','PortfolioController@portfoliovideoShow')->name('portfolio.video.show');

		Route::post('/portfolio/add/images','PortfolioController@addPortfolioImages')->name('add.portfolio.images'); 

		Route::post('/portfolio/add/video','PortfolioController@addPortfolioVideo')->name('add.portfolio.video');

		Route::post('/portfolio/addtag','PortfolioController@portfolioAddTag')->name('portfolio.addtag');

		Route::get('/portfolio/removetag','PortfolioController@removePortfolioTag')->name('portfolio.removetag');	
		Route::get('/portfolio/imagetagremove','PortfolioController@portfolioImageTagRemove')->name('portfolio.portfolioImageTagRemove');	
		
		Route::get('/portfolio/getSliderImage','PortfolioController@getSliderImage')->name('portfolio.getSliderImage');				
		Route::get('/portfolio/removemedia','PortfolioController@removePortfolioMedia')->name('portfolio.removeMedia');				

		Route::post('/portfolioGalleryUpdate','PortfolioController@portfolioGalleryUpdate')->name('portfolioGalleryUpdate');
		Route::post('/portfolioGalleryremove','PortfolioController@portfolioGalleryRemove')->name('portfolioGalleryRemove');

		Route::post('/update/profile','PortfolioController@updateProfile')->name('updateProfile');
		Route::get('/deleteprofile','PortfolioController@deleteProfile')->name('deleteprofile');

		Route::get('/portfolio/search','PortfolioController@portfolioSearch')->name('portfolio.search');
		Route::get('/portfolio/getTags','PortfolioController@getTags')->name('portfolio.getTags');
		
		Route::get('/portfolio/getTagsList','PortfolioController@getTagsList')->name('portfolio.getTagsList');
			
		//group member add /delete 
		Route::post('/addgroupmember','PortfolioController@addGroupMember')->name('addGroupMember');
		Route::get('/removegroupmember','PortfolioController@removeGroupMember')->name('removeGroupMember');
		Route::get('/editgroupmember','PortfolioController@editGroupMember')->name('editGroupMember');
		Route::post('/updateGroupMember','PortfolioController@updateGroupMember')->name('updateGroupMember');
		
 		Route::post('/closet/images','ClosetController@storeClosetImages')->name('store.closet.images');
		Route::post('/closet/media','ClosetController@storeClosetMedia')->name('store.closet.media');
		Route::get('/closet/view/gallery/{id}','ClosetController@viewGallery')->name('view.closetgallery');
		Route::get('/closet/gallery/slide/image','ClosetController@closetGallerySlideImages')->name('closetslidergallery.images');
		Route::post('/closet/video','ClosetController@storeClosetVideo')->name('store.closet.video');
		Route::get('/closet/video/show','ClosetController@closetvideoShow')->name('closet.video.show');
		Route::post('/closet/add/images','ClosetController@addClosetImages')->name('add.closet.images');
		
		Route::post('/closet/addtag','ClosetController@closetAddTag')->name('closet.addtag');

		Route::get('/closet/removetag','ClosetController@removeClosetTag')->name('closet.removetag');
		Route::post('/closet/add/video','ClosetController@addClosetVideo')->name('add.closet.video');
		Route::post('/closetGalleryUpdate','ClosetController@closetGalleryUpdate')->name('closetGalleryUpdate');
		Route::post('/closetGalleryremove','ClosetController@closetGalleryRemove')->name('closetGalleryRemove');

		Route::get('/closet/removemedia','ClosetController@removeClosetMedia')->name('closet.removeMedia');	
		
		Route::get('/closet/search','ClosetController@closetSearch')->name('closet.search');
		Route::get('/closet/getTags','ClosetController@getTags')->name('closet.getTags');
		

 		Route::post('/skills/images','SkillController@storeSkillImages')->name('store.skill.images');
		
		Route::post('/skills/add/images','SkillController@addSkillImages')->name('add.skill.images');

		Route::post('/skills/video','SkillController@storeSkillVideo')->name('store.skill.video');
		Route::post('/skills/media','SkillController@storeSkillMedia')->name('store.skill.media');
		Route::get('/skills/gallery/slide/image','SkillController@skillGallerySlideImages')->name('skillslidergallery.images');
		Route::get('/skills/view/gallery/{id}','SkillController@viewGallery')->name('view.skillgallery');
		Route::get('/skills/video/show','SkillController@videoShow')->name('skill.video.show');

		Route::post('/skills/addtag','SkillController@addTag')->name('skill.addtag');
		Route::get('/skills/removetag','SkillController@removeTag')->name('skill.removetag');
		Route::post('/skills/add/video','SkillController@addSkillVideo')->name('add.skills.video');
		Route::post('/skillGalleryUpdate','SkillController@skillGalleryUpdate')->name('skillGalleryUpdate');
		Route::get('/skills/removemedia','SkillController@removeSkillMedia')->name('skills.removeMedia');	

		Route::get('/skills/search','SkillController@skillsSearch')->name('skills.search');
		Route::get('/skills/getTags','SkillController@getTags')->name('skills.getTags');
		
 
		Route::post('storeprofile','PortfolioController@storeprofile')->name('storeprofile');

		Route::post('changepassword','PortfolioController@changePassword')->name('changepassword');

 		Route::post('/rental/images','RentalController@storeRentalImages')->name('store.rental.images');
		Route::post('/rental/media','RentalController@storeRantalMedia')->name('store.rental.media');
		Route::post('/rental/addtag','RentalController@rentalAddTag')->name('rental.addtag');

		Route::get('/rental/removetag','RentalController@removeRentalTag')->name('rental.removetag');

		Route::get('/rental/removemedia','RentalController@removeRentalMedia')->name('rental.removeMedia');

		Route::post('/rental/add/images','RentalController@addRentalImages')->name('add.rental.images');

		Route::get('/rental/search','RentalController@rentalSearch')->name('rental.search');
		Route::get('/rental/gallery/slide/image','RentalController@rentalGallerySlideImages')->name('rentalslidergallery.images');
		Route::post('/rentalGalleryUpdate','RentalController@rentalGalleryUpdate')->name('rentalGalleryUpdate');
		Route::get('/rental/getTags','RentalController@getTags')->name('rental.getTags');
	
		Route::get('/rental/view/gallery/{id}','RentalController@viewGallery')->name('view.rentalgallery');

		Route::post('/rentalGalleryremove','RentalController@rentalGalleryRemove')->name('rentalGalleryRemove');

 		Route::get('/projectdata','ResumeController@projectdata')->name('projectdata');
		Route::get('/addworktitle','ResumeController@addworktitle')->name('addworktitle');
		Route::get('/addimdc','ResumeController@addimdc')->name('addimdc');
		Route::get('/roleDelete','ResumeController@roleDelete')->name('roleDelete');
		Route::get('/addrole','ResumeController@addrole')->name('addrole');
		Route::get('/updateRole','ResumeController@updateRole')->name('updateRole');
		Route::get('/addprojecttitle','ResumeController@addprojecttitle')->name('addprojecttitle');
		Route::get('/deleteProject','ResumeController@deleteProject')->name('deleteProject');
		Route::get('/editResume','ResumeController@editResume')->name('editResume');
		Route::get('/resumeSearch','ResumeController@resumeSearch')->name('resumeSearch');
		
		

 
		Route::get('/addrestriction','RestrictionController@addrestriction')->name('addrestriction');
		Route::get('/addnudity','RestrictionController@addnudity')->name('addnudity');

 		

 });



/** end route performers **/


/****************   InternalStaff Authentication ************************/

//Route::group(['domain' => 'talent.freemancasting.com','namespace'=>'InternalStaff','as'=>'internal-staff.'],function(){
Route::group(['prefix' => 'internal-staff','namespace'=>'InternalStaff','as'=>'internal-staff.'],function(){	
	Route::get('/','InternalStaffController@index')->name('internal-staff');
	Route::group(['namespace'=>'Auth'],function(){
		// Route::get('internal-staff/register', 'RegisterController@showRegistrationForm')->name('register');
		// Route::post('internal-staff/register', 'RegisterController@register')->name('signup');
		Route::get('login', 'LoginController@showLoginForm')->name('login');
		Route::post('login', 'LoginController@login')->name('login');
		Route::post('logout', 'LoginController@logout')->name('logout');
		Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
		Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
		Route::post('password/update', 'ResetPasswordController@reset')->name('password.update');
		Route::get('password/reset/{token}', 
			'ResetPasswordController@showResetForm')->name('password.reset');
	});
	
	
	/** AUTH IN INTERNAL STAFF **/
	Route::group(['middleware' => ['auth:internal-staff']],function(){
		Route::get('/','InternalStaffController@performersList')->name('performers.list');
		Route::get('performers/list','InternalStaffController@performersList')->name('performers.list');
		Route::get('add','InternalStaffController@add')->name('add');
		Route::get('performers/detail','InternalStaffController@performersDetail')->name('performers.detail');
		
		/** SCROLL **/
		Route::get('/performersScroll','InternalStaffController@performersScroll')->name('performersScroll');
		Route::get('/performersScrollSearch','SearchController@searchScroll')->name('performersScrollSearch');
		Route::get('/myperformers','InternalStaffController@myperformers')->name('myperformers');

		//list view
		Route::post('/saveflag','InternalStaffController@saveFlag')->name('saveFlag');
		Route::get('/listview','InternalStaffController@listView')->name('listview');

		Route::post('/removeflag','InternalStaffController@removeFlag')->name('removeflag');

		Route::post('/save/result','SearchController@searchResultSave')->name('search.result');		
		Route::get('/search/result/{id}','SearchController@SearchResultDisplay')->name('search.result.display');		

		Route::get('/search','SearchController@search')->name('search');
		Route::get('/search/selectoption','SearchController@searchSelectOption')->name('searchSelectOption');
		Route::get('/portfolio/getSliderImage','InternalStaffController@getSliderImage')->name('portfolio.getSliderImage');
		Route::get('/search/advanceselect','SearchController@advanceSearch')->name('advanceSearch');
		//internal profile Update 
		Route::post('/update/profile','InternalStaffController@updateProfile')->name('updateProfile');
		Route::post('changepassword','InternalStaffController@changePassword')->name('changepassword');
		Route::post('addperformer','InternalStaffController@addperformer')->name('addperformer');
		Route::get('/projectdata','ResumeController@projectdata')->name('projectdata');
Route::get('/savenotes','InternalStaffController@savenotes')->name('savenotes');
		Route::get('/deletenote','InternalStaffController@deletenote')->name('deletenote');
		Route::get('/editnote','InternalStaffController@editnote')->name('editnote');
		Route::get('/updatenote','InternalStaffController@updatenote')->name('updatenote');

	});


	/** END AUTH IN INTERNAL STAFF **/

});

/****************  InternalStaff Authentication ************************/


/****************   Corporate Admin Authentication ************************/

Route::group(['prefix' => 'corporate-admin','namespace'=>'CorporateAdmin','as'=>'corporate-admin.'],function(){
//Route::group(['domain' => 'talent.freemancasting.com','prefix'=> 'corporate-admin','namespace'=>'CorporateAdmin','as'=>'corporate-admin.'],function(){
	
	Route::get('/','CorporateAdminController@index')->name('corporate-admin');

	Route::group(['namespace'=>'Auth'],function(){


		Route::get('login', 'LoginController@showLoginForm')->name('login');
		Route::post('login', 'LoginController@login')->name('login');
		Route::post('logout', 'LoginController@logout')->name('logout');
		Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
		Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
		Route::post('password/update', 'ResetPasswordController@reset')->name('password.update');
		Route::get('password/reset/{token}', 
			'ResetPasswordController@showResetForm')->name('password.reset');
	});



});
//Route::group(['prefix'=> 'corporate-admin','domain' => 'talent.freemancasting.com','namespace'=>'CorporateAdmin' ,'as'=>'corporate-admin.', 'middleware' => ['auth:corporate-admin']], function(){

Route::group(['prefix'=> 'corporate-admin','namespace'=>'CorporateAdmin','as'=>'corporate-admin.', 'middleware' => ['auth:corporate-admin']], function(){
	Route::get('/form','CorporateAdminController@form')->name('form');
	Route::post('/addstaff','CorporateAdminController@store')->name('addstaff');
	Route::get('list','CorporateAdminController@list')->name('list'); 
	Route::get('edit/{id}','CorporateAdminController@edit')->name('edit');
	Route::post('store','CorporateAdminController@save')->name('store');
	Route::get('delete','CorporateAdminController@delete')->name('delete');	
	Route::get('status/{id}','CorporateAdminController@status')->name('status');
	Route::get('skintone','CorporateAdminController@tone')->name('skintone');	
	Route::get('eyes','CorporateAdminController@eyes')->name('eyes');	
	Route::get('availability','CorporateAdminController@Availability')->name('availability');	
	Route::get('performerstatus','CorporateAdminController@performerStatus')->name('performerstatus');	
		
	Route::get('documents','CorporateAdminController@documents')->name('documents');	
	Route::get('piercing','CorporateAdminController@piercing')->name('piercing');	
	Route::get('tattoo','CorporateAdminController@tattoo')->name('tattoo');	
	Route::get('helpdesk','CorporateAdminController@helpdesk')->name('helpdesk');	
	
	Route::get('haircolor','CorporateAdminController@haircolor')->name('haircolor');
	
	//hair facial list

	Route::get('hairfacial','CorporateAdminController@hairfacial')->name('hairfacial');	
	
	//hair Style list

	Route::get('hairstyle','CorporateAdminController@hairstyle')->name('hairstyle');	
	
	//Body Trait list

	Route::get('bodytrait','CorporateAdminController@bodytrait')->name('bodytrait');	
	
	Route::get('ethnicity','CorporateAdminController@ethnicity')->name('ethnicity');	
	Route::get('nationality','CorporateAdminController@nationality')->name('nationality');	
	Route::get('handedness','CorporateAdminController@handedness')->name('handedness');	
		
	Route::get('agent','CorporateAdminController@Agent')->name('agent');	
	Route::get('agency','CorporateAdminController@Agency')->name('agency');	
	Route::get('union','CorporateAdminController@Union')->name('Union');	
	Route::get('flag','CorporateAdminController@Flag')->name('Flag');	
	Route::get('hairlength','CorporateAdminController@hairlength')->name('hairlength');	
	Route::get('vehicle_type','CorporateAdminController@vehicletype')->name('vehicletype');
	Route::get('vehicle_year','CorporateAdminController@vehicleyear')->name('vehicleyear');
	Route::get('vehicle_make','CorporateAdminController@vehiclemake')->name('vehiclemake');
	Route::get('vehicle_model','CorporateAdminController@vehiclemodel')->name('vehiclemodel');
	Route::get('vehicle_condition','CorporateAdminController@vehiclecondition')->name('vehiclecondition');
	Route::get('vehicle_accessories','CorporateAdminController@vehicleaccessories')->name('vehicleaccessories');
	Route::post('addtone','CorporateAdminController@addtone')->name('addtone');
	Route::post('addeyes','CorporateAdminController@addeyes')->name('addeyes');
	Route::post('addpiercing','CorporateAdminController@addPiercing')->name('addpiercing');
	Route::post('addtattoo','CorporateAdminController@addTattoo')->name('addtattoo');
	Route::post('addunion','CorporateAdminController@addunion')->name('addunion');
	Route::post('addagent','CorporateAdminController@addagent')->name('addagent');
	Route::post('addagency','CorporateAdminController@addAgency')->name('addagency');
	Route::post('addflag','CorporateAdminController@addflag')->name('addflag');
	Route::post('addhelpdesk','CorporateAdminController@addhelpdesk')->name('addhelpdesk');
	Route::post('addvehicle_type','CorporateAdminController@addvehicletype')->name('addvehicletype');
	Route::post('addvehicle_make','CorporateAdminController@addvehiclemake')->name('addvehiclemake');
	Route::post('addvehicle_accessories','CorporateAdminController@addvehicleaccessories')->name('addvehicleaccessories');

	Route::post('adddocument','CorporateAdminController@adddocument')->name('adddocument');
	Route::post('addavailability','CorporateAdminController@addavailability')->name('addavailability');
	Route::post('addstatus','CorporateAdminController@addstatus')->name('addstatus');
	Route::post('addtag','CorporateAdminController@addtag')->name('addtag');
	Route::post('addhaircolor','CorporateAdminController@addhaircolor')->name('addhaircolor');
	Route::post('addnationality','CorporateAdminController@addnationality')->name('addnationality');
	Route::post('addethnicity','CorporateAdminController@addethnicity')->name('addethnicity');
	Route::post('addhandedness','CorporateAdminController@addhandedness')->name('addhandedness');
	Route::post('addhairlength','CorporateAdminController@addhairlength')->name('addhairlength');
	Route::post('addhairfacial','CorporateAdminController@addhairfacial')->name('addhairfacial');
	Route::post('addhairstyle','CorporateAdminController@addhairstyle')->name('addhairstyle');
	Route::post('addbodytrait','CorporateAdminController@addbodytrait')->name('addbodytrait');
	Route::post('updatetone','CorporateAdminController@updatetone')->name('updatetone');
	Route::post('updateethnicity','CorporateAdminController@updateethnicity')->name('updateethnicity');
	Route::post('updateeyes','CorporateAdminController@updateeyes')->name('updateeyes');
	Route::post('updatevehicle_type','CorporateAdminController@updatevehicletype')->name('updatevehicletype');
	Route::post('updatevehicle_make','CorporateAdminController@updatevehiclemake')->name('updatevehiclemake');
	Route::post('updatevehicle_accessories','CorporateAdminController@updatevehicleaccessories')->name('updatevehicleaccessories');
	Route::post('updatepiercing','CorporateAdminController@updatepiercing')->name('updatepiercing');
	Route::post('updatetattoo','CorporateAdminController@updatetattoo')->name('updatetattoo');
	Route::post('updateHairColor','CorporateAdminController@updateHairColor')->name('updateHairColor');
	Route::post('updateHairlength','CorporateAdminController@updateHairlength')->name('updateHairlength');
	Route::post('updateHairfacial','CorporateAdminController@updateHairfacial')->name('updateHairfacial');
	Route::post('updateHairstyle','CorporateAdminController@updateHairstyle')->name('updateHairstyle');
	Route::post('updateBodytrait','CorporateAdminController@updateBodytrait')->name('updateBodytrait');
	Route::post('updateHandedness','CorporateAdminController@updateHandedness')->name('updateHandedness');
	Route::post('updatedocument','CorporateAdminController@updatedocument')->name('updatedocument');
	Route::post('updatetag','CorporateAdminController@updatetag')->name('updatetag');
	Route::post('updateflag','CorporateAdminController@updateflag')->name('updateflag');
	Route::post('updateavailability','CorporateAdminController@updateavailability')->name('updateavailability');
	Route::post('updateunion','CorporateAdminController@updateunion')->name('updateunion');
	Route::post('updateagent','CorporateAdminController@updateagent')->name('updateagent');
	Route::post('updateagency','CorporateAdminController@updateAgency')->name('updateagency');
	Route::post('updatenationality','CorporateAdminController@updatenationality')->name('updatenationality');
	Route::get('deletetone','CorporateAdminController@deletetone')->name('deletetone');
	Route::get('deleteagency','CorporateAdminController@deleteAgency')->name('deleteagency');
	Route::get('deleteavailability','CorporateAdminController@deleteavailability')->name('deleteavailability');
	Route::get('deleteStatus','CorporateAdminController@deleteStatus')->name('deleteStatus');
	
	Route::get('deleteeyes','CorporateAdminController@deleteeyes')->name('deleteeyes');
	Route::get('deletevehicle_type','CorporateAdminController@deletevehicletype')->name('deletevehicletype');
	Route::get('deletevehicle_make','CorporateAdminController@deletevehiclemake')->name('deletevehiclemake');
	Route::get('deletevehicle_accessories','CorporateAdminController@deletevehicleaccessories')->name('deletevehicleaccessories');
	Route::get('deleteeyes','CorporateAdminController@deleteeyes')->name('deleteeyes');
	Route::get('deletepiercing','CorporateAdminController@deletePiercing')->name('deletepiercing');
	Route::get('deletetattoo','CorporateAdminController@deleteTattoo')->name('deletetattoo');
	Route::get('deleteflags','CorporateAdminController@deleteflags')->name('deleteflags');
	Route::get('deleteagent','CorporateAdminController@deleteagent')->name('deleteagent');
	Route::get('deleteunion','CorporateAdminController@deleteunion')->name('deleteunion');
	Route::get('deletedocument','CorporateAdminController@deletedocument')->name('deletedocument');
	Route::get('deletetag','CorporateAdminController@deletetag')->name('deletetag');
	Route::get('deletenationality','CorporateAdminController@deletenationality')->name('deletenationality');
	Route::get('deletehaircolor','CorporateAdminController@deletehaircolor')->name('deletehaircolor');
	Route::get('deleteethnicity','CorporateAdminController@deleteethnicity')->name('deleteethnicity');
	Route::get('deletehairlength','CorporateAdminController@deletehairlength')->name('deletehairlength');
	Route::get('deletehairfacial','CorporateAdminController@deletehairfacial')->name('deletehairfacial');
	Route::get('deletehairstyle','CorporateAdminController@deletehairstyle')->name('deletehairstyle');
	Route::get('deletebodytrait','CorporateAdminController@deletebodytrait')->name('deletebodytrait');
	Route::get('deletehandedness','CorporateAdminController@deletehandedness')->name('deletehandedness');
	Route::get('edittone','CorporateAdminController@edittone')->name('edittone');
	Route::get('editeyes','CorporateAdminController@editeyes')->name('editeyes');
	Route::get('editavailability','CorporateAdminController@editavailability')->name('editavailability');

	Route::get('editststus','CorporateAdminController@editStstus')->name('editStstus');
	Route::post('updatestatus','CorporateAdminController@updateStatus')->name('updateStatus');

	Route::get('editvehicle_type','CorporateAdminController@editvehicletype')->name('editvehicletype');
	Route::get('editvehicle_make','CorporateAdminController@editvehiclemake')->name('editvehiclemake');
	Route::get('editvehicle_accessories','CorporateAdminController@editvehicleaccessories')->name('editvehicleaccessories');
	Route::get('editpiercing','CorporateAdminController@editpiercing')->name('editpiercing');
	Route::get('edittattoo','CorporateAdminController@edittattoo')->name('edittattoo');
	Route::get('editethnicity','CorporateAdminController@editethnicity')->name('editethnicity');
	Route::get('editunion','CorporateAdminController@editunion')->name('editunion');
	Route::get('editflag','CorporateAdminController@editflag')->name('editflag');
	Route::get('edithaircolor','CorporateAdminController@edithaircolor')->name('edithaircolor');
	
	Route::get('edithairlength','CorporateAdminController@edithairlength')->name('edithairlength');
	Route::get('edithairstyle','CorporateAdminController@edithairstyle')->name('edithairstyle');
	Route::get('edithairfacial','CorporateAdminController@edithairfacial')->name('edithairfacial');
	Route::get('editbodytrait','CorporateAdminController@editbodytrait')->name('editbodytrait');
	Route::get('tags','CorporateAdminController@tags')->name('tags');
	Route::get('edithandedness','CorporateAdminController@edithandedness')->name('edithandedness');
	Route::get('editagent','CorporateAdminController@editagent')->name('editagent');
	Route::get('editagency','CorporateAdminController@editagency')->name('editagency');
	Route::get('editdocument','CorporateAdminController@editdocument')->name('editdocument');
	Route::get('edittag','CorporateAdminController@edittag')->name('edittag');
	Route::get('editnationality','CorporateAdminController@editnationality')->
	name('editnationality');


});
/****************  End Corporate Admin Authentication ************************/





/****************** Internal Staff Middleware ********************/

//Route::group(['namespace'=>'Performers','domain' => 'talent.freemancasting.com','as'=>'internal-staff.', 'middleware' => ['auth:internal-staff']], function(){

//Route::group(['namespace'=>'Performers','prefix' => 'internal-staff','as'=>'internal-staff.', 'middleware' => ['auth:internal-staff']], function(){

//Route::group(['namespace'=>'Performers','domain' => 'talent.freemancasting.com','as'=>'internal-staff.', 'middleware' => ['auth:internal-staff']], function(){
Route::group(['namespace'=>'Performers','prefix' => 'internal-staff','as'=>'internal-staff.', 'middleware' => ['auth:internal-staff']], function(){

 
/*******************  Performer View  *********************************/
		
		Route::get('/portfolio/{id}','PortfolioController@index')->name('portfolio');
		Route::get('/closet/{id}','ClosetController@index')->name('closet');
		Route::get('/skills/{id}','SkillController@index')->name('skills');
		Route::get('/appearance/{id}','AppearanceController@index')->name('appearance');
		Route::post('/appearance/{id}','AppearanceController@store');
        Route::get('/rental/{id}','RentalController@index')->name('rental');
		Route::get('/resume/{id}','ResumeController@index')->name('resume');
		Route::get('/restriction/{id}','RestrictionController@index')->name('restriction');
  		Route::get('/notes/{id}','NoteController@index')->name('note');
		Route::get('/documents/{id}','DocumentsController@index')->name('documents');
		Route::get('/portfolio/view/gallery/{id}/{pid}','PortfolioController@viewGallery')->name('view.gallery');
		Route::get('/rental/view/gallery/{id}/{pid}','RentalController@viewGallery')->name('view.rentalgallery');
		Route::get('/closet/view/gallery/{id}/{pid}','ClosetController@viewGallery')->name('view.closetgallery');

});
/*****************  Internal Staff Middleware  ******************/

