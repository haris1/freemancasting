-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2019 at 02:22 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_freemancasting`
--

-- --------------------------------------------------------

--
-- Table structure for table `appearance`
--

CREATE TABLE `appearance` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ethnicity_id` bigint(20) UNSIGNED DEFAULT NULL,
  `performer_id` int(10) UNSIGNED DEFAULT NULL,
  `skin_tone_id` bigint(20) UNSIGNED DEFAULT NULL,
  `hair_color_id` bigint(20) UNSIGNED DEFAULT NULL,
  `eyes_id` bigint(20) UNSIGNED DEFAULT NULL,
  `handedness_id` bigint(20) UNSIGNED DEFAULT NULL,
  `hair_length_id` bigint(20) UNSIGNED DEFAULT NULL,
  `nationality_id` int(10) UNSIGNED DEFAULT NULL,
  `height` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bust` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waist` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hips` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dress` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jeans` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shoes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacts` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hair_length` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gloves` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glasses` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tattoos` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jacket` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sleeve` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ring` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pets` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `appearance`
--

INSERT INTO `appearance` (`id`, `ethnicity_id`, `performer_id`, `skin_tone_id`, `hair_color_id`, `eyes_id`, `handedness_id`, `hair_length_id`, `nationality_id`, `height`, `weight`, `bust`, `waist`, `hips`, `dress`, `jeans`, `shoes`, `contacts`, `hair_length`, `gloves`, `glasses`, `gender`, `tattoos`, `collar`, `jacket`, `sleeve`, `pant`, `hat`, `ring`, `size`, `vehicle`, `pets`, `created_at`, `updated_at`) VALUES
(1, 17, NULL, 26, 19, 4, 3, 8, 29, '5 feet 3 inches ', '155', '38 HH', '30', '34', '12', '12', '8', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, '2014-12-01 00:00:00', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(2, 5, NULL, 27, 7, 11, 3, 2, 212, '5\'10\"', '162', NULL, '31', NULL, NULL, '31', '11', 'No', NULL, 'M', 'No', 'Male', 'Yes', '16', '42r', '35.5', NULL, '7.25', '10', NULL, 'Yes', 'No', NULL, NULL),
(3, 5, NULL, NULL, 5, 12, 3, NULL, 29, '5\'2', '110', NULL, NULL, NULL, NULL, NULL, '7.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, '2019-06-05 00:00:00', NULL, 'No', 'Yes', NULL, NULL),
(4, 5, NULL, NULL, 5, 12, 3, NULL, 29, '5\'2', '110', NULL, NULL, NULL, NULL, NULL, '7.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, '2019-06-05 00:00:00', NULL, 'No', 'Yes', NULL, NULL),
(5, 18, NULL, 6, 7, 4, 2, 8, 213, '2019-05-04 00:00:00', '74', NULL, NULL, NULL, NULL, NULL, '2', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, '2019-12-10 00:00:00', NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(6, 6, NULL, NULL, 4, 13, 3, NULL, 29, '4ft4 inches', '90', NULL, NULL, NULL, NULL, NULL, '3', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(7, 5, NULL, 26, 24, 2, 3, 9, 214, '5â€™9â€', '190', NULL, NULL, NULL, NULL, '36', '10', 'No', NULL, 'M', 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(8, 19, NULL, NULL, 4, 1, 3, NULL, 215, '59 in', '103lbs', NULL, NULL, NULL, NULL, NULL, '24 cm', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(9, 5, NULL, NULL, 7, 7, 3, NULL, 29, '6\'', '175', NULL, NULL, NULL, NULL, NULL, '10.5', 'No', NULL, NULL, 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(10, 20, NULL, 26, 25, 14, 3, 2, 29, '2019-01-06 00:00:00', '183', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, 'L', 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(11, 5, NULL, NULL, 5, 2, 3, NULL, 216, '5\'6', '130', NULL, NULL, NULL, NULL, NULL, '7.5', 'No', NULL, 'S', 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(12, 21, NULL, 28, 4, 4, 3, 10, 73, '5 ft 3 inches', '172', '38', '41', '46', 'Size 14', 'Size  14', '6.5', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, 'Large', '22 inches', NULL, 'Medium', '6.5', NULL, 'Yes', 'No', NULL, NULL),
(13, 5, NULL, NULL, 7, 8, 3, 11, 29, '5â€™5', '115', NULL, NULL, NULL, 'Adult S', NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, 'Adult S', NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(14, 5, NULL, 29, 7, 8, 3, 12, 29, '5â€™6', '115', NULL, NULL, NULL, 'Adult S', NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(15, 22, NULL, 1, 4, 4, 3, 2, 217, '5\'. 6\'\'', '137', NULL, '32', NULL, NULL, '32', '11.5', 'No', NULL, 'M', 'No', 'Male', 'No', '15.5', '37', '23', NULL, 'small', '8 to 9', NULL, 'Yes', 'No', NULL, NULL),
(16, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Transgender', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(17, 5, NULL, NULL, 7, 4, 3, 13, 29, '75 in', '190 lbs', NULL, NULL, NULL, NULL, NULL, '10.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(18, 5, NULL, 30, 5, 15, 3, 8, 29, '5â€™9', '165', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(19, 5, NULL, NULL, 7, 8, 3, 2, 218, '6\' 0\"', '202', NULL, NULL, NULL, NULL, '34\"', '10.5', 'No', NULL, 'L', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(20, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(21, 23, NULL, 31, 7, 4, 3, 2, 219, '6', '190', NULL, '32-33', NULL, NULL, '32-33', '9.5-10', 'No', NULL, 'M', 'No', 'Male', 'No', 'M-L', 'M-L(38R-40R)', '34', NULL, '7 3/4, m', '2019-10-09 00:00:00', NULL, 'Yes', 'No', NULL, NULL),
(22, 24, NULL, NULL, 4, 4, 3, 14, 29, '2019-09-05 00:00:00', '175', NULL, NULL, NULL, NULL, NULL, '8.5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(23, 5, NULL, 26, 7, 4, 3, 15, 220, '2000-06-01 00:00:00', '215', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(24, 5, NULL, NULL, 7, 4, 3, NULL, 29, '5â€™10â€', '160', NULL, NULL, NULL, NULL, NULL, '9.5', 'No', NULL, 'M', 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(25, 4, NULL, NULL, 4, 4, 3, 16, 72, '2019-03-06 00:00:00', '195', NULL, NULL, NULL, NULL, NULL, '12', 'No', NULL, 'XL', 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(26, 5, NULL, NULL, 5, 4, 3, NULL, 29, '2019-08-04 00:00:00', '62', NULL, NULL, NULL, NULL, NULL, '4', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(27, 25, NULL, 32, 25, 4, 3, 8, 29, '4\'5\"', '75', NULL, NULL, NULL, 'Xs', NULL, '5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'Youth 12', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(28, 26, NULL, 33, 25, 4, 3, NULL, 29, '5\'6', '185', NULL, NULL, NULL, NULL, NULL, '10W/11', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(29, 20, NULL, NULL, 26, 4, 3, NULL, 29, '2019-05-05 00:00:00', '142', NULL, NULL, NULL, NULL, NULL, '8', 'No', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(30, 5, NULL, NULL, 27, 2, 3, NULL, 29, '6\'2\"', '185', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(31, 27, NULL, 34, 8, 16, 3, 17, 221, '2019-07-05 00:00:00', '135', '32-DD', NULL, NULL, '7', '1932-07-01 00:00:00', '7.5', 'No', NULL, 'M', 'Yes', 'Female', 'No', NULL, '8', NULL, NULL, NULL, '7', NULL, 'Yes', 'No', NULL, NULL),
(32, 17, NULL, 26, 19, 4, 3, 8, 29, '5 feet 3 inches ', '155', '38 HH', '30', '34', '12', '12', '8', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, '2014-12-01 00:00:00', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(33, 5, NULL, 27, 7, 11, 3, 2, 222, '5\'10\"', '162', NULL, '31', NULL, NULL, '31', '11', 'No', NULL, 'M', 'No', 'Male', 'Yes', '16', '42r', '35.5', NULL, '7.25', '10', NULL, 'Yes', 'No', NULL, NULL),
(34, 5, NULL, NULL, 5, 12, 3, NULL, 29, '5\'2', '110', NULL, NULL, NULL, NULL, NULL, '7.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, '2019-06-05 00:00:00', NULL, 'No', 'Yes', NULL, NULL),
(35, 5, NULL, NULL, 5, 12, 3, NULL, 29, '5\'2', '110', NULL, NULL, NULL, NULL, NULL, '7.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, '2019-06-05 00:00:00', NULL, 'No', 'Yes', NULL, NULL),
(36, 18, NULL, 6, 7, 4, 2, 8, 223, '2019-05-04 00:00:00', '74', NULL, NULL, NULL, NULL, NULL, '2', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, '2019-12-10 00:00:00', NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(37, 6, NULL, NULL, 4, 13, 3, NULL, 29, '4ft4 inches', '90', NULL, NULL, NULL, NULL, NULL, '3', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(38, 5, NULL, 26, 24, 2, 3, 9, 224, '5â€™9â€', '190', NULL, NULL, NULL, NULL, '36', '10', 'No', NULL, 'M', 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(39, 19, NULL, NULL, 4, 1, 3, NULL, 225, '59 in', '103lbs', NULL, NULL, NULL, NULL, NULL, '24 cm', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(40, 5, NULL, NULL, 7, 7, 3, NULL, 29, '6\'', '175', NULL, NULL, NULL, NULL, NULL, '10.5', 'No', NULL, NULL, 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(41, 20, NULL, 26, 25, 14, 3, 2, 29, '2019-01-06 00:00:00', '183', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, 'L', 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(42, 5, NULL, NULL, 5, 2, 3, NULL, 226, '5\'6', '130', NULL, NULL, NULL, NULL, NULL, '7.5', 'No', NULL, 'S', 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(43, 21, NULL, 28, 4, 4, 3, 10, 73, '5 ft 3 inches', '172', '38', '41', '46', 'Size 14', 'Size  14', '6.5', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, 'Large', '22 inches', NULL, 'Medium', '6.5', NULL, 'Yes', 'No', NULL, NULL),
(44, 5, NULL, NULL, 7, 8, 3, 11, 29, '5â€™5', '115', NULL, NULL, NULL, 'Adult S', NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, 'Adult S', NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(45, 5, NULL, 29, 7, 8, 3, 12, 29, '5â€™6', '115', NULL, NULL, NULL, 'Adult S', NULL, '9', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(46, 22, NULL, 1, 4, 4, 3, 2, 227, '5\'. 6\'\'', '137', NULL, '32', NULL, NULL, '32', '11.5', 'No', NULL, 'M', 'No', 'Male', 'No', '15.5', '37', '23', NULL, 'small', '8 to 9', NULL, 'Yes', 'No', NULL, NULL),
(47, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Transgender', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(48, 5, NULL, NULL, 7, 4, 3, 13, 29, '75 in', '190 lbs', NULL, NULL, NULL, NULL, NULL, '10.5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(49, 5, NULL, 30, 5, 15, 3, 8, 29, '5â€™9', '165', NULL, NULL, NULL, NULL, NULL, '9', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(50, 5, NULL, NULL, 7, 8, 3, 2, 228, '6\' 0\"', '202', NULL, NULL, NULL, NULL, '34\"', '10.5', 'No', NULL, 'L', 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(51, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(52, 23, NULL, 31, 7, 4, 3, 2, 229, '6', '190', NULL, '32-33', NULL, NULL, '32-33', '9.5-10', 'No', NULL, 'M', 'No', 'Male', 'No', 'M-L', 'M-L(38R-40R)', '34', NULL, '7 3/4, m', '2019-10-09 00:00:00', NULL, 'Yes', 'No', NULL, NULL),
(53, 24, NULL, NULL, 4, 4, 3, 14, 29, '2019-09-05 00:00:00', '175', NULL, NULL, NULL, NULL, NULL, '8.5', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(54, 5, NULL, 26, 7, 4, 3, 15, 230, '2000-06-01 00:00:00', '215', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(55, 5, NULL, NULL, 7, 4, 3, NULL, 29, '5â€™10â€', '160', NULL, NULL, NULL, NULL, NULL, '9.5', 'No', NULL, 'M', 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(56, 4, NULL, NULL, 4, 4, 3, 16, 72, '2019-03-06 00:00:00', '195', NULL, NULL, NULL, NULL, NULL, '12', 'No', NULL, 'XL', 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(57, 5, NULL, NULL, 5, 4, 3, NULL, 29, '2019-08-04 00:00:00', '62', NULL, NULL, NULL, NULL, NULL, '4', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(58, 25, NULL, 32, 25, 4, 3, 8, 29, '4\'5\"', '75', NULL, NULL, NULL, 'Xs', NULL, '5', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, 'Youth 12', NULL, NULL, NULL, 'No', 'Yes', NULL, NULL),
(59, 26, NULL, 33, 25, 4, 3, NULL, 29, '5\'6', '185', NULL, NULL, NULL, NULL, NULL, '10W/11', 'No', NULL, NULL, 'No', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL),
(60, 20, NULL, NULL, 26, 4, 3, NULL, 29, '2019-05-05 00:00:00', '142', NULL, NULL, NULL, NULL, NULL, '8', 'No', NULL, NULL, 'Yes', 'Female', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(61, 5, NULL, NULL, 27, 2, 3, NULL, 29, '6\'2\"', '185', NULL, NULL, NULL, NULL, NULL, '11', 'No', NULL, NULL, 'No', 'Male', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'No', NULL, NULL),
(62, 27, NULL, 34, 8, 16, 3, 17, 231, '2019-07-05 00:00:00', '135', '32-DD', NULL, NULL, '7', '1932-07-01 00:00:00', '7.5', 'No', NULL, 'M', 'Yes', 'Female', 'No', NULL, '8', NULL, NULL, NULL, '7', NULL, 'Yes', 'No', NULL, NULL),
(63, 17, NULL, 26, 19, 4, 3, 8, 29, '5 feet 3 inches ', '155', '38 HH', '30', '34', '12', '12', '8', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, '2014-12-01 00:00:00', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(64, 17, NULL, 26, 19, 4, 3, 8, 29, '5 feet 3 inches ', '155', '38 HH', '30', '34', '12', '12', '8', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, '2014-12-01 00:00:00', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(65, 17, NULL, 26, 19, 4, 3, 8, 29, '5 feet 3 inches ', '155', '38 HH', '30', '34', '12', '12', '8', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, '2014-12-01 00:00:00', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(66, 17, NULL, 26, 19, 4, 3, 8, 29, '5 feet 3 inches ', '155', '38 HH', '30', '34', '12', '12', '8', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, '2014-12-01 00:00:00', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL),
(67, 17, NULL, 26, 19, 4, 3, 8, 29, '5 feet 3 inches ', '155', '38 HH', '30', '34', '12', '12', '8', 'No', NULL, 'M', 'No', 'Female', 'No', NULL, '2014-12-01 00:00:00', NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `body_traits`
--

CREATE TABLE `body_traits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `body_trait` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `body_traits`
--

INSERT INTO `body_traits` (`id`, `body_trait`, `created_at`, `updated_at`) VALUES
(1, 'Albinism', NULL, NULL),
(2, 'Amputation', NULL, NULL),
(3, 'Body Enhancement', NULL, NULL),
(4, 'Body Modification', NULL, NULL),
(5, 'Braces', NULL, NULL),
(6, 'Burn Survivor', NULL, NULL),
(7, 'Extreme Piercing', NULL, NULL),
(8, 'Extreme Tattoo', NULL, NULL),
(9, 'Glass Eye(s)', NULL, NULL),
(10, 'Little Person', NULL, NULL),
(11, 'Piercing', NULL, NULL),
(12, 'Prosthetic', NULL, NULL),
(13, 'Scar', NULL, NULL),
(14, 'Tattoo', NULL, NULL),
(15, 'Teeth (Gold)', NULL, NULL),
(16, 'Teeth (Missing)', NULL, NULL),
(17, 'Vitiligo', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `closets`
--

CREATE TABLE `closets` (
  `id` int(10) UNSIGNED NOT NULL,
  `closet_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripation` text COLLATE utf8mb4_unicode_ci,
  `performer_id` int(10) UNSIGNED NOT NULL,
  `is_video` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `closet_gallery_tags`
--

CREATE TABLE `closet_gallery_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `closet_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `corporate_admins`
--

CREATE TABLE `corporate_admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `corporate_admins`
--

INSERT INTO `corporate_admins` (`id`, `name`, `email`, `password`, `phonenumber`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'dev', 'developer.cor@gmail.com', '$2y$10$eSqN73T.iQDrB3IGrNjVqOUIAkLvmmmgBg/bq6SShxOK3ch3ijQVu', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ethnicities`
--

CREATE TABLE `ethnicities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ethnicity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ethnicities`
--

INSERT INTO `ethnicities` (`id`, `ethnicity`, `created_at`, `updated_at`) VALUES
(1, 'Asian', NULL, NULL),
(2, 'Belizean', NULL, NULL),
(3, 'Bi Racial', NULL, NULL),
(4, 'Black', NULL, NULL),
(5, 'Caucasian', NULL, NULL),
(6, 'East Indian', NULL, NULL),
(7, 'First Nations', NULL, NULL),
(8, 'Hispanic', NULL, NULL),
(9, 'Indian', NULL, NULL),
(10, 'Inuit', NULL, NULL),
(11, 'Middle Eastern', NULL, NULL),
(12, 'New Zealander', NULL, NULL),
(13, 'Other', NULL, NULL),
(14, 'Pacific Islander', NULL, NULL),
(15, 'Polynesian', NULL, NULL),
(16, 'South Asian', NULL, NULL),
(17, ' Caucasian', NULL, NULL),
(18, 'Cacasian', NULL, NULL),
(19, 'Japanese', NULL, NULL),
(20, 'White', NULL, NULL),
(21, 'Canada', NULL, NULL),
(22, 'African', NULL, NULL),
(23, 'North African, Arab', NULL, NULL),
(24, 'Mixed - asian/caucasian', NULL, NULL),
(25, 'Mix ', NULL, NULL),
(26, 'Black - Nigerian', NULL, NULL),
(27, 'Brazilian/Italian', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `eyes`
--

CREATE TABLE `eyes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `eyes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `eyes`
--

INSERT INTO `eyes` (`id`, `eyes`, `created_at`, `updated_at`) VALUES
(1, 'Black', NULL, NULL),
(2, 'Blue', NULL, NULL),
(3, 'Blue-Green', NULL, NULL),
(4, 'Brown', NULL, NULL),
(5, 'Gold', NULL, NULL),
(6, 'Gray', NULL, NULL),
(7, 'Green', NULL, NULL),
(8, 'Hazel', NULL, NULL),
(9, 'Heterochromia', NULL, NULL),
(10, 'Other', NULL, NULL),
(11, ' Brown/ hazel', NULL, NULL),
(12, 'green/blue', NULL, NULL),
(13, 'Dark brown', NULL, NULL),
(14, 'Blue/green', NULL, NULL),
(15, 'Hazle', NULL, NULL),
(16, 'Hazel green', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hair_colors`
--

CREATE TABLE `hair_colors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hair_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hair_colors`
--

INSERT INTO `hair_colors` (`id`, `hair_color`, `created_at`, `updated_at`) VALUES
(1, 'Ash', NULL, NULL),
(2, 'Auburn', NULL, NULL),
(3, 'Bald', NULL, NULL),
(4, 'Black', NULL, NULL),
(5, 'Blonde', NULL, NULL),
(6, 'Blue ', NULL, NULL),
(7, 'Brown', NULL, NULL),
(8, 'Chestnut', NULL, NULL),
(9, 'Copper', NULL, NULL),
(10, 'Dark', NULL, NULL),
(11, 'Dirty', NULL, NULL),
(12, 'Gray', NULL, NULL),
(13, 'Green', NULL, NULL),
(14, 'Honey', NULL, NULL),
(15, 'Light', NULL, NULL),
(16, 'n/a', NULL, NULL),
(17, 'Pink', NULL, NULL),
(18, 'Platinum', NULL, NULL),
(19, 'Purple ', NULL, NULL),
(20, 'Red', NULL, NULL),
(21, 'Salt & Pepper', NULL, NULL),
(22, 'Strawberry', NULL, NULL),
(23, 'White', NULL, NULL),
(24, 'Light brown', NULL, NULL),
(25, 'Dark brown', NULL, NULL),
(26, 'Brown/red', NULL, NULL),
(27, 'Blond', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hair_facials`
--

CREATE TABLE `hair_facials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hair_facial` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hair_facials`
--

INSERT INTO `hair_facials` (`id`, `hair_facial`, `created_at`, `updated_at`) VALUES
(1, 'Beard (Long)', NULL, NULL),
(2, 'Beard (Short)', NULL, NULL),
(3, 'Beard (Styled)', NULL, NULL),
(4, 'Clean-shaven', NULL, NULL),
(5, 'Goatee', NULL, NULL),
(6, 'Moustache', NULL, NULL),
(7, 'Mutton Chops', NULL, NULL),
(8, 'n/a', NULL, NULL),
(9, 'Side Burns', NULL, NULL),
(10, 'Stubble', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hair_lengths`
--

CREATE TABLE `hair_lengths` (
  `id` int(11) NOT NULL,
  `hair_length` varchar(191) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory of common hair lengths for performers';

--
-- Dumping data for table `hair_lengths`
--

INSERT INTO `hair_lengths` (`id`, `hair_length`, `created_at`, `updated_at`) VALUES
(1, 'Bald', '2019-07-05 09:14:11', NULL),
(2, 'Short', '2019-07-05 09:14:11', NULL),
(3, 'Chin', '2019-07-05 09:14:11', NULL),
(4, 'Shoulder', '2019-07-05 09:14:11', NULL),
(5, 'Middle-back', '2019-07-05 09:14:11', NULL),
(6, 'Extremely Long', '2019-07-05 09:14:11', NULL),
(7, 'Shaved Head', '2019-07-05 09:14:11', NULL),
(8, 'Long', '2019-07-05 09:14:11', NULL),
(9, '.5mm-4â€inch', NULL, NULL),
(10, 'Chin length', NULL, NULL),
(11, 'Below shoulders', NULL, NULL),
(12, 'Medium ', NULL, NULL),
(13, 'Very Short', NULL, NULL),
(14, 'Short hair', NULL, NULL),
(15, '3 inches ', NULL, NULL),
(16, 'Jawline', NULL, NULL),
(17, 'Mid back ', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hair_styles`
--

CREATE TABLE `hair_styles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hair_style` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hair_styles`
--

INSERT INTO `hair_styles` (`id`, `hair_style`, `created_at`, `updated_at`) VALUES
(1, 'Afro', NULL, NULL),
(2, 'Bald', NULL, NULL),
(3, 'Bob', NULL, NULL),
(4, 'Braids', NULL, NULL),
(5, 'Buzz Cut', NULL, NULL),
(6, 'Classic', NULL, NULL),
(7, 'Comb Over', NULL, NULL),
(8, 'Cornrows', NULL, NULL),
(9, 'Curly', NULL, NULL),
(10, 'Dreads', NULL, NULL),
(11, 'Fauxhawk', NULL, NULL),
(12, 'Jerry Curl', NULL, NULL),
(13, 'Man Bun', NULL, NULL),
(14, 'Mohawk', NULL, NULL),
(15, 'Mullett', NULL, NULL),
(16, 'n/a', NULL, NULL),
(17, 'Perm', NULL, NULL),
(18, 'Punk / Spiked', NULL, NULL),
(19, 'Solid Color', NULL, NULL),
(20, 'Straight', NULL, NULL),
(21, 'Streaks', NULL, NULL),
(22, 'Wavy', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `handednesses`
--

CREATE TABLE `handednesses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `handedness` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `handednesses`
--

INSERT INTO `handednesses` (`id`, `handedness`, `created_at`, `updated_at`) VALUES
(1, 'Both (Ambdextrous)', NULL, NULL),
(2, 'Left', NULL, NULL),
(3, 'Right', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `internal_staffs`
--

CREATE TABLE `internal_staffs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('enable','disable') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'enable',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `internal_staffs`
--

INSERT INTO `internal_staffs` (`id`, `name`, `email`, `password`, `phonenumber`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(20, 'ankit', 'ankit@gmail.com', '11111111', '(11) 1111-1111', NULL, 'disable', '2019-07-09 02:25:33', '2019-07-09 06:37:42');

-- --------------------------------------------------------

--
-- Table structure for table `media_closets`
--

CREATE TABLE `media_closets` (
  `id` int(10) UNSIGNED NOT NULL,
  `media_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `closet_id` int(10) UNSIGNED NOT NULL,
  `performers_id` int(11) NOT NULL,
  `media_title` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_type` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media_portfolios`
--

CREATE TABLE `media_portfolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `media_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_id` int(10) UNSIGNED NOT NULL,
  `performers_id` int(11) DEFAULT NULL,
  `media_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media_skills`
--

CREATE TABLE `media_skills` (
  `id` int(10) UNSIGNED NOT NULL,
  `media_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skill_id` int(10) UNSIGNED NOT NULL,
  `performers_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2019_07_01_123429_create_media_portfolios_table', 1),
(6, '2019_07_01_123626_create_closets_table', 1),
(7, '2019_07_01_123627_create_media_closets_table', 1),
(9, '2019_07_01_123809_create_media_skills_table', 1),
(10, '2019_07_02_053842_create_corporate_admins_table', 1),
(11, '2019_07_02_053851_create_internal_staffs_table', 1),
(12, '2019_07_02_102243_create_ethnicities_table', 1),
(13, '2019_07_02_102355_create_regions_table', 1),
(14, '2019_07_02_102748_create_performer_status_table', 1),
(15, '2019_07_02_103119_create_skin_tones_table', 1),
(16, '2019_07_02_103237_create_hair_colors_table', 1),
(17, '2019_07_02_103500_create_hair_facials_table', 1),
(18, '2019_07_02_103744_create_body_traits_table', 1),
(19, '2019_07_02_103918_create_hair_styles_table', 1),
(20, '2019_07_02_104019_create_eyes_table', 1),
(21, '2019_07_02_104252_create_handednesses_table', 1),
(22, '2019_07_02_104404_create_vehicle_types_table', 1),
(23, '2019_07_02_104930_create_vehicle_accessories_table', 1),
(24, '2019_07_02_105713_create_vehicle_makes_table', 1),
(25, '2019_07_02_110011_create_seasons_table', 1),
(26, '2019_07_02_110241_create_wardrobe_accessories_table', 1),
(27, '2019_07_02_110444_create_piercings_table', 1),
(28, '2019_07_02_110515_create_props_table', 1),
(29, '2019_07_01_123720_create_skills_table', 2),
(30, '2019_07_09_061910_create_skill_gallery_tag_table', 3),
(31, '2019_07_09_061938_create_portfolio_gallery_tag_table', 3),
(32, '2019_07_09_061952_create_closet_gallery_tag_table', 3),
(34, '2019_07_09_064159_create_performers_details_table', 4),
(36, '2019_07_01_110659_create_performers_table', 5),
(37, '2019_07_11_071748_create_restrictions_table', 6),
(39, '2019_07_11_072045_create_nudity_table', 6),
(40, '2019_07_11_071842_create_notes_table', 7),
(41, '2019_07_12_053213_create_work_perfomers_table', 8),
(42, '2019_07_12_053224_create_project_perfomers_table', 8),
(43, '2019_07_12_053522_create_project_roles_table', 9),
(44, '2019_07_12_082731_create_performer_availability_table', 10),
(45, '2019_07_12_082741_create_performer_agendas_table', 10),
(46, '2019_07_12_082749_create_performer_unions_table', 10),
(47, '2019_07_12_072646_create_nudity_perfomers_table', 11),
(48, '2019_07_13_072605_create_restriction_perfomers_table', 12),
(49, '2019_07_01_123340_create_performers_portfolios_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `nationalities`
--

CREATE TABLE `nationalities` (
  `id` int(11) NOT NULL,
  `nationality` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nationalities`
--

INSERT INTO `nationalities` (`id`, `nationality`, `created_at`, `updated_at`) VALUES
(1, 'Afghan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(2, 'Albanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(3, 'Algerian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(4, 'American', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(5, 'Andorran', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(6, 'Angolan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(7, 'Argentinian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(8, 'Armenian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(9, 'Australian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(10, 'Austrian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(11, 'Azerbaijani', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(12, 'Bahamian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(13, 'Bangladeshi', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(14, 'Barbadian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(15, 'Belgian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(16, 'Belorussian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(17, 'Beninese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(18, 'Bhutanese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(19, 'Bolivian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(20, 'Bosnian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(21, 'Brazilian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(22, 'British', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(23, 'Bruneian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(24, 'Bulgarian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(25, 'Burmese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(26, 'Burundian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(27, 'Cambodian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(28, 'Cameroonian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(29, 'Canadian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(30, 'Chadian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(31, 'Chilean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(32, 'Chinese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(33, 'Colombian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(34, 'Congolese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(35, 'Croatian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(36, 'Cuban', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(37, 'Cypriot', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(38, 'Czech', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(39, 'Dane', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(40, 'Dominican', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(41, 'Dutch', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(42, 'Ecuadorean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(43, 'Egyptian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(44, 'Eritrean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(45, 'Estonian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(46, 'Ethiopian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(47, 'Fijian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(48, 'Filipino', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(49, 'Finn', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(50, 'French', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(51, 'Gabonese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(52, 'Gambian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(53, 'Georgian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(54, 'German', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(55, 'Ghanaian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(56, 'Greek', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(57, 'Grenadian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(58, 'Guatemalan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(59, 'Guinean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(60, 'Guyanese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(61, 'Haitian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(62, 'Honduran', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(63, 'Hungarian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(64, 'Icelander', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(65, 'Indian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(66, 'Indonesian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(67, 'Iranian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(68, 'Iraqi', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(69, 'Irish', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(70, 'Israeli', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(71, 'Italian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(72, 'Jamaican', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(73, 'Japanese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(74, 'Jordanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(75, 'Kazakh', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(76, 'Kenyan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(77, 'Korean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(78, 'Kuwaiti', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(79, 'Laotian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(80, 'Latvian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(81, 'Lebanese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(82, 'Liberian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(83, 'Libyan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(84, 'Liechtensteiner', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(85, 'Lithuanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(86, 'Luxembourger', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(87, 'Macedonian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(88, 'Madagascan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(89, 'Malawian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(90, 'Malaysian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(91, 'Maldivian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(92, 'Malian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(93, 'Maltese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(94, 'Mauritanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(95, 'Mauritian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(96, 'Mexican', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(97, 'Moldovan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(98, 'Monacan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(99, 'Mongolian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(100, 'Montenegrin', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(101, 'Moroccan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(102, 'Mozambican', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(103, 'Namibian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(104, 'Nepalese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(105, 'Nicaraguan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(106, 'Nigerian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(107, 'Norwegian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(108, 'Pakistani', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(109, 'Panamanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(110, 'Paraguayan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(111, 'Peruvian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(112, 'Pole', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(113, 'Portuguese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(114, 'Qatari', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(115, 'Romanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(116, 'Russian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(117, 'Rwandan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(118, 'Salvadorian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(119, 'Saudi', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(120, 'Scot', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(121, 'Senegalese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(122, 'Serbian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(123, 'Singaporean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(124, 'Slovak', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(125, 'Slovenian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(126, 'Somali', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(127, 'Spaniard', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(128, 'Sri Lankan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(129, 'Sudanese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(130, 'Surinamese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(131, 'Swazi', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(132, 'Swede', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(133, 'Swiss', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(134, 'Syrian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(135, 'Tadzhik', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(136, 'Taiwanese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(137, 'Tanzanian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(138, 'Thai', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(139, 'Togolese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(140, 'Trinidadian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(141, 'Tunisian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(142, 'Turk', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(143, 'Ugandan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(144, 'Ukrainian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(145, 'Uruguayan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(146, 'Uzbek', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(147, 'Venezuelan', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(148, 'Vietnamese', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(149, 'Welshman', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(150, 'Yemeni', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(151, 'Yugoslav', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(152, 'Zambian', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(153, 'Zimbabwean', '2019-07-03 11:21:31', '0000-00-00 00:00:00'),
(154, 'Right', '2019-07-11 22:57:06', '2019-07-11 18:30:00'),
(155, 'Right', '2019-07-12 04:29:35', NULL),
(156, 'Left', '2019-07-12 04:29:35', NULL),
(157, 'Right', '2019-07-12 04:29:35', NULL),
(158, 'Right', '2019-07-12 04:29:35', NULL),
(159, 'Right', '2019-07-12 04:29:35', NULL),
(160, 'Right', '2019-07-12 04:29:35', NULL),
(161, 'Right', '2019-07-12 04:29:36', NULL),
(162, 'Right', '2019-07-12 04:29:36', NULL),
(163, 'Right', '2019-07-12 04:29:36', NULL),
(164, 'Right', '2019-07-12 04:29:36', NULL),
(165, 'Right', '2019-07-12 04:31:38', NULL),
(166, 'Left', '2019-07-12 04:31:38', NULL),
(167, 'Right', '2019-07-12 04:31:39', NULL),
(168, 'Right', '2019-07-12 04:31:39', NULL),
(169, 'Right', '2019-07-12 04:31:39', NULL),
(170, 'Right', '2019-07-12 04:31:39', NULL),
(171, 'Right', '2019-07-12 04:31:39', NULL),
(172, 'Right', '2019-07-12 04:31:39', NULL),
(173, 'Right', '2019-07-12 04:31:39', NULL),
(174, 'Right', '2019-07-12 04:31:39', NULL),
(175, 'Right', '2019-07-12 04:40:58', NULL),
(176, 'Left', '2019-07-12 04:40:58', NULL),
(177, 'Right', '2019-07-12 04:40:58', NULL),
(178, 'Right', '2019-07-12 04:40:58', NULL),
(179, 'Right', '2019-07-12 04:40:58', NULL),
(180, 'Right', '2019-07-12 04:40:58', NULL),
(181, 'Right', '2019-07-12 04:40:58', NULL),
(182, 'Right', '2019-07-12 04:40:58', NULL),
(183, 'Right', '2019-07-12 04:40:58', NULL),
(184, 'Right', '2019-07-12 04:40:58', NULL),
(185, 'Right', '2019-07-12 06:07:14', NULL),
(186, 'Left', '2019-07-12 06:09:49', NULL),
(187, 'Right', '2019-07-12 06:11:03', NULL),
(188, 'Right', '2019-07-12 06:11:38', NULL),
(189, 'Right', '2019-07-12 06:13:00', NULL),
(190, 'Right', '2019-07-12 06:16:18', NULL),
(191, 'Right', '2019-07-12 06:17:11', NULL),
(192, 'Right', '2019-07-12 06:17:52', NULL),
(193, 'Right', '2019-07-12 06:19:11', NULL),
(194, 'Right', '2019-07-12 06:24:01', NULL),
(195, 'Right', '2019-07-12 09:03:04', NULL),
(196, 'Right', '2019-07-12 09:03:16', NULL),
(197, 'Left', '2019-07-12 09:07:42', NULL),
(198, 'Left', '2019-07-12 09:08:39', NULL),
(199, 'Right', '2019-07-12 09:10:04', NULL),
(200, 'Right', '2019-07-12 09:11:11', NULL),
(201, 'Right', '2019-07-12 09:12:10', NULL),
(202, 'Right', '2019-07-12 09:22:35', NULL),
(203, 'Right', '2019-07-12 09:22:37', NULL),
(204, 'Right', '2019-07-12 09:28:23', NULL),
(205, 'Right', '2019-07-12 09:28:45', NULL),
(206, 'Left', '2019-07-12 09:32:47', NULL),
(207, 'Left', '2019-07-12 09:33:28', NULL),
(208, 'Right', '2019-07-12 09:35:01', NULL),
(209, 'Right', '2019-07-12 09:35:26', NULL),
(210, 'Right', '2019-07-12 09:35:37', NULL),
(211, 'Right', '2019-07-12 09:38:12', NULL),
(212, 'Right', '2019-07-12 09:44:18', NULL),
(213, 'Left', '2019-07-12 09:44:18', NULL),
(214, 'Right', '2019-07-12 09:44:18', NULL),
(215, 'Right', '2019-07-12 09:44:18', NULL),
(216, 'Right', '2019-07-12 09:44:18', NULL),
(217, 'Right', '2019-07-12 09:44:18', NULL),
(218, 'Right', '2019-07-12 09:44:18', NULL),
(219, 'Right', '2019-07-12 09:44:18', NULL),
(220, 'Right', '2019-07-12 09:44:18', NULL),
(221, 'Right', '2019-07-12 09:44:18', NULL),
(222, 'Right', '2019-07-12 09:46:44', NULL),
(223, 'Left', '2019-07-12 09:46:44', NULL),
(224, 'Right', '2019-07-12 09:46:44', NULL),
(225, 'Right', '2019-07-12 09:46:44', NULL),
(226, 'Right', '2019-07-12 09:46:44', NULL),
(227, 'Right', '2019-07-12 09:46:44', NULL),
(228, 'Right', '2019-07-12 09:46:44', NULL),
(229, 'Right', '2019-07-12 09:46:44', NULL),
(230, 'Right', '2019-07-12 09:46:44', NULL),
(231, 'Right', '2019-07-12 09:46:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `performer_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `notes`, `performer_id`, `created_at`, `updated_at`) VALUES
(1, '<p>test</p>', 3, '2019-07-11 23:05:10', '2019-07-12 01:50:17'),
(2, NULL, 7, '2019-07-11 23:30:13', '2019-07-12 00:06:37'),
(3, '<p>GAURAV</p>', 5, '2019-07-11 23:59:14', '2019-07-12 00:04:20');

-- --------------------------------------------------------

--
-- Table structure for table `nudity`
--

CREATE TABLE `nudity` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nudity_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nudity_perfomers`
--

CREATE TABLE `nudity_perfomers` (
  `id` int(10) UNSIGNED NOT NULL,
  `performer_id` int(10) UNSIGNED DEFAULT NULL,
  `nudity_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('developer.foremost@gmail.com', '$2y$10$8qTjOo3qeicapdWwcJu5uuTIeFgmNDuReHCX9/mn6jDVN7HbS7zKy', '2019-07-03 07:38:43'),
('gaurav@gmail.com', '$2y$10$WMyjtKE7R1fSsCrhkiYJ5OGLpgxBCgmovA2aOpy9mXxrLxl/p3Hb6', '2019-07-12 03:09:16');

-- --------------------------------------------------------

--
-- Table structure for table `performers`
--

CREATE TABLE `performers` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middleName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(99) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postalCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` enum('male','female','transgender') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'male',
  `applicationfor` enum('Adult','Child','Teen') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Adult',
  `instagram` text COLLATE utf8mb4_unicode_ci,
  `twitter` text COLLATE utf8mb4_unicode_ci,
  `linkedin` text COLLATE utf8mb4_unicode_ci,
  `facebook` text COLLATE utf8mb4_unicode_ci,
  `imdb_url` text COLLATE utf8mb4_unicode_ci,
  `agent_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `union_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `union_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `union_det` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload_primary_url` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `performers`
--

INSERT INTO `performers` (`id`, `firstName`, `middleName`, `lastName`, `email`, `password`, `phonenumber`, `address`, `city`, `province`, `postalCode`, `country`, `date_of_birth`, `gender`, `applicationfor`, `instagram`, `twitter`, `linkedin`, `facebook`, `imdb_url`, `agent_name`, `union_name`, `union_number`, `agent`, `union_det`, `upload_primary_url`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Gaurav', NULL, 'R', 'gaurav@gmail.com', '$2y$10$4BQWJ.tNtf6NBGygk5LDfuNhxDTDb.WQOI3tVQCfYpVYWZ93Sl0Wu', '(777) 777-7777', 'Ahemdabad', NULL, NULL, NULL, NULL, '1970-01-01', 'male', 'Adult', 'ggg', 'ggg', 'ggg', 'ggg', NULL, 'Zillion', 'Zillion', '111777', NULL, NULL, '15d284ac5e9407_5d284ac5e9407.jpg', NULL, '2019-07-12 03:24:31', '2019-07-12 03:24:31'),
(3, 'Abidali', NULL, 'Dhabra', 'admin@gmail.com', '$2y$10$4FpnlgWVFU7iUxjv8lJbneWncToS8H5ruAirSpuhDQQhHx1FTWeXC', '(989) 890-8017', 'At & post - Badarpur , Ta-Vadnagar', NULL, NULL, NULL, NULL, '2019-01-07', 'male', 'Adult', 'instagram.com/developer/clients/manage/', 'https://twitter.com/login', 'https://linkedin.com/login', 'https://facebook.com/login', NULL, 'Agent', 'UnionName', '123456', NULL, NULL, '15d284b1772c2d_5d284b1772c2d.jpg', NULL, '2019-07-12 03:28:35', '2019-07-12 03:28:35'),
(84, ' Crystal', 'Ann', 'Tremblay ', 'tapadera@shaw.ca', '1234566788', '6043169338', 'Box 662 ', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1969-11-10', 'female', 'Adult', NULL, 'TREMBLAY ,  CRYSTAL ANN', NULL, 'CrystL Ann', NULL, '', '', NULL, 'No', 'Yes', NULL, NULL, NULL, NULL),
(85, ' Gregory', 'Matthew', 'Coltman', 'darkone021@gmail.com', '1234566788', '7789892634', '#20 17097 64th ave', 'SURREY', 'BC', 'V3S 1Y5', 'Canada', '1974-04-20', 'male', 'Adult', NULL, 'COLTMAN,  GREGORY MATTHEW', NULL, NULL, NULL, '', '', NULL, 'Yes', 'Yes', NULL, NULL, NULL, NULL),
(86, 'Aaliyah', 'Layne', 'Seidlitz', 'aaliyah.layne.seidlitz@gmail.com', '1234566788', '2505896482', '1154 Mason St #6', 'Victoria', 'BC', 'V8T 1A6', 'Canada', '2003-11-19', 'female', 'Adult', 'aaliyah.seidlitz', 'SEIDLITZ, AALIYAH LAYNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(87, 'Aaliyah', 'Layne', 'Seidlitz', 'aaliyah.layne.seidlitz@gmail.com', '1234566788', '2505896482', '1154 Mason St #6', 'Victoria', 'BC', 'V8T 1A6', 'Canada', '2003-11-19', 'female', 'Adult', 'aaliyah.seidlitz', 'SEIDLITZ, AALIYAH LAYNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(88, 'Aaliyah ', NULL, 'Lamirand ', 'jl-rose-86@hotmail.com', '1234566788', '6048326701', '113 scowlitz access road', 'Lake errock', 'BC', 'V0M1N0 ', 'Canada', '2010-06-13', 'female', 'Adult', NULL, 'LAMIRAND , AALIYAH', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(89, 'Aanya', NULL, 'Sethi', 'karishma.sethi@hotmail.com', '1234566788', '2508881285', '526 gurunank lane', 'Victoria ', 'BC', 'V9c 0M2', 'Canada', '2012-10-23', 'female', 'Adult', NULL, 'SETHI, AANYA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(90, 'Aaron', 'Richard,Victor', 'Callihoo', 'aaroncallihoo@yahoo.com', '1234566788', '2508994720', '3639 woodland dr ', 'Port coquitlam ', 'BC', 'V3b4r5', 'Canada', '1982-07-08', 'male', 'Adult', NULL, 'CALLIHOO, AARON RICHARD,VICTOR', NULL, 'Aaron Callihoo', NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(91, 'Aaron', 'Konosuke', 'Takahashi', 'terucanada@hotmail.com', '1234566788', '6047602535', '11058 148A street', 'Surrey', 'BC', 'V3R 3Z4', 'Canada', '2007-09-24', 'male', 'Adult', NULL, 'TAKAHASHI, AARON KONOSUKE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(92, 'Aaron', 'Michael', 'Gelowitz', 'aarongelowitz@shaw.ca', '1234566788', '6047990084', '45513 Market Way, Apt 113', 'Chilliwack', 'BC', 'V2R6A5', 'Canada', '1980-09-19', 'male', 'Adult', NULL, 'GELOWITZ, AARON MICHAEL', NULL, NULL, NULL, '', '', NULL, 'No', 'Yes', NULL, NULL, NULL, NULL),
(93, 'Aaron', 'Joshua ', 'Guenther ', 'aaron.guenther@hotmail.com', '1234566788', '2509455218', '1057 Edgehill place', 'Kamloops', 'BC', 'V2C 0G6', 'Canada', '1994-12-29', 'male', 'Adult', 'Aaronguenther22 ', 'GUENTHER , AARON JOSHUA', NULL, 'Aaron Guenther ', NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(94, 'Abbie', NULL, 'Maslin', 'abbiemaslin399@gmail.com', '1234566788', '6043169925', '6841 Centennial Ave', 'Agassiz', 'BC', 'V0M1A3', 'Canada', '1992-10-26', 'female', 'Adult', NULL, 'MASLIN, ABBIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(95, 'Abby', 'Natsuko', 'Kobayakawa', 'abkob@shaw.ca', '1234566788', '6048386511', '4950 Buxton Street', 'Burnaby', 'BC', 'V5H 1J5', 'Canada', '1958-07-24', 'female', 'Adult', NULL, 'KOBAYAKAWA, ABBY NATSUKO', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(96, 'Abby', 'Louise', 'Lamb', 'rtlamb@shaw.ca', '1234566788', '7782312556', '2813 Mara Drive', 'Coquitlam ', 'BC', 'V3c5t9 ', 'Canada', '2005-06-16', 'female', 'Adult', NULL, 'LAMB, ABBY LOUISE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(97, 'Abby', 'Louise', 'Lamb', 'rtlamb@shaw.ca', '1234566788', '7782312556', '2813 Mara Dr', 'Coquitlam ', 'BC', 'V3c5t9 ', 'Canada', '2005-06-16', 'female', 'Adult', NULL, 'LAMB, ABBY LOUISE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(98, 'Abdourahmane', NULL, 'Wane', 'yvewane7991@gmail.com', '1234566788', '7788751616', '214-1990 west 6th ave. ', 'VANCOUVER', 'BC', 'V6J 4V4', 'Canada', '1972-10-02', 'male', 'Adult', NULL, 'WANE, ABDOURAHMANE', NULL, 'Ab Wane', NULL, '', '', NULL, 'Yes', 'Yes', NULL, NULL, NULL, NULL),
(99, 'Abigayle ', 'Shirley', 'Kotyk', 'trm52002@yahoo.ca', '1234566788', '604-506-7070', '541 Hodgson Rd ', 'Williams Lake ', 'BC', 'V2G 3P8', 'Canada', '2010-09-25', 'transgender', 'Adult', NULL, 'KOTYK, ABIGAYLE  SHIRLEY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(100, 'Adam', NULL, 'Fedyk', 'AFMonarch@gmail.com', '1234566788', '604-358-2994', '1748 E 12th Ave', 'Vancouver', 'BC', 'V5N 2A5', 'Canada', '1988-04-12', 'female', 'Adult', 'https://www.instagram.com/adam.virtue/', 'FEDYK, ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(101, 'Adam', 'Michael', 'Barker', 'adamwheelman@gmail.com', '1234566788', '6049383596', '966 maple st', 'Whiterock', 'BC', 'V4b4m5', 'Canada', '1985-02-24', 'male', 'Adult', NULL, 'BARKER, ADAM MICHAEL', NULL, NULL, NULL, '', '', NULL, 'No', 'Yes', NULL, NULL, NULL, NULL),
(102, 'Adam', 'Evan', 'Brodie', 'adambrodie@ymail.com', '1234566788', '6045183900', '65521 Dogwood Drive', 'Hope', 'BC', 'V0X 1L1 ', 'Canada', '1982-08-25', 'male', 'Adult', NULL, 'BRODIE, ADAM EVAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(103, 'Adam', NULL, 'Muxlow', 'maddmux@gmail.com', '1234566788', '7783447053', '34250 Fraser street', 'Abbotsford', 'BC', 'V2s1x9', 'Canada', '1981-09-15', 'male', 'Adult', NULL, 'MUXLOW, ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(104, 'Adam', NULL, 'Boughanmi', 'boughanmi@gmail.com', '1234566788', '6044401085', '1603-1199 Seymour Street', 'Vancouver', 'BC', 'V6B1k3', 'Canada', '1985-10-11', 'male', 'Adult', NULL, 'BOUGHANMI, ADAM', NULL, 'https://www.facebook.com/boughanmia', NULL, '', '', NULL, 'Yes', 'Yes', NULL, NULL, NULL, NULL),
(105, 'Adam', 'Nicola Takeshi', 'Nishi', 'adamnishi93@gmail.com', '1234566788', '6048375740', '3120 Regent Street', 'Richmond', 'BC', 'V7E 2M4', 'Canada', '1993-10-27', 'male', 'Adult', '@adamnishi', 'NISHI, ADAM NICOLA TAKESHI', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(106, 'Adam', 'Omar', 'Ghomari', 'adamghomari@gmail.com', '1234566788', '6044013690', '1705 Wallace Street', 'Vancouver', 'BC', 'V6R 4J7', 'Canada', '2000-03-10', 'male', 'Adult', 'https://www.instagram.com/adam_ghomari/?hl=en', 'GHOMARI, ADAM OMAR', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(107, 'Adam', NULL, 'Harris', 'adamharris_15@hotmail.com', '1234566788', '2505722326', '924 glenshee place', 'Kamloops ', 'BC', 'V2e1k6', 'Canada', '1988-01-31', 'male', 'Adult', 'Adamharris15', 'HARRIS, ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'Yes', NULL, NULL, NULL, NULL),
(108, 'Adam ', NULL, 'Brown ', 'M.adam.brown87@gmail.com', '1234566788', '2892009903', '1408, 1147 quadra St', 'Victoria', 'BC', 'V8W 2K5', 'Canada', '1987-07-24', 'male', 'Adult', NULL, 'BROWN , ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(109, 'Addison', 'Daen', 'Sawatzky', 'brentsawa@hotmail.com', '1234566788', '6047981458', '2083 Aberdeen Drive', 'Agassiz', 'BC', 'V0M 1A1', 'Canada', '2009-05-21', 'female', 'Adult', NULL, 'SAWATZKY, ADDISON DAEN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(110, 'Addison', 'Lee', 'Pinsonneault', 'ml123456@gmail.com', '1234566788', '2508186688', '771 Canterbury road', 'Victoria', 'BC', 'V8X3E4', 'Canada', '2006-08-14', 'female', 'Adult', NULL, 'PINSONNEAULT, ADDISON LEE', NULL, 'https://www.facebook.com/mandy.lee.1650332', NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(111, 'Adeola', 'Phasilat', 'Mustapha', 'addypmustapha@gmail.com', '1234566788', '6043381341', '201-3031 Kingsway', 'Vancouver', 'BC', 'V5R5J6', 'Canada', '1993-09-01', 'female', 'Adult', NULL, 'MUSTAPHA, ADEOLA PHASILAT', NULL, 'https://www.facebook.com/people/Addy-Mustapha/100015769728562', NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(112, 'Adrean', NULL, 'MacDonald', 'Azilkie55@gmail.com', '1234566788', '6049975940', 'PO Box 23,', 'Lake Errock', 'BC', 'V0M 1N0', 'Canada', '1960-08-04', 'female', 'Adult', NULL, 'MACDONALD, ADREAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(113, 'Adrian', NULL, 'Heim', 'adrian.heim93@gmail.com', '1234566788', '8199939350', '5-547 Herald st. ', 'Victoria', 'BC', 'V8W 1S5', 'Canada', '1993-12-03', 'male', 'Adult', NULL, 'HEIM, ADRIAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(114, 'Adriana', 'Rachel', 'Depaschoal', 'missekko1@gmail.com', '1234566788', '7787235135', '3676 McRae Cresent ', 'Port Coquitlam ', 'BC', 'V3B4P1', 'Canada', '1981-12-07', 'female', 'Adult', 'Esquaredsquad ', 'DEPASCHOAL, ADRIANA RACHEL', NULL, 'Ekko lostnsoundz', NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(115, ' Crystal', 'Ann', 'Tremblay ', 'tapadera@shaw.ca', '1234566788', '6043169338', 'Box 662 ', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1969-11-10', 'female', 'Adult', NULL, 'TREMBLAY ,  CRYSTAL ANN', NULL, 'CrystL Ann', NULL, '', '', NULL, 'No', 'Yes', NULL, NULL, NULL, NULL),
(116, ' Gregory', 'Matthew', 'Coltman', 'darkone021@gmail.com', '1234566788', '7789892634', '#20 17097 64th ave', 'SURREY', 'BC', 'V3S 1Y5', 'Canada', '1974-04-20', 'male', 'Adult', NULL, 'COLTMAN,  GREGORY MATTHEW', NULL, NULL, NULL, '', '', NULL, 'Yes', 'Yes', NULL, NULL, NULL, NULL),
(117, 'Aaliyah', 'Layne', 'Seidlitz', 'aaliyah.layne.seidlitz@gmail.com', '1234566788', '2505896482', '1154 Mason St #6', 'Victoria', 'BC', 'V8T 1A6', 'Canada', '2003-11-19', 'female', 'Adult', 'aaliyah.seidlitz', 'SEIDLITZ, AALIYAH LAYNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(118, 'Aaliyah', 'Layne', 'Seidlitz', 'aaliyah.layne.seidlitz@gmail.com', '1234566788', '2505896482', '1154 Mason St #6', 'Victoria', 'BC', 'V8T 1A6', 'Canada', '2003-11-19', 'female', 'Adult', 'aaliyah.seidlitz', 'SEIDLITZ, AALIYAH LAYNE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(119, 'Aaliyah ', NULL, 'Lamirand ', 'jl-rose-86@hotmail.com', '1234566788', '6048326701', '113 scowlitz access road', 'Lake errock', 'BC', 'V0M1N0 ', 'Canada', '2010-06-13', 'female', 'Adult', NULL, 'LAMIRAND , AALIYAH', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(120, 'Aanya', NULL, 'Sethi', 'karishma.sethi@hotmail.com', '1234566788', '2508881285', '526 gurunank lane', 'Victoria ', 'BC', 'V9c 0M2', 'Canada', '2012-10-23', 'female', 'Adult', NULL, 'SETHI, AANYA', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(121, 'Aaron', 'Richard,Victor', 'Callihoo', 'aaroncallihoo@yahoo.com', '1234566788', '2508994720', '3639 woodland dr ', 'Port coquitlam ', 'BC', 'V3b4r5', 'Canada', '1982-07-08', 'male', 'Adult', NULL, 'CALLIHOO, AARON RICHARD,VICTOR', NULL, 'Aaron Callihoo', NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(122, 'Aaron', 'Konosuke', 'Takahashi', 'terucanada@hotmail.com', '1234566788', '6047602535', '11058 148A street', 'Surrey', 'BC', 'V3R 3Z4', 'Canada', '2007-09-24', 'male', 'Adult', NULL, 'TAKAHASHI, AARON KONOSUKE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(123, 'Aaron', 'Michael', 'Gelowitz', 'aarongelowitz@shaw.ca', '1234566788', '6047990084', '45513 Market Way, Apt 113', 'Chilliwack', 'BC', 'V2R6A5', 'Canada', '1980-09-19', 'male', 'Adult', NULL, 'GELOWITZ, AARON MICHAEL', NULL, NULL, NULL, '', '', NULL, 'No', 'Yes', NULL, NULL, NULL, NULL),
(124, 'Aaron', 'Joshua ', 'Guenther ', 'aaron.guenther@hotmail.com', '1234566788', '2509455218', '1057 Edgehill place', 'Kamloops', 'BC', 'V2C 0G6', 'Canada', '1994-12-29', 'male', 'Adult', 'Aaronguenther22 ', 'GUENTHER , AARON JOSHUA', NULL, 'Aaron Guenther ', NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(125, 'Abbie', NULL, 'Maslin', 'abbiemaslin399@gmail.com', '1234566788', '6043169925', '6841 Centennial Ave', 'Agassiz', 'BC', 'V0M1A3', 'Canada', '1992-10-26', 'female', 'Adult', NULL, 'MASLIN, ABBIE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(126, 'Abby', 'Natsuko', 'Kobayakawa', 'abkob@shaw.ca', '1234566788', '6048386511', '4950 Buxton Street', 'Burnaby', 'BC', 'V5H 1J5', 'Canada', '1958-07-24', 'female', 'Adult', NULL, 'KOBAYAKAWA, ABBY NATSUKO', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(127, 'Abby', 'Louise', 'Lamb', 'rtlamb@shaw.ca', '1234566788', '7782312556', '2813 Mara Drive', 'Coquitlam ', 'BC', 'V3c5t9 ', 'Canada', '2005-06-16', 'female', 'Adult', NULL, 'LAMB, ABBY LOUISE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(128, 'Abby', 'Louise', 'Lamb', 'rtlamb@shaw.ca', '1234566788', '7782312556', '2813 Mara Dr', 'Coquitlam ', 'BC', 'V3c5t9 ', 'Canada', '2005-06-16', 'female', 'Adult', NULL, 'LAMB, ABBY LOUISE', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(129, 'Abdourahmane', NULL, 'Wane', 'yvewane7991@gmail.com', '1234566788', '7788751616', '214-1990 west 6th ave. ', 'VANCOUVER', 'BC', 'V6J 4V4', 'Canada', '1972-10-02', 'male', 'Adult', NULL, 'WANE, ABDOURAHMANE', NULL, 'Ab Wane', NULL, '', '', NULL, 'Yes', 'Yes', NULL, NULL, NULL, NULL),
(130, 'Abigayle ', 'Shirley', 'Kotyk', 'trm52002@yahoo.ca', '1234566788', '604-506-7070', '541 Hodgson Rd ', 'Williams Lake ', 'BC', 'V2G 3P8', 'Canada', '2010-09-25', 'transgender', 'Adult', NULL, 'KOTYK, ABIGAYLE  SHIRLEY', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(131, 'Adam', NULL, 'Fedyk', 'AFMonarch@gmail.com', '1234566788', '604-358-2994', '1748 E 12th Ave', 'Vancouver', 'BC', 'V5N 2A5', 'Canada', '1988-04-12', 'female', 'Adult', 'https://www.instagram.com/adam.virtue/', 'FEDYK, ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(132, 'Adam', 'Michael', 'Barker', 'adamwheelman@gmail.com', '1234566788', '6049383596', '966 maple st', 'Whiterock', 'BC', 'V4b4m5', 'Canada', '1985-02-24', 'male', 'Adult', NULL, 'BARKER, ADAM MICHAEL', NULL, NULL, NULL, '', '', NULL, 'No', 'Yes', NULL, NULL, NULL, NULL),
(133, 'Adam', 'Evan', 'Brodie', 'adambrodie@ymail.com', '1234566788', '6045183900', '65521 Dogwood Drive', 'Hope', 'BC', 'V0X 1L1 ', 'Canada', '1982-08-25', 'male', 'Adult', NULL, 'BRODIE, ADAM EVAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(134, 'Adam', NULL, 'Muxlow', 'maddmux@gmail.com', '1234566788', '7783447053', '34250 Fraser street', 'Abbotsford', 'BC', 'V2s1x9', 'Canada', '1981-09-15', 'male', 'Adult', NULL, 'MUXLOW, ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(135, 'Adam', NULL, 'Boughanmi', 'boughanmi@gmail.com', '1234566788', '6044401085', '1603-1199 Seymour Street', 'Vancouver', 'BC', 'V6B1k3', 'Canada', '1985-10-11', 'male', 'Adult', NULL, 'BOUGHANMI, ADAM', NULL, 'https://www.facebook.com/boughanmia', NULL, '', '', NULL, 'Yes', 'Yes', NULL, NULL, NULL, NULL),
(136, 'Adam', 'Nicola Takeshi', 'Nishi', 'adamnishi93@gmail.com', '1234566788', '6048375740', '3120 Regent Street', 'Richmond', 'BC', 'V7E 2M4', 'Canada', '1993-10-27', 'male', 'Adult', '@adamnishi', 'NISHI, ADAM NICOLA TAKESHI', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(137, 'Adam', 'Omar', 'Ghomari', 'adamghomari@gmail.com', '1234566788', '6044013690', '1705 Wallace Street', 'Vancouver', 'BC', 'V6R 4J7', 'Canada', '2000-03-10', 'male', 'Adult', 'https://www.instagram.com/adam_ghomari/?hl=en', 'GHOMARI, ADAM OMAR', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(138, 'Adam', NULL, 'Harris', 'adamharris_15@hotmail.com', '1234566788', '2505722326', '924 glenshee place', 'Kamloops ', 'BC', 'V2e1k6', 'Canada', '1988-01-31', 'male', 'Adult', 'Adamharris15', 'HARRIS, ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'Yes', NULL, NULL, NULL, NULL),
(139, 'Adam ', NULL, 'Brown ', 'M.adam.brown87@gmail.com', '1234566788', '2892009903', '1408, 1147 quadra St', 'Victoria', 'BC', 'V8W 2K5', 'Canada', '1987-07-24', 'male', 'Adult', NULL, 'BROWN , ADAM', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(140, 'Addison', 'Daen', 'Sawatzky', 'brentsawa@hotmail.com', '1234566788', '6047981458', '2083 Aberdeen Drive', 'Agassiz', 'BC', 'V0M 1A1', 'Canada', '2009-05-21', 'female', 'Adult', NULL, 'SAWATZKY, ADDISON DAEN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(141, 'Addison', 'Lee', 'Pinsonneault', 'ml123456@gmail.com', '1234566788', '2508186688', '771 Canterbury road', 'Victoria', 'BC', 'V8X3E4', 'Canada', '2006-08-14', 'female', 'Adult', NULL, 'PINSONNEAULT, ADDISON LEE', NULL, 'https://www.facebook.com/mandy.lee.1650332', NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(142, 'Adeola', 'Phasilat', 'Mustapha', 'addypmustapha@gmail.com', '1234566788', '6043381341', '201-3031 Kingsway', 'Vancouver', 'BC', 'V5R5J6', 'Canada', '1993-09-01', 'female', 'Adult', NULL, 'MUSTAPHA, ADEOLA PHASILAT', NULL, 'https://www.facebook.com/people/Addy-Mustapha/100015769728562', NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(143, 'Adrean', NULL, 'MacDonald', 'Azilkie55@gmail.com', '1234566788', '6049975940', 'PO Box 23,', 'Lake Errock', 'BC', 'V0M 1N0', 'Canada', '1960-08-04', 'female', 'Adult', NULL, 'MACDONALD, ADREAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(144, 'Adrian', NULL, 'Heim', 'adrian.heim93@gmail.com', '1234566788', '8199939350', '5-547 Herald st. ', 'Victoria', 'BC', 'V8W 1S5', 'Canada', '1993-12-03', 'male', 'Adult', NULL, 'HEIM, ADRIAN', NULL, NULL, NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(145, 'Adriana', 'Rachel', 'Depaschoal', 'missekko1@gmail.com', '1234566788', '7787235135', '3676 McRae Cresent ', 'Port Coquitlam ', 'BC', 'V3B4P1', 'Canada', '1981-12-07', 'female', 'Adult', 'Esquaredsquad ', 'DEPASCHOAL, ADRIANA RACHEL', NULL, 'Ekko lostnsoundz', NULL, '', '', NULL, 'No', 'No', NULL, NULL, NULL, NULL),
(146, ' Crystal', 'Ann', 'Tremblay ', 'tapadera@shaw.ca', '1234566788', '6043169338', 'Box 662 ', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1969-11-10', 'female', 'Adult', NULL, 'TREMBLAY ,  CRYSTAL ANN', NULL, 'CrystL Ann', NULL, '', '', NULL, 'No', 'Yes', '95450D90-F49B-44EB-95B6-D5CFCEA44461.jpeg', NULL, NULL, '2019-07-12 04:23:20'),
(147, ' Crystal', 'Ann', 'Tremblay ', 'tapadera@shaw.ca', '1234566788', '6043169338', 'Box 662 ', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1969-11-10', 'female', 'Adult', NULL, 'TREMBLAY ,  CRYSTAL ANN', NULL, 'CrystL Ann', NULL, '', '', NULL, 'No', 'Yes', NULL, NULL, NULL, NULL),
(148, ' Crystal', 'Ann', 'Tremblay ', 'tapadera@shaw.ca', '1234566788', '6043169338', 'Box 662 ', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1969-11-10', 'female', 'Adult', NULL, 'TREMBLAY ,  CRYSTAL ANN', NULL, 'CrystL Ann', NULL, '', '', NULL, 'No', 'Yes', '95450D90-F49B-44EB-95B6-D5CFCEA44461.jpeg', NULL, NULL, '2019-07-12 04:30:10'),
(149, ' Crystal', 'Ann', 'Tremblay ', 'tapadera@shaw.ca', '1234566788', '6043169338', 'Box 662 ', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1969-11-10', 'female', 'Adult', NULL, 'TREMBLAY ,  CRYSTAL ANN', NULL, 'CrystL Ann', NULL, '', '', NULL, 'No', 'Yes', '95450D90-F49B-44EB-95B6-D5CFCEA44461.jpeg', NULL, NULL, '2019-07-12 04:32:32'),
(150, ' Crystal', 'Ann', 'Tremblay ', 'tapadera@shaw.ca', '1234566788', '6043169338', 'Box 662 ', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1969-11-10', 'female', 'Adult', NULL, 'TREMBLAY ,  CRYSTAL ANN', NULL, 'CrystL Ann', NULL, '', '', NULL, 'No', 'Yes', NULL, NULL, NULL, NULL),
(151, ' Crystal', 'Ann', 'Tremblay ', 'tapadera@shaw.ca', '1234566788', '6043169338', 'Box 662 ', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1969-11-10', 'female', 'Adult', NULL, 'TREMBLAY ,  CRYSTAL ANN', NULL, 'CrystL Ann', NULL, '', '', NULL, 'No', 'Yes', '95450D90-F49B-44EB-95B6-D5CFCEA44461.jpeg', NULL, NULL, '2019-07-12 04:41:57'),
(152, ' Crystal', 'Ann', 'Tremblay ', 'tapadera@shaw.ca', '1234566788', '6043169338', 'Box 662 ', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1969-11-10', 'female', 'Adult', NULL, 'TREMBLAY ,  CRYSTAL ANN', NULL, 'CrystL Ann', NULL, '', '', NULL, 'No', 'Yes', NULL, NULL, NULL, NULL),
(153, ' Crystal', 'Ann', 'Tremblay ', 'tapadera@shaw.ca', '1234566788', '6043169338', 'Box 662 ', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1969-11-10', 'female', 'Adult', NULL, 'TREMBLAY ,  CRYSTAL ANN', NULL, 'CrystL Ann', NULL, '', '', NULL, 'No', 'Yes', '95450D90-F49B-44EB-95B6-D5CFCEA44461.jpeg', NULL, NULL, '2019-07-12 05:00:30'),
(154, ' Crystal', 'Ann', 'Tremblay ', 'tapadera@shaw.ca', '1234566788', '6043169338', 'Box 662 ', 'Harrison Hot Springs', 'BC', 'V0M 1K0', 'Canada', '1969-11-10', 'female', 'Adult', NULL, 'TREMBLAY ,  CRYSTAL ANN', NULL, 'CrystL Ann', NULL, '', '', NULL, 'No', 'Yes', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `performers_details`
--

CREATE TABLE `performers_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hairStyle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hairFacial` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bgPerformerDetails` text COLLATE utf8mb4_unicode_ci,
  `unionAffiliations` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unionNum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visa_student` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visa_international` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visa_open` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visa_film` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visa_film_other` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shirt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agentDetails` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `castingTitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cdnCitizenship` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dayPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eveningPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailSecondary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `applicationFor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral1_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral1_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral2_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral2_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContact1_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContact1_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContact1_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContact2_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContact2_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergencyContact2_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driversLicense` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leftHandDrive` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `neck` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chest` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insea` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resume` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bgPerformer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `availability` text COLLATE utf8mb4_unicode_ci,
  `cdnCitizen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bcResident` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taxes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `languageFirst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `languageSecond` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `performer_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `performers_details`
--

INSERT INTO `performers_details` (`id`, `hairStyle`, `hairFacial`, `bgPerformerDetails`, `unionAffiliations`, `unionNum`, `visa_student`, `visa_international`, `visa_open`, `visa_film`, `visa_film_other`, `shirt`, `agentDetails`, `castingTitle`, `job`, `cdnCitizenship`, `visa`, `dayPhone`, `eveningPhone`, `emailSecondary`, `applicationFor`, `referral1_name`, `referral1_phone`, `referral2_name`, `referral2_phone`, `emergencyContact1_name`, `emergencyContact1_phone`, `emergencyContact1_email`, `emergencyContact2_name`, `emergencyContact2_phone`, `emergencyContact2_email`, `driversLicense`, `leftHandDrive`, `neck`, `chest`, `insea`, `resume`, `bgPerformer`, `availability`, `cdnCitizen`, `bcResident`, `taxes`, `sin`, `languageFirst`, `languageSecond`, `performer_id`, `created_at`, `updated_at`) VALUES
(1, 'Long', 'No ', NULL, 'Full Union', 'BCGEU ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6043169338', '6043169338', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Keith  Anderson ', '6047984968', 'foanders219@hotmail.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Would love the opportunity to be involved in this and experience something fun !', 'Canadian Citizen', 'Yes', 'Yes', '944', 'English', NULL, 84, NULL, NULL),
(2, 'Military ', 'Short beard ( will shave for appropriate rolls)', 'The good doctor  ( continuity doctor in suit several episodes)\n \nColony   resistance fighter (12days) \n                 Grey hat soldier.   ( 10 days)\n\nRiverdale multiple episodes teacher, bobcat, \n                   and drive passerby.\n\nMan in the high castle. Multiple days \n                      Nazi scientist/ soldier\n\nProject blue book. multiple days passerby\n\nA million little things.  Multiple days\n                     Family guest. \n\n Multiple hallmark MOW. Passerby\n\n Travels.    Emergency patient.  Arrested by \n                     main charter. Sae stunt\n\n Timeless. Passerby\n\n Dirk Gently.  Multiple days Queens guard \n                        wendimoore ( featured)\n \n The flash. Policeman with mustache\n                    ( Featured )\n\n Supergirl.   Passerby\n\n Arrow.       Multiple days  Russian henchmen, \n                    passerby\n\n X- files.  Multiple days. Astronaut. \n                 ( photo shoot), passerby', 'Ubcp', 'Ex-04169', NULL, NULL, NULL, NULL, NULL, NULL, 'HellKat talent\nPh# 604-816-4528\nKatie@hellkattalent.ca', NULL, NULL, NULL, NULL, '7789892634', '6045760038', NULL, 'Adult', 'Katie bowdring', '6045760038', 'Rick Coltman', '6045339392', 'Katie bowdring', '6045760038', 'ktk@yahoo.ca', 'Rick Coltman', '6045339392', 'rickcoltman@hotmail.com', 'Yes', 'Yes', '16', '42', NULL, 'No', 'Yes', 'Monday to Saturday.  Sundays stunt training', 'Canadian Citizen', 'Yes', 'Yes', '5866', 'English', NULL, 85, NULL, NULL),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kids ages 6-19 in Victoria,BC', NULL, NULL, NULL, '2505896482', '2505896482', 'ryannegirard@hotmail.ca', 'Teen', NULL, NULL, NULL, NULL, 'Ryanne Girard', '2505896482', 'ryannegirard@hotmail.ca', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'I believe I am free for the whole two weeks of the shot (July 1st-15th )', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 86, NULL, NULL),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kids ages 6-19 in Victoria,BC', NULL, NULL, NULL, '2505896482', '2505896482', 'ryannegirard@hotmail.ca', 'Teen', NULL, NULL, NULL, NULL, 'Ryanne Girard', '2505896482', 'ryannegirard@hotmail.ca', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'I believe I am free for the whole two weeks of the shot (July 1st-15th )', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 87, NULL, NULL),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '6048326701', '6048326701', NULL, 'Child', NULL, NULL, NULL, NULL, 'Jackie louis', '6046150067', 'ja_louis@yahoo.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 88, NULL, NULL),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '2508881285', '2508881285', NULL, 'Child', 'Gillian Croft of spotlight academy', NULL, NULL, NULL, 'Karishma sethi', '2508881285', 'karishma.sethi@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'July 1-10 and August 1-31', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Hindi', 89, NULL, NULL),
(7, 'Zero fade Ceaser', 'Thin chin strap', 'Broken Badges , The Wishing Tree , J. K. Rowling story', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Extra', NULL, NULL, NULL, '6049448567', '2508994720', 'aaroncallihoo@yahoo.com', 'Adult', 'Charlene Callihoo the Good Dr', '6047905356', 'Jake Callihoo , Twilight Zone ', '6042304429', 'Donna Callihoo', '7783887428', 'dccal1@hotmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'At the Moment my schedule is completley open as i have taken time off from Physically active work to focas on healing from knee ingury ,while looking into finding the right Barber School for me . I have been cutting mens hair from my home for almost 5 years and i would love to make it my carreer .So this would b nice and easy on my knee + i love the people and the sceen , ive always had fun being in the back ground .', 'Canadian Citizen', 'Yes', 'Yes', '4937', 'English', 'English', 90, NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '6045851976', '6047602535', NULL, 'Child', 'Terumi Takahashi', NULL, 'Terumi Takahashi', NULL, 'Terumi Takahashi', '6047602535', 'terucanada@hotmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Fluently speak Japanese\nAvailable to write Japanese \nGood sing \n', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', NULL, 91, NULL, NULL),
(9, NULL, NULL, NULL, 'Full Union', '1271', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6047990084', '6047990084', 'aarongelowitz@shaw.ca', 'Adult', NULL, NULL, NULL, NULL, 'Sharon Goldthorp', '6043167315', 'britshaw@shaw.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'march 25-29 7am-7pm', 'Canadian Citizen', 'Yes', 'Yes', '8298', 'ENGLISH', NULL, 92, NULL, NULL),
(10, 'Short clean cut', 'Clean shaven ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kamlooops', NULL, NULL, NULL, '2509455218', '2509455218', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Adam guenther', '17786827487', 'adam_guenther@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Friday-sunday', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 93, NULL, NULL),
(11, NULL, NULL, 'IBABC advert', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk in Agassiz, BC', NULL, NULL, NULL, '6043169925', '6043169925', 'abbiemaslin399@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Freddie Maslin', '6043168970', 'freddiemaslin399@hotmail.com', 'Dean Cradock', '6047956865', NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Evenings ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 94, NULL, NULL),
(12, 'Short', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '6048386511', '6044356511', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Martin Kobayakawa', '6044356511', 'mkobayakawa@shaw.ca', NULL, NULL, NULL, 'Yes', 'Yes', '14.5', NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'Yes', '7886', 'English', 'Japanese', 95, NULL, NULL),
(13, NULL, NULL, 'Netflix Lost in Space S2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7782312556', '6045522556', NULL, 'Teen', NULL, NULL, NULL, NULL, 'Tina LAMB', '7782312556', 'rtlamb@shaw.ca', 'Rick Lamb', '7782312813', 'rtlamb@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Weekdays and weekends', 'Canadian Citizen', 'Yes', 'Yes', '2233', 'English ', NULL, 96, NULL, NULL),
(14, NULL, 'No', 'Netflix Lost in Space', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ' Boys & Girls Ages 12-16 Years Old Posting', NULL, NULL, NULL, '7782312556', '6045522556', 'tinalamb2813@gmail.com', 'Teen', NULL, NULL, NULL, NULL, 'Tina Lamb-mom', '7782312556', 'rtlamb@shaw.ca', 'Rick Lamb-dad', '7782312813', 'spuzzum71@gmail.com', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Available any day/time', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'None', 97, NULL, NULL),
(15, 'short', 'goatee', 'I have been working as a background worker for nine years. A few of the shows I have worked on are: Prison Break, Falling Skies, Godzilla, Flash, Arrow, Commercials, Psyche, Once Upon a Time, Elysium, and many more. ', 'Apprentice Union', '26150', NULL, NULL, NULL, NULL, NULL, NULL, 'Dallas Talent, number phone 6048360222', 'African- Canadian, Caribbean- Canadian and Mixed Race Males & Females Ages 17-65', NULL, NULL, NULL, '7788751616', '7788751616', 'yvewane7991@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Gina Wane', '6048741614', 'hdutot@hotmail.com', 'Gina Wane', '7788771616', 'hdutot@hotmail.com', 'Yes', 'Yes', '15.5', '36.5', NULL, 'Yes', 'Yes', 'Monday to Sunday ', 'Canadian Citizen', 'Yes', 'Yes', '750446072', 'French', 'English', 98, NULL, NULL),
(16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '604-506-7070', '604-506-7070', NULL, 'Child', 'Garry Dersksen Reel kids ', '604-465-8144', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'No', NULL, 'English', NULL, 99, NULL, NULL),
(17, 'Buzz', 'Clean or Scruff', 'This was in 2009.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-358-2994', '604-358-2994', NULL, 'Adult', 'David Gauthier', '778-952-5905', 'Andy Suganda', '778-892-5080', 'Sam Fedyk', '403-869-8783', 'samfedyk@shaw.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'Yes', 'Currently: any day, any time.', 'Canadian Citizen', 'Yes', 'Yes', '4163', 'English', 'French', 100, NULL, NULL),
(18, 'Surf', '5oclock', NULL, 'Iatse loc 891 carpenters', 'Iatse loc 891', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6049383696', '6049383596', NULL, 'Adult', 'Mike bouchard', '6045546611', 'Tom clifford', '6043179467', 'Kelvie barker', '6043170835', 'kelvieb@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 101, NULL, NULL),
(19, NULL, 'Stubble', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IN HOPE, BC: Small Town People with Lots of Character Ages 35 to 60', NULL, NULL, NULL, '6045183900', '6045183900', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Melanie Skolovy-Brodie', '7788781776', 'melanieanns@live.com', 'Bob Brodie', '6043088412', NULL, 'No', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '6326', 'English', 'N/A', 102, NULL, NULL),
(20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7783447053', '7783447053', NULL, 'Adult', 'Julie wilson', '+1 (604) 649-6960', 'Katie ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '4808', 'English', NULL, 103, NULL, NULL),
(21, 'Short style, Military, police style', 'upon request', 'Most of the TV shows and Films shot in the lower mainland since 2010 to 2017. http://www.agencyclick.com/adamboughanmi', 'Full Union', '60479', NULL, NULL, NULL, NULL, NULL, NULL, 'Locol Color :604-685-0315', 'Background acting, SAE, Stand-in, Acting.', NULL, NULL, NULL, '6044401085', '6044401085', 'boughanmi@gmail.com', 'Adult', 'Local Color', '604-685-0315', NULL, NULL, '911', '911', '911@911.com', NULL, NULL, NULL, 'Yes', 'Yes', '15.5-16', 'M,42', NULL, 'Yes', 'Yes', 'Weekend Mostly from Friday 6PM to Sunday 11PM and, anytime on special projects only.', 'Canadian Citizen', 'Yes', 'Yes', '8531', 'French, Arabic', 'English', 104, NULL, NULL),
(22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese man extra', NULL, NULL, NULL, '6048375740', '6048375740', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Gerry Nishi', '6048399320', 'adamnishi93@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', 'Weekdays after 5:00pm\nWeekends any time', 'Canadian Citizen', 'Yes', 'Yes', '9330', 'English', NULL, 105, NULL, NULL),
(23, 'Curly ', 'Sruff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Anything ', NULL, NULL, NULL, '6044013690', '6044013690', 'adamghomari@gmail.com', 'Adult', 'Sara', '7789960494', 'Sara\'s talent partner ', '7783207241', 'Tamara Ghomari', '6042216029', 'tghomari@yahoo.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Currently always available', 'Canadian Citizen', 'Yes', 'Yes', '6263', 'English ', 'French ', 106, NULL, NULL),
(24, NULL, NULL, NULL, 'Full Union', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kamloops extra', NULL, NULL, NULL, '2505722326', '2505722326', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Greg', '2508191369', 'adamharris_15@hotmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'I work 4 on 4 off. I could do 8 Days of the extra roles during Kamloops filming ', 'Canadian Citizen', 'Yes', 'Yes', '2880', 'English', NULL, 107, NULL, NULL),
(25, 'Braids/pulled back', NULL, 'Xmas bells are ringing, the last bridesmaid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '2892009903', '2892009903', 'M.adam.brown87@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Alicia Brown ', '9057064683', 'alicia.brown16@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'As needed ', 'Canadian Citizen', 'Yes', 'Yes', '5472', 'English ', NULL, 108, NULL, NULL),
(26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '6047981458', '6047981458', 'brentsawa@hotmail.com', 'Child', NULL, NULL, NULL, NULL, 'Meghan Sawatzky', '6047017140', 'omeghan@hotmail.com', 'Pauline Sawatzky', '6047935304', 'paulsawa@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 109, NULL, NULL),
(27, 'Wavy', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Xs', NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '2508186688', '2508186688', 'gccmandy@gmail.com', 'Child', NULL, NULL, NULL, NULL, 'Mandy lee', '2508186688', 'ml123456@gmail.com', 'Kevin Pinsonneault', '7786768720', 'rppempire@yahoo.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'July 1-15', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', 'None', 110, NULL, NULL),
(28, 'Braids or Shoulder Length Straight Hair', NULL, 'I appeared in the audience on ABC\'s The View as well as GMA Day', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Black Extras Ages 6-65', NULL, NULL, NULL, '6043381341', '6043381341', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Fadeke Mustapha', '19252069185', 'fadekemustapha@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'I\'m available on weekends all day, and weekdays after 6. For specific/urgent shoots I can take vacation days. ', 'Canadian Citizen', 'Yes', 'Yes', '9148', 'English', 'N/A ', 111, NULL, NULL),
(29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Townsfolk in Agassiz', NULL, NULL, NULL, '6049975940', '6049975940', 'Adreanmacdonald@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Jonni Skinner', '7788621595', 'jonnette66@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'Yes', '8998', 'English', NULL, 112, NULL, NULL),
(30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8199939350', '8199939350', NULL, 'Adult', 'Suzannah Raudaschl', '7783503452', 'Tony Scott', '8198499411', 'Suzannah Raudaschl', '7783503452', 'suzannahraudaschl@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '9497', 'English', NULL, 113, NULL, NULL),
(31, 'Long waves ', 'Eyebrows black ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'All secondary /non-speaking /speaking  and ', NULL, NULL, NULL, '7787235135', '7787235135', NULL, 'Adult', 'Blue Mancuma', NULL, 'New Image Alumni graduate /James Michael Hanson ', '6047674131', 'Tabitha Herrington ', '6048345349', 'jhansone2@gmail.com', 'James Hanson ', '6047674131', 'lostnsoundz@gmail.com', 'Yes', 'No', NULL, '32', NULL, 'No', 'Yes', '21-jump street \nLook who\'s talking two \nStand in for the omen Little girl main character.... \nflexible as to most things that can give two weeks notice exempt holidays. Otherwise in general willing to be flexible. ', 'Canadian Citizen', 'Yes', 'Yes', '8867', 'English', 'Italian ', 114, NULL, NULL),
(32, 'Long', 'No ', NULL, 'Full Union', 'BCGEU ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6043169338', '6043169338', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Keith  Anderson ', '6047984968', 'foanders219@hotmail.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Would love the opportunity to be involved in this and experience something fun !', 'Canadian Citizen', 'Yes', 'Yes', '944', 'English', NULL, 115, NULL, NULL),
(33, 'Military ', 'Short beard ( will shave for appropriate rolls)', 'The good doctor  ( continuity doctor in suit several episodes)\n \nColony   resistance fighter (12days) \n                 Grey hat soldier.   ( 10 days)\n\nRiverdale multiple episodes teacher, bobcat, \n                   and drive passerby.\n\nMan in the high castle. Multiple days \n                      Nazi scientist/ soldier\n\nProject blue book. multiple days passerby\n\nA million little things.  Multiple days\n                     Family guest. \n\n Multiple hallmark MOW. Passerby\n\n Travels.    Emergency patient.  Arrested by \n                     main charter. Sae stunt\n\n Timeless. Passerby\n\n Dirk Gently.  Multiple days Queens guard \n                        wendimoore ( featured)\n \n The flash. Policeman with mustache\n                    ( Featured )\n\n Supergirl.   Passerby\n\n Arrow.       Multiple days  Russian henchmen, \n                    passerby\n\n X- files.  Multiple days. Astronaut. \n                 ( photo shoot), passerby', 'Ubcp', 'Ex-04169', NULL, NULL, NULL, NULL, NULL, NULL, 'HellKat talent\nPh# 604-816-4528\nKatie@hellkattalent.ca', NULL, NULL, NULL, NULL, '7789892634', '6045760038', NULL, 'Adult', 'Katie bowdring', '6045760038', 'Rick Coltman', '6045339392', 'Katie bowdring', '6045760038', 'ktk@yahoo.ca', 'Rick Coltman', '6045339392', 'rickcoltman@hotmail.com', 'Yes', 'Yes', '16', '42', NULL, 'No', 'Yes', 'Monday to Saturday.  Sundays stunt training', 'Canadian Citizen', 'Yes', 'Yes', '5866', 'English', NULL, 116, NULL, NULL),
(34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kids ages 6-19 in Victoria,BC', NULL, NULL, NULL, '2505896482', '2505896482', 'ryannegirard@hotmail.ca', 'Teen', NULL, NULL, NULL, NULL, 'Ryanne Girard', '2505896482', 'ryannegirard@hotmail.ca', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'I believe I am free for the whole two weeks of the shot (July 1st-15th )', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 117, NULL, NULL),
(35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kids ages 6-19 in Victoria,BC', NULL, NULL, NULL, '2505896482', '2505896482', 'ryannegirard@hotmail.ca', 'Teen', NULL, NULL, NULL, NULL, 'Ryanne Girard', '2505896482', 'ryannegirard@hotmail.ca', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'I believe I am free for the whole two weeks of the shot (July 1st-15th )', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 118, NULL, NULL),
(36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '6048326701', '6048326701', NULL, 'Child', NULL, NULL, NULL, NULL, 'Jackie louis', '6046150067', 'ja_louis@yahoo.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 119, NULL, NULL),
(37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '2508881285', '2508881285', NULL, 'Child', 'Gillian Croft of spotlight academy', NULL, NULL, NULL, 'Karishma sethi', '2508881285', 'karishma.sethi@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'July 1-10 and August 1-31', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'Hindi', 120, NULL, NULL),
(38, 'Zero fade Ceaser', 'Thin chin strap', 'Broken Badges , The Wishing Tree , J. K. Rowling story', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Extra', NULL, NULL, NULL, '6049448567', '2508994720', 'aaroncallihoo@yahoo.com', 'Adult', 'Charlene Callihoo the Good Dr', '6047905356', 'Jake Callihoo , Twilight Zone ', '6042304429', 'Donna Callihoo', '7783887428', 'dccal1@hotmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'At the Moment my schedule is completley open as i have taken time off from Physically active work to focas on healing from knee ingury ,while looking into finding the right Barber School for me . I have been cutting mens hair from my home for almost 5 years and i would love to make it my carreer .So this would b nice and easy on my knee + i love the people and the sceen , ive always had fun being in the back ground .', 'Canadian Citizen', 'Yes', 'Yes', '4937', 'English', 'English', 121, NULL, NULL),
(39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '6045851976', '6047602535', NULL, 'Child', 'Terumi Takahashi', NULL, 'Terumi Takahashi', NULL, 'Terumi Takahashi', '6047602535', 'terucanada@hotmail.com', NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'Fluently speak Japanese\nAvailable to write Japanese \nGood sing \n', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', NULL, 122, NULL, NULL),
(40, NULL, NULL, NULL, 'Full Union', '1271', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6047990084', '6047990084', 'aarongelowitz@shaw.ca', 'Adult', NULL, NULL, NULL, NULL, 'Sharon Goldthorp', '6043167315', 'britshaw@shaw.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'march 25-29 7am-7pm', 'Canadian Citizen', 'Yes', 'Yes', '8298', 'ENGLISH', NULL, 123, NULL, NULL),
(41, 'Short clean cut', 'Clean shaven ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kamlooops', NULL, NULL, NULL, '2509455218', '2509455218', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Adam guenther', '17786827487', 'adam_guenther@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'No', 'Friday-sunday', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 124, NULL, NULL),
(42, NULL, NULL, 'IBABC advert', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk in Agassiz, BC', NULL, NULL, NULL, '6043169925', '6043169925', 'abbiemaslin399@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Freddie Maslin', '6043168970', 'freddiemaslin399@hotmail.com', 'Dean Cradock', '6047956865', NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'Yes', 'Evenings ', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 125, NULL, NULL),
(43, 'Short', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese People', NULL, NULL, NULL, '6048386511', '6044356511', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Martin Kobayakawa', '6044356511', 'mkobayakawa@shaw.ca', NULL, NULL, NULL, 'Yes', 'Yes', '14.5', NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'Yes', '7886', 'English', 'Japanese', 126, NULL, NULL),
(44, NULL, NULL, 'Netflix Lost in Space S2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7782312556', '6045522556', NULL, 'Teen', NULL, NULL, NULL, NULL, 'Tina LAMB', '7782312556', 'rtlamb@shaw.ca', 'Rick Lamb', '7782312813', 'rtlamb@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'Yes', 'Weekdays and weekends', 'Canadian Citizen', 'Yes', 'Yes', '2233', 'English ', NULL, 127, NULL, NULL),
(45, NULL, 'No', 'Netflix Lost in Space', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ' Boys & Girls Ages 12-16 Years Old Posting', NULL, NULL, NULL, '7782312556', '6045522556', 'tinalamb2813@gmail.com', 'Teen', NULL, NULL, NULL, NULL, 'Tina Lamb-mom', '7782312556', 'rtlamb@shaw.ca', 'Rick Lamb-dad', '7782312813', 'spuzzum71@gmail.com', 'No', 'No', NULL, NULL, NULL, 'Yes', 'Yes', 'Available any day/time', 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', 'None', 128, NULL, NULL),
(46, 'short', 'goatee', 'I have been working as a background worker for nine years. A few of the shows I have worked on are: Prison Break, Falling Skies, Godzilla, Flash, Arrow, Commercials, Psyche, Once Upon a Time, Elysium, and many more. ', 'Apprentice Union', '26150', NULL, NULL, NULL, NULL, NULL, NULL, 'Dallas Talent, number phone 6048360222', 'African- Canadian, Caribbean- Canadian and Mixed Race Males & Females Ages 17-65', NULL, NULL, NULL, '7788751616', '7788751616', 'yvewane7991@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Gina Wane', '6048741614', 'hdutot@hotmail.com', 'Gina Wane', '7788771616', 'hdutot@hotmail.com', 'Yes', 'Yes', '15.5', '36.5', NULL, 'Yes', 'Yes', 'Monday to Sunday ', 'Canadian Citizen', 'Yes', 'Yes', '750446072', 'French', 'English', 129, NULL, NULL),
(47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '604-506-7070', '604-506-7070', NULL, 'Child', 'Garry Dersksen Reel kids ', '604-465-8144', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'No', NULL, 'English', NULL, 130, NULL, NULL),
(48, 'Buzz', 'Clean or Scruff', 'This was in 2009.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '604-358-2994', '604-358-2994', NULL, 'Adult', 'David Gauthier', '778-952-5905', 'Andy Suganda', '778-892-5080', 'Sam Fedyk', '403-869-8783', 'samfedyk@shaw.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'Yes', 'Currently: any day, any time.', 'Canadian Citizen', 'Yes', 'Yes', '4163', 'English', 'French', 131, NULL, NULL),
(49, 'Surf', '5oclock', NULL, 'Iatse loc 891 carpenters', 'Iatse loc 891', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6049383696', '6049383596', NULL, 'Adult', 'Mike bouchard', '6045546611', 'Tom clifford', '6043179467', 'Kelvie barker', '6043170835', 'kelvieb@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 132, NULL, NULL),
(50, NULL, 'Stubble', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IN HOPE, BC: Small Town People with Lots of Character Ages 35 to 60', NULL, NULL, NULL, '6045183900', '6045183900', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Melanie Skolovy-Brodie', '7788781776', 'melanieanns@live.com', 'Bob Brodie', '6043088412', NULL, 'No', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '6326', 'English', 'N/A', 133, NULL, NULL),
(51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7783447053', '7783447053', NULL, 'Adult', 'Julie wilson', '+1 (604) 649-6960', 'Katie ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '4808', 'English', NULL, 134, NULL, NULL),
(52, 'Short style, Military, police style', 'upon request', 'Most of the TV shows and Films shot in the lower mainland since 2010 to 2017. http://www.agencyclick.com/adamboughanmi', 'Full Union', '60479', NULL, NULL, NULL, NULL, NULL, NULL, 'Locol Color :604-685-0315', 'Background acting, SAE, Stand-in, Acting.', NULL, NULL, NULL, '6044401085', '6044401085', 'boughanmi@gmail.com', 'Adult', 'Local Color', '604-685-0315', NULL, NULL, '911', '911', '911@911.com', NULL, NULL, NULL, 'Yes', 'Yes', '15.5-16', 'M,42', NULL, 'Yes', 'Yes', 'Weekend Mostly from Friday 6PM to Sunday 11PM and, anytime on special projects only.', 'Canadian Citizen', 'Yes', 'Yes', '8531', 'French, Arabic', 'English', 135, NULL, NULL),
(53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Japanese man extra', NULL, NULL, NULL, '6048375740', '6048375740', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Gerry Nishi', '6048399320', 'adamnishi93@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'Yes', 'No', 'Weekdays after 5:00pm\nWeekends any time', 'Canadian Citizen', 'Yes', 'Yes', '9330', 'English', NULL, 136, NULL, NULL),
(54, 'Curly ', 'Sruff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Anything ', NULL, NULL, NULL, '6044013690', '6044013690', 'adamghomari@gmail.com', 'Adult', 'Sara', '7789960494', 'Sara\'s talent partner ', '7783207241', 'Tamara Ghomari', '6042216029', 'tghomari@yahoo.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Currently always available', 'Canadian Citizen', 'Yes', 'Yes', '6263', 'English ', 'French ', 137, NULL, NULL),
(55, NULL, NULL, NULL, 'Full Union', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kamloops extra', NULL, NULL, NULL, '2505722326', '2505722326', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Greg', '2508191369', 'adamharris_15@hotmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'I work 4 on 4 off. I could do 8 Days of the extra roles during Kamloops filming ', 'Canadian Citizen', 'Yes', 'Yes', '2880', 'English', NULL, 138, NULL, NULL),
(56, 'Braids/pulled back', NULL, 'Xmas bells are ringing, the last bridesmaid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '2892009903', '2892009903', 'M.adam.brown87@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Alicia Brown ', '9057064683', 'alicia.brown16@gmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'As needed ', 'Canadian Citizen', 'Yes', 'Yes', '5472', 'English ', NULL, 139, NULL, NULL),
(57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Friendly Townsfolk & Kids Ages 4-14 in Agassiz, BC', NULL, NULL, NULL, '6047981458', '6047981458', 'brentsawa@hotmail.com', 'Child', NULL, NULL, NULL, NULL, 'Meghan Sawatzky', '6047017140', 'omeghan@hotmail.com', 'Pauline Sawatzky', '6047935304', 'paulsawa@shaw.ca', 'No', 'No', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', NULL, 'English', NULL, 140, NULL, NULL),
(58, 'Wavy', 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Xs', NULL, 'Kids ages 6-19 in Victoria, BC', NULL, NULL, NULL, '2508186688', '2508186688', 'gccmandy@gmail.com', 'Child', NULL, NULL, NULL, NULL, 'Mandy lee', '2508186688', 'ml123456@gmail.com', 'Kevin Pinsonneault', '7786768720', 'rppempire@yahoo.com', 'No', 'No', NULL, NULL, NULL, 'No', 'No', 'July 1-15', 'Canadian Citizen', 'Yes', 'No', NULL, 'English', 'None', 141, NULL, NULL),
(59, 'Braids or Shoulder Length Straight Hair', NULL, 'I appeared in the audience on ABC\'s The View as well as GMA Day', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Black Extras Ages 6-65', NULL, NULL, NULL, '6043381341', '6043381341', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Fadeke Mustapha', '19252069185', 'fadekemustapha@hotmail.com', NULL, NULL, NULL, 'Yes', 'No', NULL, NULL, NULL, 'No', 'Yes', 'I\'m available on weekends all day, and weekdays after 6. For specific/urgent shoots I can take vacation days. ', 'Canadian Citizen', 'Yes', 'Yes', '9148', 'English', 'N/A ', 142, NULL, NULL),
(60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Townsfolk in Agassiz', NULL, NULL, NULL, '6049975940', '6049975940', 'Adreanmacdonald@gmail.com', 'Adult', NULL, NULL, NULL, NULL, 'Jonni Skinner', '7788621595', 'jonnette66@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Anytime', 'Canadian Citizen', 'Yes', 'Yes', '8998', 'English', NULL, 143, NULL, NULL),
(61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8199939350', '8199939350', NULL, 'Adult', 'Suzannah Raudaschl', '7783503452', 'Tony Scott', '8198499411', 'Suzannah Raudaschl', '7783503452', 'suzannahraudaschl@gmail.com', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', NULL, 'Canadian Citizen', 'Yes', 'Yes', '9497', 'English', NULL, 144, NULL, NULL),
(62, 'Long waves ', 'Eyebrows black ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'All secondary /non-speaking /speaking  and ', NULL, NULL, NULL, '7787235135', '7787235135', NULL, 'Adult', 'Blue Mancuma', NULL, 'New Image Alumni graduate /James Michael Hanson ', '6047674131', 'Tabitha Herrington ', '6048345349', 'jhansone2@gmail.com', 'James Hanson ', '6047674131', 'lostnsoundz@gmail.com', 'Yes', 'No', NULL, '32', NULL, 'No', 'Yes', '21-jump street \nLook who\'s talking two \nStand in for the omen Little girl main character.... \nflexible as to most things that can give two weeks notice exempt holidays. Otherwise in general willing to be flexible. ', 'Canadian Citizen', 'Yes', 'Yes', '8867', 'English', 'Italian ', 145, NULL, NULL),
(63, 'Long', 'No ', NULL, 'Full Union', 'BCGEU ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6043169338', '6043169338', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Keith  Anderson ', '6047984968', 'foanders219@hotmail.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Would love the opportunity to be involved in this and experience something fun !', 'Canadian Citizen', 'Yes', 'Yes', '944', 'English', NULL, 146, NULL, NULL),
(64, 'Long', 'No ', NULL, 'Full Union', 'BCGEU ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6043169338', '6043169338', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Keith  Anderson ', '6047984968', 'foanders219@hotmail.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Would love the opportunity to be involved in this and experience something fun !', 'Canadian Citizen', 'Yes', 'Yes', '944', 'English', NULL, 147, NULL, NULL),
(65, 'Long', 'No ', NULL, 'Full Union', 'BCGEU ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6043169338', '6043169338', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Keith  Anderson ', '6047984968', 'foanders219@hotmail.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Would love the opportunity to be involved in this and experience something fun !', 'Canadian Citizen', 'Yes', 'Yes', '944', 'English', NULL, 148, NULL, NULL),
(66, 'Long', 'No ', NULL, 'Full Union', 'BCGEU ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6043169338', '6043169338', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Keith  Anderson ', '6047984968', 'foanders219@hotmail.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Would love the opportunity to be involved in this and experience something fun !', 'Canadian Citizen', 'Yes', 'Yes', '944', 'English', NULL, 149, NULL, NULL),
(67, 'Long', 'No ', NULL, 'Full Union', 'BCGEU ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6043169338', '6043169338', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Keith  Anderson ', '6047984968', 'foanders219@hotmail.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Would love the opportunity to be involved in this and experience something fun !', 'Canadian Citizen', 'Yes', 'Yes', '944', 'English', NULL, 150, NULL, NULL),
(68, 'Long', 'No ', NULL, 'Full Union', 'BCGEU ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6043169338', '6043169338', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Keith  Anderson ', '6047984968', 'foanders219@hotmail.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Would love the opportunity to be involved in this and experience something fun !', 'Canadian Citizen', 'Yes', 'Yes', '944', 'English', NULL, 151, NULL, NULL),
(69, 'Long', 'No ', NULL, 'Full Union', 'BCGEU ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6043169338', '6043169338', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Keith  Anderson ', '6047984968', 'foanders219@hotmail.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Would love the opportunity to be involved in this and experience something fun !', 'Canadian Citizen', 'Yes', 'Yes', '944', 'English', NULL, 152, NULL, NULL),
(70, 'Long', 'No ', NULL, 'Full Union', 'BCGEU ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6043169338', '6043169338', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Keith  Anderson ', '6047984968', 'foanders219@hotmail.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Would love the opportunity to be involved in this and experience something fun !', 'Canadian Citizen', 'Yes', 'Yes', '944', 'English', NULL, 153, NULL, NULL),
(71, 'Long', 'No ', NULL, 'Full Union', 'BCGEU ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '6043169338', '6043169338', NULL, 'Adult', NULL, NULL, NULL, NULL, 'Keith  Anderson ', '6047984968', 'foanders219@hotmail.ca', NULL, NULL, NULL, 'Yes', 'Yes', NULL, NULL, NULL, 'No', 'No', 'Would love the opportunity to be involved in this and experience something fun !', 'Canadian Citizen', 'Yes', 'Yes', '944', 'English', NULL, 154, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `performer_agendas`
--

CREATE TABLE `performer_agendas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `agenda` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `performer_availability`
--

CREATE TABLE `performer_availability` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `availability` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `performer_availability`
--

INSERT INTO `performer_availability` (`id`, `availability`, `status_color`, `created_at`, `updated_at`) VALUES
(1, 'Available', '#4D9E39', NULL, NULL),
(2, 'Not Available', '#000000', NULL, NULL),
(3, 'Partial Avail', '#95D1A8', NULL, NULL),
(4, 'Full Avail', '#1EA247', NULL, NULL),
(5, 'Booked Night Shoot', '#7E0000', NULL, NULL),
(6, '1st Refusal', '#FFCE00', NULL, NULL),
(7, '2nd Refusal', '#FF1DD4', NULL, NULL),
(8, 'Pre-booked', '#000000', NULL, NULL),
(9, 'Booked', '#FF3434', NULL, NULL),
(10, 'Main Unit Episode', '#000000', NULL, NULL),
(11, 'Second Unit Episode', '#000000', NULL, NULL),
(12, 'Stand-In', '#000000', NULL, NULL),
(13, 'Wardrobe Fitting', '#000000', NULL, NULL),
(14, 'Camera Test', '#000000', NULL, NULL),
(15, 'Still Shoot', '#000000', NULL, NULL),
(16, 'Picture Cast', '#000000', NULL, NULL),
(17, 'Night Shoot', '#000000', NULL, NULL),
(18, 'Half-day', '#000000', NULL, NULL),
(19, 'Pending', '#1DBAFF', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `performer_portfolios`
--

CREATE TABLE `performer_portfolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `performer_id` int(10) UNSIGNED NOT NULL,
  `is_video` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `performer_status`
--

CREATE TABLE `performer_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `performer_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `performer_status`
--

INSERT INTO `performer_status` (`id`, `performer_status`, `created_at`, `updated_at`) VALUES
(1, 'Active', NULL, NULL),
(2, 'Do Not Book', NULL, NULL),
(3, 'Flagged', NULL, NULL),
(4, 'No Longer Working', NULL, NULL),
(5, 'Warning #1', NULL, NULL),
(6, 'Warning #2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `performer_unions`
--

CREATE TABLE `performer_unions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `union` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `performer_unions`
--

INSERT INTO `performer_unions` (`id`, `union`, `created_at`, `updated_at`) VALUES
(1, 'Screen Actors Guild', NULL, NULL),
(2, 'Screen Actors Guild Eligible', NULL, NULL),
(3, 'UBCP', NULL, NULL),
(4, 'UBCP Apprentice', NULL, NULL),
(5, 'UBCP Extra', NULL, NULL),
(6, 'UDA', NULL, NULL),
(7, 'UDA Stagiaire', NULL, NULL),
(8, 'RQD', NULL, NULL),
(9, 'ANDA', NULL, NULL),
(10, 'ACTRA', NULL, NULL),
(11, 'ACTRA Apprentice', NULL, NULL),
(12, 'AABP', NULL, NULL),
(13, 'EQUITY', NULL, NULL),
(14, 'EQUITY Permitee', NULL, NULL),
(15, 'AEA', NULL, NULL),
(16, 'CAEA', NULL, NULL),
(17, 'CAEA Apprentice', NULL, NULL),
(18, 'BAEA', NULL, NULL),
(19, 'MEAA', NULL, NULL),
(20, 'NON-UNION', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pets`
--

CREATE TABLE `pets` (
  `id` int(11) NOT NULL,
  `petName` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pets`
--

INSERT INTO `pets` (`id`, `petName`, `created_at`, `updated_at`) VALUES
(1, 'Abyssinian', '2019-07-03 11:16:40', '0000-00-00'),
(2, 'Afghan Hound', '2019-07-03 11:16:40', '0000-00-00'),
(3, 'African Grey', '2019-07-03 11:16:40', '0000-00-00'),
(4, 'Airedale Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(5, 'Akita', '2019-07-03 11:16:40', '0000-00-00'),
(6, 'Alaskan Malamute', '2019-07-03 11:16:40', '0000-00-00'),
(7, 'American Bulldog', '2019-07-03 11:16:40', '0000-00-00'),
(8, 'American Eskimo', '2019-07-03 11:16:40', '0000-00-00'),
(9, 'American Shorthair', '2019-07-03 11:16:40', '0000-00-00'),
(10, 'Anatolian Shepherd', '2019-07-03 11:16:40', '0000-00-00'),
(11, 'Australian Cattle Dog', '2019-07-03 11:16:40', '0000-00-00'),
(12, 'Australian Kelpie', '2019-07-03 11:16:40', '0000-00-00'),
(13, 'Australian Shepherd', '2019-07-03 11:16:40', '0000-00-00'),
(14, 'Basenji', '2019-07-03 11:16:40', '0000-00-00'),
(15, 'Basset Hound', '2019-07-03 11:16:40', '0000-00-00'),
(16, 'Beagle', '2019-07-03 11:16:40', '0000-00-00'),
(17, 'Bearded Collie', '2019-07-03 11:16:40', '0000-00-00'),
(18, 'Bearded Dragon', '2019-07-03 11:16:40', '0000-00-00'),
(19, 'Beauceron', '2019-07-03 11:16:40', '0000-00-00'),
(20, 'Belgian Malinois', '2019-07-03 11:16:40', '0000-00-00'),
(21, 'Belgian Shepherd', '2019-07-03 11:16:40', '0000-00-00'),
(22, 'Bengal', '2019-07-03 11:16:40', '0000-00-00'),
(23, 'Bernese Mountain Dog', '2019-07-03 11:16:40', '0000-00-00'),
(24, 'Bichon Frise', '2019-07-03 11:16:40', '0000-00-00'),
(25, 'Birman', '2019-07-03 11:16:40', '0000-00-00'),
(26, 'Black Labrador Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(27, 'Bloodhound', '2019-07-03 11:16:40', '0000-00-00'),
(28, 'Boa', '2019-07-03 11:16:40', '0000-00-00'),
(29, 'Bombay', '2019-07-03 11:16:40', '0000-00-00'),
(30, 'Border Collie', '2019-07-03 11:16:40', '0000-00-00'),
(31, 'Border Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(32, 'Borzoi', '2019-07-03 11:16:40', '0000-00-00'),
(33, 'Boston Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(34, 'Bouvier Des Flanders', '2019-07-03 11:16:40', '0000-00-00'),
(35, 'Boxer', '2019-07-03 11:16:40', '0000-00-00'),
(36, 'Britany Spaniel', '2019-07-03 11:16:40', '0000-00-00'),
(37, 'British Shorthair', '2019-07-03 11:16:40', '0000-00-00'),
(38, 'Brussels Griffon', '2019-07-03 11:16:40', '0000-00-00'),
(39, 'Budgie', '2019-07-03 11:16:40', '0000-00-00'),
(40, 'Bull Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(41, 'Bullmastiff', '2019-07-03 11:16:40', '0000-00-00'),
(42, 'Burmese', '2019-07-03 11:16:40', '0000-00-00'),
(43, 'Cairn Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(44, 'Canary', '2019-07-03 11:16:40', '0000-00-00'),
(45, 'Cane Corso Mastiff', '2019-07-03 11:16:40', '0000-00-00'),
(46, 'Catahoula Leopard Dog', '2019-07-03 11:16:40', '0000-00-00'),
(47, 'Chameleon', '2019-07-03 11:16:40', '0000-00-00'),
(48, 'Chesapeake Bay Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(49, 'Chihauhua', '2019-07-03 11:16:40', '0000-00-00'),
(50, 'Chinese Crested', '2019-07-03 11:16:40', '0000-00-00'),
(51, 'Chocolate Labrador Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(52, 'Chocolate Point Siamese', '2019-07-03 11:16:40', '0000-00-00'),
(53, 'Chow Chow', '2019-07-03 11:16:40', '0000-00-00'),
(54, 'Cockapoo', '2019-07-03 11:16:40', '0000-00-00'),
(55, 'Cockatiel', '2019-07-03 11:16:40', '0000-00-00'),
(56, 'Cockatoo', '2019-07-03 11:16:40', '0000-00-00'),
(57, 'Cocker Spaniel', '2019-07-03 11:16:40', '0000-00-00'),
(58, 'Conure', '2019-07-03 11:16:40', '0000-00-00'),
(59, 'Coonhound', '2019-07-03 11:16:40', '0000-00-00'),
(60, 'Corgi', '2019-07-03 11:16:40', '0000-00-00'),
(61, 'Corn Snake', '2019-07-03 11:16:40', '0000-00-00'),
(62, 'Cornish Rex', '2019-07-03 11:16:40', '0000-00-00'),
(63, 'Crow', '2019-07-03 11:16:40', '0000-00-00'),
(64, 'Dachshund', '2019-07-03 11:16:40', '0000-00-00'),
(65, 'Dalmation', '2019-07-03 11:16:40', '0000-00-00'),
(66, 'Devon Rex', '2019-07-03 11:16:40', '0000-00-00'),
(67, 'Doberman Pinscher', '2019-07-03 11:16:40', '0000-00-00'),
(68, 'Dogo Argentino', '2019-07-03 11:16:40', '0000-00-00'),
(69, 'Dogue De Bordeaux', '2019-07-03 11:16:40', '0000-00-00'),
(70, 'Dove', '2019-07-03 11:16:40', '0000-00-00'),
(71, 'Egyptian Mau', '2019-07-03 11:16:40', '0000-00-00'),
(72, 'Electus', '2019-07-03 11:16:40', '0000-00-00'),
(73, 'English Bulldog', '2019-07-03 11:16:40', '0000-00-00'),
(74, 'English Pointer', '2019-07-03 11:16:40', '0000-00-00'),
(75, 'English Setter', '2019-07-03 11:16:40', '0000-00-00'),
(76, 'Finch', '2019-07-03 11:16:40', '0000-00-00'),
(77, 'Flame Point Siamese', '2019-07-03 11:16:40', '0000-00-00'),
(78, 'Flat Coated Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(79, 'Fox Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(80, 'Foxhound', '2019-07-03 11:16:40', '0000-00-00'),
(81, 'French Bulldog', '2019-07-03 11:16:40', '0000-00-00'),
(82, 'Gecko', '2019-07-03 11:16:40', '0000-00-00'),
(83, 'German Shepherd', '2019-07-03 11:16:40', '0000-00-00'),
(84, 'German Shorthaired Pointer', '2019-07-03 11:16:40', '0000-00-00'),
(85, 'German Wirehaired Pointer', '2019-07-03 11:16:40', '0000-00-00'),
(86, 'Giant Schnauzer', '2019-07-03 11:16:40', '0000-00-00'),
(87, 'Golden Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(88, 'Great Dane', '2019-07-03 11:16:40', '0000-00-00'),
(89, 'Great Pyrenees', '2019-07-03 11:16:40', '0000-00-00'),
(90, 'Greyhound', '2019-07-03 11:16:40', '0000-00-00'),
(91, 'Havanese', '2019-07-03 11:16:40', '0000-00-00'),
(92, 'Hawk', '2019-07-03 11:16:40', '0000-00-00'),
(93, 'Himalayan', '2019-07-03 11:16:40', '0000-00-00'),
(94, 'Husky', '2019-07-03 11:16:40', '0000-00-00'),
(95, 'Ibizian Hound', '2019-07-03 11:16:40', '0000-00-00'),
(96, 'Iguana', '2019-07-03 11:16:40', '0000-00-00'),
(97, 'Irish Setter', '2019-07-03 11:16:40', '0000-00-00'),
(98, 'Irish Wolfhound', '2019-07-03 11:16:40', '0000-00-00'),
(99, 'Italian Greyhound', '2019-07-03 11:16:40', '0000-00-00'),
(100, 'Jack Russell Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(101, 'Japanese Chin', '2019-07-03 11:16:40', '0000-00-00'),
(102, 'Karelian Bear Dog', '2019-07-03 11:16:40', '0000-00-00'),
(103, 'Keeshond', '2019-07-03 11:16:40', '0000-00-00'),
(104, 'King Charles Spaniel', '2019-07-03 11:16:40', '0000-00-00'),
(105, 'Komondor', '2019-07-03 11:16:40', '0000-00-00'),
(106, 'Leopard Gecko', '2019-07-03 11:16:40', '0000-00-00'),
(107, 'Lhasa Apso', '2019-07-03 11:16:40', '0000-00-00'),
(108, 'Lilac Point Siamese', '2019-07-03 11:16:40', '0000-00-00'),
(109, 'Lovebird', '2019-07-03 11:16:40', '0000-00-00'),
(110, 'Macaw', '2019-07-03 11:16:40', '0000-00-00'),
(111, 'Maine Coon', '2019-07-03 11:16:40', '0000-00-00'),
(112, 'Maltese', '2019-07-03 11:16:40', '0000-00-00'),
(113, 'Manchester Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(114, 'Manx', '2019-07-03 11:16:40', '0000-00-00'),
(115, 'Maremma Sheepdog', '2019-07-03 11:16:40', '0000-00-00'),
(116, 'Mastiff', '2019-07-03 11:16:40', '0000-00-00'),
(117, 'Miniature Pinscher', '2019-07-03 11:16:40', '0000-00-00'),
(118, 'Mountain Cur', '2019-07-03 11:16:40', '0000-00-00'),
(119, 'Munchkin', '2019-07-03 11:16:40', '0000-00-00'),
(120, 'Newfoundland', '2019-07-03 11:16:40', '0000-00-00'),
(121, 'Norfolk Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(122, 'Norwegian Elkhound', '2019-07-03 11:16:40', '0000-00-00'),
(123, 'Norwegian Forest Cat', '2019-07-03 11:16:40', '0000-00-00'),
(124, 'Nova Scotia Duck-Tolling Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(125, 'Ocicat', '2019-07-03 11:16:40', '0000-00-00'),
(126, 'Old English Sheepdog', '2019-07-03 11:16:40', '0000-00-00'),
(127, 'Oriental Shorthair', '2019-07-03 11:16:40', '0000-00-00'),
(128, 'Papillon', '2019-07-03 11:16:40', '0000-00-00'),
(129, 'Parakeet', '2019-07-03 11:16:40', '0000-00-00'),
(130, 'Parrot', '2019-07-03 11:16:40', '0000-00-00'),
(131, 'Pekingese', '2019-07-03 11:16:40', '0000-00-00'),
(132, 'Persian', '2019-07-03 11:16:40', '0000-00-00'),
(133, 'Pharaoh Hound', '2019-07-03 11:16:40', '0000-00-00'),
(134, 'Pit Bull Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(135, 'Pointer', '2019-07-03 11:16:40', '0000-00-00'),
(136, 'Pomeranian', '2019-07-03 11:16:40', '0000-00-00'),
(137, 'Poodle', '2019-07-03 11:16:40', '0000-00-00'),
(138, 'Portuguese Water Dog', '2019-07-03 11:16:40', '0000-00-00'),
(139, 'Pug', '2019-07-03 11:16:40', '0000-00-00'),
(140, 'Python', '2019-07-03 11:16:40', '0000-00-00'),
(141, 'Ragdoll', '2019-07-03 11:16:40', '0000-00-00'),
(142, 'Rat Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(143, 'Rhodesian Ridgeback', '2019-07-03 11:16:40', '0000-00-00'),
(144, 'Rottweiler', '2019-07-03 11:16:40', '0000-00-00'),
(145, 'Russian Blue', '2019-07-03 11:16:40', '0000-00-00'),
(146, 'Saint Bernard', '2019-07-03 11:16:40', '0000-00-00'),
(147, 'Saluki', '2019-07-03 11:16:40', '0000-00-00'),
(148, 'Samoyed', '2019-07-03 11:16:40', '0000-00-00'),
(149, 'Savannah', '2019-07-03 11:16:40', '0000-00-00'),
(150, 'Schipperke', '2019-07-03 11:16:40', '0000-00-00'),
(151, 'Schnauzer', '2019-07-03 11:16:40', '0000-00-00'),
(152, 'Scottish Deerhound', '2019-07-03 11:16:40', '0000-00-00'),
(153, 'Scottish Fold', '2019-07-03 11:16:40', '0000-00-00'),
(154, 'Scottish Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(155, 'Seal Point Siamese', '2019-07-03 11:16:40', '0000-00-00'),
(156, 'Setter', '2019-07-03 11:16:40', '0000-00-00'),
(157, 'Shar Pei', '2019-07-03 11:16:40', '0000-00-00'),
(158, 'Sheep Dog', '2019-07-03 11:16:40', '0000-00-00'),
(159, 'Shepherd', '2019-07-03 11:16:40', '0000-00-00'),
(160, 'Shetland Sheepdog', '2019-07-03 11:16:40', '0000-00-00'),
(161, 'Shiba Inue', '2019-07-03 11:16:40', '0000-00-00'),
(162, 'Shih Tzu', '2019-07-03 11:16:40', '0000-00-00'),
(163, 'Siamese', '2019-07-03 11:16:40', '0000-00-00'),
(164, 'Siberian', '2019-07-03 11:16:40', '0000-00-00'),
(165, 'Siberian Husky', '2019-07-03 11:16:40', '0000-00-00'),
(166, 'Silky Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(167, 'Snowshoe', '2019-07-03 11:16:40', '0000-00-00'),
(168, 'Somali', '2019-07-03 11:16:40', '0000-00-00'),
(169, 'Spaniel', '2019-07-03 11:16:40', '0000-00-00'),
(170, 'Sphynx', '2019-07-03 11:16:40', '0000-00-00'),
(171, 'Spitz', '2019-07-03 11:16:40', '0000-00-00'),
(172, 'Springer Spaniel', '2019-07-03 11:16:40', '0000-00-00'),
(173, 'Staffordshire Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(174, 'Standard Poodle', '2019-07-03 11:16:40', '0000-00-00'),
(175, 'Tabby', '2019-07-03 11:16:40', '0000-00-00'),
(176, 'Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(177, 'Tortoise Shell', '2019-07-03 11:16:40', '0000-00-00'),
(178, 'Turkish Angora', '2019-07-03 11:16:40', '0000-00-00'),
(179, 'Vizsla', '2019-07-03 11:16:40', '0000-00-00'),
(180, 'Weimaraner', '2019-07-03 11:16:40', '0000-00-00'),
(181, 'West Highland Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(182, 'Wheaten Terrier', '2019-07-03 11:16:40', '0000-00-00'),
(183, 'Whippet', '2019-07-03 11:16:40', '0000-00-00'),
(184, 'Yellow Labrador Retriever', '2019-07-03 11:16:40', '0000-00-00'),
(185, 'Yorkshire Terrier', '2019-07-03 11:16:40', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `piercings`
--

CREATE TABLE `piercings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `piercing` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `piercings`
--

INSERT INTO `piercings` (`id`, `piercing`, `created_at`, `updated_at`) VALUES
(1, 'Has Vehicle', NULL, NULL),
(2, 'Rain Towers', NULL, NULL),
(3, 'Will kiss Male', NULL, NULL),
(4, 'Will Kiss Female', NULL, NULL),
(5, 'Will kiss partner', NULL, NULL),
(6, 'Will Kiss', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_gallery_tags`
--

CREATE TABLE `portfolio_gallery_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portfolio_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_perfomers`
--

CREATE TABLE `project_perfomers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IMDC_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resume_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_roles`
--

CREATE TABLE `project_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `project_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `props`
--

CREATE TABLE `props` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `prop` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `props`
--

INSERT INTO `props` (`id`, `prop`, `created_at`, `updated_at`) VALUES
(1, 'Acoustic Guitar', NULL, NULL),
(2, 'Banjo', NULL, NULL),
(3, 'Baseball Gear', NULL, NULL),
(4, 'Bass Guitar', NULL, NULL),
(5, 'Bicycle', NULL, NULL),
(6, 'BMX', NULL, NULL),
(7, 'Boat', NULL, NULL),
(8, 'bongo drums', NULL, NULL),
(9, 'Boxing Gear', NULL, NULL),
(10, 'Camping Gear', NULL, NULL),
(11, 'Cruiser Bike', NULL, NULL),
(12, 'Crutches', NULL, NULL),
(13, 'Dance Shoes', NULL, NULL),
(14, 'Downhill Skis', NULL, NULL),
(15, 'Drums', NULL, NULL),
(16, 'Electric Guitar', NULL, NULL),
(17, 'Fiddle', NULL, NULL),
(18, 'Fire Dancing Equipment', NULL, NULL),
(19, 'Firearms', NULL, NULL),
(20, 'Fireman Gear', NULL, NULL),
(21, 'Fishing Gear', NULL, NULL),
(22, 'Flute', NULL, NULL),
(23, 'Football Gear', NULL, NULL),
(24, 'Golf Clubs', NULL, NULL),
(25, 'Harmonica', NULL, NULL),
(26, 'Harp', NULL, NULL),
(27, 'Hockey Gear', NULL, NULL),
(28, 'Ice Skates', NULL, NULL),
(29, 'Instrument', NULL, NULL),
(30, 'Jogging Stroller', NULL, NULL),
(31, 'Juggling Props', NULL, NULL),
(32, 'Keyboard', NULL, NULL),
(33, 'long board', NULL, NULL),
(34, 'Luggage', NULL, NULL),
(35, 'Mandolin', NULL, NULL),
(36, 'Mountain Bike', NULL, NULL),
(37, 'MTN Climbing Gear', NULL, NULL),
(38, 'Paramedic Gear', NULL, NULL),
(39, 'Racquetball Gear', NULL, NULL),
(40, 'Road Bike', NULL, NULL),
(41, 'Rock Climbing Gear', NULL, NULL),
(42, 'Roller Blades', NULL, NULL),
(43, 'Saxophone', NULL, NULL),
(44, 'Scooter', NULL, NULL),
(45, 'Scuba Gear', NULL, NULL),
(46, 'Sitar', NULL, NULL),
(47, 'Skateboard', NULL, NULL),
(48, 'Snare Drum', NULL, NULL),
(49, 'Snowboard Gear', NULL, NULL),
(50, 'Squash Racquet', NULL, NULL),
(51, 'Stand-up Bass', NULL, NULL),
(52, 'Stenograph', NULL, NULL),
(53, 'Stilts', NULL, NULL),
(54, 'Stroller', NULL, NULL),
(55, 'Surfboard', NULL, NULL),
(56, 'Tennis Equipment', NULL, NULL),
(57, 'Trombone', NULL, NULL),
(58, 'Trumpet', NULL, NULL),
(59, 'Tuba', NULL, NULL),
(60, 'Ukelele', NULL, NULL),
(61, 'Umbrella', NULL, NULL),
(62, 'Vintage Bike', NULL, NULL),
(63, 'Violin', NULL, NULL),
(64, 'Wakeboard', NULL, NULL),
(65, 'Western Saddle', NULL, NULL),
(66, 'Wheelchair', NULL, NULL),
(67, 'Windsurfer', NULL, NULL),
(68, 'Wooden Drums', NULL, NULL),
(69, 'X Country Skis', NULL, NULL),
(70, 'Xylophone', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `region`, `created_at`, `updated_at`) VALUES
(1, 'Abbotsford', NULL, NULL),
(2, 'Armstrong', NULL, NULL),
(3, 'Burnaby', NULL, NULL),
(4, 'Campbell River', NULL, NULL),
(5, 'Castlegar', NULL, NULL),
(6, 'Chilliwack', NULL, NULL),
(7, 'Colwood', NULL, NULL),
(8, 'Coquitlam', NULL, NULL),
(9, 'Courtenay', NULL, NULL),
(10, 'Cranbrook', NULL, NULL),
(11, 'Dawson Creek', NULL, NULL),
(12, 'Duncan', NULL, NULL),
(13, 'Enderby', NULL, NULL),
(14, 'Fernie', NULL, NULL),
(15, 'Fort St. John', NULL, NULL),
(16, 'Grand Forks', NULL, NULL),
(17, 'Greenwood', NULL, NULL),
(18, 'Kamloops', NULL, NULL),
(19, 'Kelowna', NULL, NULL),
(20, 'Kimberley', NULL, NULL),
(21, 'Langford', NULL, NULL),
(22, 'Langley', NULL, NULL),
(23, 'Merritt', NULL, NULL),
(24, 'Nanaimo', NULL, NULL),
(25, 'Nelson', NULL, NULL),
(26, 'New Westminster', NULL, NULL),
(27, 'North Vancouver', NULL, NULL),
(28, 'Parksville', NULL, NULL),
(29, 'Penticton', NULL, NULL),
(30, 'Pitt Meadows', NULL, NULL),
(31, 'Port Alberni', NULL, NULL),
(32, 'Port Coquitlam', NULL, NULL),
(33, 'Port Moody', NULL, NULL),
(34, 'Powell River', NULL, NULL),
(35, 'Prince George', NULL, NULL),
(36, 'Prince Rupert', NULL, NULL),
(37, 'Quesnel', NULL, NULL),
(38, 'Revelstoke', NULL, NULL),
(39, 'Richmond', NULL, NULL),
(40, 'Rossland', NULL, NULL),
(41, 'Salmon Arm', NULL, NULL),
(42, 'Surrey', NULL, NULL),
(43, 'Terrace', NULL, NULL),
(44, 'Trail', NULL, NULL),
(45, 'Vancouver', NULL, NULL),
(46, 'Vernon', NULL, NULL),
(47, 'Victoria', NULL, NULL),
(48, 'West Kelowna', NULL, NULL),
(49, 'White Rock', NULL, NULL),
(50, 'Williams Lake', NULL, NULL),
(51, '100 Mile House', NULL, NULL),
(52, 'Barriere', NULL, NULL),
(53, 'Central Saanich', NULL, NULL),
(54, 'Chetwynd', NULL, NULL),
(55, 'Clearwater', NULL, NULL),
(56, 'Coldstream', NULL, NULL),
(57, 'Delta', NULL, NULL),
(58, 'Elkford', NULL, NULL),
(59, 'Fort St. James', NULL, NULL),
(60, 'Highlands', NULL, NULL),
(61, 'Hope', NULL, NULL),
(62, 'Houston', NULL, NULL),
(63, 'Hudsons Hope', NULL, NULL),
(64, 'Invermere', NULL, NULL),
(65, 'Kent', NULL, NULL),
(66, 'Kitimat', NULL, NULL),
(67, 'Lake Country', NULL, NULL),
(68, 'Lantzville', NULL, NULL),
(69, 'Lillooet', NULL, NULL),
(70, 'Logan Lake', NULL, NULL),
(71, 'Mackenzie', NULL, NULL),
(72, 'Maple Ridge', NULL, NULL),
(73, 'Metchosin', NULL, NULL),
(74, 'Mission', NULL, NULL),
(75, 'New Hazelton', NULL, NULL),
(76, 'North Cowichan', NULL, NULL),
(77, 'North Saanich', NULL, NULL),
(78, 'North Vancouver', NULL, NULL),
(79, 'Oak Bay', NULL, NULL),
(80, 'Peachland', NULL, NULL),
(81, 'Port Edward', NULL, NULL),
(82, 'Port Hardy', NULL, NULL),
(83, 'Saanich', NULL, NULL),
(84, 'Sechelt', NULL, NULL),
(85, 'Sicamous', NULL, NULL),
(86, 'Sooke', NULL, NULL),
(87, 'Sparwood', NULL, NULL),
(88, 'Squamish', NULL, NULL),
(89, 'Stewart', NULL, NULL),
(90, 'Summerland', NULL, NULL),
(91, 'Taylor', NULL, NULL),
(92, 'Tofino', NULL, NULL),
(93, 'Tumbler Ridge', NULL, NULL),
(94, 'Ucluelet', NULL, NULL),
(95, 'Vanderhoof', NULL, NULL),
(96, 'Wells', NULL, NULL),
(97, 'West Vancouver', NULL, NULL),
(98, 'Bowen Island', NULL, NULL),
(99, 'Alberni-Clayoquot', NULL, NULL),
(100, 'Bulkley-Nechako', NULL, NULL),
(101, 'Capital', NULL, NULL),
(102, 'Cariboo', NULL, NULL),
(103, 'Central Coast', NULL, NULL),
(104, 'Central Kootenay', NULL, NULL),
(105, 'Central Okanagan', NULL, NULL),
(106, 'Columbia Shuswap', NULL, NULL),
(107, 'Comox Valley', NULL, NULL),
(108, 'Cowichan Valley', NULL, NULL),
(109, 'East Kootenay', NULL, NULL),
(110, 'Fraser Valley', NULL, NULL),
(111, 'Fraser-Fort George', NULL, NULL),
(112, 'Greater Vancouver', NULL, NULL),
(113, 'Kitimat-Stikine', NULL, NULL),
(114, 'Kootenay Boundary', NULL, NULL),
(115, 'Mount Waddington', NULL, NULL),
(116, 'Nanaimo', NULL, NULL),
(117, 'North Okanagan', NULL, NULL),
(118, 'Okanagan-Similkameen', NULL, NULL),
(119, 'Peace River', NULL, NULL),
(120, 'Powell River', NULL, NULL),
(121, 'Skeena-Queen Charlotte', NULL, NULL),
(122, 'Squamish-Lillooet', NULL, NULL),
(123, 'Strathcona', NULL, NULL),
(124, 'Sunshine Coast', NULL, NULL),
(125, 'Thompson-Nicola', NULL, NULL),
(126, 'Comox', NULL, NULL),
(127, 'Creston', NULL, NULL),
(128, 'Gibsons', NULL, NULL),
(129, 'Golden', NULL, NULL),
(130, 'Ladysmith', NULL, NULL),
(131, 'Lake Cowichan', NULL, NULL),
(132, 'Oliver', NULL, NULL),
(133, 'Osoyoos', NULL, NULL),
(134, 'Port McNeill', NULL, NULL),
(135, 'Princeton', NULL, NULL),
(136, 'Qualicum Beach', NULL, NULL),
(137, 'Sidney', NULL, NULL),
(138, 'Smithers', NULL, NULL),
(139, 'View Royal', NULL, NULL),
(140, 'Esquimalt', NULL, NULL),
(141, 'Langley', NULL, NULL),
(142, 'Spallumcheen', NULL, NULL),
(143, 'Alert Bay', NULL, NULL),
(144, 'Anmore', NULL, NULL),
(145, 'Ashcroft', NULL, NULL),
(146, 'Belcarra', NULL, NULL),
(147, 'Burns Lake', NULL, NULL),
(148, 'Cache Creek', NULL, NULL),
(149, 'Canal Flats', NULL, NULL),
(150, 'Chase', NULL, NULL),
(151, 'Clinton', NULL, NULL),
(152, 'Cumberland', NULL, NULL),
(153, 'Fraser Lake', NULL, NULL),
(154, 'Fruitvale', NULL, NULL),
(155, 'Gold River', NULL, NULL),
(156, 'Granisle', NULL, NULL),
(157, 'Harrison Hot Springs', NULL, NULL),
(158, 'Hazelton', NULL, NULL),
(159, 'Kaslo', NULL, NULL),
(160, 'Keremeos', NULL, NULL),
(161, 'Lions Bay', NULL, NULL),
(162, 'Lumby', NULL, NULL),
(163, 'Lytton', NULL, NULL),
(164, 'Masset', NULL, NULL),
(165, 'McBride', NULL, NULL),
(166, 'Midway', NULL, NULL),
(167, 'Montrose', NULL, NULL),
(168, 'Nakusp', NULL, NULL),
(169, 'New Denver', NULL, NULL),
(170, 'Pemberton', NULL, NULL),
(171, 'Port Alice', NULL, NULL),
(172, 'Port Clements', NULL, NULL),
(173, 'Pouce Coupe', NULL, NULL),
(174, 'Queen Charlotte', NULL, NULL),
(175, 'Radium Hot Springs', NULL, NULL),
(176, 'Salmo', NULL, NULL),
(177, 'Sayward', NULL, NULL),
(178, 'Silverton', NULL, NULL),
(179, 'Slocan', NULL, NULL),
(180, 'Tahsis', NULL, NULL),
(181, 'Telkwa', NULL, NULL),
(182, 'Valemount', NULL, NULL),
(183, 'Warfield', NULL, NULL),
(184, 'Zeballos', NULL, NULL),
(185, 'Sechelt', NULL, NULL),
(186, 'Jumbo Glacier', NULL, NULL),
(187, 'Sun Peaks', NULL, NULL),
(188, 'Northern Rockies', NULL, NULL),
(189, 'Whistler', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `restrictions`
--

CREATE TABLE `restrictions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `restriction_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `restrictions`
--

INSERT INTO `restrictions` (`id`, `restriction_name`, `created_at`, `updated_at`) VALUES
(1, 'Has Vehicle', NULL, NULL),
(2, 'Rain towers', NULL, NULL),
(3, 'Will Kiss Male', NULL, NULL),
(4, 'Will Kiss Female', NULL, NULL),
(5, 'Will Kiss Partner', NULL, NULL),
(6, 'Will Kiss', NULL, NULL),
(7, 'Cigar', NULL, NULL),
(8, 'Herbal Cigarette', NULL, NULL),
(9, 'Smoke other', NULL, NULL),
(10, 'Cigarette', NULL, NULL),
(11, 'Vape', NULL, NULL),
(12, 'Open Working Day', NULL, NULL),
(13, 'Applicable to Film', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `restriction_perfomers`
--

CREATE TABLE `restriction_perfomers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `performer_id` int(10) UNSIGNED DEFAULT NULL,
  `restriction_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seasons`
--

CREATE TABLE `seasons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `season` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seasons`
--

INSERT INTO `seasons` (`id`, `season`, `created_at`, `updated_at`) VALUES
(1, 'All Season', NULL, NULL),
(2, 'Fall', NULL, NULL),
(3, 'Spring', NULL, NULL),
(4, 'Summer', NULL, NULL),
(5, 'Winter', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` int(10) UNSIGNED NOT NULL,
  `skill_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `performer_id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_video` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skill_gallery_tags`
--

CREATE TABLE `skill_gallery_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skill_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skin_tones`
--

CREATE TABLE `skin_tones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `skin_tone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `skin_tones`
--

INSERT INTO `skin_tones` (`id`, `skin_tone`, `created_at`, `updated_at`) VALUES
(1, 'Black', NULL, NULL),
(2, 'Brown', NULL, NULL),
(3, 'Dark', NULL, NULL),
(4, 'Dark Brown', NULL, NULL),
(5, 'Fair', NULL, NULL),
(6, 'Light', NULL, NULL),
(7, 'Medium', NULL, NULL),
(8, 'Olive', NULL, NULL),
(9, 'Tan', NULL, NULL),
(10, 'Very Fair', NULL, NULL),
(26, 'White', NULL, NULL),
(27, 'Dark/ olive', NULL, NULL),
(28, 'Yellow', NULL, NULL),
(29, 'Olive/fair', NULL, NULL),
(30, 'Light with tan', NULL, NULL),
(31, 'Olive, Caucasian ', NULL, NULL),
(32, 'Tanned', NULL, NULL),
(33, 'Medium-Dark', NULL, NULL),
(34, 'Medium mid tone.  Changing seasonally ', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_accessories`
--

CREATE TABLE `vehicle_accessories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vehicle_accessory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_accessories`
--

INSERT INTO `vehicle_accessories` (`id`, `vehicle_accessory`, `created_at`, `updated_at`) VALUES
(11, 'ATV Plow', NULL, NULL),
(12, 'Bike Rack (Back)', NULL, NULL),
(13, 'Bike Rack (Roof)', NULL, NULL),
(14, 'Front Jeep Bumpers', NULL, NULL),
(15, 'Front Truck Bumper', NULL, NULL),
(16, 'Front Winch', NULL, NULL),
(17, 'Hard Top', NULL, NULL),
(18, 'Lift Kit', NULL, NULL),
(19, 'Mesh Doors', NULL, NULL),
(20, 'Mounted Flood Lights', NULL, NULL),
(21, 'Rear Jeep Bumpers', NULL, NULL),
(22, 'Rear Truck Job Boxes', NULL, NULL),
(23, 'Rear Winch', NULL, NULL),
(24, 'Roof Racks', NULL, NULL),
(25, 'Soft Top', NULL, NULL),
(26, 'Truck Canopy', NULL, NULL),
(27, 'Winter Tires', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_makes`
--

CREATE TABLE `vehicle_makes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vehicle_make` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_makes`
--

INSERT INTO `vehicle_makes` (`id`, `vehicle_make`, `created_at`, `updated_at`) VALUES
(1, 'Acura', NULL, NULL),
(2, 'Alfa Romeo', NULL, NULL),
(3, 'AMC', NULL, NULL),
(4, 'Aprilla', NULL, NULL),
(5, 'Ariel Atom', NULL, NULL),
(6, 'Aston Martin', NULL, NULL),
(7, 'Audi', NULL, NULL),
(8, 'Austin Healey', NULL, NULL),
(9, 'Bentley', NULL, NULL),
(10, 'BMW', NULL, NULL),
(11, 'Bugatti', NULL, NULL),
(12, 'Buick', NULL, NULL),
(13, 'Cadillac', NULL, NULL),
(14, 'Caterham', NULL, NULL),
(15, 'Chevrolet', NULL, NULL),
(16, 'Chrysler', NULL, NULL),
(17, 'Citroen', NULL, NULL),
(18, 'Daewoo', NULL, NULL),
(19, 'Daihatsu', NULL, NULL),
(20, 'Datsun', NULL, NULL),
(21, 'De Tomaso', NULL, NULL),
(22, 'Dodge', NULL, NULL),
(23, 'Ducati', NULL, NULL),
(24, 'Eagle', NULL, NULL),
(25, 'Ferrari', NULL, NULL),
(26, 'Fiat', NULL, NULL),
(27, 'Fisker', NULL, NULL),
(28, 'Ford', NULL, NULL),
(29, 'Geo', NULL, NULL),
(30, 'GMC', NULL, NULL),
(31, 'Harley Davison', NULL, NULL),
(32, 'Holden', NULL, NULL),
(33, 'Honda', NULL, NULL),
(34, 'Hummer', NULL, NULL),
(35, 'Hyundai', NULL, NULL),
(36, 'Infiniti', NULL, NULL),
(37, 'Isuzu', NULL, NULL),
(38, 'Jaguar', NULL, NULL),
(39, 'Jeep', NULL, NULL),
(40, 'Kawasaki', NULL, NULL),
(41, 'Kia', NULL, NULL),
(42, 'Koenigsegg', NULL, NULL),
(43, 'Lamborghini', NULL, NULL),
(44, 'Lancia', NULL, NULL),
(45, 'Land Rover', NULL, NULL),
(46, 'Lexus', NULL, NULL),
(47, 'Lincoln', NULL, NULL),
(48, 'Lotus', NULL, NULL),
(49, 'Maserati', NULL, NULL),
(50, 'Maybach', NULL, NULL),
(51, 'Mazda', NULL, NULL),
(52, 'McLaren', NULL, NULL),
(53, 'Mercedes', NULL, NULL),
(54, 'Mercury', NULL, NULL),
(55, 'MG', NULL, NULL),
(56, 'Mini', NULL, NULL),
(57, 'Mitsubishi', NULL, NULL),
(58, 'Morgan', NULL, NULL),
(59, 'Mosler/Rossion', NULL, NULL),
(60, 'Nissan', NULL, NULL),
(61, 'Noble', NULL, NULL),
(62, 'Oldsmobile', NULL, NULL),
(63, 'Opel', NULL, NULL),
(64, 'Other', NULL, NULL),
(65, 'Pagani', NULL, NULL),
(66, 'Peugeot', NULL, NULL),
(67, 'Piaggio', NULL, NULL),
(68, 'Plymouth', NULL, NULL),
(69, 'Pontiac', NULL, NULL),
(70, 'Porsche', NULL, NULL),
(71, 'Proton', NULL, NULL),
(72, 'Ram', NULL, NULL),
(73, 'Renault', NULL, NULL),
(74, 'Rolls Royce', NULL, NULL),
(75, 'Saab', NULL, NULL),
(76, 'Saleen', NULL, NULL),
(77, 'Saturn', NULL, NULL),
(78, 'Scion', NULL, NULL),
(79, 'Seat', NULL, NULL),
(80, 'Shelby', NULL, NULL),
(81, 'Skoda', NULL, NULL),
(82, 'Smart', NULL, NULL),
(83, 'Subaru', NULL, NULL),
(84, 'Suzuki', NULL, NULL),
(85, 'Tesla', NULL, NULL),
(86, 'Toyota', NULL, NULL),
(87, 'Triumph', NULL, NULL),
(88, 'Vauxhall', NULL, NULL),
(89, 'Vespa', NULL, NULL),
(90, 'Volkswagen', NULL, NULL),
(91, 'Volvo', NULL, NULL),
(92, 'Westfield', NULL, NULL),
(93, 'Yamaha', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_types`
--

CREATE TABLE `vehicle_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vehicle_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_types`
--

INSERT INTO `vehicle_types` (`id`, `vehicle_type`, `created_at`, `updated_at`) VALUES
(1, 'Camper', NULL, NULL),
(2, 'Collector', NULL, NULL),
(3, 'Compact', NULL, NULL),
(4, 'Convertible', NULL, NULL),
(5, 'Crossover', NULL, NULL),
(6, 'Custom', NULL, NULL),
(7, 'Motorcycle', NULL, NULL),
(8, 'Other', NULL, NULL),
(9, 'Pickup Truck', NULL, NULL),
(10, 'RV', NULL, NULL),
(11, 'Scooter', NULL, NULL),
(12, 'Sedan', NULL, NULL),
(13, 'Sports Car', NULL, NULL),
(14, 'Station Wagon', NULL, NULL),
(15, 'SUV', NULL, NULL),
(16, 'Van', NULL, NULL),
(17, 'Vintage', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wardrobe_accessories`
--

CREATE TABLE `wardrobe_accessories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `wardrobe_accessory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wardrobe_accessories`
--

INSERT INTO `wardrobe_accessories` (`id`, `wardrobe_accessory`, `created_at`, `updated_at`) VALUES
(1, 'Hat', NULL, NULL),
(2, 'Earrings', NULL, NULL),
(3, 'Bag/Purse', NULL, NULL),
(4, 'Shoes', NULL, NULL),
(5, 'Scarves', NULL, NULL),
(6, 'Jewellery', NULL, NULL),
(7, 'Umbrella', NULL, NULL),
(8, 'Hair Accessories', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wardrobe_types`
--

CREATE TABLE `wardrobe_types` (
  `id` int(11) NOT NULL,
  `wardrobe_type` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wardrobe_types`
--

INSERT INTO `wardrobe_types` (`id`, `wardrobe_type`, `created_at`, `updated_at`) VALUES
(1, 'Athletic / Sports / Exercise', '2019-07-03 12:21:56', '0000-00-00'),
(2, 'Casual', '2019-07-03 12:21:56', '0000-00-00'),
(3, 'Upscale Casual', '2019-07-03 12:21:56', '0000-00-00'),
(4, 'Formal', '2019-07-03 12:21:56', '0000-00-00'),
(5, 'Semi-Formal', '2019-07-03 12:21:56', '0000-00-00'),
(6, 'Cocktail Attire', '2019-07-03 12:21:56', '0000-00-00'),
(7, 'Wedding Attire', '2019-07-03 12:21:56', '0000-00-00'),
(8, 'Business', '2019-07-03 12:21:56', '0000-00-00'),
(9, 'Business Casual', '2019-07-03 12:21:56', '0000-00-00'),
(10, 'Traditional Ethnic', '2019-07-03 12:21:56', '0000-00-00'),
(11, 'Country / Western', '2019-07-03 12:21:56', '0000-00-00'),
(12, 'Outdoors / Hiking / Camping', '2019-07-03 12:21:56', '0000-00-00'),
(13, 'Swimwear', '2019-07-03 12:21:56', '0000-00-00'),
(14, 'Costumes', '2019-07-03 12:21:56', '0000-00-00'),
(15, 'Sleepwear', '2019-07-03 12:21:56', '0000-00-00'),
(16, 'Nightclub', '2019-07-03 12:21:56', '0000-00-00'),
(17, 'Outerwear', '2019-07-03 12:21:56', '0000-00-00'),
(18, 'Undergarments', '2019-07-03 12:21:56', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `work_perfomers`
--

CREATE TABLE `work_perfomers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `resume_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appearance`
--
ALTER TABLE `appearance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `appearance_ethnicity_id_foreign` (`ethnicity_id`),
  ADD KEY `appearance_performer_id_foreign` (`performer_id`),
  ADD KEY `appearance_skin_tone_id_foreign` (`skin_tone_id`),
  ADD KEY `appearance_hair_color_id_foreign` (`hair_color_id`),
  ADD KEY `appearance_eyes_id_foreign` (`eyes_id`),
  ADD KEY `appearance_handedness_id_foreign` (`handedness_id`);

--
-- Indexes for table `body_traits`
--
ALTER TABLE `body_traits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `closets`
--
ALTER TABLE `closets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `closets_performer_id_foreign` (`performer_id`);

--
-- Indexes for table `closet_gallery_tags`
--
ALTER TABLE `closet_gallery_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `closet_gallery_tags_closet_id_foreign` (`closet_id`);

--
-- Indexes for table `corporate_admins`
--
ALTER TABLE `corporate_admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `corporate_admins_email_unique` (`email`);

--
-- Indexes for table `ethnicities`
--
ALTER TABLE `ethnicities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eyes`
--
ALTER TABLE `eyes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hair_colors`
--
ALTER TABLE `hair_colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hair_facials`
--
ALTER TABLE `hair_facials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hair_lengths`
--
ALTER TABLE `hair_lengths`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_uniq_cmn_hair_length_name` (`hair_length`) USING BTREE;

--
-- Indexes for table `hair_styles`
--
ALTER TABLE `hair_styles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `handednesses`
--
ALTER TABLE `handednesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internal_staffs`
--
ALTER TABLE `internal_staffs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `internal_staffs_email_unique` (`email`);

--
-- Indexes for table `media_closets`
--
ALTER TABLE `media_closets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_closets_closet_id_foreign` (`closet_id`);

--
-- Indexes for table `media_portfolios`
--
ALTER TABLE `media_portfolios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_portfolios_portfolio_id_foreign` (`portfolio_id`);

--
-- Indexes for table `media_skills`
--
ALTER TABLE `media_skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_skills_skill_id_foreign` (`skill_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nationalities`
--
ALTER TABLE `nationalities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nudity`
--
ALTER TABLE `nudity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nudity_perfomers`
--
ALTER TABLE `nudity_perfomers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nudity_perfomers_performer_id_foreign` (`performer_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `performers`
--
ALTER TABLE `performers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `performers_union_number_unique` (`union_number`);

--
-- Indexes for table `performers_details`
--
ALTER TABLE `performers_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `performers_details_performer_id_foreign` (`performer_id`);

--
-- Indexes for table `performer_agendas`
--
ALTER TABLE `performer_agendas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `performer_availability`
--
ALTER TABLE `performer_availability`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `performer_portfolios`
--
ALTER TABLE `performer_portfolios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `performer_portfolios_performer_id_foreign` (`performer_id`);

--
-- Indexes for table `performer_status`
--
ALTER TABLE `performer_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `performer_unions`
--
ALTER TABLE `performer_unions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pets`
--
ALTER TABLE `pets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `piercings`
--
ALTER TABLE `piercings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_gallery_tags`
--
ALTER TABLE `portfolio_gallery_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_gallery_tags_portfolio_id_foreign` (`portfolio_id`);

--
-- Indexes for table `project_perfomers`
--
ALTER TABLE `project_perfomers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_perfomers_resume_id_foreign` (`resume_id`);

--
-- Indexes for table `project_roles`
--
ALTER TABLE `project_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_roles_project_id_foreign` (`project_id`);

--
-- Indexes for table `props`
--
ALTER TABLE `props`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restrictions`
--
ALTER TABLE `restrictions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restriction_perfomers`
--
ALTER TABLE `restriction_perfomers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `restriction_perfomers_performer_id_foreign` (`performer_id`);

--
-- Indexes for table `seasons`
--
ALTER TABLE `seasons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `skills_performer_id_foreign` (`performer_id`);

--
-- Indexes for table `skill_gallery_tags`
--
ALTER TABLE `skill_gallery_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `skill_gallery_tags_skill_id_foreign` (`skill_id`);

--
-- Indexes for table `skin_tones`
--
ALTER TABLE `skin_tones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_accessories`
--
ALTER TABLE `vehicle_accessories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_makes`
--
ALTER TABLE `vehicle_makes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_types`
--
ALTER TABLE `vehicle_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wardrobe_accessories`
--
ALTER TABLE `wardrobe_accessories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wardrobe_types`
--
ALTER TABLE `wardrobe_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `work_perfomers`
--
ALTER TABLE `work_perfomers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appearance`
--
ALTER TABLE `appearance`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `body_traits`
--
ALTER TABLE `body_traits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `closets`
--
ALTER TABLE `closets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `closet_gallery_tags`
--
ALTER TABLE `closet_gallery_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `corporate_admins`
--
ALTER TABLE `corporate_admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ethnicities`
--
ALTER TABLE `ethnicities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `eyes`
--
ALTER TABLE `eyes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `hair_colors`
--
ALTER TABLE `hair_colors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `hair_facials`
--
ALTER TABLE `hair_facials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `hair_lengths`
--
ALTER TABLE `hair_lengths`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `hair_styles`
--
ALTER TABLE `hair_styles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `handednesses`
--
ALTER TABLE `handednesses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `internal_staffs`
--
ALTER TABLE `internal_staffs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `media_closets`
--
ALTER TABLE `media_closets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media_portfolios`
--
ALTER TABLE `media_portfolios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media_skills`
--
ALTER TABLE `media_skills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `nationalities`
--
ALTER TABLE `nationalities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=232;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nudity`
--
ALTER TABLE `nudity`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nudity_perfomers`
--
ALTER TABLE `nudity_perfomers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `performers`
--
ALTER TABLE `performers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT for table `performers_details`
--
ALTER TABLE `performers_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `performer_agendas`
--
ALTER TABLE `performer_agendas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `performer_availability`
--
ALTER TABLE `performer_availability`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `performer_portfolios`
--
ALTER TABLE `performer_portfolios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `performer_status`
--
ALTER TABLE `performer_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `performer_unions`
--
ALTER TABLE `performer_unions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `pets`
--
ALTER TABLE `pets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- AUTO_INCREMENT for table `piercings`
--
ALTER TABLE `piercings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `portfolio_gallery_tags`
--
ALTER TABLE `portfolio_gallery_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project_perfomers`
--
ALTER TABLE `project_perfomers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_roles`
--
ALTER TABLE `project_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `props`
--
ALTER TABLE `props`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;

--
-- AUTO_INCREMENT for table `restrictions`
--
ALTER TABLE `restrictions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `restriction_perfomers`
--
ALTER TABLE `restriction_perfomers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seasons`
--
ALTER TABLE `seasons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skill_gallery_tags`
--
ALTER TABLE `skill_gallery_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skin_tones`
--
ALTER TABLE `skin_tones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `vehicle_accessories`
--
ALTER TABLE `vehicle_accessories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `vehicle_makes`
--
ALTER TABLE `vehicle_makes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `vehicle_types`
--
ALTER TABLE `vehicle_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `wardrobe_accessories`
--
ALTER TABLE `wardrobe_accessories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wardrobe_types`
--
ALTER TABLE `wardrobe_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `work_perfomers`
--
ALTER TABLE `work_perfomers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appearance`
--
ALTER TABLE `appearance`
  ADD CONSTRAINT `appearance_ethnicity_id_foreign` FOREIGN KEY (`ethnicity_id`) REFERENCES `ethnicities` (`id`),
  ADD CONSTRAINT `appearance_eyes_id_foreign` FOREIGN KEY (`eyes_id`) REFERENCES `eyes` (`id`),
  ADD CONSTRAINT `appearance_hair_color_id_foreign` FOREIGN KEY (`hair_color_id`) REFERENCES `hair_colors` (`id`),
  ADD CONSTRAINT `appearance_handedness_id_foreign` FOREIGN KEY (`handedness_id`) REFERENCES `handednesses` (`id`),
  ADD CONSTRAINT `appearance_performer_id_foreign` FOREIGN KEY (`performer_id`) REFERENCES `performers` (`id`),
  ADD CONSTRAINT `appearance_skin_tone_id_foreign` FOREIGN KEY (`skin_tone_id`) REFERENCES `skin_tones` (`id`);

--
-- Constraints for table `closets`
--
ALTER TABLE `closets`
  ADD CONSTRAINT `closets_performer_id_foreign` FOREIGN KEY (`performer_id`) REFERENCES `performers` (`id`);

--
-- Constraints for table `closet_gallery_tags`
--
ALTER TABLE `closet_gallery_tags`
  ADD CONSTRAINT `closet_gallery_tags_closet_id_foreign` FOREIGN KEY (`closet_id`) REFERENCES `closets` (`id`);

--
-- Constraints for table `media_closets`
--
ALTER TABLE `media_closets`
  ADD CONSTRAINT `media_closets_closet_id_foreign` FOREIGN KEY (`closet_id`) REFERENCES `closets` (`id`);

--
-- Constraints for table `media_portfolios`
--
ALTER TABLE `media_portfolios`
  ADD CONSTRAINT `media_portfolios_portfolio_id_foreign` FOREIGN KEY (`portfolio_id`) REFERENCES `performer_portfolios` (`id`);

--
-- Constraints for table `media_skills`
--
ALTER TABLE `media_skills`
  ADD CONSTRAINT `media_skills_skill_id_foreign` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`);

--
-- Constraints for table `nudity_perfomers`
--
ALTER TABLE `nudity_perfomers`
  ADD CONSTRAINT `nudity_perfomers_performer_id_foreign` FOREIGN KEY (`performer_id`) REFERENCES `performers` (`id`);

--
-- Constraints for table `performers_details`
--
ALTER TABLE `performers_details`
  ADD CONSTRAINT `performers_details_performer_id_foreign` FOREIGN KEY (`performer_id`) REFERENCES `performers` (`id`);

--
-- Constraints for table `performer_portfolios`
--
ALTER TABLE `performer_portfolios`
  ADD CONSTRAINT `performer_portfolios_performer_id_foreign` FOREIGN KEY (`performer_id`) REFERENCES `performers` (`id`);

--
-- Constraints for table `portfolio_gallery_tags`
--
ALTER TABLE `portfolio_gallery_tags`
  ADD CONSTRAINT `portfolio_gallery_tags_portfolio_id_foreign` FOREIGN KEY (`portfolio_id`) REFERENCES `performer_portfolios` (`id`);

--
-- Constraints for table `project_perfomers`
--
ALTER TABLE `project_perfomers`
  ADD CONSTRAINT `project_perfomers_resume_id_foreign` FOREIGN KEY (`resume_id`) REFERENCES `work_perfomers` (`id`);

--
-- Constraints for table `project_roles`
--
ALTER TABLE `project_roles`
  ADD CONSTRAINT `project_roles_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `project_perfomers` (`id`);

--
-- Constraints for table `restriction_perfomers`
--
ALTER TABLE `restriction_perfomers`
  ADD CONSTRAINT `restriction_perfomers_performer_id_foreign` FOREIGN KEY (`performer_id`) REFERENCES `performers` (`id`);

--
-- Constraints for table `skills`
--
ALTER TABLE `skills`
  ADD CONSTRAINT `skills_performer_id_foreign` FOREIGN KEY (`performer_id`) REFERENCES `performers` (`id`);

--
-- Constraints for table `skill_gallery_tags`
--
ALTER TABLE `skill_gallery_tags`
  ADD CONSTRAINT `skill_gallery_tags_skill_id_foreign` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
