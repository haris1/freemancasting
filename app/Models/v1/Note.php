<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = ['notes','performer_id','username','internal_staff_id'];
    public function staffmember()
    {
                return $this->belongsTo('App\Models\v1\InternalStaff','internal_staff_id');        
    }
}
