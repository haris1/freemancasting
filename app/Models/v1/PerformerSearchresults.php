<?php

namespace App\Models\v1;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\notifications\InternalStaffResetPasswordnotification;

class PerformerSearchresults extends Authenticatable
{
  
   protected $table  = 'performer_searchresults';
     
   
    protected $fillable = [
        'my_performer',
        'search_parameter',
        'internalstaff_id' , 
    ];

  
}

