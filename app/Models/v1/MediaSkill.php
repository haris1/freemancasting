<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class MediaSkill extends Model
{
   
   protected $table  = 'media_skills';

   protected $fillable =['media_url','skill_id','media_title','tag','media_type','performers_id'];

   protected $appends  =['image_url'];

   public function checkIsVideo()
   {
      return $this->blongsToMany('App\Models\v1\PerformerSkill','skill_id');
   }

   public function getImageUrlAttribute($value)
    {
        $value = $this->media_url;
        $perId = $this->performers_id;
        if($value){
                    return asset('storage/skillsmedia').'/'.$perId.'/'.$this->media_url;     
            }else{
                    return asset('/noimage.jpg');
            }
    }
}


