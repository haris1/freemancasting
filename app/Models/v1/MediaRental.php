<?php
namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MediaRental extends Model
{

   
   protected $table  = 'media_rentals';

   protected $fillable =['media_url','rental_id','media_title','tag','media_type','performers_id'];

   protected $appends  =['image_url'];

   

   public function getImageUrlAttribute($value)
   {
    $value = $this->media_url;
      $perId= $this->performers_id;
    if($value){ 

      return asset('storage/performers').'/'.$perId.'/'.$this->media_url;     
    }else{
      return asset('/noimage.jpg');
    }
  }
}
