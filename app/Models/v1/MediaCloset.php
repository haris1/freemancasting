<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MediaCloset extends Model
{
   
   protected $table  = 'media_closets';

   protected $fillable =['media_url','closet_id','media_type','media_title','tag','performers_id'];

   protected $appends  =['image_url'];

   public function getImageUrlAttribute($value)
    {
        $value = $this->media_url;
        $perId = $this->performers_id;
        if($value){
                    return asset('/storage/closetmedia').'/'.$perId.'/'.$this->media_url;     
            }else{
                    return asset('/noimage.jpg');
            }
    }

    
}
