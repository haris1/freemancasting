<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $table='agency';
    protected $fillable = [
        'agencyName',
        'agencyAbbreviation'
    ];
}
