<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class HelpDesk extends Model
{
    protected $table  = 'help_desk';

    protected $fillable =['portfolio_help','closet_help','skills_help','stats_help','props_help','resume_help','document_help'];
}
