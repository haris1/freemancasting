<?php

namespace App\Models\v1;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
class PerformersDetails extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

       protected $table  = 'performers_details';

    protected $fillable = [
        'hairStyle',
        'hairFacial',
        'bgPerformerDetails',
        'unionAffiliations',
        'unionNum',
        'visa_student',
        'visa_international',
        'visa_open',
        'visa_film',
        'visa_film_other',
        'shirt',
        'agentDetails',
        'castingTitle',
        'job',
        'cdnCitizenship',
        'visa',
        'dayPhone',
        'eveningPhone',
        'emailSecondary',
        'applicationFor',
        'referral1_name',
        'referral1_phone',
        'referral2_name',
        'referral2_phone',
        'emergencyContact1_name',
        'emergencyContact1_phone',
        'emergencyContact1_email',
        'emergencyContact2_name',
        'emergencyContact2_phone',
        'emergencyContact2_email',
        'driversLicense',
        'leftHandDrive',
        'neck',
        'chest',
        'insea',
        'resume',
        'bgPerformer',
        'availability',
        'cdnCitizen',
        'bcResident',
        'taxes',
        'sin',
        'languageFirst',
        'languageSecond',
    ];

     
    
}   
