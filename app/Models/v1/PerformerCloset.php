<?php

namespace App\Models\v1;
use Illuminate\Database\Eloquent\Model;

class PerformerCloset extends Model
{
    //    
   protected $table  = 'closets';
   
   protected $fillable =['closet_title','descripation','performer_id','is_video'];

   protected $appends  =['closet_media_count'];

   public function performerMedia()
    {
    	return $this->hasMany('App\Models\v1\MediaCloset','closet_id');
    }
    public function closetTag()
    {
      return $this->hasMany('App\Models\v1\ClosetGalleryTag','closet_id');
    }

     public function getClosetMediaCountAttribute()
    {
        return $this->performerMedia()->count();
    }
   
}
