<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class VehicleMake extends Model
{
    protected $fillable = [
        'vehicle_make'
    ];
}
