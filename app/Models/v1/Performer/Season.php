<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    protected $fillable = [
        'season'
    ];
}
