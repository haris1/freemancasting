<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Skintone extends Model
{
	protected $table  	= 'skin_tones';

    protected $fillable = [
        'Skin_tone'
    ];
}
