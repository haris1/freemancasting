<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Restriction extends Model
{
    protected $fillable = [
        'restriction_name',
        'restrictions_type'
    ];
}
