<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class PerformerAvailability extends Model
{
    protected $table='performer_availability';
     protected $fillable = [
        'availability'
    ];
}
