<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class PerformersFlags extends Model
{
    
    protected $table 	= "performer_flags";
    protected $fillable = ['performer_id','internal_staff_id','flag_id','note'];


    public function flag()
    {
        return $this->belongsTo('App\Models\v1\Performer\Flags','flag_id');
    }
}
