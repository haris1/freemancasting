<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Piercing extends Model
{
    	 protected $table='piercings';
         protected $fillable = [
            'piercing'
        ];
}
