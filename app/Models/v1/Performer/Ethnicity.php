<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Ethnicity extends Model
{
    protected $fillable = [
        'ethnicity'
    ];
}
