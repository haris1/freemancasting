<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class RestrictionPerformer extends Model
{
    protected $table='restriction_perfomers';
    protected $fillable = [
        'performer_id',
        'restriction_id'
    ];
     public function restrictionName()
    {
       return $this->belongsTo('App\Models\v1\Performer\Restriction','restriction_id');        
    }
}
