<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class HairLength extends Model
{
    protected $table='hair_lengths';

    protected $fillable = [
        'hair_length'
    ];
}
