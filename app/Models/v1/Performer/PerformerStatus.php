<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class PerformerStatus extends Model
{
	 protected $table='performer_status';
     protected $fillable = [
        'performer_status'
    ];
}
