<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Eyes extends Model
{
    protected $fillable = [
        'eyes'
    ];
}
