<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Nationality extends Model
{
    protected $fillable = [
        'nationality'
    ];
}
