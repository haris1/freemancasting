<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class VehicleAccessory extends Model
{
    protected $fillable = [
        'vehicle_accessory'
    ];
}
