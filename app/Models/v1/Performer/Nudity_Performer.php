<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Nudity_Performer extends Model
{
    protected $table='nudity_perfomers';
    protected $fillable = ['performer_id','nudity_id'];

    public function nudityName()
    {
     return $this->belongsTo('App\Models\v1\Performer\Nudity','nudity_id');        
    }
}
