<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class HairFacial extends Model
{
    protected $fillable = [
        'hair_facial'
    ];
}
