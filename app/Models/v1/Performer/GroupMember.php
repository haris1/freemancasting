<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
    protected $table = 'group_members';
    protected $appends  =['full_name'];
    protected $fillable = [
    	'member_first_name',
    	'member_last_name',
    	'member_email',
    	'member_phone',
    	'performer_id'
    ];



 function getFullNameAttribute() {
        return $this->member_first_name . ' ' . $this->member_last_name;
  }
}

