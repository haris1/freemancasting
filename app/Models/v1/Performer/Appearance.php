<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Appearance extends Model
{
    protected $table='appearance';
    protected $fillable = [
        'ethnicity_id',
        'skin_tone_id',
        'hair_color_id',
        'eyes_id',
        'region_id',
        'handedness_id',
        'nationality_id',
        'height',
        'weight',
        'bust',
        'waist',
        'hips',
        'dress',
        'jeans',
        'shoes',
        'contacts',
        'hair_length_id',
        'gloves',
        'glasses',
        'gender',
        'tattoos',
        'collar',
        'jacket',
        'sleeve',
        'pant',
        'hat',
        'ring',
        'size',
        'vehicle',
        'pets',
        'performer_id',
        'collar',
        'sleeve',
        'pant',
        'ring',
        'hat',
        'jacket',
        'chest',
        'infant_size',
        'child_size',
        'inseam',
        'double_for',
        'piercing',
        'body_trait_id',
        'hair_style_id',
        'facial_hair_id'
    ];

    
    public function ethnicity()
    {
                return $this->belongsTo('App\Models\v1\Performer\Ethnicity','ethnicity_id');        
    }
    public function skintone()
    {
                return $this->belongsTo('App\Models\v1\Performer\Skintone','skin_tone_id');        
    }
    public function haircolor()
    {
                return $this->belongsTo('App\Models\v1\Performer\HairColor','hair_color_id');        
    }
     public function hairlength()
    {
                return $this->belongsTo('App\Models\v1\Performer\HairLength','hair_length_id');        
    }
    public function eyes()
    {
        return $this->belongsTo('App\Models\v1\Performer\Eyes','eyes_id');
    }
    public function hairstyle()
    {
        return $this->belongsTo('App\Models\v1\Performer\HairStyle','hair_style_id');
    }
    public function hairfacial()
    {
        return $this->belongsTo('App\Models\v1\Performer\HairFacial','facial_hair_id');
    }
    public function bodytrait()
    {
        return $this->belongsTo('App\Models\v1\Performer\BodyTrait','body_trait_id');
    }
    public function handedness()
    {
                return $this->belongsTo('App\Models\v1\Performer\Handedness','handedness_id');        
    }
    public function nationality()
    {
                return $this->belongsTo('App\Models\v1\Performer\Nationality','nationality_id');        
    }
    public function region()
    {
                return $this->belongsTo('App\Models\v1\Performer\Region','region_id');        
    }
    


    
}
