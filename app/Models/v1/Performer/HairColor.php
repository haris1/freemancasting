<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class HairColor extends Model
{
    protected $fillable = [
        'hair_color'
    ];
}
