<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $fillable = [
        'region'
    ];
}
