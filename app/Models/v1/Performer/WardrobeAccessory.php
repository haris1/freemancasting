<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class WardrobeAccessory extends Model
{
    protected $fillable = [
        'wardrobe_accessory'
    ];
}
