<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class HairStyle extends Model
{
    protected $fillable = [
        'hair_style'
    ];
}
