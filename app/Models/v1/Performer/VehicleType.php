<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    protected $fillable = [
        'vehicle_type'
    ];
}
