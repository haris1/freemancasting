<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class BodyTrait extends Model
{
    protected $fillable = [
        'body_trait'
    ];
}
