<?php

namespace App\Models\v1\Performer;


use Illuminate\Database\Eloquent\Model;

class PerformerUnion extends Model
{
    protected $table='performer_unions';
     protected $fillable = [
        'union',
         
    ];
}
