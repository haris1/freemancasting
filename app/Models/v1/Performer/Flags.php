<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Flags extends Model
{
    
    protected $table = "flags";
    protected $fillable = ['flag_name'];
    protected $appends =['flag_count'];
    


    public function Flags()
    {
        return $this->hasMany('App\Models\v1\Performer\PerformersFlags','flag_id');
    }

    public function getFlagCountAttribute()
    {
        return $this->Flags()->count();
    }
}
