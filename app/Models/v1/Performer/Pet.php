<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    protected $fillable = [
        'petName'
    ];
}
