<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Tattoo extends Model
{
    	 protected $table='tattoos';
         protected $fillable = [
            'tattoo'
        ];
}
