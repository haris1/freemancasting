<?php

namespace App\Models\v1\Performer;

use Illuminate\Database\Eloquent\Model;

class Handedness extends Model
{
    protected $fillable = [
        'handedness'
    ];
}
