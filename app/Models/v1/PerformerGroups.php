<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class PerformerGroups extends Model
{
	protected $table    = "performer_groups";		
    protected $fillable = ['group_name'];

    public function groupPerformers()
    {
       return $this->hasMany('App\Models\v1\Performer','group_id')->select('id','firstName','middleName','lastName','email','phonenumber','address','city','province','postalCode','referred_by','age','agent_id','union_name','group_id','instagram','twitter');        
    }

    
}
