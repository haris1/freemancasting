<?php

namespace App\Models\v1;
use Illuminate\Database\Eloquent\Model;

class PerformerAgent extends Model
{
    protected $table  = 'performer_agents';
    protected $fillable =['agent_name','agency_id'];

    public function agencies()
    {
       return $this->belongsto('App\Models\v1\Agency','agency_id');        
    } 
}


