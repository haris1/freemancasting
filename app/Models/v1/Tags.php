<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
	protected $table	= 'tags';
    protected $fillable = ['tag'];
}