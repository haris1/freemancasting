<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class SkillSection extends Model
{
     protected $table  = 'skill_sections';
   
   protected $fillable =[
   			'field_name',
   			'field_value',
   			'skill_id'
   		];

   	
}
