<?php

namespace App\Models\v1;
use Illuminate\Database\Eloquent\Model;

class PerformerPortfolio extends Model
{
    //    
   protected $table  = 'performer_portfolios';
   
   protected $fillable =['portfolio_title','description','performer_id','is_video'];

   public function performerMedia()
    {
    	return $this->hasMany('App\Models\v1\MediaPortfolio','portfolio_id');
    }
        
    public function portfolioTag()
    {
    	return $this->hasMany('App\Models\v1\PortfolioGalleryTag','portfolio_id');
    }
   

    

       
}
