<?php

namespace App\Models\v1;


use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table  = 'documents';
    protected $fillable = [
        'doument_name' ,
        'doument_type' 
    ];

}
