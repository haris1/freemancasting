<?php

namespace App\Models\v1;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\CorporateAdminResetPasswordNotification;

class CorporateAdmin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $table  = 'corporate_admins';
   protected $guard = 'corporate-admin';

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CorporateAdminResetPasswordNotification($token));
    }
    
    protected $fillable = [
        'name',
        'email',
        'phonenumber',
        'password',
        'remember_token',
        'created_at',
        'updated_at'  
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

 