<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class PerformerDocument extends Model
{
    protected $table  = 'performer_documents';
    protected $fillable =['document_id','performer_id','document_status'];
}
