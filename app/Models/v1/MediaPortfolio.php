<?php
namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
class MediaPortfolio extends Model
{

   
   protected $table  = 'media_portfolios';

   protected $fillable =['media_url','portfolio_id','media_title','tag','media_type','performers_id',''];

   protected $appends  =['image_url'];

   public function getImageUrlAttribute($value)
    {
        $value = $this->media_url;
        $perId= $this->performers_id;
        if($value){ 

              return asset('storage/performers').'/'.$perId.'/'.$this->media_url;     
              //return public_path('storage/performers'.'/'.Auth::id().'/'.$this->media_url);     

            }else{
                    return asset('/noimage.jpg');
            }
    }
}
