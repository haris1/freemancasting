<?php

namespace App\Models\v1;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Performer extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    
    protected $fillable = [
        'firstName',
        'middleName',
        'lastName',
        'email',
        'phonenumber',
        'address',
        'referred_by',
        'city',
        'age',
        'province',
        'postalCode',
        'country',
        'date_of_birth',
        'gender',
        'applicationfor',
        'parent_aggrement',
        'twitter',
        'instagram',
        'facebook',
        'linkedin',
        'password',
        'umdb_url',
        'agent_id',
        'union_name',
        'union_number',
        'availability',
        'upload_primary_url',
        'remember_token',
        'group_id',
        'status_id',
        'member_of_group',
        'nameofgroup',
        'created_at',
        'updated_at'  
    ];

    protected $appends  =[
        'image_url',
        'full_name'
      
    ];
    // protected $guard = 'users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getImageUrlAttribute($value)
    {
        $value = $this->upload_primary_url;
        $id = $this->id;
        if($value && file_exists(public_path('storage/performers'.'/'.$id.'/'.$value))){
            return asset('storage/performers'.'/'.$id.'/'.$this->upload_primary_url);
        } else {
            return asset('/noimage.jpg');
        }

        
    }
    
    function getFullNameAttribute() {
        return $this->firstName . ' ' . $this->lastName;
  }
          
    public function appearance()
    {
       return $this->hasMany('App\Models\v1\Performer\Appearance','performer_id');        
    }      
    public function availability()
    {
       return $this->hasMany('App\Models\v1\Performer\PerformerAvailability','performer_id');        
    }    
    public function restriction()
    {
       return $this->hasMany('App\Models\v1\Performer\RestrictionPerformer','performer_id');        
    }   
    public function nudity()
    {
       return $this->hasMany('App\Models\v1\Performer\Nudity_Performer','performer_id');        
    }       
    public function performerrental()
    {
        return $this->hasMany('App\Models\v1\PerformerRental','performer_id');
    }
        
    public function media_portfolio(){
        return $this->hasMany('App\Models\v1\MediaPortfolio','performers_id');
    }
    public function agentDetail()
    {
      return $this->belongsTo('App\Models\v1\PerformerAgent','agent_id');        
    }
    public function groupDetail()
    {
      return $this->belongsTo('App\Models\v1\PerformerGroups','group_id');        
    }

    public function performerFlag()
    {
        return $this->hasMany('App\Models\v1\Performer\PerformersFlags','performer_id');
    }
    
    public function unionDetail()
    {
      return $this->belongsTo('App\Models\v1\Performer\PerformerUnion','union_name');        
    }


    public function PerformerCloset()
    {
      return $this->hasMany('App\Models\v1\PerformerCloset','performer_id');        
    }

    public function GroupMembers()
    {
      return $this->hasMany('App\Models\v1\Performer\GroupMember','performer_id');        
    }

    public function PerformerSkill()
    {
      return $this->hasMany('App\Models\v1\PerformerSkill','performer_id');        
    }
    
    public function Workperformer()
    {
      return $this->hasMany('App\Models\v1\Workperformer','performer_id');        
    }

    public function PerformerPortfolio()
    {
      return $this->hasMany('App\Models\v1\PerformerPortfolio','performer_id');        
    }

   

        

     public function groupPerformers()
    {
      return $this->belongsTo('App\Models\v1\PerformerGroups','group_id');        
    }

}   
