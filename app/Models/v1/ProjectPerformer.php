<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class ProjectPerformer extends Model
{
    protected $table  = 'project_perfomers';
    protected $fillable = [
      		'project_title',
      		'resume_id',
      		'IMDC_link'

      ];

      protected $appends = ['role_count'];

   public function project_role()
	{
		return $this->hasMany('App\Models\v1\ProjectRole','project_id','id');
	}

  public function getRoleCountAttribute()
  {

    return $this->project_role()->count();

  }

}
