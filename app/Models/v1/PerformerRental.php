<?php

namespace App\Models\v1;
use Illuminate\Database\Eloquent\Model;

class PerformerRental extends Model
{
    //    
   protected $table  = 'performer_rentals';
   
   protected $fillable =['rental_title','description','performer_id','is_video'];

   public function rentalMedia()
    {
    	return $this->hasMany('App\Models\v1\MediaRental','rental_id');
    }
     
   public function rentalTag()
    {
    	return $this->hasMany('App\Models\v1\RentalGalleryTag','rental_id');
    }


    

       
}
