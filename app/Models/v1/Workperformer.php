<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class Workperformer extends Model
{
    protected $table  = 'work_perfomers';
	protected $fillable = [
		'resume_title',
		'performer_id'
	];


	public function project_performer()
	{
		return $this->hasMany('App\Models\v1\ProjectPerformer','resume_id');
	}
}
