<?php

namespace App\Models\v1;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class PerformerSkill extends Model
{
    //  
   //use  SoftDeletes;  
   protected $table  = 'skills';
   protected $softDelete = true;
   protected $fillable =['skill_title','description','performer_id','is_video'];

   public function performerMedia()
    {
    	return $this->hasMany('App\Models\v1\MediaSkill','skill_id');
    }
  
    public function skillTag()
    {
    	return $this->hasMany('App\Models\v1\SkillGalleryTag','skill_id');
    }
    public function skillFields()
    {
      return $this->hasMany('App\Models\v1\SkillSection','skill_id');
    }

    
  
   
   
}
