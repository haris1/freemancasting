<?php

namespace App\Models\v1;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\InternalStaffResetPasswordnotification;

class InternalStaff extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $table  = 'internal_staffs';
    protected $guard = 'internal-staff';
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new InternalStaffResetPasswordnotification($token));
    }
    protected $fillable = [
        'lastname',
        'firstname',
        'email',
        'phonenumber',
        'password',
         'status',
        'remember_token',
        'created_at',
        'updated_at'  
    ];
    protected $appends  =[
        'full_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
      function getFullNameAttribute() {
          return $this->firstname . ' ' . $this->lastname;
    }
}

