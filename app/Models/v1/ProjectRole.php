<?php

namespace App\Models\v1;


use Illuminate\Database\Eloquent\Model;

class ProjectRole extends Model
{
     protected $table  = 'project_roles';
     protected $fillable = [
		'role_title', 'start_date', 'end_date', 'project_id',
	];
}
