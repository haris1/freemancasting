<?php

namespace App\Http\Controllers\Performers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\v1\Performer;
use App\Models\v1\HelpDesk;
use App\Models\v1\MediaRental;
use App\Models\v1\PerformerRental;
use App\Models\v1\MediaPortfolio;
use App\Models\v1\RentalGalleryTag;
use App\Models\v1\Performer\PerformersFlags;
use App\Models\v1\Performer\Flags;
use App\Models\v1\PerformerGroups;
use App\Models\v1\Tags;
use Carbon\Carbon;
use Validator;

class RentalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id=''){
 
      if(Auth::guard('web')->check()){
          $performers_user    = Auth::user();
          $per_id = $performers_user->id;  
          $authtype = 'performer';
      }else{
          $per_id = $id;
          $performers_user    = Performer::where('id',$per_id)->first();
          $authtype = 'internal-staff';
      }
             
        $performer        = Performer::where('id',$per_id)->first();
        $age              = Carbon::parse($performer->date_of_birth)->age;
        $age_range        = $this->age_range($age);
        $groupUser        = PerformerGroups::with("groupPerformers")->where('id',$performer->group_id)                    ->first();

        $rental_list      = PerformerRental::with('rentalMedia')->where('performer_id',$per_id)->get();
      

        $sidebar_image    =  MediaPortfolio::where('performers_id',$per_id)
                                  ->where('media_type','like','%image%')
                                  ->inRandomOrder()
                                  ->take(4)
                                  ->get();
         $performerFlag = PerformersFlags::with('flag')->where('performer_id',$per_id)->get();
                          
         $flag       = Flags::all();   
         $helpdesk   = HelpDesk::find(1);                   
        return view('performers.rentals',compact('performer','age_range','rental_list','sidebar_image','performers_user','groupUser','per_id','authtype','performerFlag','flag','helpdesk'));  
    }
    // Age Range Count Function
    protected  function age_range($age)
    {
        switch ($age) {

            case ($age < 19):
            return "Minor";
            break;
            case ($age >= 20 && $age <= 30):
            return "20 - 30 years";
            break;
            case ($age >= 31 && $age <= 40):
            return "30 - 40 years";
            break;
            case ($age >= 41 && $age <= 50):
            return "40 - 50 years";
            break;    
            case ($age >= 51 && $age <= 60):
            return "50 - 60 years";
            break;    
            case ($age >= 61 && $age <= 70):
            return "60 - 70 years";
            break;    
            case ($age >= 71 && $age <= 80):
            return "70 - 80 years";
            break;    
            case ($age >= 81 && $age <= 90):
            return "80 - 90 years";
            break;
            case ($age >= 91 && $age <= 100):
            return "90 - 100 years";
            break;    
            default:
            return "100 +";
        }
    }

    // Store Images For Performer Rental
    public function storeRentalImages(Request $request)
    {
        $performers_user                = Auth::user();
        $photo                          = $request['file'];
        if(!empty($photo))
        {
            $file_name                  = time(). $photo->getClientOriginalName();
            $root                       = storage_path().'/app/public/performers/'.$performers_user->id;
            $photo->move($root,$file_name);
            $performer['name']          = $file_name;
            $performer['type']          = $request['file']->getClientMimeType();
           
        }
        else
        {
            $performer['name']       =  "";
            $performer['type']       =  "";
           
        }
      return response()->json(['data'=>$performer]);
    }
    

     //store media for performers usaer 
    public function storeRantalMedia(Request $request)
    {   
       $RentalTitleCount = PerformerRental::where('rental_title',$request->get('rental_title'))->where('performer_id',Auth::id())->count();

           if($RentalTitleCount != 0){
       $validator = Validator::make($request->all(),[  
            'rental_title'                   =>  'required|unique:performer_rentals,rental_title',
        ],[
            'rental_title.required' => 'Enter Rental Title',
            'rental_title.unique' => 'Rental Title Already Taken'
        ]);
        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }
         }     
        $performer                               =   $request->all();
        $performer_user                          =   Auth::user();

        $performerRental['rental_title']         = $request->get('rental_title');
        $performerRental['description']          = $request->get('description');
        $performerRental['performer_id']         = $performer_user->id;

        if($request->is_video                    ==  "VideoUpload")
        {
            $performerRental['is_video']         =    "yes";
        }else
        {
            $performerRental['is_video']         =    "no";
        }

        $rental                                  = new PerformerRental($performerRental);
        $rental->save();

        
        $tags = null;
        if ($request->get('tags') && $request->get('tags')!="") {
            $tags = implode(',', $request->get('tags'));
        }
        if(!empty($performer['images'])){            
            $mediaStore =[];
            foreach ($performer['images'] as $key    => $value) {

                $mediaStore[$key]['media_url']       =  $value;   
                $mediaStore[$key]['media_type']      =  $performer['type'][$key];
                $mediaStore[$key]['rental_id']       =  $rental->id;
                $mediaStore[$key]['media_title']     =  $performer['media_title'];
                $mediaStore[$key]['performers_id']   =  $performer_user->id;
                $mediaStore[$key]['tag']             =  $tags;
            }
            MediaRental::insert($mediaStore);
        }
        
        $tags=[];
        if(!empty($request['tags']))
        {
            foreach ($request['tags'] as $key => $value) {
                $tags[$key]['tag'] = $request['tags'][$key];
                $tags[$key]['rental_id'] =$rental->id;

            }
        }
        RentalGalleryTag::insert($tags);

       return response()->json(['status'=>true,'message'=>'Rental Insert Successfully']);
  }

  

    //remove image or video
    public function removeRentalMedia(Request $request)
    {
         $mediaRemove = MediaRental::where('id',$request->id)->first();
         $mediaRemove->delete();
         return response()->json(['status'=>true,'message'=>'Rental Media Remove Successfully','data'=> ""]);
    }


    //Add Images 
    public function addRentalImages(Request $request)
    {

        $performers_user       =    Auth::user();

         if(!empty($request['images'])){
        $mediaStore =[];
        foreach ($request['images'] as $key       => $value) {
            $mediaStore[$key]['performers_id']    =  $performers_user->id;
            $mediaStore[$key]['rental_id']        =  $request['rental_id'];  
            $mediaStore[$key]['media_url']        =  $value;                           
            $mediaStore[$key]['media_type']       =  $request['type'][$key];
            $mediaStore[$key]['media_title']      =  $request['media_title'];
        }
      MediaRental::insert($mediaStore);
     }

      $tags=[];
        if(!empty($request['tags']))
        {
            foreach ($request['tags'] as $key => $value) {
                $tags[$key]['tag'] = $request['tags'][$key];
                $tags[$key]['rental_id'] =$request['rental_id'];

            }
        }
        RentalGalleryTag::insert($tags);
    return response()->json(['status'=>true,'message'=>'Add Image Successfully']);
    }

    public function rentalSearch(Request $request)
    {

      if(!empty($request->get_id)){
        $per_id = $request->get_id;
        
      }else{
        $per_id = Auth::id();
      }
     


     $rental_search         = PerformerRental::whereNotNull('rental_title');
     
     $rental_search             ->where('rental_title','LIKE','%'.request('search')."%");
     $rental_search             ->where('performer_id',$per_id);

     $rental_search             ->orWhereHas('rentalTag',function($query) use($per_id){
                                 $query->where('tag','LIKE','%'.request('search')."%");
                                 $query->where('performer_id',$per_id);
                                });
     
               
     $rental_search          = $rental_search->get();

     if(count($rental_search)>0){
        
        $search_result       = $rental_search;    

        $helpdesk            = HelpDesk::find(1);

        $html = view('performers.searchResult',compact('search_result','helpdesk'))->render();       

        }else{        
        $html                 = "<div><h3>No Result Found </h3></div>";
        }

        return response()->json(['status'=>true,'html'=>$html]);
    }

    public function rentalAddTag(Request $request)
    {      

            $data               =  $request->all();
            $tag                = new RentalGalleryTag($data);
            $tag->save();
        
        if($tag)
        {
            return response()->json(['status'=>true,'message'=>'Rental Tag Insert Successfully','data'=>$tag]);          
        }
    }

    public function removeRentalTag(Request $request)
    {
       $tag               = RentalGalleryTag::where('id',$request->id)->first();
       $tag->delete();

       return response()->json(['status'=>true,'message'=>'rental Tag Remove Successfully','data'=> ""]);
   }


    public function rentalGallerySlideImages(Request $request){
        $id                                           =    $request->input('id');    
        $gallery                                      =    MediaRental::where('rental_id',$id)
        ->get();

        $slide_img=[];

        foreach ($gallery as $key => $value) {
            $slide_img[$key]    =    $value['image_url'];
        }
        $slide =  $slide_img;
        return $slide;
    }


    public function rentalGalleryUpdate(Request $request){
       $RentalTitleCount = PerformerRental::where('rental_title',$request->get('rental_title'))->where('performer_id',Auth::id())->count();

           if($RentalTitleCount >= 2){

       $validator = Validator::make($request->all(),[  
            'rental_title'                   =>  'required|unique:performer_rentals,rental_title,'.$request->gallery_id,
        ],[
            'rental_title.unique' => 'Rental Title Already Taken',
            'rental_title.required' => 'Enter Rental Title',
        ]);
        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }
    }
        $rentalId = $request->get('gallery_id');

        PerformerRental::where('id',$rentalId)
        ->update(['rental_title'=>$request->get('rental_title')]);

        $performer_user  = Auth::user();

        $type = $request->get('type');
        $tags = null;
        if ($request->get('tags') && $request->get('tags')!="") {
            $tags = implode(',', $request->get('tags'));
        }
        if(!empty($request->get('images'))){
            $mediaStore =[];
            foreach ($request->get('images') as $key   => $value) {
                $mediaStore[$key]['media_url']       =  $value;   
                $mediaStore[$key]['media_type']      =  $type[$key];
                $mediaStore[$key]['rental_id']       =  $rentalId;
                $mediaStore[$key]['media_title']     =  $request->get('media_title');
                $mediaStore[$key]['performers_id']   =  $performer_user->id;
                $mediaStore[$key]['tag']             =  $tags;
            }
            MediaRental::insert($mediaStore);
        }
        $tags=[];
        if(!empty($request['tags']))
        {
            foreach ($request['tags'] as $key => $value) {
                $tags[$key]['tag'] = $request['tags'][$key];
                $tags[$key]['rental_id'] =$rentalId;

            }
        }
        RentalGalleryTag::insert($tags);
        return response()->json([
            'status'=>true,
            'message'=>'Rental gallery update',
        ]);
    }

    public function getTags(Request $request){
        $id = $request->get('id');
        $rentalGalleryTag = RentalGalleryTag::where('rental_id',$id)->get();
        
        $tag = $request->get('tags');
        $tags = Tags::where('tag', 'like', $tag.'%')
                    ->whereNotIn('tag', $rentalGalleryTag->pluck('tag')->toArray())
                    ->get();
        return response()->json(['status'=>true,'html'=>$tags,'rental_id'=>$id]);
    }

    public function viewGallery($id,$pid='')
    {
     if($pid==''){
        $performers_user                             =    Auth::user();
    }else{
      $performers_user                         =    Performer::where('id',$pid)->first();
  }
         $gallery               =    PerformerRental::with('rentalMedia')
                                    ->where('id',$id)
                                    ->where('performer_id',$performers_user->id)
                                    ->first();


        return view('performers.rentalgallery',compact('gallery','performers_user'));
    }

    public function rentalGalleryRemove(Request $request)
    {
       
       $deleteGallery = PerformerRental::findorfail($request->id);
       MediaRental::where('rental_id',$request->id)->Delete();  
       RentalGalleryTag::where('rental_id',$request->id)->Delete();  
       $deleteGallery->Delete();
        
        return response()->json(['status'=>true,'message'=>"Remove gallery Successfully"]);
    }
}
