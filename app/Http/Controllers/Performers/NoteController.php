<?php

namespace App\Http\Controllers\Performers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\v1\Performer;
use App\Models\v1\Note;
use App\Models\v1\PerformerPortfolio;
use App\Models\v1\MediaPortfolio;
use App\Models\v1\PortfolioGalleryTag;
use App\Models\v1\PerformerGroups;
use App\Models\v1\Performer\PerformersFlags;
use App\Models\v1\Performer\Flags;
use Carbon\Carbon;
use Validator;
use DB;

class NoteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id=''){

      if(Auth::guard('web')->check()){
          $performers_user    = Auth::user();
          $per_id = $performers_user->id;  
          $authtype = 'performer';
      }else{
          $per_id = $id;
          $performers_user    = Performer::where('id',$per_id)->first();
          $authtype = 'internal-staff';
      }

        $performer         = Performer::where('id',$per_id)->first();
        $age               = Carbon::parse($performer->date_of_birth)->age;
        $age_range         = $this->age_range($age);
        $portfolio_list    = PerformerPortfolio::with('performerMedia')->where('performer_id',$per_id)->get();
        $groupUser         = PerformerGroups::with("groupPerformers")->where('id',$performer->group_id)                    ->first();
        $sidebar_image     =  MediaPortfolio::where('performers_id',$per_id)
                                ->where('media_type','like','%image%')
                                ->inRandomOrder()
                                ->take(4)
                                ->get();
        $performerFlag = PerformersFlags::with('flag')->where('performer_id',$per_id)->get();                          
        $flag       = Flags::all();
        $note   = Note::where('performer_id', '=', $per_id)->get();

        return view('performers.note',compact('performer','age_range','portfolio_list','sidebar_image','note','performers_user','groupUser','per_id','authtype','performerFlag','flag'));  
    }
    // Age Range Count Function
    protected  function age_range($age)
    {
        switch ($age) {

            case ($age < 19):
            return "Minor";
            break;
            case ($age >= 20 && $age <= 30):
            return "20 - 30 years";
            break;
            case ($age >= 31 && $age <= 40):
            return "30 - 40 years";
            break;
            case ($age >= 41 && $age <= 50):
            return "40 - 50 years";
            break;    
            case ($age >= 51 && $age <= 60):
            return "50 - 60 years";
            break;    
            case ($age >= 61 && $age <= 70):
            return "60 - 70 years";
            break;    
            case ($age >= 71 && $age <= 80):
            return "70 - 80 years";
            break;    
            case ($age >= 81 && $age <= 90):
            return "80 - 90 years";
            break;
            case ($age >= 91 && $age <= 100):
            return "90 - 100 years";
            break;    
            default:
            return "100 +";
        }
    }
    
}
