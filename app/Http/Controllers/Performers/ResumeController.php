<?php

namespace App\Http\Controllers\Performers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\v1\Performer;
use App\Models\v1\HelpDesk;
use App\Models\v1\Workperformer;
use App\Models\v1\ProjectRole;
use App\Models\v1\ProjectPerformer;
use App\Models\v1\PerformerPortfolio;
use App\Models\v1\Performer\PerformersFlags;
use App\Models\v1\Performer\Flags;
use App\Models\v1\MediaPortfolio;
use App\Models\v1\PortfolioGalleryTag;
use App\Models\v1\PerformerGroups;

use Carbon\Carbon;
use Validator;

class ResumeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id=""){

       if(Auth::guard('web')->check()){
          $performers_user    = Auth::user();
          $per_id = $performers_user->id;  
          $authtype = 'performer';
      }else{
          $per_id = $id;
           $performers_user    = Performer::where('id',$per_id)->first();
          $authtype = 'internal-staff';
      }
        $performer           = Performer::where('id',$per_id)->first();
        $age                 = Carbon::parse($performer->date_of_birth)->age;
        $age_range           = $this->age_range($age);
        $portfolio_list      = PerformerPortfolio::with('performerMedia')->where('performer_id',$per_id)->get();
        
        $groupUser           = PerformerGroups::with("groupPerformers")->where('id',$performer->group_id)                    ->first();

        $sidebar_image       =  MediaPortfolio::where('performers_id',$per_id)
                                 ->where('media_type','like','%image%')
                                 ->inRandomOrder()
                                ->take(4)
                                ->get();

   $Workperformer    = Workperformer::with('project_performer.project_role')->where('performer_id',$per_id)->get();

    $performerFlag = PerformersFlags::with('flag')->where('performer_id',$per_id)->get();   
        $flag       = Flags::all();
        $helpdesk   = HelpDesk::find(1); 
        return view('performers.resume',compact('performer','age_range','portfolio_list','sidebar_image','performers_user','Workperformer','groupUser','per_id','authtype','performerFlag','flag','helpdesk'));  



    }
    // Age Range Count Function
    protected  function age_range($age)
    {
        switch ($age) {

            case ($age < 19):
            return "Minor";
            break;
            case ($age >= 20 && $age <= 30):
            return "20 - 30 years";
            break;
            case ($age >= 31 && $age <= 40):
            return "30 - 40 years";
            break;
            case ($age >= 41 && $age <= 50):
            return "40 - 50 years";
            break;    
            case ($age >= 51 && $age <= 60):
            return "50 - 60 years";
            break;    
            case ($age >= 61 && $age <= 70):
            return "60 - 70 years";
            break;    
            case ($age >= 71 && $age <= 80):
            return "70 - 80 years";
            break;    
            case ($age >= 81 && $age <= 90):
            return "80 - 90 years";
            break;
            case ($age >= 91 && $age <= 100):
            return "90 - 100 years";
            break;    
            default:
            return "100 +";
        }
    }
    public function addworktitle(Request $request){
        // $Workperformer=new Workperformer;
        $performer = Auth::User();
        $workId = $request->get('work_id');
        
        if($workId == 0){
            $workperformer = Workperformer::create([
                                                    'resume_title'=> $request->data,
                                                    'performer_id' => $performer->id
                                                ]);
        } else {
            $workperformer = Workperformer::updateOrCreate([
                                        'id'=> $workId,
                                        'performer_id' => $performer->id
                                    ],[
                                        'resume_title'=> $request->data,
                                        'performer_id' => $performer->id
                                    ]);
        }
        // $workperformer = Workperformer::updateOrCreate([
        //                                 'resume_title'=> $request->data,
        //                                 'performer_id' => $performer->id
        //                             ],[
        //                                 'resume_title'=> $request->data,
        //                                 'performer_id' => $performer->id
        //                             ]);
        return $workperformer->id;
    }
    public function addprojecttitle(Request $request){
        $projectId = $request->get('project_id');
        $resumeId = $request->get('rid');

        if ($projectId != "") {
            $projectPerformer = ProjectPerformer::updateOrCreate([
                                                        'resume_id'=>$request->rid,
                                                        'id'=>$projectId
                                                    ],[
                                                        'resume_id'=>$request->rid,
                                                        'project_title'=>$request->data
                                                    ]);
        } else {
            $projectPerformer = ProjectPerformer::create([
                                                        'resume_id'=>$request->rid,
                                                        'project_title'=>$request->data
                                                    ]);
        }
        /*$ProjectPerformer=new ProjectPerformer;
         $ProjectPerformer->project_title=$request->data;
        $ProjectPerformer->resume_id=$request->rid;
        $ProjectPerformer->save();*/
        return $projectPerformer->id;
        
    }
    public function addimdc(Request $request){
        print_r($request);
        $ProjectPerformer   = ProjectPerformer::find($request->get('project_id'));
        $ProjectPerformer->IMDC_link= $request->input('data');
        $ProjectPerformer->save();
    }
    public function addrole(Request $request){
        $messages = [
            'project_id.required' => 'Please enter Project title first',
        ];
        $validator = Validator::make($request->all(),[
                        'project_id' => "required",
                        'role_title' => "required",
                        'start_date' => "required",
                        'end_date' => "required",
                    ],$messages);
        if ($validator->fails()) {
            return response()->json(['status'=> false,'message'=>$validator->messages()->first()]);
        }

        $data = $request->all();
        ProjectRole::create([
                                    'role_title'=>$data['role_title'],
                                    'start_date'=>date('Y-m-d h:i:s',strtotime($data['start_date'])),
                                    'end_date'=> date('Y-m-d h:i:s',strtotime($data['end_date'])),
                                    'project_id'=>$data['project_id'],
                                ]);

        // $ProjectRole             = new ProjectRole;
        // $ProjectRole->role_title = $request->roletitle;
        // $ProjectRole->start_date = date('Y-m-d h:i:s',strtotime($request->startdate));
        // $ProjectRole->end_date   = date('Y-m-d h:i:s',strtotime($request->enddate));
        // $ProjectRole->project_id = $request->pid;
        // $ProjectRole->save();
        return response()->json(['status'=>true,'message'=>'Role Added Successfully']);

    }
    public function updateRole(Request $request){
        $messages = [
            'project_id.required' => 'Please enter Project title first',
        ];
        $validator = Validator::make($request->all(),[
                        'project_id' => "required",
                        'role_title' => "required",
                        'start_date' => "required",
                        'end_date' => "required",
                    ],$messages);
        if ($validator->fails()) {
            return response()->json(['status'=> false,'message'=>$validator->messages()->first()]);
        }

        $data = $request->all();

        if (isset($data['roleId']) && $data['roleId'] != 0 && $data['roleId'] != "") {
            $ProjectRole = ProjectRole::updateOrCreate([
                                        'project_id'=>$data['project_id'],
                                        'id'=> $data['roleId'],
                                    ],[
                                        'role_title'=>$data['role_title'],
                                        'start_date'=>date('Y-m-d h:i:s',strtotime($data['start_date'])),
                                        'end_date'=> date('Y-m-d h:i:s',strtotime($data['end_date'])),
                                        'project_id'=>$data['project_id'],
                                    ]);
        } else {
            $ProjectRole =  ProjectRole::create([
                                    'role_title'=>$data['role_title'],
                                    'start_date'=>date('Y-m-d h:i:s',strtotime($data['start_date'])),
                                    'end_date'=> date('Y-m-d h:i:s',strtotime($data['end_date'])),
                                    'project_id'=>$data['project_id'],
                                ]);
        }

        return response()->json(['status'=>true,'message'=>'Role Updated Successfully','id'=>$ProjectRole->id]);
    }
    public function projectdata(Request $request){
        $ProjectRole       = ProjectRole::where('project_id',$request->data)->get();
        $ProjectPerformer  = ProjectPerformer::where('id',$request->data)->first();
        $Workperformer     = Workperformer::where('id',$ProjectPerformer->resume_id)->first();    
        $html = view('performers.resumedetail',compact('ProjectRole','ProjectPerformer','Workperformer'))->render();
        return response()->json(['status'=>true,'message'=>'','html'=>$html]);
    }


    /** delete project **/
    public function deleteProject(Request $request){
        $projectId = $request->get('project_id');
        ProjectPerformer::destroy($projectId);
        ProjectRole::where('project_id',$projectId)->delete();
        return response()->json(['status'=>true,'message'=>'Project Title Deleted Successfully']);
    }

    public function editResume(Request $request){
        $workId = $request->get('work_id');
        $workperformer = Workperformer::where('id',$workId)
                                        ->with('project_performer.project_role')
                                        ->first();
           

        $html = view('performers.editResume',compact('workperformer'))->render();


        return response()->json([
                                'status'=>true,
                                'data'=>$html,
                                'message'=>'data successfully get'
                            ]);

       
    }

    /** role delete **/
    public function roleDelete(Request $request){
        $roleId = $request->get('role_id');
        ProjectRole::destroy('id',$roleId);

        return response()->json([
                                'status'=>true,
                                'message'=>'Role Deleted Successfully'
                            ]);
    }
    public function deleteResume(Request $request){
        
        $resumeDelete = Workperformer::findorfail($request->id);

        $project_id = ProjectPerformer::where('resume_id',$request->id)->pluck('id')->toArray();

        $projectDelete = ProjectPerformer::where('resume_id',$request->id)->delete();

         $roleDelete = ProjectRole::whereIn('project_id',$project_id)->delete();

        $resumeDelete->delete();        
        return $request->id;
    }

    public function removeProject(Request $request)
    {
        

        $del= ProjectPerformer::where('id',$request->id)->first();
        $del->project_role()->delete();
        $del->delete();

        return response()->json([
                                'status'=>true,
                                'message'=>'Project  Deleted Successfully'
                            ]);
    }
    public function removeProjectRole(Request $request)
    {
          
            
        $del= ProjectRole::where('id',$request->id)->first();        
        $del->delete();

        return response()->json([
                                'status'=>true,
                                'message'=>'Project Role  Deleted Successfully'
                            ]);
    }

    // Resume Search
    public function resumeSearch(Request $request)
    {

         if(!empty($request->get_id)){
        $per_id = $request->get_id;
        
     }else{
        $per_id = Auth::id();
     }
     $helpdesk = HelpDesk::find(1);
     $Workperformer = Workperformer::with('project_performer.project_role')->whereNotNull('resume_title');
     $Workperformer->where('performer_id',$per_id);

     $Workperformer      ->whereHas('project_performer.project_role',function($query){
                           $query->where('project_title','LIKE','%'.request('search')."%");
                           $query->orWhere('role_title','LIKE','%'.request('search')."%");
                          });

     $Workperformer       = $Workperformer->get();
      
     
     
     if(count($Workperformer)>0){
        
        $search_result = $Workperformer;    

        $html = view('performers.resumeSearchResult',compact('search_result','helpdesk'))->render();       

        }else{        
        $html ="<div><h3>No Result Found </h3></div>";
        }

        return response()->json(['status'=>true,'html'=>$html]);

    }


}
