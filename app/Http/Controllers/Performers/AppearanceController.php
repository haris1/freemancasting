<?php

namespace App\Http\Controllers\Performers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\v1\Performer;
use App\Models\v1\PerformerGroups;
use App\Models\v1\MediaPortfolio;
use App\Models\v1\Performer\Eyes;
use App\Models\v1\Performer\HairColor;
use App\Models\v1\Performer\HairLength;
use App\Models\v1\Performer\Skintone;
use App\Models\v1\Performer\Handedness;
use App\Models\v1\Performer\Ethnicity;
use App\Models\v1\Performer\Nationality;
use App\Models\v1\Performer\HairFacial;
use App\Models\v1\Performer\HairStyle;
use App\Models\v1\Performer\BodyTrait;
use App\Models\v1\Performer\Appearance;
use App\Models\v1\Performer\Region;
use App\Models\v1\Performer\Restriction;
use App\Models\v1\Performer\Nudity;
use App\Models\v1\Performer\RestrictionPerformer;
use App\Models\v1\Performer\Nudity_Performer;
use App\Models\v1\Performer\PerformersFlags;
use App\Models\v1\Performer\Piercing;
use App\Models\v1\Performer\Flags;
use Carbon\Carbon;
use Validator;
class AppearanceController extends Controller
{
    public function index($id="")
    {
      
      
     if(Auth::guard('web')->check()){
      $performers_user    = Auth::user();
      $per_id = $performers_user->id;  
      $authtype = 'performer';
    }else{
      $per_id = $id;
      
          $performers_user    = Performer::where('id',$per_id)->first();
      $authtype = 'internal-staff';
    }
    	$performer        = Performer::where('id',$per_id)->first();
    	$age              = Carbon::parse($performer->date_of_birth)->age;
    	$age_range        = $this->age_range($age);
      $groupUser        = PerformerGroups::with("groupPerformers")->where('id',$performer->group_id)                    ->first();
      $sidebar_image    = MediaPortfolio::where('performers_id',$per_id)
                                ->where('media_type','like','%image%')
                                ->inRandomOrder()
                                ->take(4)
                                ->get();

        $Eyes                 = Eyes::all();
        $HairColor            = HairColor::all();
        $Skintone             = Skintone::all();
        $Handedness           = Handedness::all();
        $Ethnicity            = Ethnicity::all();
        $Nationality          = Nationality::all();
        $HairLength           = HairLength::all();
        $region               = Region::all();
        $restriction          = Restriction::all();
        $nudity               =  Nudity::all();
        $flag                 = Flags::all();
        $piercing             = Piercing::all();
        $hairfacial           = HairFacial::all();
        $bodytrait            = BodyTrait::all();
        $hairstyle            = HairStyle::all();
        $appearance           = Appearance::where('performer_id',$per_id)->first();
        $restrictionperformer = RestrictionPerformer::where(
        'performer_id', $per_id)->get();
        $restrictionperformersmoking = RestrictionPerformer::where(
        'performer_id', $per_id)
        ->whereHas('restrictionName',function($query){
        $query->where('restrictions_type','Smoking');
        })->get();

        $restrictionperformergeneral = RestrictionPerformer::where(
        'performer_id', $per_id)
        ->whereHas('restrictionName',function($query){
        $query->where('restrictions_type','General');
        })->get();
        
        $restrictionperformerInteractions = RestrictionPerformer::where(
        'performer_id', $per_id)
        ->whereHas('restrictionName',function($query){
        $query->where('restrictions_type','Interactions');
        })->get();
        
        $nudityperformer = Nudity_Performer::where(
        'performer_id',$per_id)->get();

       $performerFlag = PerformersFlags::with('flag')->where('performer_id',$per_id)->get();
    	return view('performers.appearance',compact('age_range','performer','Eyes','HairColor','HairLength','Skintone','Handedness','Ethnicity','Nationality','appearance','sidebar_image','performers_user','region','restriction','nudity','restrictionperformer','nudityperformer','groupUser','restrictionperformersmoking','restrictionperformergeneral','restrictionperformerInteractions','per_id','authtype','performerFlag','flag','piercing','hairfacial','bodytrait','hairstyle'));  
    }
    public function store(Request $request){
        



            $try=$request->all();
           $data['ethnicity_id']  = $request->ethnicity_id;
           $data['skin_tone_id']  = $request->skin_tone_id;
           $data['hair_color_id'] = $request->hair_color_id;
           $data['body_trait_id'] = $request->body_trait_id;
           $data['hair_style_id'] = $request->hair_style_id;
           $data['facial_hair_id']= $request->facial_hair_id;
           $data['eyes_id']       = $request->eyes_id;
           $data['handedness_id'] = $request->handedness_id;
           $data['performer_id']  = Auth::user()->id;
           $data['nationality_id']= $request->nationality_id;
           $data['height']        = $request->height;
           $data['weight']        = $request->weight;
           $data['bust']          = $request->bust;
           $data['waist']         = $request->waist;
           $data['hips']          = $request->hips;
           $data['dress']         = $request->dress;
           $data['jeans']         = $request->jeans;
           $data['shoes']         = $request->shoes;
           $data['contacts']      = $request->contacts;
           $data['hair_length_id']= $request->hair_length_id;
           $data['gloves']        = $request->gloves;
           $data['glasses']       = $request->glasses;
           $data['gender']        = $request->gender;
           $data['tattoos']       = $request->tattoos;     
           $data['collar']        = $request->collar;     
           $data['sleeve']        = $request->sleeve;     
           $data['pant']          = $request->pant;     
           $data['ring']          = $request->ring;     
           $data['hat']           = $request->hat;     
           $data['jacket']        = $request->jacket;     
           $data['infant_size']   = $request->infant_size;
           $data['child_size']    = $request->child_size;
           $data['inseam']        = $request->inseam;
           $data['chest']         = $request->chest;
           $data['double_for']    = $request->double_for;

           if(isset($request->piercing)){
            $data['piercing']   = implode(',',$request->piercing); 
           }
           
          
        $Appearance = Appearance::updateOrCreate(
            ['performer_id' => Auth::user()->id],$data           
        );
       
    }
    protected  function age_range($age)
    {
    	switch ($age) {
         case ($age < 19):
        return "Minor";
        break;
    		case ($age >= 20 && $age <= 30):
    		return "20 - 30 years";
    		break;
    		case ($age >= 31 && $age <= 40):
    		return "30 - 40 years";
    		break;
    		case ($age >= 41 && $age <= 50):
    		return "40 - 50 years";
    		break;    
    		case ($age >= 51 && $age <= 60):
    		return "50 - 60 years";
    		break;    
    		case ($age >= 61 && $age <= 70):
    		return "60 - 70 years";
    		break;    
    		case ($age >= 71 && $age <= 80):
    		return "70 - 80 years";
    		break;    
    		case ($age >= 81 && $age <= 90):
    		return "80 - 90 years";
    		break;
    		case ($age >= 91 && $age <= 100):
    		return "90 - 100 years";
    		break;    
    		default:
    		return "100 +";
    	}
    }


}
