<?php

namespace App\Http\Controllers\Performers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\v1\Performer;
use App\Models\v1\PerformerSkill;
use App\Models\v1\SkillSection;
use App\Models\v1\PerformerPortfolio;
use App\Models\v1\Performer\PerformerUnion;
use App\Models\v1\PerformerAgent;
use App\Models\v1\MediaPortfolio;
use App\Models\v1\Performer\Flags;
use App\Models\v1\Performer\PerformersFlags;
use App\Models\v1\Performer\Appearance;
use App\Models\v1\Tags;
use App\Models\v1\HelpDesk;
use App\Models\v1\Performer\Region;
use App\Models\v1\Performer\GroupMember;
use App\Models\v1\PortfolioGalleryTag;
use App\Models\v1\PerformerGroups;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Validator;
use Hash;
use DB;

class PortfolioController extends Controller
{
    //Redirect Portfolio Page With Performer login User Detail
    public function index($id="")
    {   
        
       if(Auth::guard('web')->check()){
          $performers_user    = Auth::user();
          $per_id = $performers_user->id;  
          $authtype = 'performer';
       }else{
          $per_id = $id;
          $performers_user    = Performer::where('id',$per_id)->first();
          $authtype = 'internal-staff';
       }

       //return PortfolioGalleryTag::where('id',$per_id)->get();        

         $performer          = Performer::with('performerFlag','GroupMembers')->where('id',$per_id)->first();        
    	$age                = Carbon::parse($performer->date_of_birth)->age;
    	$age_range          = $this->age_range($age);
        $performerId        = $performer->id;

       $portfolio_list     = PerformerPortfolio::with(['performerMedia'=>function($query) use ($performerId){
                                                        $query->where('performers_id',$performerId);
                                                    }])
                                                    ->where('performer_id',$per_id)
                                                    ->get();


       
       $groupUser           = PerformerGroups::with("groupPerformers")->where('id',$performer->group_id)                    ->first();
     
       $sidebar_image      =  MediaPortfolio::where('performers_id',$per_id)
                                ->where('media_type','like','%image%')
                                ->inRandomOrder()
                                ->take(4)
                                ->get();

     $performerFlag = PerformersFlags::with('flag')->where('performer_id',$per_id)->get();
        $flag       = Flags::all();
        $slides =  $sidebar_image->pluck('image_url');


        $tags = Tags::all()->pluck('tag')->toArray();
        $region = Region::all();  
        $helpdesk = HelpDesk::find(1);
      	return view('performers.portfolio',compact('performer','age_range','portfolio_list','sidebar_image','performers_user','tags','groupUser','per_id','authtype','slides','performerFlag','flag','region','helpdesk'));  
    }
    public function getSliderImage(Request $request){
      $per_id = $request->id;
      $performer_profile = Performer::where('id',$per_id)->get()->pluck('image_url');
      //print_r($performer_profile);
      $sidebar_image =array();
     
      $sidebar_image = MediaPortfolio::select('*')->where(function($query) use ($request){
         $query->where('media_type','like','%image%')
         ->orwhere('media_type','like','%jpg%');
     })->where('performers_id',$per_id)->get()->pluck('image_url');
      //print_r($sidebar_image);
      $slides = $performer_profile->merge($sidebar_image);
      
      return response()->json(['status'=>'true','msg'=>$slides,'message'=>'']);
  
    }
    public function agentlist(Request $request){
    $term = Input::get('term');
    $results = array();
    $queries = DB::table('performer_agents')
        ->where('agent_name', 'LIKE', '%'.$term.'%')->get();

    foreach ($queries as $query)
    {
        $results[] = [ 'id' => $query->id, 'value' => $query->agent_name];
    }
    return response()->json($results);
    }
    public function addinagentlist(Request $request){
        $PerformerAgent = new PerformerAgent;
        $PerformerAgent->agent_name = $request->data;
        $PerformerAgent->save();
                 return $PerformerAgent->id;
    }
    
    // Age Range Count Function
    protected  function age_range($age)
    {
    	switch ($age) {

         
           	case ($age < 19):
    		return "Minor";
    		break;
    		case ($age >= 20 && $age <= 30):
    		return "20 - 30 years";
    		break;
    		case ($age >= 31 && $age <= 40):
    		return "30 - 40 years";
    		break;
    		case ($age >= 41 && $age <= 50):
    		return "40 - 50 years";
    		break;    
    		case ($age >= 51 && $age <= 60):
    		return "50 - 60 years";
    		break;    
    		case ($age >= 61 && $age <= 70):
    		return "60 - 70 years";
    		break;    
    		case ($age >= 71 && $age <= 80):
    		return "70 - 80 years";
    		break;    
    		case ($age >= 81 && $age <= 90):
    		return "80 - 90 years";
    		break;
    		case ($age >= 91 && $age <= 100):
    		return "90 - 100 years";
    		break;    
    		default:
    		return "100 +";
    	}

 
    }

    // Store Images For Performer Portfolio
    public function storePortfolioImages(Request $request)
    {

        

        $performers_user            = Auth::user();
        $photo                      = $request['file'];
        if(!empty($photo))
        {
            $file_name              = time().$photo->getClientOriginalName();
            $ext              = $photo->getClientOriginalExtension();
            $root                   = storage_path().'/app/public/performers/'.$performers_user->id;
            $photo->move($root,$file_name);

            $imageFormate = array("jpg","gif","png","jpeg","bmp");
            if(in_array(strtolower($ext),$imageFormate))
            {
                $performer['type'] ="image";
            }else{
                $performer['type'] ="video";
            }


            $performer['name']      = $file_name;
            // $performer['type']      = $request['file']->getClientMimeType();
           // $performer['mediaUrl']  = 'public\performers\profile\\'.$file_name;
        }
        else
        {
            $performer['name']       =  "";
            // $performer['type']       =  "";
            //$performer['mediaUrl']   ="";
        }
        
        return response()->json(['data'=>$performer]);
    }



     // Store Video For Performer portfolio    
    public function storePortfolioVideo(Request $request)
    {   
        $performers_user            = Auth::user();
        $video                      = $request['file'];
        if(!empty($video))
        {
            $file_name              = time(). $video->getClientOriginalName();
            $root                   = storage_path().'/app/public/performers/'.$performers_user->id;
            $video->move($root,$file_name);
            $performer['name']      = $file_name;
            $performer['type']      = "video";
            
        }
        else
        {
            $performer['name']       =  "";
            $performer['type']       =  "";
            
        }
        
        return response()->json(['data'=>$performer]);

    }



    //store media for performers usaer 
    public function storePortfolioMedia(Request $request)
    {   

        $portfolioTitleCount = PerformerPortfolio::where('portfolio_title',$request->get('portfolio_title'))->where('performer_id',Auth::id())->count();

          if($portfolioTitleCount != 0){
       $validator = Validator::make($request->all(),[  
        'portfolio_title'                   =>  'required|unique:performer_portfolios,portfolio_title',
    ],[
        'portfolio_title.required'=> 'Enter Portfolio Title',
        'portfolio_title.unique' =>'Performer Title Already Taken'
    ]);

     if($validator->fails()){
        return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
    }
        }
    $performer                              =    $request->all();
    $performer_user                         =    Auth::user();

    $performerPortfolio['portfolio_title']  = $request->get('portfolio_title');
    $performerPortfolio['description']      = $request->get('description');
    $performerPortfolio['performer_id']     = $performer_user->id;
    if($request->is_video                   ==  "VideoUpload")
    {
        $performerPortfolio['is_video']     =    "yes";
    }else
    {
        $performerPortfolio['is_video']     =    "no";
    }

    $portfolio                              = new PerformerPortfolio($performerPortfolio);
    $portfolio->save();


    $tags = null;
    if ($request->get('tags') && $request->get('tags')!="") {
        $tags = implode(',', $request->get('tags'));
    }
    
    if(!empty($performer['images'])){            
       $mediaStore =[];
       foreach ($performer['images'] as $key    => $value) {
        $mediaStore[$key]['media_url']       =  $value;   
        $mediaStore[$key]['media_type']      =  $performer['type'][$key];
        $mediaStore[$key]['portfolio_id']    =  $portfolio->id;
        $mediaStore[$key]['media_title']     =  $performer['media_title'];
        $mediaStore[$key]['performers_id']   =  $performer_user->id;
        $mediaStore[$key]['tag']             =  $tags;
    }
    MediaPortfolio::insert($mediaStore);
}
        $tags=[];
        if(!empty($request['tags']))
        {
            foreach ($request['tags'] as $key => $value) {
                $tags[$key]['tag'] = $request['tags'][$key];
                $tags[$key]['portfolio_id'] =$portfolio->id;

            }
        }
        PortfolioGalleryTag::insert($tags);

return response()->json(['status'=>true,'message'=>'Portfolio Inserted Successfully']);
}

public function viewGallery($id,$pid='')
{

    if($pid==''){
    $performers_user                           =    Auth::user();
    }else{
      $performers_user                         =    Performer::where('id',$pid)->first();
    }
    $gallery                                   =    PerformerPortfolio::with('performerMedia')
    ->where('id',$id)
    ->where('performer_id',$performers_user->id)
    ->first();



     $tagList          = Tags::all();
    
    return view('performers.portfoliogallery',compact('gallery','tagList','performers_user'));
}

public function gallerySlideImages(Request $request)
{


   $id                                           =    $request->input('id');    
   $gallery                                      =    MediaPortfolio::where('portfolio_id',$id)
    ->get();


     $slide_img=[];
    foreach ($gallery as $key => $value) {
        $slide_img[$key]    =    $value['image_url'];
    }
    $slide =  $slide_img;

    $tag = [];
    $output ="";
    foreach ($gallery as $key => $value) {
       
         $tag[$key]    =    $value['tag'];
        if(!empty($value['tag'])){
            $output .='<div> <span  class="btnremadd" onclick="ImageTagRemove('.$value['id'].');">'.$value['tag'].'</span></div>';}else{
                $output = null;
            }
}
   

    return response()->json(['status'=>true,'slide'=>$slide,'output'=>$output]);
    
}


public function portfolioGalleryUpdate(Request $request){
      $portfolioTitleCount = PerformerPortfolio::where('portfolio_title',$request->get('portfolio_title'))->where('performer_id',Auth::id())->count();

          if($portfolioTitleCount >= 2){
       $validator = Validator::make($request->all(),[  
        'portfolio_title'                   =>  'required|unique:performer_portfolios,portfolio_title,'.$request->gallery_id,
    ],[
        'portfolio_title.required'=> 'Enter Portfolio Title',
        'portfolio_title.unique' =>'Performer Title Already Taken'
    ]);

     if($validator->fails()){
        return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
    }
        }
    $portfolioId = $request->get('gallery_id');
    
    PerformerPortfolio::where('id',$portfolioId)
    ->update(['portfolio_title'=>$request->get('portfolio_title')]);

    $performer_user  = Auth::user();

    $type = $request->get('type');
    $tags = null;
    if ($request->get('tags') && $request->get('tags')!="") {
        $tags = implode(',', $request->get('tags'));
    }

    if(!empty($request->get('images'))){
        $mediaStore =[];
        foreach ($request->get('images') as $key   => $value) {
            $mediaStore[$key]['media_url']       =  $value;   
            $mediaStore[$key]['media_type']      =  $type[$key];
            $mediaStore[$key]['portfolio_id']    =  $portfolioId;
            $mediaStore[$key]['media_title']     =  $request->get('media_title');
            $mediaStore[$key]['performers_id']   =  $performer_user->id;
            $mediaStore[$key]['tag']             =  $tags;
        }
        MediaPortfolio::insert($mediaStore);
    }
    $tags=[];
        if(!empty($request['tags']))
        {
            foreach ($request['tags'] as $key => $value) {
                $tags[$key]['tag'] = $request['tags'][$key];
                $tags[$key]['portfolio_id'] =$portfolioId;

            }
        }
      //  PortfolioGalleryTag::insert($tags);

    return response()->json([
        'status'=>true,
        'message'=>'Portfolio Gallery Updated Successfully',
    ]);
}
    //display video
public function portfoliovideoShow(Request $request)
{
    $video =MediaPortfolio::where('id',$request->get('id'))->first();

    return response()->json(['status'=>true,'message'=>'','data'=>$video]);

}
    //Add Images 
public function addPortfolioImages(Request $request)
{
    $tags = null;
    if ($request->get('tags') && $request->get('tags')!="") {
        $tags = implode(',', $request->get('tags'));
    }
   
   
    $performers_user       =    Auth::user();

    if(!empty($request['images'])){
        $mediaStore =[];
        foreach ($request['images'] as $key     => $value) {
            $mediaStore[$key]['performers_id']   =  $performers_user->id;
            $mediaStore[$key]['portfolio_id']    =  $request['portfolio_id'];  
            $mediaStore[$key]['media_url']       =  $value;                           
            $mediaStore[$key]['media_type']      =  $request['type'][$key];
            $mediaStore[$key]['media_title']     =  $request['media_title'];
            $mediaStore[$key]['tag']             =  $tags;
        }

        MediaPortfolio::insert($mediaStore);

    }
     $tags=[];
        if(!empty($request['tags']))
        {
            foreach ($request['tags'] as $key => $value) {
                $tags[$key]['tag'] = $request['tags'][$key];
                $tags[$key]['portfolio_id'] =$request['portfolio_id'];

            }
        }
   //PortfolioGalleryTag::insert($tags);

    return response()->json(['status'=>true,'message'=>' Image Added Successfully']);

}

    //Add Video
public function addPortfolioVideo(Request $request)
{

    $performers_user       =    Auth::user();

    if(!empty($request['videos'])){
            $mediaStore                          =[];
        foreach ($request['videos'] as $key      => $value) {
            $mediaStore[$key]['performers_id']   =  $performers_user->id;
            $mediaStore[$key]['portfolio_id']    =  $request['portfolio_id'];  
            $mediaStore[$key]['media_url']       =  $value;                           
            $mediaStore[$key]['media_type']      =  $request['type'][$key];
            $mediaStore[$key]['media_title']     =  $request['media_title'];
        }
        MediaPortfolio::insert($mediaStore);
    }

    return response()->json(['status'=>true,'message'=>' Audio Added Successfully']);

}


public function portfolioAddTag(Request $request)
{

    $data   =$request->all();
    $tag = new PortfolioGalleryTag($data);
    $tag->save();
    if($tag)
    {
     return response()->json(['status'=>true,'message'=>'Portfolio Tag Insert Successfully','data'=>$tag]);         
 }
}

public function removePortfolioTag(Request $request)
{
         $tag               = PortfolioGalleryTag::where('id',$request->id)->first();
         $tag->delete();
         
         return response()->json(['status'=>true,'message'=>'Portfolio Tag Removed Successfully','data'=> ""]);
}

    //remove image or video

    public function removePortfolioMedia(Request $request)
    {
         $mediaRemove = MediaPortfolio::where('id',$request->id)->first();
         $mediaRemove->delete();
         return response()->json(['status'=>true,'message'=>'Portfolio Media Removed Successfully','data'=> ""]);
    }


    //edit profile function 
    public function addskill(Request $request)
    {
     $skillTitleCount = PerformerSkill::where('skill_title',$request->get('skill'))->where('performer_id',Auth::id())->count();

           if($skillTitleCount >= 1){
        $validator = Validator::make($request->all(),[  
            'skill'                   =>  'required|unique:skills,skill_title',
           
            
        ],
        [
            'skill.required'          =>'Enter Skill Title',
            'skill.unique'            =>'Skill Title Already Exist'
           
        ]);
        

        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }
    }
    $PerformerSkill              = new PerformerSkill;
    $PerformerSkill->skill_title = $request->skill;
    $PerformerSkill->performer_id= Auth::user()->id;
    $PerformerSkill->save();
       return response()->json(['status'=>true,'message'=>'']);
    
    }

    public function updateProfile(Request $request)
    {
 
        $validator = Validator::make($request->all(),[  
            'firstName'                   =>  'required',
            'middleName'                  =>  'required',
            'lastName'                    =>  'required',
            'agent_id'                    =>  'required',
            'union_name'                  =>  'required',
            // 'union_number'                =>  'required',
            'date_of_birth'               =>  'required',
            'email'                       =>  'required|email',
            'phonenumber'                 =>  'required',
            'address'                     =>  'required',
            'city'                        =>  'required',
            'province'                    =>  'required',
            'postalCode'                  =>  'required',
            'country'                     =>  'required',
           
            
            
        ],[
            'agent_id.required'           =>  'Select Agent',  
            'union_name.required'         =>  'Union Name  Required'  ,
            'union_number.required'       =>  'Union Number  Required'  ,
            'date_of_birth.required'      =>  'DOB Required'  
        ]);

        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }
        $Editdata = Performer::where('id',$request->id)->first();
        // $request['instagram'] = 'https://'.$request->instagram;
        // $request['twitter']   = 'https://'.$request->twitter;
        // $request['facebook']  = 'https://'.$request->facebook;
        // $request['linkedin']  = 'https://'.$request->linkedin;
            $request['date_of_birth'] = date('Y-m-d',strtotime($request['date_of_birth']));
            
            $Editdata ->age    =  Carbon::parse( date('Y-m-d',strtotime($request['date_of_birth'])))->age;

        $Editdata->update($request->all());


       $Appearance = Appearance::where('performer_id',$request->id)->first();
        if(isset($Appearance))
        {
            $Appearance->update(array('region_id'=>$request->region_id));    
        }else{

            $Appearance = new Appearance();
            $Appearance['region_id']    = $request->region_id;
            $Appearance['performer_id'] = $request->id;
            $Appearance->save();
        }
    
        return response()->json(['status'=>true,'message'=>'Profile Updated Successfully','data'=> "Editdata"]);   
    }
    public function deleteprofile(Request $request){
        $imageUpdate  =  Performer::where('id',$request->id)->update(array( 'upload_primary_url'=>null));
        $performer =Performer::where('id',$request->id)->first();
      // $html    = view('performers.showProfile',compact('performer'))->render();
       //return response()->json(['status'=>true,'message'=>'profile deleted Successfully',"response"=>$html]);
        return response()->json(['status'=>true,'message'=>'profile Deleted Successfully']);
    }
    public function storeprofile(Request $request){
        $performers_user            = Auth::user();
        $photo                      = $request['file'];
        if(!empty($photo))
        {
            $file_name              = time(). $photo->getClientOriginalName();
            $root                   = storage_path().'/app/public/performers/'.$performers_user->id;
            $photo->move($root,$file_name);           
           
        }
        
        $imageUpdate = Performer::where('id',$performers_user->id)->update(array("upload_primary_url"=>$file_name));
       $performer =Performer::where('id',$performers_user->id)->first();
        $html    = view('performers.showProfile',compact('performer'))->render();
        return response()->json(['status'=>true,'message'=>'profile Updated Successfully','data'=>$html]);
 //    return response()->json(['status'=>true,'message'=>'profile Updated Successfully']);

    }

     

     public function changePassword(Request $request)
     {

     $validator = Validator::make($request->all(),[  
            'old_password'                   =>  'required',
            'password'                   =>  'required',
            'confirm_password'                   =>  'required|same:password',
        ]);

        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }
        $password = Performer::where('id',Auth::id())->first();

        if (Hash::check($request->old_password,$password->password)) {

      $newPassword = Performer::where('id',Auth::id())->update(array('password'=>Hash::make($request->password)));

            if($newPassword){
                return response()->json(['status'=>true,'message'=>" Password change Successfully"]);  
            }   
                
        }else{

            return response()->json(['status'=>false,'message'=>"old Password Does Not Match"]);   
        }



     }
    
    public function portfolioSearch(Request $request)
    {
       
           if(!empty($request->get_id)){
            $per_id = $request->get_id;
            
        }else{
            $per_id = Auth::id();
        }
 
    

     $portfolio_search         = PerformerPortfolio::whereNotNull('portfolio_title');
    
     $portfolio_search             ->where('portfolio_title','LIKE','%'.request('search')."%");
     $portfolio_search             ->where('performer_id',$per_id);

     $portfolio_search             ->orWhereHas('portfolioTag',function($query) use($per_id) {
                                 $query->where('tag','LIKE','%'.request('search')."%");
                                 $query->where('performer_id',$per_id);
                                });

    
               
     $portfolio_search          = $portfolio_search->get();



     if(count($portfolio_search) > 0){
        
        $search_result       = $portfolio_search;    

        $helpdesk = HelpDesk::find(1);

        $html = view('performers.portfolioSearchResult',compact('search_result','helpdesk'))->render();       

        }else{        
        $html                 = "<div><h3>No Result Found </h3></div>";
        }

        return response()->json(['status'=>true,'html'=>$html]);
    }

    public function getTags(Request $request)
    {
        $id = $request->get('id');
        $portfolioGalleryTag = PortfolioGalleryTag::where('portfolio_id',$id)->get();
        
        $tag = $request->get('tags');
        $tags = Tags::where('tag', 'like', $tag.'%')
                    ->whereNotIn('tag', $portfolioGalleryTag->pluck('tag')->toArray())
                    ->get();
        return response()->json(['status'=>true,'html'=>$tags,'protfolio_id'=>$id]);
    }
    public function addagentlist(Request $request){
        return $request->all();
    }

    public function getTagsList(Request $request){
        $tag = $request->get('tags');
        $tags = Tags::where('tag', 'like', $tag.'%')
                    ->get();
        return response()->json(['status'=>true,'html'=>$tags]);
    }

    public function portfolioGalleryRemove(Request $request)
    {
        $deleteGallery = PerformerPortfolio::findorfail($request->id);
        MediaPortfolio::where('portfolio_id',$request->id)->Delete();  
        $deleteGallery->Delete();
        
        return response()->json(['status'=>true,'message'=>"Portfolio Gallery Removed Successfully"]);
    }

    //image tag remove 
    public function portfolioImageTagRemove(Request $request)
    {
       //return $tags = MediaPortfolio::where('id',$request->id)->first();
       // return $request->all();
    }


    // Group member store (Group sidebar code )
    public function addGroupMember(Request $request)
    {
        

         $validator = Validator::make($request->all(),[  
            'member_first_name'                   =>  'required',
            'member_last_name'                   =>  'required'            
        ]);

        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }

        $group_member = $request->all();
        $group_member['performer_id'] = Auth::id();
       

        $data = new GroupMember($group_member);
        $data->save();

        return response()->json(['status'=>true,'message'=>"Group Member Added Successfully"]);
    }
    
    // remove group member
    public function removeGroupMember(Request $request)
    {
        $del = GroupMember::where('id',$request->id)->first();

        if($del)
        {
            $del->delete();
            return response()->json(['status'=>true,'message'=>"Group Member Removed Successfully"]);       
        }
    }

    public function editGroupMember(Request $request)
    {
         $member = GroupMember::where('id',$request->id)->first();

        return response()->json(['status'=>true,'member'=>$member]);   
    }
    public function updateGroupMember(Request $request)
    {
       
        $Updaterecord = $request->all();
        $member = GroupMember::where('id',$request->id)->first();

        $updateNow = $member->update($Updaterecord);

        return response()->json(['status'=>true,'message'=>"Group Member Updated Successfully"]);   
    }

   
}
