<?php
namespace App\Http\Controllers\Performers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use App\Models\v1\Performer;
use App\Models\v1\Performer\PerformersFlags;
use App\Models\v1\Performer\Flags;
use App\Models\v1\PerformerCloset;
use App\Models\v1\MediaCloset;
use App\Models\v1\Tags;
use App\Models\v1\HelpDesk;
use App\Models\v1\ClosetGalleryTag;
use App\Models\v1\PerformerGroups;
use Carbon\Carbon;
use Validator;

class ClosetController extends Controller
{
    public function index($id="")
    {
        //$performers_user    =  Auth::user();
        if(Auth::guard('web')->check()){
          $performers_user    = Auth::user();
          $per_id = $performers_user->id;  
          $authtype = 'performer';
       }else{

          $per_id = $id;
            $performers_user    = Performer::where('id',$per_id)->first();

          $authtype = 'internal-staff';
       }


        //return $performer          =  Performer::with('PerformerCloset')->where('id',$per_id)->first();

        $performer          =  Performer::where('id',$per_id)->first();
    	$age                =  Carbon::parse($performer->date_of_birth)->age;
    	$age_range          =  $this->age_range($age);
        $closet_list        =  PerformerCloset::where('performer_id',$per_id)
                                   ->get();
        $groupUser          = PerformerGroups::with("groupPerformers")->where('id',$performer->group_id)                    ->first();

        $sidebar_image          =  MediaCloset::where('performers_id',$per_id)
                                ->where('media_type','like','%image%')
                                ->inRandomOrder()
                                ->take(4)
                                ->get();
        $flag       = Flags::all();
        $performerFlag = PerformersFlags::with('flag')->where('performer_id',$per_id)->get();
        $helpdesk  = HelpDesk::find(1);
      	return view('performers.closet',compact('performer','age_range','closet_list','sidebar_image','performers_user','groupUser','per_id','authtype','performerFlag','flag','helpdesk'));  

      

    }
    protected  function age_range($age)
    {
    	switch ($age) {
            case ($age < 19):
            return "Minor";
            break;
    		case ($age >= 20 && $age <= 30):
    		return "20 - 30 years";
    		break;
    		case ($age >= 31 && $age <= 40):
    		return "30 - 40 years";
    		break;
    		case ($age >= 41 && $age <= 50):
    		return "40 - 50 years";
    		break;    
    		case ($age >= 51 && $age <= 60):
    		return "50 - 60 years";
    		break;    
    		case ($age >= 61 && $age <= 70):
    		return "60 - 70 years";
    		break;    
    		case ($age >= 71 && $age <= 80):
    		return "70 - 80 years";
    		break;    
    		case ($age >= 81 && $age <= 90):
    		return "80 - 90 years";
    		break;
    		case ($age >= 91 && $age <= 100):
    		return "90 - 100 years";
    		break;    
    		default:
    		return "100 +";
    	}
    }
      // Store Images For Performer Closet
    public function storeClosetImages(Request $request)
    {
        $performers_user        =  Auth::user();
        $photo                      = $request['file'];
        if(!empty($photo))
        {
            $file_name              = time(). $photo->getClientOriginalName();
             $ext              = $photo->getClientOriginalExtension();
            $root                   = storage_path().'/app/public/closetmedia/'.$performers_user->id;
            $photo->move($root,$file_name);
            $performer['name']      = $file_name;     

             $imageFormate = array("jpg","gif","png","jpeg","bmp");
            if(in_array(strtolower($ext),$imageFormate))
            {
                $performer['type'] ="image";
            }else{
                $performer['type'] ="video";
            }       
            // $performer['type']      = $request['file']->getClientMimeType();
            
        }
        else
        {
            $performer['name']       =  "";
            // $performer['type']       =  "";            
        }
     
        return response()->json(['data'=>$performer]);

    }

    // Store Video For Performer Skill
    public function storeClosetVideo(Request $request)
    {   
        $performers_user            =  Auth::user();          
        $video                      = $request['file'];
        if(!empty($video))
        {
            $file_name              = time(). $video->getClientOriginalName();
            $root                   = storage_path().'/app/public/closetmedia/'.$performers_user->id;
            $video->move($root,$file_name);
            $performer['name']      = $file_name;
            $performer['type']      = "video";
            
        }
        else
        {
            $performer['name']       =  "";
            $performer['type']       =  "";
            
        }
        
        return response()->json(['data'=>$performer]);

    }

     //store media for performers usaer 
    public function storeClosetMedia(Request $request)
    { 
        $closetTitleCount = PerformerCloset::where('closet_title',$request->get('closet_title'))->where('performer_id',Auth::id())->count();

          if($closetTitleCount != 0){
        $validator = Validator::make($request->all(),[  
            'closet_title'               =>  'required|unique:closets,closet_title',
        ],[
            'closet_title.unique' => 'Closet Title Already Taken'
        ]);

        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }
    }
        $performer                              =    $request->all();
        $performer_user                         = Auth::user();

        $performerCloset['closet_title']        = $request->get('closet_title');
        $performerCloset['descripation']        = $request->get('description');
        $performerCloset['performer_id']        = $performer_user->id;
        if($request->is_video                 ==  "VideoUpload")
        {
            $performerCloset['is_video']       =    "yes";
        }else
        {
            $performerCloset['is_video']       =    "no";
        }

        $closet                                 = new PerformerCloset($performerCloset);
        $closet->save();

        $tags = null;
        if ($request->get('tags') && $request->get('tags')!="") {
            $tags = implode(',', $request->get('tags'));
        }
        if(!empty($performer['images'])){
            $mediaStore =[];
            foreach ($performer['images'] as $key   => $value) {
                $mediaStore[$key]['performers_id']    =   $performer_user->id;
                $mediaStore[$key]['media_url']       =  $value;               
                $mediaStore[$key]['closet_id']       = $closet->id;
                $mediaStore[$key]['media_type']      =  $performer['type'][$key];
                $mediaStore[$key]['media_title']     =  $performer['media_title'];
                $mediaStore[$key]['tag']             =  $tags;
            }
            MediaCloset::insert($mediaStore);
        }
        $tags=[];
        if(!empty($request['tags']))
        {
            foreach ($request['tags'] as $key => $value) {
                $tags[$key]['tag'] = $request['tags'][$key];
                $tags[$key]['closet_id'] = $closet->id;

            }
        }
        ClosetGalleryTag::insert($tags);
       return response()->json(['status'=>true,'message'=>'Closet Inserted Successfully']);
    }
    public function viewGallery($id,$pid='')
    {

     if($pid==''){
        $performers_user = Auth::user();
        
         }else{
        $performers_user = Performer::where('id',$pid)->first();
        }
         $gallery = PerformerCloset::with('performerMedia')
                                    ->where('id',$id)
                                    ->where('performer_id',$performers_user->id)
                                    ->first();


        return view('performers.closetgallery',compact('gallery','performers_user'));
    }

    // closet galary Image
    public function closetGallerySlideImages(Request $request)
    {
        $id                         =    $request->input('id');
        $gallery                    =    MediaCloset::where('closet_id',$id)
                                        ->get();
        $slide_img=[];     
        foreach ($gallery as $key   => $value)
        {
            $slide_img[$key]        =    $value['image_url'];
        }
        $slide                      =  $slide_img;
        return $slide;
    }
    
    //display video
    public function closetvideoShow(Request $request)
    {
        $video =MediaCloset::where('id',$request->get('id'))->first();

        return response()->json(['status'=>true,'message'=>'','data'=>$video]);
      
    }

     public function addClosetImages(Request $request)
    {

        $performers_user       =    Auth::user();

         if(!empty($request['images'])){
        $mediaStore =[];
        foreach ($request['images'] as $key     => $value) {
            $mediaStore[$key]['performers_id']   =  $performers_user->id;
            $mediaStore[$key]['closet_id']       =  $request['closet_id'];  
            $mediaStore[$key]['media_url']      =  $value;                           
            $mediaStore[$key]['media_type']     =  $request['type'][$key];
            $mediaStore[$key]['media_title']    =  $request['media_title'];
            $mediaStore[$key]['performers_id']    =  $performers_user->id;
        }

      MediaCloset::insert($mediaStore);
    }

    $tags=[];
    if(!empty($request['tags']))
    {
        foreach ($request['tags'] as $key => $value) {
            $tags[$key]['tag'] = $request['tags'][$key];
            $tags[$key]['closet_id'] = $request['closet_id'];

        }
    }
    ClosetGalleryTag::insert($tags);

    return response()->json(['status'=>true,'message'=>'Closet Image Added Successfully']);
}
    
    //add videio
    public function addClosetVideo(Request $request)
    {
        $performers_user       =    Auth::user();

         if(!empty($request['videos'])){
        $mediaStore =[];
        foreach ($request['videos'] as $key     => $value) {
            $mediaStore[$key]['performers_id']   =  $performers_user->id;
            $mediaStore[$key]['closet_id']       =  $request['closet_id'];  
            $mediaStore[$key]['media_url']      =  $value;                           
            $mediaStore[$key]['media_type']     =  $request['type'][$key];
            $mediaStore[$key]['media_title']    =  $request['media_title'];
        }
      MediaCloset::insert($mediaStore);
    }
    return response()->json(['status'=>true,'message'=>'Closet Image Added Successfully']);
    }

public function closetAddTag(Request $request)
    {
        $data   =$request->all();
        $tag = new ClosetGalleryTag($data);
        $tag->save();
        if($tag)
        {
            return response()->json(['status'=>true,'message'=>'Closet Tag Inserted Successfully','data'=>$tag]);          
        }
    }

    public function removeClosetTag(Request $request)
    {

         $tag = ClosetGalleryTag::where('id',$request->id)->first();
         $tag->delete();

         return response()->json(['status'=>true,'message'=>'Closet Tag Removed Successfully','data'=> ""]);
    }

    //CLOSET UPDATE GALLERY
    public function closetGalleryUpdate(Request $request){
       
     $closetTitleCount = PerformerCloset::where('closet_title',$request->get('closet_title'))->where('performer_id',Auth::id())->count();

       if($closetTitleCount >= 2){
       $validator = Validator::make($request->all(),[  
           'closet_title'               =>  'required|unique:closets,closet_title,'.$request->gallery_id,
       ],[
           'closet_title.unique' => 'Closet Title Already Taken'
       ]);

       if($validator->fails()){
           return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
       }
    }



        $closetId = $request->get('gallery_id');
        $user = Auth::User();
        PerformerCloset::where('id',$closetId)
                            ->update(['closet_title'=>$request->get('closet_title')]);
        

        $type = $request->get('type');
        $tags = null;
        if ($request->get('tags') && $request->get('tags')!="") {
            $tags = implode(',', $request->get('tags'));
        }
        if(!empty($request->get('images'))){
            $mediaStore =[];
            foreach ($request->get('images') as $key   => $value) {
                $mediaStore[$key]['media_url']     =  $value;   
                $mediaStore[$key]['media_type']    =  $type[$key];
                $mediaStore[$key]['closet_id']     =  $closetId;
                $mediaStore[$key]['media_title']   =  $request->get('media_title');
                $mediaStore[$key]['performers_id'] =  $user->id;
                $mediaStore[$key]['tag']           =  $tags;
            }
            MediaCloset::insert($mediaStore);
        }
        $tags=[];
        if(!empty($request['tags']))
        {
            foreach ($request['tags'] as $key => $value) {
                $tags[$key]['tag'] = $request['tags'][$key];
                $tags[$key]['closet_id'] = $closetId;

            }
        }
      //  ClosetGalleryTag::insert($tags);

        return response()->json([
            'status'=>true,
            'message'=>'Portfolio Gallery Updated Successfully',
        ]);
    }



    //remove image or video
    public function removeClosetMedia(Request $request)
    {
         $mediaRemove = MediaCloset::where('id',$request->id)->first();
         $mediaRemove->delete();
         return response()->json(['status'=>true,'message'=>'Closet Media Remove Successfully','data'=> ""]);
    }


    //closet search
    public function closetSearch(Request $request)
    {
     
     
     if(!empty($request->get_id)){
        $per_id = $request->get_id;
        
     }else{
        $per_id = Auth::id();
     }

     

     $closet_search             = PerformerCloset::whereNotNull('closet_title');
        
    $closet_search                  ->where('closet_title','LIKE','%'.request('search')."%");
     $closet_search                  ->where('performer_id',$per_id);

     $closet_search                 ->orWhereHas('closetTag',function($query) use($per_id){
                                    $query->where('tag','LIKE','%'.request('search')."%");
                                    $query->where('performer_id',$per_id);
                                    });
     
               
     $closet_search             = $closet_search->get();

     if(count($closet_search)>0){
        
        $search_result       = $closet_search;    

        $helpdesk  = HelpDesk::find(1);

        $html = view('performers.closetSearchResult',compact('search_result','helpdesk'))->render();       

        }else{        
        $html                 = "<div><h3>No Result Found </h3></div>";
        }

        return response()->json(['status'=>true,'html'=>$html]);
    }


    public function getTags(Request $request){
        $id = $request->get('id');
        $closetGalleryTag = ClosetGalleryTag::where('closet_id',$id)->get();
        
        $tag = $request->get('tags');
        $tags = Tags::where('tag', 'like', $tag.'%')
                    ->whereNotIn('tag', $closetGalleryTag->pluck('tag')->toArray())
                    ->get();
        return response()->json(['status'=>true,'html'=>$tags,'closet_id'=>$id]);
    }

    public function closetGalleryRemove(Request $request)
    {
       
       $deleteGallery = PerformerCloset::findorfail($request->id);
       MediaCloset::where('closet_id',$request->id)->Delete();  
       ClosetGalleryTag::where('closet_id',$request->id)->Delete();  
       $deleteGallery->Delete();
        
        return response()->json(['status'=>true,'message'=>"Gallery Removed Successfully"]);
    }
}

