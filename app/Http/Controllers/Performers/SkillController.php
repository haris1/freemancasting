<?php

namespace App\Http\Controllers\Performers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\v1\Performer;
use App\Models\v1\HelpDesk;
use App\Models\v1\PerformerSkill;
use App\Models\v1\PerformerGroups;
use App\Models\v1\MediaSkill;
use App\Models\v1\MediaPortfolio;
use App\Models\v1\SkillGalleryTag;
use App\Models\v1\SkillSection;
use App\Models\v1\Performer\PerformersFlags;
use App\Models\v1\Performer\Flags;
use App\Models\v1\Tags;
use Carbon\Carbon;
use Validator;
use DB;

class SkillController extends Controller
{
    public function index($id="")
    {
        if(Auth::guard('web')->check()){
          $performers_user    = Auth::user();
          $per_id = $performers_user->id;  
          $authtype = 'performer';
       }else{
          $per_id = $id;
           $performers_user    = Performer::where('id',$per_id)->first();
          $authtype = 'internal-staff';
       }
    	$performer         =   Performer::where('id',$per_id)->first();
    	$age               =   Carbon::parse($performer->date_of_birth)->age;
    	$age_range         =   $this->age_range($age);

        $skill_list        = PerformerSkill::with('skillFields')
                                ->where('performer_id',$per_id)
                                ->get();
        $groupUser         = PerformerGroups::with("groupPerformers")->where('id',$performer->group_id)                    ->first();

       $sidebar_image     =  MediaPortfolio::where('performers_id',$per_id)
                                      ->where('media_type','like','%image%')
                                      ->inRandomOrder()
                                      ->take(4)
                                      ->get();
        $flag       = Flags::all();
        $skillsection       = SkillSection::all();
        $performerFlag = PerformersFlags::with('flag')->where('performer_id',$per_id)->get();
        $helpdesk = HelpDesk::find(1);
    	return view('performers.skills',compact('performer','age_range','skill_list','sidebar_image','performers_user','skillsection','groupUser','per_id','authtype','performerFlag','flag','helpdesk'));  
    }
    protected  function age_range($age)
    {
    	switch ($age) {
            case ($age < 19):
            return "Minor";
            break;
    		case ($age >= 20 && $age <= 30):
    		return "20 - 30 years";
    		break;
    		case ($age >= 31 && $age <= 40):
    		return "30 - 40 years";
    		break;
    		case ($age >= 41 && $age <= 50):
    		return "40 - 50 years";
    		break;    
    		case ($age >= 51 && $age <= 60):
    		return "50 - 60 years";
    		break;    
    		case ($age >= 61 && $age <= 70):
    		return "60 - 70 years";
    		break;    
    		case ($age >= 71 && $age <= 80):
    		return "70 - 80 years";
    		break;    
    		case ($age >= 81 && $age <= 90):
    		return "80 - 90 years";
    		break;
    		case ($age >= 91 && $age <= 100):
    		return "90 - 100 years";
    		break;    
    		default:
    		return "100 +";
    	}
    }


     // Store Images For Performer Skill
    public function storeSkillImages(Request $request)
    {   
        
        $performer = Auth::User();
        $photo                              = $request['file'];
        if(!empty($photo))
        {
            $file_name                      = time(). $photo->getClientOriginalName();
            $root                           = storage_path().'/app/public/skillsmedia/'.$performer->id;
            $photo->move($root,$file_name);
            $performer['name']              = $file_name;
            $performer['type']              = "image";
           
            
        }
        else
        {
            $performer['name']       =  "";
            $performer['type']       =  "";
            
        }
        
        return response()->json(['data'=>$performer]);

    }

     // Store Video For Performer Skill
    public function storeSkillVideo(Request $request)
    {   
        $performer = Auth::User();
        $video                      = $request['file'];
        if(!empty($video))
        {
            $file_name              = time(). $video->getClientOriginalName();
            $root                   = storage_path().'/app/public/skillsmedia/'.$performer->id;
            $video->move($root,$file_name);
            $performer['name']      = $file_name;
            $performer['type']      = "video";
            
        }
        else
        {
            $performer['name']       =  "";
            $performer['type']       =  "";
            
        }
        
        return response()->json(['data'=>$performer]);

    }

    //store media for Skill Media  
    public function storeSkillMedia(Request $request)
    { 


            

        $validator = Validator::make($request->all(),[  
            'skill_title'               =>  'required|unique:skills,skill_title',
        ]);

        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>'Enter Skill Title']);
        }


        $performer                            =   $request->all();
        $performer_user                       =   Auth::user();

        $performerSkill['skill_title']        =   $request->get('skill_title');
        $performerSkill['description']        =   $request->get('description');
        $performerSkill['performer_id']       =   $performer_user->id;
        
        if($request->is_video                 ==  "VideoUpload")
        {
            $performerSkill['is_video']       =    "yes";
        }else
        {
            $performerSkill['is_video']       =    "no";
        }



        $skill                                 =  new PerformerSkill($performerSkill);
        $skill->save();
        $tags = null;
        if ($request->get('tags') && $request->get('tags')!="") {
            $tags = implode(',', $request->get('tags'));
        }
        if(!empty($performer['images'])){
            $mediaStore =[];
            foreach ($performer['images'] as $key   => $value) {
                $mediaStore[$key]['performers_id']    =   $performer_user->id;
                $mediaStore[$key]['media_url']       =  $value;               
                $mediaStore[$key]['skill_id']        = $skill->id;
                $mediaStore[$key]['media_type']      =  $performer['type'][$key];
                $mediaStore[$key]['media_title']     =  $performer['media_title'];
                $mediaStore[$key]['tag']             =  $tags;
            }
            MediaSkill::insert($mediaStore);
        }
       
    return response()->json(['status'=>true,'message'=>'Skill Inserted Successfully']);
    }


  //  public function viewGallery($id)
  //   {
  //        $performers_user       =    Auth::user();
  //        $gallery               =    PerformerSkill::with('performerMedia')
  //                                   ->where('id',$id)
  //                                   ->where('performer_id',$performers_user->id)
  //                                   ->first();

    
  //   return view('performers.skillgallery',compact('gallery'));
  // }


    public function skillGallerySlideImages(Request $request)
    {
        $id                    =    $request->input('id');    
        $gallery               =    MediaSkill::where('skill_id',$id)
                                    ->get();

        $slide_img=[];
 
        foreach ($gallery as $key => $value) {
            $slide_img[$key]    =    $value['image_url'];
        }
        $slide =  $slide_img;
        return $slide;
    }

    //display video
    public function videoShow(Request $request)
    {
        $video =MediaSkill::where('id',$request->get('id'))->first();

        return response()->json(['status'=>true,'message'=>'','data'=>$video]);
      
    }
    //add images
    public function addSkillImages(Request $request)
    {
        $performers_user       =    Auth::user();

         if(!empty($request['images'])){
        $mediaStore =[];
        foreach ($request['images'] as $key     => $value) {
            $mediaStore[$key]['performers_id']   =  $performers_user->id;
            $mediaStore[$key]['skill_id']       =  $request['skill_id'];  
            $mediaStore[$key]['media_url']      =  $value;                           
            $mediaStore[$key]['media_type']     =  $request['type'][$key];
            $mediaStore[$key]['media_title']    =  $request['media_title'];
        }
      MediaSkill::insert($mediaStore);
    }
    return response()->json(['status'=>true,'message'=>'Skill Inserted Successfully']);
    }

    //add video
    
    public function addSkillVideo(Request $request)
    {
        $performers_user       =    Auth::user();

         if(!empty($request['videos'])){
        $mediaStore =[];
        foreach ($request['videos'] as $key     => $value) {
            $mediaStore[$key]['performers_id']   =  $performers_user->id;
            $mediaStore[$key]['skill_id']       =  $request['skill_id'];  
            $mediaStore[$key]['media_url']      =  $value;                           
            $mediaStore[$key]['media_type']     =  $request['type'][$key];
            $mediaStore[$key]['media_title']    =  $request['media_title'];
        }
      MediaSkill::insert($mediaStore);
    }
    return response()->json(['status'=>true,'message'=>'Skill Video Added Successfully']);
    }

    public function addTag(Request $request)
    {      
            $data                                =  $request->all();
            $tag                                 = new SkillGalleryTag($data);
            $tag->save();
        
        if($tag)
        {
            return response()->json(['status'=>true,'message'=>'Skill Tag Inserted Successfully','data'=>$tag]);          
        }
    }

    public function removeTag(Request $request)
    {
         $tag = SkillGalleryTag::where('id',$request->id)->first();
         $tag->delete();

         return response()->json(['status'=>true,'message'=>'Skill Tag Removed Successfully','data'=> ""]);
    }


    public function skillGalleryUpdate(Request $request){
        $skillId = $request->get('gallery_id');
        $user = Auth::User();
        PerformerSkill::where('id',$skillId)
                            ->update(['skill_title'=>$request->get('skill_title')]);
        

        $type = $request->get('type');
        if(!empty($request->get('images'))){
            $mediaStore =[];
            foreach ($request->get('images') as $key   => $value) {
                $mediaStore[$key]['media_url']       =  $value;   
                $mediaStore[$key]['media_type']      =  $type[$key];
                $mediaStore[$key]['skill_id']    =  $skillId;
                $mediaStore[$key]['media_title']     =  $request->get('media_title');
                $mediaStore[$key]['performers_id']     =  $user->id;
            }
            MediaSkill::insert($mediaStore);
        }

        return response()->json([
            'status'=>true,
            'message'=>'Portfolio Gallery Updated Successfully',
        ]);
    }

    //remove image or video
    public function removeSkillMedia(Request $request)
    {
         $mediaRemove = MediaSkill::where('id',$request->id)->first();
         $mediaRemove->delete();
         return response()->json(['status'=>true,'message'=>'Skill Media Removed Successfully','data'=> ""]);
    }

    // skill search
    public function skillsSearch(Request $request)
    {
         if(!empty($request->get_id)){
            $per_id = $request->get_id;
            
         }else{
            $per_id = Auth::id();
         }

    $helpdesk = HelpDesk::find(1);

    $skill_search  = PerformerSkill::with('skillFields')->whereNotNull('skill_title');

    $skill_search         ->where('performer_id',$per_id);

    $skill_search      ->whereHas('skillFields',function($query){
                           $query->where('field_name','LIKE','%'.request('search')."%");
                           $query->orWhere('field_value','LIKE','%'.request('search')."%");
                          });
     
    $skill_search       = $skill_search->get();
    
        
   if(count($skill_search)>0){
        
    $search_result = $skill_search;    

        $html = view('performers.skillsSearchResult',compact('search_result','helpdesk'))->render();       

        }else{        
        $html ="<div><h3>No Result Found </h3></div>";
        }

        return response()->json(['status'=>true,'html'=>$html]);
    }

    public function getTags(Request $request){
        $id = $request->get('id');
        $skillGalleryTag = SkillGalleryTag::where('skill_id',$id)->get();
        
        $tag = $request->get('tags');
        $tags = Tags::where('tag', 'like', $tag.'%')
                    ->whereNotIn('tag', $skillGalleryTag->pluck('tag')->toArray())
                    ->get();
        return response()->json(['status'=>true,'html'=>$tags,'skill_id'=>$id]);
    }
    public function addskillsection(Request $request){
        $validator = Validator::make($request->all(),[  
            'field_name'                   =>  'required',
            'field_value'                  =>  'required',
            
        ],
        [
            'field_name.required'          =>'Skill Name  Required',
            'field_value.required'         =>'Skill Value  Required'
        ]);
        

        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }
        $SkillSection  = new SkillSection($request->all());
        $SkillSection->save();
        $skill_id      = $request->skill_id;
        $skillsection  = SkillSection::where('skill_id',$skill_id)->get();
                         
        $html    = view('performers.skillsection',compact('skillsection'))->render();
        return response()->json(['status'=>true,'message'=>'','response'=>$html]);
    }
    public function editskillsection(Request $request){
        $validator = Validator::make($request->all(),[  
            'field_name'                   =>  'required',
            'field_value'                  =>  'required',
            
        ],
        [
            'field_name.required'          =>'Skill Name  Required',
            'field_value.required'         =>'Skill Value  Required'
        ]);
        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }
       $updateSkillSection =SkillSection::find($request->id);
       $updateSkillSection->update($request->all());
       $skillsection  = SkillSection::where('skill_id',$request->skill_id)->get();
                        
       $html    = view('performers.skillsection',compact('skillsection'))->render();
       return response()->json(['status'=>true,'message'=>'','response'=>$html]); 
    }
    public function deleteskill(Request $request){
       $PerformerSkill =  PerformerSkill::find($request->id);
       

       $PerformerSkill->performerMedia()->delete();
       $PerformerSkill->skillTag()->delete();
       $PerformerSkill->skillFields()->delete();
         $PerformerSkill->delete();
        //SkillSection::skillFields()->delete();
          return response()->json(['status'=>true,'message'=>'Skill Deleted Successfully']); 
    }
    public function deletefieldsection(Request $request){
        $deleteSkillSection =SkillSection::find($request->id);
        $deleteSkillSection->delete();
        $skillsection  = SkillSection::where('skill_id',$request->skill_id)->get();
        $html    = view('performers.skillsection',compact('skillsection'))->render();
        return response()->json(['status'=>true,'message'=>'Skill Field Deleted Successfully','response'=>$html]); 
    }
    public function getSkillTitle(Request $request){
       $PerformerSkill =  PerformerSkill::find($request->id);
       return $PerformerSkill->skill_title; 
    }
    public function updateskilltitle(Request $request){
             
        $validator = Validator::make($request->all(),[  
            'skill_title'                   =>  'required|unique:skills,skill_title,'.$request->id,
            
        ],[
            'skill_title.required'          =>'Enter Skill Title',
            'skill_title.unique'            =>'Skill Title Already Exist'
        ]);
        

        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }

        $PerformerSkill =  PerformerSkill::find($request->id);
        $PerformerSkill->update($request->all());
        return response()->json(['status'=>true,'message'=>'Skill Title Updated Successfully']); 
    }

}