<?php

namespace App\Http\Controllers\Performers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use App\Models\v1\Performer;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/portfolio';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:web')->except('logout');
    }


    public function showLoginForm(){
        return view('performers.auth.login');
    }

    public function checkUser(Request $request){
        $emailAddress = $request->input('email');
        $result = Performer::where('email',$emailAddress)->get();
        if(count($result)>1 && !empty($emailAddress)){
            $response ='';
            foreach ($result as $key => $value) {
                $response .='<option value="'.$value->firstName.'">'.$value->firstName.' '.$value->lastName.'</option>';
            }
            return response()->json(array('status'=>true,'html'=>$response,'count'=>1), 200);
            // echo $emailAddress;    
        }else{
            return response()->json(array('status'=>true,'html'=>'','count'=>0), 200);
        }
        
    }


public function login(Request $request)
    { 
        $firstName = $request->firstName;
      
        if(!empty($firstName)){

           // $performers = Performer::where('email',$request->email)->where('firstName', $firstName)->where('Password',bcrypt($request->password))->first();
           // print_r($performers);
            if(Auth::guard('web')->attempt(['email' => $request->email,'firstName'=>$firstName ,'password' => $request->password])) {
                //Auth::loginUsingId($performers->id);
           return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
            }else{
              //  return response()->json(array('status'=>false,'msg'=> 'Please enter valid credential'), 200);
                return $this->sendFailedLoginResponse($request);
            }
        }else{
            if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password])) {
               return redirect()->intended($this->redirectPath());
            }else{
                //return response()->json(array('status'=>false,'msg'=> 'Please enter valid credential'), 200);
                return $this->sendFailedLoginResponse($request);
            }
        }
    }

    protected function guard(){
        return Auth::guard('web');
    }
    public function logout(Request $request){
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect()->route('performers.login');
    }
}
