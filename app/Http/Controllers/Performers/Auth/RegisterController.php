<?php

namespace App\Http\Controllers\Performers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Models\v1\Performer;
use App\Models\v1\PerformersDetails;
use App\Models\v1\PerformerAgent;
use App\Models\v1\Performer\PerformerUnion;
use App\Models\v1\Performer\PerformerAvailability;
use App\Models\v1\PerformerPortfolio;
use App\Models\v1\PerformerSkill;
use App\Models\v1\PerformerRental;
use App\Models\v1\PerformerCloset;
use App\Models\v1\MediaPortfolio;
use App\Models\v1\Workperformer;
use App\Models\v1\Performer\Piercing;
use App\Models\v1\Performer\Tattoo;
use File;
use Image;
use Auth;
use Storage;
use Carbon\Carbon;
use App\Models\v1\PerformerGroups;
use DB;
class RegisterController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function showRegistrationForm(){
        $PerformerUnion = PerformerUnion::get();
        $agent          = PerformerAgent::get();
        $PerformerAvailability = PerformerAvailability::get();
        return view('performers.auth.register',compact('PerformerUnion','PerformerAvailability','agent'));
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstName' => ['required', 'string', 'max:255'],
            
            'lastName' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function register(Request $request)
    {   
         
        $validator = Validator::make($request->all(),[
                        'firstName' => "required",
                        
                        'lastName' => "required",
                        'email' => "required|string|email",
                        'phonenumber' => "required",
                        'password' => "required",
                        
                        // 'gender' => "required|in:male,female,transgender",
                        'date_of_birth' => "required",
                        
                        
                        'agent_id'    => "required",

                        'union_name' => "required",
                        // 'union_number' => "required|string|union_number|unique:performers,union_number",
                        // 'union_number' => "required",
                        // 'sin'          =>  "required"
                    ]);
        if ($validator->fails()) {
            return response()->json(['status'=> false,'message'=>$validator->messages()->first()]);
        }
       
        $data = $request->except('sin');

        $applicationfor = $data['applicationfor'];
        $data['parent_aggrement'] = '';
        $data['password'] = bcrypt($data['password']);
        $data['date_of_birth'] = date('Y-m-d',strtotime($data['date_of_birth']));
        $age                    =  Carbon::parse( date('Y-m-d',strtotime($data['date_of_birth'])))->age;
        $data['age']=$age;
        // $data['instagram'] = 'https://'.$data['instagram'];
        // $data['twitter']   = 'https://'.$data['twitter'];
        // $data['facebook']  = 'https://'.$data['facebook'];
        // $data['linkedin']  = 'https://'.$data['linkedin'];
        $performer = Performer::create($data);
        $perId = $performer->id; 
        
        if (isset($data['upload_primary_url']) && $data['upload_primary_url']!="") {
            $url = public_path('storage/performers/'.$data['upload_primary_url']);
            $contents = file_get_contents($url);
            $name = substr($url, strrpos($url, '/') + 1);
            
            Storage::put('public/performers/'.$perId.'/'.$name, $contents);
            File::delete($url);
        }
        if($request->hasfile('parent_aggrement') && $applicationfor=='Child'){
            $image = $request->file('parent_aggrement');
            if(!File::exists(storage_path('app/public/performers/'))){
                $directoryPath = storage_path('app/public/performers/');
                File::makeDirectory($directoryPath,0755,true);
            }            
            
            $extension = $image->getClientOriginalExtension();            
            $imageUniqueName = uniqid(uniqid(true).'_').'.'.$extension;
           // $img->save(storage_path('app/public/performers/'.$imageUniqueName));

            $image->move(storage_path('app/public/performers/'.$perId.'/'),$imageUniqueName);
            $performer->parent_aggrement = $imageUniqueName;
        }
        $performer->save();
         $performersDetails    = performersDetails::updateOrCreate(
            ['performer_id'  => $performer->id],
            ['sin'           => $request->sin ]
        );

         //blank portfolio gallary create
         $performerPortfolio = array(
            array('portfolio_title'=>'Headshots','performer_id'=> $performer->id),
            array('portfolio_title'=>'Full Body Shots','performer_id'=> $performer->id),
            array('portfolio_title'=>'Tattoos','performer_id'=> $performer->id),
            array('portfolio_title'=>'Piercings','performer_id'=> $performer->id),
         );
          DB::table('performer_portfolios')->insert($performerPortfolio); // Query Builder approach

          
    

         $performerPortfolioAudio = array(
            array('portfolio_title'=>'Audio Reel','is_video'=>'yes','performer_id'=> $performer->id),
        );
          //PerformerPortfolio::insert($performerPortfolioAudio); 
DB::table('performer_portfolios')->insert($performerPortfolioAudio); // Query Builder approach

         //default skills 
         $PerformerSkill = array(
            array('skill_title'=>'Qualifications','performer_id'=>$performer->id),
            array('skill_title'=>'Community Servants','performer_id'=>$performer->id),
            array('skill_title'=>'Sports','performer_id'=>$performer->id),
            array('skill_title'=>'Circus/Magic','performer_id'=>$performer->id),
            array('skill_title'=>'Culinary','performer_id'=>$performer->id),
            array('skill_title'=>'Dance','performer_id'=>$performer->id),
            array('skill_title'=>'Military','performer_id'=>$performer->id),
            array('skill_title'=>'Music','performer_id'=>$performer->id),
            array('skill_title'=>'Bands','performer_id'=>$performer->id),
            array('skill_title'=>'Speciality Driver','performer_id'=>$performer->id),
            array('skill_title'=>'Others','performer_id'=>$performer->id)

         );

         //DB::table('skills')->insert($PerformerSkill); // Query Builder approach
           PerformerSkill::insert($PerformerSkill);

           $Workperformer = array(            
               array('resume_title'=>'Background Work','performer_id'=> $performer->id),
               array('resume_title'=>'Speaking Roles','performer_id'=> $performer->id),
               array('resume_title'=>'Commercial Roles','performer_id'=> $performer->id)
           );   
          
            DB::table('work_perfomers')->insert($Workperformer); // Query Builder approach

         //blank CLOSET gallary create
         $performerCloset = array(
            array('closet_title'=>'Winter','performer_id'=> $performer->id),
            array('closet_title'=>'Spring','performer_id'=> $performer->id),
            array('closet_title'=>'Summer','performer_id'=> $performer->id),
            array('closet_title'=>'Fall','performer_id'=> $performer->id),
        );
         //DB::table('closets')->insert($performerCloset); 
       PerformerCloset::insert($performerCloset);
   
        $performerProps = array(
            array('rental_title'=>'My Props','performer_id'=> $performer->id),
            array('rental_title'=>'My Pets','performer_id'=> $performer->id),
            array('rental_title'=>'My Vehicle','performer_id'=> $performer->id),
            array('rental_title'=>'My Sports','performer_id'=> $performer->id),
            array('rental_title'=>'My Musical Instruments','performer_id'=> $performer->id),
        );
        // DB::table('performer_rentals')->insert($performerProps);
        PerformerRental::insert($performerProps);

        
        Auth::guard('web')->login($performer);
        return response()->json(['status'=> true,'message'=>'Your account has been registered successfully']);

    }

    public function checkedEmail(Request $request)
    {
        $validator = Validator::make($request->all(),[
                        'email' => "required|string|email|unique:performers,email",
                    ]);
        if ($validator->fails()) {
            return response()->json(['status'=> false,'message'=>$validator->messages()->first()]);
        }


        /*$email = $request->get('email');
        $performer = Performer::where('email',$email)->first();
        
        if($performer){
            return response()->json([
                'status'=>false,
                'message'=>'Email already exists please try again'
            ]);
        }*/
        return response()->json([
            'status'=>true,
            'message'=>''
        ]);
    }


    public function checkedUnionNumber(Request $request){
        // $unionNumber = $request->get('union_number');
        // $performer = Performer::where('union_number',$unionNumber)->first();
        
        // if($performer){
        //     return response()->json([
        //         'status'=>false,
        //         'message'=>'Uunion number already exists please try again'
        //     ]);
        // }
        return response()->json([
            'status'=>true,
            'message'=>''
        ]);   
    }


    //IMAGE STORE TEM
    public function imageUpload(Request $request){
        if($request->hasfile('file')){
            $image = $request->file('file');

            if(!File::exists(storage_path('app/public/performers/'))){
                $directoryPath = storage_path('app/public/performers/');
                File::makeDirectory($directoryPath,0755,true);
            }            
            $extension = $image->getClientOriginalExtension();
            $img = Image::make($image);
            $height = $img->height();
            $width = $img->width();
            $imageUniqueName = uniqid(uniqid(true).'_').'.'.$extension;
            $img->save(storage_path('app/public/performers/'.$imageUniqueName));
            $data = $imageUniqueName;
        }

        return response()->json([
            'status'=>true,
            'image'=>$data
        ]); 
    }


    public function addGroup(Request $request)
    {

         $validator = Validator::make($request->all(),[
                        'group_name' => 'unique:performer_groups'
                    ]);

        if ($validator->fails()) {
            return response()->json(['status'=> false,'message'=>$validator->messages()->first()]);
        }
            $group                  = $request->all();
            $data                   =  New PerformerGroups($group);
            $data->save();


            return response()->json(['status'=> true,'message'=>'Add Group Successfully','data'=>$data->id]);
    }

    public function groupList(Request $request)
    {
       // $grouplist = PerformerGroups::get()->pluck('id','group_name');

            $term = $request['term'];
            $results = array();
            $queries =PerformerGroups::
                where('group_name', 'LIKE', '%'.$term.'%')->get();

            foreach ($queries as $query)
            {
                $results[] = [ 'id' => $query->id, 'value' => $query->group_name];
            }
            return response()->json($results);

            }

}
