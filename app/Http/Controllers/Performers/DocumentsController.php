<?php

namespace App\Http\Controllers\Performers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\v1\Performer;
use App\Models\v1\HelpDesk;
use App\Models\v1\Performer\Document;
use App\Models\v1\MediaPortfolio;
use App\Models\v1\PerformerDocument;
use App\Models\v1\Performer\PerformersFlags;
use App\Models\v1\Performer\Flags;

//use App\Models\v1\PerformerGroups;
use Carbon\Carbon;
use Validator;

class DocumentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id=""){
 
      if(Auth::guard('web')->check()){
          $performers_user    = Auth::user();
          $per_id = $performers_user->id;  
          $authtype = 'performer';
      }else{
          $per_id = $id;
          $performers_user    = Performer::where('id',$per_id)->first();
          $authtype = 'internal-staff';
      }
      $performer           = Performer::where('id',$per_id)->first();

      //$document            = Document::where('doument_type','!=','NA')->get();
      $documentna          = Document::where('doument_type','NA')->get();
      $document            = Document::get();
        $age                 = Carbon::parse($performer->date_of_birth)->age;
        $age_range           = $this->age_range($age);
 
        $sidebar_image       =  MediaPortfolio::where('performers_id',$per_id)
                                ->where('media_type','like','%image%')
                                ->inRandomOrder()
                                ->take(4)
                                ->get();
        $performerFlag = PerformersFlags::with('flag')->where('performer_id',$per_id)->get();   

        $flag       = Flags::all(); 
        $documentlist         = Document::all();
    
     //  $performerdocument    = PerformerDocument::where('performer_id',$per_id)->get();                       

     $performerdocument    = PerformerDocument::where('performer_id',$per_id)->get(); 
     $performersStatus =array();
     $docStatus=array();
foreach ($performerdocument as $key => $value) {
  if($value->document_status=='NA'){
    $docStatus[] = $value->document_id;
  }
  $performersStatus[$value->document_id]  =  PerformerDocument::where('document_id',$value->document_id)->where('performer_id',$per_id)->first();
}
//return $performersStatus; 
        $helpdesk = HelpDesk::find(1);
        return view('performers.documents',compact('performer','documentna','age_range','sidebar_image','performers_user','document','per_id','authtype','performerFlag','flag','documentlist','performerdocument','performersStatus','docStatus','helpdesk'));  
    }
    // Age Range Count Function
    protected  function age_range($age)
    {
        switch ($age) {

            case ($age < 19):
            return "Minor";
            break;
            case ($age >= 20 && $age <= 30):
            return "20 - 30 years";
            break;
            case ($age >= 31 && $age <= 40):
            return "30 - 40 years";
            break;
            case ($age >= 41 && $age <= 50):
            return "40 - 50 years";
            break;    
            case ($age >= 51 && $age <= 60):
            return "50 - 60 years";
            break;    
            case ($age >= 61 && $age <= 70):
            return "60 - 70 years";
            break;    
            case ($age >= 71 && $age <= 80):
            return "70 - 80 years";
            break;    
            case ($age >= 81 && $age <= 90):
            return "80 - 90 years";
            break;
            case ($age >= 91 && $age <= 100):
            return "90 - 100 years";
            break;    
            default:
            return "100 +";
        }
    }
    public function addDocument(Request $request){
        $adddocument = new PerformerDocument();
        $adddocument->document_id=$request->id;
        $adddocument->performer_id=Auth::user()->id;
        $adddocument->save();
        return response()->json(['status'=>true,'message'=>'Document Inserted Successfully']);
    }
    public function removeDocument(Request $request){
        $removedocument = PerformerDocument::where('document_id','=',$request->id)->first();
          $removedocument->delete();
        return response()->json(['status'=>true,'message'=>'Document Removed Successfully']);
    }

    // document edit (in select Yes/No Or NA)
    public function editDocument(Request $request)
    {
        
         $per_id = Auth::id();
         $data = PerformerDocument::updateOrCreate(
        [
          'document_id'      => $request->get('document_id'),
          'performer_id' => $per_id
        ],
        [
          
           'document_status'       => $request->get('document_status'),          
        ]
       );

       return response()->json(['status'=>true,'message'=>'Document Status Added Successfully']);
    }
 
}
