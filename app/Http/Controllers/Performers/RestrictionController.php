<?php

namespace App\Http\Controllers\Performers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\v1\Performer;
use App\Models\v1\Performer\Restriction;
use App\Models\v1\Performer\RestrictionPerformer;
use App\Models\v1\Performer\Nudity;
use App\Models\v1\Performer\Nudity_Performer;
use App\Models\v1\PerformerPortfolio;
use App\Models\v1\MediaPortfolio;
use App\Models\v1\PortfolioGalleryTag;
use Carbon\Carbon;
use Validator;

class RestrictionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id=''){


if(Auth::guard('web')->check()){
          $performers_user    = Auth::user();
          $per_id = $performers_user->id;  
          $authtype = 'performer';
      }else{
          $per_id = $id;
          $performers_user    = Performer::where('id',$per_id)->first();
          $authtype = 'internal-staff';
      }
        //$performers_user       = Auth::user();
        $performer             = Performer::where('id',$performers_user->id)->first();
        $age                   = Carbon::parse($performer->date_of_birth)->age;
        $age_range             = $this->age_range($age);
        $portfolio_list        = PerformerPortfolio::with('performerMedia')->where('performer_id',$performers_user->id)
                                ->get();

        $sidebar_image         =  MediaPortfolio::where('performers_id',$performers_user->id)
                                 ->inRandomOrder()
                                ->take(4)
                                ->get();
        $restriction           =  Restriction::all();                        
        $nudity                =  Nudity::all();

        $restrictionperformer = RestrictionPerformer::where(
        'performer_id', '=',  $per_id)->get();
        
        $nudityperformer = Nudity_Performer::where(
        'performer_id', '=',  $per_id)->get();                        
        return view('performers.restriction',compact('performer','age_range','portfolio_list','sidebar_image','restriction','nudity','restrictionperformer','nudityperformer','performers_user'));  
    }
    // Age Range Count Function
    protected  function age_range($age)
    {
        switch ($age) {

           case ($age < 19):
            return "Minor";
            break;
            case ($age >= 20 && $age <= 30):
            return "20 - 30 years";
            break;
            case ($age >= 31 && $age <= 40):
            return "30 - 40 years";
            break;
            case ($age >= 41 && $age <= 50):
            return "40 - 50 years";
            break;    
            case ($age >= 51 && $age <= 60):
            return "50 - 60 years";
            break;    
            case ($age >= 61 && $age <= 70):
            return "60 - 70 years";
            break;    
            case ($age >= 71 && $age <= 80):
            return "70 - 80 years";
            break;    
            case ($age >= 81 && $age <= 90):
            return "80 - 90 years";
            break;
            case ($age >= 91 && $age <= 100):
            return "90 - 100 years";
            break;    
            default:
            return "100 +";
        }
    }
    public function addrestriction(Request $request){
        /*$RestrictionPerformer=new RestrictionPerformer;
        $RestrictionPerformer->restriction_id=$request->id;
        $RestrictionPerformer->Performer_id=Auth::user()->id;
        $Restriction_Performer->save(); */
    $Restriction_Performer=RestrictionPerformer::where([
        ['restriction_id', '=', $request->id],
        ['performer_id', '=', Auth::user()->id],

    ])->first();
    if($Restriction_Performer){
    $Restriction_Performer->delete();
         return ['result'=>"deleted"];
    }
    else{
       $Restriction_Performer=new RestrictionPerformer;
        $Restriction_Performer->restriction_id=$request->id;
        $Restriction_Performer->Performer_id=Auth::user()->id;
        $Restriction_Performer->save(); 
        return ['result'=>"added"];
    }
           
    }

    
        public function addnudity(Request $request){
            /*$Restriction_Performer=new Restriction_Performer;
            $Restriction_Performer->restriction_id=$request->id;
            $Restriction_Performer->Performer_id=Auth::user()->id;
            $Restriction_Performer->save(); */
        $Nudity_Performer=Nudity_Performer::where([
            ['nudity_id', '=', $request->id],
            ['performer_id', '=', Auth::user()->id],

        ])->first();
        if($Nudity_Performer){
        $Nudity_Performer->delete();
            return ['result'=>"deleted"];
        }
        else{
           $Nudity_Performer=new Nudity_Performer;
            $Nudity_Performer->nudity_id=$request->id;
            $Nudity_Performer->Performer_id=Auth::user()->id;
            $Nudity_Performer->save(); 
             return ['result'=>"added"];
        }
               
        }
    }
