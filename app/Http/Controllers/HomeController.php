<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Models\v1\Performer;
use App\Models\v1\PerformersDetails;

use App\Models\v1\Performer\Eyes;
use App\Models\v1\Performer\HairColor;
use App\Models\v1\Performer\HairLength;
use App\Models\v1\Performer\Skintone;
use App\Models\v1\Performer\Handedness;
use App\Models\v1\Performer\Ethnicity;
use App\Models\v1\Performer\Nationality;
use App\Models\v1\Performer\Appearance;
use App\Models\v1\PerformerPortfolio;
use App\Models\v1\MediaPortfolio;
 use App\Models\v1\PerformerSkill;
use App\Models\v1\PerformerRental;
use App\Models\v1\PerformerCloset;

use App\Models\v1\Workperformer;



use Storage;
use Image;
use File;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300000); //300 seconds = 5 minutes


    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$path = public_path('Posts-Export-4.xlsx');
        //$data = Excel::load($path)->get();
        if ($data->count()) {
            foreach ($data as $key => $valueData) {
                $value = $valueData;
                $firstname = $value->firstname;
                $lastname = $value->lastname;
                $birthdate = $value->birthdate;
                $email = $value->emailprimary;
                $perfomers = Performer::where([
                                                'firstName' => $firstname,
                                                'lastName' => $lastname,
                                                'date_of_birth' => $birthdate,
                                                'email'=> $email ,
                                              ])
                                        ->first();
                
                /*$arr = [
                          'firstName' => $value->firstname, 
                          'middleName' => $value->middlename,
                          'lastName' => $value->lastname,
                          'email' => $value->emailprimary,
                          'password' => bcrypt('1234566788'),
                          'phonenumber' => $value->mobilephone,
                          'address' => $value->address,
                          'city' => $value->city, 
                          'province' => $value->province,
                          'postalCode' => $value->postalcode,
                          'country' => 'Canada',
                          'date_of_birth' => $value->birthdate,
                          'gender' => $value->gender,
                          'applicationfor' => $value->applicationfor,
                          'instagram' => $value->instagram,
                          'twitter' => $value->title, 
                          'linkedin' => $value->linkedin,
                          'facebook' => $value->facebook,
                          'imdb_url' => $value->imdb,
                          'agent_name' => '',
                          'union_name' => '',
                          'union_number' => Null,
                          'agent' => $value->agent,
                          'union_det' => $value->union,
                          'upload_primary_url' => null

                         ];*/
                        // print_r($arr);exit;
                        /*if (!empty($arr)) {
                            $perId = Performer::insertGetId($arr);
                        }*/

                        // if (!empty($perId)) {
                        //  $perfomer  = [
                        //    'hairStyle'=> $value->hairstyle, 
                        //    'hairFacial'=> $value->hairfacial,
                        //    'bgPerformerDetails'=> $value->bgperformerdetails,
                        //    'unionAffiliations'=> $value->unionaffiliations,
                        //    'unionNum'=> $value->unionnum,
                        //    'visa_student'=> $value->visa_student,
                        //    'visa_international'=> $value->visa_international,
                        //    'visa_open'=> $value->visa_open, 
                        //    'visa_film'=> $value->visa_film,
                        //    'visa_film_other'=> $value->visa_film_other,
                        //    'shirt'=> $value->shirt,
                        //    'agentDetails'=> $value->agentdetails,
                        //    'castingTitle'=> $value->castingtitle,
                        //    'job'=> $value->job,
                        //    'cdnCitizenship'=> $value->cdncitizenship, 
                        //    'visa'=> $value->visa,
                        //    'dayPhone'=> $value->dayphone,
                        //    'eveningPhone'=> $value->eveningphone,
                        //    'emailSecondary'=> $value->emailsecondary,
                        //    'applicationFor'=>$value->applicationfor,
                        //    'referral1_name'=> $value->referral1_name,
                        //    'referral1_phone'=> $value->referral1_phone,
                        //    'referral2_name'=> $value->referral2_name,
                        //     'referral2_phone'=> $value->referral2_phone, 
                        //    'emergencyContact1_name'=> $value->emergencycontact1_name,
                        //    'emergencyContact1_phone'=> $value->emergencycontact1_phone,
                        //    'emergencyContact1_email'=> $value->emergencycontact1_email,
                        //    'emergencyContact2_name'=> $value->emergencycontact2_name,
                        //    'emergencyContact2_phone'=> $value->emergencycontact2_phone,
                        //    'emergencyContact2_email'=> $value->emergencycontact2_email,
                        //    'driversLicense'=> $value->driverslicense, 
                        //    'leftHandDrive'=> $value->lefthanddrive,
                        //    'neck'=> $value->neck,
                        //    'chest'=> $value->chest,
                        //    'insea'=> $value->insea,
                        //    'resume'=> $value->resume,
                        //    'bgPerformer'=> $value->bgperformer,
                        //    'availability'=> $value->availability, 
                        //    'cdnCitizen'=> $value->cdncitizen,
                        //    'bcResident'=> $value->bcresident,
                        //    'taxes'=> $value->taxes,
                        //    'sin'=> $value->sin,
                        //    'languageFirst'=>  $value->languagefirst,
                        //    'languageSecond'=> $value->languagesecond,
                        //    'performer_id' =>$perId,

                        //  ];
                        // }
                      //PerformersDetails::insertGetId($perfomer); 
                       // IMAGE UPLOAD
                        // $imageUrl = explode('|', $value->image_url);
                        // $attachmenturl = explode('|', $value->attachment_url);
                        // $images = array_filter(array_merge($imageUrl,$attachmenturl));
                        // if (count($images)>0) {
                        //     $performerPortfolio = PerformerPortfolio::create([
                        //                                                 'portfolio_title' => 'MISC',
                        //                                                 'description'     => null,
                        //                                                 'performer_id'    => $perId,
                        //                                                 'is_video'        => 'no'      
                        //                                             ]);
                        //     foreach ($images as $index => $image) {
                        //         $url = $image;
                        //         //$contents = file_get_contents($url);
                        //         $name = substr($url, strrpos($url, '/') + 1);
                        //         $mediaType = explode('.', $name);

                        //         $img = Image::make($contents);
                        //         $height = $img->height();
                        //         $width = $img->width();

                        //         if ($height > 800) {
                        //           $img->resize(1000, null, function ($constraint) {
                        //            $constraint->aspectRatio();
                        //          });
                        //          }
                        //          storage_path('app/public'),
                        //           $img->save('public/performers/'.$perId.'/'.$name);

                        //         //Storage::put('public/performers/'.$perId.'/'.$name, $contents);
                                
                        //         $perfomer = Performer::where('id',$perId)->first();
                        //         if ($perfomer->upload_primary_url == null && empty($perfomer->upload_primary_url)) {
                        //             $perfomer->upload_primary_url = $url;
                        //             $perfomer->save();
                                 
                        //         } else {
                        //             MediaPortfolio::create([
                        //                                         'media_url'     => $url,
                        //                                         'portfolio_id'  => $performerPortfolio->id,
                        //                                         'media_title'   => null,
                        //                                         'media_type'    => end($mediaType),
                        //                                         'performers_id' => $perId
                        //                                     ]);
                        //         }
                        //     }
                        //     //exit();
                        // }

                                               

                        $ethnicity = $value->ethnicity;
                        if (!empty($ethnicity)) {
                            $eth = Ethnicity::where('ethnicity',$ethnicity)->first();
                            if(empty($eth)){
                                $dataeth['ethnicity'] = $ethnicity;
                                $ethId =  Ethnicity::insertGetId($dataeth);
                            }else{
                                $ethId =$eth->id;                        
                            }
                        } else {
                            $ethId = null;
                        }
                        $appearance['ethnicity_id'] = $ethId;

                        $skintone = $value->skintone;
                        if (!empty($skintone)) {
                            $Skint = Skintone::where('skin_tone',$skintone)->first();
                            if(empty($Skint)){
                                $dataskin['skin_tone'] = $skintone;
                                $skId =  Skintone::insertGetId($dataskin);
                            }else{
                                $skId =$Skint->id;                        
                            }
                        } else {
                            $skId = null;   
                        }
                        $appearance['skin_tone_id'] = $skId;



                        $haircolour = $value->haircolour;
                        if (!empty($haircolour)) {
                            $hair = HairColor::where('hair_color',$haircolour)->first();
                            if(empty($hair)){
                                $dataHair['hair_color'] = $haircolour;
                                $hairId =  HairColor::insertGetId($dataHair);
                            }else{
                                $hairId =$hair->id;                        
                            }
                        } else {
                            $hairId = null;                        
                        }
                        $appearance['hair_color_id'] = $hairId;


                        $eyecolour = $value->eyecolour;
                        if (!empty($eyecolour)) {
                            $eye = Eyes::where('eyes',$eyecolour)->first();
                            if(empty($eye)){
                                $dataEye['eyes'] = $eyecolour;
                                $eyesId =  Eyes::insertGetId($dataEye);
                            }else{
                                $eyesId = $eye->id;                        
                            }
                        } else {
                            $eyesId = null;                        
                        }
                        $appearance['eyes_id'] = $eyesId;


                        $handedness = $value->handed;
                        if (!empty($handedness)) {
                            $handed = Handedness::where('handedness',$handedness)->first();
                            if(empty($handed)){
                                $dataHanded['handedness'] = $handedness;
                                $handedId =  Handedness::insertGetId($dataHanded);
                            }else{
                                $handedId =$handed->id;                        
                            }
                        } else {
                            $handedId = null;                        
                        }
                        $appearance['handedness_id'] = $handedId;


                        $nationality = $value->nationality;
                        if (!empty($nationality)) {
                            $nationalityData = Nationality::where('nationality',$nationality)->first();
                            if(empty($nationalityData)){
                                $dataNationality['nationality'] = $handedness;
                                $nationalityId =  Nationality::insertGetId($dataNationality);
                            }else{
                                $nationalityId =$nationalityData->id;                        
                            }
                        } else {
                            $nationalityId =null;                        
                        }
                        $appearance['nationality_id'] = $nationalityId;

                        


                        $appearance['height'] = $value->height;
                        $appearance['weight'] = $value->weight;
                        $appearance['bust'] = $value->bust;
                        $appearance['waist'] = $value->waist;
                        $appearance['hips'] = $value->hip;
                        $appearance['dress'] = $value->dress;
                        $appearance['jeans'] = $value->jean;
                        $appearance['shoes'] = $value->shoe;
                        $appearance['contacts'] = $value->contacts;



                        $hairlength = $value->hairlength;
                        if (!empty($hairlength)) {
                            $hairlengthData = HairLength::where('hair_length',$hairlength)->first();
                            if(empty($hairlengthData)){
                                $dataHairLength['hair_length'] = $hairlength;
                                $hairlengthId =  HairLength::insertGetId($dataHairLength);
                            }else{
                                $hairlengthId = $hairlengthData->id;                        
                            }
                        } else {
                            $hairlengthId = null;                        
                        }
                        $appearance['hair_length_id'] = $hairlengthId;


                        $appearance['gloves']  = $value->glove;
                        $appearance['glasses'] = $value->glasses;
                        $appearance['gender']  = $value->gender;
                        $appearance['tattoos'] = $value->tattoos;
                        $appearance['collar']  = $value->collar;
                        $appearance['jacket']  = $value->jacket;
                        $appearance['sleeve']  = $value->sleeve;
                        $appearance['pant']    = $value->pant;
                        $appearance['hat']     = $value->hat;
                        $appearance['ring']    = $value->ring;
                        $appearance['size']    = $value->size;
                        $appearance['vehicle'] = $value->vehicle;
                        $appearance['pets']    = $value->pets;
                        if ($perfomers) {
                            $appearance['performer_id']  = $perfomers->id;
                            Appearance::insertGetId($appearance);
                        }
                        //sleep(2);
                        // exit;
            }
          

        }
        //return view('home');
        return response()->json([
                      'status'=>true,
                      'message'=>'performers has been successfully created'
                    ]);
    }


    public function exportImage12(){
        $mediaPortfolio = MediaPortfolio::select('id','media_url','performers_id')
                            ->where('media_url', 'like', 'http%')
                            ->get();

        foreach ($mediaPortfolio as $key => $value) {
            $perId = $value->performers_id;
            $url = $value->media_url;
            if (!file_exists(public_path('storage/performers/'.$perId))) {
              File::makeDirectory(public_path('storage/performers/'.$perId));
            }
            $name = substr($url, strrpos($url, '/') + 1);
            $mediaType = explode('.', $name);
            // echo end($mediaType);
            
            if(end($mediaType)=="docx" || end($mediaType)=="doc" || end($mediaType)=="tif" || end($mediaType)=="bmp" || end($mediaType)=="pdf" || end($mediaType)=="rtf" || end($mediaType)=="pages" || end($mediaType)=="mp4" || end($mediaType)=="mov" || $perId =="792" || $perId =="2044" || end($mediaType) =="odt" || end($mediaType) =="zip"){
                // $contents = file_get_contents($url);
                // Storage::put('public/performers/'.$perId.'/'.$name, $contents);
            } else {
                // print_r($perId);
                // print_r($url);
                // print_r('<br>');
                // print_r(end($mediaType));
                // exit();
                $mediaType = explode('.', $name);

                $img = Image::make($url)->resize(null, 1000, function($constraint) {
                    $constraint->aspectRatio();
                });

                $img->save(public_path('storage/performers/'.$perId.'/'.$name));
                $value->media_url = $name;
                $value->save();
            }
        }

        return response()->json([
                      'status'=>true,
                      'message'=>'Export image successfully'
                    ]);
    }
    public function exportImage15(){
        $perfomers = Performer::select('id','upload_primary_url')
                            ->where('upload_primary_url', 'like', 'http%')
                            /*->with(['media_portfolio'=>function($query){
                                $query->where('media_url', 'like', 'http%')->select('id','performers_id','media_url');
                            }])*/
                            ->get();

        foreach ($perfomers as $key => $value) {
            $perId = $value->id;
            $url = $value->upload_primary_url;
            if (!file_exists(public_path('storage/performers/'.$perId))) {
              File::makeDirectory(public_path('storage/performers/'.$perId));
            }
            $name = substr($url, strrpos($url, '/') + 1);
            $mediaType = explode('.', $name);
            // echo end($mediaType);
            
            if(end($mediaType)=="docx" || end($mediaType)=="doc" || end($mediaType)=="tif" || end($mediaType)=="bmp" || $perId == '792'){
                // $contents = file_get_contents($url);
                // Storage::put('public/performers/'.$perId.'/'.$name, $contents);
            } else {
                // print_r($url);
                // print_r('<br>');
                // print_r(end($mediaType));
                // exit();
                $mediaType = explode('.', $name);

                $img = Image::make($url)->resize(null, 1000, function($constraint) {
                    $constraint->aspectRatio();
                });

                $img->save(public_path('storage/performers/'.$perId.'/'.$name));
                $value->upload_primary_url = $name;
                $value->save();
            }
            
            /*foreach ($value->media_portfolio as $media_portfolio) {
                $mediaUrl = $media_portfolio->media_url;
                $mediaName = substr($mediaUrl, strrpos($mediaUrl, '/') + 1);

                $mediaImage = Image::make($mediaUrl)->resize(null, 1000, function($constraint) {
                    $constraint->aspectRatio();
                });
                $mediaImage->save(public_path('storage/performers/'.$perId.'/'.$mediaName));
            }*/
        }

        return response()->json([
                      'status'=>true,
                      'message'=>'Export image successfully'
                    ]);
    }


    public function ageUpdate(){
       
       $performers = Performer::get();
       foreach ($performers as $key => $performer) {

         $performerPortfolio = array(
            array('portfolio_title'=>'Tattoos','performer_id'=> $performer->id),
            array('portfolio_title'=>'Piercings','performer_id'=> $performer->id),
         );
         $gallaryPortFolio = PerformerPortfolio::insert($performerPortfolio);   

        
         //default skills 
         $PerformerSkill = array(
            
            array('skill_title'=>'Community Servants','performer_id'=>$performer->id),            
            array('skill_title'=>'Circus/Magic','performer_id'=>$performer->id),
            array('skill_title'=>'Culinary','performer_id'=>$performer->id),
            array('skill_title'=>'Dance','performer_id'=>$performer->id),
            array('skill_title'=>'Military','performer_id'=>$performer->id),            
            array('skill_title'=>'Bands','performer_id'=>$performer->id),

         );
           PerformerSkill::insert($PerformerSkill);

          
         //blank CLOSET gallary create
         $performerCloset = array(
            //array('closet_title'=>'Spring','performer_id'=> $performer->id),            
            array('closet_title'=>'Fall','performer_id'=> $performer->id),            
        );
       $gallaryCloset = PerformerCloset::insert($performerCloset);

       $performerProps = array(
           array('rental_title'=>'My Props','performer_id'=> $performer->id),            
            array('rental_title'=>'My Musical Instruments','performer_id'=> $performer->id),
        );
        $gallaryProps = PerformerRental::insert($performerProps);
      }
} 
      

}
