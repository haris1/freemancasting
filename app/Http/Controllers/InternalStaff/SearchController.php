<?php

namespace App\Http\Controllers\InternalStaff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\Performer;
use App\Models\v1\Performer\Appearance; 
use Auth;
use App\Models\v1\performersDetails;
use App\Models\v1\InternalStaff;
use App\Models\v1\Performer\Ethnicity;
use App\Models\v1\Performer\Region;
use App\Models\v1\Performer\PerformerStatus;
use App\Models\v1\Performer\Skintone;
use App\Models\v1\Performer\HairColor;
use App\Models\v1\Performer\HairFacial;
use App\Models\v1\Performer\HairStyle;
use App\Models\v1\Performer\BodyTrait;
use App\Models\v1\Performer\Eyes;
use App\Models\v1\Performer\Nationality;
use App\Models\v1\Performer\Handedness;
use App\Models\v1\Performer\VehicleType;
use App\Models\v1\Performer\VehicleAccessory;
use App\Models\v1\Performer\VehicleMake;
use App\Models\v1\Performer\Pet;
use App\Models\v1\Performer\Restriction;
use App\Models\v1\Performer\Nudity;
use App\Models\v1\Performer\PerformerAvailability;
use App\Models\v1\Performer\PerformerUnion;
use App\Models\v1\PerformerPortfolio;
use App\Models\v1\MediaPortfolio;
use App\Models\v1\PerformerSearchresults;
use Validator;
use Hash;
use Carbon\Carbon;
use DB;
class SearchController extends Controller
{

	// Search result in internal-staff 
  	public function advanceSearch(Request $request)
	{
		$message ="";
		$tablist =[];
		$lastId = $request->get('lastId');
		if(empty($lastId))
		{
		 	$lastId = 0;
		}

		if($request->min_age <= 0)
		{
			$request->min_age =1;
		}
		
	
		
		$view = $request->input('viewformat');
		$performer=Performer::with('PerformerCloset','PerformerSkill.skillFields')->whereNotNull('email');
		

		if(!empty($request->get('search'))){
			$performer->where(function($query) use ($request){
			$query->orWhereRaw("CONCAT(`firstName`, ' ', `lastName`) LIKE ?", ['%'.$request->get("search").'%'])->get();	

			$query->orWhereHas('PerformerSkill.skillFields',function($query1){
				$query1->where('field_name','LIKE','%'.request('search').'%');
				$query1->orWhere('field_value','LIKE','%'.request('search').'%');
			});	

			$query->orWhereHas('Workperformer.project_performer.project_role',function($query2){
				$query2->where('project_title','LIKE','%'.request('search')."%");
				$query2->orWhere('role_title','LIKE','%'.request('search')."%");
			});	


			$query->orWhereHas('groupPerformers',function($query3){
				$query3->where('group_name','LIKE','%'.request('search')."%");
			});	

			$query->orWhereHas('PerformerPortfolio.portfolioTag',function($query4){
				$query4->where('tag','LIKE','%'.request('search')."%");
			});

			$query->orWhereHas('PerformerCloset.closetTag',function($query5){
				$query5->where('tag','LIKE','%'.request('search')."%");
			});

			$query->orWhereHas('performerrental.rentalTag',function($query6){
				$query6->where('tag','LIKE','%'.request('search')."%");
			});	


			});			    

		}
		if(!empty($request->get('gender'))){
			$tablist['#gender']='Gender';
			$performer->where('gender',$request->get("gender"));
		}
		if(!empty($request->get('status_id'))){
			$tablist['#performancestatus']='Status';
			$performer->where('status_id',$request->get("status_id"));
		}
		if(!empty($request->get('applicationfor'))){
			 $tablist['#Performancecategory']='Performer Category';		 
			$performer->where('applicationfor',$request->get("applicationfor"));
		}		
		if(!empty($request->get('union'))){
			$tablist['#union2'] = "Union";
			$performer->where('union_name',$request->get("union"));
		}
		if(!empty($request->get('ethnicity'))){
			$tablist['#ethnicity']='Ethnicity';
			$performer->whereHas('appearance',function($query){
				$query->where('ethnicity_id',request('ethnicity'));
			});
		}
		if(!empty($request->get('region'))){
			$performer->whereHas('appearance',function($query){
				$query->where('region_id',request('region'));
			});
		}
		if(!empty($request->get('availability'))){
				$performer->where('availability',$request->get('availability'));			
		}		
		if(!empty($request->get('skintone'))){
			$tablist['#skintone']='Skintone';
			$performer->whereHas('appearance',function($query){				
				$query->where('skin_tone_id',request('skintone'));
			});
		}	
		if(!empty($request->get('hairlength'))){
			$tablist['#hairlength']='Hairlength';
			$performer->whereHas('appearance',function($query){				
				$query->where('hair_length_id',request('hairlength'));
			});
		}
		if(!empty($request->get('hairstyle'))){
			$tablist['#hairstyle']='HairStyle';
			$performer->whereHas('appearance',function($query){				
				$query->where('hair_style_id',request('hairstyle'));
			});
		}
		if(!empty($request->get('hairfacial'))){
			$tablist['#hairfacial']='HairFacial';
			$performer->whereHas('appearance',function($query){				
				$query->where('facial_hair_id',request('hairfacial'));
			});
		}
		if(!empty($request->get('bodytrait'))){
			$tablist['#bodytrait']='BodyTrait';
			$performer->whereHas('appearance',function($query){				
				$query->where('body_trait_id',request('bodytrait'));
			});
		}
		if(!empty($request->get('haircolor'))){
			$tablist['#haircolor']='Haircolor';
			$performer->whereHas('appearance',function($query){
				$query->where('hair_color_id',request('haircolor'));
			});
		}
		if(!empty($request->get('eyes'))){
			$tablist['#eyes']='Eyes';
			$performer->whereHas('appearance',function($query){
				$query->where('eyes_id',request('eyes'));
			});
		}
		
		if(!empty($request->get('agency_id'))){
			$tablist['#agency']='Agency';
			$performer->whereHas('agentDetail',function($query){
				$query->where('agency_id',request('agency_id'));
			});
		}
		if(!empty($request->get('nationality'))){
			$tablist['#nationality']='Nationality';
			$performer->whereHas('appearance',function($query){
				$query->where('nationality_id',request('nationality'));
			});
		}
		if(!empty($request->get('handedness'))){
			$tablist['#handedness']='Handedness';
			$performer->whereHas('appearance',function($query){
				$query->where('handedness_id',request('handedness'));
			});
		}
		if(!empty($request->get('tattoos'))){
			$tablist['#tattoos']='Tattoos';
			$performer->whereHas('appearance',function($query){
				$query->where('tattoos',request('tattoos'));
			});
		}
		if(!empty($request->get('piercing'))){
			$tablist['#piercing']='Piercing';
			$performer->whereHas('appearance',function($query){
				$query->where('piercing','LIKE','%'.request('piercing').'%');
			});
		}
		if(!empty($request->get('gloves'))){
			$tablist['#gloves']='Gloves';
			$performer->whereHas('appearance',function($query){
				$query->where('gloves',request('gloves'));
			});
		}
		if(!empty($request->get('height_min')) && !empty($request->get('height_max')))
		{	
			$tablist['.heightclr']='Height';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('height',array(request('height_min'),request('height_max')));
			});
		}
		if(!empty($request->get('weight_min')) && !empty($request->get('weight_max')))
		{		
			$tablist['.weightclr']='Weight';			
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('weight',array(request('weight_min'),request('weight_max')));
			});
		}
		if(!empty($request->get('chest_min')) && !empty($request->get('chest_max')))
		{		
			$tablist['.chestclr']='chest';			
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('chest',array(request('chest_min'),request('chest_max')));
			});
		}
		if(!empty($request->min_age) && !empty($request->get('max_age')))
		{	
			$tablist['.ageclr']='Age';   
			$performer->whereBetween('age',[$request->min_age,$request->get("max_age")]);
			
		}
		if(!empty($request->get('bust_min')) && !empty($request->get('bust_max')))
		{		
			$tablist['.bustclr']='Bust';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('bust',array(request('bust_min'),request('bust_max')));
			});
		}
		if(!empty($request->get('shoes_min')) && !empty($request->get('shoes_max')))
		{		
			$tablist['.shoesclr']='Shoes';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('shoes',array(request('shoes_min'),request('shoes_max')));
			});
		}
		if(!empty($request->get('waist_min')) && !empty($request->get('waist_max')))
		{		
			$tablist['.waistclr']='Waist';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('waist',array(request('waist_min'),request('waist_max')));
			});
		}
		if(!empty($request->get('hip_min')) && !empty($request->get('hip_max')))
		{		
			$tablist['.hipsclr']='Hips';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('hips',array(request('hip_min'),request('hip_max')));
			});
		}
		if(!empty($request->get('sleeve_min')) && !empty($request->get('sleeve_max')))
		{		
			$tablist['.sleeveclr']='Sleeve';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('sleeve',array(request('sleeve_min'),request('sleeve_max')));
			});
		}
		if(!empty($request->get('pant_min')) && !empty($request->get('pant_max')))
		{		
			$tablist['.pantclr']='Pant';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('pant',array(request('pant_min'),request('pant_max')));
			});
		}
		if(!empty($request->get('ring_min')) && !empty($request->get('ring_max')))
		{		
			$tablist['.ringclr']='Ring';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('ring',array(request('ring_min'),request('ring_max')));
			});
		}
		if(!empty($request->get('hat_min')) && !empty($request->get('hat_max')))
		{		
			$tablist['.hatclr']='Hat';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('hat',array(request('hat_min'),request('hat_max')));
			});
		}
		if(!empty($request->get('dress_min')) && !empty($request->get('dress_max')))
		{		
			$tablist['.dressclr']='Dress';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('dress',array(request('dress_min'),request('dress_max')));
			});
		}
		if(!empty($request->get('jeansize_min')) && !empty($request->get('jeansize_max')))
		{		
			$tablist['.jeansclr']='Jeans';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('jeans',array(request('jeansize_min'),request('jeansize_max')));
			});
		}
		if(!empty($request->get('inseam_min')) && !empty($request->get('inseam_max')))
		{		
			$tablist['.inseamclr']='Inseam';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('inseam',array(request('inseam_min'),request('inseam_max')));
			});
		}
		if(!empty($request->get('collar_min')) && !empty($request->get('collar_max')))
		{		
			$tablist['.collarclr']='Collar';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('collar',array(request('collar_min'),request('collar_max')));
			});
		}
		if(!empty($request->get('jacket_min')) && !empty($request->get('jacket_max')))
		{		
			$tablist['.jacketclr']='Jacket';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('jacket',array(request('jacket_min'),request('jacket_max')));
			});
		}
		if(!empty($request->get('child_size_min')) && !empty($request->get('child_size_max')))
		{		
			$tablist['.childsizeclr']='Child size';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('child_size',array(request('child_size_min'),request('child_size_max')));
			});
		}	

		if(!empty($request->get('infant_size_min')) && !empty($request->get('infant_size_max')))
		{		
			$tablist['.infantsizeclr']='Infant size';
			$performer->whereHas('appearance',function($query){
				$query->whereBetween('infant_size',array(request('infant_size_min'),request('infant_size_max')));
			});
		}		  
		if(!empty($request->get('restriction_id'))){
			
			$performer->whereHas('restriction',function($query){
				$query->whereIn('restriction_id',[request('restriction_id')]);
			});
		}		  
		if(!empty($request->get('nudity_id'))){
			
			$performer->whereHas('nudity',function($query){
				$query->whereIn('nudity_id',[request('nudity_id')]);
			});
		}


		if($request->closetGallery){
		
		$array1 =["Winter","Spring","Summer","Fall"];
 		$array2 =$request->closetGallery;
		$resultarray = array_intersect($array1, $array2);
		}

		if(isset($resultarray[0]))
		{
			$winter =$resultarray[0];			
			$performer->withCount('performerCloset')
			->whereHas('performerCloset',function($query) use ($winter){ 
				$query->where('closet_title',$winter)->whereHas('performerMedia');
			});
			
		}if(isset($resultarray[1]))
		{
			$spring =$resultarray[1];			
			$performer->withCount('performerCloset')
			->whereHas('performerCloset',function($query) use ($spring){ 
				$query->where('closet_title',$spring)->whereHas('performerMedia');
			});
			
		}if(isset($resultarray[2])){
			$Summer =$resultarray[2];			
			$performer->withCount('performerCloset')
			->whereHas('performerCloset',function($query) use ($Summer){ 
				$query->where('closet_title',$Summer)->whereHas('performerMedia');
			});
			
		}if(isset($resultarray[3])){
			$Fall =$resultarray[3];			
			$performer->withCount('performerCloset')
			->whereHas('performerCloset',function($query) use ($Fall){ 
				$query->where('closet_title',$Fall)->whereHas('performerMedia');
			});			
		}



		

		$performer = $performer->where('id', '>', $lastId)
								->limit(40)
								->get();
								

		

		$output ='';
		if(count($performer) > 0)
		{	
			
			foreach ($performer as $value)
			 {
			 
			 	$height = "";
			 	$weight = "";
			 	$flagcount=0;
			 	$union_TAG="-";
			 	$agency="";
			 	if(isset($value->agentDetail->agencies->agencyAbbreviation))
			 	{
			 		$agency =  $value->agentDetail->agencies->agencyAbbreviation;
			 	}
			 	if(isset($value->unionDetail->union_tag))
			 	{
			 		$union_TAG = $value->unionDetail->union_tag;
			 	}
			 	if(isset($value->performerFlag))
			 	{
			 		$flagcount=count($value->performerFlag);
			 	}
			 	if(isset($value->appearance[0]->height))
			 	{

			 		$height = preg_replace(array('/\bfeet\b/','/\bft\b/','/\bf\b/','/\binches\b/','/\bin\b/'),array("'","'","'",'"','"','"'), $value->appearance[0]->height);

			 		str_replace('ft',"'",$height);
			 		if(is_numeric($height))
			 		{
			 			$height =  round(floatval($height / 30.48),2);
			 		}else{
			 			$height = $height;
			 		}
			 		if(strlen($height) < 7){$height =$height;}else{$height ="";}

			 	}
			 	if(isset($value->appearance[0]->weight))
			 	{
			 		$weight =str_replace("lbs","",$value->appearance[0]->weight) .' lbs';
			 	}

			 	if(isset($value->gender)){
                   $gender_ch = ($value->gender=="male") ? 'M' : (($value->gender=="female") ? 'F' : 'T');}else{ $gender_ch ="-";}

			 	if($request->input('viewformat') == "grid")
			 	{
                	
				$output .= '<li>';
				$output .=    '<a class="showPerformersDetail" href="javascript:void(0)"  id="'.$value->id.'">';
				$output .=        '<div class="fltsr_userimg">';
				$output .=            '<img src="'.$value->image_url.'" alt="">';
				$output .=        '</div>' ;
				$output .=        '<div class="fltsr_userdits2">';
				$output .=            '<h4>'.$value->full_name.'</h4>';
				$output .=            '<div class="hwvbox">';
				$output .=            '<p>';
				$output .=            '<span>'.$value->age.''.$gender_ch.'</span>';
				$output .=            '<span>'.$height.'</span>';
				$output .=            '<span>'.$weight.'</span>';
				$output .=            '</p>';
				$output .=            '</div>';
				$output .=            '<div class="flggrnbox">';
				$output .=                '<div class="flggrnbox_left">';
				$output .=                    '<p>'.($flagcount >= 1 ? $flagcount : 0 ).'</p>';
				$output .=                    '<i class="far fa-flag-alt"></i>';
				$output .=                 '</div>';
				$output .=                '<div class="flggrnbox_right">';
				$output .=                    '<p>'.$union_TAG.'- '.$value->union_numbe.'</p> <p>'.$agency.'</p>';
				$output .=                '</div>';           
				$output .=            '</div>';
				$output .=        '</div>';
				$output .=    '</a>';
				$output .= '</li>';              
			
				}
				else{



					$output .=    '<a class="showPerformersDetail" href="javascript:void(0)"  id="'.$value->id.'">';
					$output .=	'<div class="fremanctlistbox">';
					$output .=	'<div class="fremanctlistiner1">';
					$output .='<img src="'.$value->image_url.'" alt="">';
					$output .='</div>';


					$output .='<div class="fremanctlistiner2">';
					$output .='<h4>'.$value->full_name.'</h4>';
					$output .= '<p>';
					$output .= '<span class="fagesp">'.$value->age.''.$gender_ch.'</span> ';
					$output .= '<span class="fagesp">'.$height.'</span> ';
					$output .= '<span class="fagesp">'.$weight.'</span>';
					$output .= '</p>';
					$output .= '</div>';
					$output .=	'<div class="fremanctlistiner3">';

					$output .=	'<p></p>';
					$output .=	'</div>';
					$output .=	'<div class="fremanctlistiner4">';
					$output .=	'<h6>Profile last updated</h6>';
					$output .=	'<p>'.$value->updated_at.'</p>';
					$output .=	'</div>';
					$output .=	'<div class="fremanctlistiner5">';				
					$output .=	'<div class="flggrnbox_left1">';
					$output .=	'<p>'.($flagcount >= 1 ? $flagcount : 0 ).'</p>';
					$output .=	'<i class="far fa-flag-alt"></i>';
					$output .=	'</div>';
					$output .=	'<div class="flggrnbox_right1">';
					$output .=	'<p>'.$union_TAG.'-'.$value->union_numbe.'</p> <p>'.$agency.'</p>';
					$output .=	'</div>';
					$output .=	'</div>';
					$output .=	'</a>';				
					$output .=	'<div class="fremanctlistiner6">';
					$output .=	'<a href="'.route('internal-staff.portfolio', ['id' => $value->id]).'">View Profile</a>';
					$output .=	'</div>';
					$output .=	'</div>';

				}		

   			 }
   			 $message='success';
		}
		else{
			if($lastId==''){
				$message='error';
			$output .="<div>
				<h3>No Result Found </h3>
			</div>";		
			}	
		 }
    	

	 return response()->json(['status'=>true,'message'=>$message,'data'=>$output,'tag'=>$tablist]);
    	return $output;
	}


	 public function searchResultSave(Request $request)
    {
 	  
  	 
  	 if(!empty($request->gender)){$data['gender'] 				 = $request->gender;}
  	 if(!empty($request->availability)){$data['availability'] 	 = $request->availability;}
  	 if(!empty($request->ethnicity)){$data['ethnicity'] 		 = $request->ethnicity;}
  	 if(!empty($request->region)){$data['region'] 				 = $request->region;}
  	 if(!empty($request->region)){$data['union'] 				 = $request->union;}
  	 if(!empty($request->age_minval)){$data['age']['age_minval'] = $request->age_minval;}
  	 if(!empty($request->age_maxval)){$data['age']['age_maxval'] = $request->age_maxval;}
     
     parse_str($request->AdvanceformData,$data1); 
     
     $result= (array_merge(array_filter($data1),array_filter($data)));
	 parse_str($request->formData);       
     $saveRecord['my_performer']     =  $my_performer;
     $saveRecord['internalstaff_id'] = Auth::user()->id; 
     $saveRecord['search_parameter'] =serialize($result); 
 
      $data  = new PerformerSearchresults($saveRecord);
      $data->save();

      return response()->json(['status'=>true,'message'=>"Save Result Successfully"]); 

    }

    //search display save result
	public function SearchResultDisplay($id)
	{
		$res = PerformerSearchresults::where('id',$id)->first();
		$search_parameter =unserialize($res->search_parameter);
 
 	 	//return $search_parameter;

		$activeclass             = 'perfomer';
        $Ethnicity         = Ethnicity::all();
        $Region            = Region::all();
        //$status            = PerformerStatus::all();
        $skintone          = Skintone::all();
        $haircolor         = HairColor::all();
        $hairfacial        = HairFacial::all();
        $hairstyle         = HairStyle::all();
        $bodytrait         = BodyTrait::all(); 
        $eyes              = Eyes::all(); 
        $nationality       = Nationality::all(); 
        $handedness        = Handedness::all(); 
        $vehicletype       = VehicleType::all(); 
        $vehicleaccessory  = VehicleAccessory::all(); 
        $vehiclemake       = VehicleMake::all(); 
        $pet               = Pet::all(); 
        $performer         = Performer::limit(40)->get();
        $restriction       = Restriction::all();                        
        $nudity            = Nudity::all();
        $perfomerstatus    = PerformerStatus::all();
        $performeravailability = PerformerAvailability::all(); 
        $performerunion    = PerformerUnion::all();

        return view('internal-staff.perfomerslist',compact('Ethnicity','Region','skintone','haircolor','hairfacial','bodytrait','hairstyle','eyes','nationality','handedness','vehicletype','vehicleaccessory','vehiclemake','pet','performer','restriction','nudity','perfomerstatus','performeravailability','performerunion','activeclass','search_parameter')); 
	}
}
