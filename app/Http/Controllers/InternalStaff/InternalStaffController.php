<?php

namespace App\Http\Controllers\InternalStaff;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Auth;

use App\Models\v1\Performer;
use App\Models\v1\PerformersDetails;
use App\Models\v1\InternalStaff;
use App\Models\v1\Note;

use App\Models\v1\Performer\Ethnicity;
use App\Models\v1\Performer\Region;
use App\Models\v1\Performer\PerformerStatus;
use App\Models\v1\Performer\Skintone;
use App\Models\v1\Performer\HairColor;
use App\Models\v1\Performer\HairFacial;
use App\Models\v1\Performer\HairLength;
use App\Models\v1\Performer\HairStyle;
use App\Models\v1\Performer\BodyTrait;
use App\Models\v1\Performer\Eyes;
use App\Models\v1\Performer\Nationality;
use App\Models\v1\Performer\Handedness;
use App\Models\v1\Performer\VehicleType;
use App\Models\v1\Performer\VehicleAccessory;
use App\Models\v1\Performer\VehicleMake;
use App\Models\v1\Performer\Piercing;
use App\Models\v1\Performer\Pet;
use App\Models\v1\Performer\Restriction;
use App\Models\v1\Performer\Nudity;
use App\Models\v1\Performer\Flags;
use App\Models\v1\Agency;

use App\Models\v1\PerformerAgent;
use App\Models\v1\Performer\PerformersFlags;
use App\Models\v1\Performer\PerformerAvailability;
use App\Models\v1\Performer\PerformerUnion;
use App\Models\v1\MediaPortfolio;
use App\Models\v1\PerformerSearchresults;
use Validator;
use Hash;
use Datetime;
use Carbon\Carbon;
use App\Models\v1\MediaRental;
use App\Models\v1\PerformerRental;
use App\Models\v1\Performer\RestrictionPerformer;
use App\Models\v1\Performer\Nudity_Performer;
use DB;
class InternalStaffController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        //return view('internal-staff.index');
        if (Auth::guard('internal-staff')->check()) {
            return view('internal-staff.index');
        } else {
            return redirect()->route('internal-staff.login');
        }
    }

    //perfomer List
    public function performersList()
    {

       
        $activeclass             = 'perfomer';
        $Ethnicity         = Ethnicity::all();
        $Region            = Region::all();
        //$status            = PerformerStatus::all();
        $skintone          = Skintone::all();
        $haircolor         = HairColor::all();
        $hairfacial        = HairFacial::all();
        $hairstyle         = HairStyle::all();
        $bodytrait         = BodyTrait::all(); 
        $eyes              = Eyes::all(); 
        $nationality       = Nationality::all(); 
        $handedness        = Handedness::all(); 
        $vehicletype       = VehicleType::all(); 
        $vehicleaccessory  = VehicleAccessory::all(); 
        $vehiclemake       = VehicleMake::all(); 
        $pet               = Pet::all(); 
        $performer         = Performer::with('appearance','performerFlag','unionDetail')->limit(40)->get();   
        $restriction       = Restriction::all();                        
        $nudity            = Nudity::all();
        $perfomerstatus    = PerformerStatus::all();
        $performeravailability = PerformerAvailability::all(); 
        $performerunion    = PerformerUnion::all();
        $hairLength        = HairLength::all();
        $agent             = PerformerAgent::all();
        $agency            = Agency::all();
        $piercing          = Piercing::all(); 
        $is_show   = 1;

        return view('internal-staff.perfomerslist',compact('Ethnicity','Region','skintone','haircolor','hairfacial','bodytrait','hairstyle','eyes','nationality','handedness','vehicletype','vehicleaccessory','vehiclemake','pet','performer','restriction','nudity','hairLength','perfomerstatus','performeravailability','performerunion','activeclass','is_show','agent','agency','piercing'));   
        
    }

    //Performer Detail
    public function performersDetail(Request $request)
    {
    
      $flag              = Flags::all();
      $nudity            = Nudity::all();
      $restriction       = Restriction::all();
      $performer         = Performer::with('appearance.eyes','appearance.hairlength','appearance.haircolor','appearance.skintone','appearance.nationality','appearance.ethnicity','appearance.handedness','restriction','performerrental','restriction.restrictionName','nudity.nudityName')->where('id',$request->get('id'))->first();

      $per_id = $request->id;
      $performer_profile = Performer::where('id',$per_id)->get()->pluck('image_url');
      $sidebar_image = MediaPortfolio::select('*')->where('media_type','like','%image%')->where('performers_id',$per_id)->get()->pluck('image_url');
      $media_performer = $performer_profile->merge($sidebar_image);



       $html = view('internal-staff.detailModal',compact('performer','restriction','nudity','media_performer','flag'))->render();
       return response()->json(['status'=>true,'data'=>$media_performer,'message'=>'','html'=>$html]);

       
    }

    public function saveFlag(Request $request)
    {
    
      $validator = Validator::make($request->all(),[  
           'flag_id'                   =>  'required',          
           
       ]);

       if($validator->fails()){
           return response()->json(['status'=>false,'message'=>'Select Flag']);
       }

       $flag = $request->all();
       $flag['internal_staff_id'] =Auth::id();

       $data = PerformersFlags::updateOrCreate(
        [
          'flag_id'      => $request->get('flag_id'),
          'performer_id' => $request->get('performer_id')
        ],
        [
          
           'note'       => $request->get('note'),
           'internal_staff_id'       => $flag['internal_staff_id'],
        ]
       );
      
       $performer         = Performer::with('performerFlag')->where('id',$request->get('performer_id'))->first();
       $flagCount         = count($performer->performerFlag);

       return response()->json(['status'=>true,'message'=>' Flag Added Succsessfully','flagCount'=>$flagCount]);

    }

    public function add(){
       
       $data = array(
           array('union'=>'Screen Actors Guild'),
           array('union'=>'Screen Actors Guild Eligible'),
           array('union'=>'UBCP'),
           array('union'=>'UBCP Apprentice'),
           array('union'=>'UBCP Extra'),
           array('union'=>'UDA'),
           array('union'=>'UDA Stagiaire'),
           array('union'=>'RQD'),
           array('union'=>'ANDA'),
           array('union'=>'ACTRA'),
           array('union'=>'ACTRA Apprentice'),
           array('union'=>'AABP'),
           array('union'=>'EQUITY'),
           array('union'=>'EQUITY Permitee'),
           array('union'=>'AEA'),
           array('union'=>'CAEA'),
           array('union'=>'CAEA Apprentice'),
           array('union'=>'BAEA'),
           array('union'=>'MEAA'),
           array('union'=>'NON-UNION'),
          
         
           //...
       );

        PerformerUnion::insert($data);

    }

    public function myperformers(Request $request){
        $activeclass       = "myperfomers";
        $performerunion    = PerformerUnion::all();
        $searchResult      = PerformerSearchresults::all();
        return view('internal-staff.myperfomers',compact('activeclass','performerunion','searchResult'));   
    }

    public function performersScroll(Request $request){
      // return $request;
        $lastId = $request->get('lastId');
        if($request->get('search'))
        { 
            $search = $request->get('search');
            $performer=Performer::with('unionDetail')->where('id', '>', $lastId)
                                 ->where(function($query) use ($search){
                                    $query->where('firstName','like','%'.$search.'%')
                                 ->orWhere('lastName','like','%'.$search.'%')
                                 ->orWhere('middleName','like','%'.$search.'%');
                              })
                              ->limit(40)
                              ->get();  
                          
        }elseif($request->get('gender'))
        {
            $search    =  $request->get('gender');
            $performer =  Performer::with('unionDetail')->where('id', '>', $lastId)
                                 ->where(function($query) use ($search){
                                    $query->where('gender',$search);
                              })
                              ->limit(40)
                              ->get();                                
               
        }
        elseif($request->get('availability'))
        { 
            $search = $request->get('availability');
            $performer=Performer::with('unionDetail')->where('id', '>', $lastId)
                                 ->where(function($query) use ($search){
                                    $query->where('availability',$search);
                              })
                              ->limit(40)
                              ->get();                                
                          
        }
        elseif($request->get('ethnicity'))
        { 
           $search = $request->get('ethnicity');
           $performer=Performer::with('unionDetail')->where('id', '>', $lastId)
                                ->whereHas('appearance',function($query) use ($search){
                                  $query->where('ethnicity_id',$search);
                                })
                              ->limit(40)
                              ->get();     
        }
        elseif($request->get('region'))
        { 
           $search = $request->get('region');
           $performer=Performer::with('unionDetail')->where('id', '>', $lastId)
                                ->whereHas('appearance',function($query) use ($search){
                                  $query->where('region_id',$search);
                                })
                              ->limit(40)
                              ->get();     
        }
        elseif($request->get('min_age') && $request->get('max_age'))
        { 
           $search = $request->get('region');
           $performer=Performer::with('unionDetail')->where('id', '>', $lastId)
                                ->whereHas('appearance',function($query) use ($search){
                                  $query->whereBetween('age',[request('min_age'),request('max_age')]);
                                })
                              ->limit(40)
                              ->get();     
        }
        else
        {
        $performer=Performer::with('unionDetail')->where('id', '>', $lastId)
                              ->limit(40)
                              ->get();  
        }


        $output ='';

        if(count($performer))
        {   
          
            foreach ($performer as $value) {
              $height = "";
              $weight = "";
              $flagcount=0;
              $union_TAG="-";
              $agency="";
              
                if(isset($value->unionDetail->union_tag))
                {
                  $union_TAG = $value->unionDetail->union_tag;
                }
                 if(isset($value->appearance[0]->height))
                 {

                   $height = preg_replace(array('/\bfeet\b/','/\bft\b/','/\bf\b/','/\binches\b/','/\bin\b/'),array("'","'","'",'"','"','"'), $value->appearance[0]->height);

                   str_replace('ft',"'",$height);

                   
                   if(is_numeric($height))
                   {
                    $height =  round(floatval($height / 30.48),2);
                  }else{
                    $height = $height;
                  }
                  if(strlen($height) < 7){$height =$height;}else{$height ="";}

                 }
                 if(isset($value->appearance[0]->weight))
                 {
                  $weight =str_replace("lbs","",$value->appearance[0]->weight) .' lbs';          
                }
                if(isset($value->performerFlag))
                {
                  $flagcount=count($value->performerFlag);
                }
                if(isset($value->agentDetail->agencies->agencyAbbreviation))
                {
                  $agency =  $value->agentDetail->agencies->agencyAbbreviation;
                }
                $view = $request->input('viewformat');

                
              //return $value->id;
                if($view == 'grid'){
                $output .= '<li>';
                $output .=    '<a class="showPerformersDetail" href="javascript:void(0)"  id="'.$value->id.'">';
                $output .=        '<div class="fltsr_userimg">';
                $output .=            '<img src="'.$value->image_url.'" alt="">';
                $output .=        '</div>' ;
                $output .=        '<div class="fltsr_userdits2">';
                $output .=            '<h4>'.$value->full_name.'</h4>';
                $output .=            '<div class="hwvbox">';
                $output .=            '<p>';
                $output .=            '<span>'.$value->age.''.($value->gender == "male" ? "M" : "F" ).'</span>';
                $output .=            '<span>'.$height.'</span>';
                $output .=            '<span>'.$weight.' </span>';
                $output .=            '</p>';
                $output .=            '</div>';
                $output .=            '<div class="flggrnbox">';
                $output .=                '<div class="flggrnbox_left">';
                $output .=                    '<p>'.($flagcount >= 1 ? $flagcount : 0 ).'</p>';
                $output .=                    '<i class="far fa-flag-alt"></i>';
                $output .=                 '</div>';
                $output .=                '<div class="flggrnbox_right">';
                $output .=                    '<p>'.$union_TAG."".$value->union_numbe.'</p> <p>'.$agency.'</p>';
                $output .=                '</div>';           
                $output .=            '</div>';
                $output .=        '</div>';
                $output .=    '</a>';
                $output .= '</li>';
                }else{

                  $output .=    '<a class="showPerformersDetail" href="javascript:void(0)"  id="'.$value->id.'">';
                  $output .=  '<div class="fremanctlistbox">';
                  $output .=  '<div class="fremanctlistiner1">';
                  $output .='<img src="'.$value->image_url.'" alt="">';
                  $output .='</div>';
                  
                  
                  $output .='<div class="fremanctlistiner2">';
                  $output .='<h4>'.$value->full_name.'</h4>';
                  $output .= '<p>';
                  $output .= '<span class="fagesp">'.$value->age.' '.($value->gender == "male" ? "M" : "F" ).'</span> ';
                  $output .= '<span class="fagesp">'.$height.'</span> ';
                  $output .= '<span class="fagesp">'.$weight.'</span>';
                  $output .= '</p>';
                  $output .= '</div>';
                  $output .=  '<div class="fremanctlistiner3">';
                  
                  $output .=  '<p></p>';
                  $output .=  '</div>';
                  $output .=  '<div class="fremanctlistiner4">';
                  $output .=  '<h6>Profile last updated</h6>';
                  $output .=  '<p>'.$value->updated_at.'</p>';
                  $output .=  '</div>';
                  $output .=  '<div class="fremanctlistiner5">';        
                  $output .=  '<div class="flggrnbox_left1">';
                  $output .=  '<p>1</p>';
                  $output .=  '<i class="far fa-flag-alt"></i>';
                  $output .=  '</div>';
                  $output .=  '<div class="flggrnbox_right1">';
                  $output .=  '<p>'.$union_TAG.'-'.$value->union_numbe.'</p> <p>'.$agency.'</p>';
                  $output .=  '</div>';
                  $output .=  '</div>';
                  $output .=  '</a>';       
                  $output .=  '<div class="fremanctlistiner6">';
                  $output .=  '<a href="javascript:void(0)">View Profile</a>';
                  $output .=  '</div>';
                  $output .=  '</div>';

                }            
              
          }
            
        }
        

        return $output;
    }
    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(),[  
            'firstname'                  =>  'required',
            'lastname'                   =>  'required',
            'email'                      =>  'required|email',
            'phonenumber'                =>  'required|min:14'
        ],[
            'firstname.required' =>'First Name Required',
            'lastname.required'  =>'Last Name Required',
            'email.required'     =>'Email Name Required',
          'phonenumber.required' =>'Phone Number Required',
            'phonenumber.min'    =>'Please Enter 10 Digit Phone Number'
         ]);

        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }
        $Editdata = InternalStaff::where('id',$request->id)->first();
        $Editdata->update($request->all());
        return response()->json(['status'=>true,'message'=>'Profile updated Successfully','data'=> ""]);   
    }
    public function changePassword(Request $request)
    {

       $password = InternalStaff::where('id',Auth::id())->first();
       


       $validator = Validator::make($request->all(),[  
           'old_password'                   =>  'required',
           'password'                   =>  'required',
           'confirm_password'                   =>  'required|same:password',
       ]);

       if($validator->fails()){
           return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
       }

       if (Hash::check($request->old_password,$password->password)) {

     $newPassword = InternalStaff::where('id',Auth::id())->update(array('password'=>Hash::make($request->password)));

           if($newPassword){
               return response()->json(['status'=>true,'message'=>" Password Changed Successfully"]);  
           }   
               
       }else{

           return response()->json(['status'=>false,'message'=>"Old Password Does Not Match"]);   
       }



    }
    public function addperformer(Request $request){

       $validator = Validator::make($request->all(),[  
           'firstName'                   =>  'required',
           'middleName'                  =>  'required',
           'lastName'                    =>  'required',
           'agent_id'                    =>  'required',
           'date_of_birth'               =>  'required',
           'email'                       =>  'required|email',
           'phonenumber'                 =>  'required',
           'address'                     =>  'required',
           'city'                        =>  'required',
           'province'                    =>  'required',
           'postalCode'                  =>  'required',
           'country'                     =>  'required',
           'sin'                         =>  'required',
           'union'                       =>  'required',
           'gender'                      =>  'required',
           
       ],[
           'agent_id.required'           =>  'Select Agent'  
       ]);

       if($validator->fails()){
           return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
       }

      $Performer           = new Performer($request->except('sin'));
   
      
      $Performer->date_of_birth = date('Y-m-d',strtotime($request['date_of_birth']));
    
      $Performer->age                 =  Carbon::parse( date('Y-m-d',strtotime($request['date_of_birth'])))->age;
      $repassword = str_random(10);
      $Performer->password     = bcrypt($repassword);
      $Performer->save();
      $performersDetails    = PerformersDetails::updateOrCreate(
        ['performer_id'  => Auth::user()->id],
        ['sin'           => $request->sin ]
        );  

       
       $data = array('email'=>$request->email,'password' => $repassword,'name'=> $request->firstName.' '.$request->lastName);
         
            Mail::send('performers.performerpasswordreset', $data, function($message) use ($data) {
              $message->to($data['email'],$data['name'])->subject
                 ('Your New Password For Performer');
         
            });

        return response()->json(['status'=>true,'message'=>" Performer Added Successfully"]); 
        
    }

    //LIST VIEW 
    public function listView()
    {
     
       $performer         = Performer::with('appearance','performerFlag')->limit(10)->get(); 
      $activeclass             = 'perfomer';
      return view("internal-staff.listView",compact('activeclass'));
    }
   
 public function getSliderImage(Request $request){
      $per_id = $request->id;
      $performer_profile = Performer::where('id',$per_id)->get()->pluck('image_url');
      //print_r($performer_profile);
      $sidebar_image =array();
      $sidebar_image = MediaPortfolio::select('*')->where(function($query) use ($request){
       $query->where('media_type','like','%image%')
       ->orwhere('media_type','like','%jpg%');
     })->where('performers_id',$per_id)->get()->pluck('image_url');
      $slides = $performer_profile->merge($sidebar_image);
      
      return response()->json(['status'=>'true','msg'=>$slides,'message'=>'']);
  
    }
    public function removeFlag(Request $request){
        $flagid = PerformersFlags::find($request->id);
        $flagid->delete();
        return response()->json(['status'=>true,'message'=>"Flag Removed Successfully"]);
    }
    public function savenotes(Request $request){
         /*   $Note = Note::updateOrCreate(
                ['performer_id' => Auth::user()->id],['notes'=>$request->note]           
            ); */ 
            
            $Note = new Note;
            $Note->performer_id=$_GET['performer_id'];
            $Note->internal_staff_id=Auth::guard('internal-staff')->user()->id;
            $Note->notes=$_GET['note'];
            $Note->username=$_GET['user'];
            $Note->save(); 
            $records = Note::where('performer_id', '=', $request->performer_id)->get();
            $html = view('performers.updateNoteSection',compact('records'))->render();  
            return response()->json(['status'=>true,'html'=>$html]);
        //return redirect()->route('internal-staff.note',['id'=>$request->performer_id]);

            
    }
    public function deletenote(Request $request){
        $Note=Note::find($request->id);
        $performer_id = $Note->performer_id;
        $Note->delete();
        $records = Note::where('performer_id', '=', $performer_id)->get();
        $html = view('performers.updateNoteSection',compact('records'))->render();  
        return response()->json(['status'=>true,'html'=>$html]);
    }
    public function editnote(Request $request){
        $Note=Note::find($request->id);
        return $Note;
    }
    public function updatenote(Request $request){
        
        
        $Note           = Note::find($request->noteid);
         $performer_id  = $Note->performer_id;
        $Note->notes    = $request->note;
        $Note->username = $request->username;
        //$Note->username=$request->username;
        $Note->save();
        $records       = Note::where('performer_id', '=', $performer_id)->get();
        $html = view('performers.updateNoteSection',compact('records'))->render();

        return response()->json(['status'=>true,'html'=>$html]);
    }
} 
