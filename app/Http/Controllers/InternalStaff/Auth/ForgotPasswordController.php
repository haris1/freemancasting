<?php

namespace App\Http\Controllers\InternalStaff\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Models\v1\InternalStaff;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    } 

    // protected function guard(){
    //     return Auth::guard('internal-staff');
    // }
    public function showLinkRequestForm()
    {
        return view('internal-staff.auth.passwords.email');
    }
  
    public function broker()
    {
        return Password::broker('internal-staffs');
    }
}
