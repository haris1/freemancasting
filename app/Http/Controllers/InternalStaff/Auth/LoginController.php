<?php

namespace App\Http\Controllers\InternalStaff\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\v1\InternalStaff;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Auth;
Use Redirect;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/performers/list';

    public function __construct()
    {
        $this->middleware('guest:internal-staff')->except('logout');
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function showLoginForm()
    {
        return view('internal-staff.auth.login');
    }

    protected function guard(){
        return Auth::guard('internal-staff');
    }

    public function login(Request $request)
    { 
       
            
            

        if(Auth::guard('internal-staff')->attempt(['email' => $request->email,'status' =>'enable' ,'password' => $request->password])) {
                //Auth::loginUsingId($performers->id);
             return $this->authenticated($request, $this->guard()->user())
         ?: redirect()->intended($this->redirectPath());
         }else{
              //  return response()->json(array('status'=>false,'msg'=> 'Please enter valid credential'), 200);
          //  $errors = new MessageBag(['password' => ['Email and/or password invalid.'],'status' => 'you are disabled']);
          //  return Redirect::back()->withErrors($errors);

            return $this->sendFailedLoginResponse($request);
        }
    }
    
    public function logout(Request $request){
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect()->route('internal-staff.login');
    }
}
