<?php

namespace App\Http\Controllers\CorporateAdmin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\v1\CorporateAdmin;
use Auth;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/corporate-admin/list';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function showLoginForm()
    {
        /*echo "<pre>";
        print_r(CorporateAdmin::get()->toArray());
        exit;*/
        return view('corporate-admin.auth.login');
    }

    protected function guard(){
        return Auth::guard('corporate-admin');
    }
    public function __construct()
    {
       // $this->middleware('guest')->except('logout');
    }
    
    public function logout(Request $request){
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect()->route('corporate-admin.login');
    }
}
