<?php

namespace App\Http\Controllers\CorporateAdmin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Models\v1\InternalStaff;
use App\Models\v1\Document;
use App\Models\v1\Performer\Skintone;
use App\Models\v1\Performer\Eyes;
use App\Models\v1\Performer\HairColor;
use App\Models\v1\Performer\Nationality;
use App\Models\v1\Performer\Ethnicity;
use App\Models\v1\Performer\Handedness;
use App\Models\v1\Performer\HairLength;
use App\Models\v1\Performer\HairFacial;
use App\Models\v1\Performer\HairStyle;
use App\Models\v1\Performer\BodyTrait;
use App\Models\v1\Performer\PerformerUnion;
use App\Models\v1\Performer\VehicleType;
use App\Models\v1\Performer\VehicleMake;
use App\Models\v1\Performer\VehicleAccessory;
use App\Models\v1\Performer\Piercing;
use App\Models\v1\Performer\Tattoo;
use App\Models\v1\Performer\PerformerAvailability;
use App\Models\v1\Performer\PerformerStatus;

use App\Models\v1\PerformerAgent;
use App\Models\v1\HelpDesk;
use App\Models\v1\Agency;
use App\Models\v1\Performer\Flags;
use App\Models\v1\Tags;
use Validator;
use Hash;
use Redirect;
class CorporateAdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth:corporate-admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
      return redirect()->route('corporate-admin.list');

//        return view('corporate-admin.index');
    }
 
    //Form For Adding Staff Member

    public function form(){
      
        return view('corporate-admin.form');
    }
 
    //Listing Staff Members

    public function list(){
        $internalstaff=InternalStaff::all();
        return view('corporate-admin.list',compact('internalstaff'));
    }
 
    //Listing Documents

    public function documents(){
        $document = Document::all();
        return view('corporate-admin.documents',compact('document'));
    }

    //Adding Staff Members

    public function store(Request $request){
        $validator = Validator::make($request->all(),[  
               'firstname'                   =>  'required',
               'lastname'                   =>  'required',
               'email'                      =>  'required|email|unique:internal_staffs,email',
               'password'                   =>  'required|min:6',
               'phonenumber'                =>  'required|min:14',
           ],
           [
              'phonenumber.min' => 'Please Enter 10 Digit Phone Number'
           ]);

           if($validator->fails()){
                 return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
        $internal_staff = $request->all();
        $data  = new InternalStaff($internal_staff);
        $data->password=Hash::make($request->password);

        $data->save();
         
        $newmember = array('email'=>$request->email,'password' => $request->password,'name'=> $request->firstname.' '.$request->lastname);
          
             Mail::send('internal-staff.performerpasswordreset', $newmember, function($message) use ($newmember) {
                $message->to($newmember['email'],$newmember['name'])->subject
                   ('Your New Password For Internal Staff');
          
             });
        return response()->json(['status'=>'true','message'=>'Staff Member Inserted Successfully']);
        return redirect()->route('corporate-admin.list')->with('message','Staff Member Inserted');
    }

    //Returning Data For Updation Of Staff Members

    public function edit($id){
       
        $internalstaff=InternalStaff::find($id);
        
        return view('corporate-admin.edit',compact('internalstaff'));
    }
 
    //Updating Staff Members

    public function save(Request $request){
       $validator = Validator::make($request->all(),[  
              'firstname'                   =>  'required',
              'lastname'                   =>  'required',
              'email'                      =>  'required|email|unique:internal_staffs,email,'.$request->id,
                  'phonenumber'                =>  'required|min:14',
              ],
              [
                 'phonenumber.min' => 'Please Enter 10 Digit Phone Number'
          ]);

          if($validator->fails()){
                return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
          }
        InternalStaff::where('id',$request->id)->update(['firstname'=>$request->firstname,'lastname'=>$request->lastname,'email'=>$request->email,'phonenumber'=>
            $request->phonenumber]);
        return response()->json(['status'=>'true','message'=>'Staff Member Updated Successfully']);
        return redirect()->route('corporate-admin.list');
    }
  
    //Deleting Staff Member

    public function delete(Request $request){
          
         $internalstaff=InternalStaff::find($request->id);
         $internalstaff->delete();
        // return redirect()->route('corporate-admin.list');
    }
  
    //Changing Status Of Staff Members

    public function status($id){
         $internalstaff=InternalStaff::find($id);
        
         if($internalstaff->status=="enable"){
            $internalstaff->status="disable";                    
         }
         else{
            $internalstaff->status="enable";
         }
         $internalstaff->save();
        return redirect()->route('corporate-admin.list');

    }
 
    //Display Skin Tone List
 
    public function tone(){
            $skintone=Skintone::all();
            return view('corporate-admin.skintone',compact('skintone'));
    }
  
    //Display Help Desk 

    public function helpdesk(){
    $helpdesk = HelpDesk::find(1);

      return view('corporate-admin.helpdesk',compact('helpdesk'));
    }


    //Display Eyes List
  
    public function eyes(){
            $eyes=Eyes::all();
            return view('corporate-admin.eyes',compact('eyes'));
    }
  
    //Display Hair Color List
 
    public function haircolor(){

            $haircolor=HairColor::all();
            return view('corporate-admin.haircolor',compact('haircolor'));
    }

    //Display Nationality List
 
    public function nationality(){
        $nationality=Nationality::all();
            return view('corporate-admin.nationality',compact('nationality'));
    }
  
    //Display Piercing List
 
    public function piercing(){
      $piercing = Piercing::all();
      return view('corporate-admin.piercing',compact('piercing'));
    }
 
    //Display Tattoo List
   
    public function tattoo(){
      $tattoo = Tattoo::all();
      return view('corporate-admin.tattoo',compact('tattoo'));
    }
  
    //Display Ethnicity List
  
    public function ethnicity(){
        $ethnicity=Ethnicity::all();
        return view('corporate-admin.ethnicity',compact('ethnicity'));
    }
   
    //Display Handedness List
  
    public function handedness(){
        $handedness=Handedness::all();
        return view('corporate-admin.handedness',compact('handedness'));
    }
   
    //Display Vehicle Type List
   
    public function vehicletype(){
      $vehicletype = VehicleType::all();
      return view('corporate-admin.vehicletype',compact('vehicletype'));
    }
    
    //Display Vehicle Make List
   
    public function vehiclemake(){
     $vehiclemake = VehicleMake::all();     
      return view('corporate-admin.vehiclemake',compact('vehiclemake'));

    }
   
    //Display Vehicle Accessories List
    
    public function vehicleaccessories(){
      $vehicleaccessory = VehicleAccessory::all();
      return view('corporate-admin.vehicleaccessories',compact('vehicleaccessory'));
    }
   
    //Display Hair Length List
   
    public function hairlength(){

        $hairlength =  HairLength::all();
        return view('corporate-admin.hairlength',compact('hairlength'));
    }

    //Display Hair Style List
    
    public function hairstyle(){

        $hairstyle =  HairStyle::all();
        return view('corporate-admin.hairstyle',compact('hairstyle'));
    }
   
   //Display Hair Facial

   public function hairfacial(){

       $hairfacial =  HairFacial::all();
       return view('corporate-admin.hairfacial',compact('hairfacial'));
   }
   
   //Display Body Trait List
   
   public function bodytrait(){

       $bodytrait =  BodyTrait::all();
       return view('corporate-admin.bodytrait',compact('bodytrait'));
   }
    
    //Display TAG List
   
    public function tags(){

        $tags      =   Tags::all();
         return view('corporate-admin.tag',compact('tags'));     
    }
    
    //Display Agent List
   
    public function Agent(){
        $performeragent     =   PerformerAgent::all();
        $agency             = Agency::all();
         return view('corporate-admin.Agent',compact('performeragent','agency'));     
    }
    
    //Display Union List
    
    public function Union(){
        $PerformerUnion      =   PerformerUnion::all();
         return view('corporate-admin.Union',compact('PerformerUnion'));     
    }
    //Display Flag List     
  
    public function Flag(){
         $flags = Flags::all();
         return view('corporate-admin.Flag',compact('flags'));     
    }
    
    //Display Agency List

    public function Agency(){
      $agency = Agency::all();
      return view('corporate-admin.agency',compact('agency'));
    }

    //Display Availability
    public function Availability(){
      $availability = PerformerAvailability::all();
      //return view('corporate-admin.availability',compact('availability')); 
      return view('corporate-admin.availability',compact('availability'));
    }
    //Adding Skin Tone

    public function addtone(Request $request){
         $validator = Validator::make($request->all(),[  
                'skintone'                   =>  'required|unique:skin_tones,skin_tone',
            ],[
                'skintone.required' => 'Enter Skin Tone',
                'skintone.unique'   => ' Skin Tone Already Taken',
            ]);

            if($validator->fails()){
                return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
            }
         $skintone  = new Skintone();
         $skintone->skin_tone=$request->skintone; 
         $skintone->save();

    }

    //Adding Vehicle Type

    public function addvehicletype(Request $request){
      $validator = Validator::make($request->all(),[  
             'vehicle_type'                   =>  'required|unique:vehicle_types,vehicle_type',
         ]);

         if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
         }
         $add = new VehicleType();
         $add->vehicle_type = $request->vehicle_type;
         $add->save();
    }

    //Adding Agency 

    public function addAgency(Request $request){
    
      $validator = Validator::make($request->all(),[  
             'agencyName'                   =>  'required|unique:agency,agencyName',
             'agencyAbbreviation' => 'required'
         ]);

         if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
         }
         $add = new Agency();
         $add->agencyName = $request->agencyName;
         $add->agencyAbbreviation = $request->agencyAbbreviation;
         $add->save();
         return response()->json(['status'=>true,'message'=>'Agency Added Successfully','id'=>$add->id]);
    }
    
    //Adding Vehicle Make

    public function addvehiclemake(Request $request){
      $validator = Validator::make($request->all(),[  
             'vehicle_make'                   =>  'required|unique:vehicle_makes,vehicle_make',
         ]);

         if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
         }
         $add = new VehicleMake();
         $add->vehicle_make = $request->vehicle_make;
         $add->save();
    }

    //Adding Vehicle Accessories
    
    public function addvehicleaccessories(Request $request){
      $validator = Validator::make($request->all(),[  
             'vehicle_accessory'                   =>  'required|unique:vehicle_accessories,vehicle_accessory',
         ]);

         if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
         }
         $add = new VehicleAccessory();
         $add->vehicle_accessory = $request->vehicle_accessory;
         $add->save();
    }

    //Adding Eyes

    public function addeyes(Request $request){
        $validator = Validator::make($request->all(),[  
               'eyes'                   =>  'required|unique:eyes,eyes',
           ]);

           if($validator->fails()){
              return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
         $eyes  = new Eyes();
         $eyes->eyes=$request->eyes; 
         $eyes->save();

    }
   
    //Adding Flag

    public function addflag(Request $request){
        $validator = Validator::make($request->all(),[  
               'flag_name'                   =>  'required|unique:flags,flag_name',
           ]);

           if($validator->fails()){
             return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
         $flags  = new Flags();
         $flags->flag_name=$request->flag_name; 
         $flags->save();

    }

    //Adding Help Desk

    public function addhelpdesk(Request $request){
      $validator = Validator::make($request->all(),[  
             'portfolio_help'                =>  'required',
             'closet_help'                   =>  'required',
             'skills_help'                   =>  'required',
             'stats_help'                    =>  'required',
             'props_help'                    =>  'required',
             'resume_help'                   =>  'required',
             'document_help'                 =>  'required',
         ],[
            'portfolio_help.required'                =>  'Enter Portfolio Help',
            'closet_help.required'                   =>  'Enter Closet Help',
            'skills_help.required'                   =>  'Enter Skills Help',
            'stats_help.required'                    =>  'Enter Stats Help',
            'props_help.required'                    =>  'Enter Props Help',
            'resume_help.required'                   =>  'Enter Resume Help',
            'document_help.required'                 =>  'Enter Document Help',
         ]);

         if($validator->fails()){
           return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
         }
     // $HelpDesk =HelpDesk::find(1);
$HelpDesk =HelpDesk::updateOrCreate([
    //Add unique field combo to match here
    //For example, perhaps you only want one entry per user:
    'id'   => 1,
],$request->all());

      //$HelpDesk->update($request->all());
      return $HelpDesk;
    }

    //Adding Piercing

    public function addPiercing(Request $request){
        $validator = Validator::make($request->all(),[  
               'piercing'                   =>  'required|unique:piercings,piercing',
           ]);

           if($validator->fails()){
             return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
         $piercing  = new Piercing();
         $piercing->piercing=$request->piercing; 
         $piercing->save();
    }

    //Adding Tattoo

    public function addTattoo(Request $request){
      $validator = Validator::make($request->all(),[  
             'tattoo'                   =>  'required|unique:tattoos,tattoo',
         ]);

         if($validator->fails()){
           return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
         }
       $tattoo  = new Tattoo();
       $tattoo->tattoo=$request->tattoo; 
       $tattoo->save();
    }

    //Adding Union

    public function addunion(Request $request){
        $validator = Validator::make($request->all(),[  
               'union'                   =>  'required|unique:performer_unions,union',
               'union_tag'               =>  'required',
           ],
           [
               'union.required'     => 'Enter Union Name',
               'union.unique'       => 'Union Already Taken',
               'union_tag.required' => 'Enter Union Tag'
           ]);

           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
         $PerformerUnion  = new PerformerUnion();
         $PerformerUnion->union=$request->union; 
         $PerformerUnion->union_tag=$request->union_tag; 
         $PerformerUnion->save();

    }

    //Adding Agent
    
    public function addagent(Request $request){
       $validator = Validator::make($request->all(),[  
              'agent_name'              =>  'required|unique:performer_agents,agent_name',
              'agency_id'               =>  'required',
          ],
          [
              'agent_name.required'     => 'Enter Agent Name',
              'agent_name.unique'       => 'Agent Already Taken',
              'agency_id.required'      => 'Select Agency Name'
          ]);

          if($validator->fails()){
              return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
          }
       $PerformerAgent=new PerformerAgent($request->all());
       $PerformerAgent->save();
       return $PerformerAgent;

    }
    
    //Adding Tag

   public function addtag(Request $request){
         $validator = Validator::make($request->all(),[  
                'tag'                     =>  'required|unique:tags,tag',
            ],
            [
                'tag.required'     => 'Enter Tag Name',
                'tag.unique'       => 'Tag Already Taken',
            ]);

            if($validator->fails()){
                return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
            }
         $tags  = new Tags();
         $tags->tag=$request->tag; 
         $tags->save();

    }
    
   //Adding Document 

    public function adddocument(Request $request){
        $validator = Validator::make($request->all(),[  
               'doument_name'       =>  'required',
               'doument_type'       =>  'required',
           ],
           [
               'doument_name.required'     => 'Enter Document Name',
               'doument_type.required'     => 'Select Document Type',
           ]);

           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
        $document = new Document($request->all());
        $document->save();

    }

    //Deleting Skin Tone

    public function deletetone(Request $request){
        $Skintone=Skintone::find($request->id);
          if($Skintone->is_deleted == '1'){
             $Skintone->is_deleted = '0';
             $Skintone->save();
            return response()->json(['message'=>'Skintone Deleted Successfully']);
          }
          else{
             $Skintone->is_deleted = '1';
             $Skintone->save();
            return response()->json(['message'=>'Skintone Undeleted Successfully']);
          }
    }
   
    //Deleting Eyes

    public function deleteeyes(Request $request){
        $eyes=Eyes::find($request->id);
        
        if($eyes->is_deleted == '1'){
          $eyes->is_deleted = '0';
           $eyes->save();
          return response()->json(['message'=>'Eyes Deleted Successfully']);
        }
        else{
           $eyes->is_deleted = '1';
           $eyes->save();
          return response()->json(['message'=>'Eyes Undeleted Successfully']);
        }
        
    }
  
    //Deleting Agency

    public function deleteAgency(Request $request){
      $Agency=Agency::find($request->id);
      if($Agency->is_deleted == '1'){
         $Agency->is_deleted = '0';
         $Agency->save();
        return response()->json(['message'=>'Agency Deleted Successfully']);
      }
      else{
         $Agency->is_deleted = '1';
         $Agency->save();
        return response()->json(['message'=>'Agency Undeleted Successfully']);
      }
    }

    //Deleting Availability

    public function deleteavailability(Request $request){
      $PerformerAvailability = PerformerAvailability::find($request->id);
      if($PerformerAvailability->is_deleted == '1'){
         $PerformerAvailability->is_deleted = '0';
         $PerformerAvailability->save();
        return response()->json(['message'=>'PerformerAvailability Deleted Successfully']);
      }
      else{
         $PerformerAvailability->is_deleted = '1';
         $PerformerAvailability->save();
        return response()->json(['message'=>'PerformerAvailability Undeleted Successfully']);
      }
    }

   //Deleting Document

    public function deletedocument(Request $request){
        $document=Document::find($request->id);
        if($document->is_deleted == '1'){
           $document->is_deleted = '0';
           $document->save();
          return response()->json(['message'=>'Document Deleted Successfully']);
        }
        else{
           $document->is_deleted = '1';
           $document->save();
          return response()->json(['message'=>'Document Undeleted Successfully']);
        }
    }
  
    //Deleting Tag

    public function deletetag(Request $request){
        $tags=Tags::find($request->id);
        if($tags->is_deleted == '1'){
           $tags->is_deleted = '0';
           $tags->save();
          return response()->json(['message'=>'Tag Deleted Successfully']);
        }
        else{
           $tags->is_deleted = '1';
           $tags->save();
          return response()->json(['message'=>'Tag Undeleted Successfully']);
        }
    }
  
    //Deleting Union

    public function deleteunion(Request $request){
        $PerformerUnion=PerformerUnion::find($request->id);
       if($PerformerUnion->is_deleted == '1'){
         $PerformerUnion->is_deleted = '0';
         $PerformerUnion->save();
         return response()->json(['message'=>'PerformerUnion Deleted Successfully']);
       }
       else{
          $PerformerUnion->is_deleted = '1';
          $PerformerUnion->save();
         return response()->json(['message'=>'PerformerUnion Undeleted Successfully']);
       }
    }
   
    //Deleting Flag

    public function deleteflags(Request $request){
        $Flags=Flags::find($request->id);
         $Flags->delete();
    }
  
    //Deleting Piercing

    public function deletePiercing(Request $request){
      $Piercing=Piercing::find($request->id);
       $Piercing->delete();
    }

    //Deleting Tattoo
  
    public function deleteTattoo(Request $request){
      $Tattoo=Tattoo::find($request->id);
       $Tattoo->delete();
    }

    //Deleting Hair Color

    public function deletehaircolor(Request $request){
        $HairColor=HairColor::find($request->id);
        if($HairColor->is_deleted == '1'){
           $HairColor->is_deleted = '0';
           $HairColor->save();
          return response()->json(['message'=>'HairColor Deleted Successfully']);
        }
        else{
           $HairColor->is_deleted = '1';
           $HairColor->save();
          return response()->json(['message'=>'HairColor Undeleted Successfully']);
        }
    }
 
    //Deleting Nationality

    public function deletenationality(Request $request){
        $Nationality=Nationality::find($request->id);
         if($Nationality->is_deleted == '1'){
            $Nationality->is_deleted = '0';
            $Nationality->save();
           return response()->json(['message'=>'Nationality Deleted Successfully']);
         }
         else{
            $Nationality->is_deleted = '1';
            $Nationality->save();
           return response()->json(['message'=>'Nationality Undeleted Successfully']);
         }
    }

    //Deleting Ethnicity
  
    public function deleteethnicity(Request $request){
        $Ethnicity=Ethnicity::find($request->id);
        if($Ethnicity->is_deleted == '1'){
          $Ethnicity->is_deleted = '0';
           $Ethnicity->save();
          return response()->json(['message'=>'Ethnicity Deleted Successfully']);
        }
        else{
           $Ethnicity->is_deleted = '1';
           $Ethnicity->save();
          return response()->json(['message'=>'Ethnicity Undeleted Successfully']);
        }
    }
  
    //Deleting hair Length

    public function deletehairlength(Request $request){
        $HairLength=HairLength::find($request->id);
        if($HairLength->is_deleted == '1'){
          $HairLength->is_deleted = '0';
           $HairLength->save();
          return response()->json(['message'=>'HairLength Deleted Successfully']);
        }
        else{
           $HairLength->is_deleted = '1';
           $HairLength->save();
          return response()->json(['message'=>'HairLength Undeleted Successfully']);
        }
        
    }
   
    //Deleting Hair Facial

    public function deletehairfacial(Request $request){
        $HairFacial=HairFacial::find($request->id);
        if($HairFacial->is_deleted == '1'){
          $HairFacial->is_deleted = '0';
           $HairFacial->save();
          return response()->json(['message'=>'HairFacial Deleted Successfully']);
        }
        else{
           $HairFacial->is_deleted = '1';
           $HairFacial->save();
          return response()->json(['message'=>'HairFacial Undeleted Successfully']);
        }
        
    }

    //Deleting Hair Style

    public function deletehairstyle(Request $request){
        $HairStyle=HairStyle::find($request->id);
        if($HairStyle->is_deleted == '1'){
          $HairStyle->is_deleted = '0';
           $HairStyle->save();
          return response()->json(['message'=>'HairStyle Deleted Successfully']);
        }
        else{
           $HairStyle->is_deleted = '1';
           $HairStyle->save();
          return response()->json(['message'=>'HairStyle Undeleted Successfully']);
        }
        
    }

    //Deleting Body Trait

    public function deletebodytrait(Request $request){
        $BodyTrait=BodyTrait::find($request->id);
        if($BodyTrait->is_deleted == '1'){
          $BodyTrait->is_deleted = '0';
           $BodyTrait->save();
          return response()->json(['message'=>'BodyTrait Deleted Successfully']);
        }
        else{
           $BodyTrait->is_deleted = '1';
           $BodyTrait->save();
          return response()->json(['message'=>'BodyTrait Undeleted Successfully']);
        }
        
    }

    //Deleting Handedness

    public function deletehandedness(Request $request){
        $Handedness=Handedness::find($request->id);
       if($Handedness->is_deleted == '1'){
          $Handedness->is_deleted = '0';
          $Handedness->save();
         return response()->json(['message'=>'Handedness Deleted Successfully']);
       }
       else{
          $Handedness->is_deleted = '1';
          $Handedness->save();
         return response()->json(['message'=>'Handedness Undeleted Successfully']);
       }
    }
 
    //Deleting Agent

    public function deleteagent(Request $request){
        $PerformerAgent = PerformerAgent::find($request->id);
        if($PerformerAgent->is_deleted == '1'){
          $PerformerAgent->is_deleted = '0';
          $PerformerAgent->save();
          return response()->json(['message'=>'PerformerAgent Deleted Successfully']);
        }
        else{
           $PerformerAgent->is_deleted = '1';
           $PerformerAgent->save();
          return response()->json(['message'=>'PerformerAgent Undeleted Successfully']);
        }
       
    }
  
    //Deleting Vehicle Type

    public function deletevehicletype(Request $request){
      $VehicleType = VehicleType::find($request->id);
      $VehicleType->delete();
    }
 
    //Deleting Vehicle Type

    public function deletevehiclemake(Request $request){
      $VehicleMake = VehicleMake::find($request->id);
      $VehicleMake->delete();
    }
 
    //Deleting Vehicle Accessories

    public function deletevehicleaccessories(Request $request){
      $VehicleAccessory = VehicleAccessory::find($request->id);
      $VehicleAccessory->delete();
    }
  
    //Adding Nationality

    public function addnationality(Request $request){
        $validator = Validator::make($request->all(),[  
               'nationality'       =>  'required|unique:nationalities,nationality',
           ],
           [
               'nationality.required'     => 'Enter Nationality Name',
               'nationality.unique'     => 'Nationality Already Taken'
           ]);

           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
         $Nationality  = new Nationality();
         $Nationality->nationality=$request->nationality; 
         $Nationality->save();

    }
    //Adding Performer Availability
       public function addavailability(Request $request){
        $validator = Validator::make($request->all(),[  
               'availability'       =>  'required|unique:performer_availability,availability',
           ],
           [
               'availability.required'    => 'Enter Availability Name',
               'availability.unique'     =>  'Availability Already Taken'
           ]);

           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
         $PerformerAvailability  = new PerformerAvailability();
         $PerformerAvailability->availability=$request->availability; 
         $PerformerAvailability->status_color ="#ffffff";
         $PerformerAvailability->save();

    }
    //Adding Ethnicity

    public function addethnicity(Request $request){
        $validator = Validator::make($request->all(),[  
               'ethnicity'       =>  'required',
           ],
           [
               'ethnicity.required'     => 'Enter Ethnicity Name'
           ]);
           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
         $Ethnicity  = new Ethnicity();
         $Ethnicity->ethnicity=$request->ethnicity; 
         $Ethnicity->save();

    }
 
    //Adding Hair Length

    public function addhairlength(Request $request){
         $validator = Validator::make($request->all(),[  
                'hairlength'       =>  'required|unique:hair_lengths,hair_length',
            ],
            [
                'hairlength.required'     => 'Enter Hair Length',
                'hairlength.unique'       => 'Hair Length Already Taken'
            ]);
            if($validator->fails()){
                return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
            }
         $HairLength  = new HairLength();
         $HairLength->hair_length=$request->hairlength; 
         $HairLength->save();
    }
  
  //Adding Hair Facial

  public function addhairfacial(Request $request){
       $validator = Validator::make($request->all(),[  
              'hairfacial'       =>  'required|unique:hair_facials,hair_facial',
          ],
          [
              'hairfacial.required'     => 'Enter Hair Facial',
              'hairfacial.unique'       => 'Hair Facial Already Taken'
          ]);
          if($validator->fails()){
              return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
          }
       $HairFacial  = new HairFacial();
       $HairFacial->hair_facial=$request->hairfacial; 
       $HairFacial->save();
  }

  //Adding Hair Style

  public function addhairstyle(Request $request){
       $validator = Validator::make($request->all(),[  
              'hairstyle'       =>  'required|unique:hair_styles,hair_style',
          ],
          [
              'hairstyle.required'     => 'Enter Hair Style',
              'hairstyle.unique'       => 'Hair Style Already Taken'
          ]);
          if($validator->fails()){
              return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
          }
       $HairStyle  = new HairStyle();
       $HairStyle->hair_style=$request->hairstyle; 
       $HairStyle->save();
  }

  //Adding Body Trait

  public function addbodytrait(Request $request){
       $validator = Validator::make($request->all(),[  
              'bodytrait'       =>  'required|unique:body_traits,body_trait',
          ],
          [
              'bodytrait.required'     => 'Enter Body Trait',
              'bodytrait.unique'       => 'Body Trait Already Taken'
          ]);
          if($validator->fails()){
              return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
          }
       $BodyTrait  = new BodyTrait();
       $BodyTrait->body_trait=$request->bodytrait; 
       $BodyTrait->save();
  }


    //Adding Handedness

    public function addhandedness(Request $request){
         $validator = Validator::make($request->all(),[  
                'handedness'       =>  'required|unique:handednesses,handedness',
            ],
            [
                'handedness.required'=> 'Enter Handedness',
                'handedness.unique'  => 'Handedness Already Taken'
            ]);
            if($validator->fails()){
                return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
            }
         $Handedness  = new Handedness();
         $Handedness->handedness=$request->handedness; 
         $Handedness->save();
    }
 
    //Adding Hair Color

    public function addhaircolor(Request $request){
        $validator = Validator::make($request->all(),[  
               'haircolor'       =>  'required|unique:hair_colors,hair_color',
           ],
           [
               'haircolor.required'=> 'Enter Hair Color',
               'haircolor.unique'  => 'Hair Color Already Taken'
           ]);
           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
         $haircolor  = new HairColor();
         $haircolor->hair_color=$request->haircolor; 
         $haircolor->save();

    }
  
    //Returning Data For Updation Of Nationality

    public function editnationality(Request $request){
        $Nationality=Nationality::find($request->id);
        return $Nationality->nationality;
    }
    
    //Returning Data For Updation Of Ethnicity

    public function editethnicity(Request $request){
        $Ethnicity=Ethnicity::find($request->id);
        return $Ethnicity->ethnicity;
    }
    
    //Returning Data For Updation Of Skin Tone

    public function edittone(Request $request){
        $Skintone=Skintone::find($request->id);
        return $Skintone->skin_tone;
    }
    
    //Returning Data For Updation Of Hair Color

    public function edithaircolor(Request $request){
                
        $HairColor=HairColor::find($request->id);
        return $HairColor->hair_color;
    }
    
    //Returning Data For Updation Of Eyes

    public function editeyes(Request $request){
                
        $Eyes=Eyes::find($request->id);
        return $Eyes->eyes;
    }
    
    //Returning Data For Updation Of Availability

    public function editavailability(Request $request){
                
        $PerformerAvailability=PerformerAvailability::find($request->id);
        return $PerformerAvailability->availability;
    }

    //Returning Data For Updation Of Union

    public function editunion(Request $request){
        $PerformerUnion=PerformerUnion::find($request->id);
        return $PerformerUnion;
    }
    
    //Returning Data For Updation Of Flag

    public function editflag(Request $request){
        $Flags=Flags::find($request->id);
        return $Flags->flag_name;
    }
    
    //Returning Data For Updation Of Piercing

    public function editpiercing(Request $request){
      $Piercing = Piercing::find($request->id);
      return $Piercing->piercing;
    }

    //Returning Data For Updation Of Tattoo

    public function edittattoo(Request $request){
      $Tattoo = Tattoo::find($request->id);
      return $Tattoo->tattoo;
    }

    //Returning Data For Updation Of Vehicle Type

    public function editvehicletype(Request $request){
      $VehicleType=VehicleType::find($request->id);
      return $VehicleType->vehicle_type;
    }
 
    //Returning Data For Updation Of Vehicle Make

    public function editvehiclemake(Request $request){
      $VehicleMake=VehicleMake::find($request->id);
      return $VehicleMake->vehicle_make;
    } 
    
    //Returning Data For Updation Of Agency

    public function editagency(Request $request){
        $Agency  = Agency::find($request->id);
        return $Agency;
    }
 
     //Returning Data For Updation Of Vehicle Accessories

    public function editvehicleaccessories(Request $request){
      $VehicleAccessory=VehicleAccessory::find($request->id);
      return $VehicleAccessory->vehicle_accessory;
    }

  
     //Returning Data For Updation Of Document

     public function editdocument(Request $request){
        $Document=Document::find($request->id);
        return $Document;
    }
    
     //Returning Data For Updation Of Agent

    public function editagent(Request $request){

        $PerformerAgent=PerformerAgent::find($request->id);
        return $PerformerAgent;
    }
 
     //Returning Data For Updation Of Tag

     public function edittag(Request $request){
        $Tags=Tags::find($request->id);
        return $Tags->tag;
    }
    
     //Returning Data For Updation Of Handedness

     public function edithandedness(Request $request){
        $Handedness=Handedness::find($request->id);
        return $Handedness->handedness;
    }
    
     //Returning Data For Updation Of Hair Length

    public function edithairlength(Request $request){
        $HairLength=HairLength::find($request->id);
        return $HairLength->hair_length;
    }
   
    //Returning Data For Updation Of Hair Style

   public function edithairstyle(Request $request){
       $HairStyle=HairStyle::find($request->id);
       return $HairStyle->hair_style;
   }
   
    //Returning Data For Updation Of Hair Facial

   public function edithairfacial(Request $request){
       $HairFacial=HairFacial::find($request->id);
       return $HairFacial->hair_facial;
   } 

     //Returning Data For Updation Of Body Trait

    public function editbodytrait(Request $request){
        $BodyTrait=BodyTrait::find($request->id);
        return $BodyTrait->body_trait;
    } 

     //Updating Handedness

    public function updateHandedness(Request $request){
        $validator = Validator::make($request->all(),[  
               'handedness'       =>  'required|unique:handednesses,handedness',
           ],
           [
               'handedness.required'=> 'Enter Handedness',
               'handedness.unique'  => 'Handedness Already Taken'
           ]);
           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
        $Handedness=Handedness::find($request->id);
        $Handedness->handedness=$request->handedness;
        $Handedness->save();
        return $Handedness;       
    }
  
    //Updating Vehicle Type

    public function updatevehicletype(Request $request){
      $validator = Validator::make($request->all(),[  
             'vehicle_type'                   =>  'required|unique:vehicle_types,vehicle_type',
         ]);

         if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
         }
         $add = VehicleType::find($request->id);
         $add->vehicle_type = $request->vehicle_type;
         $add->save();
    }
    
    //Updating Vehicle Make

    public function updatevehiclemake(Request $request){
      $validator = Validator::make($request->all(),[  
             'vehicle_make'                   =>  'required|unique:vehicle_makes,vehicle_make',
         ]);

         if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
         }
         $add =VehicleMake::find($request->id);
         $add->vehicle_make = $request->vehicle_make;
         $add->save();
    }
   
    //Updating Vehicle Accessories

    public function updatevehicleaccessories(Request $request){
    
      $validator = Validator::make($request->all(),[  
             'vehicle_accessory'                   =>  'required|unique:vehicle_accessories,vehicle_accessory',
         ]);

         if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
         }
         $add = VehicleAccessory::find($request->id);
         $add->vehicle_accessory = $request->vehicle_accessory;
         $add->save();
    }

    //Updating Hair Length

    public function updateHairlength(Request $request){
        $validator = Validator::make($request->all(),[  
               'hair_length'       =>  'required|unique:hair_lengths,hair_length,'.$request->id,
           ],
           [
               'hair_length.required'     => 'Enter Hair Length',
               'hair_length.unique'       => 'Hair Length Already Taken'
           ]);
           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
        $HairLength=HairLength::find($request->id);
        $HairLength->hair_length=$request->hair_length;
        $HairLength->save();
        return $HairLength;       
    }
  
    //Updating Hair Style

    public function updateHairstyle(Request $request){
        $validator = Validator::make($request->all(),[  
               'hair_style'       =>  'required|unique:hair_styles,hair_style,'.$request->id,
           ],
           [
               'hair_style.required'     => 'Enter Hair Style',
               'hair_style.unique'       => 'Hair Style Already Taken'
           ]);
           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
        $HairStyle=HairStyle::find($request->id);
        $HairStyle->hair_style=$request->hair_style;
        $HairStyle->save();
        return $HairStyle;       
    }

    //Updating Hair Facial

    public function updateHairfacial(Request $request){
        $validator = Validator::make($request->all(),[  
               'hair_facial'       =>  'required|unique:hair_facials,hair_facial,'.$request->id,
           ],
           [
               'hair_facial.required'     => 'Enter Hair Facial',
               'hair_facial.unique'       => 'Hair Facial Already Taken'
           ]);
           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
        $HairFacial=HairFacial::find($request->id);
        $HairFacial->hair_facial=$request->hair_facial;
        $HairFacial->save();
        return $HairFacial;       
    }

    //Updating Body Trait

    public function updateBodytrait(Request $request){
        $validator = Validator::make($request->all(),[  
               'body_trait'       =>  'required|unique:body_traits,body_trait,'.$request->id,
           ],
           [
               'body_trait.required'     => 'Enter Body Trait',
               'body_trait.unique'       => 'Body Trait Already Taken'
           ]);
           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
        $BodyTrait=BodyTrait::find($request->id);
        $BodyTrait->body_trait=$request->body_trait;
        $BodyTrait->save();
        return $BodyTrait;       
    }    

    //Updating Tag

    public function updatetag(Request $request){
        $validator = Validator::make($request->all(),[  
               'tag'                     =>  'required|unique:tags,tag,'.$request->id,
           ],
           [
               'tag.required'     => 'Enter Tag Name',
               'tag.unique'       => 'Tag Already Taken',
           ]);

        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }
        $Tags=Tags::find($request->id);
        $Tags->tag=$request->tag;
        $Tags->save();
       return response()->json(['status'=>true,'message'=>'Tag Updated Successfully']);     
    }

    //Updating Flag

    public function updateflag(Request $request){
        
        $validator = Validator::make($request->all(),[  
               'flagname'                   =>  'required|unique:flags,flag_name,'.$request->flagid,
           ],
           [
               'flagname.required'     => 'Enter Flag Name',
               'flagname.unique'       => 'Flag Name Already Taken',
               
           ]);

           if($validator->fails()){
             return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
        $Flags=Flags::find($request->flagid);
        $Flags->flag_name=$request->flagname;
        $Flags->save();       
    }
 
      //Updating Availability

      public function updateavailability(Request $request){
        $validator = Validator::make($request->all(),[  
               'availability'       =>  'required|unique:performer_availability,availability,'.$request->id,
           ],
           [
               'availability.required'    => 'Enter Availability Name',
               'availability.unique'     =>  'Availability Already Taken'
           ]);

           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
         $PerformerAvailability = PerformerAvailability::find($request->id);
         $PerformerAvailability->availability=$request->availability;
         $PerformerAvailability->save();  
      }

 
    //Updating Piercing
    public function updatepiercing(Request $request){
        $validator = Validator::make($request->all(),[  
               'piercing'                   =>  'required|unique:piercings,piercing',
           ]);

           if($validator->fails()){
             return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
         $piercing  = Piercing::find($request->id);
         $piercing->piercing=$request->piercing; 
         $piercing->save();
    }
  
    //Updating Tattoo

    public function updatetattoo(Request $request){
      $validator = Validator::make($request->all(),[  
             'tattoo'                   =>  'required|unique:tattoos,tattoo',
         ]);

         if($validator->fails()){
           return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
         }
       $tattoo  = Tattoo::find($request->id);
       $tattoo->tattoo=$request->tattoo; 
       $tattoo->save();
    }

    //Updating Ethnicity

    public function updateethnicity(Request $request){
        $validator = Validator::make($request->all(),[  
               'ethnicity'       =>  'required|unique:ethnicities,ethnicity,'.$request->ethnicityid,
           ],
           [
               'ethnicity.required'     => 'Enter Ethnicity Name',
               
           ]);
           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
        $Ethnicity=Ethnicity::find($request->ethnicityid);
        $Ethnicity->ethnicity=$request->ethnicity;
        $Ethnicity->save();       
    }
 
    //Updating Union

    public function updateunion(Request $request){
       $validator = Validator::make($request->all(),[  
              'union'                   =>  'required|unique:performer_unions,union,'.$request->unionid,
              'union_tag'               =>  'required',
          ],
          [
              'union.required'     => 'Enter Union Name',
              'union.unique'       => 'Union Already Taken',
              'union_tag.required' => 'Enter Union Tag'
          ]);
          if($validator->fails()){
              return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
          }
        $PerformerUnion=PerformerUnion::find($request->unionid);
        $PerformerUnion->union=$request->union;
        $PerformerUnion->union_tag=$request->union_tag;
        $PerformerUnion->save();       
    }
 
    //Updating Skin Tone

    public function updatetone(Request $request){
        $validator = Validator::make($request->all(),[  
               'skintone'                   =>  'required|unique:skin_tones,skin_tone,'.$request->toneid,
           ],[
               'skintone.required' => 'Enter Skin Tone',
               'skintone.unique'   => ' Skin Tone Already Taken',
           ]);

           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
        $Skintone=Skintone::find($request->toneid);
        $Skintone->skin_tone=$request->skintone;
        $Skintone->save();       
    }
  
    //Updating Eyes

    public function updateeyes(Request $request){
       $validator = Validator::make($request->all(),[  
              'eyes'                   =>  'required|unique:eyes,eyes,'.$request->eyesid,
          ],
           [
               'eyes.required'     => 'Enter Eyes Name',
           ]);

          if($validator->fails()){
              return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
          }
        $Eyes=Eyes::find($request->eyesid);
        $Eyes->eyes=$request->eyes;
        $Eyes->save();       
    }
  
    //Updating Agency

    public function updateAgency(Request $request){
    
      $validator = Validator::make($request->all(),[  
             'agencyName'                   =>  'required|unique:agency,agencyName,'.$request->id,
             'agencyAbbreviation' => 'required'
         ]);

         if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
         }
         $add = Agency::find($request->id);
         $add->agencyName = $request->agencyName;
         $add->agencyAbbreviation = $request->agencyAbbreviation;
         $add->save();
         return response()->json(['status'=>true,'message'=>'Agency Updated Successfully']);
    }

   //Updating Agent

    public function updateagent(Request $request){
        $validator = Validator::make($request->all(),[  
               'agent_name'              =>  'required|unique:performer_agents,agent_name,'.$request->agentid,
               'agency_id'               =>  'required',
           ],
           [
               'agent_name.required'     => 'Enter Agent Name',
               'agent_name.unique'       => 'Agent Already Taken',
               'agency_id.required'      => 'Select Agency Name'
           ]);
        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }
        $PerformerAgent=PerformerAgent::find($request->agentid);
        $PerformerAgent->agent_name=$request->agent_name;
        $PerformerAgent->agency_id=$request->agency_id;
        $PerformerAgent->save();
        return $PerformerAgent;       
    }
  
    //Updating Document

    public function updatedocument(Request $request){
        $validator = Validator::make($request->all(),[  
               'doument_name'       =>  'required',
               'doument_type'       =>  'required',
           ],
           [
               'doument_name.required'     => 'Enter Document Name',
               'doument_type.required'     => 'Select Document Type',
           ]);

           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
        $Document=Document::find($request->documentid);
        $Document->update($request->all());  
        return $Document;    
    }
  
    //Updating Nationality

     public function updatenationality(Request $request){
        $validator = Validator::make($request->all(),[  
               'nationality'       =>  'required|unique:nationalities,nationality,'.$request->nationalityid,
           ],
           [
               'nationality.required'     => 'Enter Nationality Name',
               'nationality.unique'     => 'Nationality Already Taken'
           ]);
        if($validator->fails()){
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }
        $Nationality=Nationality::find($request->nationalityid);
        $Nationality->nationality=$request->nationality;
        $Nationality->save();       
    }
   
    //Updating Hair Color

     public function updateHairColor(Request $request){
        $validator = Validator::make($request->all(),[  
               'haircolor'       =>  'required|unique:hair_colors,hair_color,'.$request->colorid,
           ],
           [
               'haircolor.required'=> 'Enter Hair Color',
               'haircolor.unique'  => 'Hair Color Already Taken'
           ]);
           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
        $HairColor  =HairColor::find($request->colorid);
        $HairColor->hair_color=$request->haircolor;
        $HairColor->save();
    }

    //Displpay PerformerStatus
    public function performerStatus()
    {
      $perfomerstatus    = PerformerStatus::all();
      
       return view('corporate-admin.performerstatus',compact('perfomerstatus'));
    }

    //Store PerformerStatus
    public function addstatus(Request $request)
    {
      $validator = Validator::make($request->all(),[  
               'performer_status'       =>  'required|unique:performer_status,performer_status,'.$request->id,
           ],
           [
               'performer_status.required'    => 'Enter Performer status ',
               'performer_status.unique'     =>  'Performer status Already Taken'
           ]);

           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }

       $status = $request->all();

       $data = new PerformerStatus($status);
       $data->save();
       return response()->json(['status'=>true,'message'=>"Performer Status Added Successfully"]);

    }
    //Remove PerformerStatus
    public function deleteStatus(Request $request)
    {
      $del = PerformerStatus::where("id",$request->id)->first();
      if($del->is_deleted == '1'){
        $del->is_deleted = '0';
        $del->save();
        return response()->json(['message'=>'PerformerStatus Deleted Successfully']);
      }
      else{
         $del->is_deleted = '1';
         $del->save();
        return response()->json(['message'=>'PerformerStatus Undeleted Successfully']);
      }
    }

    //Edit Model Open  PerformerStatus
    public function editStstus(Request $request){
                
        $PerformerStatus=PerformerStatus::find($request->id);
        return $PerformerStatus->performer_status;
    }

    //update PerformerStatus
    public function updateStatus(Request $request){
        $validator = Validator::make($request->all(),[  
               'performer_status'       =>  'required|unique:performer_status,performer_status,'.$request->id,
           ],
           [
               'performer_status.required'    => 'Enter Performer status ',
               'performer_status.unique'     =>  'Performer status Already Taken'
           ]);

           if($validator->fails()){
               return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
           }
         $PerformerStatus = PerformerStatus::find($request->id);
         $PerformerStatus->performer_status=$request->performer_status;
         $PerformerStatus->save();  
      }
}
