<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()=='internal-staff') {
            return redirect('/internal-staff/performers/list');
        }elseif(Auth::guard($guard)->check()=='corporate-admin'){
            return redirect('/corporate-admin/form');
        }elseif(Auth::guard($guard)->check()=='web'){
            return redirect('/performers');
        }
        

    
    return $next($request);
    }
}
